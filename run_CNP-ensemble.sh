#!/bin/tcsh

set RunID    = $1
set InputDir = input

echo '**********************************************************'

#foreach ID ( D4 )
#foreach ID ( A1 A2 A3 A4 A5 A6 B1 B2 B3 B4 B5 B6 C1 C2 C3 C4 C5 C6 D2 D3 D4 D5 D6 E3 E4 E5 E6 F5 F6  )
foreach ID ( C3 C4 C5 D3 D4 D5 E3 E4 E5 )

#echo '**********************************************************'
   echo "**    compile & start ensemble member "${ID}
   #echo '**********************************************************'


   switch ( $ID )
         case C3:   #standard (D093)
            set qCN  = '  6.625'
            set qCP  = '132.5  '
            set qCSi = '  5.76 '
            breaksw
         case A1:
            set qCN  = '  6.625'
            set qCP  = '212.0  '
            set qCSi = '  5.76 '
            breaksw
         case A2:
            set qCN  = '  8.83 '
            set qCP  = '212.0  '
            set qCSi = '  7.68 '
            breaksw
         case A3:
            set qCN  = ' 10.60 '
            set qCP  = '212.0  '
            set qCSi = '  9.22 '
            breaksw
         case A4:
            set qCN  = ' 13.25 '
            set qCP  = '212.0  '
            set qCSi = ' 11.52 '
            breaksw
         case A5:
            set qCN  = ' 17.67 '
            set qCP  = '212.0  '
            set qCSi = ' 15.36 '
            breaksw
         case A6:
            set qCN  = ' 26.50 '
            set qCP  = '212.0  '
            set qCSi = ' 23.04 '
            breaksw
         case B1:
            set qCN  = '  4.97 '
            set qCP  = '159.0  '
            set qCSi = '  4.32 '
            breaksw
         case B2:
            set qCN  = '  6.625'
            set qCP  = '159.0  '
            set qCSi = '  5.76 '
            breaksw
         case B3:
            set qCN  = '  7.95 '
            set qCP  = '159.0  '
            set qCSi = '  6.91 '
            breaksw
         case B4:
            set qCN  = '  9.94 '
            set qCP  = '159.0  '
            set qCSi = '  8.64 '
            breaksw
         case B5:
            set qCN  = ' 13.25 '
            set qCP  = '159.0  '
            set qCSi = ' 11.52 '
            breaksw
         case B6:
            set qCN  = ' 19.88 '
            set qCP  = '159.0  '
            set qCSi = ' 17.28 '
            breaksw
         case C1:
            set qCN  = '  4.14 '
            set qCP  = '132.5  '
            set qCSi = '  3.60 '
            breaksw
         case C2:
            set qCN  = '  5.52 '
            set qCP  = '132.5  '
            set qCSi = '  4.80 '
            breaksw
         case C3:
            set qCN  = '  6.625'
            set qCP  = '132.5  '
            set qCSi = '  5.76 '
            breaksw
         case C4:
            set qCN  = '  8.28 '
            set qCP  = '132.5  '
            set qCSi = '  7.20 '
            breaksw
         case C5:
            set qCN  = ' 11.04 '
            set qCP  = '132.5  '
            set qCSi = '  9.60 '
            breaksw
         case C6:
            set qCN  = ' 16.56 '
            set qCP  = '132.5  '
            set qCSi = ' 14.40 '
            breaksw
         case D2:
            set qCN  = '  4.42 '
            set qCP  = '106.0  '
            set qCSi = '  3.84 '
            breaksw
         case D3:
            set qCN  = '  5.30 '
            set qCP  = '106.0  '
            set qCSi = '  4.61 '
            breaksw
         case D4:
            set qCN  = '  6.625'
            set qCP  = '106.0  '
            set qCSi = '  5.76 '
            breaksw
         case D5:
            set qCN  = '  8.83 '
            set qCP  = '106.0  '
            set qCSi = '  7.68 '
            breaksw
         case D6:
            set qCN  = ' 13.25 '
            set qCP  = '106.0  '
            set qCSi = ' 11.52 '
            breaksw
         case E3:
            set qCN  = '  3.98 '
            set qCP  = ' 79.5  '
            set qCSi = '  3.46 '
            breaksw
         case E4:
            set qCN  = '  4.97 '
            set qCP  = ' 79.5  '
            set qCSi = '  4.32 '
            breaksw
         case E5:
            set qCN  = '  6.625'
            set qCP  = ' 79.5  '
            set qCSi = '  5.76 '
            breaksw
         case E6:
            set qCN  = '  9.94 '
            set qCP  = ' 79.5  '
            set qCSi = '  8.64 '
            breaksw
         case F5:
            set qCN  = '  4.42 '
            set qCP  = ' 53.0  '
            set qCSi = '  3.84 '
            breaksw
         case F6:
            set qCN  = '  6.625'
            set qCP  = ' 53.0  '
            set qCSi = '  5.76 '
            breaksw
         default:
            echo "case default"
            breaksw
   endsw

   echo '**      qCN='$qCN'   qCP='$qCP'   qCSi='$qCSi
   echo '**********************************************************'

   # NOTE: -v qCN="${qCN}" defines an "awk-variabele" filled by the content
   #       of the shell variable, which must be stated in double-quotas to be
   #       interpreted as character-string (and to keep in filling blanks)

   awk -v qCN="${qCN}" -v qCP="${qCP}" -v qCSi="${qCSi}" '{\
         gsub("##qCN##",   qCN);        \
         gsub("##qCP##",   qCP);        \
         gsub("##qCSi#",   qCSi);       \
         print}' ${InputDir}/eco_bio.template.nml > ${InputDir}/eco_bio.nml

   echo '**      ./CompileJob-mistral.sh '$RunID'-'$ID' 2'
   echo '**********************************************************'
   ./CompileJob-mistral.sh $RunID-$ID 2

end

exit 0
