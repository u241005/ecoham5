#!/bin/tcsh
#
#---------------------------------------
# serial job
#---------------------------------------
#
# @ shell=/bin/tcsh
# @ job_type = serial
# @ job_name = MK01_1997
# @ output = $(job_name).o$(jobid)
# @ error = $(job_name).e$(jobid)
# @ notification = complete
# @ resources = ConsumableCpus(1) ConsumableMemory(4gb)
# @ environment= COPY_ALL
# @ node_usage= shared
# @ node = 1
# @ tasks_per_node = 1
# @ wall_clock_limit = 40000
# @ queue

set srcDIR=../src
set runDIR=`pwd`
set TARGET=eco4

echo '======================================================'
echo 'srcDIR:='$srcDIR
echo 'runDIR:='$runDIR
echo '------------------------------------------------------'

cd $srcDIR
if ($1 == "clean") make clean
make
cd $runDIR
ln -sf $srcDIR/$TARGET .

# mandatory for running on blizzard !
#------------------------------------------------------------------------
######################################################
# not needed anymore, as forcing is BIG_ENDIAN !     #
######################################################
# set NAME=`echo $HOST | awk '{print substr($1,1,8)}'`
# if ($NAME == "blizzard") then
#    echo ' '
#    echo '------------------------------------------------------'
#    setenv XLFRTEOPTS "ufmt_littleendian=220,221,222,223,224,225,226"
#    echo 'set XLFRTEOPTS = '$XLFRTEOPTS
# else
# endif
#------------------------------------------------------------------------
echo ' '
echo '======================================================'
echo '  '$TARGET' start running at '`date`
echo '------------------------------------------------------'
#./x1x >& run_x1x.log
./$TARGET >& run_$TARGET.log
echo '------------------------------------------------------'
echo '  '$TARGET' finished at      '`date`
echo '======================================================'
echo ' '

exit