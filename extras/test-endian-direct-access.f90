program test_read
    ! read some elements from a direct access file in both endian modes
    ! ulrich.koerner@zmaw.de 26.10.2012
    implicit none
    integer, parameter :: reclen=5, wordlen=4
    real(kind=4), dimension(reclen) :: data4
    real(kind=8), dimension(reclen) :: data8
    character(len=80) :: inputFile

    call get_command_argument(1,inputFile)
    inputFile = trim(inputFile)

    print "(A, I3, A)", 'read', reclen, ' elements in both endian modes from the direct access file:'
    print "(A)", inputFile
    print "(60('='))"

    open(unit=20, file=inputFile, convert='little_endian', form='unformatted', access='direct', recl=reclen*wordlen)
        read(20,rec=1) data4
    close(20)
    print*, 'little_endian, real(kind=4)'
    print*, data4

    print "(40('-'))"

    open(unit=20, file=inputFile, convert='big_endian', form='unformatted', access='direct', recl=reclen*wordlen)
        read(20,rec=1) data4
    close(20)
    print*, 'big_endian, real(kind=4)'
    print*, data4

    print "(80('-'))"

    open(unit=20, file=inputFile, convert='little_endian', form='unformatted', access='direct', recl=2*reclen*wordlen)
        read(20,rec=1) data8
    close(20)
    print*, 'little_endian, real(kind=8)'
    print*, data8

    print "(40('-'))"

    open(unit=20, file=inputFile, convert='big_endian', form='unformatted', access='direct', recl=2*reclen*wordlen)
        read(20,rec=1) data8
    close(20)
    print*, 'big_endian, real(kind=8)'
    print*, data8
end program test_read
