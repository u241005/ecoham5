#!/bin/csh

#  Author: Markus Kreus, 2016
#          Institute of Oceanography, Hamburg

set RunID = $1

set InputPath   = $HOME/scratch/ECOHAM.${RunID}/res.${RunID}
#set InputPath   = $HOME/scratch/ECOHAM.${RunID}.data
set OutputPath  = $HOME/scratch/ECOHAM.${RunID}.netcdf


#ncatted -O -a units,,m,c,"days since 1977-01-01 00:00" B016b_3D.1977.nc

#set yearS       = 1977
#set yearE       = 2012
set yearS       = $2
set yearE       = $3

set year = ${yearS}
echo '=============================================================='
while ( ${year} <=  ${yearE} )

  echo '-- work on files for year '${year}' --'

  # extract variables for TBNT-Tool
  cd ${OutputPath}
  mkdir -p ${OutputPath}/TBNT

#  echo '=============================================================='
#  echo './extract_TBNT.sh X '${InputPath}'/'${RunID}'.'${year}'.00/'${RunID}'_3D.nc TBNT/'${RunID}'_TBNT-X.'${year}'.nc'
#  echo '=============================================================='
#  ./extract_TBNT.sh X ${InputPath}/${RunID}.${year}.00/${RunID}_3D.nc TBNT/${RunID}_TBNT-X.${year}.nc

  echo '=============================================================='
  echo './extract_TBNT.sh N '${InputPath}'/'${RunID}'.'${year}'.00/'${RunID}'_3D.nc TBNT/'${RunID}'_TBNT-N.'${year}'.nc'
  echo '=============================================================='
  ./extract_TBNT.sh N ${InputPath}/${RunID}.${year}.00/${RunID}_3D.nc TBNT/${RunID}_TBNT-N.${year}.nc

  echo '=============================================================='
  echo './extract_TBNT.sh O '${InputPath}'/'${RunID}'.'${year}'.00/'${RunID}'_3D.nc TBNT/'${RunID}'_TBNT-O.'${year}'.nc'
  echo '=============================================================='
  ./extract_TBNT.sh O ${InputPath}/${RunID}.${year}.00/${RunID}_3D.nc TBNT/${RunID}_TBNT-O.${year}.nc


  mkdir -p ${OutputPath}/${year}
  cd ${OutputPath}/${year}

  #set if = ${InputPath}/${RunID}.${year}.00/${RunID}_3D.nc
  #set of = ${OutputPath}/${year}/${RunID}_3D.${year}.nc
  #mkdir -p ${OutputPath}/${year}
  #echo '-- copying nc-file '${if}' to '${of}
  #cp ${if} ${of}

  #ln -s ${InputPath}/${RunID}.${year}.00/${RunID}_3D.nc .
  #cdo delname,PAR_mean ${RunID}_3D.nc tmp.nc # NOT NEEDED ANY MORE
  ln -s ${InputPath}/${RunID}.${year}.00/${RunID}_3D.nc tmp.nc
  cdo splitname tmp.nc ${RunID}_3D.${year}.

  rm tmp.nc
  rm ${RunID}_3D.nc

  echo '-- compress nc-files'
  gzip *.nc

  echo '-- compress '${InputPath}'/'${RunID}'.'${year}'.00/'${RunID}'_3D.nc'
  gzip ${InputPath}/${RunID}.${year}.00/${RunID}_3D.nc

  #echo ''
  echo '=============================================================='

  @ year++

end

exit