#!/bin/csh

#  Author: Markus Kreus, 2016
#          Institute of Oceanography, Hamburg

set var=$1
set fileIn=$2
set fileOut=$3

switch ($var)
     case x1x:
         echo 'extract x1x-CYCLE'
         set varlist='x1x'
         set varlist=$varlist',f_x1x_sed,f_pev_x1x,f_res_x1x,f_riv_x1x,f_dil_x1x'
         set varlist=$varlist',f_mu_x1x,f_mv_x1x,f_mw_x1x,f_tu_x1x,f_tv_x1x,f_tw_x1x'
         breaksw
     case C:
         echo 'extract C-CYCLE'
         set varlist='dic,p1c,p2c,p3c,p3k,z1c,z2c,d1c,d2c,d2k,doc,soc,bac,sd_poc,sd_pok'
         set varlist=$varlist',f_dic_p1c,f_p1c_z1c,f_p1c_z2c,f_p1c_d1c,f_p1c_d2c,f_p1c_doc,f_p1c_soc'
         set varlist=$varlist',f_dic_p2c,f_p2c_z1c,f_p2c_z2c,f_p2c_d1c,f_p2c_d2c,f_p2c_doc,f_p2c_soc'
         set varlist=$varlist',f_dic_p3c,f_p3c_z1c,f_p3c_z2c,f_p3c_d1c,f_p3c_d2c,f_p3c_doc,f_p3c_soc'
         set varlist=$varlist',f_dic_p3k,f_p3k_z1c,f_p3k_z2c,f_p3k_d2k'
         set varlist=$varlist',f_bac_z1c,f_d1c_z1c,f_z1c_dic,f_z1c_d1c,f_z1c_d2c,f_z1c_doc,f_z1c_z2c'
         set varlist=$varlist',f_bac_z2c,f_d1c_z2c,f_z2c_dic,f_z2c_d1c,f_z2c_d2c,f_z2c_doc'
         set varlist=$varlist',f_d1c_doc,f_d2c_doc,f_soc_doc,f_doc_bac,f_bac_dic'
         set varlist=$varlist',f_dic_psk,f_psk_dic,f_psk_d2k,f_d2k_dic,f_air_o2c'
         set varlist=$varlist',f_mu_dic,f_mu_p1c,f_mu_p2c,f_mu_p3c,f_mu_p3k,f_mu_z1c,f_mu_z2c,f_mu_d1c,f_mu_d2c,f_mu_d2k,f_mu_doc,f_mu_soc,f_mu_bac'
         set varlist=$varlist',f_mv_dic,f_mv_p1c,f_mv_p2c,f_mv_p3c,f_mv_p3k,f_mv_z1c,f_mv_z2c,f_mv_d1c,f_mv_d2c,f_mv_d2k,f_mv_doc,f_mv_soc,f_mv_bac'
         set varlist=$varlist',f_mw_dic,f_mw_p1c,f_mw_p2c,f_mw_p3c,f_mw_p3k,f_mw_z1c,f_mw_z2c,f_mw_d1c,f_mw_d2c,f_mw_d2k,f_mw_doc,f_mw_soc,f_mw_bac'
         set varlist=$varlist',f_tu_dic,f_tu_p1c,f_tu_p2c,f_tu_p3c,f_tu_p3k,f_tu_z1c,f_tu_z2c,f_tu_d1c,f_tu_d2c,f_tu_d2k,f_tu_doc,f_tu_soc,f_tu_bac'
         set varlist=$varlist',f_tv_dic,f_tv_p1c,f_tv_p2c,f_tv_p3c,f_tv_p3k,f_tv_z1c,f_tv_z2c,f_tv_d1c,f_tv_d2c,f_tv_d2k,f_tv_doc,f_tv_soc,f_tv_bac'
         set varlist=$varlist',f_tw_dic,f_tw_p1c,f_tw_p2c,f_tw_p3c,f_tw_p3k,f_tw_z1c,f_tw_z2c,f_tw_d1c,f_tw_d2c,f_tw_d2k,f_tw_doc,f_tw_soc,f_tw_bac'
         set varlist=$varlist',f_dil_dic,f_dil_p1c,f_dil_p2c,f_dil_p3c,f_dil_p3k,f_dil_z1c,f_dil_z2c,f_dil_d1c,f_dil_d2c,f_dil_d2k,f_dil_doc,f_dil_soc,f_dil_bac'
         set varlist=$varlist',f_riv_dic,f_riv_p1c,f_riv_p2c,f_riv_p3c,f_riv_p3k,f_riv_z1c,f_riv_z2c,f_riv_d1c,f_riv_d2c,f_riv_d2k,f_riv_doc,f_riv_soc,f_riv_bac'
         set varlist=$varlist',f_pev_dic,f_pev_p1c,f_pev_p2c,f_pev_p3c,f_pev_p3k,f_pev_z1c,f_pev_z2c,f_pev_d1c,f_pev_d2c,f_pev_d2k,f_pev_doc,f_pev_soc,f_pev_bac'
         set varlist=$varlist',f_res_dic,f_res_p1c,f_res_p2c,f_res_p3c,f_res_p3k,f_res_z1c,f_res_z2c,f_res_d1c,f_res_d2c,f_res_d2k,f_res_doc,f_res_soc,f_res_bac'
         set varlist=$varlist',f_sed_dic,f_sed_o3c'
         set varlist=$varlist',f_dic_sed,f_p1c_sed,f_p2c_sed,f_p3c_sed,f_p3k_sed,f_z1c_sed,f_z2c_sed,f_d1c_sed,f_d2c_sed,f_d2k_sed,f_doc_sed,f_soc_sed,f_bac_sed'
         breaksw
     case N:
         echo 'extract N-CYCLE'
         set varlist='n3n,n4n,p1n,p2n,p3n,z1n,z2n,d1n,d2n,don,ban'
         set varlist=$varlist',f_n3n_p1n,f_n4n_p1n,f_p1n_z1n,f_p1n_z2n,f_p1n_d1n,f_p1n_d2n,f_p1n_don'
         set varlist=$varlist',f_n3n_p2n,f_n4n_p2n,f_p2n_z1n,f_p2n_z2n,f_p2n_d1n,f_p2n_d2n,f_p2n_don'
         set varlist=$varlist',f_n3n_p3n,f_n4n_p3n,f_p3n_z1n,f_p3n_z2n,f_p3n_d1n,f_p3n_d2n,f_p3n_don'
         set varlist=$varlist',f_d1n_z1n,f_ban_z1n,f_z1n_d1n,f_z1n_d2n,f_z1n_don,f_z1n_n4n'
         set varlist=$varlist',f_d1n_z2n,f_ban_z2n,f_z2n_d1n,f_z2n_d2n,f_z2n_don,f_z2n_n4n'
         set varlist=$varlist',f_z1n_z2n,f_d1n_don,f_d2n_don,f_don_ban,f_n4n_ban,f_ban_n4n'
         set varlist=$varlist',f_n4n_n3n,f_n3n_nn2,f_n3n_brm,f_atm_n3n,f_atm_n4n'
         set varlist=$varlist',f_mu_n3n,f_mu_n4n,f_mu_p1n,f_mu_p2n,f_mu_p3n,f_mu_z1c,f_mu_z2c,f_mu_d1n,f_mu_d2n,f_mu_don,f_mu_bac'
         set varlist=$varlist',f_mv_n3n,f_mv_n4n,f_mv_p1n,f_mv_p2n,f_mv_p3n,f_mv_z1c,f_mv_z2c,f_mv_d1n,f_mv_d2n,f_mv_don,f_mv_bac'
         set varlist=$varlist',f_mw_n3n,f_mw_n4n,f_mw_p1n,f_mw_p2n,f_mw_p3n,f_mw_z1c,f_mw_z2c,f_mw_d1n,f_mw_d2n,f_mw_don,f_mw_bac'
         set varlist=$varlist',f_tu_n3n,f_tu_n4n,f_tu_p1n,f_tu_p2n,f_tu_p3n,f_tu_z1c,f_tu_z2c,f_tu_d1n,f_tu_d2n,f_tu_don,f_tu_bac'
         set varlist=$varlist',f_tv_n3n,f_tv_n4n,f_tv_p1n,f_tv_p2n,f_tv_p3n,f_tv_z1c,f_tv_z2c,f_tv_d1n,f_tv_d2n,f_tv_don,f_tv_bac'
         set varlist=$varlist',f_tw_n3n,f_tw_n4n,f_tw_p1n,f_tw_p2n,f_tw_p3n,f_tw_z1c,f_tw_z2c,f_tw_d1n,f_tw_d2n,f_tw_don,f_tw_bac'
         set varlist=$varlist',f_dil_n3n,f_dil_n4n,f_dil_p1n,f_dil_p2n,f_dil_p3n,f_dil_z1c,f_dil_z2c,f_dil_d1n,f_dil_d2n,f_dil_don,f_dil_bac'
         set varlist=$varlist',f_riv_n3n,f_riv_n4n,f_riv_p1n,f_riv_p2n,f_riv_p3n,f_riv_z1c,f_riv_z2c,f_riv_d1n,f_riv_d2n,f_riv_don,f_riv_bac'
         set varlist=$varlist',f_pev_n3n,f_pev_n4n,f_pev_p1n,f_pev_p2n,f_pev_p3n,f_pev_z1c,f_pev_z2c,f_pev_d1n,f_pev_d2n,f_pev_don,f_pev_bac'
         set varlist=$varlist',f_res_n3n,f_res_n4n,f_res_p1n,f_res_p2n,f_res_p3n,f_res_z1c,f_res_z2c,f_res_d1n,f_res_d2n,f_res_don,f_res_bac'
         set varlist=$varlist',f_sed_n3n,f_sed_n4n,f_sed_nn2'
         set varlist=$varlist',f_n3n_sed,f_n4n_sed,f_p1n_sed,f_p2n_sed,f_p3n_sed,f_z1c_sed,f_z2c_sed,f_d1n_sed,f_d2n_sed,f_don_sed,f_bac_sed'
         breaksw
     case P:
         echo 'extract P-CYCLE'
         set varlist='n1p,p1p,p2p,p3p,z1p,z2p,d1p,d2p,dop,bap,sd_pop'
         set varlist=$varlist',f_n1p_p1p,f_p1p_z1p,f_p1p_z2p,f_p1p_d1p,f_p1p_d2p,f_p1p_dop'
         set varlist=$varlist',f_n1p_p2p,f_p2p_z1p,f_p2p_z2p,f_p2p_d1p,f_p2p_d2p,f_p2p_dop'
         set varlist=$varlist',f_n1p_p3p,f_p3p_z1p,f_p3p_z2p,f_p3p_d1p,f_p3p_d2p,f_p3p_dop,f_dop_p3p'
         set varlist=$varlist',f_d1p_z1p,f_bap_z1p,f_z1p_d1p,f_z1p_d2p,f_z1p_dop,f_z1p_n1p'
         set varlist=$varlist',f_d1p_z2p,f_bap_z2p,f_z2p_d1p,f_z2p_d2p,f_z2p_dop,f_z2p_n1p'
         set varlist=$varlist',f_z1p_z2p,f_d1p_dop,f_d2p_dop,f_dop_bap,f_n1p_bap,f_bap_n1p'
         set varlist=$varlist',f_mu_n1p,f_mu_p1p,f_mu_p2p,f_mu_p3p,f_mu_z1c,f_mu_z2c,f_mu_d1p,f_mu_d2p,f_mu_dop,f_mu_bac'
         set varlist=$varlist',f_mv_n1p,f_mv_p1p,f_mv_p2p,f_mv_p3p,f_mv_z1c,f_mv_z2c,f_mv_d1p,f_mv_d2p,f_mv_dop,f_mv_bac'
         set varlist=$varlist',f_mw_n1p,f_mw_p1p,f_mw_p2p,f_mw_p3p,f_mw_z1c,f_mw_z2c,f_mw_d1p,f_mw_d2p,f_mw_dop,f_mw_bac'
         set varlist=$varlist',f_tu_n1p,f_tu_p1p,f_tu_p2p,f_tu_p3p,f_tu_z1c,f_tu_z2c,f_tu_d1p,f_tu_d2p,f_tu_dop,f_tu_bac'
         set varlist=$varlist',f_tv_n1p,f_tv_p1p,f_tv_p2p,f_tv_p3p,f_tv_z1c,f_tv_z2c,f_tv_d1p,f_tv_d2p,f_tv_dop,f_tv_bac'
         set varlist=$varlist',f_tw_n1p,f_tw_p1p,f_tw_p2p,f_tw_p3p,f_tw_z1c,f_tw_z2c,f_tw_d1p,f_tw_d2p,f_tw_dop,f_tw_bac'
         set varlist=$varlist',f_dil_n1p,f_dil_p1p,f_dil_p2p,f_dil_p3p,f_dil_z1c,f_dil_z2c,f_dil_d1p,f_dil_d2p,f_dil_dop,f_dil_bac'
         set varlist=$varlist',f_riv_n1p,f_riv_p1p,f_riv_p2p,f_riv_p3p,f_riv_z1c,f_riv_z2c,f_riv_d1p,f_riv_d2p,f_riv_dop,f_riv_bac'
         set varlist=$varlist',f_pev_n1p,f_pev_p1p,f_pev_p2p,f_pev_p3p,f_pev_z1c,f_pev_z2c,f_pev_d1p,f_pev_d2p,f_pev_dop,f_pev_bac'
         set varlist=$varlist',f_res_n1p,f_res_p1p,f_res_p2p,f_res_p3p,f_res_z1c,f_res_z2c,f_res_d1p,f_res_d2p,f_res_dop,f_res_bac'
         set varlist=$varlist',f_n1p_sed,f_p1p_sed,f_p2p_sed,f_p3p_sed,f_z1c_sed,f_z2c_sed,f_d1p_sed,f_d2p_sed,f_dop_sed,f_bac_sed,f_sed_n1p'
         breaksw
     case O*:
         echo 'extract O2-CYCLE'
         set varlist='o2o'
         set varlist=$varlist',f_air_o2o,f_p1c_o2o,f_p2c_o2o'
         set varlist=$varlist',f_o2o_z1c,f_o2o_z2c,f_o2o_bac,f_o2o_brm,f_o2o_n4n'
         set varlist=$varlist',f_mu_o2o,f_mv_o2o,f_mw_o2o,f_tu_o2o,f_tv_o2o,f_tw_o2o'
         set varlist=$varlist',f_dil_o2o,f_riv_o2o,f_pev_o2o,f_res_o2o,f_o2o_sed'
         breaksw
     default:
         echo ' WARNING: no valid cycling element found!'
         exit
         breaksw
endsw

cdo selname,$varlist,vol,area $fileIn $fileOut

exit
