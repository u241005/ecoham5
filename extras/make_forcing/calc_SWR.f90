program calc_SWR
! !DESCRIPTION:
! this program calculates incoming SWR according to geographical location and cloudiness
!
! created by Markus Kreus based on template "mak_rad,f90" (JP)

   use mod_grid_parameters

   implicit none
!
! !LOCAL PARAMETERS:
   real, parameter    :: zero           = 0.0
   real, parameter    :: one            = 1.0
   real, parameter    :: fail           = -9.999e10

   integer, parameter :: hours_per_day  = 24
   integer, parameter :: meteotimestep  = 6     ! - timestep of meteo forcing (h)
   integer, parameter :: radsoltimestep = 2     ! - timestep of solar forcing (h)
   integer, parameter ::         dt_int = 1     ! - timestep (h) for integration / approximation of swr

   !integer, parameter :: meteo_realkind = 8   != ibyte_per_real
   integer, parameter :: meteo_realkind = 4   != ibyte_per_real

! IO - UNITS:
!------------------------------
   integer, parameter :: grid_unit      = 10
   integer, parameter :: clou_unit      = 20
#ifdef BIGEND
   character(len=*), parameter :: OUTPUT_ENDIAN = "BIG_ENDIAN"
   integer, parameter :: rad_unit       = 31
   integer, parameter :: radday_unit    = 32
#else
   character(len=*), parameter :: OUTPUT_ENDIAN = "LITTLE_ENDIAN"
   integer, parameter :: rad_unit       = 21
   integer, parameter :: radday_unit    = 22
#endif

! !LOCAL VARIABLES:
! grid
   real,    dimension(imax)      :: xlat
   integer, dimension(jmax,imax) :: k_index
   logical, dimension(jmax,imax) :: lexistk
   integer :: imaxr, jmaxr, kmaxr, iwet, iwet2

! data
   real, dimension(iiwet2)       :: buffer
   real, dimension(jmax,imax)    :: clou
   real, dimension(jmax,imax)    :: rad
   real, dimension(jmax,imax)    :: radday

   character(len=99) :: meteo_dir     ='./tmp/'
   character(len=99) :: meteo_suffix  ='.direct'
   character(len=99) :: filename
   character(len=4)  :: yearstr
   logical           :: lReadSglPrec = .true.

! parameters
   real, parameter   :: a_rp  = 0.06e0
   real, parameter   :: cc_rp = 0.4e0
   !real, parameter   :: cp_rp = 3.986e3


! variables
   real    :: time, qs_rp, c_rp, hfl_in
   integer :: year, ih, it, is
   integer :: idoy, irec_in, irec_out, irec_out2, ios
   integer :: i, j, k ,iargc
   logical :: first = .true.   ! set to .true., if "NULL-record" is desired
   !logical :: first = .false. ! set to .false., to disable writing "NULL-record"
   logical :: first_rad, first_radday
!

#ifdef DEBUG
   real(kind=meteo_realkind), dimension(iiwet2)       :: buffer_r4
   character(len=99) :: debug_dir     ='./DEBUG/'
   character(len=99) :: fmtstr
#endif
!
!-----------------------------------------------------------------------

   ! init grid
   filename = trim(gridfile)
   open(grid_unit,file=trim(filename),status='old')
   read(grid_unit,*)jmaxr,kmaxr,imaxr,iwet,iwet2 ! use this for reading indh-GridID.dat !!
   read(grid_unit,*)((k_index(j,i),j=1,jmax),i=1,imax)
   close(grid_unit)

   lexistk = .false.
   where (k_index > 0) lexistk = .true.

   do i=1,imax
      xlat(imax+1-i) = xlat0 + delta_lat*float(i-1)
   enddo

! get Year as program parameter
   if (iargc() > 0) then
      call getarg(1, yearstr)
      read(yearstr, *) Year
   else ! default settings
!       open(1, file = 'Year.dat')
!       read(1, '(i4)') Year
!       write(yearstr,'(i4)') Year
!       close(1)
      print*,' enter year to process: '
      read(*, '(i4)') Year
      write(yearstr,'(i4)') Year
   endif

! open input file for cloudiness
   filename = trim(meteo_dir)//'Clou'//yearstr//trim(meteo_suffix)    !ClouYYYY.direct
   !print*,' read cloudiness from file: ',filename
   open(clou_unit,  file   = trim(filename),              &
                    form   = 'unformatted',               &
                    status = 'old',                       &
                    access = 'direct',                    &
                    convert= 'LITTLE_ENDIAN',             &
                    recl   = iiwet2 * meteo_realkind)

! open output file for sw-radiation
   filename = trim(meteo_dir)//'rad'//yearstr//trim(meteo_suffix)     !radYYYY.direct
   open(rad_unit,   file   = trim(filename),              &
                    form   = 'unformatted',               &
                    status = 'replace',                   &
                    access = 'direct',                    &
                    convert= OUTPUT_ENDIAN,               &
                    recl   = iiwet2 * meteo_realkind)

! open output file for daiy averaged sw-radiation
   filename = trim(meteo_dir)//'radday'//yearstr//trim(meteo_suffix)  !raddayYYYY.direct
   open(radday_unit,file   = trim(filename),              &
                    form   = 'unformatted',               &
                    status = 'replace',                   &
                    access = 'direct',                    &
                    convert= OUTPUT_ENDIAN,               &
                    recl   = iiwet2 * meteo_realkind)
#ifdef DEBUG
! open output file for sw-radiation
   filename = trim(meteo_dir)//'rad'//yearstr//'.asc'     !radYYYY.asc
   open(11,         file   = trim(filename),              &
                    form   = 'formatted',                 &
                    status = 'replace')

! open output file for daiy averaged sw-radiation
   filename = trim(meteo_dir)//'radday'//yearstr//'.asc'  !raddayYYYY.asc
   open(12,         file   = trim(filename),              &
                    form   = 'formatted',                 &
                    status = 'replace')
   write(fmtstr,'(''('',i6,''(e20.10,2x))'')') iiwet2
#endif

! loop over any record found in cloudiness file
   ih   = 0
   idoy = 1
   irec_in  = 0
   irec_out = 0
   irec_out2= 0
   radday = 0.0
   first_rad    = first
   first_radday = first
   do while (.true.)
      rad=0.0
      irec_in  = irec_in  +1
      ! read actual cloudiness
      !write (*,'(1x,a,i0)') '***  ReadClou: read recNo ', irec_in
      if (lReadSglPrec) then
         call read_sp_real (clou_unit, buffer(1:iiwet2), iiwet2, ios, irec_in)
      else
         read(clou_unit, rec = irec_in, iostat = ios) buffer(1:iiwet2)
      endif
      if (ios /= 0) exit
      write (*,'(1x,a,i0)') '***  ReadClou: read recNo ', irec_in
      clou = unpack(real(buffer), lexistk, 0.0)

      do is=1,meteotimestep/dt_int
         it=it+1
         ih=ih+1

         time=float((irec_in-1)*meteotimestep+(is-1)*dt_int) ! in hours
         !time=float((idoy-1)*hours_per_day+(ih-1)*dt_int) ! in hours

         !print*,'#173',is,it,ih,time
         do  i = 1, imax
            do  j = 1, jmax
               if (lexistk(j,i)) then
                  ! calculate local solar radiation
                  c_rp  = min(100., max( 0., clou(j,i)))
                  call solrad(time, xlat(i), qs_rp)
                  hfl_in = qs_rp *(1.0e0 - 0.01e0*c_rp*cc_rp - 0.38e0*c_rp*c_rp*1.0e-4) *(1.0e0 - a_rp)
!if (i==1 .and. j==1) print*,'#152 ',idoy, ih, time, qs_rp, clou(j,i),hfl_in

                  if(hfl_in<0.0)then
                     print*,time,i,j,xlat(j),qs_rp,c_rp,hfl_in
                     read*,k
                  endif

                  call xmit(hfl_in, rad(j,i),    it)
                  call xmit(hfl_in, radday(j,i), ih)
               endif
            enddo !do j=1,jmax
         enddo !do i=1,imax


         if ( mod(is*dt_int,radsoltimestep)==0 ) then
            irec_out = irec_out +1
            buffer = pack( rad, lexistk )
            if (meteo_realkind == 4) then
               call write_sp_real (rad_unit, buffer, iiwet2, ios, irec_out)
            else
               write (rad_unit, rec = irec_out, iostat = ios) buffer
            endif
#ifdef DEBUG
            write (11, trim(fmtstr)) real(buffer,meteo_realkind)
#endif

            if (first_rad) then  ! write additional "NULL-record"
               irec_out = irec_out+1
               if (meteo_realkind == 4) then
                  call write_sp_real (rad_unit, buffer, iiwet2, ios, irec_out)
               else
                  write (rad_unit, rec = irec_out, iostat = ios) buffer
               endif
#ifdef DEBUG
               write (11, trim(fmtstr)) real(buffer,meteo_realkind)
#endif
               write (*,'(1x,a,i0)') 'Rad: write extra recNo ',irec_out
               first_rad = .false.
            endif

            it = 0
            rad= 0.0
         endif

      enddo !do is=1,meteotimestep/dt_int

      if ( mod(ih*dt_int,hours_per_day)==0 ) then
         idoy = idoy +1
         irec_out2 = irec_out2 +1
         buffer = pack( radday, lexistk )
         if (meteo_realkind == 4) then
            call write_sp_real (radday_unit, buffer, iiwet2, ios, irec_out2)
         else
            write (radday_unit, rec = irec_out2, iostat = ios) buffer
         endif
#ifdef DEBUG
         write (12, trim(fmtstr)) real(buffer,meteo_realkind)
#endif
         write (*,'(1x,a,i0)') 'Radday: write recNo ', irec_out2
         ! goto 99

         if (first_radday) then  ! write additional "NULL-record"
            irec_out2 = irec_out2 +1
            if (meteo_realkind == 4) then
               call write_sp_real (radday_unit, buffer, iiwet2, ios, irec_out2)
            else
               write (radday_unit, rec = irec_out2, iostat = ios) buffer
            endif
#ifdef DEBUG
            write (12, trim(fmtstr)) real(buffer,meteo_realkind)
#endif
            write (*,'(1x,a,i0)') 'Radday: write extra recNo ',irec_out2
            first_radday = .false.
         endif

         ih = 0
         radday = 0.0
         !exit
      endif

   enddo !do while

! ! append one record of data ...
!    buffer = pack( rad, lexistk )
!    if (meteo_realkind == 4) then
!       call write_sp_real (rad_unit, buffer, iiwet2, ios, irec)
!    else
!       write (rad_unit, rec = irec, iostat = ios) buffer
!    endif
!
!    buffer = pack( radday, lexistk )
!    if (meteo_realkind == 4) then
!       call write_sp_real (radday_unit, buffer, iiwet2, ios, irec_out2)
!    else
!       write (radday_unit, rec = irec_out2, iostat = ios) buffer
!    endif

   print*,'records read for cloudiness: ',irec_in
   print*,'records written for rad:     ',irec_out
   print*,'records written for radday:  ',irec_out2

! 99 continue
   close(clou_unit)
   close(rad_unit)
   close(radday_unit)

#ifdef DEBUG
   close(11)
   close(12)

!----------------------------------------------------------
   if (.true.) then
! open output file for daily averaged sw-radiation
   filename = trim(debug_dir)//'radday'//yearstr//trim(meteo_suffix)  !raddayYYYY.direct
   print*,'<<DEBUG>> convert to ascii from: ',trim(filename)
   open(31,         file   = trim(filename),              &
                    form   = 'unformatted',               &
                    status = 'old',                       &
                    access = 'direct',                    &
                    convert= 'BIG_ENDIAN',                &
                    recl   = iiwet2 * meteo_realkind)

! open output file for ascii daily averaged sw-radiation
   !filename = trim(debug_dir)//'radday'//yearstr//'-00.asc'  !raddayYYYY.asc
   filename = trim(debug_dir)//'radday'//yearstr//'.asc'  !raddayYYYY.asc
   open(12,         file   = trim(filename),              &
                    form   = 'formatted',                 &
                    status = 'replace')
   write(fmtstr,'(''('',i6,''(e20.10,2x))'')') iiwet2

! loop over any record found in file
   irec_in = 1
   do while (.true.)
      read(31, rec = irec_in, iostat = ios) buffer_r4(1:iiwet2)
!print*,irec_in, buffer(1:10)
      if (ios /= 0) exit
      !if (ios /= 0) then
      !   print*,'error reading ',radday_unit,irec_in
      !   exit
      !endif
      write (12, trim(fmtstr)) buffer_r4
      irec_in = irec_in + 1
      !if (irec_in > 1) exit
   enddo

   close(31)
   close(12)
   endif
!----------------------------------------------------------
   if (.true.) then
! open input file for sw-radiation
   filename = trim(debug_dir)//'rad'//yearstr//trim(meteo_suffix)     !radYYYY.direct
   print*,'<<DEBUG>> convert to ascii from: ',trim(filename)
   open(32,         file   = trim(filename),              &
                    form   = 'unformatted',               &
                    status = 'old',                       &
                    access = 'direct',                    &
                    convert= 'BIG_ENDIAN',                &
                    recl   = iiwet2 * meteo_realkind)
! open output file for ascii sw-radiation
   !filename = trim(debug_dir)//'rad'//yearstr//'-00.asc'  !raddayYYYY.asc
   filename = trim(debug_dir)//'rad'//yearstr//'.asc'  !raddayYYYY.asc
   open(11,         file   = trim(filename),              &
                    form   = 'formatted',                 &
                    status = 'replace')
   write(fmtstr,'(''('',i6,''(e20.10,2x))'')') iiwet2

! loop over any record found in file
   irec_in = 1
   do while (.true.)
      read(32, rec = irec_in, iostat = ios) buffer_r4(1:iiwet2)
!print*,irec_in, buffer(1:10)
      if (ios /= 0) exit
      !if (ios /= 0) then
      !   print*,'error reading ',rad_unit,irec_in
      !   exit
      !endif
      write (11, trim(fmtstr)) buffer_r4
      irec_in = irec_in + 1
      !if (irec_in > 12) exit
   enddo

   close(32)
   close(11)
!----------------------------------------------------------
   endif
#endif

   end

!==========================================================
! CONTAINS
!==========================================================

   subroutine idays_of_month(iyr,imon,ida,isw)
   implicit none
   integer id(12),id_sch(12),isw,iyr,imon,ida,isum,idm,i
   id(1)=31
   id(2)=28
   id(3)=31
   id(4)=30
   id(5)=31
   id(6)=30
   id(7)=31
   id(8)=31
   id(9)=30
   id(10)=31
   id(11)=30
   id(12)=31
   id_sch(1)=31
   id_sch(2)=29
   id_sch(3)=31
   id_sch(4)=30
   id_sch(5)=31
   id_sch(6)=30
   id_sch(7)=31
   id_sch(8)=31
   id_sch(9)=30
   id_sch(10)=31
   id_sch(11)=30
   id_sch(12)=31
! it does not matter if for example iyr=76 or instead 1976 is used
!            also iyr>2000 is valid

! isw=0: Berechnung der Anzahl der Tage des Monats imon im Jahr iyr
!        in : isw,iyr,imon
!        out: ida

! isw=1,..31: Berechnung der Anzahl der Tage bis zum isw.imon.iyr
!        in : isw,iyr,imon
!        out: ida


! isw<0: Berechnung des Datums isw.imon.iyr des Tags ida
!        in : isw,ida,iyr
!        out: ida,imon,iyr

   if(isw==0)then
      if(mod(iyr,4)==0.and.mod(iyr,100)/=0)then
         ida = id_sch(imon)
      else
         ida = id(imon)
      endif
      if(mod(iyr,1000)==0)then
         ida = id_sch(imon)
      endif
   endif
   if(isw>0.and.isw<=31)then
      isum = isw
      do i=1,imon-1
         if(mod(iyr,4)==0.and.mod(iyr,100)/=0)then
            idm = id_sch(i)
         else
            idm = id(i)
         endif
         if(mod(iyr,1000)==0)then
            idm = id_sch(i)
         endif
         isum=isum+idm
      enddo
      ida = isum
   endif
   if(isw<0)then
      imon = 12
      isum = id(1)
      do i=2,12
         if(ida<=isum)then
            imon = i-1
            goto 3
         endif
         if(mod(iyr,4)==0.and.mod(iyr,100)/=0)then
            idm = id_sch(i)
         else
            idm = id(i)
         endif
         if(mod(iyr,1000)==0)then
            idm = id_sch(i)
         endif
         isum=isum+idm
      enddo
      3 continue
      if(mod(iyr,4)==0.and.mod(iyr,100)/=0)then
         idm = id_sch(imon)
      else
         idm = id(imon)
      endif
      if(mod(iyr,1000)==0)then
         idm = id_sch(imon)
      endif
      isum = isum -idm
      ida = ida - isum
   endif
   return
   end

!==========================================================
   subroutine xmit(x,xm,i)
   implicit none
   integer i
   real x,xm
   if(i.eq.1)then
   xm=x
   else
   xm=xm+(x-xm)/float(i)
   endif
   return
   end

!==========================================================
      subroutine solrad(tm, rlat, qs)
!----------------------------------------------------------
!
!     title - SOLRAD      vr - 1.0      author - AL      date - 91nov07
!
!     Subroutine to calculate the solar radiation as a function of
!     the position of the sun which varies over the day as well as
!     over the year.
!
! Argument list
!
!   tm      - real input -  time in hours  TM=0 at midnight January 1st
!   rlat    - real input -  latitude  positive N  (degrees)
!   qs      - real output-  solar radiation at the sea surface  (W/m2)
!
! Variable list
!
!   pi      - real -  3.1415926536
!   dg2rd   - real -  conversion factor from degrees to radians  pi/180.0
!   decln   - real -  maximum declination angle of the earth
!   w0      - real -  angular frequency of one year period  (/hours)
!   w1      - real -  angular frequency of one day period  (/hours)
!   sc      - real -  solar constant  1368.0  (W/m2)
!   d       - real -  declination angle at given time  (radians)
!   snH     - real -  sine of altitude angle of the sun
!   h1      - real -  altitude angle of the sun  (radians)
!   q       - real -  solar radiation without absorption  (W/m2)
!   b1      - real -  transmission coefficient
!
      implicit none
!
!     Subroutine arguments.
!
      real  tm, rlat, qs
!
!     Local variables.
!
      real b1,d,dg2rd,decln,pi,q,sc,snh,w0,w1
!
      pi = 3.1415926536e0
      dg2rd = pi/180.0e0
      decln = 23.5e0*dg2rd
      w0 = 2.0e0*pi/(365.24e0*24.0e0)
      w1 = 2.0e0*pi/24.0e0
      sc = 1368.0e0
!
!     Calculate sine of the angle of the sun above the horizon, snh
!     d is the declination angle - June 21st is the 171st day after tm=0
!
      d = decln*cos(w0*tm - 2.950e0)
      snh = -cos(rlat*dg2rd)*cos(d)*cos(w1*tm) + sin(rlat*dg2rd)*sin(d)
      snh = min(1.0e0,snh)
      snh = max(0.0e0,snh)
!
      q = sc*snh
!     h1 = asin(snh)
!
!     Take into account the effect of atmospheric absorption
!     Defant 1961 suggests i(z)=i0*exp(-t*a/sin(h1))
!     where  t             turbidity factor (unspecified)
!            a             0.128 to 0.054 times -ln(sin(h1))
!            b1=exp(-t*a)  transmission coefficient between 0.6 and 0.7
!     Comparison with Bunker & Goldsmith averages gives b1=0.76 or a=0.274
!
      b1 = 0.76e0
      qs = q*b1
!
      return
      end

!==========================================================
   subroutine read_sp_real (un, variable, Dim, ios, recNo)
!-----------------------------------------------------------
!
! reads one dataset of single precission real binary data.
!
   implicit none
   INTEGER               un, Dim, ios, recNo
   INTEGER, PARAMETER :: sp = SELECTED_REAL_KIND(6,37)    !4-byte REAL
   INTEGER, PARAMETER :: dp = SELECTED_REAL_KIND(12,307)  !8-byte REAL
   REAL                  variable(Dim)
   REAL(sp)              buffer(Dim)

   if (recNo == 0) then
      READ (un, iostat = ios) buffer
   else
      READ (un, rec = recNo, iostat = ios) buffer
   endif

   variable = buffer

   return
   end subroutine read_sp_real

!==========================================================
   subroutine write_sp_real (un, variable, Dim, ios, recNo)
!-----------------------------------------------------------
!
! reads one dataset of single precission real binary data.
!
   implicit none
   INTEGER               un, Dim, ios, recNo
   INTEGER, PARAMETER :: sp = SELECTED_REAL_KIND(6,37)    !4-byte REAL
   INTEGER, PARAMETER :: dp = SELECTED_REAL_KIND(12,307)  !8-byte REAL
   REAL                  variable(Dim)
   REAL(sp)              buffer(Dim)

   buffer = real(variable,sp)

   if (recNo == 0) then
      write (un, iostat = ios) buffer
   else
      write (un, rec = recNo, iostat = ios) buffer
   endif

   return
   end subroutine write_sp_real

!==========================================================
