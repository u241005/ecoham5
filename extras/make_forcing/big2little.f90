program big2little
! !DESCRIPTION:
! this program converts files from big_endian to little_endian
! created by Markus Kreus 25-02-2015


   use mod_grid_parameters

   implicit none
!
! IO - UNITS:
!------------------------------
   integer, parameter :: big_unit      = 10
   integer, parameter :: little_unit   = 20
!
! !LOCAL PARAMETERS:
   integer, parameter :: input_realkind  = 8   != ibyte_per_real
   integer, parameter :: output_realkind = 4   != ibyte_per_real
!
! variables
   real(kind=input_realkind), dimension(iiwet) :: buffer
   integer :: iargc, ios, irec
   character(len=99) :: filename
!
!-----------------------------------------------------------------------

! get filename as program parameter
   if (iargc() > 0) then
      call getarg(1, filename)
   else ! default settings
      print*,' enter file to process: '
      read(*, '(a)') filename
   endif

! open input file
   write(*, '(3x,"read big_endian from file: ",a)') trim(filename)
   open(big_unit,file = trim(filename), status='old',         &
            form = 'unformatted',access = 'direct',           &
            recl = iiwet * input_realkind,                    &
            convert = 'BIG_ENDIAN', err = 9000)

! open output file
   filename = trim(filename)//'.little_endian'
   write(*, '(3x,"write little_endian to file: ",a)') trim(filename)
   open(little_unit,file = trim(filename), status='replace',  &
            form = 'unformatted',access = 'direct',           &
            recl = iiwet * output_realkind,                   &
            convert = 'LITTLE_ENDIAN', err = 9000)

! read any records from input file and pipe to outputfile
   irec = 0
   do while (.true.)
      irec = irec +1
      write (*,'(1x,a,i0)') '***  read/write recNo ', irec
      read (big_unit, rec = irec, iostat = ios) buffer
      if (ios /= 0) exit

      write (little_unit, rec = irec, iostat = ios) real(buffer, output_realkind)
   enddo

   print*,'overall records processed: ',irec

! close io-units
   close(big_unit)
   close(little_unit)

   return

9000 continue
   write( *,'(" open error : ",a)') trim(filename)

!==========================================================
   end program big2little
!==========================================================
