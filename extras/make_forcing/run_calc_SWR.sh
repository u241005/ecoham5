# @ shell=/bin/csh
#

setenv XLFRTEOPTS "buffering=disable_preconn:ufmt_littleendian=20-29"

# note:
# units 21-29 are used for input/output files, which are LITTLE_ENDIAN (HAMSOM default)
# units 30-xx are associated with BIG_ENDIAN, which has been ECOHAM standard at times of blizzard
# but this standard will move to LITTLE_ENDIAN in 2015 migrating to the new DKRZ supercomuter mistral.

set Grid        = NWCS20
set Orign       = NCEP
set InputPath   = /work/uo0121/EM/forcing/HAMSOM
set InputDirMet = ${InputPath}/meteo/${Orign}/${Grid}

set yearS       = 1946
set yearE       = 2012
#set yearE      = 1946

if ( -e ./tmp ) rm -r ./tmp
if ( -e forcing_${Orign}_${Grid}.md5sum ) rm forcing_${Orign}_${Grid}.md5sum

echo '=============================================================='
ls -l
md5sum *
echo '=============================================================='

set year = ${yearS}
while ( ${year} <=  ${yearE} )

  if ( -e ./tmp ) rm -r ./tmp
  mkdir ./tmp
  ln -sf ${InputDirMet}/Clou${year}.direct   ./tmp/Clou${year}.direct

  echo '-- calculate SWR for year '${year}' --'
  echo ' tag cloudiness file: '`md5sum ${InputDirMet}'/Clou'${year}'.direct'`
  ./calc_SWR ${year}

  if ( -e ./${year} ) rm -r ./${year}
  mkdir ./${year}
  mv ./tmp/rad${year}.direct                ./${year}/rad_${Orign}_${Grid}_${year}.direct
  mv ./tmp/radday${year}.direct             ./${year}/radday_${Orign}_${Grid}_${year}.direct

#===========================================================================================
# ATTENTION: copying wind forcing easily fits in here, but does not infer from scriptname
# !!!! important: no endian conversion will take place just copying windspeed files !!!
#--------------------------------------------------------------------------------------
  cp ${InputDirMet}/WinS${year}.direct      ./${year}/wins_${Orign}_${Grid}_${year}.direct
#===========================================================================================

  chmod 444 ./${year}/*

  foreach i (./${year}/*)
     md5sum $i
     echo `md5sum $i` >> forcing_${Orign}_${Grid}.md5sum
  end
  echo ''
  echo '=============================================================='

  @ year++

end

exit
