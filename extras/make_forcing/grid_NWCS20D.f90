!
! !MODULE: grid_NWCS20D.f90 --- parameters for grid: North West Continetal Shelf, 20km, Version "D" (31 layer)
!
! !INTERFACE:
   MODULE mod_grid_parameters
!
! !DESCRIPTION:
!
! !USES:
   implicit none
!
   ! GRID PARAMETERS
!    real,    parameter :: delta_lat     =  0.2                   ! delta lat in degrees
!    real,    parameter :: delta_lon     =  0.3333333333333       ! delta lon in degrees
!    real,    parameter :: xlat0         =  47.683333333          ! most southerly zeta point
!    real,    parameter :: xlon0         = -15.083333333          ! most westerly zeta point
   real,    parameter :: delta_lat     =  12./60.               ! delta lat in degrees
   real,    parameter :: delta_lon     =  20./60.               ! delta lon in degrees
   real,    parameter :: xlat0         =  47.0 +41./60          ! most southerly zeta point
   real,    parameter :: xlon0         = -15.0 -5./60.          ! most westerly zeta point
   integer, parameter :: imax          = 82                     ! number of latidude indexes
   integer, parameter :: jmax          = 88                     ! number of longitude indexes
   integer, parameter :: kmax          = 31                     ! number of layer indexes
   integer, parameter :: ijmax         = imax*jmax              ! total number of 2D-surface-cells
   integer, parameter :: ijkmax        = imax*jmax*kmax         ! total number of 3D-cells
   integer, parameter :: iiwet2        = 4455                   ! number of wet surface cells
   integer, parameter :: iiwet         = 83558                  ! number of wet cells
   character*99       :: gridfile      ='indh-NWCS20D.dat'      ! file defining topography

   ! set external partitioning in latitudinal direction, see set_nml and x1x_set.nml
   logical    :: external_partitioning = .false.           ! default is .false.
!=======================================================================

   end module mod_grid_parameters

!=======================================================================
