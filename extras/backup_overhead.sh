#!/bin/csh

#  Author: Markus Kreus, 2016
#          Institute of Oceanography, Hamburg

tar czf ECOHAM.$1.overhead.tgz ECOHAM.$1/input.$1 ECOHAM.$1/list.$1 ECOHAM.$1/objects.$1 ECOHAM.$1/RunJob.$1* ECOHAM.$1/s*.$1 ECOHAM.$1/Year.$1  ECOHAM.$1/wrk.$1 ECOHAM.$1/res.$1/*/*.nml ECOHAM.$1/res.$1/*/*.dat ECOHAM.$1/res.$1/*/*.md* ECOHAM.$1/res.$1/*/*.out &

exit