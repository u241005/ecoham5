!
! !MODULE: grid_cskc.f90 --- parameters for cskc-subgrid
!
! !INTERFACE:
   MODULE mod_grid_parameters
!
! !DESCRIPTION:
!
! !USES:
   implicit none
!
   ! GRID PARAMETERS
   real,    parameter :: delta_lat     =  0.1              ! delta lat in degrees
   real,    parameter :: delta_lon     =  0.1              ! delta lon in degrees
   real,    parameter :: xlat0         = -11.8958          ! most southerly zeta point
   real,    parameter :: xlon0         = 92.5042           ! most westerly zeta point
   integer, parameter :: imax          = 285               ! number of latidude indexes
   integer, parameter :: jmax          = 400               ! number of longitude indexes
   integer, parameter :: kmax          = 39                ! number of layer indexes
   integer, parameter :: ijmax         = imax*jmax         ! total number of 2D-cells
   integer, parameter :: ijkmax        = imax*jmax*kmax    ! total number of 3D-cells
   integer, parameter :: iiwet2        = 88281             ! number of wet surface cells
   integer, parameter :: iiwet         = 2227102           ! number of wet cells
   character* 99      :: gridfile      ='cskc.maxLay.bin'  ! file defining topography

!=======================================================================

   end module mod_grid_parameters

!=======================================================================
