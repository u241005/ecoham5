!
! !MODULE: eco_mpi_parallel.f90 --- subroutines for mpi parallelization of ecoham
!
! !INTERFACE:
   MODULE mod_mpi_parallel
!
! !DESCRIPTION:
! variables and subroutines for mpi parallelization
!
! !USES:
#ifdef MPI
   use mpi
   use mod_grid_parameters
#endif
   implicit none
!
!  default: all is public.
   public
!
! !LOCAL VARIABLES:
   integer, parameter :: master   = 0
   integer, save      :: myID     = 0
   integer            :: ierr_mpi = 0
   integer            :: ierr_all  ! error status (recv-buffer from MPI_ALLREDUCE)

#ifdef MPI
!#include "mpif.h"
   integer, dimension(:), allocatable, save :: domainrows
   integer,                            save :: worldsize
   integer,                            save :: MPI_COMM_ECOHAM
   integer,                            save :: iStart, iEnd

!    integer            :: wetcount
!    integer            :: recvLatNorth=1, recvLatSouth=imax
!    integer            :: iStartCalc=1, iEndCalc=imax
!
!=======================================================================

  contains

!=======================================================================
!
! !INTERFACE:
   subroutine mpi_parallel_init(ierr)
#define SUBROUTINE_NAME 'mpi_parallel_init'
!
! !DESCRIPTION:
!
! !USES:
   IMPLICIT NONE
!
! !OUTPUT PARAMETERS:
   integer, intent(inout)  :: ierr
!
! !LOCAL VARIABLES:
!
!-----------------------------------------------------------------------
#include "call-trace.inc"

   call MPI_INIT(ierr_mpi)
   ierr = ierr + ierr_mpi

   call MPI_COMM_DUP (MPI_COMM_WORLD, MPI_COMM_ECOHAM, ierr_mpi)
   ierr = ierr + ierr_mpi

   call MPI_COMM_RANK(MPI_COMM_ECOHAM, myID, ierr_mpi)
   ierr = ierr + ierr_mpi

   call MPI_COMM_SIZE(MPI_COMM_ECOHAM, worldsize, ierr_mpi)
   ierr = ierr + ierr_mpi

   allocate ( domainrows(0:worldsize-1) )

   !write (*,*) 'mpi_parallel#63 - myID:', myID,' worldsize:', worldsize

   end subroutine mpi_parallel_init
!
!=======================================================================
!
!INTERFACE:
   subroutine mpi_parallel_decomp(iwet3d, iS, iE, ierr)
#define SUBROUTINE_NAME 'mpi_parallel_decomp'
!
! !DESCRIPTION:
!
! !USES:
   IMPLICIT NONE
!
! !OUTPUT PARAMETERS:
   integer, intent(in)     :: iwet3d(jmax,kmax,imax)
   integer, intent(inout)  :: iS, iE
   integer, intent(inout)  :: ierr
!
! !LOCAL VARIABLES:
   integer            :: domainsize, rank, wet3DSum, wetCnt, linesize, i
   logical, parameter :: equalNoOfRows = .false. !.true.
!   logical, parameter :: equalNoOfRows = .true.
!
!-----------------------------------------------------------------------
#include "call-trace.inc"

   ! parallel decomposition
   if (imax < worldsize) then
      ierr = 1
      if (myID == master) then
         write (*,*) 'decomposition error: more processes than number of latitude lines'
      endif
      return
   end if

! - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   if (equalNoOfRows) then ! decomposition balancing number of rows (exclusive N/S-boundaries)
! - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
      domainsize = nint(real(iE-iS+1)/real(worldsize))
      if (iS + domainsize*worldsize > iE) then
         domainrows(0:worldsize-1:2) = domainsize - 1
         domainrows(1:worldsize-1:2) = domainsize
         domainrows(0)               = domainrows(0) +iS-1
         domainrows(worldsize-1)     = domainrows(worldsize-1) +iMax-iE
         linesize                    = iMax - sum(domainrows(0:worldsize-1))
         i = 2
         do while (linesize > 0)
            domainrows(i) = domainrows(i) + 1
            linesize = linesize - 1
            if (linesize == 0) exit
            i = i + 2
            if (i > worldsize-1) i = 1
         enddo
         if (myID == master) then
            write (*,*) '*** Rows for all ranks:'
            linesize = 1
            do rank = 0, worldsize-1
               wet3DSum = count( iwet3d(:,:,linesize:linesize+domainrows(rank)-1) /= 0 )
               write (*,*) '    rank, no of domainrows and wet cells: ', rank, domainrows(rank), wet3DSum
               linesize = linesize + domainrows(rank)
            enddo
         endif
      else
         domainrows(0            ) = iS + domainsize
         domainrows(1:worldsize-2) = domainsize
         domainrows(  worldsize-1) = iMax - sum(domainrows(0:worldsize-2))
         if (myID == master .and. worldsize > 1) then
            write(*,'(1x,5(a,i0))') '*** Rows for rank 0 : ',domainrows(0),               &
                                     ', rows for rank 1 - ',worldsize-2,': ', domainsize, &
                                     ', rows for rank ',worldsize-1,': ', domainrows(worldsize-1)
         endif
      endif
! - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   else    ! decomposition balancing number of (wet) gridpoints (exclusive boundaries)
! - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
      domainrows = 0
      rank       = 0
      wet3DSum   = 0
      wetCnt     = count( iwet3d(:,:,iS:iE)==1 )
      domainsize = nint ( real(wetCnt) / real(worldsize) )
      if (myID == master) then
         write(*,*) '*** Domain decomposition, each subdomain should contain about ',     &
                                 & domainsize,' wet cells.'
      endif
      domainrows(rank) = iS-1
      do i = iS, iE-1
         domainsize = nint ( real(wetCnt) / real(worldsize-rank) )
         linesize   = count( iwet3d(:,:,i) /= 0 )
         wet3DSum   = wet3DSum + linesize
         wetCnt     = wetCnt   - linesize
         if (wet3DSum > 1.2*domainsize .and. rank /= worldsize-1) then
!         if (wet3DSum > 1.0*domainsize .and. rank /= worldsize-1) then
            if (rank == 0) wet3DSum = wet3DSum + count( iwet3d(:,:,1:iS-1) /= 0 )
            if (myID == master) then
               write(*,*) '    rank, no + sum of domainrows, wet cells: ',           &
                          & rank, domainrows(rank), sum(domainrows(0:rank)), wet3DSum
            endif
            domainrows(rank) = domainrows(rank) + 1
            wet3DSum         = 0
            rank             = min (rank + 1, worldsize-1)
         else
            domainrows(rank) = domainrows(rank) + 1
         endif
      enddo
      wet3DSum = wet3DSum + count( iwet3d(:,:,iE:iMax) /= 0 )
      domainrows(rank) =  domainrows(rank) + iMax-iE+1
      if (myID == master) then
         write(*,*) '    rank, no + sum of domainrows, wet cells: ',                &
                    & rank, domainrows(rank), sum(domainrows(0:rank)), wet3DSum
      endif
! - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   endif ! select decomposition type
! - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

   iStart = 1 + sum(domainrows(0:myID-1))
   iEnd   = iStart + domainrows(myID) - 1
   ! Add remaining rows to last PE
   if (myID == worldsize-1) iEnd = iMax
   if (sum(domainrows) /= iMax) then
      if (myID == master) then
         write(*,*) '### Par_decomp: sum of all domain rows: ',                     &
                             & sum(domainrows(0:worldsize-1)), ' /= iMax: ', iMax
      endif
      ierr = 1
   endif

   if (myID .ne. master)      iS = iStart
   if (myID .ne. worldsize-1) iE = iEnd

! write (*,*) 'mpi_parallel_decomp - DONE',myID

   return

   end subroutine mpi_parallel_decomp
!
!=======================================================================
!
!INTERFACE:
   subroutine mpi_parallel_exchange_boundary (what, array, iS, iE, kDim,ierr)
#define SUBROUTINE_NAME 'mpi_parallel_exchange_boundary'
!
! !DESCRIPTION:
!--------------------------------
!---  what = 1: exchange of 1 row  at northern and southern boundary
!---         2: exchange of 2 rows at northern and southern boundary
!---         N: exchange of 1 row  at northern boundary
!---         S: exchange of 1 row  at southern boundary
!--------------------------------
!
! !USES:
   IMPLICIT NONE
!
! !OUTPUT PARAMETERS:
   integer,       intent(inout) :: ierr
!
! !INPUT PARAMETERS:
   character(len=1), intent(in) :: what
   integer         , intent(in) :: iS, iE, kDim
   real            , intent(in) :: array(jMax,kDim,iS:iE)
!
! !LOCAL VARIABLES:
   integer                      :: status(MPI_STATUS_SIZE)
!
!-----------------------------------------------------------------------
#include "call-trace.inc"

   if (worldsize <= 1) return

   if (what == '1') then

      if (myID /= 0) then
         call MPI_SENDRECV(array(1,1,iStart),   jMax*kDim, MPI_REAL8, myID-1, 0, &
                           array(1,1,iStart-1), jMax*kDim, MPI_REAL8, myID-1, 1, &
                           MPI_COMM_ECOHAM, status, ierr_mpi)
      endif
      if (myID /= worldsize-1) then
         call MPI_SENDRECV(array(1,1,iEnd),   jMax*kDim, MPI_REAL8, myID+1, 1,   &
                           array(1,1,iEnd+1), jMax*kDim, MPI_REAL8, myID+1, 0,   &
                           MPI_COMM_ECOHAM, status, ierr_mpi)
      endif

   else if (what == 'N') then

      if (myID.eq.0) then
         call MPI_SEND(array(1,1,iEnd), jMax*kDim, MPI_REAL8, 1, 0,              &
                           MPI_COMM_ECOHAM, ierr_mpi)
      elseif (myID.eq.worldsize-1) then
         call MPI_RECV(array(1,1,iStart-1), jMax*kDim, MPI_REAL8, myID-1, 0,     &
                           MPI_COMM_ECOHAM, status, ierr_mpi)
      else
         call MPI_SENDRECV(array(1,1,iEnd),     jMax*kDim, MPI_REAL8, myID+1, 0, &
                           array(1,1,iStart-1), jMax*kDim, MPI_REAL8, myID-1, 0, &
                           MPI_COMM_ECOHAM, status, ierr_mpi)
      endif

   else if (what == 'S') then

      if (myID.eq.0) then
         call MPI_RECV(array(1,1,iEnd+1), jMax*kDim, MPI_REAL8, 1, 0,            &
                           MPI_COMM_ECOHAM, status, ierr_mpi)
      elseif (myID.eq.worldsize-1) then
         call MPI_SEND(array(1,1,iStart), jMax*kDim, MPI_REAL8, myID-1, 0,       &
                           MPI_COMM_ECOHAM, ierr_mpi)
      else
         call MPI_SENDRECV(array(1,1,iStart), jMax*kDim, MPI_REAL8, myID-1, 0,   &
                           array(1,1,iEnd+1), jMax*kDim, MPI_REAL8, myID+1, 0,   &
                           MPI_COMM_ECOHAM, status, ierr_mpi)
      endif

   endif

   ierr = ierr + ierr_mpi

   end subroutine mpi_parallel_exchange_boundary
!
!=======================================================================
!
!INTERFACE:
   subroutine mpi_parallel_gather(array,kDim,ierr)
#define SUBROUTINE_NAME 'mpi_parallel_exchange_boundary'
!
! !DESCRIPTION:
! gather data from subdomains by root process
!
! !USES:
   IMPLICIT NONE
!
! !OUTPUT PARAMETERS:
   integer,       intent(inout) :: ierr
!
! !INPUT PARAMETERS:
   integer         , intent(in) :: kDim
   real            , intent(in) :: array(jMax,kDim,iMax)
!
! !LOCAL VARIABLES:
   integer                      :: status(MPI_STATUS_SIZE)
   integer                      :: rowoffset, r
!
!-----------------------------------------------------------------------
#include "call-trace.inc"

   rowoffset = 1

   if (myID.eq.0) then
      do r = 1, worldsize-1
         rowoffset = rowoffset+domainrows(r-1)
         call MPI_RECV(array(1,1,rowoffset), domainrows(r)*jMax*kDim, &
                       MPI_REAL8, r, 0, MPI_COMM_ECOHAM, status, ierr_mpi)
      enddo
   else
      call MPI_SEND(array(1,1,iStart), domainrows(myID)*jMax*kDim, &
                    MPI_REAL8, 0, 0, MPI_COMM_ECOHAM, ierr_mpi)
   endif

   ierr = ierr + ierr_mpi

   end subroutine mpi_parallel_gather
!
!=======================================================================
!
!INTERFACE:
   subroutine mpi_parallel_add_real(sendbuf, recvbuf, cnt, ierr)
#define SUBROUTINE_NAME 'mpi_parallel_add_real'
!
! !DESCRIPTION:
! add up values of sendbuf(1:cnt) over all processors and give result as
! recvbuf(1:cnt) to master processor
!
! !USES:
   IMPLICIT NONE
!
! !OUTPUT PARAMETERS:
   integer, intent(inout)  :: ierr
   integer, intent(in)     :: cnt
   real   , intent(in)     :: sendbuf(*)
   real   , intent(out)    :: recvbuf(*)
!
! !LOCAL VARIABLES:
!
!-----------------------------------------------------------------------
#include "call-trace.inc"

   call MPI_REDUCE(sendbuf, recvbuf, cnt, MPI_REAL8, MPI_SUM, 0, MPI_COMM_ECOHAM, ierr_mpi)

   ierr = ierr + ierr_mpi

   end subroutine mpi_parallel_add_real
!
!=======================================================================
!
!INTERFACE:
   subroutine mpi_parallel_final(ierr)
#define SUBROUTINE_NAME 'mpi_parallel_final'
!
! !DESCRIPTION:
!
! !USES:
   IMPLICIT NONE
!
! !OUTPUT PARAMETERS:
   integer, intent(inout)  :: ierr
!
! !LOCAL VARIABLES:
!
!-----------------------------------------------------------------------
#include "call-trace.inc"

   !call mpi_comm_free( MPI_COMM_ECOHAM, ierr_mpi)
   call MPI_FINALIZE(ierr_mpi)
   ierr = ierr + ierr_mpi

   end subroutine mpi_parallel_final
!
!=======================================================================
#endif
   end module  mod_mpi_parallel

!=======================================================================
