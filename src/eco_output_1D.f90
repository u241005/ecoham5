!--------------------------------
!
! !MODULE: output_1D ---  subroutines for ecoham-1D-output
!
! !INTERFACE:
   MODULE mod_output_1D
!
! !DESCRIPTION:
!
! !USES:
   use mod_common
   use mod_var
#ifdef MPI
   use mod_mpi_parallel
#endif
   use mod_output_var
   implicit none
!
!  default: all is public.
  public
!
! !LOCAL VARIABLES:
#ifdef debug_output1D
   real, dimension(:,:), allocatable :: st_old
#endif
   real, dimension(:,:), allocatable :: zeta_old
   real, dimension(kmax)             :: dz_old

!jp  integer, dimension(:), allocatable  :: icol_out, jcol_out
!jp  integer  :: n_pos_out
!
!=======================================================================

   contains

!=======================================================================
!
! !INTERFACE:
   subroutine init_output_1D(ierr)
#define SUBROUTINE_NAME 'init_output_1D'
!
! !DESCRIPTION:
!
! !USES:
   use mod_grid,  only: k_index, dz, dzz
   implicit none
!
! !OUTPUT PARAMETERS:
   integer, intent(inout)  :: ierr
!
! !LOCAL VARIABLES:
   integer       :: iu, n, k, k0
   integer       :: icol_output, jcol_output
#ifdef MPI
   integer       :: ierr_mpi
#endif
!
   namelist /pos_out_nml/icol_output,jcol_output
!
!-----------------------------------------------------------------------
#include "call-trace.inc"

   if (n_pos_out>=1) then
      if (myID==master) call write_log_message(" INITIALIZING 1D output ")

      ! allocate index-arrays for output columns
      allocate(icol_out(n_pos_out))
      icol_out = 0

      allocate(jcol_out(n_pos_out))
      jcol_out = 0

      if (myID == master) then
         ! read indices for output columns
         open(nml_unit, file=trim(settings_file), action='read', status='old')
         do n = 1, n_pos_out
            read(nml_unit, nml=pos_out_nml)
            jcol_out(n) = jcol_output
            icol_out(n) = icol_output
         enddo
         close(nml_unit)
      endif
#ifdef MPI
      ! broadcast for pos_out_nml
      call MPI_Bcast( icol_out,  n_pos_out, MPI_INT, master, MPI_COMM_ECOHAM, ierr_mpi )
      call MPI_Bcast( jcol_out,  n_pos_out, MPI_INT, master, MPI_COMM_ECOHAM, ierr_mpi )
#endif

      !multiple station output
      do n = 1, n_pos_out
         ! each process opens the prt files of its computation area
         !if ( icol_out(n) >= latStartIndex .and. icol_out(n) <= latEndIndex ) then
         if ( icol_out(n) >= iStartCalc .and. icol_out(n) <= iEndCalc ) then
            iu = output_1D_unit+n
            if (max(imax,jmax)>99) then
               write(filename,'(a,"_ij_",i3.3,"_",i3.3,".prt")') trim(runID),icol_out(n),jcol_out(n)
            else
               write(filename,'(a,"_ij_",i2.2,"_",i2.2,".prt")') trim(runID),icol_out(n),jcol_out(n)
            endif
            write(long_msg,'(3x,"[",i2.2,"] unit ",i3," associated to file ",a)') myID, iu, filename
            call write_log_message(long_msg); long_msg = ''
            open(iu, file=filename, status='replace')
            k0 = k_index(jcol_out(n),icol_out(n))
            write(iu,'(3i4," number of layers, ipos, jpos")') k0, icol_out(n), jcol_out(n)
            write(iu,'(8f10.2)') (dzz(k),k=1,k0-1), dz(jcol_out(n),k0,icol_out(n))

#ifdef debug_output1D
            if (n==1) then
               open(99,file='check_x1x.dat',status='replace')
               allocate( st_old(kmax,n_st_var) )
               st_old(:,:) = st(jcol_out(n),:,icol_out(n),:)
            endif
#endif
         endif
      enddo
#ifdef MPI
      ! wait for all procs to finish log_messages (doesn't cost much, because it's only once during init!)
      call MPI_ALLREDUCE(ierr, ierr_all, 1, MPI_INT, MPI_SUM, MPI_COMM_ECOHAM, ierr_mpi)
#endif
   endif

   if (ierr/=0) then
      write( error_message, '("[",i2.2,"] ERROR - init_output_1D: ",i3)') myID, ierr
      call stop_ecoham(ierr, msg=error_message);
   end if
   return

   end subroutine init_output_1D

!=======================================================================
! !INTERFACE:
   subroutine do_output_1D(ierr)
#define SUBROUTINE_NAME 'do_output_1D'
!
! !DESCRIPTION:
!
! !USES:
   use mod_grid,  only: k_index, dz
   use mod_flux,  only: dz_vol
   use mod_utils, only: DOYstring
   implicit none

   integer, intent(inout) :: ierr
!
! !LOCAL VARIABLES:
   character(len=19) :: str
   integer           :: iu, ida, ihour, imo
   integer           :: n
!
!-----------------------------------------------------------------------
#include "call-trace.inc"

   write(str,'(a19)') DOYstring(day_of_year)
   read (str,'(i4,x,i2,x,i2,x,i2)') iy1, imo, ida, ihour
   !print*,'output1D#136 ',iy1,imo,ida,ihour

   do n = 1, n_pos_out
      !if ( icol_out(n) >= latStartIndex .and. icol_out(n) <= latEndIndex ) then
      if ( icol_out(n) >= iStartCalc .and. icol_out(n) <= iEndCalc ) then

         dz_old    = dz(jcol_out(n), :, icol_out(n))                                     ! current local grid cell hights
         dz_old(1) = dz(jcol_out(n), 1, icol_out(n)) - dz_vol(jcol_out(n), icol_out(n))  ! surface grid cell hight at beginning of output timestep

#ifdef debug_output1D
         ! IMPORTANT: check must be placed before calling subroutine write_prt !!!!!
         if (n==1) then
            call check_flux(jcol_out(n),icol_out(n),ix1x,ierr)
            call check_flux(jcol_out(n),icol_out(n),in3n,ierr)
            st_old(:,:) = st(jcol_out(n),:,icol_out(n),:)
         endif
#endif
         iu = output_1D_unit+n
         !write(*,'("time: ",4i6,f15.5,3i4)')iy1,imo,ida,ihour,dz(jcol_out(n),1,icol_out(n)), &
         !                                      icol_out(n),jcol_out(n),k_index(jcol_out(n),icol_out(n))
         write(iu,'("time: ",4i6,2x,2f15.10)') iy1, imo, ida, ihour, dz(jcol_out(n), 1, icol_out(n)), dz_old(1)
         call write_prt( iu, jcol_out(n), icol_out(n), k_index(jcol_out(n),icol_out(n)), ierr )

      endif
   enddo

   if (ierr/=0) then
      write( error_message, '("[",i2.2,"] ERROR - do_output_1D: ",i3)') myID, ierr
      call stop_ecoham(ierr, msg=error_message);
   end if
   return

   end subroutine do_output_1D
!
!=======================================================================
! !INTERFACE:
   subroutine write_prt(FID,jcol,icol,kcol,ierr)
#define SUBROUTINE_NAME 'write_prt'
!
! !DESCRIPTION:
!
! !USES:
   use mod_var
   use mod_flux
#ifdef module_meteo
   use mod_meteo, only: meteodilution
#endif
#ifdef module_chemie
   use mod_chemie
#endif
#ifdef module_biogeo
   use mod_biogeo, only: rcnb, rcpb, rcnz1, rcpz1, rcnz2, rcpz2
#endif
#ifdef module_sediment
   use mod_sediment
#endif
   implicit none
!
! !INPUT PARAMETERS:
   integer, intent(in)    :: FID, icol, jcol, kcol
   integer, intent(inout) :: ierr
!
! !LOCAL VARIABLES:
   integer             :: k, n, n1, n2, n3
   real                :: dum(10), d_dum(n_st_var)
   character(len=30)   :: dum_nam(10)
!-----------------------------------------------------------------------
#include "call-trace.inc"

   dum = 0 ! to get rid of unused warning

   if (n_st_var>0) then
      write(FID,'("STATE VARIABLES")')
      n1 = 1
      n2 = 0
      do while (n2.lt.n_st_var)
         n2 = min(n1+7,n_st_var)
         do n = 1, n2-n1+1; dum_nam(n) = 'st_'//trim(st_name(n1+n-1));  enddo
         write(FID,'(" k",5x,8(a6,12x))') (dum_nam(n),n=1,(n2-n1+1))
         do k = 1, kcol
            write(FID,'(i3,8(e18.10))') k, (st(jcol,k,icol,n),n=n1,n2)
         enddo
         n1 = n2 +1
      enddo
   endif

#ifdef module_biogeo
   if (n_de_var>0) then
      write(FID,'("DERIVED VARIABLES")')
      n1 = 1
      n2 = 0
      do while (n2.lt.n_de_var)
         n2 = min(n1+7,n_de_var)
         do n = 1, n2-n1+1; dum_nam(n) = 'de_'//trim(de_name(n1+n-1));  enddo
         write(FID,'(" k",5x,8(a6,12x))') (dum_nam(n),n=1,(n2-n1+1))
         do k = 1, kcol
            write(FID,'(i3,8(e18.10))') k, (de(jcol,k,icol,n),n=n1,n2)
         enddo
         n1 = n2 +1
      enddo
   endif
#endif

#ifdef module_chemie
   if (n_ch_var>0) then
      write(FID,'("CHEMICAL VARIABLES")')
      n1 = 1
      n2 = 0
      do while (n2.lt.n_ch_var)
         n2 = min(n1+7,n_ch_var)
         write(FID,'(" k",5x,8(a6,12x))') (ch_name(n),n=n1,n2)
         do k = 1, kcol
            write(FID,'(i3,8(e18.10))') k, (ch(jcol,k,icol,n),n=n1,n2)
         enddo
         n1 = n2 +1
      enddo
   endif
#endif

   if (n_ot3D_var>0) then
      write(FID,'("OTHER VARIABLES (3D)")')
      n1 = 1
      n2 = 0
      do while (n2.lt.n_ot3D_var)
         n2 = min(n1+7,n_ot3D_var)
         do n = 1, n2-n1+1; dum_nam(n) = trim(ot3D_name(n1+n-1));  enddo
         write(FID,'(" k",5x,8(a10,8x))') (dum_nam(n),n=1,(n2-n1+1))
         do k = 1, kcol
            write(FID,'(i3,8(e18.10))') k, (ot3D(jcol,k,icol,n),n=n1,n2)
         enddo
         n1 = n2 +1
      enddo
   endif

   ! OTHER VARIABLES (2D) for 1D output not implemented yet
   !if (n_ot2D_var>0) then
   !endif


!---------- TIME INTEGRATED FLUXES (DAILY) --------------
   write(FID,'("PELAGIC FLUXES")')
!--------------- BGC FLUXES -------------------
!   write(FID,'("------ BGC ------")')
   n1 = 1
   n2 = 0
   do while (n2.lt.max_flux)
      n2 = min(n1+7,max_flux)
      n3 = n2 -n1 +1
      write(FID,'(" k",5x,8(a7,11x))') (flux_name(n),n=n1,n2)
      do k = 1, kcol
         write(FID,'(i3,8(e18.10))') k, (d_from_to(jcol,k,icol,n)/dz_old(k), n=n1,n2)
      enddo
      n1 = n2 +1
   enddo
#ifdef module_biogeo
   ! implicit fluxes of derived variables not covered jet
   n1=1
   dum_nam(1) = 'ban_z1n'; n2=n1
   dum_nam(2) = 'ban_z2n'; n2=n2+1
   dum_nam(3) = 'z1n_z2n'; n2=n2+1
   dum_nam(4) = 'bap_z1p'; n2=n2+1
   dum_nam(5) = 'bap_z2p'; n2=n2+1
   dum_nam(6) = 'z1p_z2p'; n2=n2+1
   write(FID,'(" k",5x,8(a7,11x))') (dum_nam(n),n=n1,n2)
   do k=1,kcol
      dum(1) = d_from_to(jcol,k,icol,i_bac_z1c) /rcnb  /dz_old(k)
      dum(2) = d_from_to(jcol,k,icol,i_bac_z2c) /rcnb  /dz_old(k)
      dum(3) = d_from_to(jcol,k,icol,i_z1c_z2c) /rcnz1 /dz_old(k)
      dum(4) = d_from_to(jcol,k,icol,i_bac_z1c) /rcpb  /dz_old(k)
      dum(5) = d_from_to(jcol,k,icol,i_bac_z2c) /rcpb  /dz_old(k)
      dum(6) = d_from_to(jcol,k,icol,i_z1c_z2c) /rcpz1 /dz_old(k)
      write(FID,'(i3,8(e18.10))') k,(dum(n),n=n1,n2)
   enddo
#endif

!-------------------- TRA --------------------------------
! write(FID,'("------ TRA ------")')
   n1=1
   n2=0
   do while (n2.lt.n_st_var)
      n2 = min(n1+7,n_st_var)
      do n = 1, n2-n1+1; dum_nam(n) = 'tra_'//trim(st_name(n1-1+n)); enddo
      write(FID,'(" k",5x,8(a7,11x))') (dum_nam(n),n=1,(n2-n1+1))
      do k = 1, kcol
         if (k==1) then        ! adapt to change in volume related to this process
            do n = n1, n2
               d_dum(n) = d_hyd(jcol,k,icol,n) -st(jcol,k,icol,n)*dz_adh(jcol,icol)
            enddo
            write(FID,'(i3,8(e18.10))') k, (d_dum(n)/dz_old(k), n=n1,n2)
         else
            write(FID,'(i3,8(e18.10))') k, (d_adh(jcol,k,icol,n)/dz_old(k), n=n1,n2)
         endif
      enddo
      n1 = n2 +1
   enddo
#ifdef module_biogeo
   ! implicit fluxes of derived variables
   n1=1
   dum_nam(1) = 'tra_ban'; n2=n1
   dum_nam(2) = 'tra_z1n'; n2=n2+1
   dum_nam(3) = 'tra_z2n'; n2=n2+1
   dum_nam(4) = 'tra_bap'; n2=n2+1
   dum_nam(5) = 'tra_z1p'; n2=n2+1
   dum_nam(6) = 'tra_z2p'; n2=n2+1
   write(FID,'(" k",5x,8(a7,11x))') (dum_nam(n),n=n1,n2)
   do k=1,kcol
      if (k==1) then
         dum(1) = d_dum(ibac)             /rcnb  /dz_old(k)
         dum(2) = d_dum(iz1c)             /rcnz1 /dz_old(k)
         dum(3) = d_dum(iz2c)             /rcnz2 /dz_old(k)
         dum(4) = d_dum(ibac)             /rcpb  /dz_old(k)
         dum(5) = d_dum(iz1c)             /rcpz1 /dz_old(k)
         dum(6) = d_dum(iz2c)             /rcpz2 /dz_old(k)
      else
         dum(1) = d_hyd(jcol,k,icol,ibac) /rcnb  /dz_old(k)
         dum(2) = d_hyd(jcol,k,icol,iz1c) /rcnz1 /dz_old(k)
         dum(3) = d_hyd(jcol,k,icol,iz2c) /rcnz2 /dz_old(k)
         dum(4) = d_hyd(jcol,k,icol,ibac) /rcpb  /dz_old(k)
         dum(5) = d_hyd(jcol,k,icol,iz1c) /rcpz1 /dz_old(k)
         dum(6) = d_hyd(jcol,k,icol,iz2c) /rcpz2 /dz_old(k)
      endif
      write(FID,'(i3,8(e18.10))') k,(dum(n),n=n1,n2)
   enddo
#endif
!-------------------- ADV --------------------------------
! write(FID,'("------ ADV ------")')
   n1 = 1
   n2 = 0
   do while (n2.lt.n_st_var)
      n2 = min(n1+7,n_st_var)
      do n = 1, n2-n1+1; dum_nam(n) = 'adv_'//trim(st_name(n1-1+n)); enddo
      write(FID,'(" k",5x,8(a7,11x))') (dum_nam(n),n=1,(n2-n1+1))
      do k = 1, kcol
         write(FID,'(i3,8(e18.10))') k, (d_adv(jcol,k,icol,n)/dz_old(k), n=n1,n2)
      enddo
      n1 = n2 +1
   enddo
#ifdef module_biogeo
   ! implicit fluxes of derived variables
   n1=1
   dum_nam(1) = 'adv_ban'; n2=n1
   dum_nam(2) = 'adv_z1n'; n2=n2+1
   dum_nam(3) = 'adv_z2n'; n2=n2+1
   dum_nam(4) = 'adv_bap'; n2=n2+1
   dum_nam(5) = 'adv_z1p'; n2=n2+1
   dum_nam(6) = 'adv_z2p'; n2=n2+1
   write(FID,'(" k",5x,8(a7,11x))') (dum_nam(n),n=n1,n2)
   do k=1,kcol
      dum(1) = d_adv(jcol,k,icol,ibac)/dz_old(k) /rcnb
      dum(2) = d_adv(jcol,k,icol,iz1c)/dz_old(k) /rcnz1
      dum(3) = d_adv(jcol,k,icol,iz2c)/dz_old(k) /rcnz2
      dum(4) = d_adv(jcol,k,icol,ibac)/dz_old(k) /rcpb
      dum(5) = d_adv(jcol,k,icol,iz1c)/dz_old(k) /rcpz1
      dum(6) = d_adv(jcol,k,icol,iz2c)/dz_old(k) /rcpz2
      write(FID,'(i3,8(e18.10))') k,(dum(n),n=n1,n2)
   enddo
#endif
!-------------------- ADH --------------------------------
! write(FID,'("------ ADH ------")')
   n1 = 1
   n2 = 0
   do while (n2.lt.n_st_var)
      n2 = min(n1+7,n_st_var)
      do n = 1, n2-n1+1; dum_nam(n) = 'adh_'//trim(st_name(n1-1+n)); enddo
      write(FID,'(" k",5x,8(a7,11x))') (dum_nam(n),n=1,(n2-n1+1))
      do k = 1, kcol
         if (k==1) then        ! adapt to change in volume related to this process
            do n = n1, n2
               d_dum(n) = d_adh(jcol,k,icol,n) -st(jcol,k,icol,n)*dz_adh(jcol,icol)
            enddo
            write(FID,'(i3,8(e18.10))') k, (d_dum(n)/dz_old(k), n=n1,n2)
         else
            write(FID,'(i3,8(e18.10))') k, (d_adh(jcol,k,icol,n)/dz_old(k), n=n1,n2)
         endif
      enddo
      n1 = n2 +1
   enddo
#ifdef module_biogeo
   ! implicit fluxes of derived variables
   n1=1
   dum_nam(1) = 'adh_ban'; n2=n1
   dum_nam(2) = 'adh_z1n'; n2=n2+1
   dum_nam(3) = 'adh_z2n'; n2=n2+1
   dum_nam(4) = 'adh_bap'; n2=n2+1
   dum_nam(5) = 'adh_z1p'; n2=n2+1
   dum_nam(6) = 'adh_z2p'; n2=n2+1
   write(FID,'(" k",5x,8(a7,11x))') (dum_nam(n),n=n1,n2)
   do k=1,kcol
      if (k==1) then
         dum(1) = d_dum(ibac)             /rcnb  /dz_old(k)
         dum(2) = d_dum(iz1c)             /rcnz1 /dz_old(k)
         dum(3) = d_dum(iz2c)             /rcnz2 /dz_old(k)
         dum(4) = d_dum(ibac)             /rcpb  /dz_old(k)
         dum(5) = d_dum(iz1c)             /rcpz1 /dz_old(k)
         dum(6) = d_dum(iz2c)             /rcpz2 /dz_old(k)
      else
         dum(1) = d_adh(jcol,k,icol,ibac) /rcnb  /dz_old(k)
         dum(2) = d_adh(jcol,k,icol,iz1c) /rcnz1 /dz_old(k)
         dum(3) = d_adh(jcol,k,icol,iz2c) /rcnz2 /dz_old(k)
         dum(4) = d_adh(jcol,k,icol,ibac) /rcpb  /dz_old(k)
         dum(5) = d_adh(jcol,k,icol,iz1c) /rcpz1 /dz_old(k)
         dum(6) = d_adh(jcol,k,icol,iz2c) /rcpz2 /dz_old(k)
      endif
      write(FID,'(i3,8(e18.10))') k,(dum(n),n=n1,n2)
   enddo
#endif
!-------------------- MXV --------------------------------
! write(FID,'("------ MXV ------")')
   n1 = 1
   n2 = 0
   do while (n2.lt.n_st_var)
      n2 = min(n1+7,n_st_var)
      do n = 1, n2-n1+1; dum_nam(n) = 'mxv_'//trim(st_name(n1-1+n)); enddo
      write(FID,'(" k",5x,8(a7,11x))') (dum_nam(n),n=1,(n2-n1+1))
      do k = 1, kcol
         write(FID,'(i3,8(e18.10))') k, (d_mxv(jcol,k,icol,n)/dz_old(k), n=n1,n2)
      enddo
      n1 = n2 +1
   enddo
#ifdef module_biogeo
   ! implicit fluxes of derived variables
   n1=1
   dum_nam(1) = 'mxv_ban'; n2=n1
   dum_nam(2) = 'mxv_z1n'; n2=n2+1
   dum_nam(3) = 'mxv_z2n'; n2=n2+1
   dum_nam(4) = 'mxv_bap'; n2=n2+1
   dum_nam(5) = 'mxv_z1p'; n2=n2+1
   dum_nam(6) = 'mxv_z2p'; n2=n2+1
   write(FID,'(" k",5x,8(a7,11x))') (dum_nam(n),n=n1,n2)
   do k=1,kcol
      dum(1) = d_mxv(jcol,k,icol,ibac)/dz_old(k) /rcnb
      dum(2) = d_mxv(jcol,k,icol,iz1c)/dz_old(k) /rcnz1
      dum(3) = d_mxv(jcol,k,icol,iz2c)/dz_old(k) /rcnz2
      dum(4) = d_mxv(jcol,k,icol,ibac)/dz_old(k) /rcpb
      dum(5) = d_mxv(jcol,k,icol,iz1c)/dz_old(k) /rcpz1
      dum(6) = d_mxv(jcol,k,icol,iz2c)/dz_old(k) /rcpz2
      write(FID,'(i3,8(e18.10))') k,(dum(n),n=n1,n2)
   enddo
#endif
!-------------------- MXH --------------------------------
! write(FID,'("------ MXH ------")')
   n1 = 1
   n2 = 0
   do while (n2.lt.n_st_var)
      n2 = min(n1+7,n_st_var)
      do n = 1, n2-n1+1; dum_nam(n) = 'mxh_'//trim(st_name(n1-1+n)); enddo
      write(FID,'(" k",5x,8(a7,11x))') (dum_nam(n),n=1,(n2-n1+1))
      do k = 1, kcol
         write(FID,'(i3,8(e18.10))') k, (d_mxh(jcol,k,icol,n)/dz_old(k), n=n1,n2)
      enddo
      n1 = n2 +1
   enddo
#ifdef module_biogeo
   ! implicit fluxes of derived variables
   n1=1
   dum_nam(1) = 'mxh_ban'; n2=n1
   dum_nam(2) = 'mxh_z1n'; n2=n2+1
   dum_nam(3) = 'mxh_z2n'; n2=n2+1
   dum_nam(4) = 'mxh_bap'; n2=n2+1
   dum_nam(5) = 'mxh_z1p'; n2=n2+1
   dum_nam(6) = 'mxh_z2p'; n2=n2+1
   write(FID,'(" k",5x,8(a7,11x))') (dum_nam(n),n=n1,n2)
   do k=1,kcol
      dum(1) = d_mxh(jcol,k,icol,ibac)/dz_old(k) /rcnb
      dum(2) = d_mxh(jcol,k,icol,iz1c)/dz_old(k) /rcnz1
      dum(3) = d_mxh(jcol,k,icol,iz2c)/dz_old(k) /rcnz2
      dum(4) = d_mxh(jcol,k,icol,ibac)/dz_old(k) /rcpb
      dum(5) = d_mxh(jcol,k,icol,iz1c)/dz_old(k) /rcpz1
      dum(6) = d_mxh(jcol,k,icol,iz2c)/dz_old(k) /rcpz2
      write(FID,'(i3,8(e18.10))') k,(dum(n),n=n1,n2)
   enddo
#endif
!
#ifdef module_rivers
!-------------------- RIV --------------------------------
! write(FID,'("------ RIV ------")')
   n1 = 1
   n2 = 0
   do while (n2.lt.n_st_var)
      n2 = min(n1+7,n_st_var)
      do n = 1, n2-n1+1; dum_nam(n) = 'riv_'//trim(st_name(n1-1+n)); enddo
      write(FID,'(" k",5x,8(a7,11x))') (dum_nam(n),n=1,(n2-n1+1))
      do k = 1, kcol
         if (k==1) then        ! adapt to change in volume related to this process
            do n = n1, n2
               d_dum(n) = d_riv(jcol,k,icol,n) -st(jcol,k,icol,n)*dz_riv(jcol,icol)
            enddo
            write(FID,'(i3,8(e18.10))') k, (d_dum(n)/dz_old(k), n=n1,n2)
         else
            write(FID,'(i3,8(e18.10))') k, (d_riv(jcol,k,icol,n)/dz_old(k), n=n1,n2)
         endif
      enddo
      n1 = n2 +1
   enddo
#ifdef module_biogeo
   ! implicit fluxes of derived variables
   n1=1
   dum_nam(1) = 'riv_ban'; n2=n1
   dum_nam(2) = 'riv_z1n'; n2=n2+1
   dum_nam(3) = 'riv_z2n'; n2=n2+1
   dum_nam(4) = 'riv_bap'; n2=n2+1
   dum_nam(5) = 'riv_z1p'; n2=n2+1
   dum_nam(6) = 'riv_z2p'; n2=n2+1
   write(FID,'(" k",5x,8(a7,11x))') (dum_nam(n),n=n1,n2)
   do k=1,kcol
      if (k==1) then
         dum(1) = d_dum(ibac)             /rcnb  /dz_old(k)
         dum(2) = d_dum(iz1c)             /rcnz1 /dz_old(k)
         dum(3) = d_dum(iz2c)             /rcnz2 /dz_old(k)
         dum(4) = d_dum(ibac)             /rcpb  /dz_old(k)
         dum(5) = d_dum(iz1c)             /rcpz1 /dz_old(k)
         dum(6) = d_dum(iz2c)             /rcpz2 /dz_old(k)
      else
         dum(1) = d_riv(jcol,k,icol,ibac) /rcnb  /dz_old(k)
         dum(2) = d_riv(jcol,k,icol,iz1c) /rcnz1 /dz_old(k)
         dum(3) = d_riv(jcol,k,icol,iz2c) /rcnz2 /dz_old(k)
         dum(4) = d_riv(jcol,k,icol,ibac) /rcpb  /dz_old(k)
         dum(5) = d_riv(jcol,k,icol,iz1c) /rcpz1 /dz_old(k)
         dum(6) = d_riv(jcol,k,icol,iz2c) /rcpz2 /dz_old(k)
      endif
      write(FID,'(i3,8(e18.10))') k,(dum(n),n=n1,n2)
   enddo
#endif
#endif
!
#ifdef module_meteo
   if (meteodilution==1) then
!-------------------- PEV --------------------------------
! write(FID,'("------ PEV ------")')
! NOTE! precipitation-evaporation is in (mmol m-3) !!!!!
      n1 = 1
      n2 = 0
      do while (n2.lt.n_st_var)
         n2 = min(n1+7,n_st_var)
         do n = 1, n2-n1+1; dum_nam(n) = 'pev_'//trim(st_name(n1-1+n)); enddo
         write(FID,'(" k",5x,8(a7,11x))') (dum_nam(n),n=1,(n2-n1+1))
         do k = 1, kcol
            write(FID,'(i3,8(e18.10))') k, (d_pev(jcol,k,icol,n), n=n1,n2)
         enddo
         n1 = n2 +1
      enddo
#ifdef module_biogeo
      ! implicit fluxes of derived variables
      n1=1
      dum_nam(1) = 'pev_ban'; n2=n1
      dum_nam(2) = 'pev_z1n'; n2=n2+1
      dum_nam(3) = 'pev_z2n'; n2=n2+1
      dum_nam(4) = 'pev_bap'; n2=n2+1
      dum_nam(5) = 'pev_z1p'; n2=n2+1
      dum_nam(6) = 'pev_z2p'; n2=n2+1
      write(FID,'(" k",5x,8(a7,11x))') (dum_nam(n),n=n1,n2)
      do k=1,kcol
         dum(1) = d_pev(jcol,k,icol,ibac) /rcnb
         dum(2) = d_pev(jcol,k,icol,iz1c) /rcnz1
         dum(3) = d_pev(jcol,k,icol,iz2c) /rcnz2
         dum(4) = d_pev(jcol,k,icol,ibac) /rcpb
         dum(5) = d_pev(jcol,k,icol,iz1c) /rcpz1
         dum(6) = d_pev(jcol,k,icol,iz2c) /rcpz2
         write(FID,'(i3,8(e18.10))') k,(dum(n),n=n1,n2)
      enddo
#endif
   endif
#endif
!
#ifdef module_restoring
!-------------------- RES --------------------------------
! write(FID,'("------ RES ------")')
   n1 = 1
   n2 = 0
   do while (n2.lt.n_st_var)
      n2 = min(n1+7,n_st_var)
      do n = 1, n2-n1+1; dum_nam(n) = 'res_'//trim(st_name(n1-1+n)); enddo
      write(FID,'(" k",5x,8(a7,11x))') (dum_nam(n),n=1,(n2-n1+1))
      do k = 1, kcol
         write(FID,'(i3,8(e18.10))') k, (d_res(jcol,k,icol,n)/dz_old(k), n=n1,n2)
      enddo
      n1 = n2 +1
   enddo
#ifdef module_biogeo
   ! implicit fluxes of derived variables
   n1=1
   dum_nam(1) = 'res_ban'; n2=n1
   dum_nam(2) = 'res_z1n'; n2=n2+1
   dum_nam(3) = 'res_z2n'; n2=n2+1
   dum_nam(4) = 'res_bap'; n2=n2+1
   dum_nam(5) = 'res_z1p'; n2=n2+1
   dum_nam(6) = 'res_z2p'; n2=n2+1
   write(FID,'(" k",5x,8(a7,11x))') (dum_nam(n),n=n1,n2)
   do k=1,kcol
      dum(1) = d_res(jcol,k,icol,ibac)/dz_old(k) /rcnb
      dum(2) = d_res(jcol,k,icol,iz1c)/dz_old(k) /rcnz1
      dum(3) = d_res(jcol,k,icol,iz2c)/dz_old(k) /rcnz2
      dum(4) = d_res(jcol,k,icol,ibac)/dz_old(k) /rcpb
      dum(5) = d_res(jcol,k,icol,iz1c)/dz_old(k) /rcpz1
      dum(6) = d_res(jcol,k,icol,iz2c)/dz_old(k) /rcpz2
      write(FID,'(i3,8(e18.10))') k,(dum(n),n=n1,n2)
   enddo
#endif
#endif
!-------------------- SEDIMENT --------------------------
#ifdef module_sediment
   if(n_sed_var>0)then
      write(FID,'("SEDIMENT VARIABLES")')
      n1 = 1
      n2 = 0
      do while (n2.lt.n_sed_var)
         n2 = min(n1+7,n_sed_var)
         write(FID,'(" k",5x,8(a6,12x))') (sd_name(n),n=n1,n2)
         do k = 1, kcol
            do n = 1, n2-n1+1
               if (k.lt.kcol) then
                  dum(n)=0.0
               else
                  dum(n) = sd(jcol,icol,n1+n-1)/dz_old(k) ! (mmol m-3)
               endif
            enddo
            write(FID,'(i3,8(e18.10))') k, (dum(n),n=1,n2-n1+1)
         enddo
         n1 = n2 +1
      enddo
   endif
!
   write(FID,'("SEDIMENT FLUXES")')
#ifdef module_biogeo
   ! fluxes from sediment to pelagial (remineralisation)
   n1 = 1
   n2 = 0
   do while (n2.lt.max_flux_sed)
      n2 = min(n1+7,max_flux_sed)
      !write(*,'(" k",5x,8(a7,11x))') (sedi_flux_nam(n),n=n1,n2)
      !write(*,'(i3,8(e18.10))') kcol,(sedi_flux(jcol,icol,n),n=n1,n2)
      write(FID,'(" k",5x,8(a7,11x))') (sedi_flux_name(n),n=n1,n2)
      do k = 1, kcol
         do n = 1, n2-n1+1
            if (k.lt.kcol) then
               dum(n) = 0.0
            else
               dum(n) = sedi_flux(jcol,icol,n1+n-1)/dz_old(k) ! (mmol m-3 d-1)
            endif
         enddo
         write(FID,'(i3,8(e18.10))') k, (dum(n),n=1,n2-n1+1)
      enddo
      n1 = n2 +1
   enddo
   ! fluxes from pelagial to sediment (sinking/settling)
   n1 = 1
   n2 = 0
   do while (n2.lt.n_st_var)
      n2 = min(n1+7,n_st_var)
      write(FID,'(" k",5x,8(a7,11x))') (sedo_flux_name(n), n=n1,n2)
      do k = 1, kcol
         do n = 1, n2-n1+1
            if (k.lt.kcol) then
               dum(n) = 0.0
            else
               dum(n) = sedo_flux(jcol,icol,n1+n-1)/dz_old(k) ! (mmol m-3 d-1)
            endif
         enddo
         write(FID,'(i3,8(e18.10))') k, (dum(n),n=1,n2-n1+1)
      enddo
      n1 = n2 +1
   enddo
   ! implicit fluxes of derived variables
   n1=1
   dum_nam(1) = 'ban_sed'; n2=n1
   dum_nam(2) = 'z1n_sed'; n2=n2+1
   dum_nam(3) = 'z2n_sed'; n2=n2+1
   dum_nam(4) = 'bap_sed'; n2=n2+1
   dum_nam(5) = 'z1p_sed'; n2=n2+1
   dum_nam(6) = 'z2p_sed'; n2=n2+1
   write(FID,'(" k",5x,8(a7,11x))') (dum_nam(n),n=n1,n2)
   do k = 1, kcol
      if (k.lt.kcol) then
         dum(:) = 0.0
      else
         dum(1) = sedo_flux(jcol,icol,ibac) /rcnb
         dum(2) = sedo_flux(jcol,icol,iz1c) /rcnz1
         dum(3) = sedo_flux(jcol,icol,iz2c) /rcnz2
         dum(4) = sedo_flux(jcol,icol,ibac) /rcpb
         dum(5) = sedo_flux(jcol,icol,iz1c) /rcpz1
         dum(6) = sedo_flux(jcol,icol,iz2c) /rcpz2
      endif
      write(FID,'(i3,8(e18.10))') k,(dum(n), n=n1,n2)
   enddo
#else
   ! fluxes from sediment to pelagial (remineralisation)
   n1 = 1
   n=1; dum_nam(n) = 'sed_dic';  dum(n) = f_sed_dic(jcol,icol)/dz_old(k)
   n=2; dum_nam(n) = 'sed_n4n';  dum(n) = f_sed_n4n(jcol,icol)/dz_old(k)
   n=3; dum_nam(n) = 'sed_n1p';  dum(n) = f_sed_n1p(jcol,icol)/dz_old(k)
   n=4; dum_nam(n) = 'sed_n5s';  dum(n) = f_sed_n5s(jcol,icol)/dz_old(k)
   n=5; dum_nam(n) = 'sed_nn2';  dum(n) = f_sed_nn2(jcol,icol)/dz_old(k)
   n=6; dum_nam(n) = 'sed_o3c';  dum(n) = f_sed_o3c(jcol,icol)/dz_old(k)
   n2 = n
   write(FID,'(" k",5x,8(a7,11x))') (dum_nam(n), n=n1,n2)
   write(FID,'(i3,8(e18.10))') kcol, (dum(n), n=n1,n2)
   do k = 1, kcol
         do n = 1, n2-n1+1
            if (k.lt.kcol) then
               dum(n) = 0.0
            else
               dum(n) = sedi_flux(jcol,icol,n1+n-1)/dz_old(k) ! (mmol m-3)
            endif
         enddo
         write(FID,'(i3,8(e18.10))') k, (dum(n), n=1,n2-n1+1)
   enddo
#endif
#endif

   if (ierr/=0) then
      write( error_message, '("[",i2.2,"] ERROR - write_prt: ",i3)') myID, ierr
      call stop_ecoham(ierr, msg=error_message);
   end if
   return

   end subroutine write_prt
!
!=======================================================================
#ifdef debug_output1D
!#include "check_flux.inc"
! !INTERFACE:
   subroutine check_flux(jcol,icol,ivar,ierr)
#define SUBROUTINE_NAME 'check_flux'
!
! !DESCRIPTION:
!
! !USES:
   use mod_grid,  only: k_index, dz
   use mod_utils, only: DOYstring
   use mod_var
   use mod_flux
   implicit none
!
! !INPUT PARAMETERS:
   integer, intent(in)    :: jcol, icol, ivar
   !real, intent(in)       :: dz_old
   integer, intent(inout) :: ierr
!
! !LOCAL VARIABLES:
   character(len=19)      :: str
   integer                :: k0, ida, ihour, imo, ifl !, iu, n,
   real, dimension(kmax)  :: st_, st_new_, st_old_, dz_ !, dz_fac !, dz_old!, add_mass
   real, dimension(kmax)  :: d_adh_, d_adv_, d_mxh_, d_mxv_
   real, dimension(kmax)  :: d_hyd_, d_riv_, d_res_!, d_dil_
   real, dimension(kmax)  :: f_adh_, f_adv_, f_mxh_, f_mxv_
   real, dimension(kmax)  :: f_hyd_, f_riv_, f_res_!, f_dil_
   real, dimension(kmax)  :: d_fromTo_, f_fromTo_
   real :: AA_
   integer :: k
!
!-----------------------------------------------------------------------
#include "call-trace.inc"

   write(str,'(a19)') DOYstring(day_of_year)
   read (str,'(i4,x,i2,x,i2,x,i2)') iy1, imo, ida, ihour

   k0 = k_index(jcol,icol)

   AA_       = area(jcol,icol)
   dz_       = dz(jcol,1:kmax,icol)
   !dz_old    = dz_
   !dz_old(1) = dz_(1) - dz_vol(jcol,icol)!/dt_out
   !dz_old(1) = dz_(1) - dVol(jcol,icol)*dt_out/area(jcol,icol)

!    dz_fac = 1.0
! !   dz_fac(1) = dz_(1) / (dz_(1) - dz_vol(jcol,icol))
!    dz_fac(1) = one / (one - dz_vol(jcol,icol)/dz_(1))

   d_hyd_ = d_hyd(jcol,:,icol,ivar)!/dz_old(1:kmax)
   d_adh_ = d_adh(jcol,:,icol,ivar)!/dz_old(1:kmax)
   d_adv_ = d_adv(jcol,:,icol,ivar)!/dz_old(1:kmax)
   d_mxh_ = d_mxh(jcol,:,icol,ivar)!/dz_old(1:kmax)
   d_mxv_ = d_mxv(jcol,:,icol,ivar)!/dz_old(1:kmax)
   d_riv_ = d_riv(jcol,:,icol,ivar)!/dz_old(1:kmax)
   d_res_ = d_res(jcol,:,icol,ivar)!/dz_old(1:kmax)

   f_hyd_ = d_hyd(jcol,:,icol,ivar)/dz_old(1:kmax)
   f_adh_ = d_adh(jcol,:,icol,ivar)/dz_old(1:kmax)
   f_adv_ = d_adv(jcol,:,icol,ivar)/dz_old(1:kmax)
   f_mxh_ = d_mxh(jcol,:,icol,ivar)/dz_old(1:kmax)
   f_mxv_ = d_mxv(jcol,:,icol,ivar)/dz_old(1:kmax)
   f_riv_ = d_riv(jcol,:,icol,ivar)/dz_old(1:kmax)
   f_res_ = d_res(jcol,:,icol,ivar)/dz_old(1:kmax)

   ! calculate total mass budget
   d_fromTo_ = 0.0
   if (ivar /= ix1x) then
      !sum-up pelagic sources and sinks
      do ifl = 1, max_flux
         if ( index(flux_name(ifl),trim(st_name(ivar))) ==5 ) then
   !print*,'pelagic source ',flux_name(ifl), d_fromTo_(1), d_from_to(jcol,1,icol,ifl)
            d_fromTo_ = d_fromTo_ + d_from_to(jcol,:,icol,ifl)
         elseif ( index(flux_name(ifl),trim(st_name(ivar))) ==1 ) then
   !print*,'pelagic sink ',flux_name(ifl), d_fromTo_(1), d_from_to(jcol,1,icol,ifl)
            d_fromTo_ = d_fromTo_ - d_from_to(jcol,:,icol,ifl)
         endif
      enddo

      ! sediment exchange
      do ifl = 1, max_flux_sed
         if ( index(sedi_flux_name(ifl),trim(st_name(ivar))) ==5 ) then
   !print*,'sediment source ',sedi_flux_name(ifl), d_fromTo_(k0), sedi_flux(jcol,icol,ifl)
            d_fromTo_(k0) = d_fromTo_(k0) + sedi_flux(jcol,icol,ifl)
         endif
      enddo

      do ifl = 1, n_st_var
         if ( index(sedo_flux_name(ifl),trim(st_name(ivar))) ==1 ) then
   !print*,'sediment sink ',sedo_flux_name(ifl), d_fromTo_(k0), sedo_flux(jcol,icol,ifl)
            d_fromTo_(k0) = d_fromTo_(k0) - sedo_flux(jcol,icol,ifl)
         endif
      enddo

   endif

   f_fromTo_ = d_fromTo_ /dz_old

   d_fromTo_ = d_fromTo_ +d_adh_ +d_adv_ +d_mxh_ +d_mxv_ +d_riv_ +d_res_


   st_     = st(jcol,1:kmax,icol,ivar)     ! (mmol m-3)
   st_new_ = st(jcol,1:kmax,icol,ivar)*dz_ ! (mmol m-2)
   st_old_ = st_old(:,ivar)*dz_old


! bingo!
   f_hyd_(1) = (d_hyd_(1) - st_(1)*dz_adh(jcol,icol)) /dz_old(1)
   f_adh_(1) = (d_adh_(1) - st_(1)*dz_adh(jcol,icol)) /dz_old(1)
   f_riv_(1) = (d_riv_(1) - st_(1)*dz_riv(jcol,icol)) /dz_old(1)

   f_adv_(1) = f_adv_(1) /dz_old(1)
   f_mxh_(1) = f_mxh_(1) /dz_old(1)
   f_mxv_(1) = f_mxv_(1) /dz_old(1)
   f_res_(1) = d_res_(1) /dz_old(1)

   f_fromTo_ = f_fromTo_ -f_adh_ -f_adv_ -f_mxh_ -f_mxv_ -f_riv_ -f_res_

   !f_hyd_(1) = st_new_(1)*(d_hyd_(1)/st_new_(1) - dz_adh(jcol,icol)/dz_(1)) /dz_old(1)
   !f_adh_(1) = st_new_(1)*(d_adh_(1)/st_new_(1) - dz_adh(jcol,icol)/dz_(1)) /dz_old(1)
   !f_riv_(1) = st_new_(1)*(d_riv_(1)/st_new_(1) - dz_riv(jcol,icol)/dz_(1)) /dz_old(1)

   !f_hyd_(1) = (st_new_(1)*(1.0 - dz_adh(jcol,icol)/dz_(1)) - (st_new_(1) - d_hyd_(1)) )  /dz_old(1)
   !f_adh_(1) = (st_new_(1)*(1.0 - dz_adh(jcol,icol)/dz_(1)) - (st_new_(1) - d_hyd_(1)) )  /dz_old(1)
   !f_riv_(1) = (st_new_(1)*(1.0 - dz_riv(jcol,icol)/dz_(1)) - (st_new_(1) - d_riv_(1)) )  /dz_old(1)

   !f_hyd_(1) = (st_(1)*(dz_(1) - dz_adh(jcol,icol)) - (st_new_(1) - d_hyd_(1)) )  /dz_old(1)
   !f_adh_(1) = (st_(1)*(dz_(1) - dz_adh(jcol,icol)) - (st_new_(1) - d_hyd_(1)) )  /dz_old(1)
   !f_riv_(1) = (st_(1)*(dz_(1) - dz_riv(jcol,icol)) - (st_new_(1) - d_riv_(1)) )  /dz_old(1)

   !f_hyd_(1) = (st_(1) - (st_new_(1) - d_hyd_(1)) /(dz_(1) - dz_adh(jcol,icol)) ) *(dz_(1) - dz_adh(jcol,icol)) /dz_old(1)
   !f_adh_(1) = (st_(1) - (st_new_(1) - d_hyd_(1)) /(dz_(1) - dz_adh(jcol,icol)) ) *(dz_(1) - dz_adh(jcol,icol)) /dz_old(1)
   !f_riv_(1) = (st_(1) - (st_new_(1) - d_riv_(1)) /(dz_(1) - dz_riv(jcol,icol)) ) *(dz_(1) - dz_riv(jcol,icol)) /dz_old(1)


! SCHON NICHT SCHLECHT
!   f_hyd_(1) = (st_(1) - (st_new_(1) - d_hyd_(1)) /(dz_(1) - dz_adh(jcol,icol)) )*dz_fac(1)
!   f_adh_(1) = (st_(1) - (st_new_(1) - d_hyd_(1)) /(dz_(1) - dz_adh(jcol,icol)) )*dz_fac(1)
!   f_riv_(1) = (st_(1) - (st_new_(1) - d_riv_(1)) /(dz_(1) - dz_riv(jcol,icol)) )*dz_fac(1)



   ! loop over all layers
   write(99,'(''--------------------------------'',a3,''-----------------------------------------'')') trim(st_name(ivar))
   !print*,dz_(1), dz_old(1)
   write(99,'("time: ",4i6,2x,2f15.10,3e20.10)') iy1, imo, ida, ihour, dz_old(1), dz_(1),dz_(1)-dz_old(1) ,& !dz(jcol, 1, icol), &
                                                 dz_vol(jcol,icol), dVol(jcol,icol)/area(jcol,icol)*dt_out/secs_per_day
   !write(99,'(a3,10a20)') 'k','st_old','st_new','diff_st','f_hyd','f_adh','f_adv','f_mxh','f_mxv','f_riv'
   !write(99,'(a3,10a20)') 'k','st_old','st_new','diff_st','f_hyd','f_adh','f_adv','f_riv','f_dil'
   write(99,'(a3,10a20)') 'k','st_old','st_new','diff_st','sum_f','f_adh','f_adv','f_riv','f_res'
   do k = 1, k0
      write(99,'(i3,'' (mol   )'',10e20.10)') k, st_old_(k)*AA_,         st_new_(k)*AA_,      &
                                (st_new_(k)-st_old_(k))*AA_,             d_fromTo_(k)*AA_,    &
                                d_adh_(k)*AA_, d_adv_(k)*AA_, d_riv_(k)*AA_, d_res_(k)*AA_
      write(99,'(i3,'' (mol/m2)'',10e20.10)') k, st_old_(k),             st_new_(k),          &
                                st_new_(k)-st_old_(k),                   d_fromTo_(k),        &
                                d_adh_(k), d_adv_(k), d_riv_(k), d_res_(k)
      write(99,'(i3,'' (mol/m3)'',10e20.10)') k, st_old_(k)/dz_old(k),   st_new_(k)/dz_(k),   &
                                st_new_(k)/dz_(k)-st_old_(k)/dz_old(k),  f_fromTo_(k),        &
                                f_adh_(k), f_adv_(k), f_riv_(k), f_res_(k)
   enddo

   return

   end subroutine check_flux
#endif
!
!=======================================================================
! !INTERFACE:
   subroutine close_output_1D(ierr)
#define SUBROUTINE_NAME 'close_output_1D'
!
! !DESCRIPTION:
!
! !USES:
   implicit none
!
! !INPUT PARAMETERS:
   integer, intent(inout) :: ierr
!
! !LOCAL VARIABLES:
   integer :: n
   logical :: l_open
!
!-----------------------------------------------------------------------
#include "call-trace.inc"

   do n = 1,n_pos_out
      ! if ( icol_out(n) >= iStartCalc .and. icol_out(n) <= iEndCalc ) then
      inquire (output_1D_unit+n, opened = l_open)
      if (l_open) close(output_1D_unit+n)
      ! endif
   enddo

   if (ierr/=0) then
      write( error_message, '("[",i2.2,"] ERROR - close_output_1D: ",i3)') myID, ierr
      call stop_ecoham(ierr, msg=error_message);
   end if

#ifdef debug_output1D
   close(99)
#endif
   return

   end subroutine close_output_1D
!
!=======================================================================

   end module mod_output_1D

!=======================================================================
