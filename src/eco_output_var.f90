!
! !MODULE: eco_output_var.f90 --- extra variables defined for ecoham output
!
! !INTERFACE:
   MODULE mod_output_var
!
! !DESCRIPTION:
!
! !USES:
   use mod_common
   use mod_grid
!   use mod_mpi_parallel
   implicit none
!
!  default: all is public.
   public
!
! !LOCAL VARIABLES:
   ! parameters for ONLY OUTPUT VARIABLES (OTHER VARIABLES):
   integer, parameter :: n_ot_max = 40  ! max possible number of ot variables
   integer            :: n_ot2D_var, n_ot3D_var
   ! container (arrays) for OTHER VARIABLES (2D):
   !real, pointer,     dimension(:,:,:)    :: ot2D
   real,   dimension(:,:,:), allocatable  :: ot2D
   logical,    dimension(:), allocatable  :: ot2D_out
   character(len=10), dimension(n_ot_max) :: ot2D_name
   character(len=99), dimension(n_ot_max) :: ot2D_long, ot2D_unit
   ! container (arrays) for OTHER VARIABLES (3D):
   !real, pointer,     dimension(:,:,:,:)  :: ot3D
   real, dimension(:,:,:,:), allocatable  :: ot3D
   logical,    dimension(:), allocatable  :: ot3D_out
   character(len=10), dimension(n_ot_max) :: ot3D_name
   character(len=99), dimension(n_ot_max) :: ot3D_long, ot3D_unit

   ! further index pointers are defined during runtime,
   ! but any parameters must be listed here ...
   integer :: izeta, idzeta, ivol, itemp, isalt, ivdc, i_tu, i_tv, i_tw
   integer :: iwinds, iradsol, iradday, ipar, ipar_mean, i_iopt, iextw
   integer :: isilt, ipco2, iph, ifpar, ichl_p1c, ichl_p2c
#ifndef exclude_p1x
   integer :: ilim_p1_n1p, ilim_p1_n3n, ilim_p1_n4n, ilim_p1_n5s
#ifdef module_biogeo_recom
   integer :: ilim_p1_IR , ilim_p1_RCN, ilim_p1_RNC, ilim_p1_RPC
   integer :: ilim_p1_RPN, ilim_p1_RNP, ilim_p1_RSN, ilim_p1_RNS
#endif
#endif
#ifndef exclude_p2x
   integer :: ilim_p2_n1p, ilim_p2_n3n, ilim_p2_n4n
#ifdef module_biogeo_recom
   integer :: ilim_p2_IR , ilim_p2_RCN, ilim_p2_RNC, ilim_p2_RPC
   integer :: ilim_p2_RPN, ilim_p2_RNP
#endif
#endif
#ifndef exclude_p3x
   integer :: ilim_p3_n1p, ilim_p3_n3n, ilim_p3_n4n, ichl_p3c, ilim_p3_dop
#ifdef module_biogeo_recom
   integer :: ilim_p3_IR , ilim_p3_RCN, ilim_p3_RNC, ilim_p3_RPC
   integer :: ilim_p3_RPN, ilim_p3_RNP
#endif
#endif
!
!=======================================================================

   contains

!=======================================================================
!
! !INTERFACE:
   subroutine set_ot_varnames(ierr)
#define SUBROUTINE_NAME 'set_ot_varnames'
!
! !DESCRIPTION:
!
! !USES:
   IMPLICIT NONE
!
! !OUTPUT PARAMETERS:
   integer, intent(inout)  :: ierr
!
! !LOCAL VARIABLES:
!
!-----------------------------------------------------------------------
#include "call-trace.inc"

   ! clear character arrays
   ot2D_name(:) = ''
   ot2D_long(:) = ''
   ot2D_unit(:) = ''
   ot3D_name(:) = ''
   ot3D_long(:) = ''
   ot3D_unit(:) = ''

   n_ot2D_var = 0
   n_ot3D_var = 0

   ! other (auxillary)  variables ("ot")
   n_ot2D_var = n_ot2D_var +1
   izeta      = n_ot2D_var
   ot2D_name(izeta)='dz'
   ot2D_long(izeta)='WATER LEVEL IN SURFACE GRID CELL'
   ot2D_unit(izeta)='m'

   n_ot2D_var = n_ot2D_var +1
   idzeta     = n_ot2D_var
   ot2D_name(idzeta)='zeta'
   ot2D_long(idzeta)='DEVIATION TO THEORETICAL SURFACE WATER LEVEL'
   ot2D_unit(idzeta)='m'

   n_ot3D_var = n_ot3D_var +1
   ivol       = n_ot3D_var
   ot3D_name( ivol)='vol'
   ot3D_long( ivol)='VOLUME OF WATER IN GRID CELL'
   ot3D_unit( ivol)='m3'

   n_ot3D_var = n_ot3D_var +1
   itemp      = n_ot3D_var
   ot3D_name(itemp)='temp'
   ot3D_long(itemp)='TEMPERATURE'
   ot3D_unit(itemp)='celsius'

   n_ot3D_var = n_ot3D_var +1
   isalt      = n_ot3D_var
   ot3D_name(isalt)='salt'
   ot3D_long(isalt)='SALINITY'
   ot3D_unit(isalt)='g kg-1'

   n_ot3D_var = n_ot3D_var +1
   ivdc       = n_ot3D_var
   ot3D_name( ivdc)='vdc'
   ot3D_long( ivdc)='VERTICAL DIFFUSION COEFFICIENT'
   ot3D_unit( ivdc)='m2 s-1'

   n_ot3D_var = n_ot3D_var +1
   i_tu       = n_ot3D_var
   ot3D_name( i_tu)='tu'
   ot3D_long( i_tu)='HORIZONTAL TRANSPORT NORTH TO SOUTH'
#ifdef debug_HAMSOM_forcing
   ot3D_unit( i_tu)='m2 s-1' 
#else
   ot3D_unit( i_tu)='m2 d-1'
#endif

   n_ot3D_var = n_ot3D_var +1
   i_tv       = n_ot3D_var
   ot3D_name( i_tv)='tv'
   ot3D_long( i_tv)='HORIZONTAL TRANSPORT WEST TO EAST'
#ifdef debug_HAMSOM_forcing
   ot3D_unit( i_tv)='m2 s-1' 
#else
   ot3D_unit( i_tv)='m2 d-1'
#endif

   n_ot3D_var = n_ot3D_var +1
   i_tw       = n_ot3D_var
   ot3D_name( i_tw)='tw'
   ot3D_long( i_tw)='VERTICAL TRANSPORT TOP to DOWN'
   ot3D_unit( i_tw)='m2 d-1'

#ifdef module_meteo
   n_ot2D_var = n_ot2D_var +1
   iwinds     = n_ot2D_var
   ot2D_name(iwinds)='winds'
   ot2D_long(iwinds)='wind speed (u10)'
   ot2D_unit(iwinds)='m s-1'

   n_ot2D_var = n_ot2D_var +1
   iradsol    = n_ot2D_var
   ot2D_name(iradsol)='radsol'
   ot2D_long(iradsol)='solar radiation'
   ot2D_unit(iradsol)='W m-2'

   n_ot2D_var = n_ot2D_var +1
   iradday    = n_ot2D_var
   ot2D_name(iradday)='radday'
   ot2D_long(iradday)='daily average solar radiation'
   ot2D_unit(iradday)='W m-2'

   n_ot3D_var = n_ot3D_var +1
   ipar       = n_ot3D_var
   ot3D_name(ipar)='PAR'
   ot3D_long(ipar)='photosynthetic active radiation'
   ot3D_unit(ipar)='W m-2'

   n_ot3D_var = n_ot3D_var +1
   ipar_mean  = n_ot3D_var
   ot3D_name(ipar_mean)='PAR_mean'
   ot3D_long(ipar_mean)='daily average PAR'
   ot3D_unit(ipar_mean)='W m-2'

   n_ot3D_var = n_ot3D_var +1
   i_iopt     = n_ot3D_var
   ot3D_name(i_iopt)='Iopt'
   ot3D_long(i_iopt)='optimal irradiance'
   ot3D_unit(i_iopt)='W m-2'

   n_ot3D_var = n_ot3D_var +1
   iextw     = n_ot3D_var
   ot3D_name(iextw)='extw'
   ot3D_long(iextw)='ligth extinction of water'
   ot3D_unit(iextw)='m-1'
#endif

#ifdef module_silt
   n_ot3D_var = n_ot3D_var +1
   isilt      = n_ot3D_var
   ot3D_name(isilt)='silt'
   ot3D_long(isilt)='SILT'
   ot3D_unit(isilt)='g m-3'
#endif

#ifdef module_chemie
   n_ot3D_var = n_ot3D_var +1
   ipco2      = n_ot3D_var
   ot3D_name(ipco2)='pCO2'
   ot3D_long(ipco2)='partial pressure of carbondioxide in water'
   ot3D_unit(ipco2)='microatm'

   n_ot3D_var = n_ot3D_var +1
   iph        = n_ot3D_var
   ot3D_name(iph)  ='pH'
   ot3D_long(iph)  ='pH (potential for hydrogen ion concentration)'
   ot3D_unit(iph)  ='-'
#endif

#ifdef module_biogeo
   n_ot3D_var = n_ot3D_var +1
   ifpar      = n_ot3D_var
   ot3D_name(ifpar)='fPAR'
   ot3D_long(ifpar)='light limitation factor'
   ot3D_unit(ifpar)='-'
   
#ifndef exclude_p1x
   ! diatoms (p1x)
   n_ot3D_var = n_ot3D_var +1
   ichl_p1c   = n_ot3D_var
   ot3D_name(ichl_p1c)='chl_p1c'
   ot3D_long(ichl_p1c)='diatoms chl to C ratio'
   ot3D_unit(ichl_p1c)='-'

   n_ot3D_var  = n_ot3D_var +1
   ilim_p1_n5s = n_ot3D_var
   ot3D_name(ilim_p1_n5s)='lim_p1_n5s'
   ot3D_long(ilim_p1_n5s)='diatoms SiO3 limitation factor'
   ot3D_unit(ilim_p1_n5s)='-'

   n_ot3D_var  = n_ot3D_var +1
   ilim_p1_n1p = n_ot3D_var
   ot3D_name(ilim_p1_n1p)='lim_p1_n1p'
   ot3D_long(ilim_p1_n1p)='diatoms PO4 limitation factor'
   ot3D_unit(ilim_p1_n1p)='-'

   n_ot3D_var  = n_ot3D_var +1
   ilim_p1_n3n = n_ot3D_var
   ot3D_name(ilim_p1_n3n)='lim_p1_n3n'
   ot3D_long(ilim_p1_n3n)='diatoms NO3 limitation factor'
   ot3D_unit(ilim_p1_n3n)='-'

   n_ot3D_var  = n_ot3D_var +1
   ilim_p1_n4n = n_ot3D_var
   ot3D_name(ilim_p1_n4n)='lim_p1_n4n'
   ot3D_long(ilim_p1_n4n)='diatoms NH4 limitation factor'
   ot3D_unit(ilim_p1_n4n)='-'

#ifdef module_biogeo_recom
   n_ot3D_var  = n_ot3D_var +1
   ilim_p1_IR  = n_ot3D_var
   ot3D_name(ilim_p1_IR )='lim_p1_IR'
   ot3D_long(ilim_p1_IR )='diatoms light limitation factor'
   ot3D_unit(ilim_p1_IR )='-'

   n_ot3D_var  = n_ot3D_var +1
   ilim_p1_RCN = n_ot3D_var
   ot3D_name(ilim_p1_RCN)='lim_p1_RCN'
   ot3D_long(ilim_p1_RCN)='diatoms regulation factor RCN'
   ot3D_unit(ilim_p1_RCN)='-'

   n_ot3D_var  = n_ot3D_var +1
   ilim_p1_RNC = n_ot3D_var
   ot3D_name(ilim_p1_RNC)='lim_p1_RNC'
   ot3D_long(ilim_p1_RNC)='diatoms regulation factor RNC'
   ot3D_unit(ilim_p1_RNC)='-'

   n_ot3D_var  = n_ot3D_var +1
   ilim_p1_RPC = n_ot3D_var
   ot3D_name(ilim_p1_RPC)='lim_p1_RPC'
   ot3D_long(ilim_p1_RPC)='diatoms regulation factor RPC'
   ot3D_unit(ilim_p1_RPC)='-'

   n_ot3D_var  = n_ot3D_var +1
   ilim_p1_RPN = n_ot3D_var
   ot3D_name(ilim_p1_RPN)='lim_p1_RPN'
   ot3D_long(ilim_p1_RPN)='diatoms regulation factor RPN'
   ot3D_unit(ilim_p1_RPN)='-'

   n_ot3D_var  = n_ot3D_var +1
   ilim_p1_RNP = n_ot3D_var
   ot3D_name(ilim_p1_RNP)='lim_p1_RNP'
   ot3D_long(ilim_p1_RNP)='diatoms regulation factor RNP'
   ot3D_unit(ilim_p1_RNP)='-'

   n_ot3D_var  = n_ot3D_var +1
   ilim_p1_RSN = n_ot3D_var
   ot3D_name(ilim_p1_RSN)='lim_p1_RSN'
   ot3D_long(ilim_p1_RSN)='diatoms regulation factor RSN'
   ot3D_unit(ilim_p1_RSN)='-'

   n_ot3D_var  = n_ot3D_var +1
   ilim_p1_RNS = n_ot3D_var
   ot3D_name(ilim_p1_RNS)='lim_p1_RNS'
   ot3D_long(ilim_p1_RNS)='diatoms regulation factor RNS'
   ot3D_unit(ilim_p1_RNS)='-'
#endif
#endif
#ifndef exclude_p2x
   ! flagellates (p2x)
   n_ot3D_var = n_ot3D_var +1
   ichl_p2c   = n_ot3D_var
   ot3D_name(ichl_p2c)='chl_p2c'
   ot3D_long(ichl_p2c)='flagellates chl to C ratio'
   ot3D_unit(ichl_p2c)='-'

   n_ot3D_var  = n_ot3D_var +1
   ilim_p2_n1p = n_ot3D_var
   ot3D_name(ilim_p2_n1p)='lim_p2_n1p'
   ot3D_long(ilim_p2_n1p)='flagellates PO4 limitation factor'
   ot3D_unit(ilim_p2_n1p)='-'

   n_ot3D_var  = n_ot3D_var +1
   ilim_p2_n3n = n_ot3D_var
   ot3D_name(ilim_p2_n3n)='lim_p2_n3n'
   ot3D_long(ilim_p2_n3n)='flagellates NO3 limitation factor'
   ot3D_unit(ilim_p2_n3n)='-'

   n_ot3D_var  = n_ot3D_var +1
   ilim_p2_n4n = n_ot3D_var
   ot3D_name(ilim_p2_n4n)='lim_p2_n4n'
   ot3D_long(ilim_p2_n4n)='flagellates NH4 limitation factor'
   ot3D_unit(ilim_p2_n4n)='-'

#ifdef module_biogeo_recom
   n_ot3D_var  = n_ot3D_var +1
   ilim_p2_IR  = n_ot3D_var
   ot3D_name(ilim_p2_IR )='lim_p2_IR'
   ot3D_long(ilim_p2_IR )='flagellates light limitation factor'
   ot3D_unit(ilim_p2_IR )='-'

   n_ot3D_var  = n_ot3D_var +1
   ilim_p2_RCN = n_ot3D_var
   ot3D_name(ilim_p2_RCN)='lim_p2_RCN'
   ot3D_long(ilim_p2_RCN)='flagellates regulation factor RCN'
   ot3D_unit(ilim_p2_RCN)='-'

   n_ot3D_var  = n_ot3D_var +1
   ilim_p2_RNC = n_ot3D_var
   ot3D_name(ilim_p2_RNC)='lim_p2_RNC'
   ot3D_long(ilim_p2_RNC)='flagellates regulation factor RNC'
   ot3D_unit(ilim_p2_RNC)='-'

   n_ot3D_var  = n_ot3D_var +1
   ilim_p2_RPC = n_ot3D_var
   ot3D_name(ilim_p2_RPC)='lim_p2_RPC'
   ot3D_long(ilim_p2_RPC)='flagellates regulation factor RPC'
   ot3D_unit(ilim_p2_RPC)='-'

   n_ot3D_var  = n_ot3D_var +1
   ilim_p2_RPN = n_ot3D_var
   ot3D_name(ilim_p2_RPN)='lim_p2_RPN'
   ot3D_long(ilim_p2_RPN)='flagellates regulation factor RPN'
   ot3D_unit(ilim_p2_RPN)='-'

   n_ot3D_var  = n_ot3D_var +1
   ilim_p2_RNP = n_ot3D_var
   ot3D_name(ilim_p2_RNP)='lim_p2_RNP'
   ot3D_long(ilim_p2_RNP)='flagellates regulation factor RNP'
   ot3D_unit(ilim_p2_RNP)='-'
#endif
#endif
#ifndef exclude_p3x
   ! p3x
   n_ot3D_var = n_ot3D_var +1
   ichl_p3c   = n_ot3D_var
   ot3D_name(ichl_p3c)='chl_p3c'
   ot3D_long(ichl_p3c)='p3x chl to C ratio'
   ot3D_unit(ichl_p3c)='-'

   n_ot3D_var  = n_ot3D_var +1
   ilim_p3_dop = n_ot3D_var
   ot3D_name(ilim_p3_dop)='lim_p3_dop'
   ot3D_long(ilim_p3_dop)='p3x DOP limitation factor'
   ot3D_unit(ilim_p3_dop)='-'

   n_ot3D_var  = n_ot3D_var +1
   ilim_p3_n1p = n_ot3D_var
   ot3D_name(ilim_p3_n1p)='lim_p3_n1p'
   ot3D_long(ilim_p3_n1p)='p3x PO4 limitation factor'
   ot3D_unit(ilim_p3_n1p)='-'

   n_ot3D_var  = n_ot3D_var +1
   ilim_p3_n3n = n_ot3D_var
   ot3D_name(ilim_p3_n3n)='lim_p3_n3n'
   ot3D_long(ilim_p3_n3n)='p3x NO3 limitation factor'
   ot3D_unit(ilim_p3_n3n)='-'

   n_ot3D_var  = n_ot3D_var +1
   ilim_p3_n4n = n_ot3D_var
   ot3D_name(ilim_p3_n4n)='lim_p3_n4n'
   ot3D_long(ilim_p3_n4n)='p3x NH4 limitation factor'
   ot3D_unit(ilim_p3_n4n)='-'

#ifdef module_biogeo_recom
   n_ot3D_var  = n_ot3D_var +1
   ilim_p3_IR  = n_ot3D_var
   ot3D_name(ilim_p3_IR )='lim_p3_IR'
   ot3D_long(ilim_p3_IR )='p3x light limitation factor'
   ot3D_unit(ilim_p3_IR )='-'

   n_ot3D_var  = n_ot3D_var +1
   ilim_p3_RCN = n_ot3D_var
   ot3D_name(ilim_p3_RCN)='lim_p3_RCN'
   ot3D_long(ilim_p3_RCN)='p3x regulation factor RCN'
   ot3D_unit(ilim_p3_RCN)='-'

   n_ot3D_var  = n_ot3D_var +1
   ilim_p3_RNC = n_ot3D_var
   ot3D_name(ilim_p3_RNC)='lim_p3_RNC'
   ot3D_long(ilim_p3_RNC)='p3x regulation factor RNC'
   ot3D_unit(ilim_p3_RNC)='-'

   n_ot3D_var  = n_ot3D_var +1
   ilim_p3_RPC = n_ot3D_var
   ot3D_name(ilim_p3_RPC)='lim_p3_RPC'
   ot3D_long(ilim_p3_RPC)='p3x regulation factor RPC'
   ot3D_unit(ilim_p3_RPC)='-'

   n_ot3D_var  = n_ot3D_var +1
   ilim_p3_RPN = n_ot3D_var
   ot3D_name(ilim_p3_RPN)='lim_p3_RPN'
   ot3D_long(ilim_p3_RPN)='p3x regulation factor RPN'
   ot3D_unit(ilim_p3_RPN)='-'

   n_ot3D_var  = n_ot3D_var +1
   ilim_p3_RNP = n_ot3D_var
   ot3D_name(ilim_p3_RNP)='lim_p3_RNP'
   ot3D_long(ilim_p3_RNP)='p3x regulation factor RNP'
   ot3D_unit(ilim_p3_RNP)='-'
#endif
#endif
#endif

   if (ierr/=0) then
      write(error_message,'("ERROR - set_ot_varnames",i3)') ierr
      call stop_ecoham(ierr, msg=error_message)
   endif
   return

   end subroutine set_ot_varnames

!=======================================================================
!
! !INTERFACE:
   subroutine update_ot_var(ierr)
#define SUBROUTINE_NAME 'update_ot_var'
!
! !DESCRIPTION:
!
! !USES:
   use mod_var
   use mod_grid,   only : dz, vol
   use mod_hydro,  only : temp, salt, vdc, zeta, tu, tv, tw
#ifdef debug_HAMSOM_forcing
   use mod_hydro,  only : tu_in, tv_in
#endif
#ifdef module_silt
   use mod_silt,   only : silt
#endif
#ifdef module_meteo
   use mod_meteo,  only : u10, radsol, radsol_mean
   use mod_meteo,  only : par, par_mean, Iopt, extw_field
#endif
#ifdef module_chemie
   use mod_chemie, only : ch, iprot
#endif
#ifdef module_biogeo
   use mod_biogeo, only : d_fPAR
#ifndef exclude_p1x
   use mod_biogeo, only : d_chl_p1c
   use mod_biogeo, only : d_lim_p1_n1p, d_lim_p1_n3n, d_lim_p1_n4n, d_lim_p1_n5s
#ifdef module_biogeo_recom
   use mod_biogeo, only : d_lim_p1_IR,  d_lim_p1_RCN, d_lim_p1_RNC, d_lim_p1_RPC
   use mod_biogeo, only : d_lim_p1_RPN, d_lim_p1_RNP, d_lim_p1_RSN, d_lim_p1_RNS
#endif
#endif
#ifndef exclude_p2x
   use mod_biogeo, only : d_chl_p2c
   use mod_biogeo, only : d_lim_p2_n1p, d_lim_p2_n3n, d_lim_p2_n4n
#ifdef module_biogeo_recom
   use mod_biogeo, only : d_lim_p2_IR,  d_lim_p2_RCN, d_lim_p2_RNC, d_lim_p2_RPC
   use mod_biogeo, only : d_lim_p2_RPN, d_lim_p2_RNP
#endif
#endif
#ifndef exclude_p3x
   use mod_biogeo, only : d_chl_p3c
   use mod_biogeo, only : d_lim_p3_n1p, d_lim_p3_n3n, d_lim_p3_n4n, d_lim_p3_dop
#ifdef module_biogeo_recom
   use mod_biogeo, only : d_lim_p3_IR,  d_lim_p3_RCN, d_lim_p3_RNC, d_lim_p3_RPC
   use mod_biogeo, only : d_lim_p3_RPN, d_lim_p3_RNP
#endif
#endif

#endif
   IMPLICIT NONE
!
! !OUTPUT PARAMETERS:
   integer, intent(inout)  :: ierr
!
! !LOCAL VARIABLES:
   real :: dt_avr ! d-1
!
!-----------------------------------------------------------------------

   dt_avr = dt_out/secs_per_day

! all these arrays are only defined for domain areas
   ot2D(:,  :,izeta)     = dz(:,1,:)
   ot2D(:,  :,idzeta)    = zeta
   ot3D(:,:,:,ivol)      = vol
   ot3D(:,:,:,itemp)     = temp
   ot3D(:,:,:,isalt)     = salt
   ot3D(:,:,:,ivdc)      = vdc
#ifdef debug_HAMSOM_forcing
   ot3D(:,:,:,i_tu)      = tu_in
   ot3D(:,:,:,i_tv)      = tv_in
   ot3D(:,:,:,i_tw)      = tw(:,1:kmax,:)/secs_per_day
#else
   ot3D(:,:,:,i_tu)      = tu
   ot3D(:,:,:,i_tv)      = tv
   ot3D(:,:,:,i_tw)      = tw(:,1:kmax,:)
#endif
#ifdef module_meteo
   ot2D(:,  :,iwinds)    = u10
   ot2D(:,  :,iradsol)   = radsol
   ot2D(:,  :,iradday)   = radsol_mean
   ot3D(:,:,:,ipar)      = par
   ot3D(:,:,:,ipar_mean) = par_mean
   ot3D(:,:,:,i_iopt)    = Iopt
   ot3D(:,:,:,iextw)     = extw_field
#endif
#ifdef module_silt
   ot3D(:,:,:,isilt)     = silt
#endif
#ifdef module_chemie
   where(ixistz(:,:,:)==1)ot3D(:,:,:,ipco2)     = pco2w(:,:,:)
   where(ixistz(:,:,:)==1)ot3D(:,:,:,iph)       = -log10( ch(:,:,:,iprot)*1.e-6 )
#endif
#ifdef module_biogeo
   ot3D(:,:,:,ifPAR)     = d_fPAR         /dt_avr
#ifndef exclude_p1x
   ot3D(:,:,:,ichl_p1c)    = d_chl_p1c    /dt_avr
   ot3D(:,:,:,ilim_p1_n1p) = d_lim_p1_n1p /dt_avr
   ot3D(:,:,:,ilim_p1_n3n) = d_lim_p1_n3n /dt_avr
   ot3D(:,:,:,ilim_p1_n4n) = d_lim_p1_n4n /dt_avr
   ot3D(:,:,:,ilim_p1_n5s) = d_lim_p1_n5s /dt_avr
#ifdef module_biogeo_recom
   ot3D(:,:,:,ilim_p1_IR ) = d_lim_p1_IR  /dt_avr
   ot3D(:,:,:,ilim_p1_RCN) = d_lim_p1_RCN /dt_avr
   ot3D(:,:,:,ilim_p1_RNC) = d_lim_p1_RNC /dt_avr
   ot3D(:,:,:,ilim_p1_RPC) = d_lim_p1_RPC /dt_avr
   ot3D(:,:,:,ilim_p1_RPN) = d_lim_p1_RPN /dt_avr
   ot3D(:,:,:,ilim_p1_RNP) = d_lim_p1_RNP /dt_avr
   ot3D(:,:,:,ilim_p1_RSN) = d_lim_p1_RSN /dt_avr
   ot3D(:,:,:,ilim_p1_RNS) = d_lim_p1_RNS /dt_avr
#endif
#endif
#ifndef exclude_p2x
   ot3D(:,:,:,ichl_p2c)    = d_chl_p2c    /dt_avr
   ot3D(:,:,:,ilim_p2_n1p) = d_lim_p2_n1p /dt_avr
   ot3D(:,:,:,ilim_p2_n3n) = d_lim_p2_n3n /dt_avr
   ot3D(:,:,:,ilim_p2_n4n) = d_lim_p2_n4n /dt_avr
#ifdef module_biogeo_recom
   ot3D(:,:,:,ilim_p2_IR ) = d_lim_p2_IR  /dt_avr
   ot3D(:,:,:,ilim_p2_RCN) = d_lim_p2_RCN /dt_avr
   ot3D(:,:,:,ilim_p2_RNC) = d_lim_p2_RNC /dt_avr
   ot3D(:,:,:,ilim_p2_RPC) = d_lim_p2_RPC /dt_avr
   ot3D(:,:,:,ilim_p2_RPN) = d_lim_p2_RPN /dt_avr
   ot3D(:,:,:,ilim_p2_RNP) = d_lim_p2_RNP /dt_avr
#endif
#endif
#ifndef exclude_p3x
   ot3D(:,:,:,ichl_p3c)    = d_chl_p3c    /dt_avr
   ot3D(:,:,:,ilim_p3_dop) = d_lim_p3_dop /dt_avr
   ot3D(:,:,:,ilim_p3_n1p) = d_lim_p3_n1p /dt_avr
   ot3D(:,:,:,ilim_p3_n3n) = d_lim_p3_n3n /dt_avr
   ot3D(:,:,:,ilim_p3_n4n) = d_lim_p3_n4n /dt_avr
#ifdef module_biogeo_recom
   ot3D(:,:,:,ilim_p3_IR ) = d_lim_p3_IR  /dt_avr
   ot3D(:,:,:,ilim_p3_RCN) = d_lim_p3_RCN /dt_avr
   ot3D(:,:,:,ilim_p3_RNC) = d_lim_p3_RNC /dt_avr
   ot3D(:,:,:,ilim_p3_RPC) = d_lim_p3_RPC /dt_avr
   ot3D(:,:,:,ilim_p3_RPN) = d_lim_p3_RPN /dt_avr
   ot3D(:,:,:,ilim_p3_RNP) = d_lim_p3_RNP /dt_avr
#endif
#endif
#endif

   if (ierr/=0) then
      write(error_message,'("ERROR - update_ot_var",i3)') ierr
      call stop_ecoham(ierr, msg=error_message)
   endif
   return

   end subroutine update_ot_var

!=======================================================================

   end module mod_output_var

!=======================================================================
