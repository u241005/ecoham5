!
! !MODULE: eco_flux.f90 --- major fluxes (arrays) for ecoham
!
! !INTERFACE:
   MODULE mod_flux
!
! !DESCRIPTION:
!
! !USES:
   use mod_common
!   use mod_mpi_parallel
   use mod_grid
   use mod_var,    only : n_st_var, st_name, st_unit, st_long
   implicit none

   save
!  default: all is public.
   public
!
! !LOCAL VARIABLES:
   integer            :: max_flux_sed  = 0
   integer            :: max_flux_out
#ifdef module_biogeo
!---- fluxes concerning pelagic domain ---
#ifndef TBNT_x1x_test
   integer, parameter :: max_flux      = 149
#else
   integer, parameter :: max_flux      = 151
#endif
!-- INDEX-pointers to pelagic fluxes -
   integer ::   i_dic_p1c, i_p1c_z1c, i_p1c_z2c
   integer ::   i_p1c_d1c, i_p1c_d2c, i_p1c_doc
   integer ::   i_p1c_soc, i_n3n_p1n, i_n4n_p1n
   integer ::   i_p1n_z1n, i_p1n_z2n, i_p1n_d1n
   integer ::   i_p1n_d2n, i_p1n_don, i_n1p_p1p
   integer ::   i_p1p_z1p, i_p1p_z2p, i_p1p_d1p
   integer ::   i_p1p_d2p, i_p1p_dop, i_n5s_p1s
   integer ::   i_p1s_d2s, i_dic_p2c, i_p2c_z1c
   integer ::   i_p2c_z2c, i_p2c_d1c, i_p2c_d2c
   integer ::   i_p2c_doc, i_p2c_soc, i_n3n_p2n
   integer ::   i_n4n_p2n, i_p2n_z1n, i_p2n_z2n
   integer ::   i_p2n_d1n, i_p2n_d2n, i_p2n_don
   integer ::   i_n1p_p2p, i_p2p_z1p, i_p2p_z2p
   integer ::   i_p2p_d1p, i_p2p_d2p, i_p2p_dop
   integer ::   i_dic_p3c, i_p3c_z1c, i_p3c_z2c
   integer ::   i_p3c_d1c, i_p3c_d2c, i_p3c_doc
   integer ::   i_p3c_soc, i_n3n_p3n, i_n4n_p3n
   integer ::   i_p3n_z1n, i_p3n_z2n, i_p3n_d1n
   integer ::   i_p3n_d2n, i_p3n_don, i_n1p_p3p
   integer ::   i_p3p_z1p, i_p3p_z2p, i_p3p_d1p
   integer ::   i_p3p_d2p, i_p3p_dop, i_bac_z1c
   integer ::   i_d1c_z1c, i_z1c_d1c, i_z1c_d2c
   integer ::   i_z1c_doc, i_z1c_dic, i_d1n_z1n
   integer ::   i_z1n_d1n, i_z1n_d2n, i_z1n_don
   integer ::   i_z1n_n4n, i_d1p_z1p, i_z1p_d1p
   integer ::   i_z1p_d2p, i_z1p_dop, i_z1p_n1p
   integer ::   i_bac_z2c, i_d1c_z2c, i_z2c_d1c
   integer ::   i_z2c_d2c, i_z2c_doc, i_z2c_dic
   integer ::   i_d1n_z2n, i_z2n_d1n, i_z2n_d2n
   integer ::   i_z2n_don, i_z2n_n4n, i_d1p_z2p
   integer ::   i_z2p_d1p, i_z2p_d2p, i_z2p_dop
   integer ::   i_z2p_n1p, i_doc_bac, i_bac_dic
   integer ::   i_don_ban, i_n4n_ban, i_ban_n4n
   integer ::   i_dop_bap, i_n1p_bap, i_bap_n1p
   integer ::   i_d1c_doc, i_d2c_doc, i_soc_doc
   integer ::   i_d1n_don, i_d2n_don, i_d1p_dop
   integer ::   i_d2p_dop, i_n4n_n3n, i_n3n_nn2
   integer ::   i_d2s_n5s, i_dic_psk, i_psk_dic
   integer ::   i_psk_d2k, i_d2k_dic, i_p1c_o2o
   integer ::   i_p2c_o2o, i_p3c_o2o, i_o2o_z1c
   integer ::   i_o2o_z2c, i_o2o_bac, i_o2o_n4n
   integer ::   i_dic_p3k, i_p3k_z1c, i_p3k_z2c
   integer ::   i_p3k_d2k, i_dop_p3p, i_z1c_z2c
   integer ::   i_o2o_brm, i_n3n_brm
!-- INDEX-pointers to air-sea fluxes -
   integer ::   i_air_o2c, i_air_o2o, i_atm_n3n, i_atm_n4n
!-- INDEX-pointers to fluxes for derived variables -
   integer ::   i_ban_z1n, i_ban_z2n, i_bap_z1p
   integer ::   i_bap_z2p, i_z1n_z2n, i_z1p_z2p
!-- INDEX-pointers to benthic fluxes -
   integer ::   i_sed_dic, i_sed_o3c, i_sed_n3n, i_sed_n4n
   integer ::   i_sed_n1p, i_sed_n5s, i_sed_o2o, i_sed_nn2, i_sed_alk
!-- INDEX-pointers to chlorophyll fluxes -
   integer ::   i_p1a_p1a, i_p2a_p2a, i_p3a_p3a
!-- INDEX-pointers for DOM -> DIM remineralization shortcut without bacteria
   integer ::   i_doc_dic, i_don_n4n, i_dop_n1p
   integer ::   i_soc_d1c, i_soc_d2c
#ifdef TBNT_x1x_test
!-- INDEX-pointers to x1x test fluxes -
   integer ::   i_x1x_x2x, i_x2x_x1x
#endif
#else
#  ifdef module_chemie
   integer, parameter :: max_flux = 1
!-- INDEX-pointers to air-sea fluxes -
   integer            :: i_air_o2c
#  else
   integer, parameter :: max_flux = 0
#  endif
#endif
!##############################################################################
!  NOTE: any fluxes listed below are defined as MATTER_per_AREA (e.g. mol m-2)
!##############################################################################
   real, dimension(:,:,:,:), allocatable :: f_from_to, d_from_to
!---- fluxes for hydrodynamics ---
   real, dimension(:,:),     allocatable :: zeta_old       ! water level at previous output timestep
   real, dimension(:,:),     allocatable :: dz_vol         ! change in water level due to net transport of water into surface layer
   real, dimension(:,:),     allocatable :: dz_adh         ! change in water level due to advective transport of water into surface layer
   real, dimension(:,:),     allocatable :: dz_riv         ! change in water level due to freshwater into surface layer
   real, dimension(:,:,:,:), allocatable :: f_adv, d_adv   ! advection vertical
   real, dimension(:,:,:,:), allocatable :: f_adh, d_adh   ! advection horizontal
   real, dimension(:,:,:,:), allocatable :: f_mxv, d_mxv   ! diffusion vertical
   real, dimension(:,:,:,:), allocatable :: f_mxh, d_mxh   ! diffusion horizontal
   real, dimension(:,:,:,:), allocatable :: f_hyd, d_hyd   ! hydrodynamic advection (f_adv+f_adh)
   real, dimension(:,:,:,:), allocatable :: f_riv, d_riv   ! river discharge
   real, dimension(:,:,:,:), allocatable :: f_dil, d_dil   ! river dilution
   real, dimension(:,:,:,:), allocatable :: f_pev, d_pev   ! precipitation evaporation
   real, dimension(:,:,:,:), allocatable :: f_res, d_res   ! restoring
#ifdef TBNT_output
   real, dimension(:,:,:,:), allocatable :: d_tu, d_tv, d_tw  ! transport accross grid interfaces
   real, dimension(:,:,:,:), allocatable :: d_mu, d_mv, d_mw  ! mixing accross grid interfaces
#endif
!---- fluxes concerning sediment interface ---
   real, dimension(:,:,:),   allocatable :: seto_flux, sedo_flux
   real, dimension(:,:,:),   allocatable :: seti_flux, sedi_flux
!---- variables for output control and metadata ---
   logical,           dimension(:), allocatable :: flux_out3D, flux_out2D
   character(len=7),  dimension(:), allocatable :: flux_name
   character(len=99), dimension(:), allocatable :: flux_long, flux_unit
   logical,           dimension(n_st_var)       :: sedo_flux_out2D
   character(len=7),  dimension(n_st_var)       :: sedo_flux_name
   character(len=99), dimension(n_st_var)       :: sedo_flux_long, sedo_flux_unit
   logical,           dimension(:), allocatable :: sedi_flux_out2D
   character(len=7),  dimension(:), allocatable :: sedi_flux_name
   character(len=99), dimension(:), allocatable :: sedi_flux_long, sedi_flux_unit

!=======================================================================

   contains

!=======================================================================
!
! !INTERFACE:
   subroutine init_fluxes(ierr)
#define SUBROUTINE_NAME 'init_fluxes'
!
! !DESCRIPTION:
!
! !USES:
   IMPLICIT NONE
!
! !OUTPUT PARAMETERS:
   integer, intent(inout)  :: ierr
!
! !LOCAL VARIABLES:
!
!-----------------------------------------------------------------------
#include "call-trace.inc"

   ! allocate arrays
!---- fluxes concerning pelagic domain ---
   allocate( f_from_to(jmax,kmax,iStartD:iEndD,max_flux) )
   allocate( d_from_to(jmax,kmax,iStartD:iEndD,max_flux) )
!---- fluxes for hydrodynamics ---
   allocate(zeta_old(jmax,   iStartD:iEndD) )
   allocate(dz_vol(jmax,     iStartD:iEndD) )
   allocate(dz_adh(jmax,     iStartD:iEndD) )
   allocate(dz_riv(jmax,     iStartD:iEndD) )
   allocate( f_adv(jmax,kmax,iStartD:iEndD,n_st_var) )
   allocate( d_adv(jmax,kmax,iStartD:iEndD,n_st_var) )
   allocate( f_adh(jmax,kmax,iStartD:iEndD,n_st_var) )
   allocate( d_adh(jmax,kmax,iStartD:iEndD,n_st_var) )
   allocate( f_hyd(jmax,kmax,iStartD:iEndD,n_st_var) )
   allocate( d_hyd(jmax,kmax,iStartD:iEndD,n_st_var) )
   allocate( f_mxv(jmax,kmax,iStartD:iEndD,n_st_var) )
   allocate( d_mxv(jmax,kmax,iStartD:iEndD,n_st_var) )
   allocate( f_mxh(jmax,kmax,iStartD:iEndD,n_st_var) )
   allocate( d_mxh(jmax,kmax,iStartD:iEndD,n_st_var) )
   allocate( f_riv(jmax,kmax,iStartD:iEndD,n_st_var) )
   allocate( d_riv(jmax,kmax,iStartD:iEndD,n_st_var) )
   allocate( f_dil(jmax,kmax,iStartD:iEndD,n_st_var) )
   allocate( d_dil(jmax,kmax,iStartD:iEndD,n_st_var) )
   allocate( f_pev(jmax,kmax,iStartD:iEndD,n_st_var) )
   allocate( d_pev(jmax,kmax,iStartD:iEndD,n_st_var) )
   allocate( f_res(jmax,kmax,iStartD:iEndD,n_st_var) )
   allocate( d_res(jmax,kmax,iStartD:iEndD,n_st_var) )
#ifdef TBNT_output
   allocate( d_tu(jmax,kmax,iStartD:iEndD,n_st_var) )
   allocate( d_tv(jmax,kmax,iStartD:iEndD,n_st_var) )
   allocate( d_tw(jmax,kmax,iStartD:iEndD,n_st_var) )
   allocate( d_mu(jmax,kmax,iStartD:iEndD,n_st_var) )
   allocate( d_mv(jmax,kmax,iStartD:iEndD,n_st_var) )
   allocate( d_mw(jmax,kmax,iStartD:iEndD,n_st_var) )
#endif
!---- fluxes concerning sediment interface ---
   allocate( seto_flux(jmax,iStartD:iEndD,n_st_var) )
   allocate( sedo_flux(jmax,iStartD:iEndD,n_st_var) )
!---- variables for output control --- (dimension is max_flux + all hydrodynamic fluxes)
   max_flux_out = max_flux + 9 * n_st_var
#ifdef TBNT_output
   max_flux_out = max_flux_out +  6 * n_st_var
#endif
   allocate( flux_out2D(max_flux_out) )
   allocate( flux_out3D(max_flux_out) )
   allocate( flux_name (max_flux_out) )
   allocate( flux_long (max_flux_out) )
   allocate( flux_unit (max_flux_out) )

   ! set fluxnames
   call set_fluxnames(ierr)

   ! initialise 3D-output logics
   flux_out2D = .false.
   flux_out3D = .false.

   ! initialize
   call empty_fluxd(ierr)
   call empty_flux(ierr)

   if (ierr/=0) then
      write( error_message, '("[",i2.2,"] ERROR - init_fluxes: ",i3)') myID,ierr
      call stop_ecoham(ierr, msg=error_message)
   end if
   return

   end subroutine init_fluxes

!=======================================================================
!
! !INTERFACE:
   subroutine set_fluxnames(ierr)
#define SUBROUTINE_NAME 'set_fluxnames'
!
! !DESCRIPTION:
!
! !USES:
   implicit none
!
! !OUTPUT PARAMETERS:
   integer, intent(inout)  :: ierr
!
! !LOCAL VARIABLES:
   integer            :: ind, m, n
!
!-----------------------------------------------------------------------
#include "call-trace.inc"

   m=1; n=1

   ! clear character arrays
   flux_name(:) = ''
   flux_long(:) = ''
   flux_unit(:) = ''
   sedo_flux_name(:) = ''
   sedo_flux_long(:) = ''
   sedo_flux_unit(:) = ''

#ifdef module_biogeo
   m=  1; i_dic_p1c = m; flux_name(m)='dic_p1c'; flux_unit(m)='mmolC m-2 d-1'  ; flux_long(m)=trim('f_'//flux_name(m))
   m=  2; i_p1c_z1c = m; flux_name(m)='p1c_z1c'; flux_unit(m)='mmolC m-2 d-1'  ; flux_long(m)=trim('f_'//flux_name(m))
   m=  3; i_p1c_z2c = m; flux_name(m)='p1c_z2c'; flux_unit(m)='mmolC m-2 d-1'  ; flux_long(m)=trim('f_'//flux_name(m))
   m=  4; i_p1c_d1c = m; flux_name(m)='p1c_d1c'; flux_unit(m)='mmolC m-2 d-1'  ; flux_long(m)=trim('f_'//flux_name(m))
   m=  5; i_p1c_d2c = m; flux_name(m)='p1c_d2c'; flux_unit(m)='mmolC m-2 d-1'  ; flux_long(m)=trim('f_'//flux_name(m))
   m=  6; i_p1c_doc = m; flux_name(m)='p1c_doc'; flux_unit(m)='mmolC m-2 d-1'  ; flux_long(m)=trim('f_'//flux_name(m))
   m=  7; i_p1c_soc = m; flux_name(m)='p1c_soc'; flux_unit(m)='mmolC m-2 d-1'  ; flux_long(m)=trim('f_'//flux_name(m))
   m=  8; i_n3n_p1n = m; flux_name(m)='n3n_p1n'; flux_unit(m)='mmolN m-2 d-1'  ; flux_long(m)=trim('f_'//flux_name(m))
   m=  9; i_n4n_p1n = m; flux_name(m)='n4n_p1n'; flux_unit(m)='mmolN m-2 d-1'  ; flux_long(m)=trim('f_'//flux_name(m))
   m= 10; i_p1n_z1n = m; flux_name(m)='p1n_z1n'; flux_unit(m)='mmolN m-2 d-1'  ; flux_long(m)=trim('f_'//flux_name(m))
   m= 11; i_p1n_z2n = m; flux_name(m)='p1n_z2n'; flux_unit(m)='mmolN m-2 d-1'  ; flux_long(m)=trim('f_'//flux_name(m))
   m= 12; i_p1n_d1n = m; flux_name(m)='p1n_d1n'; flux_unit(m)='mmolN m-2 d-1'  ; flux_long(m)=trim('f_'//flux_name(m))
   m= 13; i_p1n_d2n = m; flux_name(m)='p1n_d2n'; flux_unit(m)='mmolN m-2 d-1'  ; flux_long(m)=trim('f_'//flux_name(m))
   m= 14; i_p1n_don = m; flux_name(m)='p1n_don'; flux_unit(m)='mmolN m-2 d-1'  ; flux_long(m)=trim('f_'//flux_name(m))
   m= 15; i_n1p_p1p = m; flux_name(m)='n1p_p1p'; flux_unit(m)='mmolP m-2 d-1'  ; flux_long(m)=trim('f_'//flux_name(m))
   m= 16; i_p1p_z1p = m; flux_name(m)='p1p_z1p'; flux_unit(m)='mmolP m-2 d-1'  ; flux_long(m)=trim('f_'//flux_name(m))
   m= 17; i_p1p_z2p = m; flux_name(m)='p1p_z2p'; flux_unit(m)='mmolP m-2 d-1'  ; flux_long(m)=trim('f_'//flux_name(m))
   m= 18; i_p1p_d1p = m; flux_name(m)='p1p_d1p'; flux_unit(m)='mmolP m-2 d-1'  ; flux_long(m)=trim('f_'//flux_name(m))
   m= 19; i_p1p_d2p = m; flux_name(m)='p1p_d2p'; flux_unit(m)='mmolP m-2 d-1'  ; flux_long(m)=trim('f_'//flux_name(m))
   m= 20; i_p1p_dop = m; flux_name(m)='p1p_dop'; flux_unit(m)='mmolP m-2 d-1'  ; flux_long(m)=trim('f_'//flux_name(m))
   m= 21; i_n5s_p1s = m; flux_name(m)='n5s_p1s'; flux_unit(m)='mmolSi m-2 d-1' ; flux_long(m)=trim('f_'//flux_name(m))
   m= 22; i_p1s_d2s = m; flux_name(m)='p1s_d2s'; flux_unit(m)='mmolSi m-2 d-1' ; flux_long(m)=trim('f_'//flux_name(m))
   m= 23; i_dic_p2c = m; flux_name(m)='dic_p2c'; flux_unit(m)='mmolC m-2 d-1'  ; flux_long(m)=trim('f_'//flux_name(m))
   m= 24; i_p2c_z1c = m; flux_name(m)='p2c_z1c'; flux_unit(m)='mmolC m-2 d-1'  ; flux_long(m)=trim('f_'//flux_name(m))
   m= 25; i_p2c_z2c = m; flux_name(m)='p2c_z2c'; flux_unit(m)='mmolC m-2 d-1'  ; flux_long(m)=trim('f_'//flux_name(m))
   m= 26; i_p2c_d1c = m; flux_name(m)='p2c_d1c'; flux_unit(m)='mmolC m-2 d-1'  ; flux_long(m)=trim('f_'//flux_name(m))
   m= 27; i_p2c_d2c = m; flux_name(m)='p2c_d2c'; flux_unit(m)='mmolC m-2 d-1'  ; flux_long(m)=trim('f_'//flux_name(m))
   m= 28; i_p2c_doc = m; flux_name(m)='p2c_doc'; flux_unit(m)='mmolC m-2 d-1'  ; flux_long(m)=trim('f_'//flux_name(m))
   m= 29; i_p2c_soc = m; flux_name(m)='p2c_soc'; flux_unit(m)='mmolC m-2 d-1'  ; flux_long(m)=trim('f_'//flux_name(m))
   m= 30; i_n3n_p2n = m; flux_name(m)='n3n_p2n'; flux_unit(m)='mmolN m-2 d-1'  ; flux_long(m)=trim('f_'//flux_name(m))
   m= 31; i_n4n_p2n = m; flux_name(m)='n4n_p2n'; flux_unit(m)='mmolN m-2 d-1'  ; flux_long(m)=trim('f_'//flux_name(m))
   m= 32; i_p2n_z1n = m; flux_name(m)='p2n_z1n'; flux_unit(m)='mmolN m-2 d-1'  ; flux_long(m)=trim('f_'//flux_name(m))
   m= 33; i_p2n_z2n = m; flux_name(m)='p2n_z2n'; flux_unit(m)='mmolN m-2 d-1'  ; flux_long(m)=trim('f_'//flux_name(m))
   m= 34; i_p2n_d1n = m; flux_name(m)='p2n_d1n'; flux_unit(m)='mmolN m-2 d-1'  ; flux_long(m)=trim('f_'//flux_name(m))
   m= 35; i_p2n_d2n = m; flux_name(m)='p2n_d2n'; flux_unit(m)='mmolN m-2 d-1'  ; flux_long(m)=trim('f_'//flux_name(m))
   m= 36; i_p2n_don = m; flux_name(m)='p2n_don'; flux_unit(m)='mmolN m-2 d-1'  ; flux_long(m)=trim('f_'//flux_name(m))
   m= 37; i_n1p_p2p = m; flux_name(m)='n1p_p2p'; flux_unit(m)='mmolP m-2 d-1'  ; flux_long(m)=trim('f_'//flux_name(m))
   m= 38; i_p2p_z1p = m; flux_name(m)='p2p_z1p'; flux_unit(m)='mmolP m-2 d-1'  ; flux_long(m)=trim('f_'//flux_name(m))
   m= 39; i_p2p_z2p = m; flux_name(m)='p2p_z2p'; flux_unit(m)='mmolP m-2 d-1'  ; flux_long(m)=trim('f_'//flux_name(m))
   m= 40; i_p2p_d1p = m; flux_name(m)='p2p_d1p'; flux_unit(m)='mmolP m-2 d-1'  ; flux_long(m)=trim('f_'//flux_name(m))
   m= 41; i_p2p_d2p = m; flux_name(m)='p2p_d2p'; flux_unit(m)='mmolP m-2 d-1'  ; flux_long(m)=trim('f_'//flux_name(m))
   m= 42; i_p2p_dop = m; flux_name(m)='p2p_dop'; flux_unit(m)='mmolP m-2 d-1'  ; flux_long(m)=trim('f_'//flux_name(m))
   m= 43; i_dic_p3c = m; flux_name(m)='dic_p3c'; flux_unit(m)='mmolC m-2 d-1'  ; flux_long(m)=trim('f_'//flux_name(m))
   m= 44; i_p3c_z1c = m; flux_name(m)='p3c_z1c'; flux_unit(m)='mmolC m-2 d-1'  ; flux_long(m)=trim('f_'//flux_name(m))
   m= 45; i_p3c_z2c = m; flux_name(m)='p3c_z2c'; flux_unit(m)='mmolC m-2 d-1'  ; flux_long(m)=trim('f_'//flux_name(m))
   m= 46; i_p3c_d1c = m; flux_name(m)='p3c_d1c'; flux_unit(m)='mmolC m-2 d-1'  ; flux_long(m)=trim('f_'//flux_name(m))
   m= 47; i_p3c_d2c = m; flux_name(m)='p3c_d2c'; flux_unit(m)='mmolC m-2 d-1'  ; flux_long(m)=trim('f_'//flux_name(m))
   m= 48; i_p3c_doc = m; flux_name(m)='p3c_doc'; flux_unit(m)='mmolC m-2 d-1'  ; flux_long(m)=trim('f_'//flux_name(m))
   m= 49; i_p3c_soc = m; flux_name(m)='p3c_soc'; flux_unit(m)='mmolC m-2 d-1'  ; flux_long(m)=trim('f_'//flux_name(m))
   m= 50; i_n3n_p3n = m; flux_name(m)='n3n_p3n'; flux_unit(m)='mmolN m-2 d-1'  ; flux_long(m)=trim('f_'//flux_name(m))
   m= 51; i_n4n_p3n = m; flux_name(m)='n4n_p3n'; flux_unit(m)='mmolN m-2 d-1'  ; flux_long(m)=trim('f_'//flux_name(m))
   m= 52; i_p3n_z1n = m; flux_name(m)='p3n_z1n'; flux_unit(m)='mmolN m-2 d-1'  ; flux_long(m)=trim('f_'//flux_name(m))
   m= 53; i_p3n_z2n = m; flux_name(m)='p3n_z2n'; flux_unit(m)='mmolN m-2 d-1'  ; flux_long(m)=trim('f_'//flux_name(m))
   m= 54; i_p3n_d1n = m; flux_name(m)='p3n_d1n'; flux_unit(m)='mmolN m-2 d-1'  ; flux_long(m)=trim('f_'//flux_name(m))
   m= 55; i_p3n_d2n = m; flux_name(m)='p3n_d2n'; flux_unit(m)='mmolN m-2 d-1'  ; flux_long(m)=trim('f_'//flux_name(m))
   m= 56; i_p3n_don = m; flux_name(m)='p3n_don'; flux_unit(m)='mmolN m-2 d-1'  ; flux_long(m)=trim('f_'//flux_name(m))
   m= 57; i_n1p_p3p = m; flux_name(m)='n1p_p3p'; flux_unit(m)='mmolP m-2 d-1'  ; flux_long(m)=trim('f_'//flux_name(m))
   m= 58; i_p3p_z1p = m; flux_name(m)='p3p_z1p'; flux_unit(m)='mmolP m-2 d-1'  ; flux_long(m)=trim('f_'//flux_name(m))
   m= 59; i_p3p_z2p = m; flux_name(m)='p3p_z2p'; flux_unit(m)='mmolP m-2 d-1'  ; flux_long(m)=trim('f_'//flux_name(m))
   m= 60; i_p3p_d1p = m; flux_name(m)='p3p_d1p'; flux_unit(m)='mmolP m-2 d-1'  ; flux_long(m)=trim('f_'//flux_name(m))
   m= 61; i_p3p_d2p = m; flux_name(m)='p3p_d2p'; flux_unit(m)='mmolP m-2 d-1'  ; flux_long(m)=trim('f_'//flux_name(m))
   m= 62; i_p3p_dop = m; flux_name(m)='p3p_dop'; flux_unit(m)='mmolP m-2 d-1'  ; flux_long(m)=trim('f_'//flux_name(m))
   m= 63; i_bac_z1c = m; flux_name(m)='bac_z1c'; flux_unit(m)='mmolC m-2 d-1'  ; flux_long(m)=trim('f_'//flux_name(m))
   m= 64; i_d1c_z1c = m; flux_name(m)='d1c_z1c'; flux_unit(m)='mmolC m-2 d-1'  ; flux_long(m)=trim('f_'//flux_name(m))
   m= 65; i_z1c_d1c = m; flux_name(m)='z1c_d1c'; flux_unit(m)='mmolC m-2 d-1'  ; flux_long(m)=trim('f_'//flux_name(m))
   m= 66; i_z1c_d2c = m; flux_name(m)='z1c_d2c'; flux_unit(m)='mmolC m-2 d-1'  ; flux_long(m)=trim('f_'//flux_name(m))
   m= 67; i_z1c_doc = m; flux_name(m)='z1c_doc'; flux_unit(m)='mmolC m-2 d-1'  ; flux_long(m)=trim('f_'//flux_name(m))
   m= 68; i_z1c_dic = m; flux_name(m)='z1c_dic'; flux_unit(m)='mmolC m-2 d-1'  ; flux_long(m)=trim('f_'//flux_name(m))
   m= 69; i_d1n_z1n = m; flux_name(m)='d1n_z1n'; flux_unit(m)='mmolN m-2 d-1'  ; flux_long(m)=trim('f_'//flux_name(m))
   m= 70; i_z1n_d1n = m; flux_name(m)='z1n_d1n'; flux_unit(m)='mmolN m-2 d-1'  ; flux_long(m)=trim('f_'//flux_name(m))
   m= 71; i_z1n_d2n = m; flux_name(m)='z1n_d2n'; flux_unit(m)='mmolN m-2 d-1'  ; flux_long(m)=trim('f_'//flux_name(m))
   m= 72; i_z1n_don = m; flux_name(m)='z1n_don'; flux_unit(m)='mmolN m-2 d-1'  ; flux_long(m)=trim('f_'//flux_name(m))
   m= 73; i_z1n_n4n = m; flux_name(m)='z1n_n4n'; flux_unit(m)='mmolN m-2 d-1'  ; flux_long(m)=trim('f_'//flux_name(m))
   m= 74; i_d1p_z1p = m; flux_name(m)='d1p_z1p'; flux_unit(m)='mmolP m-2 d-1'  ; flux_long(m)=trim('f_'//flux_name(m))
   m= 75; i_z1p_d1p = m; flux_name(m)='z1p_d1p'; flux_unit(m)='mmolP m-2 d-1'  ; flux_long(m)=trim('f_'//flux_name(m))
   m= 76; i_z1p_d2p = m; flux_name(m)='z1p_d2p'; flux_unit(m)='mmolP m-2 d-1'  ; flux_long(m)=trim('f_'//flux_name(m))
   m= 77; i_z1p_dop = m; flux_name(m)='z1p_dop'; flux_unit(m)='mmolP m-2 d-1'  ; flux_long(m)=trim('f_'//flux_name(m))
   m= 78; i_z1p_n1p = m; flux_name(m)='z1p_n1p'; flux_unit(m)='mmolP m-2 d-1'  ; flux_long(m)=trim('f_'//flux_name(m))
   m= 79; i_bac_z2c = m; flux_name(m)='bac_z2c'; flux_unit(m)='mmolC m-2 d-1'  ; flux_long(m)=trim('f_'//flux_name(m))
   m= 80; i_d1c_z2c = m; flux_name(m)='d1c_z2c'; flux_unit(m)='mmolC m-2 d-1'  ; flux_long(m)=trim('f_'//flux_name(m))
   m= 81; i_z2c_d1c = m; flux_name(m)='z2c_d1c'; flux_unit(m)='mmolC m-2 d-1'  ; flux_long(m)=trim('f_'//flux_name(m))
   m= 82; i_z2c_d2c = m; flux_name(m)='z2c_d2c'; flux_unit(m)='mmolC m-2 d-1'  ; flux_long(m)=trim('f_'//flux_name(m))
   m= 83; i_z2c_doc = m; flux_name(m)='z2c_doc'; flux_unit(m)='mmolC m-2 d-1'  ; flux_long(m)=trim('f_'//flux_name(m))
   m= 84; i_z2c_dic = m; flux_name(m)='z2c_dic'; flux_unit(m)='mmolC m-2 d-1'  ; flux_long(m)=trim('f_'//flux_name(m))
   m= 85; i_d1n_z2n = m; flux_name(m)='d1n_z2n'; flux_unit(m)='mmolN m-2 d-1'  ; flux_long(m)=trim('f_'//flux_name(m))
   m= 86; i_z2n_d1n = m; flux_name(m)='z2n_d1n'; flux_unit(m)='mmolN m-2 d-1'  ; flux_long(m)=trim('f_'//flux_name(m))
   m= 87; i_z2n_d2n = m; flux_name(m)='z2n_d2n'; flux_unit(m)='mmolN m-2 d-1'  ; flux_long(m)=trim('f_'//flux_name(m))
   m= 88; i_z2n_don = m; flux_name(m)='z2n_don'; flux_unit(m)='mmolN m-2 d-1'  ; flux_long(m)=trim('f_'//flux_name(m))
   m= 89; i_z2n_n4n = m; flux_name(m)='z2n_n4n'; flux_unit(m)='mmolN m-2 d-1'  ; flux_long(m)=trim('f_'//flux_name(m))
   m= 90; i_d1p_z2p = m; flux_name(m)='d1p_z2p'; flux_unit(m)='mmolP m-2 d-1'  ; flux_long(m)=trim('f_'//flux_name(m))
   m= 91; i_z2p_d1p = m; flux_name(m)='z2p_d1p'; flux_unit(m)='mmolP m-2 d-1'  ; flux_long(m)=trim('f_'//flux_name(m))
   m= 92; i_z2p_d2p = m; flux_name(m)='z2p_d2p'; flux_unit(m)='mmolP m-2 d-1'  ; flux_long(m)=trim('f_'//flux_name(m))
   m= 93; i_z2p_dop = m; flux_name(m)='z2p_dop'; flux_unit(m)='mmolP m-2 d-1'  ; flux_long(m)=trim('f_'//flux_name(m))
   m= 94; i_z2p_n1p = m; flux_name(m)='z2p_n1p'; flux_unit(m)='mmolP m-2 d-1'  ; flux_long(m)=trim('f_'//flux_name(m))
   m= 95; i_doc_bac = m; flux_name(m)='doc_bac'; flux_unit(m)='mmolC m-2 d-1'  ; flux_long(m)=trim('f_'//flux_name(m))
   m= 96; i_bac_dic = m; flux_name(m)='bac_dic'; flux_unit(m)='mmolC m-2 d-1'  ; flux_long(m)=trim('f_'//flux_name(m))
   m= 97; i_don_ban = m; flux_name(m)='don_ban'; flux_unit(m)='mmolN m-2 d-1'  ; flux_long(m)=trim('f_'//flux_name(m))
   m= 98; i_n4n_ban = m; flux_name(m)='n4n_ban'; flux_unit(m)='mmolN m-2 d-1'  ; flux_long(m)=trim('f_'//flux_name(m))
   m= 99; i_ban_n4n = m; flux_name(m)='ban_n4n'; flux_unit(m)='mmolN m-2 d-1'  ; flux_long(m)=trim('f_'//flux_name(m))
   m=100; i_dop_bap = m; flux_name(m)='dop_bap'; flux_unit(m)='mmolP m-2 d-1'  ; flux_long(m)=trim('f_'//flux_name(m))
   m=101; i_n1p_bap = m; flux_name(m)='n1p_bap'; flux_unit(m)='mmolP m-2 d-1'  ; flux_long(m)=trim('f_'//flux_name(m))
   m=102; i_bap_n1p = m; flux_name(m)='bap_n1p'; flux_unit(m)='mmolP m-2 d-1'  ; flux_long(m)=trim('f_'//flux_name(m))
   m=103; i_d1c_doc = m; flux_name(m)='d1c_doc'; flux_unit(m)='mmolC m-2 d-1'  ; flux_long(m)=trim('f_'//flux_name(m))
   m=104; i_d2c_doc = m; flux_name(m)='d2c_doc'; flux_unit(m)='mmolC m-2 d-1'  ; flux_long(m)=trim('f_'//flux_name(m))
   m=105; i_soc_doc = m; flux_name(m)='soc_doc'; flux_unit(m)='mmolC m-2 d-1'  ; flux_long(m)=trim('f_'//flux_name(m))
   m=106; i_d1n_don = m; flux_name(m)='d1n_don'; flux_unit(m)='mmolN m-2 d-1'  ; flux_long(m)=trim('f_'//flux_name(m))
   m=107; i_d2n_don = m; flux_name(m)='d2n_don'; flux_unit(m)='mmolN m-2 d-1'  ; flux_long(m)=trim('f_'//flux_name(m))
   m=108; i_d1p_dop = m; flux_name(m)='d1p_dop'; flux_unit(m)='mmolP m-2 d-1'  ; flux_long(m)=trim('f_'//flux_name(m))
   m=109; i_d2p_dop = m; flux_name(m)='d2p_dop'; flux_unit(m)='mmolP m-2 d-1'  ; flux_long(m)=trim('f_'//flux_name(m))
   m=110; i_n4n_n3n = m; flux_name(m)='n4n_n3n'; flux_unit(m)='mmolN m-2 d-1'  ; flux_long(m)=trim('f_'//flux_name(m))
   m=111; i_n3n_nn2 = m; flux_name(m)='n3n_nn2'; flux_unit(m)='mmolN m-2 d-1'  ; flux_long(m)=trim('f_'//flux_name(m))
   m=112; i_d2s_n5s = m; flux_name(m)='d2s_n5s'; flux_unit(m)='mmolSi m-2 d-1' ; flux_long(m)=trim('f_'//flux_name(m))
   m=113; i_dic_psk = m; flux_name(m)='dic_psk'; flux_unit(m)='mmolC m-2 d-1'  ; flux_long(m)=trim('f_'//flux_name(m))
   m=114; i_psk_dic = m; flux_name(m)='psk_dic'; flux_unit(m)='mmolC m-2 d-1'  ; flux_long(m)=trim('f_'//flux_name(m))
   m=115; i_psk_d2k = m; flux_name(m)='psk_d2k'; flux_unit(m)='mmolC m-2 d-1'  ; flux_long(m)=trim('f_'//flux_name(m))
   m=116; i_d2k_dic = m; flux_name(m)='d2k_dic'; flux_unit(m)='mmolC m-2 d-1'  ; flux_long(m)=trim('f_'//flux_name(m))
   m=117; i_p1c_o2o = m; flux_name(m)='p1c_o2o'; flux_unit(m)='mmol m-2 d-1'   ; flux_long(m)=trim('f_'//flux_name(m))
   m=118; i_p2c_o2o = m; flux_name(m)='p2c_o2o'; flux_unit(m)='mmol m-2 d-1'   ; flux_long(m)=trim('f_'//flux_name(m))
   m=119; i_p3c_o2o = m; flux_name(m)='p3c_o2o'; flux_unit(m)='mmol m-2 d-1'   ; flux_long(m)=trim('f_'//flux_name(m))
   m=120; i_o2o_z1c = m; flux_name(m)='o2o_z1c'; flux_unit(m)='mmol m-2 d-1'   ; flux_long(m)=trim('f_'//flux_name(m))
   m=121; i_o2o_z2c = m; flux_name(m)='o2o_z2c'; flux_unit(m)='mmol m-2 d-1'   ; flux_long(m)=trim('f_'//flux_name(m))
   m=122; i_o2o_bac = m; flux_name(m)='o2o_bac'; flux_unit(m)='mmol m-2 d-1'   ; flux_long(m)=trim('f_'//flux_name(m))
   m=123; i_o2o_n4n = m; flux_name(m)='o2o_n4n'; flux_unit(m)='mmol m-2 d-1'   ; flux_long(m)=trim('f_'//flux_name(m))
   m=124; i_dic_p3k = m; flux_name(m)='dic_p3k'; flux_unit(m)='mmolC m-2 d-1'  ; flux_long(m)=trim('f_'//flux_name(m))
   m=125; i_p3k_z1c = m; flux_name(m)='p3k_z1c'; flux_unit(m)='mmolC m-2 d-1'  ; flux_long(m)=trim('f_'//flux_name(m))
   m=126; i_p3k_z2c = m; flux_name(m)='p3k_z2c'; flux_unit(m)='mmolC m-2 d-1'  ; flux_long(m)=trim('f_'//flux_name(m))
   m=127; i_p3k_d2k = m; flux_name(m)='p3k_d2k'; flux_unit(m)='mmolC m-2 d-1'  ; flux_long(m)=trim('f_'//flux_name(m))
   m=128; i_dop_p3p = m; flux_name(m)='dop_p3p'; flux_unit(m)='mmolP m-2 d-1'  ; flux_long(m)=trim('f_'//flux_name(m))
   m=129; i_z1c_z2c = m; flux_name(m)='z1c_z2c'; flux_unit(m)='mmolC m-2 d-1'  ; flux_long(m)=trim('f_'//flux_name(m))
!---------- fluxes, concerning air-sea exchange ---------
   m=130; i_air_o2c = m; flux_name(m)='air_o2c'; flux_unit(m)='mmolC m-2 d-1'  ; flux_long(m)=trim('f_'//flux_name(m))
   m=131; i_air_o2o = m; flux_name(m)='air_o2o'; flux_unit(m)='mmol m-2 d-1'   ; flux_long(m)=trim('f_'//flux_name(m))
   m=132; i_atm_n3n = m; flux_name(m)='atm_n3n'; flux_unit(m)='mmolN m-2 d-1'  ; flux_long(m)=trim('f_'//flux_name(m))
   m=133; i_atm_n4n = m; flux_name(m)='atm_n4n'; flux_unit(m)='mmolN m-2 d-1'  ; flux_long(m)=trim('f_'//flux_name(m))
!---------- fluxes, concerning derived variables only ---------
   m=134; i_ban_z1n = m; flux_name(m)='ban_z1n'; flux_unit(m)='mmolN m-2 d-1'  ; flux_long(m)=trim('f_'//flux_name(m))
   m=135; i_ban_z2n = m; flux_name(m)='ban_z2n'; flux_unit(m)='mmolN m-2 d-1'  ; flux_long(m)=trim('f_'//flux_name(m))
   m=136; i_bap_z1p = m; flux_name(m)='bap_z1p'; flux_unit(m)='mmolP m-2 d-1'  ; flux_long(m)=trim('f_'//flux_name(m))
   m=137; i_bap_z2p = m; flux_name(m)='bap_z2p'; flux_unit(m)='mmolP m-2 d-1'  ; flux_long(m)=trim('f_'//flux_name(m))
   m=138; i_z1n_z2n = m; flux_name(m)='z1n_z2n'; flux_unit(m)='mmolN m-2 d-1'  ; flux_long(m)=trim('f_'//flux_name(m))
   m=139; i_z1p_z2p = m; flux_name(m)='z1p_z2p'; flux_unit(m)='mmolP m-2 d-1'  ; flux_long(m)=trim('f_'//flux_name(m))
!---------- fluxes from pelagic to sediment not related to sinking ---
   m=140; i_o2o_brm = m; flux_name(m)='o2o_brm'; flux_unit(m)='mmol m-2 d-1'   ; flux_long(m)=trim('f_'//flux_name(m))
   m=141; i_n3n_brm = m; flux_name(m)='n3n_brm'; flux_unit(m)='mmolN m-2 d-1'  ; flux_long(m)=trim('f_'//flux_name(m))
!---------- fluxes for prognostic chlorophyll dynamics ---
   m=142; i_p1a_p1a = m; flux_name(m)='p1a_p1a'; flux_unit(m)='mg Chl m-2 d-1' ; flux_long(m)=trim('f_'//flux_name(m))
   m=143; i_p2a_p2a = m; flux_name(m)='p2a_p2a'; flux_unit(m)='mg Chl m-2 d-1' ; flux_long(m)=trim('f_'//flux_name(m))
   m=144; i_p3a_p3a = m; flux_name(m)='p3a_p3a'; flux_unit(m)='mg Chl m-2 d-1' ; flux_long(m)=trim('f_'//flux_name(m))
!---------- fluxes for DOM -> DIM remineralization shortcut without bacteria ---
   m=145; i_doc_dic = m; flux_name(m)='doc_dic'; flux_unit(m)='mmolC m-2 d-1' ; flux_long(m)=trim('f_'//flux_name(m))
   m=146; i_don_n4n = m; flux_name(m)='don_n4n'; flux_unit(m)='mmolN m-2 d-1' ; flux_long(m)=trim('f_'//flux_name(m))
   m=147; i_dop_n1p = m; flux_name(m)='dop_n1p'; flux_unit(m)='mmolP m-2 d-1' ; flux_long(m)=trim('f_'//flux_name(m))
   m=148; i_soc_d1c = m; flux_name(m)='soc_d1c'; flux_unit(m)='mmolC m-2 d-1' ; flux_long(m)=trim('f_'//flux_name(m))
   m=149; i_soc_d2c = m; flux_name(m)='soc_d2c'; flux_unit(m)='mmolC m-2 d-1' ; flux_long(m)=trim('f_'//flux_name(m))
#ifdef TBNT_x1x_test
!---------- dummy fluxes for bio-x1x ---
   m=150; i_x1x_x2x = m; flux_name(m)='x1x_x2x'; flux_unit(m)='mmol m-2 d-1'   ; flux_long(m)=trim('f_'//flux_name(m))
   m=151; i_x2x_x1x = m; flux_name(m)='x2x_x1x'; flux_unit(m)='mmol m-2 d-1'   ; flux_long(m)=trim('f_'//flux_name(m))
#endif

#else /*ifdef module_biogeo*/

#ifdef module_chemie
   m=1; i_air_o2c = m; flux_name(m)='air_o2c'; flux_unit(m)='mmolC m-2 d-1'
#endif
#endif /*ifdef module_biogeo*/

!--- define names and units for hydrodynamic fluxes ---
   m = max_flux
   do n = 1, n_st_var
      flux_name(m+n)='adv_'//trim(st_name(n));
      flux_long(m+n)='VERTICAL ADVECTION OF '//trim(st_long(n));
      ind = index(st_unit(n),'m-3')
      flux_unit(m+n)=trim(st_unit(n)(1:ind))//'m-2 d-1'
   enddo
   m = max_flux + n_st_var
   do n = 1, n_st_var
      flux_name(m+n)='adh_'//trim(st_name(n));
      flux_long(m+n)='HORIZONTAL ADVECTION OF '//trim(st_long(n));
      ind = index(st_unit(n),'m-3')
      flux_unit(m+n)=trim(st_unit(n)(1:ind))//'m-2 d-1'
   enddo
   m = max_flux + n_st_var*2
   do n = 1, n_st_var
      flux_name(m+n)='mxv_'//trim(st_name(n));
      flux_long(m+n)='VERTICAL MIXING OF '//trim(st_long(n));
      ind = index(st_unit(n),'m-3')
      flux_unit(m+n)=trim(st_unit(n)(1:ind))//'m-2 d-1'
   enddo
   m = max_flux + n_st_var*3
   do n = 1, n_st_var
      flux_name(m+n)='mxh_'//trim(st_name(n));
      flux_long(m+n)='HORIZONTAL MIXING OF '//trim(st_long(n));
      ind = index(st_unit(n),'m-3')
      flux_unit(m+n)=trim(st_unit(n)(1:ind))//'m-2 d-1'
   enddo
   m = max_flux + n_st_var*4
   do n = 1, n_st_var
      flux_name(m+n)='hyd_'//trim(st_name(n));
      flux_long(m+n)='NET. ADVECTIVE AND DIFFUSIVE FLUXES OF '//trim(st_long(n));
      ind = index(st_unit(n),'m-3')
      flux_unit(m+n)=trim(st_unit(n)(1:ind))//'m-2 d-1'
   enddo
   m = max_flux + n_st_var*5
   do n = 1, n_st_var
      flux_name(m+n)='riv_'//trim(st_name(n));
      flux_long(m+n)='RIVER DISCHARGE OF '//trim(st_long(n));
      ind = index(st_unit(n),'m-3')
      flux_unit(m+n)=trim(st_unit(n)(1:ind))//'m-2 d-1'
   enddo
   m = max_flux + n_st_var*6
   do n = 1, n_st_var
      flux_name(m+n)='dil_'//trim(st_name(n));
      flux_long(m+n)='RIVER DILUTION EFFECT ON '//trim(st_long(n));
      ind = index(st_unit(n),'m-3')
      flux_unit(m+n)=trim(st_unit(n)(1:ind))//'m-2 d-1'
   enddo
   m = max_flux + n_st_var*7
   do n = 1, n_st_var
      flux_name(m+n)='pev_'//trim(st_name(n));
      flux_long(m+n)='NET. PRECIPITATION EFFECT ON '//trim(st_long(n));
      ind = index(st_unit(n),'m-3')
      flux_unit(m+n)=trim(st_unit(n)(1:ind))//'m-2 d-1'
   enddo
   m = max_flux + n_st_var*8
   do n = 1, n_st_var
      flux_name(m+n)='res_'//trim(st_name(n));
      flux_long(m+n)='RESTORING FOR '//trim(st_long(n));
      ind = index(st_unit(n),'m-3')
      flux_unit(m+n)=trim(st_unit(n)(1:ind))//'m-2 d-1'
   enddo
#ifdef TBNT_output
   m = max_flux + n_st_var*9
   do n = 1, n_st_var
      flux_name(m+n)='tu_'//trim(st_name(n));
      flux_long(m+n)='ZONAL TRANSPORT OF '//trim(st_long(n))//' ACROSS EASTERN GRID INTERFACE';
      ind = index(st_unit(n),'m-3')
      flux_unit(m+n)=trim(st_unit(n)(1:ind))//'m-2 d-1'
   enddo

   m = max_flux + n_st_var*10
   do n = 1, n_st_var
      flux_name(m+n)='tv_'//trim(st_name(n));
      flux_long(m+n)='MERIDIONAL TRANSPORT OF '//trim(st_long(n))//' ACROSS SOUTHERN GRID INTERFACE';
      ind = index(st_unit(n),'m-3')
      flux_unit(m+n)=trim(st_unit(n)(1:ind))//'m-2 d-1'
   enddo

   m = max_flux + n_st_var*11
   do n = 1, n_st_var
      flux_name(m+n)='tw_'//trim(st_name(n));
      flux_long(m+n)='VERTICAL TRANSPORT OF '//trim(st_long(n))//' ACROSS LOWER GRID INTERFACE';
      ind = index(st_unit(n),'m-3')
      flux_unit(m+n)=trim(st_unit(n)(1:ind))//'m-2 d-1'
   enddo

   m = max_flux + n_st_var*12
   do n = 1, n_st_var
      flux_name(m+n)='mu_'//trim(st_name(n));
      flux_long(m+n)='ZONAL MIXING OF '//trim(st_long(n))//' ACROSS EASTERN GRID INTERFACE';
      ind = index(st_unit(n),'m-3')
      flux_unit(m+n)=trim(st_unit(n)(1:ind))//'m-2 d-1'
   enddo

   m = max_flux + n_st_var*13
   do n = 1, n_st_var
      flux_name(m+n)='mv_'//trim(st_name(n));
      flux_long(m+n)='MERIDIONAL MIXING OF '//trim(st_long(n))//' ACROSS SOUTHERN GRID INTERFACE';
      ind = index(st_unit(n),'m-3')
      flux_unit(m+n)=trim(st_unit(n)(1:ind))//'m-2 d-1'
   enddo

   m = max_flux + n_st_var*14
   do n = 1, n_st_var
      flux_name(m+n)='mw_'//trim(st_name(n));
      flux_long(m+n)='VERTICAL MIXING OF '//trim(st_long(n))//' ACROSS LOWER GRID INTERFACE';
      ind = index(st_unit(n),'m-3')
      flux_unit(m+n)=trim(st_unit(n)(1:ind))//'m-2 d-1'
   enddo
#endif

#ifdef module_sediment
!---------- fluxes, concerning sediment ---------
   ! fluxes from pelagial to sediment (sinking/settling)
   do n = 1, n_st_var
      sedo_flux_name(n) = trim(st_name(n))//'_sed'
      sedo_flux_long(n) = 'SEDIMENTAION OF '//trim(st_long(n))
      ind = index(st_unit(n),'m-3')
      sedo_flux_unit(n) = trim(st_unit(n)(1:ind))//'m-2 d-1'
   enddo
#endif

   if (ierr/=0) then
      write( error_message, '("[",i2.2,"] ERROR - set_fluxnames: ",i3)') myID,ierr
      call stop_ecoham(ierr, msg=error_message)
   end if
   return

   end subroutine set_fluxnames

!=======================================================================
!
! !INTERFACE:
   subroutine set_output_fluxes(ierr)
#define SUBROUTINE_NAME 'set_output_fluxes'
!
! !DESCRIPTION:
!
! !USES:
   implicit none
!
! !INPUT PARAMETERS:
   integer, intent(inout)  :: ierr
!
! !LOCAL VARIABLES:
   integer            :: m,n
!
!-----------------------------------------------------------------------
#include "call-trace.inc"

   m=1; n=1

   ! initialize with no fluxes choosen for netcdf output
   !flux_ncdf = .false.

   if (ierr/=0) then
      write( error_message, '("[",i2.2,"] ERROR - set_output_fluxes: ",i3)') myID,ierr
      call stop_ecoham(ierr, msg=error_message)
   end if
   return

   end subroutine set_output_fluxes

!=======================================================================
!
! !INTERFACE:
   subroutine cum_flux(dt_, ierr)
#define SUBROUTINE_NAME 'cum_flux'
!
! !DESCRIPTION:
! cumulate fluxes to daily fluxes
!
! !USES:
   implicit none
!
! !INPUT PARAMETERS:
   real,    intent(in)     :: dt_
   integer, intent(inout)  :: ierr
!
! !LOCAL VARIABLES:
   integer       :: i, j, k, k0, n
!
!-----------------------------------------------------------------------
#include "call-trace.inc"

   k0 = 0 ! to get rid of uninitialized warning

   select case (isw_dim) !jp
      case (3) !3D-mode
         do n = 1, n_st_var
            where(ixistz==1) d_riv(:,:,:,n) = d_riv(:,:,:,n) + f_riv(:,:,:,n)*dt_
            where(ixistz==1) d_dil(:,:,:,n) = d_dil(:,:,:,n) + f_dil(:,:,:,n)*dt_
            where(ixistz==1) d_pev(:,:,:,n) = d_pev(:,:,:,n) + f_pev(:,:,:,n)*dt_
            where(ixistz==1) d_res(:,:,:,n) = d_res(:,:,:,n) + f_res(:,:,:,n)
         enddo

         do n = 1, max_flux
            where(ixistz==1) d_from_to(:,:,:,n) = d_from_to(:,:,:,n) + f_from_to(:,:,:,n)*dt_
         enddo

#ifdef module_sediment
         do n = 1, max_flux_sed
            !print*,'sedi / seti',sedi_flux(38,18,n), seti_flux(38,18,n)
            where(ixistk==1) sedi_flux(:,:,n) = sedi_flux(:,:,n) + seti_flux(:,:,n)*dt_
         enddo
#endif
      case (1) !1D-mode
         i = iStartCalc
         j = jStartCalc
         k0 = k_index(j,i)
         do n = 1, n_st_var
            do k = 1, k0
               d_riv(j,k,i,n) = d_riv(j,k,i,n) + f_riv(j,k,i,n)*dt_
               d_dil(j,k,i,n) = d_dil(j,k,i,n) + f_dil(j,k,i,n)*dt_
               d_pev(j,k,i,n) = d_pev(j,k,i,n) + f_pev(j,k,i,n)*dt_
               d_res(j,k,i,n) = d_res(j,k,i,n) + f_res(j,k,i,n)
            enddo
         enddo

         do n = 1, max_flux
            do k = 1, k0
               d_from_to(j,k,i,n) = d_from_to(j,k,i,n) + f_from_to(j,k,i,n)*dt_
            enddo
         enddo

#ifdef module_sediment
         do n = 1, max_flux_sed
            sedi_flux(j,i,n) = sedi_flux(j,i,n) + seti_flux(j,i,n)*dt_
         enddo
#endif

   end select

   if (ierr/=0) then
      write( error_message, '("[",i2.2,"] ERROR - cum_flux: ",i3)') myID,ierr
      call stop_ecoham(ierr, msg=error_message)
   end if
   return

   end subroutine cum_flux


!=======================================================================
!
! !INTERFACE:
   subroutine cum_flux_hydro(dt_, ierr)
#define SUBROUTINE_NAME 'cum_flux_hydro'
!
! !DESCRIPTION:
! cumulate hydrodynamic fluxes to daily fluxes
!
! !USES:
   implicit none
!
! !INPUT PARAMETERS:
   real,    intent(in)     :: dt_
   integer, intent(inout)  :: ierr
!
! !LOCAL VARIABLES:
   integer       :: i, j, k, k0, n
!
!-----------------------------------------------------------------------
#include "call-trace.inc"

   select case (isw_dim) !jp
      case (3) !3D-mode
         do n = 1, n_st_var
            where(ixistz==1) d_adv(:,:,:,n) = d_adv(:,:,:,n) + f_adv(:,:,:,n)*dt_
            where(ixistz==1) d_adh(:,:,:,n) = d_adh(:,:,:,n) + f_adh(:,:,:,n)*dt_
            where(ixistz==1) d_mxv(:,:,:,n) = d_mxv(:,:,:,n) + f_mxv(:,:,:,n)*dt_
            where(ixistz==1) d_mxh(:,:,:,n) = d_mxh(:,:,:,n) + f_mxh(:,:,:,n)*dt_
            where(ixistz==1) d_hyd(:,:,:,n) = d_hyd(:,:,:,n) + f_hyd(:,:,:,n)*dt_
            where(ixistk==1) sedo_flux(:,:,n) = sedo_flux(:,:,n) + seto_flux(:,:,n)*dt_
         enddo
         where(ixistk==1) dz_vol(:,:)    = dz_vol(:,:)     +  dVol(:,:)/area(:,:)  *dt_
         where(ixistk==1) dz_adh(:,:)    = dz_adh(:,:)     +  dAdh(:,:)/area(:,:)  *dt_
         where(ixistk==1) dz_riv(:,:)    = dz_riv(:,:)     +  dRiv(:,:)/area(:,:)  *dt_

      case (1) !1D-mode
         i = iStartCalc
         j = jEndCalc
         k0 = k_index(j,i)
         do n = 1, n_st_var
            do k = 1, k0
               d_adv(j,k,i,n) = d_adv(j,k,i,n) + f_adv(j,k,i,n)*dt_
               d_adh(j,k,i,n) = d_adh(j,k,i,n) + f_adh(j,k,i,n)*dt_
               d_mxv(j,k,i,n) = d_mxv(j,k,i,n) + f_mxv(j,k,i,n)*dt_
               d_mxh(j,k,i,n) = d_mxh(j,k,i,n) + f_mxh(j,k,i,n)*dt_
               d_hyd(j,k,i,n) = d_hyd(j,k,i,n) + f_hyd(j,k,i,n)*dt_
            enddo
            sedo_flux(j,i,n) = sedo_flux(j,i,n) + seto_flux(j,i,n)*dt_
         enddo
         dz_vol(j,i)     = dz_vol(j,i)     +  dVol(j,i)/area(j,i)  *dt_
         dz_adh(j,i)     = dz_adh(j,i)     +  dAdh(j,i)/area(j,i)  *dt_
         dz_riv(j,i)     = dz_riv(j,i)     +  dRiv(j,i)/area(j,i)  *dt_

   end select

   if (ierr/=0) then
      write( error_message, '("[",i2.2,"] ERROR - cum_flux_hydro: ",i3)') myID,ierr
      call stop_ecoham(ierr, msg=error_message)
   end if
   return

   end subroutine cum_flux_hydro

!=======================================================================
!
! !INTERFACE:
   subroutine empty_flux(ierr)
#define SUBROUTINE_NAME 'empty_flux'
!
! !DESCRIPTION:
!
! !USES:
   IMPLICIT NONE
!
! !INPUT PARAMETERS:
   integer, intent(inout) :: ierr
!
! !LOCAL VARIABLES:
   integer       :: i, j, k, k0, n
!
!-----------------------------------------------------------------------
#include "call-trace.inc"

   select case (isw_dim) !jp
      case (3) !3D-mode
         do n = 1, n_st_var
            where(ixistz==1) f_riv(:,:,:,n) = zero
            where(ixistz==1) f_dil(:,:,:,n) = zero
            where(ixistz==1) f_pev(:,:,:,n) = zero
            where(ixistz==1) f_res(:,:,:,n) = zero
         enddo

         do n = 1, max_flux
            where(ixistz==1) f_from_to(:,:,:,n) = zero
         enddo

      case (1) !1D-mode
         i = iStartCalc
         j = jStartCalc
         k0 = k_index(j,i)

         do n = 1, n_st_var
            do k = 1, k0
               f_riv(j,k,i,n) = zero
               f_dil(j,k,i,n) = zero
               f_pev(j,k,i,n) = zero
               f_res(j,k,i,n) = zero
            enddo
         enddo

         do n = 1, max_flux
            do k = 1, k0
               f_from_to(j,k,i,n) = zero
            enddo
         enddo

   end select

   if (ierr/=0) then
      write( error_message, '("[",i2.2,"] ERROR - empty_flux: ",i3)') myID,ierr
      call stop_ecoham(ierr, msg=error_message)
   end if
   return

   end subroutine empty_flux

!=======================================================================
!
! !INTERFACE:
   subroutine empty_flux_hydro(ierr)
#define SUBROUTINE_NAME 'empty_flux_hydro'
!
! !DESCRIPTION:
!
! !USES:
   IMPLICIT NONE
!
! !INPUT PARAMETERS:
   integer, intent(inout) :: ierr
!
! !LOCAL VARIABLES:
   integer       :: i, j, k, k0, n
!
!-----------------------------------------------------------------------
#include "call-trace.inc"

   select case (isw_dim) !jp
      case (3) !3D-mode
         do n = 1, n_st_var
            where(ixistz==1) f_adv(:,:,:,n) = zero
            where(ixistz==1) f_adh(:,:,:,n) = zero
            where(ixistz==1) f_mxv(:,:,:,n) = zero
            where(ixistz==1) f_mxh(:,:,:,n) = zero
            where(ixistz==1) f_hyd(:,:,:,n) = zero
            where(ixistk==1) seto_flux(:,:,n) = zero
         enddo

#ifdef module_sediment
         do n = 1, max_flux_sed
            where(ixistk==1) seti_flux(:,:,n) = zero
         enddo
#endif

      case (1) !1D-mode
         i = iStartCalc
         j = jStartCalc
         k0 = k_index(j,i)

         do n = 1, n_st_var
            do k = 1, k0
               f_adv(j,k,i,n) = zero
               f_adh(j,k,i,n) = zero
               f_mxv(j,k,i,n) = zero
               f_mxh(j,k,i,n) = zero
               f_hyd(j,k,i,n) = zero
            enddo
            seto_flux(j,i,n) = zero
         enddo

#ifdef module_sediment
         do n = 1, max_flux_sed
            seti_flux(j,i,n) = zero
         enddo
#endif
   end select

   if (ierr/=0) then
      write( error_message, '("[",i2.2,"] ERROR - empty_flux_hydro: ",i3)') myID,ierr
      call stop_ecoham(ierr, msg=error_message)
   end if
   return

   end subroutine empty_flux_hydro

!=======================================================================
!
! !INTERFACE:
   subroutine empty_fluxd(ierr)
#define SUBROUTINE_NAME 'empty_fluxd'
!
! !DESCRIPTION:
!
! !USES:
   IMPLICIT NONE
!
! !INPUT PARAMETERS:
   integer, intent(inout) :: ierr
!
! !LOCAL VARIABLES:
   integer       :: i, j, k, k0, n
!
!-----------------------------------------------------------------------
#include "call-trace.inc"

   where(ixistk==1) zeta_old(:,:) = dz(:,1,:)

   dz_vol(:,:) = zero
   dz_adh(:,:) = zero
   dz_riv(:,:) = zero

   select case (isw_dim) !jp
      case (3) !3D-mode

         do n = 1, n_st_var
            where(ixistz==1) d_adv(:,:,:,n) = zero
            where(ixistz==1) d_adh(:,:,:,n) = zero
            where(ixistz==1) d_mxv(:,:,:,n) = zero
            where(ixistz==1) d_mxh(:,:,:,n) = zero
            where(ixistz==1) d_hyd(:,:,:,n) = zero
            where(ixistz==1) d_riv(:,:,:,n) = zero
            where(ixistz==1) d_dil(:,:,:,n) = zero
            where(ixistz==1) d_pev(:,:,:,n) = zero
            where(ixistz==1) d_res(:,:,:,n) = zero
            where(ixistk==1) sedo_flux(:,:,n) = zero
         enddo

         do n = 1, max_flux
            where(ixistz==1) d_from_to(:,:,:,n) = zero
         enddo

#ifdef TBNT_output
         do n = 1, n_st_var
            where(ixistz==1) d_tu(:,:,:,n) = zero
            where(ixistz==1) d_tv(:,:,:,n) = zero
            where(ixistz==1) d_tw(:,:,:,n) = zero
            where(ixistz==1) d_mu(:,:,:,n) = zero
            where(ixistz==1) d_mv(:,:,:,n) = zero
            where(ixistz==1) d_mw(:,:,:,n) = zero
         enddo
#endif

#ifdef module_sediment
         do n = 1, max_flux_sed
            where(ixistk==1) sedi_flux(:,:,n) = zero
         enddo
#endif

      case (1) !1D-mode
         i = iStartCalc
         j = jStartCalc
         k0= k_index(j,i)

         do n = 1, n_st_var
            do k = 1, k0
               d_mxv(j,k,i,n) = zero
               d_mxh(j,k,i,n) = zero
               d_hyd(j,k,i,n) = zero
            enddo
            sedo_flux(j,i,n) = zero
         enddo

         do n = 1, max_flux
            do k = 1, k0
               d_from_to(j,k,i,n) = zero
            enddo
         enddo

#ifdef TBNT_output
         do n = 1, n_st_var
            do k = 1, k0
               d_tu(j,k,i,n) = zero
               d_tv(j,k,i,n) = zero
               d_tw(j,k,i,n) = zero
               d_mu(j,k,i,n) = zero
               d_mv(j,k,i,n) = zero
               d_mw(j,k,i,n) = zero
            enddo
         enddo
#endif

#ifdef module_sediment
         do n = 1, max_flux_sed
            sedi_flux(j,i,n) = zero
         enddo
#endif

   end select

   if (ierr/=0) then
      write( error_message, '("[",i2.2,"] ERROR - empty_fluxd: ",i3)') myID,ierr
      call stop_ecoham(ierr, msg=error_message)
   end if
   return

   end subroutine empty_fluxd

!=======================================================================

   end module mod_flux

!=======================================================================
