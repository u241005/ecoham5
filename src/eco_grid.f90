!
! !MODULE: grid.f90 --- grid specifications
!
! !INTERFACE:
   MODULE mod_grid
!
! !DESCRIPTION:
!
! !USES:
   use mod_par
   use mod_common
   use mod_grid_parameters
#ifdef MPI
   use mod_mpi_parallel
#endif
   implicit none
!
!  default: all is public.
   public
!
   ! GRID VARIABLES
   real,    dimension(:,:,:), allocatable :: dz, dz_master
   real,    dimension(:,:,:), allocatable :: vol, vol_master
   real,    dimension(:,:),   allocatable :: area, area_master
   real,    dimension(:,:),   allocatable :: dep_last, dep_last_master
   real,    dimension(:,:),   allocatable :: dVol, dAdh, dRiv
   real,    dimension(:),     allocatable :: dxx, dxm, dxp, dxx2
   real                                   :: dyy, dyy2, dz_min
   real,    dimension(kmax)               :: dzz
   real,    dimension(kmax+1)             :: tiestl, tiestu

   integer, dimension(:,:),   allocatable :: k_index, k_index_master
   integer, dimension(:,:),   allocatable :: ixistk, ixistk_master
   integer, dimension(:,:,:), allocatable :: ixistz, ixistz_master
   integer, dimension(:,:,:), allocatable :: ixistu
   integer, dimension(:,:,:), allocatable :: ixistv
   integer, dimension(:,:,:), allocatable :: ixistw

   logical, dimension(:,:),   allocatable :: lxistK_eq_1
   logical, dimension(:,:,:), allocatable :: lxistZ_eq_1

   ! for namelist
   character*99 :: grid_dir
   integer      :: offset_istart, offset_iend, offset_jstart, offset_jend
!
!=======================================================================

   contains

!=======================================================================
!
! !INTERFACE:
   subroutine init_grid(ierr)
#define SUBROUTINE_NAME 'init_grid'
!
! !DESCRIPTION:
!
! !USES:
   implicit none
!
! !OUTPUT PARAMETERS:
   integer, intent(inout)  :: ierr
!
! !LOCAL VARIABLES:
   integer       :: i, j, k, k0
   integer       :: isuc, jsuc, ksuc, iwet, iwet2
!
   namelist /grid_nml/grid_dir, offset_istart, offset_iend, &
                                offset_jstart, offset_jend
!
!-----------------------------------------------------------------------
#include "call-trace.inc"

   ! allocate arrays defined for full domain and known on each CPU
   allocate( k_index_master(jmax,imax) )
   allocate( dep_last_master(jmax,imax) )
   allocate( ixistK_master(jmax,imax) )
   allocate( ixistZ_master(jmax,kmax,imax) )

   ! open & read namelist settings
   !--------------------------------------------------------------
   open(nml_unit,file=trim(settings_file),action='read',status='old')
   read(nml_unit,nml=grid_nml)
   close(nml_unit)

   ! read definitions from file
   call read_grid(ierr)

   ! dz_min must be analysed here, as only master can loop over full domain
   dz_min = 1.0e6
   do i=1,imax
      do j=1,jmax
         k0 = k_index_master(j,i)
         do k=1,k0-1
            dz_min = min(dz_min,tiestl(k+1)-tiestl(k))
         enddo
         if(k0.ge.1)then
            dz_min = min(dz_min,dep_last_master(j,i))
         endif
      enddo
   enddo

   ! create ixistX-arrays and count iiWET on full domain
   ixistK_master=0
   ixistZ_master=0
   iwet2=0
   iwet=0
   do i = 1, iMax
      do j = 1, jMax
         k0=k_index_master(j,i)
         if(k0>0)then
            iwet2 = iwet2+1
            ixistK_master(j,i) = 1
         endif
         do k = 1, k0
            ixistZ_master(j,k,i) = 1
            iwet = iwet+1
         enddo
      enddo
   enddo
   if(iwet/=iiwet)then
      ierr = 1
      write(error_message,'("ERROR: iiWET dimensions are not correct (iiwet,iiwet*):",2i8)') iwet,iiwet
      call stop_ecoham(ierr, msg=error_message)
   endif

   ! apply offset only to open boundaries --> adjust namelist entries
   if (count (ixistz_master(  : ,1,  1 )<1) == jmax) offset_istart = 0
   if (count (ixistz_master(  : ,1,imax)<1) == jmax) offset_iend   = 0
   if (count (ixistz_master(  1 ,1,  : )<1) == imax) offset_jstart = 0
   if (count (ixistz_master(jmax,1,  : )<1) == imax) offset_jend   = 0

   ! set indices for calculation area
   iStartCalc = 3       +offset_istart     ! offset_istart = 3
   iEndCalc   = imax -1 -offset_iend       ! offset_iend   = 2
   jStartCalc = 3       +offset_jstart     ! offset_jstart = 3
   jEndCalc   = jmax -1 -offset_jend       ! offset_jend   = 2

#ifdef MPI
   call mpi_parallel_decomp(ixistz_master,iStartCalc,iEndCalc,ierr_mpi)
   ierr = ierr + ierr_mpi
   if (ierr/=0) call stop_ecoham(ierr, msg='ERROR - mpi_parallel_decomp')
   iStartD = max (1   , iStart-1)
   iEndD   = min (iMax, iEnd  +1)
   if (myID == master) then
      call write_log_message(line_separator)
      call write_log_message('         *** Domain decomposition table ***')
      call write_log_message(line_separator)
      write(long_msg,'("[rank] iStartC   iEndC  iiwetC iStartD   iEndD   rowsD  iiwetD")')
      call write_log_message(long_msg); long_msg = ''
      write(long_msg,'(" [",a2,"]",8i8)') 'NB', 0, 0,                     &
                         count(ixistz_master(:,:,1:iStartCalc-1)==1),     &
                         0, 0, iStartCalc-1, 0
      call write_log_message(long_msg); long_msg = ''
   endif
   do i = 0, worldsize-1
      if (i == myID) then
         write(long_msg,'(" [",i2.2,"]",8i8)') i, iStartCalc, iEndCalc,    &
                         count(ixistz_master(:,:,iStartCalc:iEndCalc)==1), &
                         iStartD, iEndD, iEndD-iStartD+1,                  &
                         count(ixistz_master(:,:,iStartD:iEndD)==1)

         call write_log_message(long_msg); long_msg = ''
      endif
      call MPI_ALLREDUCE(ierr, ierr_all, 1, MPI_INT, MPI_SUM, MPI_COMM_ECOHAM, ierr_mpi)
   enddo
   if (myID == worldsize-1) then
      write(long_msg,'(" [",a2,"]",8i8)') 'SB', 0, 0,                     &
                         count(ixistz_master(:,:,iEndCalc+1:iMax)==1),    &
                         0, 0, iMax-iEndCalc, 0
      call write_log_message(long_msg); long_msg = ''
   endif
   call MPI_ALLREDUCE(ierr, ierr_all, 1, MPI_INT, MPI_SUM, MPI_COMM_ECOHAM, ierr_mpi)
   if (myID == master) call write_log_message(line_separator)
#endif

   ! allocate and initialize arrays defined on local domains
   allocate( dep_last(jmax,iStartD:iEndD))
   dep_last = zero
   allocate( k_index (jmax,iStartD:iEndD))
   k_index = 0
   allocate( ixistZ(jmax,kmax,iStartD:iEndD) )
   ixistZ = 0
   allocate( ixistK(jmax,     iStartD:iEndD) )
   ixistK = 0

   dep_last(:,iStartD:iEndD) = dep_last_master(:,iStartD:iEndD)
   k_index (:,iStartD:iEndD) = k_index_master (:,iStartD:iEndD)
   ixistZ(:,:,iStartD:iEndD) = ixistZ_master(:,:,iStartD:iEndD)
   ixistK  (:,iStartD:iEndD) = ixistK_master  (:,iStartD:iEndD)

   ! deallocate arrays for full domain no longer needed
   deallocate( dep_last_master )

   do k=1,kmax
      tiestu(k)=(tiestl(k+1)+tiestl(k))/2.
      dzz(k)   = tiestl(k+1)-tiestl(k)
   enddo

   allocate( dxx (     iStartD:iEndD) )
   allocate( dxx2(     iStartD:iEndD) )
   allocate( area(jmax,iStartD:iEndD) )
   dyy = ddlat(0)
   dyy2 = dyy*dyy
   do i = iStartD, iEndD
      dxx(i) = (ddlon(i)+ddlon(i+1))/2.
      !dxx(i) = ddx(i)
      dxx2(i) = dxx(i)*dxx(i)
      do j = 1, jmax
         k0 = k_index(j,i)
         if(k0.gt.0)then
            area(j,i) = dxx(i)*dyy
         endif
      enddo
   enddo

   allocate(  dz(jmax,kmax,iStartD:iEndD))
   allocate( vol(jmax,kmax,iStartD:iEndD))
   allocate( dVol(jmax,iStartD:iEndD))
   allocate( dAdh(jmax,iStartD:iEndD))
   allocate( dRiv(jmax,iStartD:iEndD))
   dz  = zero
   vol = zero
   dVol = zero
   dAdh = zero
   dRiv = zero
   do i = iStartD, iEndD
      do j = 1, jmax
         k0 = k_index(j,i)
         do k = 1, k0-1
            dz(j,k,i)  = dzz(k)
            vol(j,k,i) = area(j,i)*dzz(k)
         enddo
         if(k0.ge.1)then
            dz(j,k0,i)  = dep_last(j,i)
            vol(j,k0,i) = area(j,i)*dep_last(j,i)
         endif
      enddo
   enddo

   allocate( ixistU(jmax,  kmax,iStartD:iEndD) )
   allocate( ixistV(jmax,  kmax,iStartD:iEndD) )
   allocate( ixistW(jmax,0:kmax,iStartD:iEndD) )
   ixistW(:,0,:) = 0
   do i = iStartD, iEndD
      iSuc = min(i+1, iMax)
      do j = 1, jMax
         jSuc = min(j+1, jMax)
         do k = 1, kMax
            kSuc = min(k+1, kMax)
            ixistU(j,k,i) = ixistZ(j,k,i) * ixistZ_master(jsuc,k,i)
            ixistV(j,k,i) = ixistZ(j,k,i) * ixistZ_master(j,k,isuc)
            ixistW(j,k,i) = ixistZ(j,k,i) * ixistZ_master(j,ksuc,i)
         enddo
      enddo
   enddo

   if (ierr/=0) then
      write( error_message, '("[",i2.2,"] ERROR - init_grid: ",i3)') myID,ierr
      call stop_ecoham(ierr, msg=error_message)
   end if
   return

   end subroutine init_grid

!=======================================================================
!
! !INTERFACE:
   subroutine read_grid(ierr)
#define SUBROUTINE_NAME 'read_grid'
!
! !DESCRIPTION:
!
! !USES:
   implicit none
!
! !OUTPUT PARAMETERS:
   integer, intent(inout)  :: ierr
!
! !LOCAL VARIABLES:
   integer       :: i,j,k,imaxr,jmaxr,kmaxr,iwet,iwet2
   integer(kind=4), allocatable :: ibuf(:,:)
   real   (kind=4), allocatable :: buf(:)
!-----------------------------------------------------------------------
#include "call-trace.inc"

   filename = trim(grid_dir)//trim(gridfile)
   if (index(filename,'.bin') /= 0) then
     allocate ( ibuf(jMax,iMax), buf(kmax+1) )
     open(grid_unit,file=trim(filename),form='unformatted',convert='little_endian',status='old')
     read(grid_unit)ibuf(1:5,1) ! use this for reading indh-GridID.dat !!
     jmaxr = ibuf(1,1); kmaxr = ibuf(2,1); imaxr = ibuf(3,1); iwet = ibuf(4,1); iwet2 = ibuf(5,1)
     read(grid_unit)ibuf
     k_index_master = ibuf
     read(grid_unit) buf
     tiestl(1:kMax+1) = real (buf)
     read(grid_unit) ibuf
     dep_last_master = real (ibuf)
     close(grid_unit)
     deallocate ( ibuf, buf )
   else
     open(grid_unit,file=trim(filename),status='old')
     read(grid_unit,*)jmaxr,kmaxr,imaxr,iwet,iwet2 ! use this for reading indh-GridID.dat !!
     read(grid_unit,*)((k_index_master(j,i),j=1,jmax),i=1,imax)
     read(grid_unit,*)(tiestl(k),k=1,kmax+1)
     read(grid_unit,*)((dep_last_master(j,i),j=1,jmax),i=1,imax)
     close(grid_unit)
   endif

   if(jmaxr/=jmax.or.imaxr/=imax.or.kmaxr/=kmax.or.iwet/=iiwet.or.iwet2/=iiwet2)then
      ierr = 1
      write (log_msg(1),'("GRID dimensions are not correct")')
      write (log_msg(2),'("defined parameters(jmax,imax,kmax,iiwet,iiwet2) :",5i8)') jmax,imax,kmax,iiwet,iiwet2
      write (log_msg(3),'("read from file    (jmax,imax,kmax,iiwet,iiwet2) :",5i8)') jmaxr,imaxr,kmaxr,iwet,iwet2
      long_msg = trim(log_msg(1))//new_line('a')//trim(log_msg(2))//new_line('a')//trim(log_msg(3))
      call write_log_message(long_msg); long_msg=''
   endif

   if (ierr/=0) then
      write( error_message, '("[",i2.2,"] ERROR - read_grid: ",i3)') myID,ierr
      call stop_ecoham(ierr, msg=error_message)
   end if
   return

   end subroutine read_grid

!=======================================================================
!
! !INTERFACE:
   real function ddx(i)
!
! !DESCRIPTION:
! i is Z-point
! function returns dlon (m) at center of i
!
! !USES:
   implicit none
!
! !INPUT PARAMETERS:
   integer, intent(in)   :: i
!
! !LOCAL VARIABLES:
   real          :: xlat,alat
!-----------------------------------------------------------------------

   xlat=xlat0+real(imax-i)*delta_lat
   alat=xlat*pi/180.
   ddx=abs(cos(alat)*earth_umf*delta_lon/.360)

   return
   end function ddx

!=======================================================================
!
! !INTERFACE:
   real function ddlon(i)
!
! !DESCRIPTION:
! i is Z-point
! function returns dlon (m) delta_lat/2 degree north of i
!
! !USES:
   implicit none
!
! !INPUT PARAMETERS:
   integer, intent(in)   :: i
!
! !LOCAL VARIABLES:
   real          :: xlat,alat
!-----------------------------------------------------------------------

   xlat=xlat0+real(imax-i)*delta_lat+delta_lat/2.
   alat=xlat*pi/180.
   ddlon=abs(cos(alat)*earth_umf*delta_lon/.360)

   return
   end function ddlon

!=======================================================================
!
! !INTERFACE:
!
! !INTERFACE:
   real function ddlat(jdum)
!
! !DESCRIPTION:
! function returns dlat (m)
!
! !USES:
   implicit none
!
! !INPUT PARAMETERS:
   integer, intent(in)   :: jdum
!
! !LOCAL VARIABLES:
   real          :: dum
!-----------------------------------------------------------------------
   dum = real(jdum) ! to get rid of warning: "unused variable"

   ddlat=earth_umf*delta_lat/.36

   return
   end function ddlat

!=======================================================================

   end module mod_grid

!=======================================================================