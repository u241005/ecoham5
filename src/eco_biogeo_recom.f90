#ifdef module_biogeo
!
! !MODULE: eco_biogeo_recom.f90 --- pelagic biogeochemistry for ecoham with adoptions from CNP-REcoM
!
! !INTERFACE:
   MODULE mod_biogeo_recom
#ifdef module_biogeo_recom
!
! !DESCRIPTION:
! This module is the container for pelagic ecoham biogeochemistry
!
! !USES:
   use mod_common
!   use mod_mpi_parallel
   use mod_var
   use mod_flux
   implicit none
!
!  default: all is private
   private
!
! !PUBLIC MEMBER FUNCTIONS:
   public init_biogeo, do_biogeo, update_biogeo
   public cum_biofunctions, empty_biofunctions
!
! !LOCAL VARIABLES:
! used for namelist:
   integer :: extp_feedback
   real :: extp, extp1, extp2, extp3, vp1, vp2, vp3, excess, soc_rate
   real, public :: q_c_cal
   real :: xk1, xk21, xk22, xkp, xks, gam1, gam2
   real :: xmu11, xmu12, wp1c, wp2c, wp3c, wd2k, g1_max, g2_max
   real :: xk31, xk32, p1_p1n, p1_p2n, p1_p3n, p1_d1n
   real :: p1_ban, p2_p1n, p2_p2n, p2_p3n, p2_d1n, p2_ban, p2_z1n
   real :: xmu21, xmu22, xmu6, beta1, beta2, xk16, xk26
   real :: aeps1, aeps2, delta_don1, delta_don2, frac_det, frac_d2x
   real :: wd1c, wd2c, xmu4n, xmu5n, rxmu4c, xk4, eta, vb, xmu3
   real :: xknit, rhoC, rhoN, rhoP, brc, brn, brp, brs
   real, public :: red, rcn, rcp, rcs, rcn2, rcp2, rcn3, rcp3
   real, public :: rcnb, rcpb, rcnz1, rcpz1, rcnz2, rcpz2
   real, public :: chl_p1c, chl_p2c, chl_p3c
   real :: xmq11, xmq12, xkpb, xk13, xk23, xkp3, gam3
   real :: xmu13, xmq13, c_max, rccalc_min, xkc_ir, xkk_ir, xkk, detach_min
   real :: xmq21, xmq22, xmu6c, frac_dic, frac_n1p, frac_n4n, g1_exp, g2_exp
! used for CNP-REcoM:
   real, parameter :: TINY= 1.0e-3
   real :: redf, Tref, Ar, zeta_nit, sigma_NC, sigma_NP, sigma_PC, sigma_PN
   real :: f_pcho_max, rho_soc, agg_pcho, agg_tepc, agg_det
   real :: Ar_p1, alpha_p1, mu_p1_max, k_p1_n3n, k_p1_n4n, k_p1_n1p, k_p1_n5s
   real :: Ar_p2, alpha_p2, mu_p2_max, k_p2_n3n, k_p2_n4n, k_p2_n1p
   real :: Ar_p3, alpha_p3, mu_p3_max, k_p3_n3n, k_p3_n4n, k_p3_n1p
   real :: deg_p1a, r0_p1, loss_p1c, loss_p1n, loss_p1p, agg_p1n
   real :: deg_p2a, r0_p2, loss_p2c, loss_p2n, loss_p2p, agg_p2n
   real :: deg_p3a, r0_p3, loss_p3c, loss_p3n, loss_p3p, agg_p3n
   real :: Qmin_p1s_p1n, Qmax_p1s_p1n
   real :: Qmin_p1n_p1c, Qmax_p1n_p1c, Qmin_p1p_p1n, Qmax_p1p_p1n, Qmax_p1a_p1n
   real :: Qmin_p2n_p2c, Qmax_p2n_p2c, Qmin_p2p_p2n, Qmax_p2p_p2n, Qmax_p2a_p2n
   real :: Qmin_p3n_p3c, Qmax_p3n_p3c, Qmin_p3p_p3n, Qmax_p3p_p3n, Qmax_p3a_p3n
   real :: Q0_p1n_p1p, Qupt_din_p1c, Qupt_dip_p1n, Qupt_dis_p1n
   real :: Q0_p2n_p2p, Qupt_din_p2c, Qupt_dip_p2n
   real :: Q0_p3n_p3p, Qupt_din_p3c, Qupt_dip_p3n

! public parameters used in other modules:
   real :: adepth_max
   real, allocatable, dimension(:,:,:), public :: d_fPAR, d_chl_p1c, d_chl_p2c, d_chl_p3c
   real, allocatable, dimension(:,:,:), public :: d_lim_p1_n1p, d_lim_p1_n3n, d_lim_p1_n4n, d_lim_p1_n5s
   real, allocatable, dimension(:,:,:), public :: d_lim_p2_n1p, d_lim_p2_n3n, d_lim_p2_n4n
   real, allocatable, dimension(:,:,:), public :: d_lim_p3_n1p, d_lim_p3_n3n, d_lim_p3_n4n, d_lim_p3_dop
   real, allocatable, dimension(:,:,:), public :: d_lim_p1_IR,  d_lim_p1_RCN, d_lim_p1_RNC, d_lim_p1_RPC
   real, allocatable, dimension(:,:,:), public :: d_lim_p2_IR,  d_lim_p2_RCN, d_lim_p2_RNC, d_lim_p2_RPC
   real, allocatable, dimension(:,:,:), public :: d_lim_p3_IR,  d_lim_p3_RCN, d_lim_p3_RNC, d_lim_p3_RPC
   real, allocatable, dimension(:,:,:), public :: d_lim_p1_RPN, d_lim_p1_RNP, d_lim_p1_RSN, d_lim_p1_RNS
   real, allocatable, dimension(:,:,:), public :: d_lim_p2_RPN, d_lim_p2_RNP
   real, allocatable, dimension(:,:,:), public :: d_lim_p3_RPN, d_lim_p3_RNP
   integer :: iexcess
! parameters private to module bio:
   real, allocatable, dimension(:,:,:) :: lim_p1_n1p, lim_p1_n3n, lim_p1_n4n, lim_p1_n5s
   real, allocatable, dimension(:,:,:) :: lim_p2_n1p, lim_p2_n3n, lim_p2_n4n
   real, allocatable, dimension(:,:,:) :: lim_p3_n1p, lim_p3_n3n, lim_p3_n4n, lim_p3_dop
   real, allocatable, dimension(:,:,:) :: lim_p1_RSN, lim_p1_RNS
   real, allocatable, dimension(:,:,:) :: lim_p1_IR,  lim_p1_RCN, lim_p1_RNC, lim_p1_RPC, lim_p1_RPN, lim_p1_RNP
   real, allocatable, dimension(:,:,:) :: lim_p2_IR,  lim_p2_RCN, lim_p2_RNC, lim_p2_RPC, lim_p2_RPN, lim_p2_RNP
   real, allocatable, dimension(:,:,:) :: lim_p3_IR,  lim_p3_RCN, lim_p3_RNC, lim_p3_RPC, lim_p3_RPN, lim_p3_RNP
   real, allocatable, dimension(:,:,:) :: fPAR, q_chl_p1c, q_chl_p2c, q_chl_p3c
   real, allocatable, dimension(:,:,:) :: omega, anitdep
   real, allocatable, dimension(:,:,:) :: fp3, fp3k
   real, allocatable, dimension(:,:,:) :: tfac, tfac_p1, tfac_p2, tfac_p3
   real, allocatable, dimension(:,:,:) :: tfac_z1, tfac_z2
!
#ifdef cskc
   character(len=*), parameter, private :: init_endian   = 'LITTLE_ENDIAN'
#else
   character(len=*), parameter, private :: init_endian   = 'BIG_ENDIAN'
#endif
   character(len=*), parameter, private :: init_dir      = 'Input/'
   integer,          parameter, private :: init_realkind = 8   != ibyte_per_real
!
!=======================================================================

   contains

!=======================================================================
!
! !INTERFACE:
   subroutine init_biogeo(ierr)
#define SUBROUTINE_NAME 'init_biogeo_recom'
!
! !DESCRIPTION:
!
! !USES:
   !use mod_hydro,     only : salt, temp, rho, pres
   use mod_utils,     only : calcite
#ifdef module_restoring
   use mod_restoring, only : get_restoring, do_restoring
#endif
   implicit none
!
! !INTPUT PARAMETERS:
   integer, intent(inout) :: ierr
!
! !LOCAL VARIABLES:
   integer              :: k, n, iz(kmax)
   real                 :: wmax
   logical              :: file_exist
   real(kind=init_realkind), allocatable :: input1d(:), input3d(:,:,:)
!
   namelist /bio_nml/  extp_feedback, &
             extp, extp1, extp2, extp3, vp1, vp2, vp3, xk1, xk13,      &
             xk21, xk22, xk23, xkp, xkp3, xks, gam1, gam2, gam3,       &
             xmu11, xmu12, xmu13, xmq11, xmq12, xmq13,                 &
             wp1c, wp2c, wp3c, excess, soc_rate, q_c_cal,              &
             c_max, rccalc_min, xkc_ir, xkk_ir, xkk, detach_min,       &
             g1_max, g2_max, g1_exp, g2_exp, xk31, xk32,               &
             p1_p1n, p1_p2n, p1_p3n,p1_d1n, p1_ban,                    &
             p2_p1n, p2_p2n, p2_p3n, p2_d1n, p2_ban, p2_z1n,           &
             xmu21, xmu22, xmq21, xmq22, xmu6, xmu6c, beta1, beta2,    &
             xk16, xk26, aeps1, aeps2, delta_don1, delta_don2,         &
             frac_dic, frac_n4n, frac_n1p, frac_det, frac_d2x,         &
             wd1c, wd2c, xmu4n, xmu5n, rxmu4c,                         &
             xk4, xkpb, eta, vb, xmu3, xknit, rhoC, rhoN, rhoP,        &
             red, rcn, rcp, rcs, rcn2, rcp2, rcn3, rcp3, rcnb, rcpb,   &
             rcnz1, rcpz1, rcnz2, rcpz2, chl_p1c, chl_p2c, chl_p3c

   namelist /recomCNP_nml/  redf, Tref, Ar, &
             zeta_nit, sigma_NC, sigma_NP, sigma_PC, sigma_PN,          &
             f_pcho_max, rho_soc, agg_pcho, agg_tepc, agg_det, & !agg_d1n, agg_d2n,  &
             Ar_p1, alpha_p1, mu_p1_max,                                &
             K_p1_n3n, K_p1_n4n, K_p1_n1p, K_p1_n5s,                    &
             deg_p1a, r0_p1, loss_p1c, loss_p1n, loss_p1p, agg_p1n,     &
             Qmin_p1n_p1c, Qmax_p1n_p1c, Qmin_p1p_p1n, Qmax_p1p_p1n,    &
             Qmin_p1s_p1n, Qmax_p1s_p1n, Qmax_p1a_p1n,                  &
             Qupt_din_p1c, Qupt_dip_p1n, Qupt_dis_p1n, Q0_p1n_p1p,      &
             Ar_p2, alpha_p2, mu_p2_max,                                &
             K_p2_n3n, K_p2_n4n, K_p2_n1p,                              &
             deg_p2a, r0_p2, loss_p2c, loss_p2n, loss_p2p, agg_p2n,     &
             Qmin_p2n_p2c, Qmax_p2n_p2c, Qmin_p2p_p2n, Qmax_p2p_p2n,    &
             Qmax_p2a_p2n, Qupt_din_p2c, Qupt_dip_p2n, Q0_p2n_p2p,      &
             Ar_p3, alpha_p3, mu_p3_max,                                &
             K_p3_n3n, K_p3_n4n, K_p3_n1p,                              &
             deg_p3a, r0_p3, loss_p3c, loss_p3n, loss_p3p, agg_p3n,     &
             Qmin_p3n_p3c, Qmax_p3n_p3c, Qmin_p3p_p3n, Qmax_p3p_p3n,    &
             Qmax_p3a_p3n, Qupt_din_p3c, Qupt_dip_p3n, Q0_p3n_p3p

!
!-------------------------------------------------------------------------
#include "call-trace.inc"

   if (myID == master) call write_log_message(" INITIALIZING biogeo_recom")

! #if !defined module_restoring && defined exclude_restoring_cells_biogeo
!    error_message = 'ERROR: You may not specify -Dexclude_restoring_cells_biogeo' &
!                   //' without Flag -Dmodule_restoring!'
!    call stop_ecoham(ierr, msg=error_message)
! #endif

   if (myID == master) then
#ifdef exclude_p1x
      write(long_msg,'(3x,"-exclude first phytoplankton group (e.g. diatoms)")')
      call write_log_message(long_msg); long_msg = ''
#endif
#ifdef exclude_p2x
      write(long_msg,'(3x,"-exclude second phytoplankton group (e.g. flagellates)")')
      call write_log_message(long_msg); long_msg = ''
#endif
#ifdef exclude_p3x
      write(long_msg,'(3x,"-exclude third phytoplankton group (recomended for D093)")')
      call write_log_message(long_msg); long_msg = ''
#endif
#ifdef exclude_z1x
      write(long_msg,'(3x,"-exclude first zooplankton group (e.g. mircozoo)")')
      call write_log_message(long_msg); long_msg = ''
#endif
#ifdef exclude_z2x
      write(long_msg,'(3x,"-exclude second zooplankton group (e.g. mesozoo)")')
      call write_log_message(long_msg); long_msg = ''
#endif
#ifdef exclude_bac
      write(long_msg,'(3x,"-exclude bacteria group (apply direct remineralization DOM->DIM)")')
      call write_log_message(long_msg); long_msg = ''
#endif

#if !defined exclude_p1x || !defined exclude_p2x || !defined exclude_p2x
#ifdef extp_d093
      write(long_msg,'(3x,"-phytoplankton light extinction according to fasham (recommended for D093)")')
      call write_log_message(long_msg); long_msg = ''
#endif
#ifdef variable_phytoplankton_stoichiometry
      write(long_msg,'(3x,"-use real C:N:P-ratios from statevariables instead of values from namelist")')
      call write_log_message(long_msg); long_msg = ''
#endif
#endif

#if !defined exclude_z1x || !defined exclude_z2x
#ifdef fasham_grazing
      write(long_msg,'(3x,"-grazing is formulated based on fasham (recomended for D093)")')
      call write_log_message(long_msg); long_msg = ''
#endif
#ifdef fasham_losses
      write(long_msg,'(3x,"-loss-terms are formulated based on Fasham (recomended for D093)")')
      call write_log_message(long_msg); long_msg = ''
#ifdef fasham_losses_revised
      write(long_msg,'(3x,"-apply revision from markus on fasham formulations (will break D093 compatibility)")')
      call write_log_message(long_msg); long_msg = ''
#endif
#ifdef old_feces_stoichiometry
      write(long_msg,'(3x,"-feces have C:N:P from zooplankton (recomended for D093)")')
      call write_log_message(long_msg); long_msg = ''
#else
      write(long_msg,'(3x,"-flag old_feces_stoichiometry disabled (will break D093 compatibility)")')
      call write_log_message(long_msg); long_msg = ''
#endif
#else
      write(long_msg,'(3x,"-loss-terms according to suggestion from book Sterner&Elser(2002)-pp197")')
      call write_log_message(long_msg); long_msg = ''
#endif
#endif

#if !defined exclude_bac
#ifdef old_bac
      write(long_msg,'(3x,"-using old_bac (bacteria according to D093)")')
      call write_log_message(long_msg); long_msg = ''
#else
      write(long_msg,'(3x,"-using new bacteria formulations")')
      call write_log_message(long_msg); long_msg = ''
#endif
#endif
   endif ! master

   open(nml_unit, file=trim(settings_bio), action='read', status='old')
   read(nml_unit, nml=bio_nml)
   close(nml_unit)

   open(nml_unit, file='eco_recomCNP.nml', action='read', status='old')
   read(nml_unit, nml=recomCNP_nml)
   close(nml_unit)

   if (abs(excess)<1.e-5) then
      iexcess = 0
   else
      iexcess = 1
   endif

   ! who sinks?
   do n = 1, n_st_var
      sink(n) = 0.0 ! initialize all to zero
   enddo
   sink(ip1c) = wp1c ! sinking of p1c
   sink(ip1n) = wp1c ! sinking of p1n
   sink(ip1p) = wp1c ! sinking of p1p
   sink(ip2c) = wp2c ! sinking of p2c
   sink(ip2n) = wp2c ! sinking of p2n
   sink(ip2p) = wp2c ! sinking of p2p
   sink(ip3c) = wp3c ! sinking of p3c
   sink(ip3n) = wp3c ! sinking of p3n
   sink(ip3p) = wp3c ! sinking of p3p
   sink(id1c) = wd1c ! sinking of d1c
   sink(id1n) = wd1c ! sinking of d1n
   sink(id1p) = wd1c ! sinking of d1p
   sink(id2c) = wd2c ! sinking of d2c
   sink(id2n) = wd2c ! sinking of d2n
   sink(id2p) = wd2c ! sinking of d2p
   sink(id2s) = wd2c ! sinking of d2s
   sink(id2k) = wd2c ! sinking of d2k
#ifdef TBNT_x1x_test
   sink(ix1x) = wd1c ! sinking for "biological" x1x
   sink(ix2x) = wd2c ! sinking for "biological" x2x
#endif

   ! check whether CFL criterion for sinking is violated
   ! for parallel code dz_min ist calculated for each process
   wmax = max(wp1c, wp2c, wp3c, wd1c, wd2c, wd2k)
   if (dz_min .lt. wmax*dt) then
      ierr = 1
      call stop_ecoham(ierr, msg="ERROR: CFL violated for sinking")
   endif

   ! allocate and initialize arrays
   call allocate_arrays_biogeo(ierr)

   ! initialize chlorophyll-to-C ratio
   where(ixistZ==1) q_chl_p1c(:,:,:) = chl_p1c !0.003*12 ! g Chl (mol C)^(-1)
   where(ixistZ==1) q_chl_p2c(:,:,:) = chl_p2c !0.003*12 ! g Chl (mol C)^(-1)
   where(ixistZ==1) q_chl_p3c(:,:,:) = chl_p3c !0.003*12 ! g Chl (mol C)^(-1)

   !initialize statevariables on wet points
   if (iwarm.ne.1) then
      if (myID == master) call write_log_message("   coldstart initialization")
      ! INITIAL VALUES most in [mmol m-3]
      ! default initialization, homogenous distribution
      where(ixistz==1) st(:,:,:,ix1x) = 1.
      where(ixistz==1) st(:,:,:,ialk) = 2358.
      where(ixistz==1) st(:,:,:,idic) = 2163.    ! DIC
      where(ixistz==1) st(:,:,:,in3n) = 8.       ! nitrate
      where(ixistz==1) st(:,:,:,in4n) = 0.1      ! ammonium
      where(ixistz==1) st(:,:,:,in1p) = 1.       ! phosphate
      where(ixistz==1) st(:,:,:,in5s) = 5.       ! silicate
      where(ixistz==1) st(:,:,:,iz1c) = 0.25*red
      where(ixistz==1) st(:,:,:,iz2c) = 0.25*red
      where(ixistz==1) st(:,:,:,ibac) = 0.25
      where(ixistz==1) st(:,:,:,id1c) = 0.01*red
      where(ixistz==1) st(:,:,:,id1n) = 0.01
      where(ixistz==1) st(:,:,:,id1p) = 0.01/16.
      where(ixistz==1) st(:,:,:,id2c) = 0.0001*red
      where(ixistz==1) st(:,:,:,id2n) = 0.0001
      where(ixistz==1) st(:,:,:,id2p) = 0.0001/16.
      where(ixistz==1) st(:,:,:,id2s) = 0.0001/5.76
      where(ixistz==1) st(:,:,:,id2k) = 0.0001*red/40.
      where(ixistz==1) st(:,:,:,isoc) = 0.1
      where(ixistz==1) st(:,:,:,idon) = 0.01
      where(ixistz==1) st(:,:,:,idoc) = 0.01*red
      where(ixistz==1) st(:,:,:,idop) = 0.01/16.
      where(ixistz==1) st(:,:,:,io2o) = 280.
      ! phytoplankton initialization with homogenous two layer distribution
      iz = 1
      where (tiestl(1:kmax) < 150.) iz = 1
      k = sum(iz)
      where(ixistz(:,  1:k   ,:) ==1) st(:,  1:k   ,:,ip1n) = 0.1
      where(ixistz(:,k+1:kmax,:) ==1) st(:,k+1:kmax,:,ip1n) = 0.0001
      ! derive other phytoplankton variables according to given quotas
      where(ixistz==1) st(:,:,:,ip1c) = st(:,:,:,ip1n)*rcn
      where(ixistz==1) st(:,:,:,ip1p) = st(:,:,:,ip1c)/rcp
      where(ixistz==1) st(:,:,:,ip1s) = st(:,:,:,ip1c)/rcs
      where(ixistz==1) st(:,:,:,ip2c) = st(:,:,:,ip1c)
      where(ixistz==1) st(:,:,:,ip2n) = st(:,:,:,ip1n)
      where(ixistz==1) st(:,:,:,ip2p) = st(:,:,:,ip1p)
      where(ixistz==1) st(:,:,:,ip3c) = st(:,:,:,ip1c)
      where(ixistz==1) st(:,:,:,ip3n) = st(:,:,:,ip1n)
      where(ixistz==1) st(:,:,:,ip3p) = st(:,:,:,ip1p)
      where(ixistz==1) st(:,:,:,ip3k) = st(:,:,:,ip1c)/q_c_cal
      ! Chlorophyll
      where(ixistz==1) st(:,:,:,ip1a) = st(:,:,:,ip1c)*q_chl_p1c
      where(ixistz==1) st(:,:,:,ip2a) = st(:,:,:,ip2c)*q_chl_p2c
      where(ixistz==1) st(:,:,:,ip3a) = st(:,:,:,ip3c)*q_chl_p3c

      if (iwarm == 2) then
        ! read from initial distribution files, if exist, and overwrite
         allocate (input1d(iiwet),input3d(jMax,kMax,iMax))
         do n = 1, n_st_var
            filename = trim(init_dir)//'init.'//trim(st_name(n))//'.direct'
            inquire (file=trim(filename), exist=file_exist)
            if (file_exist) then
               if (myID == master) call write_log_message("   include initialization from "//trim(filename) )
               open (warm_unit, file=trim(filename), status='old', form='unformatted', &
                                access='direct', recl=init_realkind*iiwet, convert=trim(init_endian) )
               read (warm_unit, rec = 1) input1d
               input3d(:,:,:) = unpack(real(input1d),ixistz_master(:,:,:)==1,fail)
               st(:,:,:,n)    = input3d(:,:,iStartD:iEndD)
               close (warm_unit)
            else
              if (myID == master) call write_log_message("   init file "//trim(filename)//" does not exist" )
            endif
         enddo
         deallocate (input1d, input3d)
      endif
   endif

#ifdef exclude_p1x
   extp1 = 0.0
   chl_p1c = 0.0  ! g Chl (mol C)^(-1)
   where(ixistZ==1) q_chl_p1c(:,:,:) = 0. ! g Chl (mol C)^(-1)
   where(ixistZ==1) st(:,:,:,ip1c)   = 1.
   where(ixistZ==1) st(:,:,:,ip1n)   = 1.
   where(ixistZ==1) st(:,:,:,ip1p)   = 1.
   where(ixistZ==1) st(:,:,:,ip1s)   = 1.
#endif
#ifdef exclude_p2x
   extp2 = 0.0
   chl_p2c = 0.0  ! g Chl (mol C)^(-1)
   where(ixistZ==1) q_chl_p2c(:,:,:) = 0. ! g Chl (mol C)^(-1)
   where(ixistZ==1) st(:,:,:,ip2c)   = 1.
   where(ixistZ==1) st(:,:,:,ip2n)   = 1.
   where(ixistZ==1) st(:,:,:,ip2p)   = 1.
#endif
#ifdef exclude_p3x
   extp3 = 0.0
   chl_p3c = 0.0  ! g Chl (mol C)^(-1)
   where(ixistZ==1) q_chl_p3c(:,:,:) = 0. ! g Chl (mol C)^(-1)
   where(ixistZ==1) st(:,:,:,ip3c)   = 1.
   where(ixistZ==1) st(:,:,:,ip3n)   = 1.
   where(ixistZ==1) st(:,:,:,ip3p)   = 1.
   where(ixistZ==1) st(:,:,:,ip3k)   = 1.
#endif

#ifdef module_restoring
      ! adjust boundaries / restoring-cells
      call get_restoring(day_of_year,ierr)
      call do_restoring(day_of_year,ierr)
#endif

   if (ierr/=0) then
      write( error_message, '("[",i2.2,"] ERROR - init_biogeo_recom: ",i3)') myID,ierr
      call stop_ecoham(ierr, msg=error_message)
   end if
   return

   end subroutine init_biogeo

!=======================================================================
!
! !INTERFACE:
   subroutine update_biogeo(ierr)
#define SUBROUTINE_NAME 'update_biogeo_recom'
!
! !DESCRIPTION:
!
! !USES:
   use mod_hydro, only : temp
   use mod_meteo, only : extp_field
   implicit none
!
! !INPUT PARAMETERS:
   integer, intent(inout) :: ierr
!
! !LOCAL VARIABLES:
   integer  :: i, j, k, k0
!
!-------------------------------------------------------------------------
#include "call-trace.inc"

   !if (myID==master) call write_log_message(" UPDATING biogeo_recom ")

   select case (isw_dim) !jp
      case (3) !3D-mode
         ! update temperature effect factors
         where(ixistz==1) tfac_p1 = exp( alog(1.5)*((temp-10.)/10.) )   ! for diatoms
         where(ixistz==1) tfac_p2 = exp( alog(1.5)*((temp-10.)/10.) )   ! for flagellates
         where(ixistz==1) tfac_p3 = exp( alog(1.5)*((temp-10.)/10.) )   ! for coccos
         where(ixistz==1) tfac_z1 = exp( alog(1.0)*((temp-10.)/10.) )   ! for microzoo
         where(ixistz==1) tfac_z2 = exp( alog(1.0)*((temp-10.)/10.) )   ! for mesozoo
         where(ixistz==1) tfac    = one                                 ! for ecosystem
         ! @ CNP-REcoM
         !where(ixistz==1) tfac_p1 = exp( -Ar_p1 * ( one/(273.15+temp) - one/Tref )) ! for diatoms (Tref is given in [Kelvin]!)
         !where(ixistz==1) tfac_p2 = exp( -Ar_p2 * ( one/(273.15+temp) - one/Tref )) ! for flagell (Tref is given in [Kelvin]!)
         !where(ixistz==1) tfac_p3 = exp( -Ar_p3 * ( one/(273.15+temp) - one/Tref )) ! for coccos  (Tref is given in [Kelvin]!)
         ! update derived variables ..
         where(ixistz==1) de(:,:,:,iz1n) = st(:,:,:,iz1c)/rcnz1         ! micro-zooplankton (N)
         where(ixistz==1) de(:,:,:,iz1p) = st(:,:,:,iz1c)/rcpz1         ! micro-zooplankton (P)
         where(ixistz==1) de(:,:,:,iz2n) = st(:,:,:,iz2c)/rcnz2         ! meso-zooplankton (N)
         where(ixistz==1) de(:,:,:,iz2p) = st(:,:,:,iz2c)/rcpz2         ! meso-zooplankton (P)
         where(ixistz==1) de(:,:,:,iban) = st(:,:,:,ibac)/rcnb          ! bacteria (N)
         where(ixistz==1) de(:,:,:,ibap) = st(:,:,:,ibac)/rcpb          ! bacteria (P)
#ifdef fixed_CHL_to_C
         where(ixistz==1) st(:,:,:,ip1a) = st(:,:,:,ip1c)*q_chl_p1c     ! diatoms (Chl)
         where(ixistz==1) st(:,:,:,ip2a) = st(:,:,:,ip2c)*q_chl_p2c     ! flagellates (Chl)
         where(ixistz==1) st(:,:,:,ip3a) = st(:,:,:,ip3c)*q_chl_p3c     ! coccos (Chl)
#endif
         select case(extp_feedback)
            case (1)     ! 1 - light extinction based on dynamic Chla
               where(ixistz==1) extp_field = extp*(st(:,:,:,ip1a) + st(:,:,:,ip2a) + st(:,:,:,ip3a))
            case default ! 0 - light extinction based on carbon biomass and constant Chla:C ratios
               where(ixistz==1) extp_field = extp*(  st(:,:,:,ip1c)*chl_p1c   &
                                                   + st(:,:,:,ip2c)*chl_p2c   &
                                                   + st(:,:,:,ip3c)*chl_p3c)
         end select
#ifdef extp_d093
         ! light extinction based on nitrogen biomass and species-specific extinction coefficients (fasham)
         where(ixistz==1) extp_field = extp1*st(:,:,:,ip1n) + extp2*st(:,:,:,ip2n) + extp3*st(:,:,:,ip3n)
#endif

      case (1) !1D-mode
         i = iStartCalc
         j = jStartCalc
         k0 = k_index(j,i)
         do k = 1, k0
            ! update temperature effect factors
            ! @ ecoham
            tfac_p1(j,k,i) = exp( alog(1.5)*((temp(j,k,i)-10.)/10.) )   ! for diatoms
            tfac_p2(j,k,i) = exp( alog(1.5)*((temp(j,k,i)-10.)/10.) )   ! for flagellates
            tfac_p3(j,k,i) = exp( alog(1.5)*((temp(j,k,i)-10.)/10.) )   ! for coccos
            tfac_z1(j,k,i) = exp( alog(1.0)*((temp(j,k,i)-10.)/10.) )   ! for microzoo
            tfac_z2(j,k,i) = exp( alog(1.0)*((temp(j,k,i)-10.)/10.) )   ! for mesozoo
            tfac(j,k,i)    = one                                        ! for ecosystem
            ! @ CNP-REcoM
            !tfac_p1(j,k,i) = exp( -Ar_p1 * ( one/(273.15+temp(j,k,i)) - one/Tref )) ! for diatoms (Tref is given in [Kelvin]!)
            !tfac_p2(j,k,i) = exp( -Ar_p2 * ( one/(273.15+temp(j,k,i)) - one/Tref )) ! for flagell (Tref is given in [Kelvin]!)
            !tfac_p3(j,k,i) = exp( -Ar_p3 * ( one/(273.15+temp(j,k,i)) - one/Tref )) ! for coccos  (Tref is given in [Kelvin]!)
            ! update derived variables ..
            de(j,k,i,iz1n) = st(j,k,i,iz1c)/rcnz1                       ! micro-zooplankton (N)
            de(j,k,i,iz1p) = st(j,k,i,iz1c)/rcpz1                       ! micro-zooplankton (P)
            de(j,k,i,iz2n) = st(j,k,i,iz2c)/rcnz2                       ! meso-zooplankton (N)
            de(j,k,i,iz2p) = st(j,k,i,iz2c)/rcpz2                       ! meso-zooplankton (P)
            de(j,k,i,iban) = st(j,k,i,ibac)/rcnb                        ! bacteria (N)
            de(j,k,i,ibap) = st(j,k,i,ibac)/rcpb                        ! bacteria (P)
#ifdef fixed_CHL_to_C
            st(j,k,i,ip1a) = st(j,k,i,ip1c)*q_chl_p1c(j,k,i)            ! diatoms (Chl)
            st(j,k,i,ip2a) = st(j,k,i,ip2c)*q_chl_p2c(j,k,i)            ! flagellates (Chl)
            st(j,k,i,ip3a) = st(j,k,i,ip3c)*q_chl_p3c(j,k,i)            ! coccos (Chl)
#endif
            select case(extp_feedback)
               case (1)     ! 1 - light extinction based on dynamic Chla
                  extp_field(j,k,i) = extp*(st(j,k,i,ip1a) + st(j,k,i,ip2a) + st(j,k,i,ip3a))
               case default ! 0 - light extinction based on carbon biomass and constant Chla:C ratios
                  extp_field(j,k,i) = extp*(  st(j,k,i,ip1c)*chl_p1c   &
                                            + st(j,k,i,ip2c)*chl_p2c   &
                                            + st(j,k,i,ip3c)*chl_p3c)
            end select
#ifdef extp_d093
            ! light extinction based on nitrogen biomass and species-specific extinction coefficients (fasham)
            extp_field(j,k,i) = extp1*st(j,k,i,ip1n) + extp2*st(j,k,i,ip2n) + extp3*st(j,k,i,ip3n)
#endif
         enddo
   end select

   if (ierr/=0) then
      write( error_message, '("[",i2.2,"] ERROR - update_biogeo_recom: ",i3)') myID,ierr
      call stop_ecoham(ierr, msg=error_message)
   end if
   return

   end subroutine update_biogeo

!=======================================================================
!
! !INTERFACE:
   subroutine do_biogeo(ierr)
#define SUBROUTINE_NAME 'do_biogeo_recom'
!
! !DESCRIPTION:
!
! !USES:
   use mod_hydro,  only : temp
   use mod_meteo,  only : par, pafr, radsol, Iopt, Iopt_min, Iopt_max ! only needed fo ecoham
#ifdef CLOERN_RATIO
   use mod_meteo,  only : par_mean
#endif
#ifdef module_chemie
   use mod_chemie
#endif
#if defined module_restoring && defined exclude_restoring_cells_biogeo
   use mod_restoring, only: k_index_exclude_restoring
#endif
   implicit none
!
! !INPUT PARAMETERS:
   integer, intent(inout) :: ierr
!
! !LOCAL VARIABLES:
   real :: f_FromTo(max_flux)  !!! (mol m-3 d-1)!!
! NOTE: Fluxes are first calculated as "change-in-concentration" (e.g. mol m-3 d-1) locally (grid cell)
! and are stored in a local dummy array f_FromTo. Bevore applying to sst, these have to be transfered
! by vertical integration (*dz) to the public array f_from_to, which is defined as (mol m-2).
!
   real, parameter :: tres_n1p= 1.0e-6 ! treshold value for uptake of phosphate by bacteria
   real, parameter :: tres_n4n= 1.0e-6 ! treshold value for uptake of ammonium by bacteria
   real, parameter :: tres_pXn= 1.0e-6 ! treshold value for phytoplankton losses and mortality
!
   real :: p1a, p1c, p1n, p1p, p1s
   real :: p2a, p2c, p2n, p2p
   real :: p3a, p3c, p3n, p3p, p3k
   real :: z1c, z1n, z1p, z2c, z2n, z2p
   real :: d1c, d1n, d1p, d2c, d2n, d2p, d2s, d2k
   real :: soc, doc, don, dop, bac, ban, bap
   real :: n3n, n4n, n1p, n5s, tem
   real :: tres_n4n, tres_n1p
   real :: eps, fy
   ! used for ecoham:
   !real :: limd_n, limd_np, limf_n, limf_np, lim_p, lim_s, lim_nps
   !real :: x21, x22, rcnd1, rcpd1, rcndo, rcpdo, q11, q12, q21, q22
   !real :: ploss1, ploss2, nut_lim
   ! used for CNP-REcoM:
   real :: q_p1c_p1n, q_p1c_p1p, q_p1n_p1c, q_p1p_p1n, q_p1a_p1c, q_p1a_p1n, q_p1s_p1n
   real :: q_p2c_p2n, q_p2c_p2p, q_p2n_p2c, q_p2p_p2n, q_p2a_p2c, q_p2a_p2n
   real :: q_p3c_p3n, q_p3c_p3p, q_p3n_p3c, q_p3p_p3n, q_p3a_p3c, q_p3a_p3n, q_p3c_p3k
   real :: q_d1c_d1n, q_d1c_d1p, q_d2c_d2n, q_d2c_d2p, q_doc_don, q_doc_dop
   real :: q_bac_ban, q_bac_bap, q_bap_ban, q_d1p_d1n, q_d2p_d2n
   real :: q_z1c_z1n, q_z1c_z1p, q_z1p_z1n, q_z2c_z2n, q_z2c_z2p, q_z2p_z2n
   real :: tk, par_, aggregation_soc, bac_loss
#if !defined exclude_p1x || !defined exclude_p2x || !defined exclude_p3x
   real :: limN, limP,  mort, f_IR, f_RCN, f_RNC, f_RPC, f_RPN, f_RNP
   real :: Tf, frac_d2n_, x1, x2, q2 !, q1
#endif
#ifndef exclude_p1x
   logical :: limN_liebig=.true. ! .false. does not properly work jet!!!
   real :: f_dic_p1c_red, f_din_p1n_netto, frac_p1c_pcho, res_p1
   real :: p1c_phot, p1n_assim, p1p_assim, p1a_synth
   real :: p1c_loss, p1n_loss, p1p_loss, decay_p1a
   real :: limS, f_RSN, f_RNS, dia_ups, dia_adds_loss
#endif
#ifndef exclude_p2x
   real :: f_dic_p2c_red, f_din_p2n_netto, frac_p2c_pcho, res_p2
   real :: p2c_phot, p2n_assim, p2p_assim, p2a_synth
   real :: p2c_loss, p2n_loss, p2p_loss, decay_p2a
#endif
#ifndef exclude_p3x
   real :: f_dic_p3c_red, f_din_p3n_netto, frac_p3c_pcho, res_p3
   !real :: plossk, rccalc_neu
   !logical :: dop_uptake
   ! used for CNP-REcoM:
   real :: p3c_phot, p3n_assim, p3p_assim, p3a_synth
   real :: p3c_loss, p3n_loss, p3p_loss, decay_p3a, limK
#endif
#if !defined exclude_z1x || !defined exclude_z2x
   real :: pnenn, grnenn, zloss, mu_max, food
   real :: y1, y2, y3, y4, y5, y6
#endif
#ifndef exclude_z1x
   real :: g1_p1n, g1_p2n, g1_p3n, g1_d1n, g1_ban
   real :: z1c_food, z1n_food, z1p_food
   real :: z1c_faec, z1n_faec, z1p_faec
   real :: z1c_loss, z1n_loss, z1p_loss
   real :: z1c_mort, z1n_mort, z1p_mort
   real :: z1n_pred, fzc1, fzn1, fzp1
#endif
#ifndef exclude_z2x
   real :: g2_p1n, g2_p2n, g2_p3n, g2_d1n, g2_ban, g2_z1n
   real :: z2c_food, z2n_food, z2p_food
   real :: z2c_faec, z2n_faec, z2p_faec
   real :: z2c_loss, z2n_loss, z2p_loss
   real :: z2c_mort, z2n_mort, z2p_mort
   real :: z2n_pred, fzc2, fzn2, fzp2
#endif
#ifndef exclude_bac
   real :: fbc, fbn, fbpdiff, fbpmax, fbpreq, fdc_diff
#endif
   integer :: i, j, k, k0, n
!
!-------------------------------------------------------------------------
#include "call-trace.inc"

! #ifdef exclude_bac
!    ! hard coded remineralization rates used if bacteria are switched off (-Dexclude_bac)
!    rhoC = 0.372
!    rhoN = 0.240
!    rhoP = 0.240
! #endif

   eps      = 1.0e-6
   tres_n4n = 0.1    ! treshold value for uptake of ammonium by bacteria
   tres_n1p = 0.1    ! treshold value for uptake of phosphate by bacteria

#if !defined exclude_z1x || !defined exclude_z2x
   y1=0.0;y2=0.0;y3=0.0;y4=0.0;y5=0.0;y6=0.0; ! to get rid of "unused" warning
#endif

   ! determination of oxic-anoxic condition
   oswtch = 1
   where (st(:,:,iStartD:iEndD,io2o) <= 0.)  oswtch(:,:,iStartD:iEndD) = 0
   nswtch = 1
   where (st(:,:,iStartD:iEndD,in3n) <= 0.1) nswtch(:,:,iStartD:iEndD) = 0

   do i = iStartCalc, iEndCalc
      do j = jStartCalc, jEndCalc
         k0 = k_index(j,i)
#if defined module_restoring && defined exclude_restoring_cells_biogeo
         ! filter for any statevariable will be x1x mask !!
         k0 = k_index_exclude_restoring(j,i,ix1x)
#else
         k0 = k_index(j,i)
#endif
         do k = 1, k0
            ! write index string (usefull for debugging!)
            write(str_jki, fmt_jki) i, j, k

            ! initialize /reset local f_FromTo
            f_FromTo = zero

            ! extract current gridpoint values
            tem = temp(j,k,i)
            tk  = real(273.15d0)+tem
            par_= par(j,k,i)
            p1a = st(j,k,i,ip1a)
            p1c = st(j,k,i,ip1c)
            p1n = st(j,k,i,ip1n)
            p1p = st(j,k,i,ip1p)
            p1s = st(j,k,i,ip1s)
            p2a = st(j,k,i,ip2a)
            p2c = st(j,k,i,ip2c)
            p2n = st(j,k,i,ip2n)
            p2p = st(j,k,i,ip2p)
            p3a = st(j,k,i,ip3a)
            p3c = st(j,k,i,ip3c)
            p3n = st(j,k,i,ip3n)
            p3p = st(j,k,i,ip3p)
            p3k = st(j,k,i,ip3k)
            z1c = st(j,k,i,iz1c)
            z1n = st(j,k,i,iz1c)/rcnz1
            z1p = st(j,k,i,iz1c)/rcpz1
            z2c = st(j,k,i,iz2c)
            z2n = st(j,k,i,iz2c)/rcnz2
            z2p = st(j,k,i,iz2c)/rcpz2
            bac = st(j,k,i,ibac)
            ban = st(j,k,i,ibac)/rcnb
            bap = st(j,k,i,ibac)/rcpb
            d1c = st(j,k,i,id1c)
            d1n = st(j,k,i,id1n)
            d1p = st(j,k,i,id1p)
            d2c = st(j,k,i,id2c)
            d2n = st(j,k,i,id2n)
            d2p = st(j,k,i,id2p)
            d2s = st(j,k,i,id2s)
            d2k = st(j,k,i,id2k)
            soc = st(j,k,i,isoc)
            doc = st(j,k,i,idoc)
            don = st(j,k,i,idon)
            dop = st(j,k,i,idop)
            n3n = st(j,k,i,in3n)
            n4n = st(j,k,i,in4n)
            n1p = st(j,k,i,in1p)
            n5s = st(j,k,i,in5s)
#ifdef exclude_p1x
            p1c = 1.0
            p1n = 1.0
            p1p = 1.0
            p1s = 1.0
#endif
#ifdef exclude_p2x
            p2c = 1.0
            p2n = 1.0
            p2p = 1.0
#endif
#ifdef exclude_p3x
            p3c = 1.0
            p3n = 1.0
            p3p = 1.0
            p3k = p3c/q_c_cal
#endif

            q_p1c_p1n = p1c/p1n    !  C/N   p1 (diatoms)
            q_p1c_p1p = p1c/p1p    !  C/P   p1 (diatoms)
            q_p1n_p1c = p1n/p1c    !  N/C   p1 (diatoms)
            q_p1p_p1n = p1p/p1n    !  P/N   p1 (diatoms)
            q_p1s_p1n = p1s/p1n    !  S/N   p1 (diatoms)
            q_p1a_p1n = p1a/p1n    !  Chl/N p1 (diatoms)
            q_p1a_p1c = p1a/p1c    !  Chl/C p1 (diatoms)

            q_p2c_p2n = p2c/p2n    !  C/N   p2 (flagellates)
            q_p2c_p2p = p2c/p2p    !  C/P   p2 (flagellates)
            q_p2n_p2c = p2n/p2c    !  N/C   p2 (flagellates)
            q_p2p_p2n = p2p/p2n    !  P/N   p2 (flagellates)
            q_p2a_p2n = p2a/p2n    !  Chl/N p2 (flagellates)
            q_p2a_p2c = p2a/p2c    !  Chl/C p2 (flagellates)

            q_p3c_p3n = p3c/p3n    !  C/N   p3 (e.g. cocos, phaeocystis)
            q_p3c_p3p = p3c/p3p    !  C/P   p3 (e.g. cocos, phaeocystis)
            q_p3n_p3c = p3n/p3c    !  N/C   p3 (e.g. cocos, phaeocystis)
            q_p3p_p3n = p3p/p3n    !  P/N   p3 (e.g. cocos, phaeocystis)
            q_p3a_p3n = p3a/p3n    !  Chl/N p3 (e.g. cocos, phaeocystis)
            q_p3a_p3c = p3a/p3c    !  Chl/C p3 (e.g. cocos, phaeocystis)
            q_p3c_p3k = p3c/p3k    !  C/CaCo3 p3 (e.g. cocos, phaeocystis)

            q_bac_ban = rcnb       !  C/N Bacteria !p1c/p1n
            q_bac_bap = rcpb       !  C/P Bacteria !p1c/p1p
            q_bap_ban = rcnb/rcpb  !  P/N Bacteria !p1p/p1n

            q_z1c_z1n = rcnz1      !  C/N Micro-zooplankton !z1c/z1n
            q_z1c_z1p = rcpz1      !  C/P Micro-zooplankton !z1c/z1p
            q_z1p_z1n = rcnz1/rcpz1 ! P/N Micro-zooplankton !z1p/z1n
            q_z2c_z2n = rcnz2      !  C/N Meso-zooplankton  !z2c/z2n
            q_z2c_z2p = rcpz2      !  C/P Meso-zooplankton  !z2c/z2p
            q_z2p_z2n = rcnz2/rcpz2 ! P/N Meso-zooplankton  !z2p/z2n

            q_d1c_d1n = d1c/d1n    !  C/N Detritus (slow sinking)
            q_d1c_d1p = d1c/d1p    !  C/P Detritus (slow sinking)
            q_d1p_d1n = d1p/d1n    !  P/N Detritus (slow sinking)
            q_d2c_d2n = d2c/d2n    !  C/N Detritus (fast sinking)
            q_d2c_d2p = d2c/d2p    !  C/P Detritus (fast sinking)
            q_d2p_d2n = d2p/d2n    !  P/N Detritus (fast sinking)
            q_doc_don = doc/don    !  C/N DOM
            q_doc_dop = doc/dop    !  C/P DOM

            ! light limitation factor (@ECOHAM
            fPAR(j,k,i) = par_/Iopt(j,k,i)*exp(one-par_/Iopt(j,k,i))

! print*,'#755 fPar', par_/Iopt(j,k,i)*exp(one-par_/Iopt(j,k,i)), &
!                     par_/Iopt_min*exp(one-par_/Iopt_min), &
!                     par_/Iopt_max*exp(one-par_/Iopt_max)
            ! light limitation factor
            fPAR(j,k,i) = par(j,k,i)/Iopt(j,k,i)*exp(one-par(j,k,i)/Iopt(j,k,i))

#ifdef exclude_p1x
            !================================================================================
            ! PHYTOPLANKTON-p1* (switched-off)
            !================================================================================
            ! set preferences for zooplankton grazing (IMPORTANT, because p1-biomass may be different from zero!)
            p1_p1n = 0.
            p2_p1n = 0.
#else
            !================================================================================
            ! PHYTOPLANKTON-p1*  (DIATOMS)
            !================================================================================
            ! check whether cell-quota are within valid range
            call check_quota(p1c, p1n, p1p, p1s, 'diatoms', trim(str_jki), ierr)

            ! calculate some rates:
            ! limitation due to external nutrient concentrations
!             if (.false.) then
!             x1 = n3n/k_p1_n3n !xk1
!             x2 = n4n/k_p1_n4n !xk21
!             if (n3n .lt. TINY) x1 = zero
!             if (n4n .lt. TINY) x2 = zero
!             limN = (x1+x2) / (one+x1+x2)             ! nitrogen limitation
!             q2 = min(one, max(zero, x2  / (x1+x2)))
!             else
!             limN = (n3n+n4n)/(k_p1_n3n+n3n+n4n)
!             q2 = n4n/(k_p1_n4n+n4n)
!             if (n3n .lt. TINY)  q2 = one
!             end
!             limP = n1p/(k_p1_n1p + n1p) !xkp         ! phosphorous limitation
!             limS = n5s/(k_p1_n5s + n5s) !xks         ! silicon limitation
!             if ( n1p .lt. TINY+TINY) limP = zero
!             if ( n5s .lt. TINY+TINY) limS = zero

            x1 = n3n/k_p1_n3n !xk1
            !x2 = max(zero, (n4n -TINY)/k_p1_n4n) !xk21
            x2 = n4n/k_p1_n4n !xk21
            lim_p1_n3n(j,k,i) = x1/(one+x1+x2)
            lim_p1_n4n(j,k,i) = x2/(one+x1+x2)
            if (n3n .lt. Tiny) lim_p1_n3n(j,k,i) = zero
            if (n4n .lt. Tiny) lim_p1_n4n(j,k,i) = zero
            limN  = lim_p1_n3n(j,k,i) + lim_p1_n4n(j,k,i)
            limP = n1p/(k_p1_n1p + n1p) !xkp         ! phosphorous limitation
            limS = n5s/(k_p1_n5s + n5s) !xks         ! silicon limitation
            if (n1p .lt. Tiny) limP = zero
            if (n5s .lt. Tiny) limS = zero
            if (limN_liebig) limN = min(limN, limS) ! Liebig's law for N-uptake in case of silicate limitation
!             q1 = lim_p1_n3n(j,k,i)*limN/(lim_p1_n3n(j,k,i)+lim_p1_n4n(j,k,i)+eps)
!             q2 = lim_p1_n4n(j,k,i)*limN/(lim_p1_n3n(j,k,i)+lim_p1_n4n(j,k,i)+eps)
! print*,'#786',n3n,n4n,limN, limS, q1,q2,q1+q2,lim_p1_n3n(j,k,i),lim_p1_n4n(j,k,i)
            q2 = min(one, max(zero, lim_p1_n4n(j,k,i) / ( lim_p1_n3n(j,k,i) + lim_p1_n4n(j,k,i) + eps ) ))

            ! - nutrient uptake:
! if (k==1) then
! print*,'#702',n3n,n4n,n5s,k_p1_n3n,k_p1_n3n,k_p1_n5s
! print*,'#703',limN,limS,min(limN, limS),q2,x1,x2
! endif

#ifdef debug_CNP_function
print*,'########### call uptake_recomCNP (in):  ###########'
write(*,'(''IR, mu_max, f_T, limN, limP       :'',5e20.10)') par(j,k,i), mu_p1_max, tfac_p1(j,k,i), limN, limP
write(*,'(''qNC, QNCuptake, QNCmin, QNCmax    :'',5e20.10)') q_p1n_p1c, Qupt_din_p1c, Qmin_p1n_p1c, Qmax_p1n_p1c
!write(*,'(''qPC, QPCmax                       :'',5e20.10)') q_p1p_p1c, Qmax_p1p_p1c
write(*,'(''qPN, QPNuptake,QPNmin, QPNmax     :'',5e20.10)') q_p1p_p1n, Qupt_dip_p1n, Qmin_p1p_p1n, Qmax_p1p_p1n
write(*,'(''alpha, theta, theta_max           :'',5e20.10)') alpha_p1, q_p1a_p1n, Qmax_p1a_p1n
write(*,'(''sigmaNC, sigmaNP, sigmaPC, sigmaPN:'',5e20.10)') sigma_NC, sigma_NP, sigma_PC, sigma_PN
#endif
            call uptake_recomCNP(par_, mu_p1_max, tfac_p1(j,k,i), limN, limP,       &
                           q_p1n_p1c, Qupt_din_p1c, Qmin_p1n_p1c, Qmax_p1n_p1c,     &
                           q_p1p_p1n, Qupt_dip_p1n, Qmin_p1p_p1n, Qmax_p1p_p1n,     &
                           alpha_p1, q_p1a_p1n, Qmax_p1a_p1n,                       &
                           p1c_phot, p1n_assim, p1p_assim, p1a_synth,               &
                           f_IR, f_RCN, f_RNC, f_RPC, f_RPN, f_RNP, ierr)
#ifdef debug_CNP_function
print*,'########### call uptake_recomCNP (out):  ###########'
write(*,'(''p1c_phot,p1n_assim,p1p_assim,p1a_synth  :'',5e20.10)') p1c_phot, p1n_assim, p1p_assim, p1a_synth
write(*,'(''f_IR,f_RNC,f_RPC,f_RPN,f_RNP            :'',5e20.10)') f_IR, f_RNC, f_RPC, f_RPN, f_RNP
#endif
! write(*,'(''fPar, f_IR            :'',5e20.10)') fPAR(j,k,i), f_IR, Iopt(j,k,i)

            ! - maintenance respiration / leakage of inorganic matter
            res_p1 = r0_p1*tfac_p1(j,k,i)

            ! - calculate net N-uptake
            f_din_p1n_netto = p1n_assim *p1c - res_p1 *p1n                     ! net DIN uptake

            !----------------------------------------------------------------------------------------
            ! diatoms silicon uptake
            !----------------------------------------------------------------------------------------
            ! @REcoM2
            ! max uptake Si:C = 0.2
            ! p1s_assim = Vc * mu_c_max * ft * q_upt_p1s_p1c * RSiC * RNC * limS
            !----------------------------------------------------------------------------------------
            ! regulation of S-uptake according to cellular quota and/or external resources
            if (q_p1s_p1n > Qmin_p1s_p1n) then
               f_RSN = max(zero, one - ((q_p1s_p1n-Qmin_p1s_p1n)/(Qmax_p1s_p1n-Qmin_p1s_p1n))**2.);
            else
               f_RSN = one
            endif

            if (limN_liebig) then
               ! skeleton silicon binding (fixed N:Si-ratio)
               f_FromTo(i_n5s_p1s) = Qupt_dis_p1n *f_din_p1n_netto *f_RSN
               ! N-uptake was already reduced due to Liebig's law: min(limN, limS)
               f_RNS = one
            else
               ! skeleton silicon binding related to N-uptake rate and cell stoichiometry
               f_FromTo(i_n5s_p1s) = Qupt_dis_p1n *p1n_assim *f_RSN *limS
               ! N-uptake (down-)regulated according Si:N cell quota
               f_RNS = max( one, q_p1s_p1n/Qmin_p1s_p1n )
               p1n_assim = p1n_assim * f_RNS
               p1a_synth = p1a_synth * f_RNS
               ! re-calculate net N-uptake
               f_din_p1n_netto = p1n_assim *p1c - res_p1 *p1n
            endif

            ! net primary production (source terms)
            ! @ e.g. ecoham
            !f_FromTo(i_dic_p1c) = p1c*tfac_p1(j,k,i)*vp1*fPAR(j,k,i)*nut_lim                   ! primary production
            !f_dic_p1c_red       = p1c*tfac_p1(j,k,i)*vp1*fPAR(j,k,i)*lim_nps
            !f_FromTo(i_dic_p1c) = f_dic_p1c_red+excess*(f_FromTo(i_dic_p1c)-f_dic_p1c_red)
            ! @ CNP-REcoM
            f_FromTo(i_dic_p1c) = (p1c_phot - zeta_nit*p1n_assim - res_p1) *p1c    ! net DIC uptake
            f_dic_p1c_red   = f_din_p1n_netto *rcn                                 ! net redfield DIC uptake
            !f_FromTo(i_n3n_p1n) = p1n_assim *p1c *(one-q2)                        ! net n3n uptake
            !f_FromTo(i_n4n_p1n) = p1n_assim *p1c *(    q2) - res_p1 *p1n          ! net n4n uptake
            if (f_din_p1n_netto > zero) then
               f_FromTo(i_n3n_p1n) = f_din_p1n_netto *(one-q2)                     ! net n3n uptake
               f_FromTo(i_n4n_p1n) = f_din_p1n_netto *(    q2)                     ! net n4n uptake
            else ! in case of negative net uptake, lysis and excretion is assumed as n4n only
               f_FromTo(i_n4n_p1n) = f_din_p1n_netto                               ! n4n loss
            endif
            f_FromTo(i_n1p_p1p) = p1p_assim *p1n - res_p1 *p1p                     ! net n1p uptake

            ! write limitation factors to global arrays (for output)
            lim_p1_n1p(j,k,i) = limP
            lim_p1_n5s(j,k,i) = limS
            lim_p1_IR (j,k,i) = f_IR
            lim_p1_RCN(j,k,i) = f_RCN
            lim_p1_RNC(j,k,i) = f_RNC
            lim_p1_RPC(j,k,i) = f_RPC
            lim_p1_RPN(j,k,i) = f_RPN
            lim_p1_RNP(j,k,i) = f_RNP
            lim_p1_RSN(j,k,i) = f_RSN
            lim_p1_RNS(j,k,i) = f_RNS


! if (f_FromTo(i_n4n_p1n) < 0.) then
! print*, f_FromTo(i_n4n_p1n), p1n_assim, q2, r0_p1, tfac_p1(j,k,i), -res_p1*p1n
! endif
            ! losses (sink terms)
            ! - excretion / leakage
            Tf = one
            p1c_loss = Tf* loss_p1c
            p1n_loss = Tf* loss_p1n
            p1p_loss = Tf* loss_p1p
            if ( q_p1n_p1c > Qmax_p1n_p1c ) p1c_loss = p1n_loss
            !if (q_p1p_p1n .lt. Qmin_p1p_p1n) p1p_loss = p1n_loss
            if (iexcess == 1) then
               ! @ ecoham
               !f_FromTo(i_p1c_soc) = f_FromTo(i_dic_p1c)-f_dic_p1c_red
               !f_FromTo(i_p1c_doc) = gam1*f_dic_p1c_red
               ! @ CNP-REcoM
               frac_p1c_pcho = max(zero,f_pcho_max *(one-(q_p1n_p1c/Qmax_p1n_p1c)))
               f_FromTo(i_p1c_soc) = p1c_loss *(one-frac_p1c_pcho) *p1c
               f_FromTo(i_p1c_doc) = p1c_loss *(    frac_p1c_pcho) *p1c
            else
               f_FromTo(i_p1c_soc) = zero
               f_FromTo(i_p1c_doc) = p1c_loss *p1c
            endif
            f_FromTo(i_p1n_don) = p1n_loss *p1n
            f_FromTo(i_p1p_dop) = p1p_loss *p1p
            ! - chlorophyll decay - assuming relaxation towards Qmax_p1a_p1n
            decay_p1a = deg_p1a                                                   ! [s^-1]??
            if (q_p1a_p1n .gt. Qmax_p1a_p1n) then
               decay_p1a = res_p1 + p1n_loss
            endif

            ! - natural mortality (without predation)
            mort = tfac(j,k,i)*xmu11 + xmq11*p1c         !@ ecoham
            frac_d2n_ = frac_d2x                         !@ ecoham
            !mort = agg_p1n*p1n + agg_det*(d1n+d2n)       !@ CNP-REcoM
            !frac_d2n_ = d2n/(d1n+d2n)                    !@ CNP-REcoM
            if (p1n.lt.eps) mort  = zero ! treshold for mortality niche
            f_FromTo(i_p1c_d1c) = mort*p1c*(one-frac_d2n_)
            f_FromTo(i_p1n_d1n) = mort*p1n*(one-frac_d2n_)
            f_FromTo(i_p1p_d1p) = mort*p1p*(one-frac_d2n_)
            f_FromTo(i_p1c_d2c) = mort*p1c*     frac_d2n_
            f_FromTo(i_p1n_d2n) = mort*p1n*     frac_d2n_
            f_FromTo(i_p1p_d2p) = mort*p1p*     frac_d2n_
            f_FromTo(i_p1s_d2s) = mort*p1s !p1c/rcs !sollte man p1s_d2s als proxi für die Aufteilung zu d1*/d2* nehmen?
            if (p1s.lt.eps) f_FromTo(i_p1s_d2s) = zero ! treshold for mortality niche
#ifdef CLOERN_RATIO
            ! calculate diatom chl:C ratio after Cloern et al. 1995 (cl)
            !q_chl_p1c(j,k,i) = (0.003 + max(0.,0.0154*exp(0.05*bioT)   &
            !                    *exp(-0.059*par_mean(j,k,i))*lim_nps))*12.0 ! g Chl (mol C)^(-1)
            q_chl_p1c(j,k,i) = (0.003 + max(0.,0.0154*exp(0.05*tem)   &
                                *exp(-0.059*par_mean(j,k,i))*limN))*12.0 ! g Chl (mol C)^(-1)
#else
            q_chl_p1c(j,k,i) = q_p1a_p1c
#endif  /* #ifdef CLOERN_RATIO */
#endif  /* #ifdef exclude_p1x */

#ifdef exclude_p2x
            !================================================================================
            ! PHYTOPLANKTON-p2* (switched-off)
            !================================================================================
            ! set preferences for zooplankton grazing (IMPORTANT, because p2-biomass may be different from zero!)
            p1_p2n = 0.
            p2_p2n = 0.
#else
            !================================================================================
            ! PHYTOPLANKTON-p2* (FLAGGELATES)
            !================================================================================
            ! check whether cell-quota are within valid range
            call check_quota(p2c, p2n, p2p, p2n, 'flagellates', trim(str_jki), ierr)

            ! calculate some rates:
            ! limitation due to external nutrient concentrations
!             x1 = n3n/k_p2_n3n !xk1
!             x2 = n4n/k_p2_n4n !xk22
!             if (n3n .lt. TINY+TINY) x1 = zero
!             if (n4n .lt. TINY+TINY) x2 = zero
!             q2 = one
!             if (x1 > zero) q2 = min(one, max(zero, x2  / (one+x1+x2)))
!             limN = (x1+x2) / (one+x1+x2)             ! nitrogen limitation
!             limP = n1p/(k_p2_n1p + n1p) !xkp         ! phosphorous limitation
!             if ( n1p .lt. TINY+TINY) limP = zero
            x1 = n3n/k_p2_n3n !xk1
            x2 = n4n/k_p2_n4n !xk21
            lim_p2_n3n(j,k,i) = x1/(one+x1+x2)
            lim_p2_n4n(j,k,i) = x2/(one+x1+x2)
            if (n3n .lt. TINY) lim_p2_n3n(j,k,i) = zero
            if (n4n .lt. TINY) lim_p2_n4n(j,k,i) = zero
            limN  = lim_p2_n3n(j,k,i) + lim_p2_n4n(j,k,i)
            limP = n1p/(k_p2_n1p + n1p) !xkp         ! phosphorous limitation
            if (n1p .lt. TINY) limP = zero
            q2 = min(one, max(zero, lim_p2_n4n(j,k,i) / ( lim_p2_n3n(j,k,i) + lim_p2_n4n(j,k,i) + eps ) ))

            ! - nutrient uptake:
            call uptake_recomCNP(par_, mu_p2_max, tfac_p2(j,k,i), limN, limP,       &
                           q_p2n_p2c, Qupt_din_p2c, Qmin_p2n_p2c, Qmax_p2n_p2c,     &
                           q_p2p_p2n, Qupt_dip_p2n, Qmin_p2p_p2n, Qmax_p2p_p2n,     &
                           alpha_p2, q_p2a_p2n, Qmax_p2a_p2n,                       &
                           p2c_phot, p2n_assim, p2p_assim, p2a_synth,               &
                           f_IR, f_RCN, f_RNC, f_RPC, f_RPN, f_RNP, ierr)

            ! - maintenance respiration / leakage of inorganic matter
            res_p2 = r0_p2*tfac_p2(j,k,i)

            ! - calculate net N-uptake
            f_din_p2n_netto = p2n_assim *p2c - res_p2 *p2n                     ! net DIN uptake

            ! net primary production (source terms)
            ! @ e.g. ecoham
            !f_FromTo(i_dic_p2c) = p2c*tfac_p2(j,k,i)*vp2*fPAR(j,k,i)*nut_lim                   ! primary production
            !f_dic_p2c_red       = p2c*tfac_p2(j,k,i)*vp2*fPAR(j,k,i)*limf_np
            !f_FromTo(i_dic_p2c) = f_dic_p2c_red+excess*(f_FromTo(i_dic_p2c)-f_dic_p2c_red)
            ! @ CNP-REcoM
            f_FromTo(i_dic_p2c) = (p2c_phot - zeta_nit*p2n_assim - res_p2) *p2c    ! net DIC uptake
            f_dic_p2c_red   = f_din_p2n_netto *rcn                                 ! net redfield DIC uptake
            !f_FromTo(i_n3n_p2n) = p2n_assim *p2c *(one-q2)                        ! net n3n uptake
            !f_FromTo(i_n4n_p2n) = p2n_assim *p2c *(    q2) - res_p2 *p2n          ! net n4n uptake
            if (f_din_p2n_netto > zero) then
               f_FromTo(i_n3n_p2n) = f_din_p2n_netto *(one-q2)                     ! net n3n uptake
               f_FromTo(i_n4n_p2n) = f_din_p2n_netto *(    q2)                     ! net n4n uptake
            else ! in case of negative net uptake, lysis and excretion is assumed as n4n only
               f_FromTo(i_n4n_p2n) = f_din_p2n_netto                               ! n4n loss
            endif
            f_FromTo(i_n1p_p2p) = p2p_assim *p2n - res_p2 *p2p                     ! net n1p uptake
            f_FromTo(i_dic_psk) = f_dic_p2c_red /q_c_cal                           ! skeleton carbonate building

            ! write limitation factors to global arrays (for output)
            lim_p2_n1p(j,k,i) = limP
            lim_p2_IR (j,k,i) = f_IR
            lim_p2_RCN(j,k,i) = f_RCN
            lim_p2_RNC(j,k,i) = f_RNC
            lim_p2_RPC(j,k,i) = f_RPC
            lim_p2_RPN(j,k,i) = f_RPN
            lim_p2_RNP(j,k,i) = f_RNP

            ! losses (sink terms)
            ! - excretion / leakage
            Tf = one
            p2c_loss = Tf* loss_p2c
            p2n_loss = Tf* loss_p2n
            p2p_loss = Tf* loss_p2p
            if ( q_p2n_p2c > Qmax_p2n_p2c ) p2c_loss = p2n_loss
            !if (q_p2p_p2n .lt. Qmin_p2p_p2n) p2p_loss = p2n_loss
            if (iexcess == 1) then
               ! @ ecoham
               f_FromTo(i_p2c_soc) = f_FromTo(i_dic_p2c)-f_dic_p2c_red
               f_FromTo(i_p2c_doc) = gam2*f_dic_p2c_red
               ! @ CNP-REcoM
               frac_p2c_pcho = max(zero,f_pcho_max *(one-(q_p2n_p2c/Qmax_p2n_p2c)))
               f_FromTo(i_p2c_soc) = p2c_loss *(one-frac_p2c_pcho) *p2c
               f_FromTo(i_p2c_doc) = p2c_loss *(    frac_p2c_pcho) *p2c
            else
               f_FromTo(i_p2c_soc) = zero
               f_FromTo(i_p2c_doc) = p2c_loss *p2c
            endif
            f_FromTo(i_p2n_don) = p2n_loss *p2n
            f_FromTo(i_p2p_dop) = p2p_loss *p2p
            ! - chlorophyll decay - assuming relaxation towards Qmax_p2a_p2n
            decay_p2a = deg_p2a                                                   ! [s^-1]??
            if (q_p2a_p2n .gt. Qmax_p2a_p2n) then
               decay_p2a = res_p2 + p2n_loss
            endif

            ! - natural mortality (without predation)
            mort = tfac(j,k,i)*xmu12 + xmq12*p2c          !@ ecoham
            frac_d2n_ = frac_d2x                          !@ ecoham
            !mort = agg_p2n*p2n + agg_det*(d1n+d2n)        !@ CNP-REcoM
            !frac_d2n_ = d2n/(d1n+d2n)                     !@ CNP-REcoM
            if (p2n.lt.eps) mort  = zero ! treshold for mortality niche
            f_FromTo(i_p2c_d1c) = mort*p2c*(one-frac_d2n_)
            f_FromTo(i_p2n_d1n) = mort*p2n*(one-frac_d2n_)
            f_FromTo(i_p2p_d1p) = mort*p2p*(one-frac_d2n_)
            f_FromTo(i_p2c_d2c) = mort*p2c*     frac_d2n_
            f_FromTo(i_p2n_d2n) = mort*p2n*     frac_d2n_
            f_FromTo(i_p2p_d2p) = mort*p2p*     frac_d2n_
#ifdef CLOERN_RATIO
            ! calculate flagellate chl:C ratio after Cloern et al. 1995 (cl)
            !q_chl_p2c(j,k,i) = (0.003 + max(0.,0.0154*exp(0.05*bioT)   &
            !                    *exp(-0.059*par_mean(j,k,i))*limf_np))*12.0 ! g Chl (mol C)^(-1)
            q_chl_p2c(j,k,i) = (0.003 + max(0.,0.0154*exp(0.05*tem)   &
                                *exp(-0.059*par_mean(j,k,i))*limN))*12.0 ! g Chl (mol C)^(-1)
#else
            q_chl_p2c(j,k,i) = q_p2a_p2c
#endif  /* #ifdef CLOERN_RATIO */
#endif  /* #ifdef exclude_p2x */

#ifdef exclude_p3x
            !================================================================================
            ! PHYTOPLANKTON-p3* (switched-off)
            !================================================================================
            ! set preferences for zooplankton grazing (IMPORTANT, because p3-biomass may be different from zero!)
            p1_p3n = 0.
            p2_p3n = 0.
#else
            !================================================================================
            ! PHYTOPLANKTON-p3* (COCCOLITHOPHORIDES)
            !================================================================================
            ! check whether cell-quota are within valid range
            call check_quota(p3c, p3n, p3p, p3n, 'phyto-3', trim(str_jki), ierr)

            ! calculate some rates:
            ! limitation due to external nutrient concentrations
            x1 = n3n/k_p3_n3n !xk13
            x2 = n4n/k_p3_n4n !xk23
            if (n3n .lt. TINY+TINY) x1 = zero
            if (n4n .lt. TINY+TINY) x2 = zero
            q2 = one
            if (x1 > zero) q2 = min(one, max(zero, x2  / (one+x1+x2)))
            limN = (x1+x2) / (one+x1+x2)             ! nitrogen limitation
            limP = n1p/(k_p3_n1p + n1p) !xkp3        ! phosphorous limitation
            if ( n1p .lt. TINY+TINY) limP = zero
            limK = zero                              ! calcite limitation
!mk: still to do ...
!             !IL-P3:Neu: if n1p is limiting p3c is able to use dop as P source
!             lim_p3_dop(j,k,i) = st(j,k,i,idop)/(xkp3+st(j,k,i,idop))
!             if((lim_p3_n1p(j,k,i)<=0.7).and.(lim_p3_dop(j,k,i)>=lim_p3_n1p(j,k,i))) dop_uptake=.true.  !0.7 chosen arbitrarily
!             if (dop_uptake) then
!                limc_p = lim_p3_dop(j,k,i)
!             else
!                limc_p = lim_p3_n1p(j,k,i)
!             endif
! #ifdef module_chemie
!             !IL-P3:Neu omega
!             omega(j,k,i) = ch(j,k,i,io3c)*ca(j,k,i)/aksp(j,k,i)
! #else
!             omega(j,k,i) = zero
! #endif
!             if (omega(j,k,i)>1.0) then !Die Gefahr bei omega =0.6 durch null zu teilen muss abgefangen werden
!                limK = ((omega(j,k,i)-1.0)/((omega(j,k,i)-1.0)+xkk))
!             endif
!mk: ... end

            ! - nutrient uptake:
            limN = min(limN, limK) ! Liebig's law for N-uptake in case of CaCo3 limitation
            call uptake_recomCNP(par_, mu_p3_max, tfac_p3(j,k,i), limN, limP,       &
                           q_p3n_p3c, Qupt_din_p3c, Qmin_p3n_p3c, Qmax_p3n_p3c,     &
                           q_p3p_p3n, Qupt_dip_p3n, Qmin_p3p_p3n, Qmax_p3p_p3n,     &
                           alpha_p3, q_p3a_p3n, Qmax_p3a_p3n,                       &
                           p3c_phot, p3n_assim, p3p_assim, p3a_synth,               &
                           f_IR, f_RCN, f_RNC, f_RPC, f_RPN, f_RNP, ierr)

            ! - maintenance respiration / leakage of inorganic matter
            res_p3 = r0_p3*tfac_p3(j,k,i)
            ! - calculate net N-uptake
            f_din_p3n_netto = p3n_assim *p3c - res_p3 *p3n                     ! net DIN uptake

            ! net primary production (source terms)
            ! @ e.g. ecoham
            !f_FromTo(i_dic_p3c) = p3c*tfac_p3(j,k,i)*vp3*fPAR(j,k,i)*nut_lim                   ! primary production
            !f_dic_p3c_red       = p3c*tfac_p3(j,k,i)*vp3*fPAR(j,k,i)*limf_np
            !f_FromTo(i_dic_p3c) = f_dic_p3c_red+excess*(f_FromTo(i_dic_p3c)-f_dic_p3c_red)
            ! @ CNP-REcoM
            f_FromTo(i_dic_p3c) = (p3c_phot - zeta_nit*p3n_assim - res_p3) *p3c    ! net DIC uptake
            f_dic_p3c_red   = f_din_p3n_netto *rcn                                 ! net redfield DIC uptake
            !f_FromTo(i_n3n_p3n) = p3n_assim *p3c *(one-q2)                        ! net n3n uptake
            !f_FromTo(i_n4n_p3n) = p3n_assim *p3c *(    q2) - res_p3 *p3n          ! net n4n uptake
            if (f_din_p3n_netto > zero) then
               f_FromTo(i_n3n_p3n) = f_din_p3n_netto *(one-q2)                     ! net n3n uptake
               f_FromTo(i_n4n_p3n) = f_din_p3n_netto *(    q2)                     ! net n4n uptake
            else ! in case of negative net uptake, lysis and excretion is assumed as n4n only
               f_FromTo(i_n4n_p3n) = f_din_p3n_netto                               ! n4n loss
            endif
            f_FromTo(i_n1p_p3p) = p3p_assim *p3n - res_p3 *p3p                     ! net n1p uptake

            ! write limitation factors to global arrays (for output)
            lim_p3_n3n(j,k,i) = limN *(one-q2)
            lim_p3_n4n(j,k,i) = limN *(    q2)
            lim_p3_n1p(j,k,i) = limP
            lim_p3_dop(j,k,i) = one
            lim_p3_IR (j,k,i) = f_IR
            lim_p3_RCN(j,k,i) = f_RCN
            lim_p3_RNC(j,k,i) = f_RNC
            lim_p3_RPC(j,k,i) = f_RPC
            lim_p3_RPN(j,k,i) = f_RPN
            lim_p3_RNP(j,k,i) = f_RNP

            ! losses (sink terms)
            ! - excretion / leakage
            Tf = one
            p3c_loss = Tf* loss_p3c
            p3n_loss = Tf* loss_p3n
            p3p_loss = Tf* loss_p3p
            if ( q_p3n_p3c > Qmax_p3n_p3c ) p3c_loss = p3n_loss
           !if (q_p3p_p3n .lt. Qmin_p3p_p3n) p3p_loss = p3n_loss
            if (iexcess == 1) then
               ! @ ecoham
               f_FromTo(i_p3c_soc) = f_FromTo(i_dic_p3c)-f_dic_p3c_red
               f_FromTo(i_p3c_doc) = gam2*f_dic_p3c_red
               ! @ CNP-REcoM
               frac_p3c_pcho = max(zero,f_pcho_max *(one-(q_p3n_p3c/Qmax_p3n_p3c)))
               f_FromTo(i_p3c_soc) = p3c_loss *(one-frac_p3c_pcho) *p3c
               f_FromTo(i_p3c_doc) = p3c_loss *(    frac_p3c_pcho) *p3c
            else
               f_FromTo(i_p3c_soc) = zero
               f_FromTo(i_p3c_doc) = p3c_loss *p3c
            endif
            f_FromTo(i_p3n_don) = p3n_loss *p3n
            f_FromTo(i_p3p_dop) = p3p_loss *p3p
            ! - chlorophyll decay - assuming relaxation towards Qmax_p3a_p3n
            decay_p3a = deg_p3a                                                   ! [s^-1]??
            if (q_p3a_p3n .gt. Qmax_p3a_p3n) then
               decay_p3a = res_p3 + p3n_loss
            endif
            ! - natural mortality (without predation)
            mort = tfac(j,k,i)*xmu13 + xmq13*p3c          !@ ecoham
            frac_d2n_ = frac_d2x                          !@ ecoham
            !mort = agg_p3n*p3n + agg_det*(d1n+d2n)        !@ CNP-REcoM
            !frac_d2n_ = d2n/(d1n+d2n)                     !@ CNP-REcoM
            if (p3n.lt.eps) mort  = zero ! treshold for mortality niche
            f_FromTo(i_p3c_d1c) = mort*p3c*(one-frac_d2n_)
            f_FromTo(i_p3n_d1n) = mort*p3n*(one-frac_d2n_)
            f_FromTo(i_p3p_d1p) = mort*p3p*(one-frac_d2n_)
            f_FromTo(i_p3c_d2c) = mort*p3c*     frac_d2n_
            f_FromTo(i_p3n_d2n) = mort*p3n*     frac_d2n_
            f_FromTo(i_p3p_d2p) = mort*p3p*     frac_d2n_

!mk: still to do ...
!             !if a maximum of coccoliths is reached, then all surplus coccospheres are dettached,
!             !else, only 10 % are dettached(Tyrell&Taylor)
!             rccalc_neu = (p3c +f_FromTo(i_dic_p3c) -f_FromTo(i_p3c_doc)         &
!                               -f_FromTo(i_p3c_soc) -f_FromTo(i_p3c_d1c)         &
!                               -f_FromTo(i_p3c_d2c))/(p3k+f_FromTo(i_dic_p3k))
!
!             if (rccalc_neu<rccalc_min) then
!                plossk = p3k +f_FromTo(i_dic_p3k) -((p3c+f_FromTo(i_dic_p3c)  &
!                             -f_FromTo(i_p3c_doc) -f_FromTo(i_p3c_soc)           &
!                             -f_FromTo(i_p3c_d1c) -f_FromTo(i_p3c_d2c))/rccalc_min)
!                !q_p3c_p3k = rccalc_min
!             else
!                !plossk = detach_min*p3k + (f_FromTo(i_p3c_d1c)+f_FromTo(i_p3c_d2c))/rccalc_neu
!                plossk = detach_min*p3k + (f_FromTo(i_p3c_d1c)+f_FromTo(i_p3c_d2c))/q_p3c_p3k
!                !q_p3c_p3k = rccalc_neu
!             endif
!             f_FromTo(i_p3k_d2k) = max(zero,plossk)
!mk: ... end
#ifdef CLOERN_RATIO
            ! calculate cocolithophores chl:C ratio after Cloern et al. 1995 (cl)
            !q_chl_p3c(j,k,i) = (0.003 + max(0.,0.0154*exp(0.05*bioT)   &
            !                    *exp(-0.059*par_mean(j,k,i))*limc_np))*12.0 ! g Chl (mol C)^(-1)
            q_chl_p3c(j,k,i) = (0.003 + max(0.,0.0154*exp(0.05*tem)   &
                                *exp(-0.059*par_mean(j,k,i))*limN))*12.0 ! g Chl (mol C)^(-1)
#else
            q_chl_p3c(j,k,i) = q_p3a_p3c
#endif  /* #ifdef CLOERN_RATIO */
#endif  /* #ifdef exclude_p2x */

#ifndef exclude_z1x
      !================================================================================
      ! MICRO-ZOOPLANKTON (z1*)
      !================================================================================
            mu_max=0.0; zloss=0.0  ! to get rid of "unused" warning
#ifdef exclude_bac
            p1_ban = 0.
#endif
#ifdef fasham_grazing
      ! grazing formulation originally from Fasham et al. 1990
            pnenn = one/(p1_p1n*p1n + p1_p2n*p2n + p1_p3n*p3n + p1_d1n*d1n + p1_ban*ban)
            y1 = p1_p1n*p1n*pnenn
            y2 = p1_p2n*p2n*pnenn
            y3 = p1_p3n*p3n*pnenn
            y4 = p1_d1n*d1n*pnenn
            y5 = p1_ban*ban*pnenn
            y6 = zero
            food = y1*p1n + y2*p2n + y3*p3n + y4*d1n + y5*ban
            grnenn = one/(xk31+food)
            g1_p1n = tfac_z1(j,k,i)*g1_max *y1*p1n*grnenn *z1n       ! microzoo->diat
            g1_p2n = tfac_z1(j,k,i)*g1_max *y2*p2n*grnenn *z1n       ! microzoo->flag
            g1_p3n = tfac_z1(j,k,i)*g1_max *y3*p3n*grnenn *z1n       ! microzoo->coco
            g1_d1n = tfac_z1(j,k,i)*g1_max *y4*d1n*grnenn *z1n       ! microzoo->det1
            g1_ban = tfac_z1(j,k,i)*g1_max *y5*ban*grnenn *z1n       ! microzoo->bac

#else
      ! grazing according to Gentleman et al. 2003 (Table 2a, 3a)
            g1_exp = 1. !Hollig type II,  "Gentleman"-Class I(b)
            g1_exp = 2. !Hollig type III, "Gentleman"-Class II(c)
            food = p1_p1n*(p1n**g1_exp) +p1_p2n*(p2n**g1_exp) +p1_p3n*(p3n**g1_exp) &
                  +p1_d1n*(d1n**g1_exp) +p1_ban*(ban**g1_exp)
            grnenn = (xk31**g1_exp)+food
            g1_p1n = tfac_z1(j,k,i)*g1_max *p1_p1n*(p1n**g1_exp)/grnenn *z1n    ! microzoo->dia
            g1_p2n = tfac_z1(j,k,i)*g1_max *p1_p2n*(p2n**g1_exp)/grnenn *z1n    ! microzoo->fla
            g1_p3n = tfac_z1(j,k,i)*g1_max *p1_p3n*(p3n**g1_exp)/grnenn *z1n    ! microzoo->cocos
            g1_d1n = tfac_z1(j,k,i)*g1_max *p1_d1n*(d1n**g1_exp)/grnenn *z1n    ! microzoo->det1
            g1_ban = tfac_z1(j,k,i)*g1_max *p1_ban*(ban**g1_exp)/grnenn *z1n    ! microzoo->bac
#endif
            !fluxes
            f_FromTo(i_p1c_z1c) = g1_p1n*q_p1c_p1n
            f_FromTo(i_p1n_z1n) = g1_p1n
            f_FromTo(i_p1p_z1p) = g1_p1n*q_p1p_p1n
            f_FromTo(i_p2c_z1c) = g1_p2n*q_p2c_p2n
            f_FromTo(i_p2n_z1n) = g1_p2n
            f_FromTo(i_p2p_z1p) = g1_p2n*q_p2p_p2n
            f_FromTo(i_p3c_z1c) = g1_p3n*q_p3c_p3n
            f_FromTo(i_p3n_z1n) = g1_p3n
            f_FromTo(i_p3p_z1p) = g1_p3n*q_p3p_p3n
            f_FromTo(i_d1c_z1c) = g1_d1n*q_d1c_d1n
            f_FromTo(i_d1n_z1n) = g1_d1n
            f_FromTo(i_d1p_z1p) = g1_d1n*q_d1p_d1n
            f_FromTo(i_bac_z1c) = g1_ban*q_bac_ban
            f_FromTo(i_ban_z1n) = g1_ban
            f_FromTo(i_bap_z1p) = g1_ban*q_bap_ban

            f_FromTo(i_p3k_z1c) = f_FromTo(i_p3c_z1c)/q_p3c_p3k !Amount of attached calcite that is ingested by zooplankton
            !f_FromTo(i_z1c_d2k) = f_FromTo(i_p3k_z1c)          !is leaving the zooplankton gut without changing
      ! total uptake
            z1c_food = f_FromTo(i_p1c_z1c) +f_FromTo(i_p2c_z1c) +f_FromTo(i_p3c_z1c) &
                      +f_FromTo(i_d1c_z1c) +f_FromTo(i_bac_z1c)
            z1n_food = f_FromTo(i_p1n_z1n) +f_FromTo(i_p2n_z1n) +f_FromTo(i_p3n_z1n) &
                      +f_FromTo(i_d1n_z1n) +f_FromTo(i_ban_z1n)
            z1p_food = f_FromTo(i_p1p_z1p) +f_FromTo(i_p2p_z1p) +f_FromTo(i_p3p_z1p) &
                      +f_FromTo(i_d1p_z1p) +f_FromTo(i_bap_z1p)

      ! losses microzooplankton
      !======================================================================================================
      ! fecal pellets
            ! C:N:P of feaces is related to the stoichiometry of food and assimilation efficiencies
            ! (beta). If beta's are equal, faeces have same C:N:P as combined food
            z1c_faec = (one-beta1)*z1c_food
            z1n_faec = (one-beta1)*z1n_food
            z1p_faec = (one-beta1)*z1p_food
       ! predation pressure (NEW)
            z1n_pred = zero
#ifdef explicit_predation
       ! place to implement explicit formulations/sources for predation
#endif
       ! mortality (NOTE: Fasham formulations don't separate metabolic losses from mortality!)
#ifdef fasham_mortality
            !density dependent mortality (according to Fasham et al. 1990)
            z1n_mort = z1n_pred + tfac_z1(j,k,i) *xmu21*z1n/(xk16+z1n) *z1n
#else
            !linear + quadratic mortality closure
            !z1n_mort = z1n_pred + tfac_z1(j,k,i) *(xmu21                     +xmq21*z1n) *z1n
            ! following formulation does the same job with respect to choice of parameters (e.g. xk16=0),
            ! but additionally allows for keeking "Fasham"-like behavior at low concentrations
            z1n_mort = z1n_pred + tfac_z1(j,k,i) *(xmu21*z1n/(xk16+z1n)+xmq21*z1n) *z1n
#endif
            z1c_mort = z1n_mort*rcnz1           ! mortality/predation must have C/N of zooplankton !!
            z1p_mort = z1n_mort*rcnz1/rcpz1     ! mortality/predation must have N/P of zooplankton !!

#ifdef fasham_losses
#ifdef fasham_losses_revised
            !----------------------------------------------------------------------------------
            ! fasham losses revised (markus version 2010)
            !----------------------------------------------------------------------------------
            ! re-calculation of predation-loss (mortality) based on assumptions given by Fasham:
            ! - there is no matter unused by higher trophic levels, thus all mortality
            !   is due to predation (even if dead zooplankton has been eaten) and
            !   everything must be split up to DIM, DOM and detritus
            ! - assuming GGE of 25% and AE of 75%, the infinity series of predators lead
            !   to 33%-egested detritus (:=frac_det) and 66%-excretion/respiration (:=(1-frac_det))
            ! - whithin bulk "fasham-losses" (zloss), predation is the only way to produce detritus
            !   via egestion. Thus, the total fraction of predation on "fasham-losses" is given by
            !   mort = (1-aeps-delta)/frac_det* zloss
            zloss = z1n_mort
            z1n_mort = (one-aeps1-delta_don1)/frac_det*zloss
            z1c_mort = z1n_mort*rcnz2         ! mortality/predation must have C/N = rcnz !!
            z1p_mort = z1n_mort*rcnz2/rcpz2   ! mortality/predation must have N/P = rcpz/rcnz !!
            ! calculate metabolic loss
            z1n_loss = zloss - z1n_mort
            ! calculate growth rate related to organism's N-budget
            ! growth = ingested -egested -metabolism
            mu_max = (z1n_food-z1n_faec-(zloss-z1n_mort))/z1n -xmu6
            !metabolic losses = ingested -egested -growth
            z1c_loss = max(zero, z1c_food -z1c_faec -mu_max*z1c)
            z1n_loss = max(zero, z1n_food -z1n_faec -mu_max*z1n)
            z1p_loss = max(zero, z1p_food -z1p_faec -mu_max*z1p)
            !---------------------------------------------------------------------------
            ! carbon-fluxes
            f_FromTo(i_z1c_dic) = (z1c_loss + (one-frac_det)*z1c_mort) *(one-frac_dic)
            f_FromTo(i_z1c_doc) = (z1c_loss + (one-frac_det)*z1c_mort) *     frac_dic
            f_FromTo(i_z1c_d1c) = (z1c_faec +      frac_det *z1c_mort) *(one-frac_d2x)
            f_FromTo(i_z1c_d2c) = (z1c_faec +      frac_det *z1c_mort) *     frac_d2x
            ! nitrogen-fluxes
            f_FromTo(i_z1n_don) = (z1n_loss + (one-frac_det)*z1n_mort) *(one-frac_n4n)
            f_FromTo(i_z1n_n4n) = (z1n_loss + (one-frac_det)*z1n_mort) *     frac_n4n
            f_FromTo(i_z1n_d1n) = (z1n_faec +      frac_det *z1n_mort) *(one-frac_d2x)
            f_FromTo(i_z1n_d2n) = (z1n_faec +      frac_det *z1n_mort) *     frac_d2x
            ! phosphorous-fluxes
            f_FromTo(i_z1p_dop) = (z1p_loss + (one-frac_det)*z1p_mort) *(one-frac_n1p)
            f_FromTo(i_z1p_n1p) = (z1p_loss + (one-frac_det)*z1p_mort) *     frac_n1p
            f_FromTo(i_z1p_d1p) = (z1p_faec +      frac_det *z1p_mort) *(one-frac_d2x)
            f_FromTo(i_z1p_d2p) = (z1p_faec +      frac_det *z1p_mort) *     frac_d2x
#else
            !----------------------------------------------------------------------------------
            ! ECOHAM4-Version (according to D093)
            !----------------------------------------------------------------------------------
            z1c_loss = zero
            z1n_loss = zero
            z1p_loss = zero
#ifdef old_feces_stoichiometry
            ! feces have C:N:P from zooplankton (according to run D093)
            z1c_faec = z1n_faec*rcnz1
            z1p_faec = z1n_faec*rcnz1/rcpz1
#endif
      ! parameterization is based on nitrogen
            f_FromTo(i_z1n_d1n) = ((z1n_loss+z1n_mort)*(one-aeps1-delta_don1) + z1n_faec) *(one-frac_d2x)
            f_FromTo(i_z1n_d2n) = ((z1n_loss+z1n_mort)*(one-aeps1-delta_don1) + z1n_faec) *frac_d2x
            f_FromTo(i_z1n_n4n) =  (z1n_loss+z1n_mort)*aeps1
            f_FromTo(i_z1n_don) =  (z1n_loss+z1n_mort)*delta_don1
      ! carbon and phosphorous fluxes
            f_FromTo(i_z1c_d1c) = ((z1c_loss+z1c_mort)*(one-aeps1-delta_don1) + z1c_faec) *(one-frac_d2x)
            f_FromTo(i_z1c_d2c) = ((z1c_loss+z1c_mort)*(one-aeps1-delta_don1) + z1c_faec) *frac_d2x
            f_FromTo(i_z1c_dic) =  (z1c_loss+z1c_mort)*aeps1
            f_FromTo(i_z1c_doc) =  (z1c_loss+z1c_mort)*delta_don1
            f_FromTo(i_z1p_d1p) = ((z1p_loss+z1p_mort)*(one-aeps1-delta_don1) + z1p_faec) *(one-frac_d2x)
            f_FromTo(i_z1p_d2p) = ((z1p_loss+z1p_mort)*(one-aeps1-delta_don1) + z1p_faec) *frac_d2x
            f_FromTo(i_z1p_n1p) =  (z1p_loss+z1p_mort)*aeps1
            f_FromTo(i_z1p_dop) =  (z1p_loss+z1p_mort)*delta_don1
#endif
! #ifdef fasham_losses coded above !
#else
!----------------------------------------------------------------------------------------------
! ATTENTION!!! New formulations strictly separate metabolic losses from mortality / predation !
!----------------------------------------------------------------------------------------------
            !----------------------------------------------------------------------------------
            ! following suggestion from book Sterner&Elser(2002)-pp197
            !----------------------------------------------------------------------------------
            ! calculate potential growth rate related to most sparse food-element(-currency)
            ! xmu6c represents energy required for feeding-activities & biosynthesis
            mu_max = min((one-xmu6c)*(z1c_food-z1c_faec)/z1c - xmu6, &
                                     (z1n_food-z1n_faec)/z1n - xmu6, &
                                     (z1p_food-z1p_faec)/z1p - xmu6)
            ! calculate metabolic loss rates, assuming that any "excess" uptake is respired/excreted:
            !metabolic losses = ingested -egested -growth
            z1c_loss = max(zero, z1c_food -z1c_faec -mu_max*z1c)
            z1n_loss = max(zero, z1n_food -z1n_faec -mu_max*z1n)
            z1p_loss = max(zero, z1p_food -z1p_faec -mu_max*z1p)
            !---------------------------------------------------------------------------
            ! NOTE:
            ! further, assimilation efficiencies could be related to match-missmatch of
            ! C:N:P-ratios of prey and zooplankton  -> not present jet !!!
            !---------------------------------------------------------------------------
            ! carbon-fluxes
            f_FromTo(i_z1c_dic) = (z1c_loss + (one-frac_det)*z1c_mort) *(one-frac_dic)
            f_FromTo(i_z1c_doc) = (z1c_loss + (one-frac_det)*z1c_mort) *     frac_dic
            f_FromTo(i_z1c_d1c) = (z1c_faec +      frac_det *z1c_mort) *(one-frac_d2x)
            f_FromTo(i_z1c_d2c) = (z1c_faec +      frac_det *z1c_mort) *     frac_d2x
            ! nitrogen-fluxes
            f_FromTo(i_z1n_don) = (z1n_loss + (one-frac_det)*z1n_mort) *(one-frac_n4n)
            f_FromTo(i_z1n_n4n) = (z1n_loss + (one-frac_det)*z1n_mort) *     frac_n4n
            f_FromTo(i_z1n_d1n) = (z1n_faec +      frac_det *z1n_mort) *(one-frac_d2x)
            f_FromTo(i_z1n_d2n) = (z1n_faec +      frac_det *z1n_mort) *     frac_d2x
            ! phosphorous-fluxes
            f_FromTo(i_z1p_dop) = (z1p_loss + (one-frac_det)*z1p_mort) *(one-frac_n1p)
            f_FromTo(i_z1p_n1p) = (z1p_loss + (one-frac_det)*z1p_mort) *     frac_n1p
            f_FromTo(i_z1p_d1p) = (z1p_faec +      frac_det *z1p_mort) *(one-frac_d2x)
            f_FromTo(i_z1p_d2p) = (z1p_faec +      frac_det *z1p_mort) *     frac_d2x
#endif

      ! balance fluxes for micro-zooplankton
      ! unbalanced net C flux into micro-zooplankton
            fzc1 = z1c_food -f_FromTo(i_z1c_d1c) -f_FromTo(i_z1c_d2c) &
                            -f_FromTo(i_z1c_doc) -f_FromTo(i_z1c_dic)
      ! unbalanced net N flux into micro-zooplankton
            fzn1 = z1n_food -f_FromTo(i_z1n_d1n) -f_FromTo(i_z1n_d2n) &
                            -f_FromTo(i_z1n_don) -f_FromTo(i_z1n_n4n)
      ! unbalanced net P flux into micro-zooplankton
            fzp1 = z1p_food -f_FromTo(i_z1p_d1p) -f_FromTo(i_z1p_d2p) &
                            -f_FromTo(i_z1p_dop) -f_FromTo(i_z1p_n1p)
            if ((abs(fzc1-rcnz1*fzn1)<1.0e-12) .and. (abs(fzc1-rcpz1*fzp1)<1.0e-12)) then
               !print*,'!everything is fine!'
               !print*,(fzc1-rcnz1*fzn1),(fzc1-rcpz1*fzp1),fzc1,rcnz1*fzn1,rcpz1*fzp1
            else
#ifdef debug_zoo
               if ((fzc1 <= rcnz1*fzn1) .and. (fzc1 <= rcpz1*fzp1)) then
                  write(long_msg,'("[",i2.2,"] microzoo",a,": zuviel N und P",3e20.10)') &
                                 myID,trim(str_jki),fzc1,rcnz1*fzn1,rcpz1*fzp1
                  call write_log_message(long_msg); long_msg = ''
               else if ((fzc1 >= rcnz1*fzn1) .and. (fzn1 <= rcpz1/rcnz1*fzp1)) then !Zuviel C und P
                  write(long_msg,'("[",i2.2,"] microzoo",a,": zuviel C und P",3e20.10)') &
                                 myID,trim(str_jki),fzc1,rcnz1*fzn1,rcpz1*fzp1
                  call write_log_message(long_msg); long_msg = ''
               else if ((fzc1 >= rcpz1*fzp1) .and. (fzn1 >= rcpz1/rcnz1*fzp1)) then !Zuviel C und N
                  write(long_msg,'("[",i2.2,"] microzoo",a,": zuviel C und N",3e20.10)') &
                                 myID,trim(str_jki),fzc1,rcnz1*fzn1,rcpz1*fzp1
                  call write_log_message(long_msg); long_msg = ''
               endif
#endif
            ! compensation of unbalanced into meso-zooplankton
               if ((fzc1 <= rcnz1*fzn1) .and. (fzc1 <= rcpz1*fzp1)) then !Zuviel N und P
                  f_FromTo(i_z1n_don) = f_FromTo(i_z1n_don)+(fzn1-fzc1/rcnz1)/2.
                  f_FromTo(i_z1n_n4n) = f_FromTo(i_z1n_n4n)+(fzn1-fzc1/rcnz1)/2.
                  f_FromTo(i_z1p_dop) = f_FromTo(i_z1p_dop)+(fzp1-fzc1/rcpz1)/2.
                  f_FromTo(i_z1p_n1p) = f_FromTo(i_z1p_n1p)+(fzp1-fzc1/rcpz1)/2.
               else if ((fzc1 >= rcnz1*fzn1) .and. (fzn1 <= rcpz1/rcnz1*fzp1)) then !Zuviel C und P
                  f_FromTo(i_z1c_dic) = f_FromTo(i_z1c_dic)+(fzc1-fzn1*rcnz1)
                  f_FromTo(i_z1p_dop) = f_FromTo(i_z1p_dop)+(fzp1-fzn1*rcnz1/rcpz1)/2.
                  f_FromTo(i_z1p_n1p) = f_FromTo(i_z1p_n1p)+(fzp1-fzn1*rcnz1/rcpz1)/2.
               else if ((fzc1 >= rcpz1*fzp1) .and. (fzn1 >= rcpz1/rcnz1*fzp1)) then !Zuviel C und N
                  f_FromTo(i_z1c_dic) = f_FromTo(i_z1c_dic)+(fzc1-fzp1*rcpz1)
                  f_FromTo(i_z1n_don) = f_FromTo(i_z1n_don)+(fzn1-fzp1*rcpz1/rcnz1)/2.
                  f_FromTo(i_z1n_n4n) = f_FromTo(i_z1n_n4n)+(fzn1-fzp1*rcpz1/rcnz1)/2.
               else
                  !print*,myID,'microzoo: ',fzc1,fzn1*rcnz1,fzp1*rcpz1
                  ierr = 1
                  write(fmtstr,'(a)') '("[",i2.2,"] - Fehler bei micro-zooplankton flux",a)'
                  write(error_message,trim(fmtstr)) myID, trim(str_jki)
                  call stop_ecoham(ierr, msg=error_message)
               endif
            endif
#endif /* #ifdef exclude_z1x */

#ifndef exclude_z2x
      !================================================================================
      ! MESO-ZOOPLANKTON (z2*)
      !================================================================================
            mu_max=0.0; zloss=0.0  ! to get rid of "unused" warning
#ifdef exclude_bac
            p2_ban = 0.
#endif
#ifdef fasham_grazing
      ! grazing formulation originally from Fasham et al. 1990
            pnenn = one/(p2_p1n*p1n + p2_p2n*p2n + p2_p3n*p3n + p2_d1n*d1n + p2_ban*ban + p2_z1n*z1n)
            y1 = p2_p1n*p1n*pnenn
            y2 = p2_p2n*p2n*pnenn
            y3 = p2_p3n*p3n*pnenn
            y4 = p2_d1n*d1n*pnenn
            y5 = p2_ban*ban*pnenn
            y6 = p2_z1n*z1n*pnenn
            food = y1*p1n + y2*p2n + y3*p3n + y4*d1n + y5*ban + y6*z1n
            grnenn = one/(xk32+food)
            g2_p1n = tfac_z2(j,k,i)*g2_max *y1*p1n*grnenn *z2n       ! mesozoo->diat
            g2_p2n = tfac_z2(j,k,i)*g2_max *y2*p2n*grnenn *z2n       ! mesozoo->flag
            g2_p3n = tfac_z2(j,k,i)*g2_max *y3*p3n*grnenn *z2n       ! mesozoo->coco
            g2_d1n = tfac_z2(j,k,i)*g2_max *y4*d1n*grnenn *z2n       ! mesozoo->det1
            g2_ban = tfac_z2(j,k,i)*g2_max *y5*ban*grnenn *z2n       ! mesozoo->bac
            g2_z1n = tfac_z2(j,k,i)*g2_max *y6*z1n*grnenn *z2n       ! mesozoo->micro
#else
      ! grazing according to Gentleman et al. 2003 (Table 2a, 3a)
            g2_exp=1. !Hollig type II,  "Gentleman"-Class I(b)
            g2_exp=2. !Hollig type III, "Gentleman"-Class II(c)
            food = p2_p1n*(p1n**g2_exp) +p2_p2n*(p2n**g2_exp) +p2_p3n*(p3n**g2_exp) &
                  +p2_d1n*(d1n**g2_exp) +p2_ban*(ban**g2_exp) +p2_z1n*(z1n**g2_exp)
            grnenn = (xk32**g2_exp)+food
            g2_p1n = tfac_z2(j,k,i)*g2_max *p2_p1n*(p1n**g2_exp)/grnenn *z2n    ! mesozoo->dia
            g2_p2n = tfac_z2(j,k,i)*g2_max *p2_p2n*(p2n**g2_exp)/grnenn *z2n    ! mesozoo->fla
            g2_p3n = tfac_z2(j,k,i)*g2_max *p2_p3n*(p3n**g2_exp)/grnenn *z2n    ! mesozoo->cocos
            g2_d1n = tfac_z2(j,k,i)*g2_max *p2_d1n*(d1n**g2_exp)/grnenn *z2n    ! mesozoo->det1
            g2_ban = tfac_z2(j,k,i)*g2_max *p2_ban*(ban**g2_exp)/grnenn *z2n    ! mesozoo->bac
            g2_z1n = tfac_z2(j,k,i)*g2_max *p2_z1n*(z1n**g2_exp)/grnenn *z2n    ! mesozoo->micro
#endif
            !fluxes
            f_FromTo(i_p1c_z2c) = g2_p1n*q_p1c_p1n
            f_FromTo(i_p1n_z2n) = g2_p1n
            f_FromTo(i_p1p_z2p) = g2_p1n*q_p1p_p1n
            f_FromTo(i_p2c_z2c) = g2_p2n*q_p2c_p2n
            f_FromTo(i_p2n_z2n) = g2_p2n
            f_FromTo(i_p2p_z2p) = g2_p2n*q_p2p_p2n
            f_FromTo(i_p3c_z2c) = g2_p3n*q_p3c_p3n
            f_FromTo(i_p3n_z2n) = g2_p3n
            f_FromTo(i_p3p_z2p) = g2_p3n*q_p3p_p3n
            f_FromTo(i_d1c_z2c) = g2_d1n*q_d1c_d1n
            f_FromTo(i_d1n_z2n) = g2_d1n
            f_FromTo(i_d1p_z2p) = g2_d1n*q_d1p_d1n
            f_FromTo(i_bac_z2c) = g2_ban*q_bac_ban
            f_FromTo(i_ban_z2n) = g2_ban
            f_FromTo(i_bap_z2p) = g2_ban*q_bap_ban
            f_FromTo(i_z1c_z2c) = g2_z1n*q_z1c_z1n
            f_FromTo(i_z1n_z2n) = g2_z1n
            f_FromTo(i_z1p_z2p) = g2_z1n*q_z1p_z1n
            f_FromTo(i_p3k_z2c) = f_FromTo(i_p3c_z2c)/q_p3c_p3k !Amount of attached calcite that is ingested by zooplankton
            !f_FromTo(i_z2c_d2k)= f_FromTo(i_p3k_z2c)           !is leaving the zooplankton gut without changing
      ! total uptake
            z2c_food = f_FromTo(i_p1c_z2c) +f_FromTo(i_p2c_z2c) +f_FromTo(i_p3c_z2c) &
                      +f_FromTo(i_d1c_z2c) +f_FromTo(i_bac_z2c) +f_FromTo(i_z1c_z2c)
            z2n_food = f_FromTo(i_p1n_z2n) +f_FromTo(i_p2n_z2n) +f_FromTo(i_p3n_z2n) &
                      +f_FromTo(i_d1n_z2n) +f_FromTo(i_ban_z2n) +f_FromTo(i_z1n_z2n)
            z2p_food = f_FromTo(i_p1p_z2p) +f_FromTo(i_p2p_z2p) +f_FromTo(i_p3p_z2p) &
                      +f_FromTo(i_d1p_z2p) +f_FromTo(i_bap_z2p) +f_FromTo(i_z1p_z2p)

      ! losses mesozooplankton
      !======================================================================================================
      ! fecal pellets
            ! C:N:P of feaces is related to the stoichiometry of food and assimilation efficiencies
            ! (beta). If beta's are equal, faeces have same C:N:P as combined food
            z2c_faec = (one-beta2)*z2c_food
            z2n_faec = (one-beta2)*z2n_food
            z2p_faec = (one-beta2)*z2p_food
       ! predation pressure (NEW)
            z2n_pred = zero
#ifdef explicit_predation
       ! place to implement explicit formulations/sources for predation
#endif
       ! mortality (NOTE: Fasham formulations don't separate metabolic losses from mortality!)
#ifdef fasham_mortality
            !density dependent mortality (according to Fasham et al. 1990)
            z2n_mort = z2n_pred + tfac_z2(j,k,i) *xmu22*z2n/(xk26+z2n) *z2n
#else
            !linear + quadratic mortality closure
            !z1n_mort = z2n_pred + tfac_z2(j,k,i) *(xmu22                     +xmq22*z2n) *z2n
            ! following formulation does the same job with respect to choice of parameters (e.g. xk26=0),
            ! but additionally allows for keeking "Fasham"-like behavior at low concentrations
            z2n_mort = z2n_pred + tfac_z2(j,k,i) *(xmu22*z2n/(xk26+z2n)+xmq22*z2n) *z2n
#endif
            z2c_mort = z2n_mort*rcnz2           ! mortality/predation must have C/N of zooplankton !!
            z2p_mort = z2n_mort*rcnz2/rcpz2     ! mortality/predation must have N/P of zooplankton !!

#ifdef fasham_losses
#ifdef fasham_losses_revised
            !----------------------------------------------------------------------------------
            ! fasham losses revised (markus version 2010)
            !----------------------------------------------------------------------------------
            ! re-calculation of predation-loss (mortality) based on assumptions given by Fasham:
            ! - there is no matter unused by higher trophic levels, thus all mortality
            !   is due to predation (even if dead zooplankton has been eaten) and
            !   everything must be split up to DIM, DOM and detritus
            ! - assuming GGE of 25% and AE of 75%, the infinity series of predators lead
            !   to 33%-egested detritus (:=frac_det) and 66%-excretion/respiration (:=(1-frac_det))
            ! - whithin bulk "fasham-losses" (zloss), predation is the only way to produce detritus
            !   via egestion. Thus, the total fraction of predation on "fasham-losses" is given by
            !   mort = (1-aeps-delta)/frac_det* zloss
            zloss = z2n_mort
            z2n_mort = (one-aeps2-delta_don2)/frac_det*zloss
            z2c_mort = z2n_mort*rcnz2         ! mortality/predation must have C/N = rcnz !!
            z2p_mort = z2n_mort*rcnz2/rcpz2   ! mortality/predation must have N/P = rcpz/rcnz !!
            ! calculate metabolic loss
            z2n_loss = zloss - z2n_mort
            ! calculate growth rate related to organism's N-budget
            ! growth = ingested -egested -metabolism -basal metabolism
            mu_max = (z2n_food-z2n_faec-(zloss-z2n_mort))/z2n -xmu6
            !metabolic losses = ingested -egested -growth
            z2c_loss = z2c_food -z2c_faec -mu_max*z2c
            z2n_loss = z2n_food -z2n_faec -mu_max*z2n
            z2p_loss = z2p_food -z2p_faec -mu_max*z2p
            !---------------------------------------------------------------------------
            ! carbon-fluxes
            f_FromTo(i_z2c_dic) = (z2c_loss + (one-frac_det)*z2c_mort) *(one-frac_dic)
            f_FromTo(i_z2c_doc) = (z2c_loss + (one-frac_det)*z2c_mort) *     frac_dic
            f_FromTo(i_z2c_d1c) = (z2c_faec +      frac_det *z2c_mort) *(one-frac_d2x)
            f_FromTo(i_z2c_d2c) = (z2c_faec +      frac_det *z2c_mort) *     frac_d2x
            ! nitrogen-fluxes
            f_FromTo(i_z2n_don) = (z2n_loss + (one-frac_det)*z2n_mort) *(one-frac_n4n)
            f_FromTo(i_z2n_n4n) = (z2n_loss + (one-frac_det)*z2n_mort) *     frac_n4n
            f_FromTo(i_z2n_d1n) = (z2n_faec +      frac_det *z2n_mort) *(one-frac_d2x)
            f_FromTo(i_z2n_d2n) = (z2n_faec +      frac_det *z2n_mort) *     frac_d2x
            ! phosphorous-fluxes
            f_FromTo(i_z2p_dop) = (z2p_loss + (one-frac_det)*z2p_mort) *(one-frac_n1p)
            f_FromTo(i_z2p_n1p) = (z2p_loss + (one-frac_det)*z2p_mort) *     frac_n1p
            f_FromTo(i_z2p_d1p) = (z2p_faec +      frac_det *z2p_mort) *(one-frac_d2x)
            f_FromTo(i_z2p_d2p) = (z2p_faec +      frac_det *z2p_mort) *     frac_d2x
#else
            !----------------------------------------------------------------------------------
            ! ECOHAM4-Version (according to D093)
            !----------------------------------------------------------------------------------
            z2c_loss = 0.0
            z2n_loss = 0.0
            z2p_loss = 0.0
#ifdef old_feces_stoichiometry
            ! feces have C:N:P from zooplankton (according to run D093)
            z2c_faec = z2n_faec*rcnz2
            z2p_faec = z2n_faec*rcnz2/rcpz2
#endif
      ! parameterization is based on nitrogen
            f_FromTo(i_z2n_d1n) = ((z2n_loss+z2n_mort)*(one-aeps2-delta_don2) + z2n_faec) *(one-frac_d2x)
            f_FromTo(i_z2n_d2n) = ((z2n_loss+z2n_mort)*(one-aeps2-delta_don2) + z2n_faec) *frac_d2x
            f_FromTo(i_z2n_n4n) =  (z2n_loss+z2n_mort)*aeps2
            f_FromTo(i_z2n_don) =  (z2n_loss+z2n_mort)*delta_don2
      ! carbon and phosphorous fluxes
            f_FromTo(i_z2c_d1c) = ((z2c_loss+z2c_mort)*(one-aeps2-delta_don2) + z2c_faec) *(one-frac_d2x)
            f_FromTo(i_z2c_d2c) = ((z2c_loss+z2c_mort)*(one-aeps2-delta_don2) + z2c_faec) *frac_d2x
            f_FromTo(i_z2c_dic) =  (z2c_loss+z2c_mort)*aeps2
            f_FromTo(i_z2c_doc) =  (z2c_loss+z2c_mort)*delta_don2
            f_FromTo(i_z2p_d1p) = ((z2p_loss+z2p_mort)*(one-aeps2-delta_don2) + z2p_faec) *(one-frac_d2x)
            f_FromTo(i_z2p_d2p) = ((z2p_loss+z2p_mort)*(one-aeps2-delta_don2) + z2p_faec) *frac_d2x
            f_FromTo(i_z2p_n1p) =  (z2p_loss+z2p_mort)*aeps2
            f_FromTo(i_z2p_dop) =  (z2p_loss+z2p_mort)*delta_don2
!----------------------------------------------------------------------------------------------------------------

#endif
! #ifdef fasham_losses coded above !
#else
!----------------------------------------------------------------------------------------------
! ATTENTION!!! New formulations strictly separate metabolic losses from mortality / predation !
!----------------------------------------------------------------------------------------------
            !----------------------------------------------------------------------------------
            ! following suggestion from book Sterner&Elser(2002)-pp197
            !----------------------------------------------------------------------------------
            ! calculate potential growth rate related to most sparse food-element(-currency)
            ! xmu6c represents energy required for feeding-activities & biosynthesis
            mu_max = min(((one-xmu6c)*z2c_food-z2c_faec)/z2c - xmu6, &
                                     (z2n_food-z2n_faec)/z2n - xmu6, &
                                     (z2p_food-z2p_faec)/z2p - xmu6)
            ! calculate metabolic loss rates, assuming that any "excess" uptake is respired/excreted:
            !metabolic losses = ingested -egested -growth
            z2c_loss = max(zero, z2c_food -z2c_faec -mu_max*z2c)
            z2n_loss = max(zero, z2n_food -z2n_faec -mu_max*z2n)
            z2p_loss = max(zero, z2p_food -z2p_faec -mu_max*z2p)
            !---------------------------------------------------------------------------
            ! NOTE:
            ! further, assimilation efficiencies could be related to match-missmatch of
            ! C:N:P-ratios of prey and zooplankton  -> not present jet !!!
            !---------------------------------------------------------------------------
            ! carbon-fluxes
            f_FromTo(i_z2c_dic) = (z2c_loss + (one-frac_det)*z2c_mort) *(one-frac_dic)
            f_FromTo(i_z2c_doc) = (z2c_loss + (one-frac_det)*z2c_mort) *     frac_dic
            f_FromTo(i_z2c_d1c) = (z2c_faec +      frac_det *z2c_mort) *(one-frac_d2x)
            f_FromTo(i_z2c_d2c) = (z2c_faec +      frac_det *z2c_mort) *     frac_d2x
            ! nitrogen-fluxes
            f_FromTo(i_z2n_don) = (z2n_loss + (one-frac_det)*z2n_mort) *(one-frac_n4n)
            f_FromTo(i_z2n_n4n) = (z2n_loss + (one-frac_det)*z2n_mort) *     frac_n4n
            f_FromTo(i_z2n_d1n) = (z2n_faec +      frac_det *z2n_mort) *(one-frac_d2x)
            f_FromTo(i_z2n_d2n) = (z2n_faec +      frac_det *z2n_mort) *     frac_d2x
            ! phosphorous-fluxes
            f_FromTo(i_z2p_dop) = (z2p_loss + (one-frac_det)*z2p_mort) *(one-frac_n1p)
            f_FromTo(i_z2p_n1p) = (z2p_loss + (one-frac_det)*z2p_mort) *     frac_n1p
            f_FromTo(i_z2p_d1p) = (z2p_faec +      frac_det *z2p_mort) *(one-frac_d2x)
            f_FromTo(i_z2p_d2p) = (z2p_faec +      frac_det *z2p_mort) *     frac_d2x
#endif

      ! balance fluxes for meso-zooplankton
      ! unbalanced net C flux into meso-zooplankton
            fzc2 = z2c_food -f_FromTo(i_z2c_d1c) -f_FromTo(i_z2c_d2c)   &
                            -f_FromTo(i_z2c_doc) -f_FromTo(i_z2c_dic)
      ! unbalanced net N flux into meso-zooplankton
            fzn2 = z2n_food -f_FromTo(i_z2n_d1n) -f_FromTo(i_z2n_d2n)   &
                            -f_FromTo(i_z2n_don) -f_FromTo(i_z2n_n4n)
      ! unbalanced net P flux into meso-zooplankton
            fzp2 = z2p_food -f_FromTo(i_z2p_d1p) -f_FromTo(i_z2p_d2p)   &
                            -f_FromTo(i_z2p_dop) -f_FromTo(i_z2p_n1p)
            if ((abs(fzc2-rcnz2*fzn2)<1.0e-12).and.(abs(fzc2-rcpz2*fzp2)<1.0e-12)) then
               !print*,'!everything is fine!'
               !print*,(fzc2-rcnz2*fzn2),(fzc2-rcpz2*fzp2),fzc2,rcnz2*fzn2,rcpz2*fzp2
            else
#ifdef debug_zoo
               if ((fzc2 <= rcnz2*fzn2).and.(fzc2 <= rcpz2*fzp2)) then
                  write(long_msg,'("[",i2.2,"] mesozoo",a,": zuviel N und P",3e20.10)') &
                                 myID,trim(str_jki),fzc2,rcnz2*fzn2,rcpz2*fzp2
                  call write_log_message(long_msg); long_msg = ''
               else if ((fzc2 >= rcnz2*fzn2).and.(fzn2 <= rcpz2/rcnz2*fzp2)) then !Zuviel C und P
                  write(long_msg,'("[",i2.2,"] mesozoo",a,": zuviel C und P",3e20.10)') &
                                 myID,trim(str_jki),fzc2,rcnz2*fzn2,rcpz2*fzp2
                  call write_log_message(long_msg); long_msg = ''
               else if ((fzc2 >= rcpz2*fzp2).and.(fzn2 >= rcpz2/rcnz2*fzp2)) then !Zuviel C und N
                  write(long_msg,'("[",i2.2,"] mesozoo",a,": zuviel C und N",3e20.10)') &
                                 myID,trim(str_jki),fzc2,rcnz2*fzn2,rcpz2*fzp2
                  call write_log_message(long_msg); long_msg = ''
               endif
#endif
            ! compensation of unbalanced into meso-zooplankton
               if ((fzc2 <= rcnz2*fzn2).and.(fzc2 <= rcpz2*fzp2)) then !Zuviel N und P
                  f_FromTo(i_z2n_don)=f_FromTo(i_z2n_don)+(fzn2-fzc2/rcnz2)/2.
                  f_FromTo(i_z2n_n4n)=f_FromTo(i_z2n_n4n)+(fzn2-fzc2/rcnz2)/2.
                  f_FromTo(i_z2p_dop)=f_FromTo(i_z2p_dop)+(fzp2-fzc2/rcpz2)/2.
                  f_FromTo(i_z2p_n1p)=f_FromTo(i_z2p_n1p)+(fzp2-fzc2/rcpz2)/2.
               else if ((fzc2 >= rcnz2*fzn2).and.(fzn2 <= rcpz2/rcnz2*fzp2)) then !Zuviel C und P
                  f_FromTo(i_z2c_dic)=f_FromTo(i_z2c_dic)+(fzc2-fzn2*rcnz2)
                  f_FromTo(i_z2p_dop)=f_FromTo(i_z2p_dop)+(fzp2-fzn2*rcnz2/rcpz2)/2.
                  f_FromTo(i_z2p_n1p)=f_FromTo(i_z2p_n1p)+(fzp2-fzn2*rcnz2/rcpz2)/2.
               else if ((fzc2 >= rcpz2*fzp2).and.(fzn2 >= rcpz2/rcnz2*fzp2)) then !Zuviel C und N
                  f_FromTo(i_z2c_dic)=f_FromTo(i_z2c_dic)+(fzc2-fzp2*rcpz2)
                  f_FromTo(i_z2n_don)=f_FromTo(i_z2n_don)+(fzn2-fzp2*rcpz2/rcnz2)/2.
                  f_FromTo(i_z2n_n4n)=f_FromTo(i_z2n_n4n)+(fzn2-fzp2*rcpz2/rcnz2)/2.
               else
                  ierr = 1
                  write(fmtstr,'(a)') '("[",i2.2,"] - Fehler bei meso-zooplankton flux",a)'
                  write(error_message,trim(fmtstr)) myID, trim(str_jki)
                  call stop_ecoham(ierr, msg=error_message)
               endif
            endif
#endif /* #ifdef exclude_z2x */

      !================================================================================
      ! DETRITUS
      !================================================================================
      ! DETRITUS-C
            f_FromTo(i_d1c_doc)=xmu4n*tfac(j,k,i)*d1c*rxmu4c
            f_FromTo(i_d2c_doc)=xmu5n*tfac(j,k,i)*d2c*rxmu4c
      ! DETRITUS-N
            f_FromTo(i_d1n_don)=xmu4n*tfac(j,k,i)*d1n
            f_FromTo(i_d2n_don)=xmu5n*tfac(j,k,i)*d2n
      ! DETRITUS-P
            f_FromTo(i_d1p_dop)=xmu4n*tfac(j,k,i)*d1p
            f_FromTo(i_d2p_dop)=xmu5n*tfac(j,k,i)*d2p

      ! DETRITUS-Silicon
#if !defined exclude_p1x
      !  f_FromTo(f_p1s_d2s)=(f_FromTo(i_p1c_d2c)+f_FromTo(i_p1c_d1c) &
      !                       +f_FromTo(i_p1c_doc)+f_FromTo(i_p1c_zec))/rcs !E0xx
      !E065 and E067--------------------------------------------------------
      !      !f_FromTo(i_p1s_d2s)=(f_FromTo(i_p1c_d1c)+f_FromTo(i_p1c_d2c))/rcs
      !      dia_adds_loss = (f_FromTo(i_p1c_doc)+f_FromTo(i_p1c_z1c)+f_FromTo(i_p1c_z2c))/rcs
      !      dia_ups       = f_FromTo(i_n5s_p1s)
      !      f_FromTo(i_n5s_p1s) =                     max(0.0,dia_ups - dia_adds_loss)   !JP Vermindere n5s Aufnahme
      !      f_FromTo(i_p1s_d2s) = f_FromTo(i_p1s_d2s)+max(0.0,dia_adds_loss - dia_ups)   !JP Wenn das nicht reicht, schmeisse den Rest raus
      !MK------------------------------------------------------------
            !!if (rcn/q_p1s_p1n > rcs) then !.and. p1s.gt.eps
            if (q_p1s_p1n > Qmin_p1s_p1n) then !.and. p1s.gt.eps
              !dia_adds_loss = max(f_FromTo(i_p1n_don)*q_p1s_p1n   ,f_FromTo(i_p1c_doc)*q_p1s_p1c   )
              ! @ a06
              dia_adds_loss = max( f_FromTo(i_p1n_don)/p1n, &
                                   f_FromTo(i_p1c_doc)/p1c, &
                                   f_FromTo(i_p1p_dop)/p1p) *p1s *(1-f_RSN)
              ! @ a05
              !dia_adds_loss = max( f_FromTo(i_p1n_don)          *Qmin_p1s_p1n, &
              !                     f_FromTo(i_p1c_doc)*q_p1n_p1c*Qmin_p1s_p1n, &
              !                     f_FromTo(i_p1p_dop)/q_p1p_p1n*Qmin_p1s_p1n)
              ! @ a04
              !dia_adds_loss = max( f_FromTo(i_p1n_don)*Qmin_p1s_p1n,              &
              !                     f_FromTo(i_p1c_doc)*Qmax_p1n_p1c*Qmin_p1s_p1n, &
              !                     f_FromTo(i_p1p_dop)/Qmin_p1p_p1n*Qmin_p1s_p1n)
              dia_ups       = f_FromTo(i_n5s_p1s)
              f_FromTo(i_n5s_p1s)=                      max(zero, dia_ups - dia_adds_loss)  !MK reduce uptake to compensate for loss due to excretion/lysis
              f_FromTo(i_p1s_d2s)=f_FromTo(i_p1s_d2s) + max(zero, dia_adds_loss - dia_ups)  !MK apply kind of homeostatis
            endif
            f_FromTo(i_p1s_d2s)=f_FromTo(i_p1s_d2s) + ( f_FromTo(i_p1n_z1n)+f_FromTo(i_p1n_z2n) )*q_p1s_p1n    !MK zooplankton grazing is passed-through to detritus
#endif
            !------------------------------------------------------------
            f_FromTo(i_d2s_n5s)=xmu5n*d2s*tfac(j,k,i)*rxmu4c/10.         !like C/10

            ! DETRITUS-skeleton
#if !defined exclude_p2x
            !Amount of attached calcite that is ingested is leaving the zooplankton gut without changing
            !!f_FromTo(i_psk_d2k) = f_FromTo(i_p3k_d2k)+f_FromTo(i_p3k_z1c)+f_FromTo(i_p3k_z2c)
            !!f_FromTo(i_psk_d2k) =(f_FromTo(i_p2c_d1c)+f_FromTo(i_p2c_d2c)  &
            !!                            +f_FromTo(i_p2c_doc)+f_FromTo(i_p2c_zic)+f_FromTo(i_p2c_zec))/q_c_cal
            !delta_o3c(j,k,i)=ro(j,k,i)*max(0.,o3c(j,k,i)/ro(j,k,i)-aksp(j,k,i)/ca(j,k,i))       !jp ca and aksp per kg
            !f_FromTo(i_d2k_dic) = d2k/30.*(one-delta_o3c(j,k,i)/(delta_o3c(j,k,i)+c0))   !  Ad-hoc Heinze et al
            !MK found in eco9fs:
            f_FromTo(i_psk_d2k) = (f_FromTo(i_p2c_z1c) +f_FromTo(i_p2c_z2c)  &
                                         +f_FromTo(i_p2c_d1c) +f_FromTo(i_p2c_d2c)  &
                                         +f_FromTo(i_p2c_doc))/q_c_cal
            ! jpnew including overflow
            !f_FromTo(i_psk_d2k) = f_FromTo(i_psk_d2k) +f_FromTo(i_p2c_soc)/q_c_cal

            ! FS: CaCO3-diss in copepod guts
            f_FromTo(i_psk_dic) = 0.*(f_FromTo(i_p2c_z1c)+f_FromTo(i_p2c_z2c))/q_c_cal
            f_FromTo(i_psk_d2k) = f_FromTo(i_psk_d2k) -f_FromTo(i_psk_dic)
#endif
#ifdef module_chemie
            delta_o3c(j,k,i) = ch(j,k,i,io3c) - aksp(j,k,i)/ca(j,k,i)
            if (caco3_diss==1.or.delta_o3c(j,k,i)<0.0) then
               delta_o3c(j,k,i) = max(0.,ch(j,k,i,io3c)-aksp(j,k,i)/ca(j,k,i))
               f_FromTo(i_d2k_dic) = d2k/30.*(one-delta_o3c(j,k,i)/(delta_o3c(j,k,i)+c0))
            else
               delta_o3c(j,k,i) = max(0.,ch(j,k,i,io3c)-aksp(j,k,i)/ca(j,k,i))
               f_FromTo(i_d2k_dic) = 0.0
            endif
#endif

      !================================================================================
      ! BACTERIA (switched-off)
      !================================================================================
!            fy=0.0 ! to get rid of "unused" warning
            ! dd(doc ,dic ,ci) = rhoC   *Tf  *cc( doc,ci)    !remin. of (labile) dissolved organic carbon
            ! dd(don ,din ,ci) = rhoN   *Tf  *cc( don,ci)    !remin. of (labile) dissolved organic nitrogen
            ! dd(dop ,dip ,ci) = rhoP   *Tf  *cc( dop,ci)    !remin. of (labile) dissolved organic phosphorous
            f_FromTo(i_doc_dic) = rhoC *tfac(j,k,i) *doc
            f_FromTo(i_don_n4n) = rhoN *tfac(j,k,i) *don
            f_FromTo(i_dop_n1p) = rhoP *tfac(j,k,i) *dop

#ifndef exclude_bac
      !================================================================================
      ! BACTERIA
      !================================================================================
            fy = don/(xk4+don)                            ! food dependency
            !fy = min(don/(xk4+don),dop/(xkpb+dop))        ! food dependency
            bac_loss = xmu3*tfac(j,k,i) !+ 0.1*xmu3*ban
      ! BACTERIA-N
            f_FromTo(i_don_ban) = vb*tfac(j,k,i)*fy*ban
            f_FromTo(i_n4n_ban) = 0.0
            !f_FromTo(i_ban_n4n) = xmu3*tfac(j,k,i)*ban
            f_FromTo(i_ban_n4n) = bac_loss*ban
      ! BACTERIA-C
      !      f_FromTo(i_doc_bac) = f_FromTo(i_don_ban)*q_doc_don !MK macht das Sinn?
            f_FromTo(i_doc_bac) = f_FromTo(i_don_ban)*q_doc_don
            !f_FromTo(i_bac_dic) = xmu3*tfac(j,k,i)*bac
            f_FromTo(i_bac_dic) = bac_loss*bac
      ! BACTERIA-P
      !      f_FromTo(i_dop_bap) = f_FromTo(i_don_ban)*rcnb/rcpb
      !changing dop_bap (before: f_dop_bap(j,k,i) = u1/rnpb);now direct calculation; 0.01 estimated because xk4 for N uptake is 0.1
      !FS       if (dop.gt.1.e-4) then
            if (dop.gt.TINY) then
               f_FromTo(i_dop_bap) = vb*tfac(j,k,i) *dop/(xkpb+dop) *bap
            else
      !        f_FromTo(i_n1p_bap) = u1/rnpb
               f_FromTo(i_dop_bap) = 0.
            endif
      !if (j==14.and.i==32.and.k==1) then  !jpdeb
      !print*, k, td, f_FromTo(i_n4n_ban), rcnb, ' n4n_ban in do bio 3' !jpdeb
      !continue
      !endif

#ifdef old_bac
      ! unbalanced net C flow into bacteria
            fbc = f_FromTo(i_doc_bac) -f_FromTo(i_bac_dic)  &
                 -f_FromTo(i_bac_z1c) -f_FromTo(i_bac_z2c)
      ! unbalanced net N flow into bacteria
            fbn = f_FromTo(i_don_ban) +f_FromTo(i_n4n_ban) -f_FromTo(i_ban_n4n)  &
                 -f_FromTo(i_ban_z1n) -f_FromTo(i_ban_z2n)

            if (fbc.gt.fbn*rcnb) then  !more C than N
               if (n4n<=tres_n4n) then !enhance respiration, if no ammonium is available
                  f_FromTo(i_bac_dic) = f_FromTo(i_bac_dic) +fbc -fbn*rcnb
                  f_FromTo(i_n4n_ban) = 0.
               else !take up ammonium to support C uptake
                  f_FromTo(i_n4n_ban) = (q_doc_don/rcnb - 1.0)*f_FromTo(i_don_ban)
               endif
            else !more N than C -> suppress respiration
               fdc_diff = f_FromTo(i_bac_dic) +fbc -fbn*rcnb
               f_FromTo(i_bac_dic) = max(0.,fdc_diff)
               f_FromTo(i_ban_n4n) = f_FromTo(i_ban_n4n) -min(0.,fdc_diff/rcnb)
            endif

            fbpmax = (f_FromTo(i_n4n_ban)/n4n)*n1p !maximal moeglicher n1p uptake
            fbpreq = (f_FromTo(i_don_ban) + f_FromTo(i_n4n_ban) &
                    - f_FromTo(i_ban_n4n))*rcnb/rcpb - f_FromTo(i_dop_bap) !benoetigtes P
            !fbpreq kann neg. werden.

            f_FromTo(i_n1p_bap) = min(max(0.,fbpreq),fbpmax)
            f_FromTo(i_bap_n1p) = max(0.,(-fbpreq))

            fbpdiff = max(0.,fbpreq - fbpmax)
            f_FromTo(i_ban_n4n) = f_FromTo(i_ban_n4n) + fbpdiff*rcpb/rcnb
            f_FromTo(i_bac_dic) = f_FromTo(i_bac_dic) + fbpdiff*rcpb

#else  /* #ifdef old_bac */
      ! unbalanced net C flow into bacteria
            fbc = f_FromTo(i_doc_bac) -f_FromTo(i_bac_dic)  &
                 -f_FromTo(i_bac_z1c) -f_FromTo(i_bac_z2c)
      ! unbalanced net N flow into bacteria
            fbn = f_FromTo(i_don_ban) +f_FromTo(i_n4n_ban) -f_FromTo(i_ban_n4n)  &
                 -f_FromTo(i_ban_z1n) -f_FromTo(i_ban_z2n)
            fdc_diff = abs(fbc - fbn*rcnb)

            if (fbc.gt.fbn*rcnb) then ! Aufnahme von Ammonium würde Wachstum stimulieren ...
               if (n4n<=tres_n4n) then ! unterhalb des thresholds findet keine Aufnahme statt,
                  f_FromTo(i_n4n_ban) = 0.
                  ! entsprechend wird respiration erhöht
                  f_FromTo(i_bac_dic)= f_FromTo(i_bac_dic) +fdc_diff
               else ! es wird soviel amonium aufgenommen,wie benötigt wird,
                  f_FromTo(i_n4n_ban)= f_FromTo(i_doc_bac)/rcnb -f_FromTo(i_don_ban)
               endif
            else !more N than C ! -> C-Aufnahme limitiert das Wachstum
               ! zu viel aufgenommenes DON wird als Ammonium ausgeschieden
               f_FromTo(i_ban_n4n) = f_FromTo(i_ban_n4n) +fdc_diff/rcnb
            endif

           ! Nun wird der Bedarf an P berechnet, um der N-Bilanz zu entsprechen.
           ! Für den Fall, dass bereits ausreichend P über DOP aufgenommen wurde, wird dieser Term negativ.
            fbpreq = (f_FromTo(i_don_ban) +f_FromTo(i_n4n_ban) &
                     -f_FromTo(i_ban_n4n))*rcnb/rcpb -f_FromTo(i_dop_bap) !benoetigtes P !fbpreq kann neg. werden.
            if (fbpreq .le. 0) then ! ueberschuessiges P wird als DIP ausgeschieden
               f_FromTo(i_bap_n1p) = abs(fbpreq)
            else   ! fehlendes P wird aus dem DIP-pool aufgenommen
               fbpmax = vb*bap ! Die P-aufnahme kann nicht schneller als vb erfolgen.
               if (n1p<=tres_n1p) fbpmax = 0.0 ! unterhalb des thresholds findet keine n1p-Aufnahme statt,
               f_FromTo(i_n1p_bap) = min(fbpreq,fbpmax)
               ! Gegebenenfalls werden die C- und N-Verluste entsprechend angepasst
               if (fbpreq .gt. fbpmax) then
                  fbpdiff = fbpreq - fbpmax
                  f_FromTo(i_ban_n4n) = f_FromTo(i_ban_n4n) + fbpdiff*rcpb/rcnb
                  f_FromTo(i_bac_dic) = f_FromTo(i_bac_dic) + fbpdiff*rcpb
               endif
            endif
#endif /* #ifdef old_bac */
#endif /* #ifndef exclude_bac */

      !================================================================================
      ! SEMI LABILE DOC
      !================================================================================
            !f_FromTo(i_soc_doc) = real(iexcess)*st(j,k,i,isoc)*soc_rate
            f_FromTo(i_soc_doc) = real(iexcess)*rho_soc*tfac(j,k,i)*soc
            !f_FromTo(i_soc_d1c) = (agg_pcho*soc + agg_tepc*d1c) *soc
            aggregation_soc = (agg_pcho*max(0.,soc-100.) + agg_tepc*(d1c+d2c)) *max(0.,soc-100.)
            frac_d2n_ = d2n/(d1n+d2n)
            f_FromTo(i_soc_d1c) = aggregation_soc * (one-frac_d2n_)
            f_FromTo(i_soc_d2c) = aggregation_soc *      frac_d2n_

      !================================================================================
      ! NITRIFICATION
      !================================================================================
            if (pafr*radsol(j,i)>eps .and. par(j,k,i)>eps) then
              anitdep(j,k,i) = 0.01 * (pafr*radsol(j,i)/par(j,k,i))
            endif
            if (par(j,k,i)<=eps) then
              anitdep(j,k,i) = 1.
            endif
            anitdep(j,k,i) = min(1.,anitdep(j,k,i))
            f_FromTo(i_n4n_n3n) = oswtch(j,k,i)*xknit*n4n*anitdep(j,k,i)*tfac(j,k,i)   !additional oxygen limitation
      !if(i==18.and.j==38.and.k==1)then
      !print*,td,f_n4n_n3n(j,k,i),oswtch(j,k,i),xknit,n4n,anitdep(j,k,i),tfac(j,k,i),' n4n_n3n'  !jpdeb
      !continue
      !endif
      !================================================================================
      ! DENITRIFICATION
      !================================================================================
            f_FromTo(i_n3n_nn2) = 0.5*(one-oswtch(j,k,i))*nswtch(j,k,i)  &
                                            *f_FromTo(i_bac_dic)/rcnb                  !anoxic pel denitri

      !================================================================================
      ! OXYGEN
      !================================================================================
           !f_FromTo(i_p1c_o2o) = f_FromTo(i_n3n_p1n)*(red+2.) +f_FromTo(i_n4n_p1n)*red         !lm
           !f_FromTo(i_p2c_o2o) = f_FromTo(i_n3n_p2n)*(red+2.) +f_FromTo(i_n4n_p2n)*red         !lm
            f_FromTo(i_p1c_o2o) = f_FromTo(i_dic_p1c)
            f_FromTo(i_p2c_o2o) = f_FromTo(i_dic_p2c)
            f_FromTo(i_p3c_o2o) = f_FromTo(i_dic_p3c)
            f_FromTo(i_o2o_z1c) = f_FromTo(i_z1c_dic)
            f_FromTo(i_o2o_z2c) = f_FromTo(i_z2c_dic)
            f_FromTo(i_o2o_n4n) = 2.0*f_FromTo(i_n4n_n3n)                              !oxic    nitrification lm

      !================================================================================
      ! REMINERALISATION
      !================================================================================
      ! pelagic remi
            f_FromTo(i_o2o_bac) = oswtch(j,k,i)*(f_FromTo(i_bac_dic)+f_FromTo(i_doc_dic)) !oxic
      !      when st_o2o<0: 1mol H2S=-1mol o2 (not -0.5mol O2)
            f_FromTo(i_o2o_bac) = f_FromTo(i_o2o_bac)                              &
                                  + (one-oswtch(j,k,i))*(one-nswtch(j,k,i))        &
                                    *( f_FromTo(i_bac_dic)+f_FromTo(i_doc_dic) )       !anoxic2 H2S production

#ifndef fixed_CHL_to_C
      !================================================================================
      ! PROGNOSTIC CHLOROPHYLL
      !================================================================================
#ifdef exclude_p1x
            f_FromTo(i_p1a_p1a) = zero
#else
            f_FromTo(i_p1a_p1a) =  p1a_synth *p1c - decay_p1a *p1a                       &  ! net chlorophyll synthesis
                                 -( f_FromTo(i_p1c_d1c)+f_FromTo(i_p1c_d2c) ) *q_p1a_p1c &  ! aggregation loss
                                 -( f_FromTo(i_p1c_z1c)+f_FromTo(i_p1c_z1c) ) *q_p1a_p1c    ! grazing by heterotrophs
#endif
#ifdef exclude_p2x
            f_FromTo(i_p2a_p2a) = zero
#else
            f_FromTo(i_p2a_p2a) =  p2a_synth *p2c - decay_p2a *p2a                       &  ! net chlorophyll synthesis
                                 -( f_FromTo(i_p2c_d1c)+f_FromTo(i_p2c_d2c) ) *q_p2a_p2c &  ! aggregation loss
                                 -( f_FromTo(i_p2c_z1c)+f_FromTo(i_p2c_z1c) ) *q_p2a_p2c    ! grazing by heterotrophs
#endif
#ifdef exclude_p3x
            f_FromTo(i_p3a_p3a) = zero
#else
            f_FromTo(i_p3a_p3a) =  p3a_synth *p3c - decay_p3a *p3a                       &  ! net chlorophyll synthesis
                                 -( f_FromTo(i_p3c_d1c)+f_FromTo(i_p3c_d2c) ) *q_p3a_p3c &  ! aggregation loss
                                 -( f_FromTo(i_p3c_z1c)+f_FromTo(i_p3c_z1c) ) *q_p3a_p3c    ! grazing by heterotrophs
#endif
#endif
#ifdef TBNT_x1x_test
      !================================================================================
      ! DUMMY FLUXES FOR X1X
      !================================================================================
      ! constant factor in source should be larger than in sink to avoid negative state variable
            f_FromTo(i_x2x_x1x) = 0.2 *float(k0-k+1)/float(k0)*st(j,k,i,ix2x) ! source
            f_FromTo(i_x1x_x2x) = 0.19*float(k0-k+1)/float(k0)*st(j,k,i,ix1x) ! sink
#endif


      !================================================================================
      ! MAP LOCAL PRIVATE FLUXES f_local (mol m-3) TO PULIC ARRAY f_from_to (mol m-2)
      !================================================================================
            do n = 1, max_flux
               f_from_to(j,k,i,n) = f_from_to(j,k,i,n) + f_FromTo(n) *dz(j,k,i)
            enddo

      !================================================================================
      ! SOURCE EQUATIONS
      !================================================================================
      ! please sort entries in following order ...
      ! ix1x= 1,ialk= 2,idic= 3,in3n= 4,in4n= 5,in1p= 6,in5s= 7,ip1c= 8,ip1n= 9,ip1p=10
      ! ip1s=11,ip2c=12,ip2n=13,ip2p=14,ip3c=15,ip3n=16,ip3p=17,ip3k=18,iz1c=19,iz2c=20
      ! ibac=21,id1c=22,id1n=23,id1p=24,id2c=25,id2n=26,id2p=27,id2s=28,id2k=29,isoc=30
      ! idoc=31,idon=32,idop=33,io2o=34,ip1a=35,ip2a=36,ip3a=37
#ifdef TBNT_x1x_test
            sst(j,k,i,ix1x) = sst(j,k,i,ix1x) +f_from_to(j,k,i,i_x2x_x1x) -f_from_to(j,k,i,i_x1x_x2x)
            sst(j,k,i,ix2x) = sst(j,k,i,ix2x) -f_from_to(j,k,i,i_x2x_x1x) +f_from_to(j,k,i,i_x1x_x2x)
#endif
            if(talk_treat==1)then
! print*,'  ', f_from_to(j,k,i,i_d2k_dic), f_from_to(j,k,i,i_psk_dic), f_from_to(j,k,i,i_dic_psk), f_from_to(j,k,i,i_n4n_n3n)
! print*,'  ', f_from_to(j,k,i,i_n3n_p1n), f_from_to(j,k,i,i_n4n_p1n), f_from_to(j,k,i,i_n3n_p2n), f_from_to(j,k,i,i_n4n_p2n)
! print*,'  ', f_from_to(j,k,i,i_n3n_p3n), f_from_to(j,k,i,i_n4n_p3n), f_from_to(j,k,i,i_ban_n4n), f_from_to(j,k,i,i_n4n_ban)
! print*,'  ', f_from_to(j,k,i,i_z1n_n4n), f_from_to(j,k,i,i_z2n_n4n), f_from_to(j,k,i,i_atm_n4n), f_from_to(j,k,i,i_atm_n3n)
! print*,'  ', f_from_to(j,k,i,i_n1p_p1p), f_from_to(j,k,i,i_n1p_p2p), f_from_to(j,k,i,i_n1p_p3p), f_from_to(j,k,i,i_z1p_n1p)
! print*,'  ', f_from_to(j,k,i,i_z2p_n1p), f_from_to(j,k,i,i_n1p_bap), f_from_to(j,k,i,i_bap_n1p)
               sst(j,k,i,ialk) = sst(j,k,i,ialk) &
                                          +2.*(f_from_to(j,k,i,i_d2k_dic) +f_from_to(j,k,i,i_psk_dic)   &
                                              -f_from_to(j,k,i,i_dic_psk) -f_from_to(j,k,i,i_n4n_n3n))  &
                                              +f_from_to(j,k,i,i_n3n_p1n) -f_from_to(j,k,i,i_n4n_p1n)   &
                                              +f_from_to(j,k,i,i_n3n_p2n) -f_from_to(j,k,i,i_n4n_p2n)   &
                                              +f_from_to(j,k,i,i_n3n_p3n) -f_from_to(j,k,i,i_n4n_p3n)   &
                                              +f_from_to(j,k,i,i_ban_n4n) -f_from_to(j,k,i,i_n4n_ban)   &
                                              +f_from_to(j,k,i,i_z1n_n4n) +f_from_to(j,k,i,i_z2n_n4n)   &
                                              +f_from_to(j,k,i,i_atm_n4n) -f_from_to(j,k,i,i_atm_n3n)   &
                                              +f_from_to(j,k,i,i_n1p_p1p) +f_from_to(j,k,i,i_n1p_p2p)   &
                                              +f_from_to(j,k,i,i_n1p_p3p) -f_from_to(j,k,i,i_z1p_n1p)   &
                                              -f_from_to(j,k,i,i_z2p_n1p) +f_from_to(j,k,i,i_n1p_bap)   &
                                              -f_from_to(j,k,i,i_bap_n1p)
            endif
            sst(j,k,i,idic) = sst(j,k,i,idic) +f_from_to(j,k,i,i_bac_dic) +f_from_to(j,k,i,i_z1c_dic)   &
                                              +f_from_to(j,k,i,i_z2c_dic) +f_from_to(j,k,i,i_d2k_dic)   &
                                              -f_from_to(j,k,i,i_dic_p1c) -f_from_to(j,k,i,i_dic_p2c)   &
                                              -f_from_to(j,k,i,i_dic_p3c) -f_from_to(j,k,i,i_dic_p3k)   &
                                              -f_from_to(j,k,i,i_dic_psk) +f_from_to(j,k,i,i_doc_dic)
            sst(j,k,i,in3n) = sst(j,k,i,in3n) +f_from_to(j,k,i,i_n4n_n3n) -f_from_to(j,k,i,i_n3n_p1n)   &
                                              -f_from_to(j,k,i,i_n3n_p2n) -f_from_to(j,k,i,i_n3n_p3n)   &
                                              -f_from_to(j,k,i,i_n3n_nn2)
            sst(j,k,i,in4n) = sst(j,k,i,in4n) +f_from_to(j,k,i,i_ban_n4n) +f_from_to(j,k,i,i_z1n_n4n)   &
                                              +f_from_to(j,k,i,i_z2n_n4n) -f_from_to(j,k,i,i_n4n_p1n)   &
                                              -f_from_to(j,k,i,i_n4n_p2n) -f_from_to(j,k,i,i_n4n_p3n)   &
                                              -f_from_to(j,k,i,i_n4n_ban) -f_from_to(j,k,i,i_n4n_n3n)   &
                                              +f_from_to(j,k,i,i_don_n4n)
            sst(j,k,i,in1p) = sst(j,k,i,in1p) +f_from_to(j,k,i,i_bap_n1p) +f_from_to(j,k,i,i_z1p_n1p)   &
                                              +f_from_to(j,k,i,i_z2p_n1p) -f_from_to(j,k,i,i_n1p_p1p)   &
                                              -f_from_to(j,k,i,i_n1p_p2p) -f_from_to(j,k,i,i_n1p_p3p)   &
                                              -f_from_to(j,k,i,i_n1p_bap) +f_from_to(j,k,i,i_dop_n1p)
            sst(j,k,i,in5s) = sst(j,k,i,in5s) +f_from_to(j,k,i,i_d2s_n5s) -f_from_to(j,k,i,i_n5s_p1s)
#ifndef exclude_p1x
            sst(j,k,i,ip1c) = sst(j,k,i,ip1c) +f_from_to(j,k,i,i_dic_p1c) -f_from_to(j,k,i,i_p1c_d1c)   &
                                              -f_from_to(j,k,i,i_p1c_d2c) -f_from_to(j,k,i,i_p1c_doc)   &
                                              -f_from_to(j,k,i,i_p1c_soc) -f_from_to(j,k,i,i_p1c_z1c)   &
                                              -f_from_to(j,k,i,i_p1c_z2c)
            sst(j,k,i,ip1n) = sst(j,k,i,ip1n) +f_from_to(j,k,i,i_n3n_p1n) +f_from_to(j,k,i,i_n4n_p1n)   &
                                              -f_from_to(j,k,i,i_p1n_d1n) -f_from_to(j,k,i,i_p1n_d2n)   &
                                              -f_from_to(j,k,i,i_p1n_don) -f_from_to(j,k,i,i_p1n_z1n)   &
                                              -f_from_to(j,k,i,i_p1n_z2n)
            sst(j,k,i,ip1p) = sst(j,k,i,ip1p) +f_from_to(j,k,i,i_n1p_p1p) -f_from_to(j,k,i,i_p1p_d1p)   &
                                              -f_from_to(j,k,i,i_p1p_d2p) -f_from_to(j,k,i,i_p1p_dop)   &
                                              -f_from_to(j,k,i,i_p1p_z1p) -f_from_to(j,k,i,i_p1p_z2p)
            sst(j,k,i,ip1s) = sst(j,k,i,ip1s) +f_from_to(j,k,i,i_n5s_p1s) -f_from_to(j,k,i,i_p1s_d2s)
            sst(j,k,i,ip1a) = sst(j,k,i,ip1a) +f_from_to(j,k,i,i_p1a_p1a)
#endif
#ifndef exclude_p2x
            sst(j,k,i,ip2c) = sst(j,k,i,ip2c) +f_from_to(j,k,i,i_dic_p2c) -f_from_to(j,k,i,i_p2c_d1c)   &
                                              -f_from_to(j,k,i,i_p2c_d2c) -f_from_to(j,k,i,i_p2c_doc)   &
                                              -f_from_to(j,k,i,i_p2c_soc) -f_from_to(j,k,i,i_p2c_z1c)   &
                                              -f_from_to(j,k,i,i_p2c_z2c)
            sst(j,k,i,ip2n) = sst(j,k,i,ip2n) +f_from_to(j,k,i,i_n3n_p2n) +f_from_to(j,k,i,i_n4n_p2n)   &
                                              -f_from_to(j,k,i,i_p2n_d1n) -f_from_to(j,k,i,i_p2n_d2n)   &
                                              -f_from_to(j,k,i,i_p2n_don) -f_from_to(j,k,i,i_p2n_z1n)   &
                                              -f_from_to(j,k,i,i_p2n_z2n)
            sst(j,k,i,ip2p) = sst(j,k,i,ip2p) +f_from_to(j,k,i,i_n1p_p2p) -f_from_to(j,k,i,i_p2p_d1p)   &
                                              -f_from_to(j,k,i,i_p2p_d2p) -f_from_to(j,k,i,i_p2p_dop)   &
                                              -f_from_to(j,k,i,i_p2p_z1p) -f_from_to(j,k,i,i_p2p_z2p)
            sst(j,k,i,ip2a) = sst(j,k,i,ip2a) +f_from_to(j,k,i,i_p2a_p2a)
#endif
#ifndef exclude_p3x
            sst(j,k,i,ip3c) = sst(j,k,i,ip3c) +f_from_to(j,k,i,i_dic_p3c) -f_from_to(j,k,i,i_p3c_d1c)   &
                                              -f_from_to(j,k,i,i_p3c_d2c) -f_from_to(j,k,i,i_p3c_doc)   &
                                              -f_from_to(j,k,i,i_p3c_soc) -f_from_to(j,k,i,i_p3c_z1c)   &
                                              -f_from_to(j,k,i,i_p3c_z2c)
            sst(j,k,i,ip3n) = sst(j,k,i,ip3n) +f_from_to(j,k,i,i_n3n_p3n) +f_from_to(j,k,i,i_n4n_p3n)   &
                                              -f_from_to(j,k,i,i_p3n_d1n) -f_from_to(j,k,i,i_p3n_d2n)   &
                                              -f_from_to(j,k,i,i_p3n_don) -f_from_to(j,k,i,i_p3n_z1n)   &
                                              -f_from_to(j,k,i,i_p3n_z2n)
            sst(j,k,i,ip3p) = sst(j,k,i,ip3p) +f_from_to(j,k,i,i_n1p_p3p) +f_from_to(j,k,i,i_dop_p3p)   &
                                              -f_from_to(j,k,i,i_p3p_d1p) -f_from_to(j,k,i,i_p3p_d2p)   &
                                              -f_from_to(j,k,i,i_p3p_dop) -f_from_to(j,k,i,i_p3p_z1p)   &
                                              -f_from_to(j,k,i,i_p3p_z2p)
            sst(j,k,i,ip3k) = sst(j,k,i,ip3k) +f_from_to(j,k,i,i_dic_p3k) -f_from_to(j,k,i,i_p3k_z1c)   &
                                              -f_from_to(j,k,i,i_p3k_z2c) -f_from_to(j,k,i,i_p3k_d2k)
            sst(j,k,i,ip3a) = sst(j,k,i,ip3a) +f_from_to(j,k,i,i_p3a_p3a)
#endif
#ifndef exclude_z1x
            sst(j,k,i,iz1c) = sst(j,k,i,iz1c) +f_from_to(j,k,i,i_p1c_z1c) +f_from_to(j,k,i,i_p2c_z1c)   &
                                              +f_from_to(j,k,i,i_p3c_z1c) +f_from_to(j,k,i,i_bac_z1c)   &
                                              +f_from_to(j,k,i,i_d1c_z1c) -f_from_to(j,k,i,i_z1c_dic)   &
                                              -f_from_to(j,k,i,i_z1c_d1c) -f_from_to(j,k,i,i_z1c_d2c)   &
                                              -f_from_to(j,k,i,i_z1c_doc) -f_from_to(j,k,i,i_z1c_z2c)
#endif
#ifndef exclude_z2x
            sst(j,k,i,iz2c) = sst(j,k,i,iz2c) +f_from_to(j,k,i,i_p1c_z2c) +f_from_to(j,k,i,i_p2c_z2c)   &
                                              +f_from_to(j,k,i,i_p3c_z2c) +f_from_to(j,k,i,i_bac_z2c)   &
                                              +f_from_to(j,k,i,i_d1c_z2c) -f_from_to(j,k,i,i_z2c_dic)   &
                                              -f_from_to(j,k,i,i_z2c_d1c) -f_from_to(j,k,i,i_z2c_d2c)   &
                                              -f_from_to(j,k,i,i_z2c_doc) +f_from_to(j,k,i,i_z1c_z2c)
#endif
#ifndef exclude_bac
            sst(j,k,i,ibac) = sst(j,k,i,ibac) +f_from_to(j,k,i,i_doc_bac) -f_from_to(j,k,i,i_bac_dic)   &
                                              -f_from_to(j,k,i,i_bac_z1c) -f_from_to(j,k,i,i_bac_z2c)
#endif
            sst(j,k,i,id1c) = sst(j,k,i,id1c) +f_from_to(j,k,i,i_p1c_d1c) +f_from_to(j,k,i,i_p2c_d1c)   &
                                              +f_from_to(j,k,i,i_p3c_d1c) +f_from_to(j,k,i,i_z1c_d1c)   &
                                              +f_from_to(j,k,i,i_z2c_d1c) +f_from_to(j,k,i,i_soc_d1c)   &
                                              -f_from_to(j,k,i,i_d1c_z1c) -f_from_to(j,k,i,i_d1c_z2c)   &
                                              -f_from_to(j,k,i,i_d1c_doc)
            sst(j,k,i,id1n) = sst(j,k,i,id1n) +f_from_to(j,k,i,i_p1n_d1n) +f_from_to(j,k,i,i_p2n_d1n)   &
                                              +f_from_to(j,k,i,i_p3n_d1n) +f_from_to(j,k,i,i_z1n_d1n)   &
                                              +f_from_to(j,k,i,i_z2n_d1n) -f_from_to(j,k,i,i_d1n_z1n)   &
                                              -f_from_to(j,k,i,i_d1n_z2n) -f_from_to(j,k,i,i_d1n_don)
            sst(j,k,i,id1p) = sst(j,k,i,id1p) +f_from_to(j,k,i,i_p1p_d1p) +f_from_to(j,k,i,i_p2p_d1p)   &
                                              +f_from_to(j,k,i,i_p3p_d1p) +f_from_to(j,k,i,i_z1p_d1p)   &
                                              +f_from_to(j,k,i,i_z2p_d1p) -f_from_to(j,k,i,i_d1p_z1p)   &
                                              -f_from_to(j,k,i,i_d1p_z2p) -f_from_to(j,k,i,i_d1p_dop)
            sst(j,k,i,id2c) = sst(j,k,i,id2c) +f_from_to(j,k,i,i_p1c_d2c) +f_from_to(j,k,i,i_p2c_d2c)   &
                                              +f_from_to(j,k,i,i_p3c_d2c) +f_from_to(j,k,i,i_z1c_d2c)   &
                                              +f_from_to(j,k,i,i_z2c_d2c) +f_from_to(j,k,i,i_soc_d2c)   &
                                              -f_from_to(j,k,i,i_d2c_doc)
            sst(j,k,i,id2n) = sst(j,k,i,id2n) +f_from_to(j,k,i,i_p1n_d2n) +f_from_to(j,k,i,i_p2n_d2n)   &
                                              +f_from_to(j,k,i,i_p3n_d2n) +f_from_to(j,k,i,i_z1n_d2n)   &
                                              +f_from_to(j,k,i,i_z2n_d2n) -f_from_to(j,k,i,i_d2n_don)
            sst(j,k,i,id2p) = sst(j,k,i,id2p) +f_from_to(j,k,i,i_p1p_d2p) +f_from_to(j,k,i,i_p2p_d2p)   &
                                              +f_from_to(j,k,i,i_p3p_d2p) +f_from_to(j,k,i,i_z1p_d2p)   &
                                              +f_from_to(j,k,i,i_z2p_d2p) -f_from_to(j,k,i,i_d2p_dop)
            sst(j,k,i,id2s) = sst(j,k,i,id2s) +f_from_to(j,k,i,i_p1s_d2s) -f_from_to(j,k,i,i_d2s_n5s)
            sst(j,k,i,id2k) = sst(j,k,i,id2k) +f_from_to(j,k,i,i_psk_d2k) +f_from_to(j,k,i,i_p3k_d2k)   &
                                              -f_from_to(j,k,i,i_d2k_dic)
            sst(j,k,i,isoc) = sst(j,k,i,isoc) +f_from_to(j,k,i,i_p1c_soc) +f_from_to(j,k,i,i_p2c_soc)   &
                                              +f_from_to(j,k,i,i_p3c_soc) -f_from_to(j,k,i,i_soc_doc)   &
                                              -f_from_to(j,k,i,i_soc_d1c) -f_from_to(j,k,i,i_soc_d2c)
            sst(j,k,i,idoc) = sst(j,k,i,idoc) +f_from_to(j,k,i,i_p1c_doc) +f_from_to(j,k,i,i_p2c_doc)   &
                                              +f_from_to(j,k,i,i_p3c_doc) +f_from_to(j,k,i,i_soc_doc)   &
                                              +f_from_to(j,k,i,i_z1c_doc) +f_from_to(j,k,i,i_z2c_doc)   &
                                              +f_from_to(j,k,i,i_d1c_doc) +f_from_to(j,k,i,i_d2c_doc)   &
                                              -f_from_to(j,k,i,i_doc_bac) -f_from_to(j,k,i,i_doc_dic)

! if(i==79.and.j==11.and.k==9)then
! write(log_unit,'(9e15.6," doc79119")')    f_from_to(j,k,i,i_p1c_doc),f_from_to(j,k,i,i_p2c_doc),    &
!                                           f_from_to(j,k,i,i_p3c_doc),f_from_to(j,k,i,i_soc_doc),    &
!                                           f_from_to(j,k,i,i_z1c_doc),f_from_to(j,k,i,i_z2c_doc),    &
!                                           f_from_to(j,k,i,i_d1c_doc),f_from_to(j,k,i,i_d2c_doc),    &
!                                           f_from_to(j,k,i,i_doc_bac)
! endif
            sst(j,k,i,idon) = sst(j,k,i,idon) +f_from_to(j,k,i,i_p1n_don) +f_from_to(j,k,i,i_p2n_don)   &
                                              +f_from_to(j,k,i,i_p3n_don) +f_from_to(j,k,i,i_z1n_don)   &
                                              +f_from_to(j,k,i,i_z2n_don) +f_from_to(j,k,i,i_d1n_don)   &
                                              +f_from_to(j,k,i,i_d2n_don) -f_from_to(j,k,i,i_don_ban)   &
                                              -f_from_to(j,k,i,i_don_n4n)
            sst(j,k,i,idop) = sst(j,k,i,idop) +f_from_to(j,k,i,i_p1p_dop) +f_from_to(j,k,i,i_p2p_dop)   &
                                              +f_from_to(j,k,i,i_p3p_dop) +f_from_to(j,k,i,i_z1p_dop)   &
                                              +f_from_to(j,k,i,i_z2p_dop) +f_from_to(j,k,i,i_d1p_dop)   &
                                              +f_from_to(j,k,i,i_d2p_dop) -f_from_to(j,k,i,i_dop_bap)   &
                                              -f_from_to(j,k,i,i_dop_p3p) -f_from_to(j,k,i,i_dop_n1p)
            sst(j,k,i,io2o) = sst(j,k,i,io2o) +f_from_to(j,k,i,i_p1c_o2o) +f_from_to(j,k,i,i_p2c_o2o)   &
                                              +f_from_to(j,k,i,i_p3c_o2o) -f_from_to(j,k,i,i_o2o_n4n)   &
                                              -f_from_to(j,k,i,i_o2o_z1c) -f_from_to(j,k,i,i_o2o_z2c)   &
                                              -f_from_to(j,k,i,i_o2o_bac)
         enddo
      enddo
   enddo

   if (ierr/=0) then
      write( error_message, '("[",i2.2,"] ERROR - do_biogeo_recom: ",i3)') myID,ierr
      call stop_ecoham(ierr, msg=error_message)
   end if
   return

   end subroutine do_biogeo

!=======================================================================
!
! !INTERFACE:
   subroutine cum_biofunctions(dt_,ierr)
#define SUBROUTINE_NAME 'cum_biofunctions'
!
! !DESCRIPTION:
! cumulate functional parameters to daily values
!
! !USES:
   implicit none
!
! !INPUT PARAMETERS:
   real, intent(in)        :: dt_
   integer, intent(inout)  :: ierr
!
! !LOCAL VARIABLES:
   integer               :: i, j, k, k0
!
!-----------------------------------------------------------------------
#include "call-trace.inc"

   select case (isw_dim) !jp
      case (3) !3D-mode
         where(ixistz==1) d_fPAR       = d_fPAR       + fPAR*dt_
#ifndef exclude_p1x
         where(ixistz==1) d_lim_p1_n1p = d_lim_p1_n1p + lim_p1_n1p*dt_
         where(ixistz==1) d_lim_p1_n3n = d_lim_p1_n3n + lim_p1_n3n*dt_
         where(ixistz==1) d_lim_p1_n4n = d_lim_p1_n4n + lim_p1_n4n*dt_
         where(ixistz==1) d_lim_p1_n5s = d_lim_p1_n5s + lim_p1_n5s*dt_
         where(ixistz==1) d_lim_p1_IR  = d_lim_p1_IR  + lim_p1_IR *dt_
         where(ixistz==1) d_lim_p1_RCN = d_lim_p1_RCN + lim_p1_RCN*dt_
         where(ixistz==1) d_lim_p1_RNC = d_lim_p1_RNC + lim_p1_RNC*dt_
         where(ixistz==1) d_lim_p1_RPC = d_lim_p1_RPC + lim_p1_RPC*dt_
         where(ixistz==1) d_lim_p1_RPN = d_lim_p1_RPN + lim_p1_RPN*dt_
         where(ixistz==1) d_lim_p1_RNP = d_lim_p1_RNP + lim_p1_RNP*dt_
         where(ixistz==1) d_lim_p1_RSN = d_lim_p1_RSN + lim_p1_RSN*dt_
         where(ixistz==1) d_lim_p1_RNS = d_lim_p1_RNS + lim_p1_RNS*dt_
         !MK calculate daily mean from Chl:C
         where(ixistz==1) d_chl_p1c    = d_chl_p1c    + q_chl_p1c*dt_
#endif
#ifndef exclude_p2x
         where(ixistz==1) d_lim_p2_n1p = d_lim_p2_n1p + lim_p2_n1p*dt_
         where(ixistz==1) d_lim_p2_n3n = d_lim_p2_n3n + lim_p2_n3n*dt_
         where(ixistz==1) d_lim_p2_n4n = d_lim_p2_n4n + lim_p2_n4n*dt_
         where(ixistz==1) d_lim_p2_IR  = d_lim_p2_IR  + lim_p2_IR *dt_
         where(ixistz==1) d_lim_p2_RCN = d_lim_p2_RCN + lim_p2_RCN*dt_
         where(ixistz==1) d_lim_p2_RNC = d_lim_p2_RNC + lim_p2_RNC*dt_
         where(ixistz==1) d_lim_p2_RPC = d_lim_p2_RPC + lim_p2_RPC*dt_
         where(ixistz==1) d_lim_p2_RPN = d_lim_p2_RPN + lim_p2_RPN*dt_
         where(ixistz==1) d_lim_p2_RNP = d_lim_p2_RNP + lim_p2_RNP*dt_
         where(ixistz==1) d_chl_p2c    = d_chl_p2c    + q_chl_p2c *dt_
#endif
#ifndef exclude_p3x
         where(ixistz==1) d_lim_p3_n1p = d_lim_p3_n1p + lim_p3_n1p*dt_
         where(ixistz==1) d_lim_p3_n3n = d_lim_p3_n3n + lim_p3_n3n*dt_
         where(ixistz==1) d_lim_p3_n4n = d_lim_p3_n4n + lim_p3_n4n*dt_
         where(ixistz==1) d_lim_p3_dop = d_lim_p3_dop + lim_p3_dop*dt_
         where(ixistz==1) d_lim_p3_IR  = d_lim_p3_IR  + lim_p3_IR *dt_
         where(ixistz==1) d_lim_p3_RCN = d_lim_p3_RCN + lim_p3_RCN*dt_
         where(ixistz==1) d_lim_p3_RNC = d_lim_p3_RNC + lim_p3_RNC*dt_
         where(ixistz==1) d_lim_p3_RPC = d_lim_p3_RPC + lim_p3_RPC*dt_
         where(ixistz==1) d_lim_p3_RPN = d_lim_p3_RPN + lim_p3_RPN*dt_
         where(ixistz==1) d_lim_p3_RNP = d_lim_p3_RNP + lim_p3_RNP*dt_
         where(ixistz==1) d_chl_p3c    = d_chl_p3c    + q_chl_p3c*dt_
#endif
      case (1) !1D-mode
         i = iStartCalc
         j = jStartCalc
         k0 = k_index(j,i)
         do k = 1, k0
            d_fPAR(j,k,i)       = d_fPAR(j,k,i)       + fPAR(j,k,i)*dt_
#ifndef exclude_p1x
            d_lim_p1_n1p(j,k,i) = d_lim_p1_n1p(j,k,i) + lim_p1_n1p(j,k,i)*dt_
            d_lim_p1_n3n(j,k,i) = d_lim_p1_n3n(j,k,i) + lim_p1_n3n(j,k,i)*dt_
            d_lim_p1_n4n(j,k,i) = d_lim_p1_n4n(j,k,i) + lim_p1_n4n(j,k,i)*dt_
            d_lim_p1_n5s(j,k,i) = d_lim_p1_n5s(j,k,i) + lim_p1_n5s(j,k,i)*dt_
            d_lim_p1_IR (j,k,i) = d_lim_p1_IR (j,k,i) + lim_p1_IR (j,k,i)*dt_
            d_lim_p1_RCN(j,k,i) = d_lim_p1_RCN(j,k,i) + lim_p1_RCN(j,k,i)*dt_
            d_lim_p1_RNC(j,k,i) = d_lim_p1_RNC(j,k,i) + lim_p1_RNC(j,k,i)*dt_
            d_lim_p1_RPC(j,k,i) = d_lim_p1_RPC(j,k,i) + lim_p1_RPC(j,k,i)*dt_
            d_lim_p1_RPN(j,k,i) = d_lim_p1_RPN(j,k,i) + lim_p1_RPN(j,k,i)*dt_
            d_lim_p1_RNP(j,k,i) = d_lim_p1_RNP(j,k,i) + lim_p1_RNP(j,k,i)*dt_
            d_lim_p1_RSN(j,k,i) = d_lim_p1_RSN(j,k,i) + lim_p1_RSN(j,k,i)*dt_
            d_lim_p1_RNS(j,k,i) = d_lim_p1_RNS(j,k,i) + lim_p1_RNS(j,k,i)*dt_
            !MK calculate daily mean from Chl:C
            d_chl_p1c(j,k,i)    = d_chl_p1c(j,k,i)    + q_chl_p1c(j,k,i)*dt_
#endif
#ifndef exclude_p2x
            d_lim_p2_n1p(j,k,i) = d_lim_p2_n1p(j,k,i) + lim_p2_n1p(j,k,i)*dt_
            d_lim_p2_n3n(j,k,i) = d_lim_p2_n3n(j,k,i) + lim_p2_n3n(j,k,i)*dt_
            d_lim_p2_n4n(j,k,i) = d_lim_p2_n4n(j,k,i) + lim_p2_n4n(j,k,i)*dt_
            d_lim_p2_IR (j,k,i) = d_lim_p2_IR (j,k,i) + lim_p2_IR (j,k,i)*dt_
            d_lim_p2_RCN(j,k,i) = d_lim_p2_RCN(j,k,i) + lim_p2_RCN(j,k,i)*dt_
            d_lim_p2_RNC(j,k,i) = d_lim_p2_RNC(j,k,i) + lim_p2_RNC(j,k,i)*dt_
            d_lim_p2_RPC(j,k,i) = d_lim_p2_RPC(j,k,i) + lim_p2_RPC(j,k,i)*dt_
            d_lim_p2_RPN(j,k,i) = d_lim_p2_RPN(j,k,i) + lim_p2_RPN(j,k,i)*dt_
            d_lim_p2_RNP(j,k,i) = d_lim_p2_RNP(j,k,i) + lim_p2_RNP(j,k,i)*dt_
            d_chl_p2c(j,k,i)    = d_chl_p2c(j,k,i)    + q_chl_p2c(j,k,i)*dt_
#endif
#ifndef exclude_p3x
            d_lim_p3_n1p(j,k,i) = d_lim_p3_n1p(j,k,i) + lim_p3_n1p(j,k,i)*dt_
            d_lim_p3_n3n(j,k,i) = d_lim_p3_n3n(j,k,i) + lim_p3_n3n(j,k,i)*dt_
            d_lim_p3_n4n(j,k,i) = d_lim_p3_n4n(j,k,i) + lim_p3_n4n(j,k,i)*dt_
            d_lim_p3_dop(j,k,i) = d_lim_p3_dop(j,k,i) + lim_p3_dop(j,k,i)*dt_
            d_lim_p3_IR (j,k,i) = d_lim_p3_IR (j,k,i) + lim_p3_IR (j,k,i)*dt_
            d_lim_p3_RCN(j,k,i) = d_lim_p3_RCN(j,k,i) + lim_p3_RCN(j,k,i)*dt_
            d_lim_p3_RNC(j,k,i) = d_lim_p3_RNC(j,k,i) + lim_p3_RNC(j,k,i)*dt_
            d_lim_p3_RPC(j,k,i) = d_lim_p3_RPC(j,k,i) + lim_p3_RPC(j,k,i)*dt_
            d_lim_p3_RPN(j,k,i) = d_lim_p3_RPN(j,k,i) + lim_p3_RPN(j,k,i)*dt_
            d_lim_p3_RNP(j,k,i) = d_lim_p3_RNP(j,k,i) + lim_p3_RNP(j,k,i)*dt_
            d_chl_p3c(j,k,i)    = d_chl_p3c(j,k,i)    + q_chl_p3c(j,k,i)*dt_
#endif
         enddo
   end select

   if (ierr/=0) then
      write( error_message, '("[",i2.2,"] ERROR - cum_biofunctions: ",i3)') myID,ierr
      call stop_ecoham(ierr, msg=error_message)
   end if
   return

   end subroutine cum_biofunctions

!=======================================================================
!
! !INTERFACE:
   subroutine empty_biofunctions(ierr)
#define SUBROUTINE_NAME 'empty_biofunctions'
!
! !DESCRIPTION:
! cumulate functional parameters to daily values
!
! !USES:
   implicit none
!
! !INPUT PARAMETERS:
   integer, intent(inout)  :: ierr
!
! !LOCAL VARIABLES:
   integer               :: i, j, k, k0
!
!-----------------------------------------------------------------------
#include "call-trace.inc"

   select case (isw_dim) !jp
      case (3) !3D-mode
         where(ixistz==1) d_fPar    = zero
#ifndef exclude_p1x
         where(ixistz==1) d_lim_p1_n1p = zero
         where(ixistz==1) d_lim_p1_n3n = zero
         where(ixistz==1) d_lim_p1_n4n = zero
         where(ixistz==1) d_lim_p1_n5s = zero
         where(ixistz==1) d_lim_p1_IR  = zero
         where(ixistz==1) d_lim_p1_RCN = zero
         where(ixistz==1) d_lim_p1_RNC = zero
         where(ixistz==1) d_lim_p1_RPC = zero
         where(ixistz==1) d_lim_p1_RPN = zero
         where(ixistz==1) d_lim_p1_RNP = zero
         where(ixistz==1) d_lim_p1_RSN = zero
         where(ixistz==1) d_lim_p1_RNS = zero
#endif
#ifndef exclude_p2x
         where(ixistz==1) d_lim_p2_n1p = zero
         where(ixistz==1) d_lim_p2_n3n = zero
         where(ixistz==1) d_lim_p2_n4n = zero
         where(ixistz==1) d_lim_p2_IR  = zero
         where(ixistz==1) d_lim_p2_RCN = zero
         where(ixistz==1) d_lim_p2_RNC = zero
         where(ixistz==1) d_lim_p2_RPC = zero
         where(ixistz==1) d_lim_p2_RPN = zero
         where(ixistz==1) d_lim_p2_RNP = zero
#endif
#ifndef exclude_p3x
         where(ixistz==1) d_lim_p3_n1p = zero
         where(ixistz==1) d_lim_p3_n3n = zero
         where(ixistz==1) d_lim_p3_n4n = zero
         where(ixistz==1) d_lim_p3_dop = zero
         where(ixistz==1) d_lim_p3_IR  = zero
         where(ixistz==1) d_lim_p3_RCN = zero
         where(ixistz==1) d_lim_p3_RNC = zero
         where(ixistz==1) d_lim_p3_RPC = zero
         where(ixistz==1) d_lim_p3_RPN = zero
         where(ixistz==1) d_lim_p3_RNP = zero
#endif
         where(ixistz==1) d_chl_p1c = zero
         where(ixistz==1) d_chl_p2c = zero
         where(ixistz==1) d_chl_p3c = zero
      case (1) !1D-mode
         i = iStartCalc
         j = jStartCalc
         k0 = k_index(j,i)
         do k = 1, k0
            d_fPAR(j,k,i)    = zero
#ifndef exclude_p1x
            d_lim_p1_n1p(j,k,i) = zero
            d_lim_p1_n3n(j,k,i) = zero
            d_lim_p1_n4n(j,k,i) = zero
            d_lim_p1_n5s(j,k,i) = zero
            d_lim_p1_IR (j,k,i) = zero
            d_lim_p1_RCN(j,k,i) = zero
            d_lim_p1_RNC(j,k,i) = zero
            d_lim_p1_RPC(j,k,i) = zero
            d_lim_p1_RPN(j,k,i) = zero
            d_lim_p1_RNP(j,k,i) = zero
            d_lim_p1_RSN(j,k,i) = zero
            d_lim_p1_RNS(j,k,i) = zero
#endif
#ifndef exclude_p2x
            d_lim_p2_n1p(j,k,i) = zero
            d_lim_p2_n3n(j,k,i) = zero
            d_lim_p2_n4n(j,k,i) = zero
            d_lim_p2_RCN(j,k,i) = zero
            d_lim_p2_RNC(j,k,i) = zero
            d_lim_p2_RPC(j,k,i) = zero
            d_lim_p2_RPN(j,k,i) = zero
            d_lim_p2_RNP(j,k,i) = zero
#endif
#ifndef exclude_p3x
            d_lim_p3_n1p(j,k,i) = zero
            d_lim_p3_n3n(j,k,i) = zero
            d_lim_p3_n4n(j,k,i) = zero
            d_lim_p3_dop(j,k,i) = zero
            d_lim_p3_IR (j,k,i) = zero
            d_lim_p3_RCN(j,k,i) = zero
            d_lim_p3_RNC(j,k,i) = zero
            d_lim_p3_RPC(j,k,i) = zero
            d_lim_p3_RPN(j,k,i) = zero
            d_lim_p3_RNP(j,k,i) = zero
#endif
            d_chl_p1c(j,k,i) = zero
            d_chl_p2c(j,k,i) = zero
            d_chl_p3c(j,k,i) = zero
         enddo
   end select

   if (ierr/=0) then
      write( error_message, '("[",i2.2,"] ERROR - empty_biofunctions: ",i3)') myID,ierr
      call stop_ecoham(ierr, msg=error_message)
   end if
   return

   end subroutine empty_biofunctions

!=======================================================================
!
! !INTERFACE:
   subroutine allocate_arrays_biogeo(ierr)
#define SUBROUTINE_NAME 'allocate_arrays_biogeo_recom'
!
! !DESCRIPTION:
!
! !USES:
   implicit none
!
! !OUTPUT PARAMETERS:
   integer, intent(inout) :: ierr
!
! !LOCAL VARIABLES:
!   integer       :: i,j,k,k0,n
!
!-------------------------------------------------------------------------
#include "call-trace.inc"

   !initialize arrays
   allocate(         tfac(jmax,kmax,iStartD:iEndD) );    tfac      = zero
   allocate(      tfac_p1(jmax,kmax,iStartD:iEndD) );    tfac_p1   = zero
   allocate(      tfac_p2(jmax,kmax,iStartD:iEndD) );    tfac_p2   = zero
   allocate(      tfac_p3(jmax,kmax,iStartD:iEndD) );    tfac_p3   = zero
   allocate(      tfac_z1(jmax,kmax,iStartD:iEndD) );    tfac_z1   = zero
   allocate(      tfac_z2(jmax,kmax,iStartD:iEndD) );    tfac_z2   = zero

   allocate(         fPAR(jmax,kmax,iStartD:iEndD) );    fPAR      = zero
   allocate(       d_fPAR(jmax,kmax,iStartD:iEndD) );    d_fPAR    = zero
   allocate(    q_chl_p1c(jmax,kmax,iStartD:iEndD) );    q_chl_p1c = zero
   allocate(    q_chl_p2c(jmax,kmax,iStartD:iEndD) );    q_chl_p2c = zero
   allocate(    q_chl_p3c(jmax,kmax,iStartD:iEndD) );    q_chl_p3c = zero
   allocate(    d_chl_p1c(jmax,kmax,iStartD:iEndD) );    d_chl_p1c = zero
   allocate(    d_chl_p2c(jmax,kmax,iStartD:iEndD) );    d_chl_p2c = zero
   allocate(    d_chl_p3c(jmax,kmax,iStartD:iEndD) );    d_chl_p3c = zero

#ifndef exclude_p1x
   ! p1x - diatoms
   allocate(   lim_p1_n1p(jmax,kmax,iStartD:iEndD) );    lim_p1_n1p   = zero
   allocate(   lim_p1_n3n(jmax,kmax,iStartD:iEndD) );    lim_p1_n3n   = zero
   allocate(   lim_p1_n4n(jmax,kmax,iStartD:iEndD) );    lim_p1_n4n   = zero
   allocate(   lim_p1_n5s(jmax,kmax,iStartD:iEndD) );    lim_p1_n5s   = zero
   allocate(   lim_p1_IR (jmax,kmax,iStartD:iEndD) );    lim_p1_IR    = zero
   allocate(   lim_p1_RCN(jmax,kmax,iStartD:iEndD) );    lim_p1_RCN   = zero
   allocate(   lim_p1_RNC(jmax,kmax,iStartD:iEndD) );    lim_p1_RNC   = zero
   allocate(   lim_p1_RPC(jmax,kmax,iStartD:iEndD) );    lim_p1_RPC   = zero
   allocate(   lim_p1_RPN(jmax,kmax,iStartD:iEndD) );    lim_p1_RPN   = zero
   allocate(   lim_p1_RNP(jmax,kmax,iStartD:iEndD) );    lim_p1_RNP   = zero
   allocate(   lim_p1_RSN(jmax,kmax,iStartD:iEndD) );    lim_p1_RSN   = zero
   allocate(   lim_p1_RNS(jmax,kmax,iStartD:iEndD) );    lim_p1_RNS   = zero
   allocate( d_lim_p1_n1p(jmax,kmax,iStartD:iEndD) );    d_lim_p1_n1p = zero
   allocate( d_lim_p1_n3n(jmax,kmax,iStartD:iEndD) );    d_lim_p1_n3n = zero
   allocate( d_lim_p1_n4n(jmax,kmax,iStartD:iEndD) );    d_lim_p1_n4n = zero
   allocate( d_lim_p1_n5s(jmax,kmax,iStartD:iEndD) );    d_lim_p1_n5s = zero
   allocate( d_lim_p1_IR (jmax,kmax,iStartD:iEndD) );    d_lim_p1_IR  = zero
   allocate( d_lim_p1_RCN(jmax,kmax,iStartD:iEndD) );    d_lim_p1_RCN = zero
   allocate( d_lim_p1_RNC(jmax,kmax,iStartD:iEndD) );    d_lim_p1_RNC = zero
   allocate( d_lim_p1_RPC(jmax,kmax,iStartD:iEndD) );    d_lim_p1_RPC = zero
   allocate( d_lim_p1_RPN(jmax,kmax,iStartD:iEndD) );    d_lim_p1_RPN = zero
   allocate( d_lim_p1_RNP(jmax,kmax,iStartD:iEndD) );    d_lim_p1_RNP = zero
   allocate( d_lim_p1_RSN(jmax,kmax,iStartD:iEndD) );    d_lim_p1_RSN = zero
   allocate( d_lim_p1_RNS(jmax,kmax,iStartD:iEndD) );    d_lim_p1_RNS = zero
#endif
#ifndef exclude_p2x
   ! p2x - flagellates
   allocate(   lim_p2_n1p(jmax,kmax,iStartD:iEndD) );    lim_p2_n1p   = zero
   allocate(   lim_p2_n3n(jmax,kmax,iStartD:iEndD) );    lim_p2_n3n   = zero
   allocate(   lim_p2_n4n(jmax,kmax,iStartD:iEndD) );    lim_p2_n4n   = zero
   allocate(   lim_p2_IR (jmax,kmax,iStartD:iEndD) );    lim_p2_IR    = zero
   allocate(   lim_p2_RCN(jmax,kmax,iStartD:iEndD) );    lim_p2_RCN   = zero
   allocate(   lim_p2_RNC(jmax,kmax,iStartD:iEndD) );    lim_p2_RNC   = zero
   allocate(   lim_p2_RPC(jmax,kmax,iStartD:iEndD) );    lim_p2_RPC   = zero
   allocate(   lim_p2_RPN(jmax,kmax,iStartD:iEndD) );    lim_p2_RPN   = zero
   allocate(   lim_p2_RNP(jmax,kmax,iStartD:iEndD) );    lim_p2_RNP   = zero
   allocate( d_lim_p2_n1p(jmax,kmax,iStartD:iEndD) );    d_lim_p2_n1p = zero
   allocate( d_lim_p2_n3n(jmax,kmax,iStartD:iEndD) );    d_lim_p2_n3n = zero
   allocate( d_lim_p2_n4n(jmax,kmax,iStartD:iEndD) );    d_lim_p2_n4n = zero
   allocate( d_lim_p2_IR (jmax,kmax,iStartD:iEndD) );    d_lim_p2_IR  = zero
   allocate( d_lim_p2_RCN(jmax,kmax,iStartD:iEndD) );    d_lim_p2_RCN = zero
   allocate( d_lim_p2_RNC(jmax,kmax,iStartD:iEndD) );    d_lim_p2_RNC = zero
   allocate( d_lim_p2_RPC(jmax,kmax,iStartD:iEndD) );    d_lim_p2_RPC = zero
   allocate( d_lim_p2_RPN(jmax,kmax,iStartD:iEndD) );    d_lim_p2_RPN = zero
   allocate( d_lim_p2_RNP(jmax,kmax,iStartD:iEndD) );    d_lim_p2_RNP = zero
#endif
#ifndef exclude_p3x
   ! p3x - 3rd phytoplankton group
   allocate(          fp3(jmax,kmax,iStartD:iEndD) );    fp3          = zero
   allocate(         fp3k(jmax,kmax,iStartD:iEndD) );    fp3k         = zero
   allocate(   lim_p3_n1p(jmax,kmax,iStartD:iEndD) );    lim_p3_n1p   = zero
   allocate(   lim_p3_n3n(jmax,kmax,iStartD:iEndD) );    lim_p3_n3n   = zero
   allocate(   lim_p3_n4n(jmax,kmax,iStartD:iEndD) );    lim_p3_n4n   = zero
   allocate(   lim_p3_dop(jmax,kmax,iStartD:iEndD) );    lim_p3_dop   = zero
   allocate(   lim_p3_IR (jmax,kmax,iStartD:iEndD) );    lim_p3_IR    = zero
   allocate(   lim_p3_RCN(jmax,kmax,iStartD:iEndD) );    lim_p3_RCN   = zero
   allocate(   lim_p3_RNC(jmax,kmax,iStartD:iEndD) );    lim_p3_RNC   = zero
   allocate(   lim_p3_RPC(jmax,kmax,iStartD:iEndD) );    lim_p3_RPC   = zero
   allocate(   lim_p3_RPN(jmax,kmax,iStartD:iEndD) );    lim_p3_RPN   = zero
   allocate(   lim_p3_RNP(jmax,kmax,iStartD:iEndD) );    lim_p3_RNP   = zero
   allocate( d_lim_p3_n1p(jmax,kmax,iStartD:iEndD) );    d_lim_p3_n1p = zero
   allocate( d_lim_p3_n3n(jmax,kmax,iStartD:iEndD) );    d_lim_p3_n3n = zero
   allocate( d_lim_p3_n4n(jmax,kmax,iStartD:iEndD) );    d_lim_p3_n4n = zero
   allocate( d_lim_p3_dop(jmax,kmax,iStartD:iEndD) );    d_lim_p3_dop = zero
   allocate( d_lim_p3_IR (jmax,kmax,iStartD:iEndD) );    d_lim_p3_IR  = zero
   allocate( d_lim_p3_RCN(jmax,kmax,iStartD:iEndD) );    d_lim_p3_RCN = zero
   allocate( d_lim_p3_RNC(jmax,kmax,iStartD:iEndD) );    d_lim_p3_RNC = zero
   allocate( d_lim_p3_RPC(jmax,kmax,iStartD:iEndD) );    d_lim_p3_RPC = zero
   allocate( d_lim_p3_RPN(jmax,kmax,iStartD:iEndD) );    d_lim_p3_RPN = zero
   allocate( d_lim_p3_RNP(jmax,kmax,iStartD:iEndD) );    d_lim_p3_RNP = zero
#endif

   allocate(        omega(jmax,kmax,iStartD:iEndD) );    omega     = zero
   allocate(      anitdep(jmax,kmax,iStartD:iEndD) );    anitdep   = zero

   if (ierr/=0) then
      write( error_message, '("[",i2.2,"] ERROR - allocate_arrays_biogeo_recom: ",i3)') myID,ierr
      call stop_ecoham(ierr, msg=error_message)
   end if
   return

   end subroutine allocate_arrays_biogeo

!=======================================================================
!
! !INTERFACE:
   subroutine uptake_recomCNP(IR, mu_max, f_T, limN, limP,      &
                              qNC, QNCuptake, QNCmin, QNCmax,   &
                              qPN, QPNuptake, QPNmin, QPNmax,   &
                              alpha, theta, theta_max,          &
                              mu_C, mu_N, mu_P, mu_chl, f_IR,   &
                              f_RCN, f_RNC, f_RPC, f_RPN, f_RNP, ierr)
!
! !DESCRIPTION:
! Here, the local photosynthetic production is calculated
!
! !USES:
   implicit none
!
! !INPUT VARIABLES & PARAMETERS:
   real, intent(in)     :: IR, mu_max, f_T, limN, limP
   real, intent(in)     :: qNC, QNCuptake, QNCmin, QNCmax
   real, intent(in)     :: qPN, QPNuptake, QPNmin, QPNmax
   real, intent(in)     :: alpha, theta, theta_max
!
! !OUTPUT PARAMETERS:
   real, intent(out)    :: mu_C      ! [molC molC^-1 s^-1] - C-related growth
   real, intent(out)    :: mu_N      ! [molN molC^-1 s^-1] - C-related N-uptake
   real, intent(out)    :: mu_P      ! [molP molN^-1 s^-1] - N-related P-uptake
   real, intent(out)    :: mu_chl    ! [mgChla mmolN^-1]   - N-related Chl-synthesis
   real, intent(out)    :: f_IR      ! limitation factor due to light
   real, intent(out)    :: f_RCN     ! limitation factor on C-uptake due to N-resources
   real, intent(out)    :: f_RNC     ! limitation factor on N-uptake due to C-resources
   real, intent(out)    :: f_RPC     ! limitation factor on P-uptake due to C-resources
   real, intent(out)    :: f_RPN     ! limitation factor on P-uptake due to N-resources
   real, intent(out)    :: f_RNP     ! limitation factor on N-uptake due to P-resources
   integer, intent(inout)  :: ierr
!
! !REVISION HISTORY:
!  Original author(s): Markus Kreus
!
! !LOCAL VARIABLES:
   real                 :: Pmax!, dummy
!
!-----------------------------------------------------------------------

   ! to get rid of warning: Unused dummy argument 'qpc'
   !dummy = qPC + QPCmax + sigmaNP + sigmaPC
#ifdef debug_CNP_functions
print*,'########### subroutine uptake_recomCNP - intent(in):  ###########'
write(*,'(''IR, mu_max, f_T, limN, limP       :'',5e20.10)') IR, mu_max, f_T, limN, limP
write(*,'(''qNC, QNCuptake, QNCmin, QNCmax    :'',5e20.10)') qNC, QNCuptake, QNCmin, QNCmax
write(*,'(''qPN, QPNuptake,QPNmin, QPNmax     :'',5e20.10)') qPN, QPNuptake,QPNmin, QPNmax
write(*,'(''alpha, theta, theta_max           :'',5e20.10)') alpha, theta, theta_max

   ! check whether cell-quota are within valid range
   !if (qNC<QNCmin .or. qNC>QNCmax) then
   if (1./qNC>50 .or. 1./qNC<0.5) then
!       ierr = 1
!       write( error_message, '("[",i2.2,"] ERROR - C:N-quota out of valid range (min/max): ",3f7.1)') &
!                                          myID, 1./QNCmax, 1./QNCmin, 1./qNC
!       call stop_ecoham(ierr, msg=error_message)
      write( long_msg, '("[",i2.2,"] WARNING - C:N-quota out of valid range (min/max): ",3f7.1)') &
                                         myID, 1./QNCmax, 1./QNCmin, 1./qNC
      call write_log_message(long_msg); long_msg = ''
   endif
   !if (qPN<QPNmin .or. qPN>QPNmax) then
   if (1./qPN>100 .or. 1./qPN<0.5) then
!       ierr = 1
!       write( error_message, '("[",i2.2,"] ERROR - N:P-quota out of valid range (min/max): ",3f7.1)') &
!                                          myID, 1./QPNmax, 1./QPNmin, 1./qPN
!       call stop_ecoham(ierr, msg=error_message)
      write( long_msg, '("[",i2.2,"] WARNING - N:P-quota out of valid range (min/max): ",3f7.1)') &
                                         myID, 1./QPNmax, 1./QPNmin, 1./qPN
      call write_log_message(long_msg); long_msg = ''
   endif
#endif

   ! initialize output parameters
   mu_C = zero
   mu_N = zero
   mu_P = zero
   mu_chl = zero
   f_IR  = zero
   f_RNC = zero
   f_RPC = zero
   f_RPN = zero
   f_RNP = zero

!# ifdef SKIP
   ! - photosynthesis and carbon assimilation
   f_RCN = max( zero, (qNC-QNCmin)/(QNCmax-QNCmin) )               ! [-]
   Pmax = mu_max *f_T *f_RCN                                       ! [s^-1]
   ! - for comparism: Pmax as formulated in i.e. Baumert, Pahlow etc.
   !Pmax = mu_max *f_T *(one- QNCmin/qNC)                          ! [s^-1]
   mu_C = Pmax*(one -exp(-alpha *theta *IR /Pmax))                 ! [s^-1]
   ! light limitation @ gotm-CNPrecom
   !f_IR = mu_C / f_RCN
   f_IR = one -exp(-alpha *theta *IR /(mu_max *f_t *min(one,f_RCN)))

#ifdef debug_CNP_function
   print*,'########### photosynthesis and carbon assimilation  ###########'
   write(*,'(''mu_C, Pmax, f_RCN, f_IR           :'',5e20.10)') mu_C, Pmax, f_RCN, f_IR, mu_C/f_RCN
#endif

   ! - nutrient uptake with variable N:P-ratio
   ! down-regulation of P-uptake if cell is P-replete
   f_RPC = one
   ! down-regulation of P-uptake due to higher P:N-quota caused by N-limitation
   f_RPN = one/(one+exp(-sigma_PN*(QPNmax-qPN)))                   ! [-]
   ! P-assimilation is not directly related to amount of proteins (as "expressed" by qNC)
   mu_P = QPNuptake *mu_max *f_T *f_RPC *f_RPN *limP               ! [mmolP mmolN^-1 s^-1]
#ifdef debug_CNP_function
   print*,'########### P-assimilation  ###########'
   write(*,'(''mu_P, fRPC, f_RPN                 :'',5e20.10)') mu_P, f_RPC, f_RPN
#endif

   ! regulation of N-uptake according to cellular quota
   f_RNP = max(zero,one - (QPNmin/qPN))                            ! [-] (Droop)
   f_RNC = one/(one+exp(-sigma_NC*(QNCmax-qNC)))                   ! [-]
   ! carbon specific nitrogen uptake rate
   mu_N = QNCuptake *mu_max *f_T *f_RNP *f_RNC *limN               ! [mmolN mmolC^-1 s^-1] (vgl. Geider)
   if (mu_N .lt. TINY .or. qNC .gt. QNCmax) mu_N = zero            ! [mmolN mmolC^-1 s^-1]
#ifdef debug_CNP_function
   print*,'########### N-assimilation  ###########'
   write(*,'(''mu_N, fRNP, f_RNC                 :'',5e20.10)') mu_N, f_RNP, f_RNC
#endif

   ! - chlorophyll synthesis
   if (mu_C .le. TINY) then
      mu_chl = mu_N *theta_max                                     ! [mgChla mmolN^-1]
   else
      mu_chl = mu_N *theta_max *mu_C/(alpha*theta*IR)              ! [mgChla mmolN^-1]
   endif
#ifdef debug_CNP_function
   print*,'########### chlorophyll synthesis  ###########'
   write(*,'(''mu_Chl                            :'',5e20.10)') mu_Chl
#endif

!#endif
   if (ierr/=0) then
      write( error_message, '("[",i2.2,"] ERROR - uptake_recomCNP: ",i3)') myID, ierr
      call stop_ecoham(ierr, msg=error_message)
   end if
   return

   end subroutine uptake_recomCNP

!=======================================================================
!
! !INTERFACE:
   subroutine check_quota(phyC, phyN, phyP, phySi, name, pos, ierr)
!
! !DESCRIPTION:
! test whether cell quota are in a reasonable range
!
! !USES:
   implicit none
!
! !INPUT VARIABLES & PARAMETERS:
   real,             intent(in)    :: phyC, phyN, phyP, phySi
   character(len=*), intent(in)    :: name, pos
   integer,          intent(inout) :: ierr
!
! !LOCAL PARAMETERS:
   character(len=99) :: fmtstr
   real, parameter :: QCNmin =  5.0
   real, parameter :: QCNmax = 25.0
   real, parameter :: QNPmin =  3.0
   real, parameter :: QNPmax = 50.0
   real, parameter :: QSNmin =  0.5
   real, parameter :: QSNmax =  5.0
!
! !LOCAL VARIABLES:
   real  :: qCN, qNP, qSN
!
!-----------------------------------------------------------------------

   write(fmtstr,'(a)') '("[",i2.2,"] ERROR - ",a3,"-quota at ",a," for ",a," out of valid range (min/max): ",3f7.1)'

   qCN = phyC/phyN
   qNP = phyN/phyP
   qSN = phySi/phyN

   ! check C:N-quota
   if (qCN<QCNmin .or. qCN>QCNmax) then
       ierr = ierr+1
       !write( long_msg, '("[",i2.2,"] ERROR - C:N-quota at ",a," for ",a," out of valid range (min/max): ",3f7.1)') &
       write( long_msg, trim(fmtstr)) myID, 'C:N', pos, name, QCNmin, QCNmax, qCN
       call write_log_message(long_msg); long_msg = ''
   endif

  ! check N:P-quota
   if (qNP<QNPmin .or. qNP>QNPmax) then
       ierr = ierr+1
       !write( long_msg, '("[",i2.2,"] ERROR - N:P-quota for ",a," out of valid range (min/max): ",3f7.1)') &
       write( long_msg, trim(fmtstr)) myID, 'N:P', pos, name, QNPmin, QNPmax, qNP
       call write_log_message(long_msg); long_msg = ''
   endif

  ! check Si:N-quota
   if (qSN<QSNmin .or. qSN>QSNmax) then
       ierr = ierr+1
       !write( long_msg, '("[",i2.2,"] ERROR - S:N-quota for ",a," out of valid range (min/max): ",3f7.1)') &
       write( long_msg, trim(fmtstr)) myID, 'S:N', pos, name, QSNmin, QSNmax, qSN
       call write_log_message(long_msg); long_msg = ''
   endif

   if (ierr/=0) then
      write( error_message, '("[",i2.2,"] ERROR - check_quota: ",i3)') myID, ierr
      call stop_ecoham(ierr, msg=error_message)
   end if
   return

   end subroutine check_quota

!=======================================================================
#endif
   end module mod_biogeo_recom

!=======================================================================
#endif