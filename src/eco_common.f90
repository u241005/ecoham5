!
! !MODULE: eco_common.f90 --- common definitions for ecoham
!
! !INTERFACE:
   MODULE mod_common
!
! !DESCRIPTION:
! This module is the container for variables extensively used
!
! !USES:
   use mod_par
   use mod_grid_parameters
   use mod_mpi_parallel
   implicit none
!
!  default: all is public.
   public

   logical          :: offline=.true.

   ! OVERHEAD
   character(len=99):: runID
   character(len=99):: error_message
   character(len=50):: line_separator='--------------------------------------------------'

   ! TIME VARIABLES
   integer          :: iy1,im1,id1,ih1   !starttime
   integer          :: iy2,im2,id2,ih2   !endtime
   real             :: dt                !timestep in days (!!!) applying advection/diffusion (:= max dt for biogeo)
   integer          :: dt_step           !timestep in seconds for non-recursive biogeo-calls and for applying advection/diffusion
   integer          :: dt_main           !timestep for iteration of "do_main"
   integer          :: dt_out            !timestep in seconds for writing output data and specifying integration intervall
   real             :: day_of_year       !cumulative run time
   integer          :: year              !current year
   integer          :: EndOfIteration    !number of iterations to complete simulation period
   integer          :: EndOfMainstep     !number of iterations to complete dt_main
   integer          :: iday_start        !startday
   integer          :: iday_end          !endday
   real             :: rday_start        !startday as real
   real             :: rday_end          !endday as real
   real             :: cpu_t1,cpu_t2     !cpu_time at start and end of simulation
   real             :: cpu_t3,cpu_t4     !cpu_time at begin and end of time loop
   character(len=19):: timestamp         !date & time as string (yyyy-mm-dd HH:MM:SS)

   ! GRID VARIABLES
   integer    :: iStartCalc    = 3       !first i-index for calculations on local domain (latitudninal direction)
   integer    :: iEndCalc      = imax-1  !last  i-index for calculations on local domain (latitudninal direction)
   integer    :: jStartCalc    = 3       !first j-index for calculations on local domain (longitudininal direction)
   integer    :: jEndCalc      = jmax-1  !last  j-index for calculations on local domain (longitudininal direction)
   integer    :: iStartD       = 1       !first index of local domain in latitudninal direction
   integer    :: iEndD         = imax    !last  index of local domain in latitudninal direction
   integer    :: iStartB       = 1       !first index of local domain boundary in latitudninal direction
   integer    :: iEndB         = imax    !last  index of local domain boundary in latitudninal direction

!    ! MPI VARIABLES
!    integer, parameter :: master  = 0
!    integer            :: myID    = 0
!    integer            :: ierr_mpi = 0

   ! ASCII-units (100-109)
   integer    :: asc_unit        = 100
   integer    :: log_unit        = 101
   integer    :: end_unit        = 102
   integer    :: nml_unit        = 103
   integer    :: river_index_unit= 104
   integer    :: debugUnit       = 105
   integer    :: debugRiverUnit  = 106
   ! INPUT-units
   ! -- overhead --
   integer    :: warm_unit       = 110
   integer    :: grid_unit       = 111
   ! -- hydro --
   integer    :: temp_unit       = 120
   integer    :: salt_unit       = 121
   integer    :: tu_unit         = 122
   integer    :: tv_unit         = 123
   integer    :: ax_unit         = 124
   integer    :: ay_unit         = 125
#ifdef makehyd_Input
   integer    :: tw_unit         = 126
   integer    :: vdc_unit        = 127
   integer    :: vol_unit        = 128
#else
   integer    :: z_unit          = 126
   integer    :: av_unit         = 127
   integer    :: sm_unit         = 128
#endif
   ! -- meteo --
   integer    :: wins_unit       = 130
   integer    :: radday_unit     = 131
   integer    :: rad_unit        = 132
   integer    :: extw_unit       = 133
   integer    :: prec_evap_unit  = 134
   integer    :: N_depos_unit    = 135
   ! -- river --
   integer    :: river_unit      = 140
   integer    :: freshwater_unit = 141
   ! -- silt --
   integer    :: silt_unit       = 145
   ! -- restoring --
   integer    :: rest_unit       = 150 ! ,151, ... ,150+n_st_var
   ! OUTPUT-units
   ! -- 3D-output --
   integer    :: sec_unit        = 200 ! ... 249
   integer    :: output_3D_unit  = 250 ! ... 299
   ! -- 1D-output --
   integer    :: output_1D_unit  = 300 ! ... 399
   integer, dimension(:), allocatable  :: icol_out, jcol_out
   integer  :: n_pos_out

   ! COMMON (temporary) ARRAYS FOR OUTPUT
   real,    dimension(:,:,:),   allocatable :: full2D
   real,    dimension(:,:,:,:), allocatable :: full3D

   ! COMMON ARRAYS BRIDGING BIOLOGY, CHEMISTRY AND SEDIMENT
   real,    dimension(:,:,:), allocatable :: pco2w, delta_o3c, aksp, ca
   integer, dimension(:,:,:), allocatable :: oswtch, nswtch
   !
#ifdef adjust_stoichiometry
   real, public :: red, rcn, rcp, rcs, rcn2, rcp2, rcn3, rcp3
   real, public :: chl_p1c, chl_p2c, chl_p3c
#endif

   ! SETUP PARAMETERS
!&set_nml
   integer    :: isw_dim      =   3      ! isw_dim=1 1D mode,isw_dim=3 3D mode
   integer    :: isw_sed      =   1      ! emr_sediment=2, plate_sediment=1, without sediment=0
   real       :: relrate      =   0.40   ! max rel.rate of change per time step
   integer    :: talk_treat   =   1      ! 0 - diagnostic treatment of alkalinity
!                                        ! 1 - prognostic treatment of alkalinity
   integer    :: CaCo3_diss   =   1      ! 0 - no CaCo3 dissolution above lysocline
!                                        ! 1 - with CaCO3 dissolution also above lysocline
   integer    :: iwarm        =   0      ! warmstart=1
!                             =          ! cold<=0; cold=-n: n days spinup of hydro
   integer    :: iwarm_out    =   0      ! 0 - write warmstart only at end
!                             =          ! 1 - (re-)write warmstart every day
   logical    :: lfreshwater  = .false.  ! hydro forcing requires freshwater input
!
   ! ERROR-HANDLING
   !integer            :: ierr = 0         ! error status (ierr=0 :='OK', ierr/=0 :='abort')
   integer            :: logfile_handle, buf_len
   integer, parameter :: log_msg_len=120
   integer, parameter :: long_msg_len=log_msg_len*8        ! assumed maximum length for composed messages
   character(len=log_msg_len), dimension(8) :: log_msg     ! temp array to assemble composed messages
   character(len=long_msg_len)              :: long_msg
   character(len=long_msg_len+1)            :: log_buffer

   ! environment
   character(len=30) :: fmt_jki, str_jki
   character(len=99) :: filename, fmtstr
   character(len=99) :: settings_file    = 'eco_set.nml'
   character(len=99) :: settings_bio     = 'eco_bio.nml'
   character(len=99) :: warmstart_file   = 'warmstart.asc'
   character(len=10) :: warmout_file_ext = '.asc'          ! extension for warmstart file (.bin or .asc)
!
!=======================================================================

   contains

!=======================================================================
!
! !INTERFACE:
   subroutine allocate_commonvars(ierr)
#define SUBROUTINE_NAME 'allocate_commonvars'
!
! !DESCRIPTION:
!
! !USES:
   IMPLICIT NONE
!
! !OUTPUT PARAMETERS:
   integer, intent(inout)  :: ierr
!
! !LOCAL VARIABLES:
!-----------------------------------------------------------------------
#include "call-trace.inc"

   ! allocate arrays defined for domain size
   allocate(    pco2w(jmax,kmax,iStartD:iEndD) ); pco2w     = zero
   allocate(     aksp(jmax,kmax,iStartD:iEndD) ); aksp      = zero
   allocate(       ca(jmax,kmax,iStartD:iEndD) ); ca        = zero
   allocate(delta_o3c(jmax,kmax,iStartD:iEndD) ); delta_o3c = zero
   allocate(   oswtch(jmax,kmax,iStartD:iEndD) ); oswtch    = 0
   allocate(   nswtch(jmax,kmax,iStartD:iEndD) ); nswtch    = 0

   return
   end subroutine allocate_commonvars

!=======================================================================
!
! !INTERFACE:
   subroutine write_log_message(buffer, write2stdout)
!#define SUBROUTINE_NAME 'write_log_message'
!
! !DESCRIPTION:
! all processes should be able to call this subroutine
! multi line messages are possible if the new_line ist used in the composed message
! mpi_file_write_shared needs an additional newline at the end for FORTRAN like output
!
! !USES:
#ifdef MPI
   use mpi
#endif
   IMPLICIT NONE
!
! !INPUT PARAMETERS:
   character(len=*),  intent(in) :: buffer
   logical, optional, intent(in) :: write2stdout
#ifdef MPI
   integer                       :: status_mpi(MPI_STATUS_SIZE)
#endif
!
! !LOCAL VARIABLES:
!-----------------------------------------------------------------------

#ifdef MPI
   log_buffer = trim(buffer) // new_line('a')
   buf_len = len_trim( log_buffer)
   call MPI_File_write_shared(logfile_handle, log_buffer, buf_len, MPI_CHARACTER, status_mpi, ierr_mpi)
#else
   write(log_unit,'(a)') trim(buffer)
#endif
   ! write log message both to log file and stdout (default)
   if (.not. present(write2stdout) .or. write2stdout) then
      write(*, '(a)') trim(buffer)
   end if

   end subroutine write_log_message

!=======================================================================
! !INTERFACE:
   subroutine stop_ecoham(ierr, msg)
!
! !DESCRIPTION:
!
! !USES:
   implicit none
!
! !INPUT PARAMETERS:
!
! !OUTPUT PARAMETERS:
   integer,        intent(in) :: ierr
   character(len=*), optional :: msg
!
! !LOCAL VARIABLES:
!
!-----------------------------------------------------------------------

   if (ierr/=0) then
      ! in error case print a meaningful error message (subroutine, why, error code, processor rank etc.)
      write(log_msg(1),'("*******************************************************")')
      write(log_msg(2),'("***  STOPPING SIMULATION by subroutine stop_ecoham  ***")')
      write(log_msg(3),'("*******************************************************")')
      write(log_msg(4),'("stop_ecoham has been called by processor ",i5)') myID
      if (present(msg)) then
         write(log_msg(5),'("status: ",a)') trim(msg)
      else
         write(log_msg(5),'("status: UNKNOWN")')
      endif
      long_msg = trim(log_msg(1))//new_line('a')//trim(log_msg(2))//new_line('a')// &
                 trim(log_msg(3))//new_line('a')//trim(log_msg(4))//new_line('a')// &
                 trim(log_msg(5))
      call write_log_message(long_msg); long_msg = ''
      flush(0)

#ifdef MPI
      ! abort all tasks in the group 'MPI_COMM_WORLD'
      call MPI_ABORT (MPI_COMM_ECOHAM, 1, ierr_mpi)
      ! check if MPI_ABORT successfully completed
      if (ierr_mpi /= MPI_SUCCESS) then
         write (*,*) ' MPI_ABORT failed. Error =  ', ierr_mpi
         stop
      endif
#else
      stop
#endif
   endif

   return
   end subroutine stop_ecoham

!=======================================================================

   end module mod_common

!=======================================================================
