program ecoham5
#define PROGRAM_NAME 'ecoham5'
!=======================================================================
! coupled HAMSOM - passive tracer model
!=======================================================================
!
! !DESCRIPTION:
!
! !USES:
   use mod_common,     only: EndOfIteration, day_of_year
   use mod_common,     only: line_separator, error_message
   use mod_common,     only: write_log_message, long_msg
   use mod_common,     only: cpu_t3, cpu_t4, myId, master
   use mod_utils,      only: DOYstring
   use mod_init,       only: init_setup
   use mod_hydro,      only: init_hydro, get_hydro, close_hydro
   use mod_boundaries, only: init_boundaries, get_boundaries, close_boundaries
   use mod_main,       only: init_main, do_main, end_main
#ifdef convert_warmstart
   use mod_biogeo,     only: init_biogeo
   use mod_output,     only: warm_out
#endif
#ifdef MPI
   use mod_mpi_parallel
#endif
   implicit none
!
! !LOCAL VARIABLES:
   integer :: StepOfIteration, ierr
!
!=======================================================================

!------------------------------------------------------
!                   INITIALIZE
!------------------------------------------------------
#include "call-trace.inc"

   ! initialize error status
   ! IMPORTANT: this is the ONLY place, where it may be set equal to zero !!!
   ierr = 0

   call init_setup(ierr)

   call init_hydro(ierr)

   call init_boundaries(ierr)

   call init_main(ierr)

   if (myID == master) then
      call write_log_message(line_separator)
      call write_log_message(" ENTER TIME LOOP ")
   end if ! master

!------------------------------------------------------
!                   TIME LOOP
!------------------------------------------------------
   call CPU_Time(cpu_t3)
   do StepOfIteration = 1, EndOfIteration
      if (myID == master) then
         call write_log_message(line_separator)
         write(long_msg,'(1x,"entering iteration: ",i6," of ",i6," starting ",a20)') &
                     StepOfIteration, EndOfIteration, DOYstring(day_of_year)
         call write_log_message(long_msg); long_msg=''
      end if ! master

      call get_hydro(ierr)

      call get_boundaries(ierr)

      call do_main(ierr)

! print*,myID,'do_main - DONE'
! call stop_ecoham(1)

   enddo
   call CPU_Time(cpu_t4)

!------------------------------------------------------
!                   FINISH
!------------------------------------------------------

   if (myID == master) call write_log_message(line_separator)

   if (ierr/=0) then
      if (trim(error_message) == '') error_message = 'error_message missing'
      write(long_msg,'("[",i2.2,"] - ",a)') myID, error_message
      call write_log_message(long_msg); long_msg=''
   endif

   if (myID == master) then
      if (ierr==0) call write_log_message(" TIME LOOP FINISHED")
      call write_log_message(line_separator)
   endif

   call close_hydro(ierr)
   call close_boundaries(ierr)
   call end_main(ierr)

!=======================================================================

   end program ecoham5

!=======================================================================
