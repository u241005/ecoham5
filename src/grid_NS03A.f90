!
! !MODULE: grid_NS03A.f90 --- parameters grid: North Sea, 3km, Version "A" (30 layer)
!
! !INTERFACE:
   MODULE mod_grid_parameters
!
! !DESCRIPTION:
!
! !USES:
   implicit none
!
   ! GRID PARAMETERS
!    real,    parameter :: delta_lat     =  0.025            ! delta lat in degrees
!    real,    parameter :: delta_lon     =  0.0416666666667  ! delta lon in degrees
!    real,    parameter :: xlat0         =  50.8708          ! most southerly zeta point
!    real,    parameter :: xlon0         = -4.0625333333     ! most westerly zeta point
   real,    parameter :: delta_lat     =  1.5/60.          ! delta lat in degrees
   real,    parameter :: delta_lon     =  2.5/60.          ! delta lon in degrees
   real,    parameter :: xlat0         = 50.0 + 52.25/60.  ! most southerly zeta point
   real,    parameter :: xlon0         = -4.0 - 3.75/60.   ! most westerly zeta point
   integer, parameter :: imax          = 380               ! number of latidude indexes
   integer, parameter :: jmax          = 414               ! number of longitude indexes
   integer, parameter :: kmax          = 30                ! number of layer indexes
   integer, parameter :: ijmax         = imax*jmax         ! total number of 2D-surface-cells
   integer, parameter :: ijkmax        = imax*jmax*kmax    ! total number of 3D-cells
   integer, parameter :: iiwet2        = 84324             ! number of wet surface cells
   integer, parameter :: iiwet         = 939017            ! number of wet cells

   ! set external partitioning in latitudinal direction, see eco_set_nml
   logical    :: external_partitioning = .false.           ! default is .false.
!=======================================================================

   end module mod_grid_parameters

!=======================================================================