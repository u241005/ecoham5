!
! !MODULE: eco_ncdfout.f90 --- supporting NetCDF-output for ecoham
!
! !INTERFACE:
   module mod_ncdfout
#ifdef NETCDF
!
! !DESCRIPTION:
!  This module provides routines for saving results using NetCDF format.
!
! !USES:
   use mod_common
   use mod_var
   use netcdf
   implicit none
!   include 'netcdf.inc'
!
   private
! !PUBLIC MEMBER FUNCTIONS:
   public init_ncdf, do_ncdf_out, close_ncdf
!
! !PRIVATE DATA MEMBERS:
#ifdef NETCDF_DOUBLE
   integer,           parameter :: nc_kind = 8 ! double precision
#else
   integer,           parameter :: nc_kind = 4 ! single precision
#endif
!  netCDF file id
   integer, parameter           :: ncid_default = -1
   integer                      :: ncid  = ncid_default
   character (len=99)           :: ncfile
! We are writing 4D data, lon-lvl-lat grid, with unlimited timesteps of data.
!  dimensions
   integer,           parameter :: NLONS = jmax
   integer,           parameter :: NLATS = imax
   integer,           parameter :: NLVLS = kmax
   integer                      :: NRECS = NF90_UNLIMITED
!  dimension names
   character (len=*), parameter :: LON_NAME = "longitude"
   character (len=*), parameter :: LAT_NAME = "latitude"
   character (len=*), parameter :: LVL_NAME = "depth"
   character (len=*), parameter :: REC_NAME = "time"
!  dimension ids
   integer                      :: lon_dimid, lat_dimid
   integer                      :: lvl_dimid, rec_dimid
   integer,           parameter :: dim1=1,dim2=2,dim3=3,dim4=4
   integer                      :: dimid2D(dim3), dimid3D(dim4)
   ! These program variables hold grid info for latitudes, longitudes and depth
   real                         :: lons(NLONS), lats(NLATS), depth(NLVLS)
   integer                      :: lon_varid, lat_varid, lvl_varid
   ! These program variables holds the time axis.
   integer                      :: rec_varid
   ! We recommend that each variable carry a "units" attribute.
   character (len=*), parameter :: UNITS     = "units"
   character (len=*), parameter :: LON_UNITS = "degrees_east"
   character (len=*), parameter :: LAT_UNITS = "degrees_north"
   character (len=*), parameter :: LVL_UNITS = "meters"
   character (len=30)           :: REC_UNITS
   ! The start and count arrays will tell the netCDF library where to
   ! write our data.
   integer                      :: nc_start(dim4), nc_count(dim4)
   ! Output variables to hold the data we will write out. We will only
   ! need enough space to hold one timestep of data; one record.
   real                         :: data_out2D(NLONS, NLATS)
   real                         :: data_out3D(NLONS, NLATS, NLVLS)

   ! OVERHEAD
   logical, private             :: first=.true.
   integer, public              :: set_no
   character(len=7), public     :: ncdf_time_unit = "seconds"  ! default setting
   real                         :: ncdf_time

   ! These program variables hold info for non-(time)varying grid specifications
   integer                      :: depth_varid, area_varid, vol0_varid
   ! Output variable ids
   integer,           allocatable :: nc_varid2D(:)
   integer,           allocatable :: nc_varid3D(:)

   !  Output (meta-)data (filled from outside)
   integer,                        public :: nc_out2D, nc_out3D
   character(len=20), allocatable, public :: nc_name2D(:), nc_name3D(:)
   character(len=99), allocatable, public :: nc_long2D(:), nc_long3D(:)
   character(len=99), allocatable, public :: nc_unit2D(:), nc_unit3D(:)
   !real,              allocatable, public :: nc_data2D(:,:,:), nc_data3D(:,:,:,:)
!
!=======================================================================

   contains

!=======================================================================
!
! !IROUTINE: Create the NetCDF file
!
! !INTERFACE:
   subroutine init_ncdf(fn,title,start_time,ierr)
#define SUBROUTINE_NAME 'init_ncdf'
!
! !DESCRIPTION:
!  Opens and creates the NetCDF file, and initialises all dimensions and
!  variables
!
! !USES:
   use mod_grid, only: dzz, ixistK_master, ixistZ_master
   use mod_grid, only: area_master, dz_master, vol_master
   IMPLICIT NONE
!
! !INPUT PARAMETERS:
   character(len=*), intent(in) :: fn,start_time,title
!
! !OUTPUT PARAMETERS:
   integer,       intent(inout) :: ierr
!
! !LOCAL VARIABLES:
   real                         :: buffer2D(jmax,     imax)
   real                         :: buffer3D(jmax,kmax,imax)
   integer                      :: i,j,k,n,iret
   character(len=128)           :: history

!-------------------------------------------------------------------------
#include "call-trace.inc"

   set_no  = 0
   if (dt_out >= secs_per_day) then
      ncdf_time_unit = "days"
   elseif (dt_out >= 3600) then
      ncdf_time_unit = "hours"
   elseif (dt_out >= 60) then
      ncdf_time_unit = "minutes"
   endif

   write(history,'(a128)') trim(title) ! to get rid of "unused" warning

   REC_UNITS = trim(ncdf_time_unit)//' since '//trim(start_time)

   ! specify lat/lon-grid
   do j = 1, NLONS
      lons(j) = xlon0 + (j-1)*delta_lon
   enddo
   do i = 1, NLATS
      lats(i) = xlat0 + (i-1)*delta_lat
   enddo
   depth(1) = 0.5*dzz(1)
   do k = 2, NLVLS
      depth(k) = depth(k-1) + 0.5*dzz(k-1) +0.5*dzz(k)
   enddo

   ! Create the netCDF file. The nf90_clobber parameter tells netCDF to
   ! overwrite this file, if it already exists.
   ncfile = fn
   call check( nf90_create(ncfile, NF90_CLOBBER, ncid), iret ); ierr=ierr+iret
   if (iret.ne.0) then
      write(error_message,'("ERROR creating ",a)') trim(ncfile)
      call write_log_message(error_message)
      return
   endif

   ! Define the dimensions. NetCDF will hand back an ID for each.
   ! The record dimension is defined to have unlimited length
   ! - it can grow as needed. In this example it is the time dimension.
   call check( nf90_def_dim(ncid, LAT_NAME, NLATS, lat_dimid), iret )
   ierr = ierr + iret
   if (iret.ne.0) then
      call write_log_message('ERROR defining dimension for lat_dimid')
      return
   endif
   call check( nf90_def_dim(ncid, LON_NAME, NLONS, lon_dimid), iret )
   ierr = ierr + iret
   if (iret.ne.0) then
      call write_log_message('ERROR defining dimension for lon_dimid')
      return
   endif
   call check( nf90_def_dim(ncid, LVL_NAME, NLVLS, lvl_dimid), iret )
   ierr = ierr + iret
   if (iret.ne.0) then
      call write_log_message('ERROR defining dimension for lvl_dimid')
      return
   endif
   call check( nf90_def_dim(ncid, REC_NAME, NF90_UNLIMITED, rec_dimid), iret )
   ierr = ierr + iret
   if (iret.ne.0) then
      call write_log_message('ERROR defining dimension for rec_dimid')
      return
   endif

   ! Define the coordinate variables. We will only define coordinate
   ! variables for lat and lon.  Ordinarily we would need to provide
   ! an array of dimension IDs for each variable's dimensions, but
   ! since coordinate variables only have one dimension, we can
   ! simply provide the address of that dimension ID (lat_dimid) and
   ! similarly for (lon_dimid).
   call check( nf90_def_var(ncid, LAT_NAME, NF90_REAL, lat_dimid, lat_varid), iret )
   ierr = ierr + iret
   if (iret.ne.0) then
      call write_log_message('ERROR defining var lat_varid')
      return
   endif
   call check( nf90_def_var(ncid, LON_NAME, NF90_REAL, lon_dimid, lon_varid), iret )
   ierr = ierr + iret
   if (iret.ne.0) then
      call write_log_message('ERROR defining var lon_varid')
      return
   endif
   call check( nf90_def_var(ncid, LVL_NAME, NF90_REAL, lvl_dimid, lvl_varid), iret )
   ierr = ierr + iret
   if (iret.ne.0) then
      call write_log_message('ERROR defining var lvl_varid')
      return
   endif
   call check( nf90_def_var(ncid, REC_NAME, NF90_REAL, rec_dimid, rec_varid), iret )
   ierr = ierr + iret
   if (iret.ne.0) then
       call write_log_message('ERROR defining var rec_varid');
      return
   endif

   ! Assign units attributes to coordinate variables.
   call check( nf90_put_att(ncid, lat_varid, "units", LAT_UNITS), iret )
   ierr = ierr + iret
   if (iret.ne.0) then
      call write_log_message('ERROR assigning attributes to lat_varid')
      return
   endif
   call check( nf90_put_att(ncid, lon_varid, "units", LON_UNITS), iret )
   ierr = ierr + iret
   if (iret.ne.0) then
   call write_log_message('ERROR assigning attributes to lon_varid')
      return
   endif
   call check( nf90_put_att(ncid, lvl_varid, "units", LVL_UNITS), iret )
   ierr = ierr + iret
   if (iret.ne.0) then
      call write_log_message('ERROR assigning attributes to lvl_varid')
      return
   endif
   call check( nf90_put_att(ncid, rec_varid, "units", REC_UNITS), iret )
   ierr = ierr + iret
   if (iret.ne.0) then
      call write_log_message('ERROR assigning attributes to rec_varid')
      return
   endif

   ! The dimid array is used to pass the dimids of the dimensions of
   ! the netCDF variables. Both of the netCDF variables we are creating
   ! share the same four dimensions. In Fortran, the unlimited
   ! dimension must come last on the list of dimids.
   dimid2D = (/ lon_dimid, lat_dimid, rec_dimid /)
   dimid3D = (/ lon_dimid, lat_dimid, lvl_dimid, rec_dimid /)


   ! Define non-(time)varying grid specifications
#ifndef NETCDF_DOUBLE
   ! single precision
   call check( nf90_def_var(ncid, 'bathymetry', NF90_REAL,          &
                 (/ lon_dimid, lat_dimid /), depth_varid), iret )
#else
   ! double precision
   call check( nf90_def_var(ncid, 'bathymetry', NF90_DOUBLE,        &
                 (/ lon_dimid, lat_dimid /), depth_varid), iret )
#endif
   ierr = ierr + iret
   if (iret.ne.0) then
      write(error_message,'("ERROR defining bathymetry")')
      call write_log_message(error_message)
      return
   endif
   call check(set_attributes(ncid, depth_varid, long_name='DEPTH_BELOW_GEOID',  &
                         units='m', FillValue=fail), iret)
   ierr = ierr + iret
   if (iret.ne.0) then
      call write_log_message('ERROR assigning attributes to bathymetry')
      return
   endif
   
#ifndef NETCDF_DOUBLE
   ! single precision
   call check( nf90_def_var(ncid, 'area', NF90_REAL,                &
               (/ lon_dimid, lat_dimid /), area_varid), iret )
#else
   ! double precision
   call check( nf90_def_var(ncid, 'area', NF90_DOUBLE,              &
               (/ lon_dimid, lat_dimid /), area_varid), iret )
#endif

   ierr = ierr + iret
   if (iret.ne.0) then
      write(error_message,'("ERROR defining area")')
      call write_log_message(error_message)
      return
   endif
   call check(set_attributes(ncid, area_varid, long_name='CELL_AREA',   &
                         units='m2', FillValue=fail), iret)
   ierr = ierr + iret
   if (iret.ne.0) then
      call write_log_message('ERROR assigning attributes to area')
      return
   endif

#ifndef NETCDF_DOUBLE
   ! single precision
   call check( nf90_def_var(ncid, 'vol0', NF90_REAL,                   &
               (/ lon_dimid, lat_dimid, lvl_dimid /), vol0_varid), iret )
#else
   ! double precision
   call check( nf90_def_var(ncid, 'vol0', NF90_DOUBLE,                 &
               (/ lon_dimid, lat_dimid, lvl_dimid /), vol0_varid), iret )
#endif

   ierr = ierr + iret
   if (iret.ne.0) then
      write(error_message,'("ERROR defining vol0")')
      call write_log_message(error_message)
      return
   endif
   call check(set_attributes(ncid, vol0_varid, long_name='REFERENCE CELL VOLUME', &
                         units='m3', FillValue=fail), iret)
   ierr = ierr + iret
   if (iret.ne.0) then
      call write_log_message('ERROR assigning attributes to vol0')
      return
   endif

   ! Define the netCDF variables and assign attributes

   ! 2D variables
   allocate( nc_varid2D(nc_out2D) )
   !if (nc_out2D > 0) then
   do n = 1, nc_out2D
#ifndef NETCDF_DOUBLE
      ! single precision
      call check( nf90_def_var(ncid, trim(nc_name2D(n)), NF90_REAL, dimid2D, nc_varid2D(n)), iret)
#else
      ! double precision
      call check( nf90_def_var(ncid, trim(nc_name2D(n)), NF90_DOUBLE, dimid2D, nc_varid2D(n)), iret)
#endif
      ierr = ierr + iret
      if (iret.ne.0) then
         write(error_message,'("ERROR defining netcdf-var ",a)') nc_name2D(n)
         call write_log_message(error_message)
         return
      endif

      call check(set_attributes(ncid, nc_varid2D(n), long_name=nc_long2D(n),  &
                            units=nc_unit2D(n), FillValue=fail), iret)
      ierr = ierr + iret
      if (iret.ne.0) then
         write(error_message,'("ERROR assigning netcdf-attributes to ",a)') nc_name2D(n)
         call write_log_message(error_message)
         return
      endif
   enddo
   !endif

   ! 3D variables
   allocate( nc_varid3D(nc_out3D) )
   !if (nc_out3D > 0) then
   do n = 1, nc_out3D
#ifndef NETCDF_DOUBLE
      ! single precision
      call check( nf90_def_var(ncid, trim(nc_name3D(n)), NF90_REAL, dimid3D, nc_varid3D(n)), iret)
#else
      ! double precision
      call check( nf90_def_var(ncid, trim(nc_name3D(n)), NF90_DOUBLE, dimid3D, nc_varid3D(n)), iret)
#endif
      ierr = ierr + iret
      if (iret.ne.0) then
         write(error_message,'("ERROR defining netcdf-var ",a)') nc_name3D(n)
         call write_log_message(error_message)
         return
      endif

      call check(set_attributes(ncid, nc_varid3D(n), long_name=nc_long3D(n),  &
                            units=nc_unit3D(n), FillValue=fail), iret)
      ierr = ierr + iret
      if (iret.ne.0) then
         write(error_message,'("ERROR assigning netcdf-attributes to ",a)') nc_name3D(n)
         call write_log_message(error_message)
         return
      endif
   enddo
   !endif

!  !global attributes
!  call check(nf_put_att_text(ncid,NF_GLOBAL,'Title',trim(title),title), iret); ierr=ierr+iret
!  history = 'Created by ECO49_x1x'
!  call check(nf_put_att_text(ncid,NF_GLOBAL,'history',trim(history),history), iret); ierr=ierr+iret
!  call check(nf_put_att_text(ncid,NF_GLOBAL,'Conventions',6,'COARDS'), iret); ierr=ierr+iret
!  if (ierr.ne.0) call write_log_message("ERROR setting global attributes"); return

   ! End define mode.
   call check( nf90_enddef(ncid), iret )
   ierr = ierr + iret
   if (iret.ne.0) then
      call write_log_message("ERROR ending define mode")
      return
   endif

   ! Write the coordinate variable data. This will put the latitudes
   ! and longitudes of our data grid into the netCDF file.
   call check( nf90_put_var(ncid, lat_varid, lats), iret )
   ierr = ierr + iret
   if (iret.ne.0) then
      call write_log_message("ERROR writing to lat_varid")
      return
   endif
   call check( nf90_put_var(ncid, lon_varid, lons), iret )
   ierr = ierr + iret
   if (iret.ne.0) then
      call write_log_message("ERROR writing to lon_varid")
      return
   endif
   call check( nf90_put_var(ncid, lvl_varid, depth), iret )
   ierr = ierr + iret
   if (iret.ne.0) then
      call write_log_message("ERROR writing to lvl_varid")
      return
   endif

   ! Write non-(time)varying grid specifications
   buffer2D = sum(dz_master(:,:,:), 2, ixistZ_master==1)
   where(ixistK_master==0) buffer2D = fail
   do j = 1, NLATS
      data_out2D(:,j) = buffer2D(:,NLATS-j+1)
   enddo
   call check( nf90_put_var(ncid, depth_varid, data_out2D), iret)
   ierr = ierr + iret
   if (iret.ne.0) then
      call write_log_message('ERROR writing NETCDF for bathymetry')
      return
   endif

   buffer2D = area_master
   where(ixistK_master==0) buffer2D = fail
   do j = 1, NLATS
      data_out2D(:,j) = buffer2D(:,NLATS-j+1)
   enddo
   call check( nf90_put_var(ncid, area_varid, data_out2D), iret)
   ierr = ierr + iret
   if (iret.ne.0) then
      call write_log_message('ERROR writing NETCDF for area')
      return
   endif

   buffer3D = vol_master
   where(ixistZ_master==0) buffer3D = fail
   do k = 1, NLVLS
      do j = 1, NLATS
         data_out3D(:,j,k) = buffer3D(:,k,NLATS-j+1)
      enddo
   enddo
   call check( nf90_put_var(ncid, vol0_varid, data_out3D), iret)
   ierr = ierr + iret
   if (iret.ne.0) then
      call write_log_message('ERROR writing NETCDF for vol0')
      return
   endif

   ! These settings tell netcdf to write one timestep of data. (The
   ! setting of start(4) inside the loop below tells netCDF which
   ! timestep to write.)
   nc_count = (/ NLONS, NLATS, NLVLS, dim1 /)
   nc_start = (/ dim1, dim1, dim1, dim1 /)

   ! Write the pretend data at initialisation (t=0). This will write
   ! our outputdata. The arrays only hold one timestep worth
   ! of data. We will just rewrite the same data for each timestep. In
   ! a real :: application, the data would change between timesteps.
   set_no = 0
   nc_start(4) = set_no

!   call do_ncdf_out(0.)

!    ! Close the file. This causes netCDF to flush all buffers and make
!    ! sure your data are really written to disk.
!    call check( nf90_close(ncid) )
!    stop "*** SUCCESS writing netcdf file !"

   if (ierr/=0) then
      write( error_message, '("[",i2.2,"] ERROR - init_ncdf",i3)') myID, ierr
      call stop_ecoham(ierr, msg=error_message)
   end if
   return

   end subroutine init_ncdf

!=======================================================================
! !INTERFACE:
   subroutine do_ncdf_out(secs,ierr)
#define SUBROUTINE_NAME 'do_ncdf_out'
!
! !DESCRIPTION:
!  Write results to the NetCDF file.
!
! !USES:
   use mod_var

   IMPLICIT NONE
!
! !INPUT PARAMETERS:
   integer,         intent(in)  :: secs
!
! !OUTPUT PARAMETERS:
   integer,       intent(inout) :: ierr
!
! !LOCAL VARIABLES:
   integer                      :: j,k,n,iret
!
!-------------------------------------------------------------------------
#include "call-trace.inc"

   set_no = set_no + 1

   select case (ncdf_time_unit)
      case("seconds")
         ncdf_time = real(secs)
      case("minutes")
         ncdf_time = real(secs/60.)
      case("hours")
         ncdf_time = real(secs/3600.)
      case("days")
         ncdf_time = real(secs/secs_per_day)
      case default
         ncdf_time = real(secs)
   end select

   ! Write outputdata each timestep. The arrays only hold one timestep worth  of data.
   nc_start(4) = set_no

   ! write time
   call check(store_data(ncid, rec_varid, scalar=ncdf_time), iret)
   ierr = ierr + iret
   if (iret.ne.0) return

   ! write 2D data
   do n = 1, nc_out2D
      !data_out2D(:,:) = nc_data2D(:,:,n)
      do j = 1, NLATS
         data_out2D(:,j) = full2D(:,NLATS-j+1,n)
      enddo
      call check(store_data(ncid, nc_varid2D(n), array2d=data_out2D), iret)
      ierr = ierr + iret
      if (iret.ne.0) then
         write(error_message,'("ERROR writing NETCDF for ",a)') nc_name2D(n)
         call write_log_message(error_message)
         return
      endif
   enddo

   ! write 3D data
   do n = 1, nc_out3D
      !data_out3D(:,:,:) = nc_data3D(:,:,:,n)
      do k = 1, NLVLS
         do j = 1, NLATS
            data_out3D(:,j,k) = full3D(:,k,NLATS-j+1,n)
         enddo
      enddo
      call check(store_data(ncid, nc_varid3D(n), array3d=data_out3D), iret)
      ierr = ierr + iret
      if (iret.ne.0) then
         write(error_message,'("ERROR writing NETCDF for ",a)') nc_name3D(n)
         call write_log_message(error_message)
         return
      endif
   enddo

   ! flush buffer to file
   call check( nf90_sync(ncid), iret)
   if (iret.ne.0) then
      write(error_message,'("ERROR flushing data to ",a)') trim(ncfile)
      call write_log_message(error_message)
      return
   endif

   if (ierr/=0) then
      write( error_message, '("[",i2.2,"] ERROR - do_ncdf_out",i3)') myID, ierr
      call stop_ecoham(ierr, msg=error_message)
   end if
   return

   end subroutine do_ncdf_out

!=======================================================================
!
! !INTERFACE:
   subroutine close_ncdf(ierr)
#define SUBROUTINE_NAME 'close_output_3D'
!
! !DESCRIPTION:
!  Closes the NetCDF file.
!
! !USES:
   implicit none
!
! !OUTPUT PARAMETERS:
   integer,       intent(inout) :: ierr
!
! !LOCAL VARIABLES:
   integer                      :: iret
!
!-------------------------------------------------------------------------
#include "call-trace.inc"

   if (ncid == ncid_default) return ! return of nc file has not been opened

   ! Close the file. This frees up any internal netCDF resources
   ! associated with the file, and flushes any buffers.
   call check( nf90_close(ncid), iret )
   ierr = ierr + iret
   if (ierr.ne.0) then
      call write_log_message(" ERROR closing netcdf file")
      call stop_ecoham(ierr)
   else
      call write_log_message(" NETCDF data written to file "//trim(ncfile))
   endif

   deallocate( nc_varid2D )
   deallocate( nc_varid3D )

   first=.true.
   set_no=0

   return

   end subroutine close_ncdf

!=======================================================================
!
! !INTERFACE:
   subroutine check(status, iret)
!
! !DESCRIPTION:
! test success of netcdf-operation
!
! !USES:
   integer,           intent(in)  :: status
   integer,           intent(out) :: iret
!
!-----------------------------------------------------------------------

   iret = 0
   if(status /= nf90_noerr) then
      call write_log_message( trim(nf90_strerror(status)) )
      iret = 1
   end if

   return

   end subroutine check

!=======================================================================
!
! !INTERFACE:
   integer function set_attributes(ncid,id,                         &
                                   units,long_name,                 &
                                   valid_min,valid_max,valid_range, &
                                   scale_factor,add_offset,         &
                                   FillValue,missing_value)
#define FUNCTION_NAME 'set_attributes'
!
! !DESCRIPTION:
!  This routine is used to set a number of attributes for variables.
!  The routine makes heavy use of the optional keyword.
!  The list of recognized keywords is very easy to extend. We have
!  included a sub-set of the COARDS conventions.
!  Original author(s): Karsten Bolding & Hans Burchard
!
! !USES:
  IMPLICIT NONE
!
! !INPUT PARAMETERS:
   integer,         intent(in)     :: ncid,id
   character(len=*), optional      :: units,long_name
   real, optional                  :: valid_min,valid_max
   real, optional                  :: valid_range(2)
   real, optional                  :: scale_factor,add_offset
   real, optional                  :: FillValue,missing_value
!
! !LOCAL VARIABLES:
   integer                         :: iret
   real(kind=nc_kind)              :: r4(2)
!
!-----------------------------------------------------------------------
#include "call-trace.inc"

   set_attributes = 0

   if(present(units)) then
       iret = nf90_put_att(ncid,id,'units',units)
       set_attributes = set_attributes + iret
   end if

   if(present(long_name)) then
       iret = nf90_put_att(ncid,id,'long_name',long_name)
       set_attributes = set_attributes + iret
   end if

   if(present(valid_min)) then
       r4(1) = real(valid_min,nc_kind)
       iret = nf90_put_att(ncid,id,'valid_min',r4(1))
       set_attributes = set_attributes  + iret
   end if

   if(present(valid_max)) then
       r4(1) = real(valid_max,nc_kind)
       iret = nf90_put_att(ncid,id,'valid_max',r4(1))
       set_attributes = set_attributes  + iret
   end if

   if(present(valid_range)) then
       r4(1) = real(valid_range(1),nc_kind)
       r4(2) = real(valid_range(2),nc_kind)
       iret = nf90_put_att(ncid,id,'valid_range',r4)
       set_attributes = set_attributes  + iret
   end if

   if(present(scale_factor)) then
      r4(1) = real(scale_factor,nc_kind)
      iret = nf90_put_att(ncid,id,'scale_factor',r4(1))
      set_attributes = set_attributes  + iret
   end if

   if(present(add_offset)) then
      r4(1) = real(add_offset,nc_kind)
      iret = nf90_put_att(ncid,id,'add_offset',r4(1))
      set_attributes = set_attributes  + iret
   end if

   if(present(FillValue)) then
      r4(1) = real(FillValue,nc_kind)
      iret = nf90_put_att(ncid,id,'_FillValue',r4(1))
      set_attributes = set_attributes  + iret
   end if

   if(present(missing_value)) then
      r4(1) = real(missing_value,nc_kind)
      iret = nf90_put_att(ncid,id,'missing_value',r4(1))
      set_attributes = set_attributes  + iret
   end if

   return

   end function set_attributes

!=======================================================================
!
! !INTERFACE:
   integer function store_data(ncid,id,scalar,array1d,array2d,array3d)
#define FUNCTION_NAME 'set_attributes'
!
! !DESCRIPTION:
!  This routine is used to store a  variable in the NetCDF file.
!  The subroutine uses optional parameters to find out which data type to save.
!  (function based on "store_data" from GOTM)
!
! !USES:
   IMPLICIT NONE
!
! !INPUT PARAMETERS:
   integer,         intent(in) :: ncid,id
   real, optional              :: scalar
   real, optional              :: array1d(NLVLS)
   real, optional              :: array2d(NLONS,NLATS)
   real, optional              :: array3d(NLONS,NLATS,NLVLS)
!
! !LOCAL VARIABLES:
   integer                     :: n, iret
   real(kind=nc_kind)          :: r4_0d
   real(kind=nc_kind)          :: r4_1D(NLVLS)
   real(kind=nc_kind)          :: r4_2d(NLONS,NLATS)
   real(kind=nc_kind)          :: r4_3d(NLONS,NLATS,NLVLS)
!
!-----------------------------------------------------------------------
#include "call-trace.inc"

   store_data = 0

   n=0
   if(present(scalar))  n = n+1
   if(present(array1d)) n = n+1
   if(present(array2d)) n = n+1
   if(present(array3d)) n = n+1
   if (n.eq.0) then
      store_data = store_data + 1
      call write_log_message('At least one optional argument has to be passed to - store_data()')
      return
   endif
   if (n .ne. 1) then
      store_data = store_data + 1
      call write_log_message('Only one optional argument must be passed to - store_data()')
      return
   endif

   if (present(scalar)) then
      r4_0d = real(scalar,nc_kind)
      call check( nf90_put_var(ncid, id, r4_0d, (/ nc_start(4) /) ), iret )
      if (iret.ne.0) then
         store_data = store_data + iret
         call write_log_message('ERROR writing scalar to nc_data')
         return
      endif
   endif

   if (present(array1d)) then
      r4_1d = real(array1d,nc_kind)
      call check( nf90_put_var(ncid, id, r4_1d, start=nc_start(3:4), count=nc_count(3:4)), iret )
      if (iret.ne.0) then
         store_data = store_data + iret
         call write_log_message('ERROR writing array1d to nc_data')
         return
      endif
   endif

   if (present(array2d)) then
      r4_2d = real(array2d,nc_kind)
      call check( nf90_put_var(ncid, id, r4_2d, start=(/nc_start(1:2), nc_start(4)/),   &
                                                count=(/nc_count(1:2), nc_count(4)/) ), iret )
      if (iret.ne.0) then
         store_data = store_data + iret
         call write_log_message('ERROR writing array2d to nc_data')
         return
      endif
   endif

   if (present(array3d)) then
      r4_3d = real(array3d,nc_kind)
      call check( nf90_put_var(ncid, id, r4_3d, start=nc_start, count=nc_count), iret )
      if (iret.ne.0) then
         store_data = store_data + iret
         call write_log_message('ERROR writing array3d to nc_data')
         return
      endif
   endif

   return

   end function store_data

!=======================================================================
#endif
   end module mod_ncdfout

!=======================================================================
