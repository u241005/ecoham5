#ifdef sediment_EMR
! !MODULE: eco_sediment_emr.f90 --- benthic biology for ecoham from Ernst-Meier-Reimer
!
! !INTERFACE:
   MODULE mod_sediment
#ifdef module_sediment
!
! !DESCRIPTION:
! This module is the container for benthic biogeochemical processes (subroutines)
!
! !USES:
   use mod_common
   use mod_grid
#ifdef MPI
   use mod_mpi_parallel
#endif
   use mod_var
   use mod_flux
   use mod_param_EMR
   implicit none
!
!  default: all is private.
   private
!
! !PUBLIC MEMBER FUNCTIONS:
   public init_sediment, do_sediment, warm_in_EMR, warm_out_EMR
   public sd, n_sed_var
   public dth, dzzs, z_sed, vdc_sediment, porsol, porwat
   public powtra, sol_diss, sedict, sedlay, sedhpl, mix_diss
   public rcar, r0, ro2ut, nitdem, n2prod, rnit, adv_sol
   public poc_sed, pon_sed, pop_sed, cal_sed, sil_sed, ter_sed
   public poc_sey, pon_sey, pop_sey, cal_sey, sil_sey, ter_sey
   public sedfluxo, sedfluxd, sedfluxy, dzero, azero, gypens
!
! !PUBLIC VARIABLES:
   integer, parameter :: n_sed_var_EMR = 14
   integer, parameter :: max_flux_sed_EMR = 9
   ! INDEX-pointers to sediment STATE VARIABLES:
   integer, parameter :: ised_poc=1 ,ised_pon=2 ,ised_pok=3
   integer, parameter :: ised_pop=4 ,ised_pos=5 ,ised_ter=6
   integer, parameter :: ipow_dic=7 ,ipow_o2o=8 ,ipow_n5s=9
   integer, parameter :: ipow_n3n=10,ipow_ph =11,ipow_alk=12
   integer, parameter :: ipow_nn2=13,ipow_n4n=14
   !
   real, dimension(:,:), allocatable, public :: f_poc_sed
   real, dimension(:,:), allocatable, public :: f_pon_sed
   real, dimension(:,:), allocatable, public :: f_pop_sed
   real, dimension(:,:), allocatable, public :: f_pos_sed
   real, dimension(:,:), allocatable, public :: f_pok_sed
   real, dimension(:,:), allocatable, public :: f_n3n_brm
   real, dimension(:,:), allocatable, public :: f_o2o_brm
   real, dimension(:,:), allocatable, public :: f_sed_o2o      !seti_o2o
   real, dimension(:,:), allocatable, public :: f_sed_dic      !seti_dic
   real, dimension(:,:), allocatable, public :: f_sed_n4n      !seti_n4n
   real, dimension(:,:), allocatable, public :: f_sed_n3n      !seti_n3n
   real, dimension(:,:), allocatable, public :: f_sed_n1p      !seti_n1p
   real, dimension(:,:), allocatable, public :: f_sed_n5s      !seti_n5s
   real, dimension(:,:), allocatable, public :: f_sed_nn2      !seti_nn2
   real, dimension(:,:), allocatable, public :: f_sed_o3c      !seti_o3c
   real, dimension(:,:), allocatable, public :: f_sed_alk      !seti_alk
!
! !GRID PARAMETERS:
   integer, parameter, public ::  ks = 12     ! number of sediment layers (k-dimension)
!
! !LOCAL VARIABLES (private):
   ! grid
   real, dimension(ks+1)                :: dzs      ! distance betweeen mid points of sediment layers
   real, dimension(ks+1)                :: dzzs     ! thickness of sediment layers
   real, dimension(ks+1)                :: z_sed    ! depth of sediment layers for output in bgcmean
   !real, dimension(:,:),    allocatable :: dz2      ! thickness of deepest pelagic layer
   !real, dimension(:,:),    allocatable :: dep2     ! mean depth of this layer
   real, dimension(:,:),    allocatable :: bolay    ! layer thickness (m) of pelagic bottom layer
   !real, dimension(:,:),    allocatable :: ptiestu  ! depth (m) of deepest zeta in pelagic bottom layer
   logical, dimension(:,:), allocatable :: mask2    ! logical land-sea-mask (water=.true.)
   ! local copies of pelagic variables
   !real, dimension(:,:),    allocatable :: dic2d, alk2d, n3n2d, n4n2d
   !real, dimension(:,:),    allocatable :: n1p2d, o2o2d, n5s2d
   real, dimension(:,:),    allocatable :: psao, ptho, tfac
   real, dimension(:,:),    allocatable :: prorca, prcaca, silpro
   real, dimension(:,:),    allocatable :: cinflux, ninflux, hi
   ! local arrays
   real, dimension(:,:),    allocatable :: aksp_sed
   real, dimension(:,:),    allocatable :: akw3, akb3, ak13, ak23
   real, dimension(:,:),    allocatable :: corflux
   real, dimension(:,:),    allocatable :: porflux
   real, dimension(:,:),    allocatable :: norflux
   real, dimension(:,:),    allocatable :: produs
   real, dimension(:,:),    allocatable :: poc_sed, poc_sey
   real, dimension(:,:),    allocatable :: pon_sed, pon_sey
   real, dimension(:,:),    allocatable :: pop_sed, pop_sey
   real, dimension(:,:),    allocatable :: cal_sed, cal_sey
   real, dimension(:,:),    allocatable :: sil_sed, sil_sey
   real, dimension(:,:),    allocatable :: ter_sed, ter_sey
   real, dimension(:,:),    allocatable :: bolven
   real, dimension(:,:,:),  allocatable :: porsol
   real, dimension(:,:,:),  allocatable :: porwat
   real, dimension(:,:,:),  allocatable :: porwah
   real, dimension(:,:,:),  allocatable :: ph
   real, dimension(:,:,:),  allocatable :: phneu
   real, dimension(:,:,:),  allocatable :: sedhpl
   real, dimension(:,:,:),  allocatable :: pown2bud
   real, dimension(:,:,:),  allocatable :: powh2obud
   real, dimension(:,:,:),  allocatable :: ocetra   ! container for ocean tracers

   real, dimension(:,:,:),  allocatable :: burial, sedsum
   real, dimension(:,:,:),  allocatable :: sedfluxo, sedfluxd, sedfluxy
   real, dimension(:,:,:),  allocatable :: bgcsedfl, powsum
   real, dimension(:,:,:,:),allocatable :: sedlay, adv_sol
   real, dimension(:,:,:,:),allocatable :: powtra, sedict, mix_diss, sol_diss

   ! Paulmier_coefficients
   real, dimension(:,:,:),  allocatable :: rcar
   real, dimension(:,:,:),  allocatable :: rnit
   real, dimension(:,:,:),  allocatable :: r0
   real, dimension(:,:,:),  allocatable :: ro2ut
   real, dimension(:,:,:),  allocatable :: nitdem
   real, dimension(:,:,:),  allocatable :: n2prod

   real, dimension(ks+1) :: seddw
   real, dimension(ks+1) :: seddzi
   
   real, dimension(npowtra) :: azero,dzero
   logical                  :: gypens = .true.

   ! local scalars
   real    :: calfa, oplfa, orgfa, clafa, solfu ! volumetric coefficients
   real    :: dt_sed, dth
   integer :: iWarm_EMR = 1

!    character(len=*), parameter :: init_endian   = 'BIG_ENDIAN'
   character(len=*), parameter :: input_dir       = 'Input/'
!    integer,          parameter :: init_realkind = 8   != ibyte_per_real
   character(len=99) :: warmstart_file_sediment   = 'warmstart_EMR.in'
!
! ***************************************************************************
! These variables should be defined as parameter or filled via namelist  ...
! ***************************************************************************
! densities etc. for SEDIMENT SHIFTING
! define weight of calcium carbonate, opal, and poc [kg/kmol] !wk: [mg/mmol]
      real, parameter :: calcwei = 100. ! 40+12+3*16 kg/kmol C !!wk: dieselben zahlen fuer einheit mg/mmol
      real, parameter :: opalwei = 60.  ! 28 + 2*16  kg/kmol Si
      real, parameter :: orgwei  = 30.  ! from 12 kg/kmol * 2.5 POC[kg]/DW[kg]
                     ! after Alldredge, 1998: POC(g)/DW(g) = 0.4 of diatom marine snow, size 1mm3
                     ! wk: why factor 2.5 - not clear??
! define densities of caco3, opal, poc and clay [kg/m3]        wk: [mg/m3]
      real, parameter :: calcdens = 2.6e9
      real, parameter :: opaldens = 2.2e9
      real, parameter :: orgdens  = 1.1e9
      real, parameter :: claydens = 2.6e9                 !quartz

      !remineralization rates
      real, parameter :: disso = 2.e-12           !wk: dissolution rate opal            [1/(mmol Si(OH)4/m3)*1/sec]
      real, parameter :: disso1= (1.e-10)*2.0     !wk: remineralisation rate POP (aerob)[1/(mmol O2/m3)*1/sec]
      real, parameter :: disso2= 1.e-12           !wk: dissolution rate CaCO3           [1/(mmol CO3--/m3)*1/sec]
      real, parameter :: denit = 150./86400.      !wk: denitrification rate             [1/sec]
      real, parameter :: surat = 0.0006/86400.    !jp: sulfate reduction rate           [1/sec]
      real, parameter :: nirat = 0.25/86400.      !jp: nitrification rate               [1/sec]
      real, parameter :: prod  = 0.0333*3.        !jp: Clay Sedimentation               [mg/s/m2]
!      real, parameter :: prod  = 0.0           !mk: for similarity with ecoham4_sng 

      real, parameter :: silsat= 1.e3             ! Silicate saturation concentration is 1 mol/m3

 ! define vertical diffusion coefficient for sediment
     real, parameter, dimension (ks):: vdc_sediment = 2.e-9  !m2/s  
     
!
! integer, parameter :: ii=17, jj=30, kk=1
! integer, parameter :: ii=32, jj=14, kk=1
  integer, parameter :: ii=3,  jj=5,  kk=1
!
!=======================================================================

   contains

!=======================================================================
!
! !INTERFACE:
   subroutine init_sediment(ierr)
#define SUBROUTINE_NAME 'init_sediment'
!
! !DESCRIPTION:
!
! !USES:
   use mod_utils, only: DOYstring
   implicit none
!
! !INPUT PARAMETERS:
   integer, intent(inout) :: ierr
!
! !LOCAL VARIABLES:
   integer                :: i, j
#ifdef debugEMR
   integer                :: k, n
#endif
!
!-------------------------------------------------------------------------
#include "call-trace.inc"

#ifndef sediment_EMR
   ierr = 1
   write(log_msg(1),'("##########################################################################")')
   write(log_msg(2),'("# ERROR: you can not run this code without flag -Dsediment_EMR    #")')
   write(log_msg(3),'("#        for using the EMR model please enable and re-compile            #")')
   write(log_msg(4),'("##########################################################################")')
   long_msg = trim( log_msg(1)) // new_line('a') // trim( log_msg(2)) // new_line('a') // &
            & trim( log_msg(3)) // new_line('a') // trim( log_msg(4))
   call write_log_message(long_msg); long_msg = ''
   call stop_ecoham(ierr, msg=long_msg); long_msg = ''
#endif

#ifdef old_sed_coupling
   ierr = 1
   write(log_msg(1),'("##########################################################################")')
   write(log_msg(2),'("# ERROR: you can not run this code with flag -Dold_sed_coupling enabled  #")')
   write(log_msg(3),'("#        for using the EMR model please disable and re-compile           #")')
   write(log_msg(4),'("##########################################################################")')
   long_msg = trim( log_msg(1)) // new_line('a') // trim( log_msg(2)) // new_line('a') // &
            & trim( log_msg(3)) // new_line('a') // trim( log_msg(4))
   call write_log_message(long_msg); long_msg = ''
   call stop_ecoham(ierr, msg=long_msg); long_msg = ''
#endif

   if (myID==master) call write_log_message(" INITIALIZING sediment_emr ")

   ! initialise arrays for sediment statevariables
   n_sed_var = n_sed_var_EMR
   allocate(  sd(jmax,iStartD:iEndD,n_sed_var)) ; sd = fail
   !allocate( ssd(jmax,iStartD:iEndD,n_sed_var))
   !call empty_ssd(ierr)

   ! initialise arrays for fluxes from sediment to pelagial
   max_flux_sed = max_flux_sed_EMR
   allocate( seti_flux(jmax,iStartD:iEndD,max_flux_sed))
   allocate( sedi_flux(jmax,iStartD:iEndD,max_flux_sed))

   ! initialize meta data for output (sd var)
   allocate( sd_out2D(n_sed_var)); sd_out2D = .false.
   allocate( sd_name (n_sed_var))
   allocate( sd_long (n_sed_var))
   allocate( sd_unit (n_sed_var))
   allocate( sedi_flux_out2D(max_flux_sed)); sd_out2D = .false.
   allocate( sedi_flux_name (max_flux_sed))
   allocate( sedi_flux_long (max_flux_sed))
   allocate( sedi_flux_unit (max_flux_sed))
   call set_varnames_emr(ierr)

   ! define grid properties for benthic domain
   call init_sediment_grid(ierr)

   ! allocate and initialize local arrays
   call allocate_sediment_vars(ierr)

   ! define sediment porosity, porewater and solid volumes
   call init_sediment_porosity(ierr)

   !initialize as coldstart
   if (iwarm_EMR /= 1) then

      if (myID==master) call write_log_message("   coldstart initialization")
      call init_aquatic_tracers(ierr)
      call init_pore_water_tracers(ierr)
      dth = 0.
      if (iwarm == 2) then
        ! read from initial distribution files, if exist, and overwrite
      endif

   else !initialize as warmstart
      call warm_in_EMR(ierr)

      ! Initial values for aquatic (advected) ocean tracers (for warmstart)
      do i = iStartD, iEndD
         do j = 1, jmax
            if (ixistk(j,i) /= 1) cycle
            ocetra(j,i,isco212) = powtra(j,1,i,ipowaic)     ! [kmol/m3] wk: [mmol/m3]
            ocetra(j,i,ialkali) = powtra(j,1,i,ipowaal)
            ocetra(j,i,iano3)   = powtra(j,1,i,ipowno3)
            ocetra(j,i,ianh4)   = powtra(j,1,i,ipownh4)
            ocetra(j,i,iphosph) = powtra(j,1,i,ipowaph)
            ocetra(j,i,isilica) = powtra(j,1,i,ipowasi)
            ocetra(j,i,ioxygen) = powtra(j,1,i,ipowaox)
            ocetra(j,i,igasnit) = powtra(j,1,i,ipown2)
         enddo
      enddo

   endif

   ! seddzi(1) - in m-1 - for diffusion into the pelagic: (jp: is this not a decrease of the coeff?)
   seddzi(1) = 500.

! define volumes occupied by solid constituents [m3/kmol]   !wk: [m3/mmol]
   calfa = calcwei/calcdens         ! 3.85e-2    !wk: 3.85e-8
   oplfa = opalwei/opaldens         ! 2.73e-2    !wk: 2.73e-8
   orgfa = orgwei/orgdens           ! 3.0e-2     !wk: 2.73e-8
   clafa = 1./claydens              ! 3.85e-4 (in m3/kg)!wk: 3.85e-10 m3/mg

! determine parameter dependent diffusion values (Gypens et al 2008)
   dzero(ipowno3) = 3.5208e-6/3600. !m2 s-1
   azero(ipowno3) = 0.14e-6/3600.   !m2 s-1 degC-1

   dzero(ipowaic) = dzero(ipowno3)
   azero(ipowaic) = azero(ipowno3)

   dzero(ipowaal) = dzero(ipowno3)
   azero(ipowaal) = azero(ipowno3)

   dzero(ipowasi) = dzero(ipowno3)
   azero(ipowasi) = azero(ipowno3)

   dzero(ipownh4) = dzero(ipowno3)
   azero(ipownh4) = azero(ipowno3)

   dzero(ipowaph) = 1.2889e-6/3600.  !m2 s-1
   azero(ipowaph) = 0.06377e-6/3600. !m2 s-1 degC-1

   dzero(ipowaox) = 3.9797e-6/3600. !m2 s-1
   azero(ipowaox) = 0.1608e-6/3600. !m2 s-1 degC-1

   dzero(ipown2)  = dzero(ipowaox)
   azero(ipown2)  = azero(ipowaox)

   !call update_sd_vars

   if(ierr/=0) then
      write(error_message,'("ERROR - init_sediment_emr ",i3)') ierr
      call stop_ecoham(ierr, msg=error_message)
   endif
   return

   end subroutine init_sediment
   !
!=======================================================================
!
! !INTERFACE:
   subroutine set_varnames_emr(ierr)
#define SUBROUTINE_NAME 'set_varnames_emr'
!
! !DESCRIPTION:
! !
! !USES:
   implicit none
!
! !OUTPUT PARAMETERS:
   integer, intent(inout)  :: ierr
!
! !LOCAL VARIABLES:
   integer            :: m
!
!-----------------------------------------------------------------------
#include "call-trace.inc"

   ! clear character arrays
   sd_name(:) = ''
   sd_long(:) = ''
   sd_unit(:) = ''

   ! set varnames for prognostic sediment variables ("sd") - "EMR"
   ! sediment dissolved state variables
   sd_name(ipow_dic)='sd_dic'  !ipowaic
   sd_long(ipow_dic)='SEDIMENT POREWATER INORGANIC CARBON'
   sd_unit(ipow_dic)='unit?' !'mmolC m-2'

   sd_name(ipow_o2o)='sd_oxy'  !ipowaox
   sd_long(ipow_o2o)='SEDIMENT POREWATER OXYGEN'
   sd_unit(ipow_o2o)='unit?' !''mmolC m-2'

   sd_name(ipow_n5s)='sd_n5s'  !ipowasi
   sd_long(ipow_n5s)='SEDIMENT POREWATER SILICATE'
   sd_unit(ipow_n5s)='unit?' !''mmolC m-2'

   sd_name(ipow_n3n)='sd_n3n'  !ipowno3
   sd_long(ipow_n3n)='SEDIMENT POREWATER NITRATE'
   sd_unit(ipow_n3n)='unit?' !''mmolC m-2'

   sd_name(ipow_ph) ='sd_ph'  !ipowaph
   sd_long(ipow_ph) ='SEDIMENT POREWATER PH'
   sd_unit(ipow_ph) ='-'

   sd_name(ipow_alk)='sd_alk'  !ipowaal
   sd_long(ipow_alk)='SEDIMENT POREWATER ALKALINITY'
   sd_unit(ipow_alk)='unit?' ! 'mmolC m-2'

   sd_name(ipow_nn2)='sd_nn2'  !ipown2
   sd_long(ipow_nn2)='SEDIMENT POREWATER DINITROGEN (N2)'
   sd_unit(ipow_nn2)='unit?' ! 'mmolC m-2'

   sd_name(ipow_n4n)='sd_n4n'  !ipownh4
   sd_long(ipow_n4n)='SEDIMENT POREWATER AMMONIUM'
   sd_unit(ipow_n4n)='unit?' ! 'mmolC m-2'

   ! sediment particulate state variables
   sd_name(ised_poc)='sd_poc'  !issspoc
   sd_long(ised_poc)='SEDIMENT PARTICULATE CARBON'
   sd_unit(ised_poc)='mmolC m-2'

   sd_name(ised_pon)='sd_pon'  !issspon
   sd_long(ised_pon)='SEDIMENT PARTICULATE NITROGEN'
   sd_unit(ised_pon)='mmolN m-2'

   sd_name(ised_pop)='sd_pop'  !issso12
   sd_long(ised_pop)='SEDIMENT PARTICULATE PHOSPHORUS'
   sd_unit(ised_pop)='mmolP m-2'

   sd_name(ised_pos)='sd_pos' !issssil
   sd_long(ised_pos)='SEDIMENT PARTICULATE SILICATE (OPAL)'
   sd_unit(ised_pos)='mmolSi m-2'

   sd_name(ised_pok)='sd_pok' !isssc12
   sd_long(ised_pok)='SEDIMENT PARTICULATE CALCITE (CaCO3)'
   sd_unit(ised_pok)='mmolC m-2'

   sd_name(ised_ter)='sd_ter' !issster
   sd_long(ised_ter)='SEDIMENT PARTICULATE TERIGEN'
   sd_unit(ised_ter)='mmolC m-2'

   ! fluxes from sediment to pelagial (remineralisation)
   sedi_flux_name(:) = ''
   sedi_flux_long(:) = ''
   sedi_flux_unit(:) = ''

   m=1; i_sed_dic = m; sedi_flux_name(m)='sed_dic'; sedi_flux_unit(m)='mmolC m-3 d-1'  ! seti_dic
   m=2; i_sed_n4n = m; sedi_flux_name(m)='sed_n4n'; sedi_flux_unit(m)='mmolN m-3 d-1'  ! seti_n4n
   m=3; i_sed_o3c = m; sedi_flux_name(m)='sed_o3c'; sedi_flux_unit(m)='mmolC m-3 d-1'  ! seti_o3c
   m=4; i_sed_n1p = m; sedi_flux_name(m)='sed_n1p'; sedi_flux_unit(m)='mmolP m-3 d-1'  ! seti_n1p
   m=5; i_sed_n5s = m; sedi_flux_name(m)='sed_n5s'; sedi_flux_unit(m)='mmolN m-3 d-1'  ! seti_n5s
   m=6; i_sed_nn2 = m; sedi_flux_name(m)='sed_nn2'; sedi_flux_unit(m)='mmolN m-3 d-1'  ! seti_nn2
   m=7; i_sed_n3n = m; sedi_flux_name(m)='sed_n3n'; sedi_flux_unit(m)='mmolN m-3 d-1'  ! seti_n3n
   m=8; i_sed_alk = m; sedi_flux_name(m)='sed_alk'; sedi_flux_unit(m)='mmol  m-3 d-1'  ! seti_alk
   m=9; i_sed_o2o = m; sedi_flux_name(m)='sed_o2o'; sedi_flux_unit(m)='mmol  m-3 d-1'  ! seti_o2o

   if (ierr/=0) then
      write( error_message, '("[",i2.2,"] ERROR - set_varnames_emr",i3)') myID, ierr
      call stop_ecoham(ierr, msg=error_message);
   end if
   return

   end subroutine set_varnames_emr

!=======================================================================
!
! !INTERFACE:
   subroutine do_sediment(td_,dt_,ierr)
#define SUBROUTINE_NAME 'do_sediment'
!
! !DESCRIPTION:
! routine handles benthic processes in 3d on global timestep (dt_)
!
! !USES:
   use mod_utils,  only: DOYstring
   use mod_hydro,  only: temp, salt
   implicit none
!
! !OUTPUT PARAMETERS:
   real,    intent(inout) :: td_,dt_
   integer, intent(inout) :: ierr
!
! !LOCAL VARIABLES:
   integer, parameter :: dt_EMR = 300 ! timestep for EMR-sediment (s)
   integer            :: iy, id, ih, im
   integer            :: i, j, k, k0, iv, is, nstep_EMR
   real               :: dtbgc, dt_step, dtemp, dt_fac, td_EMR
   character(len=19)  :: str
!
!-----------------------------------------------------------------------
#include "call-trace.inc"

   td_EMR = td_ ! global DOY

   !timestep
   dtbgc = dt_*secs_per_day       ! timestep (s)
   nstep_EMR = max(1, int(dtbgc /dt_EMR))
   dt_step   = dtbgc / nstep_EMR

   write(str,'(a19)') DOYstring(day_of_year)
   read (str,'(i4,x,i2,x,i2,x,i2)') iy, im, id, ih
   !print*,'enter do_sediment#387 ',iy,im,id,ih

   ! update ocean tracers
   call update_aquatic_tracers(ierr) ! update pelagic biogeo tracers

   ! update pelagic environment
!    do i = iStartCalc, iEndCalc
!       do j = jStartCalc, jEndCalc
   do i = iStartD, iEndD
      do j = 1, jmax
         if (.not. mask2(j,i)) cycle
         k0 = k_index(j,i)
         ptho (j,i) = temp(j,k0,i)
         psao (j,i) = salt(j,k0,i)
         bolay(j,i) = dz  (j,k0,i)
      enddo
   enddo

  ! determine diffusion rates sedict (Gypens et al 2008):
   if (gypens) then
      do i = iStartCalc, iEndCalc
         do j = jStartCalc, jEndCalc
            if (.not. mask2(j,i)) cycle
            do iv = 1, npowtra
               do k = 1, ks
                  dtemp = dzero(iv) + azero(iv)*ptho(j,i)
                  if (porwat(j,1,i) <0.4) then !sand
                     sedict(j,k,i,iv) = dtemp*porwat(j,k,i)*dt_step ! units(sedict): m**2
                  endif
                  if (porwat(j,1,i)>=0.4) then !mud
                     sedict(j,k,i,iv) = dtemp*porwat(j,k,i)*porwat(j,k,i)*dt_step ! units(sedict): m**2
                  endif
                  if (k==1) then
                     sedict(j,k,i,iv) = 10.*sedict(j,k,i,iv)
                  endif
               enddo
            enddo
         enddo
      enddo
   else !not gypens
      sedict = vdc_sediment(1)*dt_step
      do i = iStartCalc, iEndCalc
         do j = jStartCalc, jEndCalc
            if (.not. mask2(j,i)) cycle
            do iv = 1, npowtra
               k = 1
               sedict(j,k,i,iv)=3. * vdc_sediment(k)*dt_step  ! units(sedict): m**2
            enddo
         enddo
      enddo
   endif !if gypens

   ! set sinking fluxes accross pelagial interface on micro timestep (dt_step)
   dt_fac = dt_step/secs_per_day
   do i = iStartCalc, iEndCalc
      do j = jStartCalc, jEndCalc
         if (.not. mask2(j,i)) cycle
         ! sedimentation fluxes (mmol m-2)
         prorca(j,i) = f_pop_sed(j,i)*dt_fac     !wk: flux in P units
         prcaca(j,i) = f_pok_sed(j,i)*dt_fac     !jp: Calcite
         silpro(j,i) = f_pos_sed(j,i)*dt_fac     !jp: Opal
         cinflux(j,i)= f_poc_sed(j,i)*dt_fac     !jp: carbon
         ninflux(j,i)= f_pon_sed(j,i)*dt_fac     !jp: PON
      enddo
   enddo

   ! empty container for diffusive fluxes from EMR to pelagial
   sedfluxo = zero

   ! time loop
   do is = 1, nstep_EMR

      if (myID == master .and. nstep_EMR>1) then
         write(long_msg,'(6x,"EMR sub-step: ",i2," /",i2,3x,"time: ",f10.4)') is,nstep_EMR,td_EMR
         call write_log_message(long_msg); long_msg = ''
      endif

      call do_sediment_step(dt_step,ierr)

      td_EMR = td_EMR + dt_step/secs_per_day

   enddo ! time loop

   ! map sediment fluxes to the pelagic domain (mmol m-2 d-1)
   ! (sedfluxo fluxes are defined in subroutine DIPOWA)
   dt_fac = secs_per_day/dtbgc
   do i = iStartCalc, iEndCalc
      do j = jStartCalc, jEndCalc
         if (.not. mask2(j,i)) cycle
         k0 = k_index(j,i)
         f_sed_dic(j,i) = sedfluxo(j,i,ipowaic) *dt_fac  ! (mmol m-2 d-1)
         f_sed_alk(j,i) = sedfluxo(j,i,ipowaal) *dt_fac  ! (mmol m-2 d-1)
         f_sed_n3n(j,i) = sedfluxo(j,i,ipowno3) *dt_fac  ! (mmol m-2 d-1)
         f_sed_n4n(j,i) = sedfluxo(j,i,ipownh4) *dt_fac  ! (mmol m-2 d-1)
         f_sed_n1p(j,i) = sedfluxo(j,i,ipowaph) *dt_fac  ! (mmol m-2 d-1)
         f_sed_o2o(j,i) = sedfluxo(j,i,ipowaox) *dt_fac  ! (mmol m-2 d-1)
         f_sed_n5s(j,i) = sedfluxo(j,i,ipowasi) *dt_fac  ! (mmol m-2 d-1)
         f_sed_nn2(j,i) = sedfluxo(j,i,ipown2)  *dt_fac  ! (mmol m-2 d-1)
      enddo
   enddo

   ! integrate EMR 3D statevariables to SD output 2D vars (mmol m-2 d-1)
   sd = zero
   do i = iStartCalc, iEndCalc
      do j = jStartCalc, jEndCalc
         if (.not. mask2(j,i)) cycle
         do k = 1, ks
            sd(j,i,ised_poc) = sd(j,i,ised_poc) + sedlay(j,k,i,issspoc) *dzzs(k) !MK wie lautet hier die richtige Umrechnung?
            sd(j,i,ised_pon) = sd(j,i,ised_pon) + sedlay(j,k,i,issspon) *dzzs(k) !MK wie lautet hier die richtige Umrechnung?
            sd(j,i,ised_pok) = sd(j,i,ised_pok) + sedlay(j,k,i,isssc12) *dzzs(k) !MK wie lautet hier die richtige Umrechnung?
            sd(j,i,ised_pop) = sd(j,i,ised_pop) + sedlay(j,k,i,issso12) *dzzs(k) !MK wie lautet hier die richtige Umrechnung?
            sd(j,i,ised_pos) = sd(j,i,ised_pos) + sedlay(j,k,i,issssil) *dzzs(k) !MK wie lautet hier die richtige Umrechnung?
            sd(j,i,ised_ter) = sd(j,i,ised_ter) + sedlay(j,k,i,issster) *dzzs(k) !MK wie lautet hier die richtige Umrechnung?
            sd(j,i,ipow_dic) = sd(j,i,ipow_dic) + powtra(j,k,i,ipowaic) *dzzs(k) !MK wie lautet hier die richtige Umrechnung?
            sd(j,i,ipow_o2o) = sd(j,i,ipow_o2o) + powtra(j,k,i,ipowaox) *dzzs(k) !MK wie lautet hier die richtige Umrechnung?
            sd(j,i,ipow_n5s) = sd(j,i,ipow_n5s) + powtra(j,k,i,ipowasi) *dzzs(k) !MK wie lautet hier die richtige Umrechnung?
            sd(j,i,ipow_n3n) = sd(j,i,ipow_n3n) + powtra(j,k,i,ipowno3) *dzzs(k) !MK wie lautet hier die richtige Umrechnung?
            sd(j,i,ipow_ph ) = sd(j,i,ipow_ph ) + powtra(j,k,i,ipowaph) *dzzs(k) !MK wie lautet hier die richtige Umrechnung?
            sd(j,i,ipow_alk) = sd(j,i,ipow_alk) + powtra(j,k,i,ipowaal) *dzzs(k) !MK wie lautet hier die richtige Umrechnung?
            sd(j,i,ipow_nn2) = sd(j,i,ipow_nn2) + powtra(j,k,i,ipown2 ) *dzzs(k) !MK wie lautet hier die richtige Umrechnung?
            sd(j,i,ipow_n4n) = sd(j,i,ipow_n4n) + powtra(j,k,i,ipownh4) *dzzs(k) !MK wie lautet hier die richtige Umrechnung?
         enddo
      enddo
   enddo

   if(ierr/=0) then
      write(error_message,'("ERROR - do_sediment_emr ",i3)') ierr
      call stop_ecoham(ierr, msg=error_message)
   endif
   return

   end subroutine do_sediment

!=======================================================================
!
! !INTERFACE:
   subroutine do_sediment_step(dt_,ierr)
#define SUBROUTINE_NAME 'do_sediment_step'
!
! !DESCRIPTION:
! routine handles benthic processes in 3d on EMR timestep (dt_)
!
! !USES:
   use mod_utils,  only: DOYstring
   implicit none
!
! !OUTPUT PARAMETERS:
   real,    intent(inout) :: dt_
   integer, intent(inout) :: ierr
!
! !LOCAL VARIABLES:
!
!-----------------------------------------------------------------------
#include "call-trace.inc"

   !update stoichiometric coefficients: rcar, rnit, ro2ut, nitdem, n2prod
   !write(*,'(''>>  call update_Paulmier_coefficients '')')
   call update_Paulmier_coefficients(ierr)


   !calculate pore water chemistry
   !write(*,'(''>>  call POWACHEXPL '',e14.6,i3)') dt_, ierr
   call POWACHEXPL(dt_, ierr)   ! pore water chemistry includes call to subroutine DIPOWA

   !calculate advection (sediment shift) of solids
   !write(*,'(''>>  call SEDSHI '')')
   call SEDSHI        ! sediment shift (of solids)

   !stop 'eco_sediment_EMR #482'
   if(ierr/=0) then
      write(error_message,'("ERROR - do_sediment_step ",i3)') ierr
      call stop_ecoham(ierr, msg=error_message)
   endif
   return

   end subroutine do_sediment_step
!
!=======================================================================
!
! !INTERFACE:
   subroutine SEDSHI
!
!$Source: /server/cvs/mpiom1/mpi-om/src_hamocc/sedshi.f90,v $\\
!$Revision: 1.2.2.1.20.1 $\\
!$Date: 2006/04/03 11:27:49 $\\
!**********************************************************************
!**** *SEDSHI* - .
!     Ernst Maier-Reimer,    *MPI-Met, HH*    10.04.01
!
!     Modified
!     --------
!     S.Legutke,        *MPI-MaD, HH*    10.04.01
!     - rename ssssil(j,k,i)=sedlay(j,k,i,issssil) etc.
!     I. Kriest         *MPI-Met, HH*,   27.05.03
!     - change specific weights for opal, CaCO3, POC --> bodensed.f90
!     - include upward transport
!     Purpose
!     -------
!
!     Method
!     -------
!     .
!**   Interface.
!     ----------
!     *CALL*       *SEDSHI*
!
!     Externals
!     ---------
!     none.
!**********************************************************************

   implicit none

   integer:: i,j,k,iv
   !real, dimension(jmax,iStartD:iEndD) :: wsed   ! shifting velocity for upward/downward shifts
   real, dimension(jmax,iStartD:iEndD) :: fulsed
   real :: sedlo,uebers
   real :: seddef                           ! sediment deficiency
   real :: spresent, buried
   real :: refill, frac, wsed
   real :: sedcal, sedcla, sedopl, sedorg
   !real :: burback, rf


! DOWNWARD SHIFTING
! shift solid sediment downwards, if layer is full, i.e., if
! the volume filled by the four constituents poc, opal, caco3, clay
! is more than porsol*seddw
! the outflow of layer i is given by sedlay(i)*porsol(i)*seddw(i), it is
! distributed in the layer below over a volume of porsol(i+1)*seddw(i+1)
!  print*,'ks=',ks

   do i = iStartCalc, iEndCalc
      do j = jStartCalc, jEndCalc
         if (.not. mask2(j,i)) cycle
         do k = 1, ks-1
            sedlo = orgfa*rcar(j,k,i)*sedlay(j,k,i,issso12) + calfa*sedlay(j,k,i,isssc12)  &
         &        + oplfa            *sedlay(j,k,i,issssil) + clafa*sedlay(j,k,i,issster)
!           poc = orgfa*sedlay(j,k,i,issso12)*rcar(j,k,i)
!           cal = calfa*sedlay(j,k,i,isssc12)
!           sil = oplfa*sedlay(j,k,i,issssil)
!           ter = clafa*sedlay(j,k,i,issster)

! "full" sediment has sedlo=1. for sedlo>1., wsed is >0.
            wsed = max(0., (sedlo - 1.) / (sedlo + 1.e-10))

! filling downward  (accumulation)
            do iv = 1,nsedtra
               uebers = wsed*sedlay(j,k,i,iv)
               sedlay (j,k  ,i,iv) = sedlay (j,k  ,i,iv) - uebers
               adv_sol(j,k  ,i,iv) = adv_sol(j,k  ,i,iv) - uebers
               uebers = uebers*(seddw(k)*porsol(j,k,i))/(seddw(k+1)*porsol(j,k+1,i))
               sedlay (j,k+1,i,iv) = sedlay (j,k+1,i,iv) + uebers
               adv_sol(j,k+1,i,iv) = adv_sol(j,k+1,i,iv) + uebers
            enddo !end iv-loop
         enddo !end k-loop
      enddo !end i-loop
   enddo !end j-loop


! store amount lost from bottom sediment layer - this is a kind of
! permanent burial in deep consolidated layer, and this stuff is
! effectively lost from the whole ocean+sediment(+atmosphere) system.
! Would have to be supplied by river runoff or simple addition e.g.
! to surface layers in the long range. Can be supplied again if a
! sediment column has a deficiency in volume.

   do i = iStartCalc, iEndCalc
      do j = jStartCalc, jEndCalc
         if (.not. mask2(j,i)) cycle
         sedlo = rcar(j,ks,i)*orgfa*sedlay(j,ks,i,issso12) + calfa*sedlay(j,ks,i,isssc12) &
               &            + oplfa*sedlay(j,ks,i,issssil) + clafa*sedlay(j,ks,i,issster)
         wsed = max(0., (sedlo - 1.) / (sedlo + 1.e-10))

         do iv = 1, nsedtra
            uebers = wsed*sedlay(j,ks,i,iv)
            sedlay (j,ks,i,iv) = sedlay (j,ks,i,iv) - uebers
            adv_sol(j,ks,i,iv) = adv_sol(j,ks,i,iv) - uebers
            burial (j,i,iv)    = burial(j,i,iv) + uebers*seddw(ks)*porsol(j,ks,i)
         enddo !end iv-loop
      enddo !end j-loop
   enddo !end i-loop

   return       !<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< switch off upward shift

! now the loading nowhere exceeds 1.

! digging from below in case of erosion (js and initialization with 0 or none, as currently)
! UPWARD SHIFTING
! shift solid sediment upwards, if total sediment volume is less
! than required, i.e., if the volume filled by the four constituents
! poc, opal, caco3, clay (integrated over the total sediment column)
! is less than porsol*seddw (integrated over the total sediment column)
! first, the deepest box is filled from below with total required volume;
! then, successively, the following layers are filled upwards.
! if there is not enough solid matter to fill the column, add clay. (js: so implicite initial state is all clay)

   fulsed  = 0.
!mk burback = 0.; rf = 0.

! determine how the total sediment column is filled
   do i = iStartCalc, iEndCalc
      do j = jStartCalc, jEndCalc
         if (.not. mask2(j,i)) cycle
         do k = 1, ks
            sedorg = orgfa * sedlay(j,k,i,issso12) * rcar(j,k,i)
            sedcal = calfa * sedlay(j,k,i,isssc12)
            sedopl = oplfa * sedlay(j,k,i,issssil)
            sedcla = clafa * sedlay(j,k,i,issster)
            sedlo  = sedorg + sedcal + sedopl + sedcla
            fulsed(j,i) = fulsed(j,i) + porsol(j,k,i)*seddw(k)*sedlo
          enddo
      enddo !end j-loop
   enddo !end i-loop

! shift the sediment deficiency from the deepest (burial)
! layer into layer ks
   do i = iStartCalc, iEndCalc
      do j = jStartCalc, jEndCalc
         if (.not. mask2(j,i)) cycle
! deficiency with respect to fully loaded sediment |packed in sedlay(j,k,is) ??
! this is the volume of sediment shifted upwards from the burial layer

         seddef = solfu - fulsed(j,i)  ! 'sediment deficiency', solfu = total column inegrated solid fraction volume (bodensed)

! total volume of solid constituents in buried layer
         spresent = rcar(j,ks,i)*orgfa*burial(j,i,issso12) + calfa*burial(j,i,isssc12) &
                  &            + oplfa*burial(j,i,issssil) + clafa*burial(j,i,issster)

! determine whether an additional amount of clay is needed from the burial
! layer to fill the whole sediment; I assume that there is an infinite
! supply of clay from below
         burial(j,i,issster) = burial(j,i,issster) + max(0., seddef - spresent) / clafa

! determine new volume of buried layer
         buried = rcar(j,ks,i)*orgfa*burial(j,i,issso12) + calfa*burial(j,i,isssc12)   &
                &            + oplfa*burial(j,i,issssil) + clafa*burial(j,i,issster)
! fill the deepest active sediment layer
         refill=seddef/buried

         frac = porsol(j,ks,i)*seddw(ks) !changed k to ks, ik
         sedlay(j,ks,i,issso12) = sedlay(j,ks,i,issso12) + refill*burial(j,i,issso12)/frac
!mk      rf = rf + refill*burial(j,i,issssil)/frac

         sedlay(j,ks,i,isssc12) = sedlay(j,ks,i,isssc12) + refill*burial(j,i,isssc12)/frac
         sedlay(j,ks,i,issssil) = sedlay(j,ks,i,issssil) + refill*burial(j,i,issssil)/frac
!mk      burback = burback + refill*burial(j,i,issssil)/frac
         sedlay(j,ks,i,issster) = sedlay(j,ks,i,issster) + refill*burial(j,i,issster)/frac

! account for losses in buried sediment
         burial(j,i,issso12) = burial(j,i,issso12) - refill*burial(j,i,issso12)
         burial(j,i,isssc12) = burial(j,i,isssc12) - refill*burial(j,i,isssc12)
         burial(j,i,issssil) = burial(j,i,issssil) - refill*burial(j,i,issssil)
         burial(j,i,issster) = burial(j,i,issster) - refill*burial(j,i,issster)

      enddo !end j-loop
   enddo !end i-loop

   ! redistribute overload of deepest layer ks to layers 2 to ks
   do i = iStartCalc, iEndCalc
      do j = jStartCalc, jEndCalc
         if (.not. mask2(j,i)) cycle
         do k = ks,2,-1
            sedlo = rcar(j,k,i)*orgfa*sedlay(j,k,i,issso12) + calfa*sedlay(j,k,i,isssc12)    &
                  &           + oplfa*sedlay(j,k,i,issssil) + clafa*sedlay(j,k,i,issster)
            wsed = max(0., (sedlo - 1.) / (sedlo + 1.e-10))

            do iv = 1, 4
               uebers = sedlay(j,k,i,iv)*wsed
               frac   = porsol(j,k,i)*seddw(k)/(porsol(j,k-1,i)*seddw(k-1))
               sedlay(j,k  ,i,iv) = sedlay(j,k,i,iv)-uebers
               sedlay(j,k-1,i,iv) = sedlay(j,k-1,i,iv)+uebers*frac   ! note k-1 here =upward shift
            enddo !end iv-loop
         enddo  !end k-loop
      enddo !end j-loop
   enddo !end i-loop

   return
   end subroutine sedshi
!
!=======================================================================
!
! !INTERFACE:
   subroutine POWACHEXPL(dt_, ierr)
!
!**********************************************************************
!
!**** *POWACHEXPL* - .
!
!     Ernst Maier-Reimer,    *MPI-Met, HH*    10.04.01
!
!     Modified
!     --------
!     S.Legutke,        *MPI-MaD, HH*    10.04.01
!     S.Lorenz/JO.Beismann, OpenMP parallel    *MPI-Met, HH*  24.08.07
!
!**   Interface.
!     ----------
!
!     *CALL*       *POWACHEXPL*
!     *COMMON*     *PARAM1_BGC.h* - declaration of ocean/sediment tracer.
!
!**   Interface to ocean model (parameter list):
!     -----------------------------------------
!
!     *real*    *psao*    - potential temperature [deg C].
!     *real*    *pwo*     - vertical velocity in scalar points [m/s].
!
!     Externals
!     ---------
!     none.
!ssso12 refers to the phosphorus of organic debris
!**********************************************************************

   implicit none

   real,    intent(in)     :: dt_
   integer :: i,j,k,iter, ierr

   real,dimension(jmax,ks,iStartD:iEndD) :: porwati, porsoli, remian
   real,dimension(jmax,ks) :: solrat, powcar, aerob, anaerob, ansulf
   real :: orgsed
#ifdef __c_isotopes
   real :: ratc13, ratc14, rato13, rato14, poso13, poso14
#endif
   real :: dissot, dissot1,dissot2,denitt,undsa,suratt,niratt
   real :: umfa,bt,alk,c,posol
   real :: ak1,ak2,akb,akw
   real :: h,t1,t2,a,dadh,dddhhh,satlev,o2o_old
   real, parameter :: ox_half = 50. !jp

! *****************************************************************
! accelerated sediment
! needed for boundary layer ventilation in fast sediment routine
!     real :: pwo(jmax,kpke+1,imax)
!     real :: bolven(jmax)

   !write(*,'(''>>  call CHEMCALC '',e20.10)') ptho(jj,ii)
   call CHEMCALC                 !calculates chemical constants

! A LOOP OVER I
! RJ: This loop must go from 1 to imax in the parallel version,
!     otherways we had to do a boundary exchange
   remian=0.
   porwati=1./porwat
   porsoli=1./porsol

   ierr=0
! Dissolution rate constant of opal (disso) [1/(kmol Si(OH)4/m3)*1/sec]
   dissot=disso*dt_

! Degradation rate constant of POP (disso) [1/(kmol O2/m3)*1/sec]
   dissot1=disso1*dt_

! Dissolution rate constant of CaCO3 (disso) [1/(kmol CO3--/m3)*1/sec]
   dissot2=disso2*dt_
   denitt = denit *dt_
   suratt = surat *dt_
   niratt = nirat *dt_

   !write(*,'(''CALCULATE SILICATE-OPAL CYCLE'')')
!    do i = iStartCalc, iEndCalc
!       do j = jStartCalc, jEndCalc
   do i = iStartD, iEndD
      do j = 1, jmax
         do k=1,ks
         solrat(j,k) = 0.         ! only used in powach.f90
         powcar(j,k) = 0.         ! carbonate ions concentration
         anaerob(j,k)= 0.
         aerob(j,k)  = 0.
         ansulf(j,k) = 0.
         enddo
      enddo

! CALCULATE SILICATE-OPAL CYCLE
!******************************************************************

! Calculate updated degradation rate from updated undersaturation.
! Calculate new solid sediment.
! Update pore water concentration from new undersaturation.

!       do j = jStartCalc, jEndCalc
      do j = 1, jmax
         if (.not. mask2(j,i)) cycle
         sedlay(j,1,i,issssil)=sedlay(j,1,i,issssil)+silpro(j,i)/(porsol(j,1,i)*seddw(1))  ! sinking flux
         do k=1,ks
            umfa=porsol(j,k,i)/porwat(j,k,i)       ! only for isotopes
            undsa=silsat-powtra(j,k,i,ipowasi)     ! undersaturation of silicate
            posol=dissot*undsa*sedlay(j,k,i,issssil)
            sedlay(j,k,i,issssil)=sedlay(j,k,i,issssil)-posol*porsoli(j,k,i)
            powtra(j,k,i,ipowasi)=powtra(j,k,i,ipowasi)+posol*porwati(j,k,i)
            sol_diss(j,k,i,ipowasi) = sol_diss(j,k,i,ipowasi) + posol
         enddo
      enddo
   enddo

! CALCULATE CaCO3-CO3 CYCLE AND SIMULTANEOUS CO3-UNDERSATURATION DIFFUSION
!*************************************************************************
   !write(*,'(''CALCULATE CaCO3-CO3 CYCLE AND SIMULTANEOUS CO3-UNDERSATURATION DIFFUSION'')')
!    do i = iStartCalc, iEndCalc
   do i = iStartD, iEndD

! COMPUTE NEW POWCAR=CARBONATE ION CONCENTRATION IN THE SEDIMENT
! FROM CHANGED ALKALINITY (NITRATE PRODUCTION DURING REMINERALISATION)
! AND DIC GAIN. ITERATE 5 TIMES. THIS CHANGES PH (SEDHPL) OF SEDIMENT.

! as we have sedhpl ( code number 50) in the restart file one iteration is enough
      do ITER=1,10  ! calculates carbonate ion concentration
!          do j = jStartCalc, jEndCalc
         do j = 1, jmax
         if (.not. mask2(j,i)) cycle
            bt = rrrcl*psao(j,i)
            do k = 1, ks
!alkalinity is increased during denitrification due to consumption of H+ via NO3 (see Wolf-Gladrow etal,2007)
               alk = powtra(j,k,i,ipowaal)
               c   = powtra(j,k,i,ipowaic)
#ifdef __c_isotopes
               c   = c + powtra(j,k,i,ipowc13)
#endif
               ak1 = ak13(j,i)
               ak2 = ak23(j,i)
               akb = akb3(j,i)
               akw = akw3(j,i)
               h   = sedhpl(j,k,i)
               t1  = h/ak1
               t2  = h/ak2
               a    = c*(2.+t2) /(1.+t2+t2*t1) +akw/h -h +bt/(1.+h/akb) -alk
               dadh = c*( 1./(ak2*(1.+t2+t2*t1)) - (2.+t2)*(1./ak2 + 2.*t1/ak2)/(1.+t2+t2*t1)**2) &
                    &   -akw/h**2 -1.-(bt/akb)/(1.+h/akb)**2
               dddhhh = a/dadh
               sedhpl(j,k,i) = max(h - dddhhh, 1.e-11)    !proton concentration
               powcar(j,k)   = c / (1. + t2*(1.+t1))
            enddo
         enddo
      enddo

! Evaluate boundary conditions for sediment-water column exchange.
! Current undersaturation of bottom water: sedb(i,0) and
! Approximation for new solid sediment, as from sedimentation flux: solrat(i,1)

! CO3 saturation concentration is aksp/calcon as in CARCHM
! (calcon defined in BELEG_BGC with 1.03e-2; 1/calcon =~ 97.)

! Calculate updated degradation rate from updated undersaturation.
! Calculate new solid sediment.
! No update of powcar pore water concentration from new undersaturation so far.
! Instead, only update DIC, and, of course, alkalinity.
! This also includes gains from aerobic and anaerobic decomposition.

!       do j = jStartCalc, jEndCalc
      do j = 1, jmax
         if (.not. mask2(j,i)) cycle
         aksp_sed(j,i) = aksp_sed(j,i)*1.e6*1.e6
         do k = 1, ks
            satlev = aksp_sed(j,i)/calcon+2.e-5
!in oversaturated ( wrt calcite, powcar> satlev) water the "undersaturation" is negativ.
!there is evidence that in warm water like the Mediterranean spontaneous
!adsorption to existing calcareous shells happens. In most parts of the
!ocean this seems to be unlikely. Thus, we restrict on real undersaturation:
            undsa  = max(satlev - powcar(j, k), 0.)
            posol  = dissot2*sedlay(j,k,i,isssc12)*undsa
#ifdef __c_isotopes
            poso13 = dissot2*sedlay(j,k,i,isssc13)*undsa
            poso14 = dissot2*sedlay(j,k,i,isssc14)*undsa
            sedlay(j,k,i,isssc13) = sedlay(j,k,i,isssc13) - poso13*porsoli(j,k,i)
            sedlay(j,k,i,isssc14) = sedlay(j,k,i,isssc14) - poso14*porsoli(j,k,i)
            powtra(j,k,i,ipowc13) = powtra(j,k,i,ipowc13) + poso13*porwati(j,k,i)
            powtra(j,k,i,ipowc14) = powtra(j,k,i,ipowc14) + poso14*porwati(j,k,i)
            powtra(j,k,i,ipowaal) = powtra(j,k,i,ipowaal) + poso13*porwati(j,k,i)*2.
#endif
            sedlay(j,k,i,isssc12) = sedlay(j,k,i,isssc12) - posol*porsoli(j,k,i)
            powtra(j,k,i,ipowaic) = powtra(j,k,i,ipowaic) + posol*porwati(j,k,i)
            powtra(j,k,i,ipowaal) = powtra(j,k,i,ipowaal) + posol*porwati(j,k,i)*2.
            sol_diss(j,k,i,11)      = sol_diss(j,k,i,11)     + posol
            sol_diss(j,k,i,ipowaal) = sol_diss(j,k,i,ipowaal)+ posol*2.
         enddo
      enddo

!       do j = jStartCalc, jEndCalc
      do j = 1, jmax
         if (.not. mask2(j,i)) cycle
         sedlay(j,1,i,isssc12) = sedlay(j,1,i,isssc12) + prcaca(j,i)/(porsol(j,1,i)*seddw(1)) ! calcite influx
#ifdef __c_isotopes
         sedlay(j,1,i,isssc13) = sedlay(j,1,i,isssc13) + prca13(j,i)/(porsol(j,1,i)*seddw(1))
         sedlay(j,1,i,isssc14) = sedlay(j,1,i,isssc14) + prca14(j,i)/(porsol(j,1,i)*seddw(1))
!          prca13(j,i) = 0.
!          prca14(j,i) = 0.
#endif
      enddo
   enddo  !i loop

! CALCULATE OXYGEN-POC CYCLE (aerobic remineralisation)
!******************************************************
! This scheme is not based on undersaturation, but on O2 itself

! Evaluate boundary conditions for sediment-water column exchange.
! Current concentration of bottom water: sedb(i,0) and
! Approximation for new solid sediment, as from sedimentation flux: solrat(i,1)

!write(*,"CALCULATE OXYGEN-POC CYCLE (aerobic remineralisation)")
!    do i = iStartCalc, iEndCalc
   do i = iStartD, iEndD
! Calculate updated degradation rate from updated concentration.
! Calculate new solid sediment.
! Update pore water concentration.

!       do j = jStartCalc, jEndCalc
      do j = 1, jmax
         if (.not. mask2(j,i)) cycle
         do k = 1, ks
            umfa   = porsol(j,k,i)/porwat(j,k,i)
            posol  = dissot1*sedlay(j,k,i,issso12)*powtra(j,k,i,ipowaox)
#ifdef __c_isotopes
            poso13 = dissot1*sedlay(j,k,i,issso13)*powtra(j,k,i,ipowaox)
            poso14 = dissot1*sedlay(j,k,i,issso14)*powtra(j,k,i,ipowaox)
#endif
            aerob(j,k) = posol*umfa             !this has P units: kmol P/m3 of pore water
            o2o_old    = powtra(j,k,i,ipowaox)
            powtra(j,k,i,ipowaox) = powtra(j,k,i,ipowaox) - posol*porwati(j,k,i)*ro2ut(j,k,i)
            if (powtra(j,k,i,ipowaox).lt.0.) then
               print*,'# 1279 oxygen < 0',i,j,k,powtra(j,k,i,ipowaox)
               stop 'eco_sediment_emr #936'   !JP muss drinn bleiben
               ierr=1
               return
            endif
            sedlay(j,k,i,issso12) = sedlay(j,k,i,issso12) - posol*porsoli(j,k,i)
            sedlay(j,k,i,issspoc) = sedlay(j,k,i,issspoc) - posol*porsoli(j,k,i)*rcar(j,k,i)
            sedlay(j,k,i,issspon) = sedlay(j,k,i,issspon) - posol*porsoli(j,k,i)*rnit(j,k,i)
            powtra(j,k,i,ipowaph) = powtra(j,k,i,ipowaph) + posol*porwati(j,k,i)
            powtra(j,k,i,ipowno3) = powtra(j,k,i,ipowno3) + posol*porwati(j,k,i)*rnit(j,k,i)
            powtra(j,k,i,ipowaic) = powtra(j,k,i,ipowaic) + posol*porwati(j,k,i)*rcar(j,k,i)
            powtra(j,k,i,ipowaal) = powtra(j,k,i,ipowaal) - posol*porwati(j,k,i)*rnit(j,k,i)

            sol_diss(j,k,i,ipowaph) = sol_diss(j,k,i,ipowaph) + posol
            sol_diss(j,k,i,ipowno3) = sol_diss(j,k,i,ipowno3) + posol*rnit(j,k,i)
            sol_diss(j,k,i,ipowaic) = sol_diss(j,k,i,ipowaic) + posol*rcar(j,k,i)
            sol_diss(j,k,i,ipowaox) = sol_diss(j,k,i,ipowaox) - posol*ro2ut(j,k,i)
            sol_diss(j,k,i,ipowaal) = sol_diss(j,k,i,ipowaal) - posol*rnit(j,k,i)

#ifdef __c_isotopes
            sedlay(j,k,i,issso13) = sedlay(j,k,i,issso13) - poso13*porsoli(j,k,i)
            sedlay(j,k,i,issso14) = sedlay(j,k,i,issso14) - poso14*porsoli(j,k,i)
            powtra(j,k,i,ipowc13) = powtra(j,k,i,ipowc13) + poso13*porwati(j,k,i)*rcar(j,k,i)
            powtra(j,k,i,ipowc14) = powtra(j,k,i,ipowc14) + poso14*porwati(j,k,i)*rcar(j,k,i)
            powtra(j,k,i,ipowaph) = powtra(j,k,i,ipowaph) + poso13*porwati(j,k,i)
            powtra(j,k,i,ipowno3) = powtra(j,k,i,ipowno3) + poso13*porwati(j,k,i)*rnit(j,k,i)
            powtra(j,k,i,ipowaox) = powtra(j,k,i,ipowaox) - poso13*porwati(j,k,i)*ro2ut(j,k,i)
#endif
         enddo
      enddo
!       do j = jStartCalc, jEndCalc
      do j = 1, jmax
         if (.not. mask2(j,i)) cycle
         sedlay(j,1,i,issso12) = sedlay(j,1,i,issso12) + prorca (j,i)/(porsol(j,1,i)*seddw(1))
         sedlay(j,1,i,issspoc) = sedlay(j,1,i,issspoc) + cinflux(j,i)/(porsol(j,1,i)*seddw(1))
         sedlay(j,1,i,issspon) = sedlay(j,1,i,issspon) + ninflux(j,i)/(porsol(j,1,i)*seddw(1))

         porflux(j,i) = porflux(j,i) + prorca(j,i) !jp mitfuehren
         corflux(j,i) = corflux(j,i) + cinflux(j,i)
         norflux(j,i) = norflux(j,i) + ninflux(j,i)
#ifdef __c_isotopes
         sedlay(j,1,i,issso13) = sedlay(j,1,i,issso13) + pror13(j,i)/(porsol(j,1,i)*seddw(1))
         sedlay(j,1,i,issso14) = sedlay(j,1,i,issso14) + pror14(j,i)/(porsol(j,1,i)*seddw(1))
!          pror13(j,i)=0.
!          pror14(j,i)=0.
#endif
      enddo
   enddo

! CALCULATE NITRATE REDUCTION UNDER ANAEROBIC CONDITIONS EXPLICITELY
!*******************************************************************
! Denitrification rate constant of POP (denit) [1/sec]

!write(*,'("CALCULATE NITRATE REDUCTION UNDER ANAEROBIC CONDITIONS EXPLICITELY")')
!    do i = iStartCalc, iEndCalc
!       do j = jStartCalc, jEndCalc
   do i = iStartD, iEndD
      do j = 1, jmax
         if (.not. mask2(j,i)) cycle
         do k = 1, ks
            umfa = porsol(j,k,i)/porwat(j,k,i)         !only used in isotope part (not appropriate?)
#ifndef __c_isotopes
            orgsed = sedlay(j,k,i,issso12)
            if (orgsed.lt.0.) orgsed = 0.
#else
            orgsed = sedlay(j,k,i,issso12) + sedlay(j,k,i,issso13)

#endif
! anaerobic remineralisation - dependent on nitrate and POP (question: min(PON, POC,POP) better?)
            posol = denitt * MIN(0.5 * powtra(j,k,i,ipowno3)/nitdem(j,k,i),sedlay(j,k,i,issso12))
            posol = posol * (1. - powtra(j,k,i,ipowaox)/(ox_half + powtra(j,k,i,ipowaox)))
#ifdef __c_isotopes
            rato13 = sedlay(j,k,i,issso13)/(sedlay(j,k,i,issso12)+1.e-24)
            rato14 = sedlay(j,k,i,issso14)/(sedlay(j,k,i,issso12)+1.e-24)
            posol  = posol/(1.+rato13)
            poso13 = posol*rato13
            poso14 = posol*rato14
            sedlay(j,k,i,issso13) = sedlay(j,k,i,issso13) - poso13
            sedlay(j,k,i,issso14) = sedlay(j,k,i,issso14) - poso14
            powtra(j,k,i,ipowc13) = powtra(j,k,i,ipowc13) + poso13*umfa*rcar(j,k,i)
            powtra(j,k,i,ipowc14) = powtra(j,k,i,ipowc14) + poso14*umfa*rcar(j,k,i)
            powtra(j,k,i,ipowaph) = powtra(j,k,i,ipowaph) + poso13*umfa
            powtra(j,k,i,ipowno3) = powtra(j,k,i,ipowno3) + poso13*umfa*rnit(j,k,i)

#endif
            anaerob(j,k) = posol*umfa          !this has P units: kmol P/m3 of pore water
            sedlay(j,k,i,issso12) = sedlay(j,k,i,issso12) - posol*porsoli(j,k,i)
            sedlay(j,k,i,issspoc) = sedlay(j,k,i,issspoc) - posol*porsoli(j,k,i)*rcar(j,k,i)
            sedlay(j,k,i,issspon) = sedlay(j,k,i,issspon) - posol*porsoli(j,k,i)*rnit(j,k,i)
            powtra(j,k,i,ipowaph) = powtra(j,k,i,ipowaph) + posol*porwati(j,k,i)
            powtra(j,k,i,ipowaic) = powtra(j,k,i,ipowaic) + posol*porwati(j,k,i)*rcar(j,k,i)
            powtra(j,k,i,ipowno3) = powtra(j,k,i,ipowno3) - posol*porwati(j,k,i)*nitdem(j,k,i)
            powtra(j,k,i,ipowaal) = powtra(j,k,i,ipowaal) + posol*porwati(j,k,i)*nitdem(j,k,i)
            powtra(j,k,i,ipown2)  = powtra(j,k,i,ipown2)  + posol*porwati(j,k,i)*n2prod(j,k,i)
            powh2obud(j,k,i)      = powh2obud(j,k,i)      + posol*porwati(j,k,i)*n2prod(j,k,i)*0.5
            sol_diss(j,k,i,ipowaph) = sol_diss(j,k,i,ipowaph) + posol
            sol_diss(j,k,i,ipowaic) = sol_diss(j,k,i,ipowaic) + posol*rcar(j,k,i)
            sol_diss(j,k,i,ipown2 ) = sol_diss(j,k,i,ipown2 ) - posol*rnit(j,k,i)
            sol_diss(j,k,i,9)       = sol_diss(j,k,i,9)       - posol*nitdem(j,k,i)
            sol_diss(j,k,i,ipowaal) = sol_diss(j,k,i,ipowaal) + posol*nitdem(j,k,i)
         enddo   ! k
      enddo   ! j
   enddo   ! i

! sulphate reduction in sediments
! SR rate constant of POP (surat) [1/sec]
! Store flux in array anaerob, for later computation of DIC and alkalinity.

!write(*,'("CALCULATE SULPHATE REDUCTION IN SEDIMENTS")')
!    do i = iStartCalc, iEndCalc
!       do j = jStartCalc, jEndCalc
   do i = iStartD, iEndD
      do j = 1, jmax
         if (.not. mask2(j,i)) cycle
         do k=1,ks
            umfa = porsol(j,k,i)/porwat(j,k,i)
            if (powtra(j,k,i,ipowaox)<1. .and. powtra(j,k,i,ipowno3)<1.) then
#ifndef __c_isotopes
               orgsed = sedlay(j,k,i,issso12)
#else
               orgsed = sedlay(j,k,i,issso12) + sedlay(j,k,i,issso13)
#endif
               posol  = suratt * orgsed
#ifdef __c_isotopes
               rato13 = sedlay(j,k,i,issso13)/(sedlay(j,k,i,issso12)+1.e-24)
               rato14 = sedlay(j,k,i,issso14)/(sedlay(j,k,i,issso12)+1.e-24)
               posol  = posol/(1.+rato13)
               poso13 = posol*rato13
               poso14 = posol*rato14
               sedlay(j,k,i,issso13) = sedlay(j,k,i,issso13) - poso13
               sedlay(j,k,i,issso14) = sedlay(j,k,i,issso14) - poso14
               powtra(j,k,i,ipowc13) = powtra(j,k,i,ipowc13) + poso13*umfa*rcar(j,k,i)
               powtra(j,k,i,ipowc14) = powtra(j,k,i,ipowc14) + poso14*umfa*rcar(j,k,i)
               powtra(j,k,i,ipowaph) = powtra(j,k,i,ipowaph) + poso13*umfa
               powtra(j,k,i,ipowno3) = powtra(j,k,i,ipowno3) + poso13*umfa*rnit(j,k,i)
#endif
               ansulf(j,k) = posol*umfa         !this has P units: kmol P/m3 of pore water
               sedlay(j,k,i,issso12) = sedlay(j,k,i,issso12) - posol*porsoli(j,k,i)
               sedlay(j,k,i,issspoc) = sedlay(j,k,i,issspoc) - posol*porsoli(j,k,i)*rcar(j,k,i)
               sedlay(j,k,i,issspon) = sedlay(j,k,i,issspon) - posol*porsoli(j,k,i)*rnit(j,k,i)
               powtra(j,k,i,ipowaic) = powtra(j,k,i,ipowaic) + posol*porwati(j,k,i)*rcar(j,k,i)
               powtra(j,k,i,ipowaph) = powtra(j,k,i,ipowaph) + posol*porwati(j,k,i)
               powtra(j,k,i,ipownh4) = powtra(j,k,i,ipownh4) + posol*porwati(j,k,i)*rnit(j,k,i)
               powtra(j,k,i,ipowaal) = powtra(j,k,i,ipowaal) + posol*porwati(j,k,i)*rnit(j,k,i)
               sol_diss(j,k,i,ipowaic) = sol_diss(j,k,i,ipowaic) +posol*rcar(j,k,i)
               sol_diss(j,k,i,ipowaph) = sol_diss(j,k,i,ipowaph) +posol
               sol_diss(j,k,i,ipownh4) = sol_diss(j,k,i,ipownh4) +posol*rnit(j,k,i)
               sol_diss(j,k,i,ipowaal) = sol_diss(j,k,i,ipowaal) +posol*rnit(j,k,i)
            endif   ! oxygen <1.
         enddo   ! k
      enddo   ! j
   enddo   ! i

! nitrification of nh4 to no3, if o2 > 1 mmol O2 m-3

!write(*,'("CALCULATE NITRIFICATION (AEROBIC) OF NH4 TO NO3")')
!    do i = iStartCalc, iEndCalc
!       do j = jStartCalc, jEndCalc
   do i = iStartD, iEndD
      do j = 1, jmax
         if (.not. mask2(j,i)) cycle
         do k = 1, ks
            if (powtra(j,k,i,ipowaox).gt.1.) then
               powtra(j,k,i,ipowno3) = powtra(j,k,i,ipowno3) + niratt * powtra(j,k,i,ipownh4)*porwati(j,k,i)
               powtra(j,k,i,ipownh4) = powtra(j,k,i,ipownh4) - niratt * powtra(j,k,i,ipownh4)*porwati(j,k,i)
               powtra(j,k,i,ipowaox) = powtra(j,k,i,ipowaox) - niratt * powtra(j,k,i,ipownh4)*porwati(j,k,i)*2.
               if (powtra(j,k,i,ipowaox).lt.0.) powtra(j,k,i,ipowaox) = 0.
               powtra(j,k,i,ipowaal) = powtra(j,k,i,ipowaal) - niratt * powtra(j,k,i,ipownh4)*porwati(j,k,i)*2.
               sol_diss(j,k,i,10)      = sol_diss(j,k,i,10)      + niratt * powtra(j,k,i,ipownh4)
               sol_diss(j,k,i,ipowaox) = sol_diss(j,k,i,ipowaox) - niratt * powtra(j,k,i,ipownh4)*2.
               sol_diss(j,k,i,ipowaal) = sol_diss(j,k,i,ipowaal) - niratt * powtra(j,k,i,ipownh4)*2.
            endif  ! ox>1
         enddo  ! k
      enddo  ! j
   enddo  ! i

   !write(*,'("CALL DIPOWA")')
   call DIPOWA

!ik add clay sedimentation
!ik this is currently assumed to depend on total and corg sedimentation:
!ik f(POC) [kg C] / f(total) [kg] = 0.05
!ik thus it is

!write(*,'("ADD CLAY SEDIMENTATION")')

!    do i = iStartCalc, iEndCalc
!       do j = jStartCalc, jEndCalc
   do i = iStartD, iEndD
      do j = 1, jmax
         if (.not. mask2(j,i)) cycle
         produs(j,i) = prod*dt_
         sedlay(j,1,i,issster) = sedlay(j,1,i,issster) + produs(j,i)/(porsol(j,1,i)*seddw(1))
!          ter_sed(j,i) = ter_sed(j,i) + produs(j,i)
      enddo
   enddo

!    silpro = 0.
!    prorca = 0.
! #ifdef __c_isotopes
!    pror13 = 0.
!    pror14 = 0.
!    prca13 = 0.
!    prca14 = 0.
! #endif
!    prcaca = 0.
!    produs = 0.

   return
   end subroutine POWACHEXPL

!=======================================================================
!
! !INTERFACE:
   subroutine CHEMCALC

   implicit none

   real, parameter :: SMICR = 1.
   real, parameter :: THIRD = 1./3.
   real, parameter :: TEN   = 10.
   real, parameter :: PERC  = 0.01
   real, parameter :: tmelt = 273.15

   real    :: cek0, ckb, ck1, ck2, ckw, oxy, ani, cp, p, tc
   real    :: q, t, s, ak0, ak1, ak2, aksp0, akw, akb, cl
   real    :: chemcm(jmax,imax,8,12), satoxy(jmax,imax)
   integer :: kplmonth, kchem, kmon
   integer :: i, j

!*        21. CHEMICAL CONSTANTS - SURFACE LAYER
!             -------- --------- - ------- -----
   kplmonth = 1

!    do i = iStartCalc, iEndCalc
!       do j = jStartCalc, jEndCalc
   do i = iStartD, iEndD
      do j = 1, jmax
         if (.not. mask2(j,i)) cycle
!
!*        21.1 SET ABSOLUTE TEMPERATURE (SST) and salinity (SSS)
!              ------------------------
         T = ptho(j,i) + tmelt                ! degC to K
         Q = T*PERC                           ! perc=0.01
         s = max(25., psao(j,i))              ! minimum salinity 25

!      Laughing gas solubility (WEISS, 1974)
!      --------------------------------------
!      rs = a1 + a2 * (100. / t) + a3 * LOG(t/100.)                  &
!     &    +s*( b1 +b2*(t/100.) + b3*(t/100.)**2)
!       satn2o(j,i)=atn2o*exp(rs)
!
!*        21.2 CHLORINITY (WOOSTER ET AL., 1969)
!              ---------------------------------
!
         CL = S*SALCHL
!
!*        21.3 LN(K0) OF SOLUBILITY OF CO2 (EQ. 12, WEISS, 1974)
!              -------------------------------------------------
         cek0 = c00 + c01 / Q + c02 * LOG(q) + s * (c03 + c04 * q + c05 * Q**2)
!
!*        21.4 PK1, PK2 OF CARB. ACID, PKB OF BORIC ACID
!              -----------------------------------------
!*             AFTER EDMOND AND GIESKES (1970)
!              -------------------------------

         CKB = CB0/T + CB1*T + CB2 + CB3*CL**THIRD
         ck1 = c10 / t + c11 + c12 * s * LOG(t) + c13 * s**2
         ck2 = c20 / t + c21 + c22 * s * LOG(t) + c23 * s**2
!
!*        21.5 CKW (H2O) (DICKSON AND RILEY, 1979)
!              ------------------------------------

         CKW = CW0/T + CW1 + CW2*SQRT(S)
!
!*****CKW COULD ADDITIONALLY BE EXPRESSED SALIN. DEPENDENT *********
!
!        21.6 LN(K0) OF SOLUBILITY OF O2 (EQ. 4, WEISS, 1970)
!              -----------------------------------------------

         oxy = ox0 + ox1/q + ox2*LOG(q) + ox3*q + s*(ox4 + ox5*q + ox6*q**2)

!*       SOLUBILITY OF N2
!        WEISS, R. F. (1970), DEEP-SEA RESEARCH, VOL. 17, 721-735.
!              -----------------------------------------------

         ani = an0 + an1/q + an2*LOG(q) + an3*q + s*(an4 + an5*q + an6*q**2)
!
!*        21.7 K1, K2 OF CARB. ACID, KB OF BORIC ACID (EDMOND AND GIESKES,1970)
!              ----------------------------------------------------------------
         AK1 = TEN**(-CK1)
         AK2 = TEN**(-CK2)
         AKB = TEN**(-CKB)
!
!*        21.8 IONIC PRODUCT OF WATER KW (H2O) (DICKSON AND RILEY, 1979)
!              ----------------------------------------------------------------
         AKW = TEN**(-CKW)
         AKW3(j,i) = AKW
!
!*       21.9 CO2 SOLUBILITY IN SEAWATER (WEISS, 1974, CF. EQ. 12)
!              ----------------------------------------------------------------
         AK0 = EXP(CEK0)*SMICR
!
!*       21.10 DENSITY OF SEAWATER AND TOTAL BORATE IN MOLES/L

!*       21.11 SET CHEMICAL CONSTANTS
         CHEMCM(j,i,5,kplmonth) = AK0
         CHEMCM(j,i,4,kplmonth) = ak1
         CHEMCM(j,i,3,kplmonth) = ak2
         CHEMCM(j,i,1,kplmonth) = akb
         CHEMCM(j,i,2,kplmonth) = AKW
         CHEMCM(j,i,6,kplmonth) = 1.22e-5 * S    !  = BOR
!
!*       21.12 O2/N2 SOLUBILITY IN SEAWATER (WEISS, 1970)
!              ----------------------------------------------------------------
         CHEMCM(j,i,7,kplmonth) = EXP(OXY)*OXYCO
         CHEMCM(j,i,8,kplmonth) = EXP(ANI)*OXYCO
      enddo
   enddo

   do kmon = 1, 12
      do kchem = 1, 8
!          do i = iStartCalc, iEndCalc
!             do j = jStartCalc, jEndCalc
         do i = iStartD, iEndD
            do j = 1, jmax
               CHEMCM(j,i,kchem,kmon) = CHEMCM(j,i,kchem,kplmonth)
            enddo
         enddo
      enddo
   enddo
!     -----------------------------------------------------------------
!*        22. CHEMICAL CONSTANTS - DEEP OCEAN
!             ----------------------------------------------------------------
!
!*        22.1 APPROX. SEAWATER PRESSURE AT U-POINT DEPTH (BAR)
!              ----------------------------------------------------------------
   p = 1.025e-1

!    do i = iStartCalc, iEndCalc
!       do j = jStartCalc, jEndCalc
   do i = iStartD, iEndD
      do j = 1, jmax

!*        22.4 SET ABSOLUTE TEMPERATURE
!              ----------------------------------------------------------------
         t = ptho(j,i) + tmelt
         q = t*perc
         s = max(25., psao(j,i))
!
!*        22.5 CHLORINITY (WOOSTER ET AL., 1969)
!              ----------------------------------------------------------------
         CL = S*SALCHL
!
!*        22.6 LN(K0) OF SOLUBILITY OF CO2 (EQ. 12, WEISS, 1974)
!              ----------------------------------------------------------------
         cek0 = c00 + c01/q + c02*LOG(q) + s*(c03 + c04*q + c05*q**2)

!
!*        22.7 PK1, PK2 OF CARBONIC ACID, PKB OF BORIC ACID
!              ----------------------------------------------------------------
!              AFTER EDMOND AND GIESKES (1970)

         CKB = CB0/T + CB1*T + CB2 + CB3*CL**THIRD
!
         ck1 = c10/t + c11 + c12*s*LOG(t) + c13*s**2
         ck2 = c20/t + c21 + c22*s*LOG(t) + c23*s**2

!*        22.8 LN(K0) OF SOLUBILITY OF O2 (EQ. 4, WEISS, 1970)
!              ----------------------------------------------------------------
         oxy = ox0 + ox1/q + ox2*LOG(q) + ox3*q + s*(ox4 + ox5*q + ox6*q**2)

         satoxy(j,i) = exp(oxy)*oxyco
         satoxy(j,i) = satoxy(j,i)*1e6

!*        22.9 K1, K2 OF CARBONIC ACID, KB OF BORIC ACID, KW (H2O) (LIT.?)
         AK1 = TEN**(-CK1)
         AK2 = TEN**(-CK2)
         AKB = TEN**(-CKB)
!
!*       22.10 APPARENT SOLUBILITY PRODUCT K'SP OF CALCITE IN SEAWATER
!              ----------------------------------------------------------------
!              (S=27-43, T=2-25 DEG C) AT P=0 (ATMOSPH. PRESSURE)
!              (INGLE, 1800, EQ. 6)     !wk: einheit(aksp0): mol2/kg2

         aksp0 = 1.E-7 * (akcc1 + akcc2 * s**(THIRD) + akcc3*LOG10(s) + akcc4 * t**2)

         CKW = CW0/T + CW1 + CW2*SQRT(S)
         AKW3(j,i) = TEN**(-CKW)
!
!*       22.11 FORMULA FOR CP AFTER EDMOND AND GIESKES (1970)
!              ----------------------------------------------------------------
!              (REFERENCE TO CULBERSON AND PYTKOQICZ (1968) AS MADE
!              IN BROECKER ET AL. (1982) IS INCORRECT; HERE RGAS IS
!              TAKEN TENFOLD TO CORRECT FOR THE NOTATION OF P IN
!              DBAR INSTEAD OF BAR AND THE EXPRESSION FOR CP IS
!              MULTIPLIED BY LN(10.) TO ALLOW USE OF EXP-FUNCTION
!              WITH BASIS E IN THE FORMULA FOR AKSP_SED (CF. EDMOND
!              AND GIESKES (1970), P. 1285 AND P. 1286 (THE SMALL
!              FORMULA ON P. 1286 IS RIGHT AND CONSISTENT WITH THE
!              SIGN IN PARTIAL MOLAR VOLUME CHANGE AS SHOWN ON
!              P. 1285))
         CP = P/(RGAS*T)
!
!*       22.12 KB OF BORIC ACID, K1,K2 OF CARBONIC ACID PRESSURE
!              CORRECTION AFTER CULBERSON AND PYTKOWICZ (1968)
!              (CF. BROECKER ET AL., 1982)
!              ----------------------------------------------------------------
         TC = ptho(j,i)
         AKB3(j,i) = AKB*EXP(CP*(DEVKB-DEVKBT*TC))
         AK13(j,i) = AK1*EXP(CP*(DEVK1-DEVK1T*TC))
         AK23(j,i) = AK2*EXP(CP*(DEVK2-DEVK2T*TC))
!
!        22.13 APPARENT SOLUBILITY PRODUCT K'SP OF CALCITE (OR ARAGONITE)
!              ----------------------------------------------------------------
!              AS FUNCTION OF PRESSURE FOLLOWING EDMOND AND GIESKES (1970)
!              (P. 1285) AND BERNER (1976)
         AKSP_SED(j,i) = ARACAL*AKSP0*EXP(CP*(DEVKS-DEVKST*TC))   !wk: factor exp(..)~ 1.0, aracal=1

      enddo
   enddo
   return
   end subroutine CHEMCALC
!
!=======================================================================
! !INTERFACE:
   subroutine DIPOWA
!
!**** *DIPOWA* - 'diffusion of pore water'
!      vertical diffusion of sediment pore water tracers
!
!     Ernst Maier-Reimer,    *MPI-Met, HH*    10.04.01
!
!     Modified
!     --------
!     S.Legutke,        *MPI-MaD, HH*    10.04.01
!     - all npowtra-1 properties are diffused in 1 go.
!     js: not mass conserving check c13/powtra/ocetra
!
!     Purpose
!     -------
!     calculate vertical diffusion of sediment pore water properties
!     and diffusive flux through the ocean/sediment interface.
!     integration.
!
!     Method
!     -------
!     implicit formulation;
!     constant diffusion coefficient : 1.e-9 set in BODENSED.
!     diffusion coefficient : zcoefsu/zcoeflo for upper/lower
!     sediment layer boundary.
!
!**   Interface.
!     ----------
!     *CALL*       *DIPOWA*
!
!     Externals
!     ---------
!     none.
!**********************************************************************

   implicit none

   integer :: l,iv,i,j,k
   integer :: iv_oc                              ! index of ocetra in powtra loop

   real :: sedb(jmax,0:ks,npowtra)                ! ????
   real :: zcoefsu(jmax,0:ks,imax),zcoeflo(jmax,0:ks,imax) ! diffusion coefficients (upper/lower)
   real :: TREDSY(jmax,0:ks,3)                   ! redsy for 'reduced system'?
   real :: aprior,powtra_old                     ! start value of oceanic tracer in bottom layer

!ik accelerated sediment
!ik needed for boundary layer ventilation in fast sediment routine

! diffusion coefficient for bottom sediment layer
! calculate bottom ventilation rate for scaling of sediment-water exchange
   where (mask2) bolven = 1.

   do iv = 1, npowtra      ! loop over pore water tracers
!       do i = iStartCalc, iEndCalc
!          do j = jStartCalc, jEndCalc
      do i = iStartD, iEndD
         do j = 1, jmax
            if (.not. mask2(j,i)) cycle
            zcoefsu(j,0,i) = 0.0
            do  k=1,ks
               zcoefsu(j,k  ,i) = -sedict(j,k,i,iv)*seddzi(k)*porwah(j,k,i)    ! sediment diffusion coefficient * 1/dz * fraction of pore water at half depths
               zcoeflo(j,k-1,i) = -sedict(j,k,i,iv)*seddzi(k)*porwah(j,k,i)    ! the lower diffusive flux of layer k is identical to the upper diff. flux of layer k+1
            enddo
            zcoeflo(j,ks,i) = 0.0
         enddo
      enddo



!    do i = iStartCalc, iEndCalc
      do i = iStartD, iEndD
         k = 0
!       do j = jStartCalc, jEndCalc
         do j = 1, jmax
            if (.not. mask2(j,i)) cycle
            tredsy(j,k,1) = zcoefsu(j,k,i)
            tredsy(j,k,3) = zcoeflo(j,k,i)
            tredsy(j,k,2) = bolven(j,i)*bolay(j,i) - tredsy(j,k,1) - tredsy(j,k,3) ! dz(kbo) - diff upper - diff lower
         enddo

         k = 0
         iv_oc = iv
#ifdef __c_isotopes
         if (iv.eq.ipowc13) iv_oc=isco213
         if (iv.eq.ipowc14) iv_oc=isco214
#endif
!       do j = jStartCalc, jEndCalc
         do j = 1, jmax
            sedb(j, k, iv) = 0.
            if (.not. mask2(j,i)) cycle
            if (iv.eq.igasnit) then
               sedb(j,k,iv) = 0.
            else
               sedb(j,k,iv) = ocetra(j,i,iv_oc)*bolay(j,i)*bolven(j,i) !tracer_concentration(kbo) * dz(kbo)
            endif
         enddo

!          do j = jStartCalc, jEndCalc
         do j = 1, jmax
            if (.not. mask2(j,i)) cycle
            do k = 1, ks
            tredsy(j,k,1) = zcoefsu(j,k,i)
            tredsy(j,k,3) = zcoeflo(j,k,i)
            tredsy(j,k,2) = seddw(k)*porwat(j,k,i) -tredsy(j,k,1) -tredsy(j,k,3)
            enddo
         enddo

!       do j = jStartCalc, jEndCalc
         do j = 1, jmax
            if (.not. mask2(j,i)) cycle
            do k = 1,ks
               sedb(j,k,iv) = powtra(j,k,i,iv)*porwat(j,k,i)*seddw(k) ! tracer_concentration(k[1:ks]) * porewater fraction(k) * dz(k)
            enddo
         enddo

!          do j = jStartCalc, jEndCalc
         do j = 1, jmax
            if (.not. mask2(j,i)) cycle
            do k = 1, ks
               tredsy(j,k-1,1) = tredsy(j,k,1) / tredsy(j,k-1,2)    ! this overwrites tredsy(k=0) for k=1
               tredsy(j,k,2)   = tredsy(j,k,2)-tredsy(j,k-1,3)*tredsy(j,k,1) /tredsy(j,k-1,2)
            enddo
         enddo

! diffusion from above
!          do j = jStartCalc, jEndCalc
         do j = 1, jmax
            if (.not. mask2(j,i)) cycle
            do k = 1, ks
               sedb(j,k,iv) = sedb(j,k,iv)- tredsy(j,k-1,1) * sedb(j,k-1,iv)
            enddo
         enddo

! sediment bottom layer
         k = ks
!          do j = jStartCalc, jEndCalc
         do j = 1, jmax
            if (.not. mask2(j,i)) cycle
            powtra_old = powtra(j,k,i,iv)
            powtra(j,k,i,iv) = sedb(j,k,iv) / tredsy(j,k,2)
            mix_diss(j,k,i,iv) = mix_diss(j,k,i,iv) + powtra(j,k,i,iv) - powtra_old
         enddo

! sediment column
!          do j = jStartCalc, jEndCalc
         do j = 1, jmax
            if (.not. mask2(j,i)) cycle
            do k = 1, ks-1
               l = ks-k
               powtra_old = powtra(j,l,i,iv)
               powtra(j,l,i,iv) = ( sedb(j,l,iv) - tredsy(j,l,3) * powtra(j,l+1,i,iv) ) / tredsy(j,l,2)
               mix_diss(j,l,i,iv) = (mix_diss(j,l,i,iv) + powtra(j,l,i,iv) - powtra_old)
            enddo
         enddo

! sediment ocean interface
         ! caution - the following assumes same indecees for ocetra and powtra test npowa_base 071106
         ! check mo_param1_bgc.f90 for consistency
         iv_oc = iv
#ifdef __c_isotopes
         if (iv.eq.ipowc13) iv_oc=isco213
         if (iv.eq.ipowc14) iv_oc=isco214
#endif
!          do j = jStartCalc, jEndCalc
         do j = 1, jmax
            l = 0
            if (.not. mask2(j,i)) cycle
            aprior = ocetra(j,i,iv_oc)
!mk            if (iv_oc.eq.iano3)  aprior = ocetra(j,i,iano3)
!jp            if (iv.eq.igasnit .and. ocetra(j,i,igasnit).gt.1.e3) aprior = 1.e3
            ocetra(j,i,iv_oc) = ( sedb(j,l,iv) - tredsy(j,l,3) * powtra(j,l+1,i,iv) ) / tredsy(j,l,2)
            sedfluxo(j,i,iv) = sedfluxo(j,i,iv) + (ocetra(j,i,iv)-aprior)*bolay(j,i)
            sedfluxd(j,i,iv) = sedfluxd(j,i,iv) + (ocetra(j,i,iv)-aprior)*bolay(j,i)  !used in inventory_bgc/maschk (diagnostics)
            sedfluxy(j,i,iv) = sedfluxy(j,i,iv) + (ocetra(j,i,iv)-aprior)*bolay(j,i)
         enddo ! j loop
      enddo  ! i loop
   enddo    ! iv loop

   return
   end subroutine DIPOWA

!=======================================================================
!
! !INTERFACE:
   subroutine init_pore_water_tracers(ierr)
#define SUBROUTINE_NAME 'init_pore_water_tracers'
!
! !DESCRIPTION:
!
! !USES:
   implicit none
!
! !INPUT PARAMETERS:
   integer, intent(inout) :: ierr
!
! !LOCAL VARIABLES:
   integer                :: i, j, k
!
!-----------------------------------------------------------------------
#include "call-trace.inc"

!  Initial values for sediment pore water tracers and solid components

!    do i = iStartCalc, iEndCalc
!       do j = jStartCalc, jEndCalc
   do i = iStartD, iEndD
      do j = 1, jmax
         if (ixistk(j,i) /= 1) cycle
         do k = 1, ks
            powtra(j,k,i,ipowaic) = ocetra(j,i,isco212)
            powtra(j,k,i,ipowaal) = ocetra(j,i,ialkali)
            powtra(j,k,i,ipowaph) = ocetra(j,i,iphosph)
            powtra(j,k,i,ipowaox) = ocetra(j,i,ioxygen)
            powtra(j,k,i,ipown2)  = 100.
            powtra(j,k,i,ipowno3) = ocetra(j,i,iano3)
            powtra(j,k,i,ipowasi) = ocetra(j,i,isilica)
            powtra(j,k,i,ipownh4) = ocetra(j,i,ianh4)
            sedlay(j,k,i,issso12) = 1.e3
            sedlay(j,k,i,issspoc) = sedlay(j,k,i,issso12)*135.
            sedlay(j,k,i,issspon) = sedlay(j,k,i,issso12)*20.
            sedlay(j,k,i,isssc12) = 3.1e4
            sedlay(j,k,i,issssil) = 4.e4
            sedlay(j,k,i,issster) = 2.6e9
            sedhpl(j,k,i)         = hi(j,i)
            ph(j,k,i)             = -log10(sedhpl(j,k,i))
         enddo
      enddo
   enddo

   end subroutine init_pore_water_tracers

!=======================================================================
!
! !INTERFACE:
   subroutine init_aquatic_tracers(ierr)
#define SUBROUTINE_NAME 'init_aquatic_tracers'
!
! !DESCRIPTION:
!
! !USES:
   implicit none
!
! !INPUT PARAMETERS:
   integer, intent(inout) :: ierr
!
! !LOCAL VARIABLES:
   integer                :: i, j
!
!-----------------------------------------------------------------------
#include "call-trace.inc"

   ! Initial values for aquatic (advected) ocean tracers (for new start)
!    do i = iStartCalc, iEndCalc
!       do j = jStartCalc, jEndCalc
   do i = iStartD, iEndD
      do j = 1, jmax
         if (ixistk(j,i) /= 1) cycle
!          ocetra(j,i,isco212) = 2.2e3     ! [kmol/m3] wk: [mmol/m3]
!          ocetra(j,i,ialkali) = 2.365e3
!          ocetra(j,i,igasnit) = 0.0
!          ocetra(j,i,iphosph) = 0.6
!          ocetra(j,i,isilica) = 10.
!          ocetra(j,i,ioxygen) = 1.e2
!          ocetra(j,i,iano3)   = 10.
!          ocetra(j,i,ianh4)   =  2.
         ! values from eco9_warm.in for i=17,j=30, k=1
         ocetra(j,i,isco212) = 0.2186249244E+04
         ocetra(j,i,ialkali) = 0.2359499470E+04
         ocetra(j,i,igasnit) = 0.0
         ocetra(j,i,iphosph) = 0.3867889989E-01
         ocetra(j,i,isilica) = 0.4491782788E+01
         ocetra(j,i,ioxygen) = 0.2847101396E+03
         ocetra(j,i,iano3)   = 0.8953637831E+01
         ocetra(j,i,ianh4)   = 0.3867889984E+00

         hi(j,i)             = 1.e-8
         hi(j,i)             = 1.26e-8
!MK      co3(j,i)            = 150.         ! this good for initialisation -> 2.e-4? !wk: 150 mmol/m3
      enddo
   enddo

   return

   end subroutine init_aquatic_tracers

!=======================================================================
!
! !INTERFACE:
   subroutine update_aquatic_tracers(ierr)
#define SUBROUTINE_NAME 'update_aquatic_tracers'
!
! !DESCRIPTION:
!
! !USES:
   implicit none
!
! !INPUT PARAMETERS:
   integer, intent(inout) :: ierr
!
! !LOCAL VARIABLES:
   integer                :: i, j, k0
!
!-----------------------------------------------------------------------
#include "call-trace.inc"

   ! Initial values for aquatic (advected) ocean tracers (for new start)
!    do i = iStartCalc, iEndCalc
!       do j = jStartCalc, jEndCalc
   do i = iStartD, iEndD
      do j = 1, jmax
         if (ixistk(j,i) /= 1) cycle
         k0 = k_index(j,i)
#ifdef module_biogeo
         ocetra(j,i,isco212) = st(j,k0,i,idic)  !JP achtung kmol
         ocetra(j,i,ialkali) = st(j,k0,i,ialk)
         ocetra(j,i,iano3)   = st(j,k0,i,in3n)
         ocetra(j,i,ianh4)   = st(j,k0,i,in4n)
         ocetra(j,i,iphosph) = st(j,k0,i,in1p)
         ocetra(j,i,isilica) = st(j,k0,i,in5s)
         ocetra(j,i,ioxygen) = st(j,k0,i,io2o)
         ocetra(j,i,igasnit) = 0.
#else
         ocetra(j,i,isco212) = powtra(j,1,i,ipowaic)     ! [kmol/m3] wk: [mmol/m3]
         ocetra(j,i,ialkali) = powtra(j,1,i,ipowaal)
         ocetra(j,i,iano3)   = powtra(j,1,i,ipowno3)
         ocetra(j,i,ianh4)   = powtra(j,1,i,ipownh4)
         ocetra(j,i,iphosph) = powtra(j,1,i,ipowaph)
         ocetra(j,i,isilica) = powtra(j,1,i,ipowasi)
         ocetra(j,i,ioxygen) = powtra(j,1,i,ipowaox)
         ocetra(j,i,igasnit) = powtra(j,1,i,ipown2)
#endif
      enddo
   enddo

   return

   end subroutine update_aquatic_tracers

!=======================================================================
!
! !INTERFACE:
   subroutine allocate_sediment_vars(ierr)
#define SUBROUTINE_NAME 'allocate_sediment_vars'
!
! !DESCRIPTION:
!
! !USES:
   implicit none
!
! !OUTPUT PARAMETERS:
   integer, intent(inout) :: ierr
!
! !LOCAL VARIABLES:
!   integer       :: i,j,k,k0,n
!
!-------------------------------------------------------------------------
#include "call-trace.inc"

   !initialize arrays
   !GLOBAL ARRAYS (public):
   allocate(  f_poc_sed(jmax,iStartD:iEndD) ); f_poc_sed = 0.
   allocate(  f_pon_sed(jmax,iStartD:iEndD) ); f_pon_sed = 0.
   allocate(  f_pop_sed(jmax,iStartD:iEndD) ); f_pop_sed = 0.
   allocate(  f_pos_sed(jmax,iStartD:iEndD) ); f_pos_sed = 0.
   allocate(  f_pok_sed(jmax,iStartD:iEndD) ); f_pok_sed = 0.

   allocate(  f_sed_dic(jmax,iStartD:iEndD) ); f_sed_dic = 0.
   allocate(  f_sed_n4n(jmax,iStartD:iEndD) ); f_sed_n4n = 0.
   allocate(  f_sed_o3c(jmax,iStartD:iEndD) ); f_sed_o3c = 0.
   allocate(  f_sed_n1p(jmax,iStartD:iEndD) ); f_sed_n1p = 0.
   allocate(  f_sed_n5s(jmax,iStartD:iEndD) ); f_sed_n5s = 0.
   allocate(  f_sed_nn2(jmax,iStartD:iEndD) ); f_sed_nn2 = 0.
   allocate(  f_sed_n3n(jmax,iStartD:iEndD) ); f_sed_n3n = 0.
   allocate(  f_sed_alk(jmax,iStartD:iEndD) ); f_sed_alk = 0.
   allocate(  f_sed_o2o(jmax,iStartD:iEndD) ); f_sed_o2o = 0.

   !LOCAL ARRAYS (private):
   allocate(    psao(jmax,   iStartD:iEndD) ); psao      = 0.
   allocate(    ptho(jmax,   iStartD:iEndD) ); ptho      = 0.
   allocate(    tfac(jmax,   iStartD:iEndD) ); tfac      = 1.
   allocate(  prorca(jmax,   iStartD:iEndD) ); prorca    = 0.
   allocate(  prcaca(jmax,   iStartD:iEndD) ); prcaca    = 0.
   allocate(  silpro(jmax,   iStartD:iEndD) ); silpro    = 0.
   allocate( cinflux(jmax,   iStartD:iEndD) ); cinflux   = 0.
   allocate( ninflux(jmax,   iStartD:iEndD) ); ninflux   = 0.
   allocate(      hi(jmax,   iStartD:iEndD) ); hi        = 0.
   allocate(aksp_sed(jmax,   iStartD:iEndD) ); aksp_sed  = 0.
   allocate(    akw3(jmax,   iStartD:iEndD) ); akw3      = 0.
   allocate(    akb3(jmax,   iStartD:iEndD) ); akb3      = 0.
   allocate(    ak13(jmax,   iStartD:iEndD) ); ak13      = 0.
   allocate(    ak23(jmax,   iStartD:iEndD) ); ak23      = 0.
   allocate( corflux(jmax,   iStartD:iEndD) ); corflux   = 0.
   allocate( porflux(jmax,   iStartD:iEndD) ); porflux   = 0.
   allocate( norflux(jmax,   iStartD:iEndD) ); norflux   = 0.
   allocate(  produs(jmax,   iStartD:iEndD) ); produs    = 0.
   allocate( poc_sed(jmax,   iStartD:iEndD) ); poc_sed   = 0.
   allocate( pon_sed(jmax,   iStartD:iEndD) ); pon_sed   = 0.
   allocate( pop_sed(jmax,   iStartD:iEndD) ); pop_sed   = 0.
   allocate( cal_sed(jmax,   iStartD:iEndD) ); cal_sed   = 0.
   allocate( sil_sed(jmax,   iStartD:iEndD) ); sil_sed   = 0.
   allocate( ter_sed(jmax,   iStartD:iEndD) ); ter_sed   = 0.
   allocate( poc_sey(jmax,   iStartD:iEndD) ); poc_sey   = 0.
   allocate( pon_sey(jmax,   iStartD:iEndD) ); pon_sey   = 0.
   allocate( pop_sey(jmax,   iStartD:iEndD) ); pop_sey   = 0.
   allocate( cal_sey(jmax,   iStartD:iEndD) ); cal_sey   = 0.
   allocate( sil_sey(jmax,   iStartD:iEndD) ); sil_sey   = 0.
   allocate( ter_sey(jmax,   iStartD:iEndD) ); ter_sey   = 0.
!    allocate(      hi(jmax,   iStartD:iEndD) ); hi        = 0.
   allocate(  bolven(jmax,   iStartD:iEndD) ); bolven    = 1.
   allocate(   phneu(jmax,ks,iStartD:iEndD) ); phneu     = 0.
   allocate(    rcar(jmax,ks,iStartD:iEndD) ); rcar      = 0.
   allocate(    rnit(jmax,ks,iStartD:iEndD) ); rnit      = 0.
   allocate(      r0(jmax,ks,iStartD:iEndD) ); r0        = 0.
   allocate(   ro2ut(jmax,ks,iStartD:iEndD) ); ro2ut     = 0.
   allocate(  nitdem(jmax,ks,iStartD:iEndD) ); nitdem    = 0.
   allocate(  n2prod(jmax,ks,iStartD:iEndD) ); n2prod    = 0.
   allocate(  porsol(jmax,ks,iStartD:iEndD) ); porsol    = 0.
   allocate(  porwat(jmax,ks,iStartD:iEndD) ); porwat    = 0.
   allocate(  porwah(jmax,ks,iStartD:iEndD) ); porwah    = 0.
   allocate(  sedhpl(jmax,ks,iStartD:iEndD) ); sedhpl    = 0.
   allocate(pown2bud(jmax,ks,iStartD:iEndD) ); pown2bud  = 0.
   allocate(powh2obud(jmax,ks,iStartD:iEndD)); powh2obud = 0.
   allocate(      ph(jmax,ks,iStartD:iEndD) ); ph        = 0.
! nsedtra ! sediment solid tracer
   allocate(  burial(jmax,   iStartD:iEndD,nsedtra) ); burial   = 0.
   allocate(  sedsum(jmax,   iStartD:iEndD,nsedtra) ); sedsum   = 0.
   allocate(  sedlay(jmax,ks,iStartD:iEndD,nsedtra) ); sedlay   = 0.
   allocate( adv_sol(jmax,ks,iStartD:iEndD,nsedtra) ); adv_sol  = 0.
! npowtra ! sediment porewater tracer
   allocate(sedfluxo(jmax,   iStartD:iEndD,npowtra) ); sedfluxo = 0.
   allocate(sedfluxd(jmax,   iStartD:iEndD,npowtra) ); sedfluxd = 0.
   allocate(sedfluxy(jmax,   iStartD:iEndD,npowtra) ); sedfluxy = 0.
   allocate(bgcsedfl(jmax,   iStartD:iEndD,npowtra) ); bgcsedfl = 0.
   allocate(  powsum(jmax,   iStartD:iEndD,npowtra) ); powsum   = 0.
   allocate(  powtra(jmax,ks,iStartD:iEndD,npowtra) ); powtra   = 0.
   allocate(  sedict(jmax,ks,iStartD:iEndD,npowtra) ); sedict   = 0.
   allocate(mix_diss(jmax,ks,iStartD:iEndD,npowtra) ); mix_diss = 0.
   allocate(sol_diss(jmax,ks,iStartD:iEndD,npowtra) ); sol_diss = 0.
! nocetra ! ocean tracer
   allocate(  ocetra(jmax,   iStartD:iEndD,ntraad)  ); ocetra   = 0.

!    allocate(      dic2d(jmax,iStartD:iEndD) ); dic2d = 0.
!    allocate(      alk2d(jmax,iStartD:iEndD) ); alk2d = 0.
!    allocate(      n3n2d(jmax,iStartD:iEndD) ); n3n2d = 0.
!    allocate(      n4n2d(jmax,iStartD:iEndD) ); n4n2d = 0.
!    allocate(      n1p2d(jmax,iStartD:iEndD) ); n1p2d = 0.
!    allocate(      o2o2d(jmax,iStartD:iEndD) ); o2o2d = 0.
!    allocate(      n5s2d(jmax,iStartD:iEndD) ); n5s2d = 0.

   if (ierr/=0) then
      write( error_message, '("[",i2.2,"] ERROR - allocate_sediment_vars: ",i3)') myID,ierr
      call stop_ecoham(ierr, msg=error_message)
   end if

   return

   end subroutine allocate_sediment_vars

!=======================================================================
!
! !INTERFACE:
   subroutine warm_out_EMR(ierr)
#define SUBROUTINE_NAME 'warm_out_EMR'
!
! !DESCRIPTION:
!
! !USES:
   use mod_utils,  only : dayOfYear2mmdd, next_time_step, DOYstring
   implicit none
!
! !OUTPUT PARAMETERS:
   integer, intent(inout)  :: ierr
!
! !LOCAL VARIABLES:
   real          :: eps, dnext
   integer       :: iy0, im0, id0, ih0
   integer       :: i, j, k
   character(len=10) :: dumstr
!
!-------------------------------------------------------------------------
#include "call-trace.inc"

   ! initialize
   eps = 1.e-8

   iy0 = year
   ih0 = int(day_of_year-int(day_of_year))

   if (day_of_year < eps) then
      im0 = 1; id0 = 1
   else
      call dayOfYear2mmdd(int(day_of_year+eps),iy0,im0,id0)
      dnext = 24.
      call next_time_step(dnext,iy0,im0,id0,ih0)
   endif

   ! write warmstart file
   write(dumstr,'(i4.4,2("-",i2.2))') iy0, im0, id0
   if (iwarm_out==1) then
      filename = trim(runID)//'_warmstart_EMR'//trim(warmout_file_ext)
   else
      filename = trim(runID)//'_warmstart_EMR_'//trim(dumstr)//trim(warmout_file_ext)
   endif

   open(warm_unit,file=trim(filename))
   do i = 1, imax
      do j = 1, jmax
         if (ixistk_master(j,i)/=1) cycle
         do k = 1,ks
            if(phneu(j,k,i).lt.10.0) then
               phneu(j,k,i) = -log10(sedhpl(j,k,i))
            else
               phneu(j,k,i) = 0.0
            endif
            write(warm_unit,'(11e14.6)') z_sed(k), powtra(j,k,i,ipowaic),powtra(j,k,i,ipowaox), &
                                             powtra(j,k,i,ipowasi),powtra(j,k,i,ipowno3), &
                                             powtra(j,k,i,ipowaph),powtra(j,k,i,ipowaal), &
                                             powtra(j,k,i,ipown2) ,phneu (j,k,i),         &
                                             powtra(j,k,i,ipownh4)
         enddo
         do k = 1,ks
            write(warm_unit,'(11e14.6)') z_sed(k), sedlay(j,k,i,issso12),sedlay(j,k,i,isssc12), &
                                             sedlay(j,k,i,issssil),sedlay(j,k,i,issster), &
                                             sedlay(j,k,i,issspon),sedlay(j,k,i,issspoc)
         enddo
      enddo
   enddo

   close(warm_unit)

   if (ierr/=0) then
      write( error_message, '("[",i2.2,"] ERROR - warm_out_EMR: ",i3)') myID,ierr
      call stop_ecoham(ierr, msg=error_message)
   end if
   return

   end subroutine warm_out_EMR

!=======================================================================
!
! !INTERFACE:
   subroutine warm_in_EMR(ierr)
#define SUBROUTINE_NAME 'warm_in_EMR'
!
! !DESCRIPTION:
!
! !USES:
   implicit none
!
! !OUTPUT PARAMETERS:
   integer, intent(inout)  :: ierr
!
! !LOCAL VARIABLES:
   integer   :: i, j, k
#ifdef debugEMR_warmstart_extra
   integer   :: n, n1, n2
   integer   :: iw, l, imap_sed(jmax,imax)
#endif
!
!-------------------------------------------------------------------------
#include "call-trace.inc"

   filename = warmstart_file_sediment

   if (myID == master) then
      write(log_msg(1),'(" WARMSTART INITIALIZATION")')
      write(log_msg(2),'(3x,"warmstart data from file: ",a)') trim(filename)
      long_msg = trim( log_msg(1)) // new_line('a') // trim( log_msg(2))
      call write_log_message(long_msg)
      log_msg(:) = ''; long_msg = ''
   endif

   open(warm_unit,file=trim(filename),status = 'old', err = 99,form='formatted')
#ifdef convert_warmstart_EMR
   do j = 1, jmax        !lon (w->e)
     !do i = imax, 1, -1 !lat (s->n)
      do i = imax-1, 3, -1 !lat (s->n)
#else
   do i = 1, imax        !lat (n->s)
      do j = 1, jmax     !lon (w->e)
#endif
         if (ixistk_master(j,i)/=1) cycle
! Dissolved:
         do k = 1, ks
            read(warm_unit,'(11e14.6)', end=98) z_sed(k), powtra(j,k,i,ipowaic),powtra(j,k,i,ipowaox), & ! dep,  DIC, Ox
                                    powtra(j,k,i,ipowasi),powtra(j,k,i,ipowno3), & ! SiO4, NO3
                                    powtra(j,k,i,ipowaph),powtra(j,k,i,ipowaal), & ! pH  , Alk
                                    powtra(j,k,i,ipown2), phneu (j,k,i),         & ! N2  , pH
                                    powtra(j,k,i,ipownh4)                          ! NH4

            sedhpl(j,k,i) = 10.**(-phneu(j,k,i))
            ph(j,k,i) = phneu(j,k,i)
         enddo

! Solids:
         do k = 1, ks
            read(warm_unit,'(11e14.6)', end=98) z_sed(k), sedlay(j,k,i,issso12),sedlay(j,k,i,isssc12), &  ! dep, POP, Calcite
                                    sedlay(j,k,i,issssil),sedlay(j,k,i,issster), &  ! OPAL, Terigen
                                    sedlay(j,k,i,issspon),sedlay(j,k,i,issspoc)     ! PON, POC

         enddo
         !stop '#2099'
      enddo
   enddo
   98 continue
   close(warm_unit)

#ifdef convert_warmstart_EMR
   ! special mode for converting old warmstart files to the new data structure
   call warm_out_EMR(ierr)
   call stop_ecoham(1, msg='convert_warmstart - DONE!')
#endif

   if (ierr/=0) then
      write( error_message, '("[",i2.2,"] ERROR - warm_in_EMR: ",i3)') myID,ierr
      call stop_ecoham(ierr, msg=error_message)
   end if
   return

   99 continue
   ierr = 1
   call stop_ecoham(ierr, msg="WARMSTART open error : "//trim(filename))

   end subroutine warm_in_EMR

!=======================================================================
!
! !INTERFACE:
   subroutine init_sediment_grid(ierr)
#define SUBROUTINE_NAME 'init_sediment_grid'
!
! !DESCRIPTION:
!
! !USES:
   implicit none
!
! !OUTPUT PARAMETERS:
   integer, intent(inout)  :: ierr
!
! !LOCAL VARIABLES:
   real      :: dep
   integer   :: i, j, k, k0
!
!-------------------------------------------------------------------------
#include "call-trace.inc"

   allocate( mask2(jmax,iStartD:iEndD) )
   mask2(:,:) = .false.
   where ( ixistk==1 ) mask2 = .true.

   ! definition of sediment layers (dzs-distance betweeen mid points of layers):
   dzs( 1) = 0.001
   dzs( 2) = 0.003
   dzs( 3) = 0.005
   dzs( 4) = 0.007
   dzs( 5) = 0.009
   dzs( 6) = 0.011
   dzs( 7) = 0.013
   dzs( 8) = 0.015
   dzs( 9) = 0.017
   dzs(10) = 0.019
   dzs(11) = 0.021
   dzs(12) = 0.023
   dzs(13) = 0.025

   ! define thickness of sediment layers dzzs
   do k = 1,ks
      dzzs(k) = 0.5*(dzs(k)+dzs(k+1))
   enddo

   ! depth of sediment layers for output in bgcmean
   z_sed(1) = -dzs(1)
   do k = 2, ks
      z_sed(k) = z_sed(k-1) - dzs(k)
   enddo

   ! propperties of pelagic bottom layer
   !allocate(    dz2(jmax, iStartD:iEndD) ) ! substituted by bolay
   !allocate(   dep2(jmax, iStartD:iEndD) ) ! substituted by ptiestu
   allocate( bolay  (jmax, iStartD:iEndD) )
   !allocate( ptiestu(jmax, iStartD:iEndD) )
!    do i = iStartCalc, iEndCalc
!       do j = jStartCalc, jEndCalc
   do i = iStartD, iEndD
      do j = 1, jmax
         k0 = k_index(j,i)
         dep = 0.
         do k = 1,k0
            dep = dep + dz(j,k,i)
            if (k==k0) then
               !dz2( j,i)   = dz(j,k,i)            ! layer thickness (m) of pelagic bottom layer
               !dep2(j,i)   = dep - 0.5*dz(j,k,i)  ! depth (m) of deepest zeta in pelagic bottom layer
               bolay(j,i)   = dz(j,k,i)            ! layer thickness (m) of pelagic bottom layer
               !ptiestu(j,i) = dep - 0.5*dz(j,k,i)  ! depth (m) of deepest zeta in pelagic bottom layer
            endif
         enddo
      enddo
   enddo

   if (ierr/=0) then
      write( error_message, '("[",i2.2,"] ERROR - init_sediment_grid: ",i3)') myID,ierr
      call stop_ecoham(ierr, msg=error_message)
   end if
   return

   end subroutine init_sediment_grid

!=======================================================================
!
! !INTERFACE:
   subroutine init_sediment_porosity(ierr)
#define SUBROUTINE_NAME 'init_sediment_porosity'
!
! !DESCRIPTION:
!
! !USES:
   implicit none
!
! !OUTPUT PARAMETERS:
   integer, intent(inout)  :: ierr
!
! !LOCAL VARIABLES:
   real, dimension (jmax,imax)   :: poro_field ! to be defined on global domain
   real      :: sedac
   integer   :: isac
   integer   :: i, j, k
!    integer, dimension (jmax,imax)   :: imask ! to be defined on global domain
!
!-------------------------------------------------------------------------
#include "call-trace.inc"

! ******************************************************************
! the following section was to include the SEDIMENT ACCELERATION it is off
! mechanism:
!switch for diffusion acceleration (isac):
    isac  = 1
    sedac = 1. / real(isac)

! determine total solid sediment thickness sumsed
! and 'reduced' sediment volume, i.e. volume of solid part (solfu)
!    sumsed = 0.
!    do k = 1, ks
!        porwat(k) = porwat(k) * sedac
!        porsol(k) = porsol(k) * sedac
!      sumsed = sumsed + seddw(k)
!    enddo
! ******************************************************************


   ! read sediment porosity from file
   filename = trim(input_dir)//'porosity.dat'
   open(asc_unit, file=trim(filename), status='old', form='formatted')
   do i=1,imax
      read(asc_unit,*)(poro_field(j,i),j=1,jmax)
   enddo
   close(asc_unit)

   ! comment what's done !!!
   if(isw_dim == 1)then  !1D
      i = iStartCalc
      j = jStartCalc
      porwat(j,1,i) = poro_field(j,i)
      porsol(j,1,i) = 1. - porwat(j,1,i)
      porwat(j,1,i) = porwat(j,1,i)*sedac
      porsol(j,1,i) = porsol(j,1,i)*sedac
      seddw(1) = 0.5 * (dzs(1) + dzs(2))

      do k = 2, ks
         seddw(k) = 0.5 * (dzs(k) + dzs(k+1))
         porwat(j,k,i) = porwat(j,1,i)*exp(2.12268*z_sed(k))
         porsol(j,k,i) = 1. - porwat(j,k,i)
      enddo
   else  !3D
!       do i = iStartCalc, iEndCalc
!          do j = jStartCalc, jEndCalc
      do i = iStartD, iEndD
         do j = 1, jmax
            if (.not. mask2(j,i)) cycle
            porwat(j,1,i) = poro_field(j,i)
            porsol(j,1,i) = 1. - porwat(j,1,i)
            porwat(j,1,i) = porwat(j,1,i)*sedac
            porsol(j,1,i) = porsol(j,1,i)*sedac
            seddw(1) = 0.5 * (dzs(1) + dzs(2))
            do k = 2, ks
               ! define sediment layer thicknesses (seddw [m]) and porosities:
               seddw(k) = 0.5 * (dzs(k) + dzs(k+1))
               porwat(j,k,i)=porwat(j,1,i)*exp(2.12268*z_sed(k))        !vertical gradient derived from original MPI model
               porsol(j,k,i) = 1. - porwat(j,k,i)
               porwat(j,k,i) = porwat(j,k,i)*sedac
               porsol(j,k,i) = porsol(j,k,i)*sedac
            enddo
         enddo
      enddo
   endif  ! if(isw_dim == 1)

! determine total solid sediment volume:
   solfu = 0.
!    do i = iStartCalc, iEndCalc
!       do j = jStartCalc, jEndCalc
   do i = iStartD, iEndD
      do j = 1, jmax
         if (.not. mask2(j,i)) cycle
         do k = 1, ks
            solfu = solfu + seddw(k)*porsol(j,k,i)
            !powfu = powfu + seddw(k)*porwat(j,k,i)
            if (k.eq.1) porwah(j,k,i) = 0.5 * (1. + porwat(j,1,i))     !for the diffusion in dipowa.f90
            if (k.ge.2) porwah(j,k,i) = 0.5 * (porwat(j,k,i) + porwat(j,k-1,i))
            seddzi(k+1) = 1. / dzs(k+1)                         ! inverse
         enddo
      enddo
   enddo

   if (ierr/=0) then
      write( error_message, '("[",i2.2,"] ERROR - init_sediment_grid: ",i3)') myID,ierr
      call stop_ecoham(ierr, msg=error_message)
   end if
   return

   end subroutine init_sediment_porosity

!=======================================================================
!
! !INTERFACE:
   subroutine update_Paulmier_coefficients(ierr)
#define SUBROUTINE_NAME 'update_Paulmier_coefficients'
!
! !DESCRIPTION:
! updates stoichiometric coefficients: rcar, rnit, ro2ut, nitdem, n2prod
! (see Paulmier et al., 2009)
!
! !USES:
   implicit none
!
! !OUTPUT PARAMETERS:
   integer, intent(inout)  :: ierr
!
! !LOCAL VARIABLES:
    real, dimension(jmax,ks,iStartD:iEndD) :: b, c
    integer   :: i, j, k
!
!-------------------------------------------------------------------------
#include "call-trace.inc"

!    do i = iStartCalc, iEndCalc
!       do j = jStartCalc, jEndCalc
   do i = iStartD, iEndD
      do j = 1, jmax
         if (.not. mask2(j,i)) cycle
         !tfac(j,i) = exp( alog(1.5)*((ptho(j,i)-10.)/10.) )  !mk found "alog(1.5)" as well
         tfac(j,i) = exp( alog(1.2)*((ptho(j,i)-10.)/10.) )  !jp ptho pot temp; is it already defined for ifirst=1?

         ! calculation of stoichiometric factors according to Paulmier et al. (2009)
         do k = 1, ks
            rcar(j,k,i) = sedlay(j,k,i,issspoc)/sedlay(j,k,i,issso12) !C/P ratio in the solid part
            rnit(j,k,i) = sedlay(j,k,i,issspon)/sedlay(j,k,i,issso12) !N/P ratio in the solid part
            b(j,k,i)  = 72. + 2.*rcar(j,k,i) + 3.*rnit(j,k,i) + 3.
            c(j,k,i)  = rcar(j,k,i) + 4.
            r0(j,k,i) = rcar(j,k,i) + 1./4.*b(j,k,i) - 1./2.*c(j,k,i) - 3./4.*rnit(j,k,i) + 5./4.
            ro2ut(j,k,i) = r0(j,k,i) + 2.*rnit(j,k,i)                 ! -O2/P ratio aerobic remineralisation
            nitdem(j,k,i) = 4./5.*r0(j,k,i) + 3./5.*rnit(j,k,i)       !-NO3/P ratio anaerobic remineralisation
            n2prod(j,k,i) = nitdem(j,k,i) + rnit(j,k,i)               ! N2/P  ratio denitrification
         enddo
      enddo
   enddo

   if (ierr/=0) then
      write( error_message, '("[",i2.2,"] ERROR - update_Paulmier_coefficients: ",i3)') myID,ierr
      call stop_ecoham(ierr, msg=error_message)
   end if
   return

   end subroutine update_Paulmier_coefficients

!=======================================================================
#endif /*#ifdef module_sediment*/
   end module mod_sediment

!=======================================================================
#endif /*#ifdef sediment_EMR*/