!--------------------------------
!
! !MODULE: eco_output.f90 ---  subroutines for ecoham-output
!
! !INTERFACE:
   MODULE mod_output
!
! !DESCRIPTION:
!
! !USES:
   use mod_common
   use mod_output_var
   use mod_output_1D
   use mod_output_3D
#if defined module_sediment && defined sediment_EMR
   use mod_output_EMR
#endif
   implicit none
!
!  default: all is private.
   private
! !PUBLIC MEMBER FUNCTIONS:
   public init_output, do_output, warm_out, warm_in, close_output
   public n_pos_out, icol_out, jcol_out
   public n_str2D_out, n_str3D_out
!
! !LOCAL VARIABLES:
!
!=======================================================================

   contains

!=======================================================================
!
! !INTERFACE:
   subroutine init_output(ierr)
!
! !DESCRIPTION:
!
! !USES:
#ifdef module_sections
   use mod_sections, only: l_sections, init_sections_output
#endif
   implicit none
!
! !OUTPUT PARAMETERS:
   integer, intent(inout)  :: ierr
!
! !LOCAL VARIABLES:
!
!-----------------------------------------------------------------------

   ! set varnames
   call set_ot_varnames(ierr)

   ! allocate arrays for 2D extra output (othervars-2D)
   allocate( ot2D(jmax,     iStartD:iEndD,n_ot2D_var))
   ot2D = fail
   allocate( ot2D_out(n_ot2D_var) )
   ot2D_out = .false.

   ! allocate arrays for 3D extra output (othervars-3D)
   allocate( ot3D(jmax,kmax,iStartD:iEndD,n_ot3D_var))
   ot3D = fail
   allocate( ot3D_out(n_ot3D_var) )
   ot3D_out = .false.


#ifdef module_sections
   call init_sections_output(ierr) ! init section output
#endif

   !1D-output
   call init_output_1D(ierr)

   !3D-output
   if (isw_dim==3) call init_output_3D(ierr)

#if defined module_sediment && defined sediment_EMR
   call init_output_EMR(ierr)
#endif

   if (ierr/=0) then
      write( error_message, '("[",i2.2,"] ERROR - init_output: ",i3)') myID,ierr
      call stop_ecoham(ierr, msg=error_message)
   end if
   return

   end subroutine init_output

!=======================================================================
! !INTERFACE:
   subroutine do_output(ierr)
!
! !DESCRIPTION:
!
! !USES:
#ifdef module_sections
   use mod_sections, only: l_sections, do_sections_output
#endif
   implicit none
!
! !OUTPUT PARAMETERS:
   integer, intent(inout)  :: ierr
!
! !LOCAL VARIABLES:
!
!-----------------------------------------------------------------------

   ! update variables in "ot"-array
   call update_ot_var(ierr)

   ! write prt-files
   call do_output_1D(ierr)

#ifdef module_sections
   if (l_sections) call do_sections_output(ierr) ! write section output
#endif

   ! write 3D output
   if(isw_dim==3) then
      call do_output_3D(ierr)
      !print*,'do_output_3D'
   endif

#if defined module_sediment && defined sediment_EMR
!print*,'eco_output#129  do_output_EMR'
   call do_output_EMR(ierr)
#endif

   ! re-write warmstart every day
   if(iwarm_out>0)then
      call warm_out(ierr)
   endif

   if (ierr/=0) then
      write( error_message, '("[",i2.2,"] ERROR - do_output: ",i3)') myID,ierr
      call stop_ecoham(ierr, msg=error_message)
   end if
   return

   end subroutine do_output

!=======================================================================
!
! !INTERFACE:
   subroutine warm_out(ierr)
#define SUBROUTINE_NAME 'warm_out'
!
! !DESCRIPTION:
!
! !USES:
   use mod_var
!   use mod_var, only : st, st_name, n_st_var
!   use mod_var, only : sed, sed_name, n_sed_var
   use mod_utils,  only : dayOfYear2mmdd, next_time_step, DOYstring
#ifdef module_chemie
   use mod_chemie, only : ch, ch_name, n_ch_var
#endif
#ifdef module_meteo
   use mod_meteo,  only : Iopt
#endif
  implicit none
!
! !INPUT PARAMETERS:
   integer, intent(inout) :: ierr
!
! !OUTPUT PARAMETERS:
!
! !LOCAL VARIABLES:
   character(len=6), allocatable, dimension(:) :: warm3D_name
   real, allocatable, dimension(:,:,:,:)       :: warm3D
   real, allocatable, dimension(:,:,:)         :: warm2D
   real, allocatable, dimension(:,:)           :: buf
   real          :: eps, dnext
   integer       :: n_dim_warm3D, n_dim_warm2D
   integer       :: n_st_warm, n_ch_warm, n_opt_warm, n_sed_warm
   integer       :: iy0, im0, id0, ih0
   integer       :: i, j, k, k0, ic, ic1, n, n_dum
   logical       :: bin_out  = .false.
   character(len=10) :: dumstr
!
!-------------------------------------------------------------------------
#include "call-trace.inc"

   ! initialize
   eps = 1.e-8
   n_st_warm  = 0
   n_ch_warm  = 0
   n_opt_warm = 0
   n_sed_warm = 0
   k0 = 0

   iy0 = year
   ih0 = int(day_of_year-int(day_of_year))

   if (day_of_year < eps) then
      im0 = 1; id0 = 1
   else
      call dayOfYear2mmdd(int(day_of_year+eps),iy0,im0,id0)
      dnext = 24.
      call next_time_step(dnext,iy0,im0,id0,ih0)
   endif

   write(dumstr,'(i4.4,2("-",i2.2))') iy0, im0, id0
   if (iwarm_out==1) then
      filename = trim(runID)//'_warmstart'//trim(warmout_file_ext)
   else
      filename = trim(runID)//'_warmstart_'//trim(dumstr)//trim(warmout_file_ext)
   endif

   ! estimate number of parameters in warm3D
   n_st_warm    = n_st_var
   n_dim_warm3D = n_st_warm
#ifdef module_chemie
   n_ch_warm    = n_ch_var
   n_dim_warm3D = n_dim_warm3D + n_ch_warm
#endif
#ifdef module_meteo
   n_opt_warm   = 1 ! iOpt
   n_dim_warm3D = n_dim_warm3D + n_opt_warm
#endif

   if (index(warmout_file_ext,'.bin') /= 0) then
      if (isw_dim == 1) then
         warmout_file_ext = '.asc'
         filename(index(filename,'.bin'):) = trim(warmout_file_ext)
         if (myID == master) then
            call write_log_message('warm_out: file name changed to asc because of isw_dim == 1!')
         endif
      else
         bin_out = .true.
      endif
   endif

   select case (isw_dim) !jp
      case (3) !3D-mode

         if (myID == master) then
            allocate( warm3D_name(n_dim_warm3D) )
            warm3D_name(:) = ''
         endif

         allocate( warm3D(jmax,kmax,imax,n_dim_warm3D) )
         warm3D = fail

         ! collect data
         do n = 1, n_st_var
            warm3D(:,:,iStartD:iEndD,n) = st(:,:,iStartD:iEndD,n)
            if (myID == master) warm3D_name(n) = st_name(n)
         enddo
         n_dum = n_st_var
#ifdef module_chemie
         do n = 1, n_ch_var
            n_dum = n_dum +1
            warm3D(:,:,iStartD:iEndD,n_dum) = ch(:,:,iStartD:iEndD,n)
            if (myID == master) warm3D_name(n_dum) = ch_name(n)
         enddo
#endif
#ifdef module_meteo
         n_opt_warm = 1
         n_dum = n_dum +1
         warm3D(:,:,iStartD:iEndD,n_dum) = Iopt(:,:,iStartD:iEndD)
         if (myID == master) warm3D_name(n_dum)  = 'Iopt'
#endif
! add check whether n_dum matches n_dim_warm3D

#ifdef module_sediment
         n_sed_warm   = n_sed_var
         n_dim_warm2D = n_sed_warm
         allocate( warm2D(jmax,imax,n_dim_warm2D) )
         warm2D = fail
         warm2D(:,iStartD:iEndD,:) = sd(:,iStartD:iEndD,:)
#else
         n_dim_warm2D = 0
#endif
! add check whether n_dum matches n_dim_warm2D

#ifdef MPI
         ! mpi_gather warm2D / warm3D
         do n = 1, n_dim_warm2D
            call mpi_parallel_gather(warm2D(:,:,n),1,ierr_mpi)
            ierr = ierr + ierr_mpi
         enddo

         do n = 1, n_dim_warm3D
            call mpi_parallel_gather(warm3D(:,:,:,n),kmax,ierr_mpi)
            ierr = ierr + ierr_mpi
         enddo

         ! check whether gather was fine
         if (ierr/=0) then
            write(error_message,'("ERROR - warm_out: gathering data from domains failed")')
            call stop_ecoham(ierr, error_message);
         endif
#endif

         if (myID == master) then

            if (bin_out) then  ! binary output

               allocate (buf(iiwet,max(n_dim_warm3D,n_dim_warm2D)) )
               open (warm_unit, file=trim(filename), form='unformatted')
               write(warm_unit) iy0, im0, id0, ih0, iiwet, n_sed_warm, n_st_warm, n_ch_warm, n_opt_warm
               write(warm_unit) warm3D_name
               if (n_dim_warm3D > 0) then
                  do n = 1, n_dim_warm3D
                     buf(:,n) = pack(warm3D(:,:,:,n),ixistz_master==1)
                  enddo
                  write(warm_unit) buf(:,1:n_dim_warm3D)
               endif
               if (n_dim_warm2D > 0) then
                  do n = 1, n_dim_warm2D
                     buf(1:iiwet2,n) = pack(warm2D(:,:,n),ixistk_master==1)
                  enddo
                  write(warm_unit) buf(1:iiwet2,1:n_dim_warm2D)
               endif
               deallocate (buf)
               close(warm_unit)

            else  ! ascii output
               open (warm_unit, file=trim(filename), form='formatted')
               write(warm_unit, '(4i4,i8,6i4)') iy0, im0, id0, ih0, iiwet,  &
                                             n_sed_warm, n_st_warm, n_ch_warm, n_opt_warm
               write(warm_unit, '(4x,a2,3(5x,a1),50(a16,4x))') 'ic','i','j','k',(warm3D_name(n),n=1,n_dum)

               ic = 0
               do i = 1, imax
                  do j = 1, jmax
                     k0 = k_index_master(j,i)
                     if (k0 .ge. 1)then
                        do k = 1, k0
                           ic = ic + 1
                           write(warm_unit,'(4i6,50(e20.10))') ic, i, j, k, (warm3D(j,k,i,n),n=1,n_dum)
                        enddo
                     endif
                  enddo
               enddo
#ifdef module_sediment
               do i = 1, imax
                  do j = 1, jmax
                     write(warm_unit,'(10(e20.10))') (warm2D(j,i,n),n=1,n_dim_warm2D)
                  enddo
               enddo
#endif
               close(warm_unit)
            endif  ! bin_out
            deallocate(warm3D_name)
         endif ! master
         deallocate(warm3D)

      case (1) !1D-mode
         i = iStartCalc
         j = jStartCalc
         k0 = k_index(j,i)

         ! write warmstart only from process covering latIndex
         if ( i >= iStartCalc .and. i <= iEndCalc ) then
            open(warm_unit, file=trim(filename), status='replace')

            allocate( warm3D_name(n_dim_warm3D))
            warm3D_name(:) = ''

            allocate( warm3D(1,1:k0,1,n_dim_warm3D))
            warm3D = fail

            do n = 1, n_st_var
               do k = 1, k0
                  warm3D(1,k,1,n) = st(j,k,i,n)
               enddo
               warm3D_name(n) = st_name(n)
            enddo
            n_dum = n_st_var
#ifdef module_chemie
            do n = 1, n_ch_var
               n_dum = n_dum +1
               do k = 1, k0
                  warm3D(1,k,1,n_dum) = ch(j,k,i,n)
               enddo
               warm3D_name(n_dum) = ch_name(n)
            enddo
#endif
#ifdef module_meteo
            n_opt_warm = 1
            n_dum = n_dum +1
            do k = 1, k0
               warm3D(1,k,1,n_dum) = Iopt(j,k,i)
            enddo
            warm3D_name(n_dum) = 'Iopt'
#endif
            ! write header
            write(warm_unit,'(4i4,i8,6i4)') iy0, im0, id0, ih0, 1, n_sed_var, n_st_warm, n_ch_warm, n_opt_warm
            write(warm_unit,'(4x,a2,3(5x,a1),50(a16,4x))') 'ic','i','j','k',(warm3D_name(n),n=1,n_dum)

            ic  = 0
            ic1 = 0
            do i = 1, imax
               do j = 1, jmax
                  if (i==iStartCalc .and. j==jStartCalc) ic1 = ic
                  k0 = k_index(j,i)
                  if (k0 .ge. 1)then
                     do k = 1, k0
                        ic = ic + 1
                     enddo
                  endif
               enddo
            enddo
            i = iStartCalc
            j = jStartCalc
            k0 = k_index(j,i)
            ic = ic1
            if (k0 .ge. 1) then
               do k = 1, k0
                  ic = ic + 1
                  write(warm_unit,'(4i6,50(e20.10))') ic, i, j, k, (warm3D(1,k,1,n),n=1,n_dum)
               enddo
            endif
#ifdef module_sediment
            write(warm_unit,'(10(e20.10))') (sd(j,i,n),n=1,n_sed_var)
#endif
            close(warm_unit)

            deallocate(warm3D_name)
            deallocate(warm3D)
         endif

   end select

   if (myID == master) then
      write(long_msg,'(" WARMSTART data for day ",a," written to ",a)') &
                        DOYstring(day_of_year),trim(filename)
      call write_log_message(long_msg); long_msg = ''
   endif

   if (ierr/=0) then
      write( error_message, '("[",i2.2,"] ERROR - warm_out: ",i3)') myID,ierr
      call stop_ecoham(ierr, msg=error_message)
   end if
   return

   end subroutine warm_out

!=======================================================================
!
! !INTERFACE:
   subroutine warm_in(ierr)
#define SUBROUTINE_NAME 'warm_in'
!
! !DESCRIPTION:
!
! !USES:
   use mod_var,      only : st, n_st_var
#ifdef convert_warmstart
   use mod_hydro,    only : rho, temp, salt
#endif
#ifdef module_sediment
   use mod_sediment, only : sd, n_sed_var
#endif
#ifdef module_chemie
   use mod_chemie,   only : ch, n_ch_var
#endif
#if defined module_biogeo && defined module_meteo
   use mod_meteo,    only : Iopt
#endif
  implicit none
!
! !INPUT PARAMETERS:
   integer, intent(inout) :: ierr
!
! !OUTPUT PARAMETERS:
!
! !LOCAL VARIABLES:
   real, allocatable, dimension(:,:,:,:) :: warm3D
#ifdef module_sediment
   real, allocatable, dimension(:,:,:)   :: warm2D
   integer       :: n_dim_warm2D
#endif
   real, allocatable, dimension(:)       :: warm0D
   real, allocatable, dimension(:,:)     :: buf
   integer       :: iwet_warm, n_dim_warm3D
   integer       :: n_st_warm, n_ch_warm, n_opt_warm, n_sed_warm
   integer       :: iy0, im0, id0, ih0
   integer       :: i, j, k, k0 ,ii, jj, kk, ic, ic3, ic2, ic1, n
#if defined module_biogeo || defined module_chemie
   integer       :: nS, nE
#endif
   logical       :: bin_in
   !logical       :: startdate_from_warmstartfile = .true.
   !character(len=19) :: time_str
   character(len=99) :: warm_fmt
!
!-------------------------------------------------------------------------
#include "call-trace.inc"

   n_st_warm  = 0 ! to get rid of uninitialized warning
   n_ch_warm  = 0 ! to get rid of uninitialized warning
   n_opt_warm = 0 ! to get rid of uninitialized warning
   n_sed_warm = 0 ! to get rid of uninitialized warning
   n_sed_warm = 0 ! to get rid of uninitialized warning

   filename = warmstart_file
   if (index(filename,'.bin') /= 0 .and. isw_dim == 1) filename(index(filename,'.bin'):) = '.asc'
   bin_in = .false.
   if (index(filename,'.bin') /= 0) bin_in = .true.

   if (myID == master) then
      write(log_msg(1),'(" WARMSTART INITIALIZATION")')
      write(log_msg(2),'(3x,"warmstart data from file: ",a)') trim(filename)
      long_msg = trim( log_msg(1)) // new_line('a') // trim( log_msg(2))
      call write_log_message(long_msg)
      log_msg(:) = ''; long_msg = ''
   endif

   if (bin_in) then
      open(warm_unit, file=trim(filename), status='old', err=99, form='unformatted')
      read(warm_unit) iy0,im0,id0,ih0,iwet_warm, n_sed_warm, n_st_warm, n_ch_warm, n_opt_warm
      read(warm_unit)
   else
      open(warm_unit, file=trim(filename), status='old', err=99, form='formatted')
      read(warm_unit,*) iy0,im0,id0,ih0,iwet_warm, n_sed_warm, n_st_warm, n_ch_warm, n_opt_warm
      read(warm_unit,*)
   endif

   if (myID == master) then
   if (isw_dim==3 .and. iwet_warm.ne.iiwet) then
      ierr = 1
      write(log_msg(1),'("ERROR: you cannot run the 3D mode with this warmstart file")')
      write(log_msg(2),'(7x,a,i8,x,a,i8)') 'your setup requires iiwet=', iiwet, &
                                             'but warmstart provides iwet_warm=', iwet_warm
      long_msg = trim( log_msg(1)) // new_line('a') // trim( log_msg(2))
      call write_log_message(long_msg); long_msg = ''
      call stop_ecoham(ierr, msg=long_msg); long_msg = ''
   endif

   if (n_sed_var>0 .and. n_sed_warm/=n_sed_var) then
      if (n_sed_warm==0) then
         ierr = 1
         write(error_message,'("ERROR: you cannot warmstart without sediment definitions")')
         call stop_ecoham(ierr, msg=error_message)
      else
         write(log_msg(1),'("##########################################################################")')
         write(log_msg(2),'("#  WARNING: n_sed_var does not match n_sed_warm found in warmstart file  #")')
         write(log_msg(3),'("##########################################################################")')
         long_msg = trim(log_msg(1))//new_line('a')//trim(log_msg(2))//new_line('a')//trim(log_msg(3))
         call write_log_message(long_msg); long_msg = ''
      endif
   endif

   if (n_st_var>0 .and. n_st_warm/=n_st_var) then
      if (n_st_warm==0) then
         ierr = 1
         write(error_message,'("ERROR: you cannot warmstart without biological definitions")')
         call stop_ecoham(ierr, msg=error_message)
      else
         write(log_msg(1),'("##########################################################################")')
         write(log_msg(2),'("#  WARNING: n_st_var does not match n_st_warm found in warmstart file    #")')
         write(log_msg(3),'("##########################################################################")')
         long_msg = trim(log_msg(1))//new_line('a')//trim(log_msg(2))//new_line('a')//trim(log_msg(3))
         call write_log_message(long_msg); long_msg = ''
      endif
   endif

#ifdef module_chemie
   if (n_ch_var>0 .and. n_ch_warm/=n_ch_var) then
      if (n_ch_warm==0) then
         ierr = 1
         write(error_message,'("ERROR: you cannot warmstart without chemical definitions")')
         call stop_ecoham(ierr, msg=error_message)
      elseif (n_ch_warm/=n_ch_var) then
         write(log_msg(1),'("##########################################################################")')
         write(log_msg(2),'("#  WARNING: n_ch_var does not match n_ch_warm found in warmstart file    #")')
         write(log_msg(3),'("##########################################################################")')
         long_msg = trim(log_msg(1))//new_line('a')//trim(log_msg(2))//new_line('a')//trim(log_msg(3))
         call write_log_message(long_msg); long_msg = ''
      endif
   endif
#endif

   if (n_opt_warm==0) then
      ierr = 1
      write(error_message,'("ERROR: you cannot warmstart without optional definitions")')
      call stop_ecoham(ierr, msg=error_message)
   endif

   if (iy1/=iy0 .or. im1/=im0 .or. id1/=id0 .or. ih1/=ih0) then
      write(log_msg(1),'("###########################################################################")')
      write(log_msg(2),'("#  WARNING: time read from warmstart differ from starttime in eco_set.nml #")')
      write(log_msg(3),'("###########################################################################")')
      long_msg = trim(log_msg(1))//new_line('a')//trim(log_msg(2))//new_line('a')//trim(log_msg(3))
      call write_log_message(long_msg); long_msg = ''
   endif
   endif

!   if (startdate_from_warmstartfile) then
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! NOT READY YET !!! -> MK: DEVELOPMENT REJECTED
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!       iy1 = iy0
!       im1 = im0
!       id1 = id0
!       ih1 = ih0
!       write(time_str,'(i4.4,a1,i2.2,a1,i2.2,x,i2.2,a3)') iy1,'-',im1,'-',id1,ih1,':00'
!       write(log_msg(1),'("##########################################################################")')
!       write(log_msg(2),'("#  INFO: starttime reset from warmstart file entry: ",a19,"  #")') time_str
!       write(log_msg(3),'("##########################################################################")')
!       long_msg = trim(log_msg(1))//new_line('a')//trim(log_msg(2))//new_line('a')//trim(log_msg(3))
!       call write_log_message(long_msg); long_msg = ''
!
!       call idays_of_month(iy1,im1,iday_start,id1)
!       call idays_of_month(iy2,im2,iday_end,  id2)
!       rday_start = real(iday_start -1) + max(0,min(24,ih1))/24.
!       rday_end   = real(iday_end   -1) + max(0,min(24,ih2))/24.
!       year = iy1
!
!       ! calculate number of iterations needed
!       !iterations to complete start day
!       EndOfIteration = ceiling( (iday_start-rday_start) / (dt_main/secs_per_day) )
! !print*,iday_start,rday_start,EndOfIteration
!       !iterations for full days
!       EndOfIteration = EndOfIteration + int( (int(rday_end)-iday_start)/ (dt_main/secs_per_day) )
! !print*,iday_end,iday_start, int( (int(rday_end)-iday_start)/ (dt_main/secs_per_day) ),EndOfIteration
!       !iterations to complete last day
!       EndOfIteration = EndOfIteration + ceiling( (iday_end-rday_end) / (dt_main/secs_per_day) )
! !print*,iday_end,rday_end,ceiling( (iday_end-rday_end) / (dt_main/secs_per_day) ),EndOfIteration
! !call stop_ecoham(1, msg='eco_output#597')
!   endif

   n_dim_warm3D = n_st_warm + n_ch_warm + n_opt_warm
   write(warm_fmt,'("(4i6,",i3,"(e20.10))")') n_dim_warm3D
   allocate ( warm3D(jmax,kmax,imax,n_dim_warm3D) )
   warm3D = fail

   allocate( warm0D(n_dim_warm3D))
   warm0D = fail

   if (iwet_warm==iiwet) then ! read 3D-warmstart file
      if (bin_in) then
         allocate ( buf(iiwet,n_dim_warm3D) )
         read (warm_unit) buf(:,1:n_dim_warm3D)
         do n = 1,n_dim_warm3D
            warm3D(:,:,:,n) = unpack(buf(:,n), ixistz_master==1, fail)
         enddo
         deallocate(buf)
      else
         do ic3 = 1, iwet_warm
            read(warm_unit,warm_fmt) ic,ii,jj,kk,(warm0D(n),n=1,n_dim_warm3D)
            ! Hier könnte noch ein check hin, der die Übereinstimmung der Indizes prüft!
            warm3D(jj,kk,ii,:) = warm0D(:)
         enddo
      endif
   else
      if (bin_in .and. isw_dim == 1) then
         warmout_file_ext = '.asc'
         filename(index(filename,'.bin'):) = trim(warmout_file_ext)
         if (myID == master) then
            call write_log_message('warm_in: file name changed to asc because of isw_dim == 1!')
         endif
      endif

      ! read first line of data
      read(warm_unit,warm_fmt) ic,ii,jj,kk,(warm0D(n),n=1,n_dim_warm3D)

      ! check position
      ic1 = 0
      ic2 = 0
      do i = 1, imax
         do j = 1, jmax
            k0 = k_index_master(j,i)
            if (k0 .ge. 1)then
               do k = 1, k0
                  ic1 = ic1 + 1
                  if (i==ii .and. j==jj .and. k==1) ic2 = ic1
               enddo
            endif
         enddo
      enddo
      if (ic.ne.ic2) then
         if (myID == master) then
            ierr = 1
            write(error_message,'(a)') 'ERROR: grid indeces from 1D warmstart'  //  &
                        'does not match position indices specified in eco_set.nml'
            call stop_ecoham(ierr, msg=error_message)
         endif
      endif
      k0 = k_index_master(jj,ii)
      if (k0 .ge. 1)then
         warm3D(jj,kk,ii,:) = warm0D(:)
         kk = kk+1
         do k = kk, k0
            read(warm_unit,warm_fmt) ic,ii,jj,kk,(warm3D(jj,k,ii,n),n=1,n_dim_warm3D)
         enddo
      endif
   endif

#ifdef module_biogeo
   if (n_st_warm  > 0) st(:,:,iStartD:iEndD,1:n_st_warm) = warm3D(:,:,iStartD:iEndD,1:n_st_warm)
#ifdef module_meteo
   nE = n_st_warm+n_ch_warm+1
   if (n_opt_warm > 0) iopt(:,:,iStartD:iEndD) = warm3D(:,:,iStartD:iEndD,nE)
#endif
#ifdef exclude_p1x
   where(ixistZ==1) st(:,:,:,ip1c)   = 1.
   where(ixistZ==1) st(:,:,:,ip1n)   = 1.
   where(ixistZ==1) st(:,:,:,ip1p)   = 1.
   where(ixistZ==1) st(:,:,:,ip1s)   = 1.
#endif
#ifdef exclude_p2x
   where(ixistZ==1) st(:,:,:,ip2c)   = 1.
   where(ixistZ==1) st(:,:,:,ip2n)   = 1.
   where(ixistZ==1) st(:,:,:,ip2p)   = 1.
#endif
#ifdef exclude_p3x
   where(ixistZ==1) st(:,:,:,ip3c)   = 1.
   where(ixistZ==1) st(:,:,:,ip3n)   = 1.
   where(ixistZ==1) st(:,:,:,ip3p)   = 1.
   where(ixistZ==1) st(:,:,:,ip3k)   = 1.
#endif
#endif
#ifdef module_chemie
   nS = n_st_warm+1
   nE = n_st_warm+n_ch_warm
   if (n_ch_warm > 0) ch(:,:,iStartD:iEndD,1:n_ch_warm) = warm3D(:,:,iStartD:iEndD,nS:nE)
! #ifdef convert_warmstart
!    if (n_ch_warm > 0) then
!       do n=1,n_ch_warm
!          ch(:,:,iStartD:iEndD,n) = ch(:,:,iStartD:iEndD,n) /rho(:,:,iStartD:iEndD)
!       enddo
!    endif
! #endif
#endif
   deallocate( warm3D )
   where(ixistZ==1) st(:,:,:,1)   = 1. ! re-init x1x

#ifdef adjust_stoichiometry
      if (myID == master) then
         call write_log_message('   warm_in: adjust phytoplankon stoichiometry')
      endif
#ifndef exclude_p1x
      where(ixistZ==1) st(:,:,:,ip1a)   = st(:,:,:,ip1n)*rcn*chl_p1c
      where(ixistZ==1) st(:,:,:,ip1c)   = st(:,:,:,ip1n)*rcn
      where(ixistZ==1) st(:,:,:,ip1p)   = st(:,:,:,ip1n)*rcn/rcp
      where(ixistZ==1) st(:,:,:,ip1s)   = st(:,:,:,ip1n)*rcn/rcs
#endif
#ifndef exclude_p2x
      where(ixistZ==1) st(:,:,:,ip2a)   = st(:,:,:,ip2n)*rcn2*chl_p2c
      where(ixistZ==1) st(:,:,:,ip2c)   = st(:,:,:,ip2n)*rcn2
      where(ixistZ==1) st(:,:,:,ip2p)   = st(:,:,:,ip2n)*rcn2/rcp2
#endif
#ifndef exclude_p3x
      where(ixistZ==1) st(:,:,:,ip3a)   = st(:,:,:,ip3n)*rcn3*chl_p3c
      where(ixistZ==1) st(:,:,:,ip3c)   = st(:,:,:,ip3n)*rcn3
      where(ixistZ==1) st(:,:,:,ip3p)   = st(:,:,:,ip3n)*rcn3/rcp3
#endif

#else
   if (n_st_warm==34 .and. n_st_var==37) then
      if (myID == master) then
         call write_log_message('   warm_in: pXa derived from carbon biomass (pXa = pXc*0.24)')
      endif

      where(ixistZ==1) st(:,:,:,ip1a)   = st(:,:,:,ip1c)*0.24 !chl_p1c ! dummy-init p1a
      where(ixistZ==1) st(:,:,:,ip2a)   = st(:,:,:,ip2c)*0.24 !chl_p2c ! dummy-init p2a
      where(ixistZ==1) st(:,:,:,ip3a)   = 1. ! dummy-init p3a
   endif
#endif

#ifdef module_sediment
   n_dim_warm2D = n_sed_warm
   allocate( warm2D(jmax,imax,n_dim_warm2D) )
   warm2D = fail

   if (n_dim_warm2D > 0) then
      if (iwet_warm == iiwet) then !3D-mode
         if (bin_in) then
            allocate ( buf(iiwet2,n_dim_warm2D) )
            read (warm_unit) buf(:,1:n_dim_warm2D)
            do n = 1,n_dim_warm2D
               warm2D(:,:,n) = unpack(buf(:,n), ixistK_master(:,:)==1, fail)
            enddo
            deallocate(buf)
         else
            do i = 1, imax
               do j = 1, jmax
                  read(warm_unit,'(10(e20.10))') (warm2D(j,i,n),n=1,n_dim_warm2D)
               enddo
            enddo
            do n = 1, n_dim_warm2D
               where(ixistK_master==0) warm2D(:,:,n) = fail
            enddo
         endif
      elseif (iwet_warm == 1) then !1D-mode
         warm2D = fail
         read(warm_unit,'(10(e20.10))') (warm2D(jj,ii,n),n=1,n_dim_warm2D) !MK wo kommt hier jj,ii her?
      else
         ierr = 1
         write(error_message,'("ERROR: you cannot warmstart sediment with this warmstart file")')
         call stop_ecoham(ierr, msg=error_message)
      endif
      sd(:,iStartD:iEndD,1:n_dim_warm2D) = warm2D(:,iStartD:iEndD,1:n_dim_warm2D)
   endif

   deallocate( warm2D )
#endif

   close(warm_unit)

! #ifdef module_biogeo
! !    print*,'min/max(p1a)',minval (st(:,:,:,ip1c)),maxval (st(:,:,:,ip1c))
! !    print*,'min/max(p2a)',minval (st(:,:,:,ip2c)),maxval (st(:,:,:,ip2c))
! !    print*,'min/max(p3a)',minval (st(:,:,:,ip3c)),maxval (st(:,:,:,ip3c))
! !    stop
!    if (maxval (st(:,:,:,ip1a)) < 0.0) where(ixistz==1) st(:,:,:,ip1a) = st(:,:,:,ip1n)*rcn*0.24
!    if (maxval (st(:,:,:,ip2a)) < 0.0) where(ixistz==1) st(:,:,:,ip2a) = st(:,:,:,ip2n)*rcn*0.24
!    if (maxval (st(:,:,:,ip3a)) < 0.0) where(ixistz==1) st(:,:,:,ip3a) = st(:,:,:,ip3n)*rcn*0.24
! #endif

#ifdef convert_warmstart
   ! special mode for converting old warmstart files to the new data structure
   call warm_out(ierr)
   call stop_ecoham(1, msg='convert_warmstart - DONE!')
#endif

   if (ierr/=0) then
      write( error_message, '("[",i2.2,"] ERROR - warm_in: ",i3)') myID,ierr
      call stop_ecoham(ierr, msg=error_message)
   end if
   return

   99 continue
   ierr = 1
   call stop_ecoham(ierr, msg="WARMSTART open error : "//trim(filename))

   end subroutine warm_in

!=======================================================================
!
! !INTERFACE:
   subroutine close_output(ierr)
!
! !DESCRIPTION:
!
! !USES:
#ifdef module_sections
   use mod_sections, only: l_sections
#endif
   implicit none
!
! !INPUT PARAMETERS:
   integer, intent(inout) :: ierr
!
! !LOCAL VARIABLES:
!
!-----------------------------------------------------------------------

   call close_output_1D(ierr)

#if defined module_sediment && defined sediment_EMR
   call close_output_EMR(ierr)
#endif

   if (isw_dim==3)  call close_output_3D(ierr)

#ifdef module_sections
   if (l_sections) close (sec_unit)
#endif

   if (ierr/=0) then
      write( error_message, '("[",i2.2,"] ERROR - close_output",i3)') myID, ierr
      call stop_ecoham(ierr, msg=error_message)
   end if
   return

   end subroutine close_output

!=======================================================================

   end module mod_output

!=======================================================================
