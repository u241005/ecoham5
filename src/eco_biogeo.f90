!
! !MODULE: eco_biogeo --- wrapper for pelagic biogeochemistry
!
! !INTERFACE:
   MODULE mod_biogeo
#ifdef module_biogeo
!
! !DESCRIPTION:
! This module is the linker for pelagic biogeochemistry being used in ecoham
!
! !USES:
#ifdef module_biogeo_recom
   use mod_biogeo_recom
#else
   use mod_biogeo_ecoham
#endif

!=======================================================================
# endif
   end module mod_biogeo

!=======================================================================
