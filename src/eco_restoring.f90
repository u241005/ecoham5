!
! !MODULE: eco_restoring.f90 ---  restoring / nudging of ecoham statevariable
!
! !INTERFACE:
   MODULE mod_restoring
#ifdef module_restoring
!
! !DESCRIPTION:
! This module handles restoring / nudging of prognostic statevariables
!
! IO - UNITS:
!------------------------------
! rest_unit = 150 ... 150+n_st_var (max 199)
!------------------------------
!
! !USES:
   use mod_common
   use mod_var
   use mod_flux
#ifdef MPI
   use mod_mpi_parallel
#endif
   implicit none
!
!  default: all is private.
   private
! !PUBLIC MEMBER FUNCTIONS:
   public init_restoring, get_restoring, update_restoring
   public do_restoring, close_restoring
!
! !PUBLIC VARIABLES:
   character(len=99),                   public :: restoring_dir        ! ='../RESTORING'
   character(len=99),                   public :: restoring_prefix     ! ='eco4_rest'
   character(len=99),                   public :: restoring_suffix     ! ='.direct'
   integer,                             public :: restoring_sets = 367 ! max numbers of restoring sets/entries
   integer,dimension(iiwet2,n_st_var),  public :: ipr,jpr              ! index pointers to water columns beeing restored
   integer,dimension(n_st_var),public          :: n_rest_col           ! number of affected watercolumns
   integer,dimension(:,:,:),allocatable,public :: k_index_exclude_restoring ! mask for water columns beeing restored
!
! !LOCAL VARIABLES:
   integer, dimension(:,:), allocatable   :: k_index_boundaries_master ! mask for water columns beeing restored
   real, dimension(n_st_var) :: restoring_constant = 0.0   ! restoring coefficient for inner cells
!                                                          != 0 -> no nudging
!                                                          != 1 -> direct restoring
!                                                          != large number -> weak nudging
   real                                   :: gamma, delta  ! weighting factors for restoring / nudging
   integer, dimension(:,:), allocatable   :: idoy          ! time index (day-of-year) for sampling data
   integer, dimension(n_st_var)           :: rest_packsize ! restoring data pack size  !jp

#if defined cskc
   character(len=*), parameter :: restoring_endian='LITTLE_ENDIAN'
   integer,          parameter :: restoring_realkind = 8   != ibyte_per_real
#elif defined RestoringBigEndR8
   character(len=*), parameter :: restoring_endian='BIG_ENDIAN'
   integer,          parameter :: restoring_realkind = 8   != ibyte_per_real
#else
   ! standard case used in ECOHAM5
   character(len=*), parameter :: restoring_endian='LITTLE_ENDIAN'
   integer,          parameter :: restoring_realkind = 4   != ibyte_per_real
#endif

   real,dimension(kmax,iiwet2,n_st_var)   :: rest_old, rest_new  ! array containing past and future restoring data
   integer, dimension(n_st_var)           :: irec_old, irec_new  ! record index for past and future sampling data
   integer, dimension(n_st_var)           :: n_rest_set          ! number of restoring sets / sampling points
!
!    integer :: ii=80,jj=3,kk=1
!=======================================================================

   contains

!=======================================================================
!
! !INTERFACE:
   subroutine init_restoring(ierr)
#define SUBROUTINE_NAME 'init_restoring'
!
! !DESCRIPTION:
! routine initializes the restoring mechanism
!
! !USES:
   use mod_var
   use mod_utils, only: idays_of_month
   implicit none
!
! !OUTPUT PARAMETERS:
   integer, intent(inout)  :: ierr
!
! !LOCAL VARIABLES:
   real(restoring_realkind), allocatable :: buffer(:)
   integer    :: k1, k2, maxDOY, im
   integer    :: i, j, m, n, loc
!
   namelist /restoring_nml/restoring_dir,restoring_prefix,restoring_suffix, &
                           restoring_sets
!
!-----------------------------------------------------------------------
#include "call-trace.inc"

   if (myID == master) call write_log_message(' INITIALIZING restoring data')

#if defined module_biogeo || defined module_chemie
   ! NOTE:
   ! Optional nudging with specific restoring time constant for each state-
   ! variable is restricted to the inner domain only and affects only those
   ! cells, where data are specified in the restoring file.
   ! On boundaries, "hard restoring" (substitution) is applied.
#ifdef TA_nudging
   restoring_constant(ialk) = 14. !days
   if (myID == master) then
      write(long_msg,'(3x,"-alkalinity is nudged to data in inner domain (recommended for D093)")')
      call write_log_message(long_msg); long_msg = ''
   endif
#endif
#ifdef DIC_nudging
   restoring_constant(idic) = 1.  !days
   if (myID == master) then
      write(long_msg,'(3x,"-DIC is nudged to data in inner domain (recommended for D093)")')
      call write_log_message(long_msg); long_msg = ''
   endif
#endif
#endif

! open & read settings for restoring / nudging
!--------------------------------------------------------------
   if (myID == master) then ! first reading and estimate is done on master, only
      open(nml_unit, file=trim(settings_file), action='read', status='old')
      read(nml_unit, nml=restoring_nml)
      close(nml_unit)

      ! check whether number of statevariables will break with io-units offered
      if (rest_unit+n_st_var .ge. 200) then
         long_msg = 'ERROR: number of statevariables conflicts with available io-units' &
                    //new_line('a')// 'adjust entries in module mod_common and compile again'
         call stop_ecoham(ierr, msg=long_msg); long_msg = ''
      endif

      ! estimate max. dimension of restoring entries
      restoring_sets = 0
      do n = 1, n_st_var
#ifdef exclude_p1x
      select case (trim(st_name(n)))
         case ('p1c','p1n','p1p','p1a','p1s')
            cycle  ! do nothing !
      end select
#endif
#ifdef exclude_p2x
      select case (trim(st_name(n)))
         case ('p2c','p2n','p2p','p2a')
            cycle  ! do nothing !
      end select
#endif
#ifdef exclude_p3x
      select case (trim(st_name(n)))
         case ('p3c','p3n','p3p','p3k','p3a')
            cycle  ! do nothing !
      end select
#endif

         ! open file
         filename = trim(restoring_dir)//trim(restoring_prefix)//'_'//trim(st_name(n))//'.header'
         open(asc_unit, file=trim(filename), status='old', form='formatted')
         ! read times for all sampling points
         read(asc_unit,*,end=97) rest_packsize(n)
         read(asc_unit,*,end=97) n_rest_set(n)
         restoring_sets = max(restoring_sets,n_rest_set(n))
         close(asc_unit)
      enddo
   end if ! master

#ifdef MPI
   call MPI_Bcast( restoring_sets, 1, MPI_INT, master, MPI_COMM_ECOHAM, ierr_mpi)
#endif

   ! allocate arrays known on any local domain
   allocate( idoy(restoring_sets,n_st_var) )
   idoy = 0

   allocate( k_index_exclude_restoring(jmax,iStartD:iEndD,n_st_var) )
   k_index_exclude_restoring = 0

   ! allocate arrays needed for full domain
   if (myID == master) then
      allocate( k_index_boundaries_master(jmax,imax) )
   endif

   ! read metadata and create index-array on full domain each state variable
   do n = 1, n_st_var

#ifdef exclude_p1x
      select case (trim(st_name(n)))
         case ('p1c','p1n','p1p','p1a','p1s')
            cycle  ! do nothing !
      end select
#endif
#ifdef exclude_p2x
      select case (trim(st_name(n)))
         case ('p2c','p2n','p2p','p2a')
            cycle  ! do nothing !
      end select
#endif
#ifdef exclude_p3x
      select case (trim(st_name(n)))
         case ('p3c','p3n','p3p','p3k','p3a')
            cycle  ! do nothing !
      end select
#endif

      if (myID == master) then
         ! open file
         filename = trim(restoring_dir)//trim(restoring_prefix)//'_'//trim(st_name(n))//'.header'
         write(long_msg,'(3x,"restoring ",a6,4x," from file: ",a)') st_name(n), trim(filename)
         call write_log_message(long_msg); long_msg = ''

         ! read header
         open(asc_unit,file=trim(filename),status='old', form='formatted')
         ! read times for all sampling points
         read(asc_unit,*,end=97)rest_packsize(n)
         read(asc_unit,*,end=97)n_rest_set(n)
         do m = 1, n_rest_set(n)
            read(asc_unit,*)idoy(m,n)  ! idoy(366,n_st_var):= time entries associated to data sets
         enddo
         ! set first and last record to 01.01. 0:00 and 31.12. 24:00, respectively
         im = 12
         call idays_of_month(year,im,maxDOY,31)
         idoy(1,n) = 0
         idoy(n_rest_set(n),n) = maxDOY

         ! read & construct grid pointers for affected water columns
         read(asc_unit,*)n_rest_col(n)
         do loc = 1, n_rest_col(n)                ! loop over affected water columns
            read(asc_unit,*) j, i, k1, k2         ! get lon(j)/lat(i) indices
            if (k_index_master(j,i)>0) then       ! construct pointer indices
               ipr(loc,n) = i
               jpr(loc,n) = j
               if (k1/=1.or.k2/=k_index_master(j,i)) then
                  ierr = 1
                  write(long_msg, '("ERROR: restoring column does not have max extension",4i6)') i, j, k1, k2
                  call stop_ecoham(ierr, msg=long_msg); long_msg = ''
               endif
            else
               ierr = 1
               write( long_msg, '("ERROR: restoring index on land",3i6)')i,j,n
               call stop_ecoham(ierr, msg=long_msg); long_msg = ''
            endif

            !print*,loc, ipr(loc,n), jpr(loc,n)
         enddo
         close(asc_unit)

         ! BOUNDARIES: check, whether sufficient watercolumns are specified each statevariable to fill boundaries
         k_index_boundaries_master(:,:) = 0
         k_index_boundaries_master(1:(2+offset_jstart)    , :) = 1
         k_index_boundaries_master((jmax-offset_jend):jmax, :) = 1
         k_index_boundaries_master(:, 1:(2+offset_istart)    ) = 1
         k_index_boundaries_master(:, (imax-offset_iend):imax) = 1

         ! exclude dry points
         where(ixistk_master==0) k_index_boundaries_master(:,:) = 0

         do loc = 1, n_rest_col(n)    ! loop over restoring water columns
            k_index_boundaries_master(jpr(loc,n),ipr(loc,n)) = 0
         enddo
         if(sum(k_index_boundaries_master(:,:))>0) then
            ierr = 1
            where(ixistk_master==0) k_index_boundaries_master(:,:) = 99
            open (asc_unit, file = 'restoring_error_indices.mask', &
                            form = 'formatted', status = 'replace')
            write(fmtstr,'(''('',i3,''i1)'')') jmax
            do i = 1, imax
               write(asc_unit,trim(fmtstr)) k_index_boundaries_master(:,i)
            enddo
            close(asc_unit)
            write(log_msg(1),'("ERROR: boundary value missing for ",a)') st_name(n)
            write(log_msg(2),'(3x,"either reduce boundary offset or process ",&
                           &"restoring data according to offset choosen")')
            write(log_msg(3),'("sum(k_index_boundaries_master): ",i8)') sum(k_index_boundaries_master(:,:))
            long_msg = trim(log_msg(1))//new_line('a')//trim(log_msg(2))//new_line('a')//trim(log_msg(3))//new_line('a')
            call write_log_message(long_msg); long_msg = ''
            call stop_ecoham(ierr)
         endif

         ! initial open for files with real data
         filename = trim(restoring_dir)// &
                    trim(restoring_prefix)//'_'//trim(st_name(n))//trim(restoring_suffix)
         open(rest_unit+n,file = trim(filename), status='old',          &
                  form = 'unformatted', access = 'direct',              &
                  recl = rest_packsize(n) * restoring_realkind,         &
                  convert = trim(restoring_endian))

         ! initialize record counters
         irec_old(n)=0
         irec_new(n)=0
         ! scan for pair of sampling data matching simulation time
         do m = max(1,irec_new(n)), n_rest_set(n)-1
            if(day_of_year>=idoy(m,n).and.day_of_year<idoy(m+1,n))then
               irec_old(n)=m
               irec_new(n)=m+1
               exit
            endif
         enddo
         if (irec_old(n)==0 .or. irec_new(n)==0) then
            write(long_msg,'("ERROR: no restoring records matching time ",&
                              &f7.3," found for ",a)') day_of_year, st_name(n)
            call write_log_message(long_msg); long_msg = ''
         endif
         !print*,st_name(n), rest_unit+n, irec_old(n), irec_new(n), n_rest_set(n)

         ! check for endian type and precision
         if ( st_name(n) == 'x1x' ) then
            allocate( buffer(rest_packsize(n)) )
            read(rest_unit+n, rec = 1) buffer
            !print*,minval(buffer),maxval(buffer)
            if ( minval(buffer)<one .or. maxval(buffer)>one ) then
               write(long_msg,'("ERROR: restoring data for x1x suspicious.",a)') &
                                ' Check settings for endian style and realkind !'
               call write_log_message(long_msg); long_msg = ''
               ierr = 1
               exit ! exit n-loop
            endif
            deallocate( buffer )
         endif

         ! initialize restoring data
         call read_restoring(rest_unit+n,irec_old(n),rest_old(:,:,n),ierr)
         call read_restoring(rest_unit+n,irec_new(n),rest_new(:,:,n),ierr)

      endif ! master

#ifdef MPI
      call MPI_Bcast( idoy, restoring_sets*n_st_var, MPI_INT, master, MPI_COMM_ECOHAM, ierr_mpi)
      call MPI_Bcast( n_rest_col, n_st_var, MPI_INT, master, MPI_COMM_ECOHAM, ierr_mpi)
      call MPI_Bcast( ipr, iiwet2*n_st_var, MPI_INT, master, MPI_COMM_ECOHAM, ierr_mpi)
      call MPI_Bcast( jpr, iiwet2*n_st_var, MPI_INT, master, MPI_COMM_ECOHAM, ierr_mpi)
      call MPI_Bcast( irec_old, n_st_var, MPI_INT, master, MPI_COMM_ECOHAM, ierr_mpi)
      call MPI_Bcast( irec_new, n_st_var, MPI_INT, master, MPI_COMM_ECOHAM, ierr_mpi)
      call MPI_Bcast( rest_old, kmax*iiwet2*n_st_var, MPI_REAL8, master, MPI_COMM_ECOHAM, ierr_mpi)
      call MPI_Bcast( rest_new, kmax*iiwet2*n_st_var, MPI_REAL8, master, MPI_COMM_ECOHAM, ierr_mpi)
#endif

      ! construct mask each statevariable for watercolumns being excluded from hydro calculations
      k_index_exclude_restoring(:,:,n) = k_index(:,:)      ! equal to k_index at first
      !if(restoring_constant(n).eq.1.)then                 ! exclude only in case of "hard" restoring
      do i = iStartD, iEndD
         do j = jStartCalc, jEndCalc
            do loc = 1, n_rest_col(n)                      ! loop over restoring water columns
               if (ipr(loc,n)==i .and. jpr(loc,n)==j) then
                  k_index_exclude_restoring(j,i,n) = -1    ! exclude restoring cells from calculations
               endif
            enddo
         enddo
      enddo
      !endif

   enddo

   if (myID == master) then
      deallocate(k_index_boundaries_master )

   !  do n=1,n_st_var
   !     write(*,'("idoy_",a3,":",20i5)') st_name(n),(idoy(m,n),m=1,n_rest_set(n))
   !  enddo
   endif

   if (ierr/=0) then
      write( error_message, '("[",i2.2,"] ERROR - init_restoring: ",i3)') myID,ierr
      call stop_ecoham(ierr, msg=error_message)
   end if
   return

97 continue
   ierr = 1
   write(long_msg,'("[",i2.2,"] RESTORING: termination due to unexpected EOF in ",a)') myID, trim(filename)
   call stop_ecoham(ierr, msg=long_msg)

   end subroutine init_restoring

!=======================================================================
!
! !INTERFACE:
   subroutine get_restoring(td_,ierr)
#define SUBROUTINE_NAME 'get_restoring'
!
! !DESCRIPTION:
! subroutine for reading restoring data
!
! !USES:
   implicit none
!
! !INPUT PARAMETERS:
   real(kind=8), intent(in)     :: td_
!
! !OUTPUT PARAMETERS:
   integer,      intent(inout)  :: ierr
!
! !LOCAL VARIABLES:
   integer                      :: n
!
!-----------------------------------------------------------------------
#include "call-trace.inc"

   do n = 1, n_st_var

#ifdef exclude_p1x
      select case (trim(st_name(n)))
         case ('p1c','p1n','p1p','p1a','p1s')
            cycle  ! do nothing !
      end select
#endif
#ifdef exclude_p2x
      select case (trim(st_name(n)))
         case ('p2c','p2n','p2p','p2a')
            cycle  ! do nothing !
      end select
#endif
#ifdef exclude_p3x
      select case (trim(st_name(n)))
         case ('p3c','p3n','p3p','p3k','p3a')
            cycle  ! do nothing !
      end select
#endif

      ! check, whether new data are needed
      if(td_>=idoy(irec_new(n),n))then
         irec_old(n) = irec_new(n)
         irec_new(n) = irec_old(n)+1

         rest_old(:,:,n) = rest_new(:,:,n)
         if (myID == master) then
            !print*,'call read_restoring ',rest_unit+n, irec_new(n)
            call read_restoring(rest_unit+n,irec_new(n),rest_new(:,:,n),ierr)
         endif
#ifdef MPI
         call MPI_Bcast( rest_new, kmax*iiwet2*n_st_var, MPI_REAL8, master, MPI_COMM_ECOHAM, ierr_mpi)
#endif

      endif

   enddo

   if (ierr/=0) then
      write( error_message, '("[",i2.2,"] ERROR - get_restoring: ",i3)') myID,ierr
      call stop_ecoham(ierr, msg=error_message)
   end if
   return

   end subroutine get_restoring

!=======================================================================
!
! !INTERFACE:
   subroutine read_restoring(iunit,irec,rest2D,ierr)
#define SUBROUTINE_NAME 'read_restoring'
!
! !DESCRIPTION:
!
! !USES:
   implicit none
!
! !INPUT PARAMETERS:
   integer,      intent(in)     :: iunit
   integer,      intent(in)     :: irec
!
! !OUTPUT PARAMETERS:
   real, dimension(kmax,iiwet2), intent(out) :: rest2D
   integer,                    intent(inout) :: ierr
!
! !LOCAL VARIABLES:
   real,    dimension(jmax,kmax,imax) :: dumw
   logical, dimension(jmax,kmax,imax) :: mask
   real(kind=restoring_realkind), dimension(iiwet)   :: input3d
   real,                          dimension(iiwet)   :: restpack
   integer                :: ios
   integer                :: i,j,k,k0,loc,n
!
!-----------------------------------------------------------------------
#include "call-trace.inc"

   ! get variable index
   n = iunit-rest_unit
   rest2D = fail

   ! construct mask for unpack
   mask = .false.
   do loc = 1, n_rest_col(n)
      i = ipr(loc,n)
      j = jpr(loc,n)
      k0 = k_index_master(j,i)
      do k = 1, k0
         mask(j,k,i) = .true.
      enddo
   enddo

   ! read data and assign to gridcells
   read(iunit, rec = irec, iostat = ios) input3d(1:rest_packsize(n))
!   print*,'read data (unit, irec)', iunit, irec, rest_packsize(n), count(mask), &
!                 minval(input3d(1:rest_packsize(n))), maxval(input3d(1:rest_packsize(n)))
   if (ios /= 0 .and. ios /= 5002) then
      ierr = 1
      write(long_msg,'("ERROR reading restoring data from unit ", &
                     & i3," rec=",i5," iostat=",i5)') iunit,irec,ios
      call stop_ecoham(ierr, long_msg); long_msg = ''
   endif
   restpack = real( input3d )
   dumw(:,:,:) = unpack( restpack(1:rest_packsize(n)), mask, fail )
   do loc = 1, n_rest_col(n)
      i = ipr(loc,n)
      j = jpr(loc,n)
      k0 = k_index_master(j,i)
      do k = 1, k0
         rest2D(k,loc) = dumw(j,k,i)
      enddo
   enddo

   if (ierr/=0) then
      write( error_message, '("[",i2.2,"] ERROR - read_restoring: ",i3)') myID,ierr
      call stop_ecoham(ierr, msg=error_message)
   end if
   return

   end subroutine read_restoring

!=======================================================================
!
! !INTERFACE:
   subroutine update_restoring(td_,ierr)
#define SUBROUTINE_NAME 'update_restoring'
!
! !DESCRIPTION:
!
! !USES:
   implicit none
!
! !INPUT PARAMETERS:
   real(kind=8), intent(in)     :: td_
!
! !OUTPUT PARAMETERS:
   integer,      intent(inout)  :: ierr
!
! !LOCAL VARIABLES:
   real :: dummy
!
!-----------------------------------------------------------------------
#include "call-trace.inc"

   dummy = td_  !to get rid of warning unused ...

   if (ierr/=0) then
      write( error_message, '("[",i2.2,"] ERROR - update_restoring: ",i3)') myID,ierr
      call stop_ecoham(ierr, msg=error_message)
   end if
   return

   end subroutine update_restoring

!=======================================================================
!
! !INTERFACE:
   subroutine do_restoring(td_,ierr)
#define SUBROUTINE_NAME 'do_restoring'
!
! !DESCRIPTION:
! container for really applying restoring / nudging values to statevariables
!
! !USES:
   implicit none
!
! !INPUT PARAMETERS:
   real,    intent(in)     :: td_
   integer, intent(inout)  :: ierr
!
! !OUTPUT PARAMETERS:
!
! !LOCAL VARIABLES:
   integer       :: i, j, k, k0, loc, n
   real          :: g1, g2, t1, t2, rest_val
!
!-----------------------------------------------------------------------
#include "call-trace.inc"

   t1=0; t2=0; ! to get rid of warning "uninitialized"
   g1=0; g2=0; ! to get rid of warning "uninitialized"

! apply for each statevariable
   do n = 1, n_st_var
#ifdef exclude_p1x
      select case (trim(st_name(n)))
         case ('p1c','p1n','p1p','p1a','p1s')
            cycle  ! do nothing !
      end select
#endif   
#ifdef exclude_p2x
      select case (trim(st_name(n)))
         case ('p2c','p2n','p2p','p2a')
            cycle  ! do nothing !
      end select
#endif
#ifdef exclude_p3x
      select case (trim(st_name(n)))
         case ('p3c','p3n','p3p','p3k','p3a')
            cycle  ! do nothing !
      end select
#endif

      t1 = real(idoy(irec_old(n),n))
      t2 = real(idoy(irec_new(n),n))
      g1 = max(zero, (t2-td_)/(t2-t1))
      g2 = one - g1

      if (g1<0 .or. g2<0) then
         ierr = 1
         write (long_msg,*) 'do_restore #472 - ',st_name(n),td_,idoy(irec_old(n),n),idoy(irec_new(n),n),g1,g2
         call write_log_message(long_msg); long_msg = ''
         call stop_ecoham(ierr, msg='FATAL ERROR: negative weighting factor')
      endif

! if (n==idic) write(*,'("do restore#462 -",a5,":",f9.4,"idoy(old/new):",2i5,    &
!                    &   "g1/g2:",2f9.6,"irec(old/new):",2i4)') st_name(n), td_,  &
!                    &  idoy(irec_old(n),n),idoy(irec_new(n),n),g1,g2,irec_old(n),irec_new(n)

      do loc = 1, n_rest_col(n)
         i = ipr(loc,n)
         if (iStartD<=i .and. i<=iEndD) then !latArea of domain
            j = jpr(loc,n)
            ! on real boundaries only apply "hard restoring"
            gamma = 1.0
            delta = 0.0
            ! set time constant and apply nudging in inner field
            if (iStartCalc<=i .and. i<=iEndCalc .and. jStartCalc<=j .and. j<=jEndCalc) then
               if (restoring_constant(n)>=1.) then
                  gamma = 1. / restoring_constant(n)
                  delta = 1.0 - gamma
               else ! no nudging / restoring
                  gamma = 0.0
                  delta = 1.0
               endif
            endif

! #ifdef module_chemie
!          ! for alkalinity and dic apply nudging everywhere
!          if (n==ialk .or. n==idic)then
!             gamma = 1. / restoring_constant(n)
!             delta = 1.0 - gamma
!          endif
! #endif
            k0 = k_index(j,i)
            do k = 1, k0
            !print*,i,j,k,g1,rest_old(k,loc,n),g2,rest_new(k,loc,n)
               rest_val = g1*rest_old(k,loc,n) + g2*rest_new(k,loc,n)
               rest_val = gamma*rest_val + delta*st(j,k,i,n)
#ifdef fixed_Baltic_DIC_TA
               if(n==ialk.or.n==idic)then
                 if(j>=86.and.i>=44.and.i<=47)then !Baltic
                   rest_val=1600.
                 endif
               endif
#endif
               f_res(j,k,i,n) = (rest_val - st(j,k,i,n)) *dz(j,k,i)
               st(j,k,i,n)    = rest_val
!if(n==idic.and.i==ii.and.j==jj.and.k==1)then
!print*,' old: ',g1,rest_old(k,loc,n)
!print*,' new: ',g2,rest_new(k,loc,n)
!!print*,rest_val,gamma,g1*rest_old(k,loc,n) + g2*rest_new(k,loc,n),delta,st(j,k,i,n)
!endif
            enddo
         endif !latArea
      enddo !do loc=1,n_rest_col(n)

   enddo !do n=1,n_st_var

#ifdef adjust_stoichiometry
   do n = 1, n_st_var
      select case (trim(st_name(n)))
         case ('p1a','p1c','p1p','p1s','p2a','p2c','p2p','p3a','p3c','p3p')
            ! continue this loop
         case default
            cycle  ! leave this loop and do nothing !
      end select
      do loc = 1, n_rest_col(n)
         i = ipr(loc,n)
         if (iStartD<=i .and. i<=iEndD) then !latArea of domain
            j = jpr(loc,n)
            k0 = k_index(j,i)
            select case (trim(st_name(n)))
#ifndef exclude_p1x
               case ('p1a'); st(j,1:k0,i,n) = st(j,1:k0,i,ip1n)*rcn*chl_p1c
               case ('p1c'); st(j,1:k0,i,n) = st(j,1:k0,i,ip1n)*rcn
               case ('p1p'); st(j,1:k0,i,n) = st(j,1:k0,i,ip1n)*rcn/rcp
               case ('p1s'); st(j,1:k0,i,n) = st(j,1:k0,i,ip1n)*rcn/rcs
#endif
#ifndef exclude_p2x
               case ('p2a'); st(j,1:k0,i,n) = st(j,1:k0,i,ip2n)*rcn2*chl_p2c
               case ('p2c'); st(j,1:k0,i,n) = st(j,1:k0,i,ip2n)*rcn2
               case ('p2p'); st(j,1:k0,i,n) = st(j,1:k0,i,ip2n)*rcn2/rcp2
#endif
#ifndef exclude_p3x
               case ('p3a'); st(j,1:k0,i,n) = st(j,1:k0,i,ip3n)*rcn3*chl_p3c
               case ('p3c'); st(j,1:k0,i,n) = st(j,1:k0,i,ip3n)*rcn3
               case ('p3p'); st(j,1:k0,i,n) = st(j,1:k0,i,ip3n)*rcn3/rcp3
#endif
            end select
         endif !latArea
      enddo !do loc=1,n_rest_col(n)
   enddo !do n=1,n_st_var
#endif

   if (ierr/=0) then
      write( error_message, '("[",i2.2,"] ERROR - do_restoring: ",i3)') myID,ierr
      call stop_ecoham(ierr, msg=error_message)
   end if
   return

   end subroutine do_restoring

!=======================================================================
!
! !INTERFACE:
   subroutine close_restoring(ierr)
#define SUBROUTINE_NAME 'close_restoring'
!
! !DESCRIPTION:
!
! !USES:
   implicit none
!
! !INPUT PARAMETERS:
   integer, intent(inout)  :: ierr
!
   integer :: n
!-----------------------------------------------------------------------
#include "call-trace.inc"

   if (myID == master) then
      close(rest_unit)
      do n=1,n_st_var
         close(rest_unit+1)
      enddo
   endif

   if (ierr/=0) then
      write( error_message, '("[",i2.2,"] ERROR - close_restoring",i3)') myID, ierr
      call stop_ecoham(ierr, msg=error_message)
   end if
   return

   end subroutine close_restoring

!=======================================================================
# endif
   end module mod_restoring

!=======================================================================
