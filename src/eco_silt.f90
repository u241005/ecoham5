!
! !MODULE: eco_silt.f90 ---  pelagic silt concentratios for ecoham
!
! !INTERFACE:
   MODULE mod_silt
#ifdef module_silt
!
! !DESCRIPTION:
! This module is the container for handling pelagic silt
! Silt concentrations may be either set constant, read
! from file or calculated dynamically online.
! Dynamic silt is modelled in relation to wind forcing.
! Model formulations are based on code-sniplets from
! Piet Ruardij (NIOZ) and Johann V.d.Molen (Cefas).
!
! IO - UNITS:
!------------------------------
! silt_unit       = 145
!------------------------------
!
! !USES:
   use mod_common
   use mod_grid
#ifdef MPI
   use mod_mpi_parallel
#endif
   implicit none
!
!  default: all is private.
   private
! !PUBLIC MEMBER FUNCTIONS:
   public init_silt, get_silt, update_silt, calc_silt, close_silt
!
! !PUBLIC VARIABLES:
   real, dimension(:,:,:), allocatable, public :: silt   ! silt concentration (g m-3)
!
! !LOCAL VARIABLES:
   character*99  :: silt_dir              !='../FORCING_Jeb40/FORC/1997/'
   character*99  :: silt_dat              !='silt.dat'
   integer       :: silt_mode     =  1    ! 0 - const. silt concentration "silt_conc"
!                                         ! 1 - read silt data drom file
!                                         ! 2 - ! dynamic silt model
   real          :: silt_conc     =  0.0  !initial silt concentration, if silt_mode=0 (g m-3)
   real          :: exts          =  0.06 !extinction coeffiecient for silt (mg/l):ERSEM=0.04
   integer       :: silt_timestep = 24    !timestep of silt forcing (h)
   real          :: poro_const    =  0.4  !initial porosity (PietRuardij: values between: 0.4 coarse sand to 0.7 very fine clay.)

#if defined SiltBigEndR8
   character(len=*), parameter :: silt_endian='BIG_ENDIAN'
   integer,          parameter :: silt_realkind = 8   != ibyte_per_real
#else
   character(len=*), parameter :: silt_endian='LITTLE_ENDIAN'
   integer,          parameter :: silt_realkind = 4   != ibyte_per_real
#endif
   real                        :: silt_t1, silt_t2
   integer                     :: irec_silt_old
   integer                     :: n_step_silt         ! (-)    number of timesteps for integration
   integer                     :: dt_silt    = 3600   ! (s)    timestep for silt model
   real, parameter             :: fw         =  0.1   ! (-)    wave-friction factor
   real, parameter             :: p_silt_min =  0.15  ! (%)    minimum vol. percentage of silt in water
   real, parameter             :: kt0        = -0.02  ! (1/d)  SPM time constant (background "diffusion")
   real, parameter             :: kt1        =  8.0   ! (m3/(kg*d))  SPM time constant (concentration dependent settling)
   real, dimension(:,:),   allocatable :: p_poro      ! volume percentage of water in sediment (-)
   real, dimension(:,:),   allocatable :: p_silt      ! volume percentage of silt in water (%)
   real, dimension(:,:,:), allocatable :: silt_new, silt_old
!
   integer :: ii=10,jj=10 ! for bug-hunting ...
!
!=======================================================================

   contains

!=======================================================================
!
! !INTERFACE:
   subroutine init_silt(ierr)
#define SUBROUTINE_NAME 'init_silt'
!
! !DESCRIPTION:
!
! !USES:
   use mod_utils, only : idays_of_month
   implicit none
!
! !OUTPUT PARAMETERS:
   integer, intent(inout)  :: ierr
!
! !LOCAL VARIABLES:
   integer                 :: im, ida, rc
!
   namelist /silt_nml/silt_dir, silt_dat, silt_mode, silt_conc, exts,   &
                       silt_timestep, poro_const

!
!-----------------------------------------------------------------------
#include "call-trace.inc"

   if (myID==master)  call write_log_message(" INITIALIZING silt")

   ! open & read silt settings
   !--------------------------------------------------------------
   open(nml_unit,file=trim(settings_file), action='read', status='old')
   read(nml_unit, nml=silt_nml)
   close(nml_unit)

   ! calculate number of days for current year
   im = 12 !! MK: idays_of_month(-,X,-,-) is intend(inout) !!
   call idays_of_month(year,im,ida,31)

   ! allocate memory and default initialization
   allocate(silt(jmax,kmax,iStartD:iEndD),stat=rc)
   if (rc /= 0) STOP 'allocate_memory(): Error allocating (silt)'
   silt = fail

   select case (silt_mode)
      case (0) ! apply const. silt concentration
         if (myID == master) then
            write( long_msg, '(3x,a9,11x," set constant: ",a," = ",f4.1,a)') &
            &      'silt data', 'silt_conc', silt_conc,' (g m-3)'
            call write_log_message(long_msg); long_msg = ''
         endif ! master
         where(ixistZ==1) silt = silt_conc

      case (1) ! read silt data from file
         allocate( silt_new(jmax,kmax,iStartD:iEndD) ); silt_new = fail
         allocate( silt_old(jmax,kmax,iStartD:iEndD) ); silt_old = fail
         filename = trim(silt_dir)//trim(silt_dat)
         if (myID == master) then
            write( long_msg, '(3x,a9,11x," from file: ",a)') 'silt data', trim(filename)
            call write_log_message(long_msg); long_msg = ''
         endif ! master
         open(silt_unit,file = trim(filename), status='old',         &
                  form = 'unformatted',access = 'direct',            &
                  recl = iiwet * silt_realkind,                      &
                  convert = trim(silt_endian), err = 9000)
         irec_silt_old = -1

         call get_silt(day_of_year,ierr)

         call update_silt(day_of_year,ierr)

      case (2) ! dynamic silt model
         if (myID==master) then
            write(long_msg,'(3x,"use dynamic silt model")')
            call write_log_message(long_msg); long_msg = ''
         endif ! master

         !calculate time stepping
!print*,'silt_init#146',dt,dt_silt,dt*secs_per_day
         n_step_silt = max(1,int(dt*secs_per_day/dt_silt))
         if (n_step_silt==1) dt_silt = nint(dt*secs_per_day)
!print*,'silt_init#146',n_step_silt,dt*secs_per_day/dt_silt

         if (myID==master) then
            write( long_msg, '(3x,"silt model using nstep=",i3," and dt_silt=",i5)') n_step_silt,dt_silt
            call write_log_message(long_msg); long_msg = ''
         endif ! master

         ! allocate arrays
         allocate(p_poro(jmax,iStartD:iEndD),stat=rc)
         if (rc /= 0) STOP 'allocate_memory(): Error allocating (p_poro)'
         p_poro = fail

         allocate(p_silt(jmax,iStartD:iEndD),stat=rc)
         if (rc /= 0) STOP 'allocate_memory(): Error allocating (p_silt)'
         p_silt = fail

         ! (PietRuardij: values between: 0.4 coarse sand to 0.0 very fine clay.)
         where(ixistK==1) p_poro = poro_const    ! volume percentage of water in sediment

         !calculate silt: total in sediment value ( between 0-1)
         ! e0 = 0.38662 porosity for a pure sand bed
         ! c1 = 0.00415 empirical constant (Ruardij, pers. comm., 2007)
         !p_silt = (p_poro - 0.38662 )/ 0.00415
         ! ... turn this percentage into a relative number between 0 and 1
         where(ixistK==1) p_silt = (p_poro - 0.38662 )/ 0.00415 / 100. !(%)

         ! set background concentration
         where(ixistZ==1) silt = silt_conc

         call update_silt(day_of_year,ierr)

      case default ! no silt
         where(ixistZ==1) silt = ZERO

   end select

!print*,'silt#138',silt(4,1,4),exts

   if (ierr/=0) then
      write( error_message, '("[",i2.2,"] ERROR - init_silt: ",i3)') myID, ierr
      call stop_ecoham(ierr, error_message);
   end if
   return

9000 continue
   ierr = 1
   write( error_message,'("mod_silt: open error : ",a)') trim(filename)
   call stop_ecoham(ierr, error_message);

   end subroutine init_silt

!=======================================================================
! !INTERFACE:
   subroutine get_silt(td_,ierr)
#define SUBROUTINE_NAME 'get_silt'
!
! !DESCRIPTION:
!
! !USES:
   implicit none
!
! !INPUT PARAMETERS:
   real,    intent(in)     :: td_
!
! !OUTPUT PARAMETERS:
   integer, intent(inout)  :: ierr
!
! !LOCAL VARIABLES:
   !real(kind=silt_realkind), dimension(iiwet2)   :: input2d
   real(kind=silt_realkind), dimension(iiwet)    :: input3d
   real, dimension(jmax,kmax,imax)   :: in_array3D
   integer                 :: irec1, irec2, ios
!   character(len=99)       :: fmtstr
!   integer                 :: i,j
!
!-----------------------------------------------------------------------
#include "call-trace.inc"

   select case (silt_mode)
      case (1) ! read silt data from file

         irec1 = int((24./real(silt_timestep))*td_)+1
         !irec1 = min(irec1,1460) ! would be obsolete, if forcing was properly done
         !irec2 = min(int(24/silt_timestep*365),irec1+1)
         if (irec1/=irec_silt_old) then
            silt_t1 = (irec1-1) *silt_timestep/24.
            silt_t2 = silt_t1 + silt_timestep/24.
            read (silt_unit, rec = irec1, iostat = ios) input3d
            if (ios == 5002) then ! reached end-of-file
               irec1 = irec_silt_old
            else
               in_array3D = unpack(real(input3d), ixistz_master==1, fail)
               silt_old(:,:,iStartD:iEndD) = in_array3D(:,:,iStartD:iEndD)
            endif

#ifdef SiltInterpolationOff
            irec2 = irec1
#else
            irec2 = irec1+1
#endif
            read (silt_unit, rec = irec2, iostat = ios) input3d
            if (ios /= 0) then
               if (myID==master)  then
                  write(long_msg,'("[",i2.2,"] ERROR reading SILT for day ",f8.3)') myID, td_
                  call write_log_message(long_msg); long_msg = ''
                  !print*,td_,irec1,irec2,ios
                  if (ios == 5002) then ! reached end-of-file
                     irec2 = irec1
                     write(long_msg,'("SILT is set from record entries ",2i6)') irec1,irec2
                     call write_log_message(long_msg); long_msg = ''
                  endif
               endif
            else
               in_array3D = unpack(real(input3d), ixistz_master==1, fail)
               silt_new(:,:,iStartD:iEndD) = in_array3D(:,:,iStartD:iEndD)
            endif

            !if (myID==master) print*,'read silt data for day ',td_,irec1,irec2

!--------------------------------------
! ! output silt.dump for debugging
!--------------------------------------
! if (myID == master) then
!    filename = 'silt_new.dump'
!    open(146,file = trim(filename), form='formatted', status='replace')
!    write(fmtstr,'(''('',i3,''e20.10)'')') jmax
!    do i=1,imax
!       write(146,fmtstr) (silt_new(j,1,i),j=1,jmax)
!    enddo
!    close(146)
! !  stop 'silt#264'
! end if ! master
!--------------------------------------

            irec_silt_old = irec1
         endif

      case default
         ! do nothing !

   end select

   if (ierr/=0) then
      write( error_message, '("[",i2.2,"] ERROR - get_silt: ",i3)') myID, ierr
      call stop_ecoham(ierr, error_message);
   end if
   return

   end subroutine get_silt

!=======================================================================
! !INTERFACE:
   subroutine update_silt(td_,ierr)
#define SUBROUTINE_NAME 'update_silt'
!
! !DESCRIPTION:
!
! !USES:
   use mod_var
#ifdef module_meteo
   use mod_meteo, only : exts_field
#endif
   implicit none
!
! !INPUT PARAMETERS:
   real,    intent(in)     :: td_
!
! !OUTPUT PARAMETERS:
   integer, intent(inout)  :: ierr
!
! !LOCAL VARIABLES:
   real                    :: alpha, beta
!
!-----------------------------------------------------------------------
#include "call-trace.inc"

   select case (silt_mode)
      case (1) ! read silt data from file
         call get_silt(td_,ierr)
         !! temporal interpolation
         alpha = (td_-silt_t1)/(silt_t2-silt_t1)
         beta  = 1.-alpha
         silt  = alpha * silt_new + beta * silt_old  !(g(?) m-3)

      case (2) ! dynamic silt model
         call calc_silt(ierr)

   end select

#ifdef module_meteo
   where(ixistZ==1) exts_field = silt*exts
#endif

   if (ierr/=0) then
      write( error_message, '("[",i2.2,"] ERROR - update_silt: ",i3)') myID, ierr
      call stop_ecoham(ierr, error_message);
   end if
   return

   end subroutine update_silt

!=======================================================================
! !INTERFACE:
   subroutine calc_silt(ierr)
#define SUBROUTINE_NAME 'calc_silt'
!
! !DESCRIPTION:
!
! !USES:
   use mod_var
#ifdef module_meteo
   use mod_meteo, only : u10
#endif
   use mod_hydro, only : rho
   implicit none
!
! !INPUT PARAMETERS:
!
! !INPUT PARAMETERS:
   integer, intent(inout) :: ierr
!
! !OUTPUT PARAMETERS:
!
! !LOCAL VARIABLES:
   real, parameter         :: g   = 9.81   ! (m/s2) gravitational acceleration
   real, parameter         :: c2  = 0.234  ! - empirical constant -
   real, parameter         :: c3  = 8.14   ! - empirical constant -
   real, parameter         :: c4  = 0.7    ! - empirical constant -
   real, parameter         :: c5  = 1.2    ! - empirical constant -
   real, parameter         :: H0  = 0.25   ! (m) minimum wave heigth; was 0.75(m)
   real, parameter         :: Tz0 = 3.0    ! (s) minimum zero crossing period; was 5.0(s)
   real                    :: Silt_surf, Silt_bot, Silt_grad
   real                    :: Silt_settling, Silt_stirring
   real                    :: wind = 10.0   ! default windspeed (m/s)
   real                    :: k_timelag, depth, depthfact
   real                    :: U, Hs, Tz, uw, tau
!   real                    :: deltatemp, strat_thresh
   integer                 :: it
   integer                 :: i,j,k,k0
!
!-----------------------------------------------------------------------
#include "call-trace.inc"

   do i = max(iStartCalc-1,iStartD), min(iEndCalc+1,iEndD)
      do j = max(jstartCalc-1,1), min(jEndCalc+1,jmax)
         k0 = k_index(j,i)
         if (k0.ge.1) then
#ifdef module_meteo
            wind  = u10(j,i)          ! windspeed (m/s)
#endif
            depth = sum(dz(j,1:k0,i)) ! actual water depth (m)
!uk vorberechnen: depth_sum2tok0 = sum(dz(j,2:k0,i)
!uk where ((k_index(j,i) = 1) depth_sum2tok0=0.
!uk depth = depth_sum2tok0 + dz(j,1,i)

            ! simple wave generation:
            ! Calculate significant wave heigth and zero-crossing period from
            ! the equilibrium formulation for fully developed waves based on
            ! the JONSWAP spectrum (van Rijn (1993), p. 331).
            U  = c4*(wind**c5)      ! adjusted wind speed (m/s) applying empirical constants
            Hs = c2*(U**2)/g        ! significant wave hight (m) applying empirical constants
            Tz = c3*U/g             ! zero-crossing period (s) applying empirical constants
            ! limit wave heigth and period:
            ! For wave hight, first ensure that it is larger than a minimum value
            ! H0(m) (tuning value). This ensures a background hydrodynamic
            ! activity that can generate SPM concentrations even in windless
            ! conditions, and that accounts for e.g. far-field waves travelling
            ! into the area and/or tidal currents:
            Hs = max(H0,Hs)         ! (m)
            ! Then, take account of wave breaking in relation to local water depth:
            Hs = min(Hs,0.4*depth)  ! (m)
            ! For (zero-crossing) wave period, prescribe a minimum of Tz0(s)
            ! (e.g. 5s assumed to be a reasonable minimum for coastal conditions
            ! with low wind speed). Effectively, this rule keeps the orbital
            ! velocity calculation finite:
            Tz = max(Tz0,Tz)        ! (s)

            ! shear stress calculation
            ! Calculate wave-orbital velocity amplitude at the sea bed using
            ! linear wave theory:
            uw = pi*Hs/(Tz*sinh(2*pi/Tz*sqrt(depth/g))) ! (m/s) JM linear wave theory
            ! Calculate wave-induced bed-shear stress (kg m-1 s-2): (fw: wave-friction factor)
            tau = 0.5*fw*rho(j,k0,i)*(uw**2)            ! JM 0.5*fw*rho*u**2
            ! Correction, because ECOHAM "rho" is (kg/m3/1000) !!!
            tau = tau*1000. ! (kg m-1 s-2)
!print*,ro(iz1),rho,wind
!pause

            do it = 1, n_step_silt
               ! update concentration
               ! Calculate the SPM concentration at the surface that would have occured
               ! if the suspended material present at the previous time step would be
               ! allowed to settle using a very simple decay formulation.
               ! The decay time scale is defined as a function of the surface
               ! concentration to simulate that coarse fractions settle faster than
               ! the fine fractions:
               ! kt0 - ensuring background concentration
               ! kt1 - proportionality constant to set the concentration dependence (kg/m3!!)
!uk kt0 und kt1 haben unterschiedliche Einheiten
!uk kommentieren
               k_timelag = max(ZERO, kt0/secs_per_day + kt1/secs_per_day*Silt(j,1,i)/1000.)
               ! decreasing surface concentration due to settling:
               Silt_settling = Silt(j,1,i)*exp(-k_timelag*dt_silt)  ! timestep in seconds!
               ! Calculate an SPM surface concentration proportional to the shear
               ! stress (stirring controlled concentration). The magnitude of the SPM
               ! concentration is proportional to the applied shear stress:
               !depthfact = max(ZERO,1.86-0.039*depth)   !depth dependent proportionality parameter
               !depthfact = max(ZERO,1.25-0.011*depth)   !depth dependent proportionality parameter
               depthfact = 20.0/(20.0+depth)             !depth dependent proportionality parameter
               !depthfact = 1.0 ! MK
               Silt_stirring = depthfact * max(p_silt_min, p_silt(j,i)) * tau !(g/m3)

! if(i==ii .and. j==jj)then
!  print*,'=================================================='
!  print*,'Silt_settling:',Silt_settling,Silt(j,1,i),k_timelag,exp(-k_timelag*dt_silt)  ! timestep in seconds!
!  print*,'Silt_stirring:',Silt_stirring,depthfact,p_silt_min,p_silt(j,i),tau
! endif

               ! Take the new SPM surface concentration as the maximum of the settling
               ! and stirring concentration:
               Silt_surf = max(Silt_settling,Silt_stirring)
               ! create linear depth dependence
               ! Set the SPM bottom concentration to X-times the surface concentration:
               !Asummption in lowest layer silt is factor 2 higher than in surface layer
               !Silt_bot = 2.0*Silt_surf ! X = 2.0; was 10.0
               Silt_bot = 1.0*Silt_surf ! X = 2.0; was 10.0
               ! Calculate the gradient in respective to instantaneous water depth and
               ! the heights of bottom and surface cells, respectively:
               Silt_grad = (Silt_bot-Silt_surf)/(depth-0.5*(dz(j,k0,i)+dz(j,1,i)))
               ! Set the concentrations at each depth level
               Silt(j, 1,i) = Silt_surf
               Silt(j,k0,i) = Silt_bot
               do k = k0-1, 2, -1
                  Silt(j,k,i) = Silt(j,k+1,i)-Silt_grad*0.5*(dz(j,k+1,i)+dz(j,k,i))
               end do
            enddo

!       ! add temperature-dependence to simulate higher concentrations beneath thermocline
!       do n=1,iz0-1
!          deltatemp=-(ETW(n)-ETW(iz0))
!          strat_thresh=-(ETW(iz1)-ETW(iz0))-0.2
!          if (strat_thresh>0.3 .and. deltatemp>strat_thresh) Silt(n)=strat_fact*Silt(n)
!       end do

! if(i==ii.and.j==jj) then
! print*,'================================================='
! print*,' wind=',u10(j,i),' tau=',tau,' p_silt=',p_silt(j,i),' depthfact=',depthfact
! print*,' silt_surf=',Silt(j,1,i),' silt_min=',p_silt_min,' silt_dyn=',p_silt(j,i)*tau
! !stop
! endif

        end if
     end do
   end do

   if (ierr/=0) then
      write( error_message, '("[",i2.2,"] ERROR - calc_silt: ",i3)') myID, ierr
      call stop_ecoham(ierr, error_message);
   end if
   return

   end subroutine calc_silt

!=======================================================================
! !INTERFACE:
   subroutine close_silt(ierr)
#define SUBROUTINE_NAME 'close_silt'
!
! !DESCRIPTION:
!
! !USES:
   implicit none
!
! !INPUT PARAMETERS:
   integer, intent(inout)  :: ierr
!
!-----------------------------------------------------------------------
#include "call-trace.inc"

   close(silt_unit)

   if (ierr/=0) then
      write( error_message, '("[",i2.2,"] ERROR - close_silt: ",i3)') myID, ierr
      call stop_ecoham(ierr, error_message);
   end if
   return

   end subroutine close_silt

!=======================================================================
# endif
   end module mod_silt

!=======================================================================
