!
! !MODULE: hydro.f90 --- advection and diffusion of tracers
!
! !INTERFACE:
   MODULE mod_hydro
!
! !DESCRIPTION:
! This module is the container for algotrithm for advection and diffusion of tracers
!
! IO - UNITS are defined in eco_common.f90 !!
!--------------------------------------------
!
! !USES:
   use mod_common
   use mod_grid
#ifdef MPI
   use mod_mpi_parallel
#endif
   implicit none
!
!  default: all is private.
   private
! !PUBLIC MEMBER FUNCTIONS:
   public init_hydro, get_hydro, read_hydro
   public update_hydro, do_hydro, hyd_up, close_hydro
!
! !PUBLIC VARIABLES:
   character(len=99), public :: hydro_dir, hydro_suffix
   integer     , public :: hydrotimestep ! time step (h) for hydro-forcing
   logical     , public :: advec, dif_hor, dif_ver, dyn_hor
   real        , public :: hev
   real,    dimension(:,:,:), allocatable, public :: temp  ! temperature (degC)
   real,    dimension(:,:,:), allocatable, public :: salt  ! salinity    (g/kg)
   real,    dimension(:,:,:), allocatable, public :: rho   ! density     (kg/m3)
   real,    dimension(:,:,:), allocatable, public :: pres  ! pressure    ( ?  )
   real,    dimension(:,:,:), allocatable, public :: ax    ! horizontal diffusion coeff. (u-variance) (?)
   real,    dimension(:,:,:), allocatable, public :: ay    ! horizontal diffusion coeff. (v-variance) (?)
   real,    dimension(:,:,:), allocatable, public :: vdc   ! vertical diffusion coefficient (m2/d)
   real,    dimension(:,:,:), pointer,     public :: tu    ! zonal transport velocity (eastward direction positive) (m2/d)
   real,    dimension(:,:,:), pointer,     public :: tv    ! meridional transport velocity (northward direction positive) (m2/d)
   real,    dimension(:,:,:), pointer,     public :: tw    ! vertical transport velocity (downward direction positive) (m2/d)
   real,    dimension(:,:)  , pointer,     public :: freshwater
   real,    dimension(:,:)  , pointer,     public :: zeta
#ifdef debug_HAMSOM_forcing
   real,    dimension(:,:,:), pointer,     public :: tu_in    ! zonal transport velocity (eastward direction positive) (m2/d)
   real,    dimension(:,:,:), pointer,     public :: tv_in    ! meridional transport velocity (northward direction positive) (m2/d)
#endif
!
! !LOCAL VARIABLES:
#if defined makehyd_Input
#if defined HydroLittleEndR8
   character(len=*), parameter :: hydro_endian='LITTLE_ENDIAN'
   integer,          parameter :: hydro_realkind = 8   != ibyte_per_real
#else
   character(len=*), parameter :: hydro_endian='BIG_ENDIAN'
   integer,          parameter :: hydro_realkind = 8   != ibyte_per_real
#endif
#else
#if defined HydroLittleEndR8
   character(len=*), parameter :: hydro_endian='LITTLE_ENDIAN'
   integer,          parameter :: hydro_realkind = 8   != ibyte_per_real !jp
#else
   ! standard case used in ECOHAM5
   character(len=*), parameter :: hydro_endian='LITTLE_ENDIAN'
   integer,          parameter :: hydro_realkind = 4   != ibyte_per_real
#endif
#endif
   real, dimension(:,:,:), pointer :: temp_new, temp_old
   real, dimension(:,:,:), pointer :: salt_new, salt_old
   real, dimension(:,  :), pointer :: vol_new, vol_old
#ifdef makehyd_Input
   real, dimension(:,:,:), pointer :: vdc_new, vdc_old
#else
   real, dimension(:,:,:), pointer :: av_new, av_old
   real, dimension(:,:,:), pointer :: sm_new, sm_old
#endif
   real, dimension(:,:,:), pointer :: ax_new, ax_old
   real, dimension(:,:,:), pointer :: ay_new, ay_old
   real, dimension(:,:,:), pointer :: tu_new, tu_old
   real, dimension(:,:,:), pointer :: tv_new, tv_old
   real, dimension(:,:,:), pointer :: du_new, du_old
   real, dimension(:,:,:), pointer :: dv_new, dv_old
#ifdef advection_acceleration
   real, dimension(:,:,:,:), allocatable :: ustar, vstar, wstar
#endif
   real, parameter       :: vdc_max = 8.e-2
   real                  :: hydro_t1, hydro_t2
   integer               :: irec_hydro_old
   logical               :: update_velocities = .true.
   logical               :: lsm_include       = .false.
!
   integer :: ii=9,  jj=59, kk=1
   !integer :: ii=28, jj=23 ! Rhine @ NS20C
!
!=======================================================================

   contains

!=======================================================================
!
! !INTERFACE:
   subroutine init_hydro(ierr)
#define SUBROUTINE_NAME 'init_hydro'
!
! !DESCRIPTION:
!
! !USES:
   use mod_var, only: n_st_var
   implicit none
!
! !OUTPUT PARAMETERS:
   integer, intent(inout)  :: ierr
!
! !LOCAL VARIABLES:
   integer                 :: i, ios, UN
   character*99            :: fmtstr!,txt
   logical :: lexist
!
   namelist /hydro_nml/hydro_dir, hydro_suffix, hydrotimestep,  &
                      advec, dif_hor, dif_ver, dyn_hor, hev,    &
                      lfreshwater, lsm_include
!
!--------------------------------------------------------------
#include "call-trace.inc"

   if (myID==master) call write_log_message(' INITIALIZING hydrodynamics ')

! #if !defined module_restoring && defined exclude_restoring_cells_hydro
!    if (myID==master) then
!       error_message = 'ERROR: You may not specify -Dexclude_restoring_cells_hydro' &
!                       //' without Flag -Dmodule_restoring!'
!       call stop_ecoham(ierr, msg=error_message)
!    endif
! #endif

! allocate arrays defiend for domian size
   allocate( zeta(jmax,     iStartD:iEndD) )
   allocate( temp(jmax,kmax,iStartD:iEndD) )
   allocate( salt(jmax,kmax,iStartD:iEndD) )
   allocate(  rho(jmax,kmax,iStartD:iEndD) )
   allocate( pres(jmax,kmax,iStartD:iEndD) )
   allocate(  vdc(jmax,kmax,iStartD:iEndD) ) 
   allocate(   ax(jmax,kmax,iStartD:iEndD) )
   allocate(   ay(jmax,kmax,iStartD:iEndD) )
   allocate(   tu(jmax,kmax,iStartD:iEndD) )
   allocate(   tv(jmax,kmax,iStartD:iEndD) )
   allocate(   tw(jmax,0:kmax,iStartD:iEndD) )
#ifdef debug_HAMSOM_forcing
   allocate(   tu_in(jmax,kmax,iStartD:iEndD) )
   allocate(   tv_in(jmax,kmax,iStartD:iEndD) )
#endif
#ifdef advection_acceleration
   allocate( ustar(-1:1,jmax,kmax,iStartD:iEndD) )
   allocate( vstar(-1:1,jmax,kmax,iStartD:iEndD) )
   allocate( wstar(-1:1,jmax,kmax,iStartD:iEndD) )
#endif

   allocate( temp_old(jmax,kmax,iStartD:iEndD) )
   allocate( temp_new(jmax,kmax,iStartD:iEndD) )
   allocate( salt_old(jmax,kmax,iStartD:iEndD) )
   allocate( salt_new(jmax,kmax,iStartD:iEndD) )
   allocate(  vol_old(jmax     ,iStartD:iEndD) )
   allocate(  vol_new(jmax     ,iStartD:iEndD) )
#ifdef makehyd_Input
   allocate(  vdc_old(jmax,kmax,iStartD:iEndD) )
   allocate(  vdc_new(jmax,kmax,iStartD:iEndD) )
#else
   allocate(   av_old(jmax,kmax,iStartD:iEndD) )
   allocate(   av_new(jmax,kmax,iStartD:iEndD) )
#endif
   allocate(   ax_old(jmax,kmax,iStartD:iEndD) )
   allocate(   ax_new(jmax,kmax,iStartD:iEndD) )
   allocate(   ay_old(jmax,kmax,iStartD:iEndD) )
   allocate(   ay_new(jmax,kmax,iStartD:iEndD) )

! set defaults
   advec   = .true.
   dif_hor = .true.
   dif_ver = .true.
   dyn_hor = .true.
   tu = 0.0
   tv = 0.0
   tw = 0.0
   vdc = 0.0 !jp
   vol_old = vol(:,1,:)
   vol_new = vol(:,1,:)
#ifdef debug_HAMSOM_forcing
   tu_in = 0.0
   tv_in = 0.0
   open(debugUnit,file='debug.dat',form='formatted',status='replace')
!    write(debugUnit,'(4a3,9a20)') 'i','j','k', ' ExistZ(j,k,i)', 'wIntegral(j)',   &
!                   'rdln(i)', 'AvrU(j,k,i  )',      &
!                   'rdln(i)', 'AvrU(j-1,k,i)',      &
!                   'dvo',     'AvrV(j,k,i-1)',      &
!                   'dvu',     'AvrV(j,  k,i)'
   write(debugUnit,'(4a3,9a20)') 'i','j','k', ' ExistZ(j,k,i)', &
                  'wIntegral(j)','AvrCumW(j,k,i)','AvrFW(j,i)',  &
                  'AvrU(j,k,i  )', 'AvrU(j-1,k,i)',              &
                  'AvrV(j,k,i-1)', 'AvrV(j,  k,i)'
#endif
#ifdef advection_acceleration
   ustar =0.0
   vstar =0.0
   wstar =0.0
#endif

! open & read namelist settings
!--------------------------------------------------------------
   open(nml_unit,file=trim(settings_file),action='read',status='old')
   read(nml_unit,nml=hydro_nml)
   close(nml_unit)

#ifndef makehyd_Input
   if (lfreshwater) then
      allocate( freshwater(jmax,iStartD:iEndD) )
      freshwater = 0.0
    endif
#endif

   if (dif_hor) then
      do i = iStartD, iEndD
         ax(:,:,i) = hev*secs_per_day/dxx2(i)           ! 1/d
      enddo
      ay = hev*secs_per_day/dyy2                        ! 1/d
   endif

   if (offline) then
      UN = temp_unit
      filename = trim(hydro_dir)//'temp'//trim(hydro_suffix)
      open(UN, file = trim(filename),       &
          status = 'old',                   &
          action = 'read',                  &
          form   = 'unformatted',           &
          convert= trim(hydro_endian),      &
          access = 'direct',                &
          recl   = hydro_realkind*iiwet,    &
          iostat = ios, err = 9000)
      if (myID == master) then
         fmtstr = '(3x,"temperature",9x,"(unit=",i3.0,") from file: ",a)'
         write(long_msg,fmtstr) temp_unit, trim(filename)
         call write_log_message(long_msg); long_msg = ''
      endif

      UN = salt_unit
      filename = trim(hydro_dir)//'salt'//trim(hydro_suffix)
      open(UN, file = trim(filename),       &
          status = 'old',                   &
          action = 'read',                  &
          form   = 'unformatted',           &
          convert= trim(hydro_endian),      &
          access = 'direct',                &
          recl   = hydro_realkind*iiwet,    &
          iostat = ios, err = 9000)
      if (myID == master) then
         fmtstr = '(3x,"salinity",12x,"(unit=",i3.0,") from file: ",a)'
         write(long_msg,fmtstr) UN, trim(filename)
         call write_log_message(long_msg); long_msg = ''
      endif

#ifdef makehyd_Input
      UN = vdc_unit
      filename = trim(hydro_dir)//'vdc'//trim(hydro_suffix)
      open(UN, file = trim(filename),       &
          status = 'old',                   &
          action = 'read',                  &
          form   = 'unformatted',           &
          convert= trim(hydro_endian),      &
          access = 'direct',                &
          recl   = hydro_realkind*iiwet,    &
          iostat = ios, err = 9000)
      if (myID == master) then
         fmtstr = '(3x,"vert. diffusion",5x,"(unit=",i3.0,") from file: ",a)'
         write(long_msg,fmtstr) UN, trim(filename)
         call write_log_message(long_msg); long_msg = ''
      endif

      UN = vol_unit
      filename = trim(hydro_dir)//'vol'//trim(hydro_suffix)
      open(UN, file = trim(filename),       &
          status = 'old',                   &
          action = 'read',                  &
          form   = 'unformatted',           &
          convert= trim(hydro_endian),      &
          access = 'direct',                &
          recl   = hydro_realkind*iiwet,    &
          iostat = ios, err = 9000)
      if (myID == master) then
         fmtstr = '(3x,"cell volumes",8x,"(unit=",i3.0,") from file: ",a)'
         write(long_msg,fmtstr) UN, trim(filename)
         call write_log_message(long_msg); long_msg = ''
      endif

#else

      UN = av_unit
      filename = trim(hydro_dir)//'av'//trim(hydro_suffix)
      open(UN, file = trim(filename),       &
          status = 'old',                   &
          action = 'read',                  &
          form   = 'unformatted',           &
          convert= trim(hydro_endian),      &
          access = 'direct',                &
          recl   = hydro_realkind*iiwet,    &
          iostat = ios, err = 9000)
      if (myID == master) then
         fmtstr = '(3x,"vert. diffusion",5x,"(unit=",i3.0,") from file: ",a)'
         write(long_msg,fmtstr) UN, trim(filename)
         call write_log_message(long_msg); long_msg = ''
      endif

      if (lsm_include) then
         allocate(   sm_old(jmax,kmax,iStartD:iEndD) )
         allocate(   sm_new(jmax,kmax,iStartD:iEndD) )

         UN = sm_unit
         filename = trim(hydro_dir)//'sm'//trim(hydro_suffix)
         open(UN, file = trim(filename),       &
            status = 'old',                   &
            action = 'read',                  &
            form   = 'unformatted',           &
            convert= trim(hydro_endian),      &
            access = 'direct',                &
            recl   = hydro_realkind*iiwet,    &
            iostat = ios, err = 9000)
         if (myID == master) then
            fmtstr = '(3x,"Schmidt-Number",6x,"(unit=",i3.0,") from file: ",a)'
            write(long_msg,fmtstr) UN, trim(filename)
            call write_log_message(long_msg); long_msg = ''
         endif
      else
         if (myID == master) then
            fmtstr = '(3x,"Schmidt-Number will not be applied!")'
            write(long_msg,fmtstr) !UN, trim(filename)
            call write_log_message(long_msg); long_msg = ''
         endif
      endif

      UN = z_unit
      filename = trim(hydro_dir)//'zeta'//trim(hydro_suffix)
      open(UN, file = trim(filename),       &
          status = 'old',                   &
          action = 'read',                  &
          form   = 'unformatted',           &
          convert= trim(hydro_endian),      &
          access = 'direct',                &
          recl   = hydro_realkind*iiwet2,   &
          iostat = ios, err = 9000)
      if (myID == master) then
         fmtstr = '(3x,"water level",9x,"(unit=",i3.0,") from file: ",a)'
         write(long_msg,fmtstr) UN, trim(filename)
         call write_log_message(long_msg); long_msg = ''
      endif
#endif

      UN = tu_unit
      filename = trim(hydro_dir)//'tu'//trim(hydro_suffix)
      open(UN, file = trim(filename),       &
          status = 'old',                   &
          action = 'read',                  &
          form   = 'unformatted',           &
          convert= trim(hydro_endian),      &
          access = 'direct',                &
          recl   = hydro_realkind*iiwet,    &
          iostat = ios, err = 9000)
      if (myID == master) then
         fmtstr = '(3x,"u-velocities",8x,"(unit=",i3.0,") from file: ",a)'
         write(long_msg,fmtstr) UN, trim(filename)
         call write_log_message(long_msg); long_msg = ''
      endif

      UN = tv_unit
      filename = trim(hydro_dir)//'tv'//trim(hydro_suffix)
      open(UN, file = trim(filename),       &
          status = 'old',                   &
          action = 'read',                  &
          form   = 'unformatted',           &
          convert= trim(hydro_endian),      &
          access = 'direct',                &
          recl   = hydro_realkind*iiwet,    &
          iostat = ios, err = 9000)
      if (myID == master) then
         fmtstr = '(3x,"v-velocities",8x,"(unit=",i3.0,") from file: ",a)'
         write(long_msg,fmtstr) UN, trim(filename)
         call write_log_message(long_msg); long_msg = ''
      endif

#ifdef makehyd_Input
      UN = tw_unit
      filename = trim(hydro_dir)//'tw'//trim(hydro_suffix)
      open(UN, file = trim(filename),       &
          status = 'old',                   &
          action = 'read',                  &
          form   = 'unformatted',           &
          convert= trim(hydro_endian),      &
          access = 'direct',                &
          recl   = hydro_realkind*iiwet,    &
          iostat = ios, err = 9000)
      if (myID == master) then
         fmtstr = '(3x,"w-velocities",8x,"(unit=",i3.0,") from file: ",a)'
         write(long_msg,fmtstr) UN, trim(filename)
         call write_log_message(long_msg); long_msg = ''
      endif
#endif

      if (dyn_hor) then
         UN = ax_unit
         filename = trim(hydro_dir)//'ax'//trim(hydro_suffix)
         open(UN, file = trim(filename),      &
            status = 'old',                   &
            action = 'read',                  &
            form   = 'unformatted',           &
            convert= trim(hydro_endian),      &
            access = 'direct',                &
            recl   = hydro_realkind*iiwet,    &
            iostat = ios, err = 9000)
         if (myID == master) then
            fmtstr = '(3x,"ax",18x,"(unit=",i3.0,") from file: ",a)'
            write(long_msg,fmtstr) UN, trim(filename)
            call write_log_message(long_msg); long_msg = ''
         endif

         UN = ay_unit
         filename = trim(hydro_dir)//'ay'//trim(hydro_suffix)
         open(UN, file = trim(filename),      &
            status = 'old',                   &
            action = 'read',                  &
            form   = 'unformatted',           &
            convert= trim(hydro_endian),      &
            access = 'direct',                &
            recl   = hydro_realkind*iiwet,    &
            iostat = ios, err = 9000)
         if (myID == master) then
            fmtstr = '(3x,"ay",18x,"(unit=",i3.0,") from file: ",a)'
            write(long_msg,fmtstr) UN, trim(filename)
            call write_log_message(long_msg); long_msg = ''
         endif
      endif

      irec_hydro_old = -1
!jpdeb print*,'hydro#106',day_of_year,hydro_t1,hydro_t2,irec1,irec2,' init_hydro' !jpdeb
      call read_hydro(ierr)

   else
      call stop_ecoham(1, msg='ERROR: online hydro not ready jet')
      !call init_hamsom
      !call update_hydro(day_of_year,ierr)
   endif

   if (ierr/=0) then
      write( error_message, '("[",i2.2,"] ERROR - init_hydro: ",i3)') myID, ierr
      call stop_ecoham(ierr, msg=error_message)
   end if
   return

9000 continue
   ierr = 1
   inquire (file=trim(filename), exist=lexist, iostat=ios)
   print*,'exist ',trim(filename),'?' ,lexist,ios
   print*,'open error ',UN,trim(filename),' ',trim(hydro_endian),hydro_realkind*iiwet,ios

   write(error_message,'("HYDRO: open error : ",i5,x,a,i5)') UN,trim(filename),ios
   call stop_ecoham(ierr, msg=error_message)

   end subroutine init_hydro

!=======================================================================
!
! !INTERFACE:
   subroutine get_hydro(ierr)
#define SUBROUTINE_NAME 'get_hydro'
!
! !DESCRIPTION:
!
! !USES:
   implicit none
!
! !OUTPUT PARAMETERS:
   integer, intent(inout)  :: ierr
!
! !LOCAL VARIABLES:
!
!--------------------------------------------------------------
#include "call-trace.inc"

   if (offline) then
      call read_hydro(ierr)
   else
      call stop_ecoham(1, msg='ERROR: online hydro not ready jet')
      !call do_hamsom
      !call update_hydro(day_of_year,ierr)
   endif

   if (ierr/=0) then
      write( error_message, '("[",i2.2,"] ERROR - get_hydro: ",i3)') myID, ierr
      call stop_ecoham(ierr, msg=error_message)
   end if
   return

   end subroutine get_hydro
!
!=======================================================================
!
! !INTERFACE:
   subroutine read_hydro(ierr)
#define SUBROUTINE_NAME 'read_hydro'
!
! !DESCRIPTION:
!
! !USES:
   implicit none
!
! !OUTPUT PARAMETERS:
   integer, intent(inout) :: ierr
!
! !LOCAL VARIABLES:
   real, dimension(:,:,:), pointer :: dum3D
   real                   :: recc
   integer                :: i, irec1, irec2
#ifndef makehyd_Input
   real                   :: tw_
   integer                :: j, k, k0
   logical                :: lSaveInitialState = .true.
#else
   logical                :: lSaveInitialState = .false.
#endif
! #ifdef MPI
!    real, allocatable      :: buf(:,:,:)
! #endif
#ifdef debug_HAMSOM_forcing
   real                   :: wIntegral, dvu, dvo, PhiU, dPhi, rdln
#endif
!
!--------------------------------------------------------------
#include "call-trace.inc"

   recc  = day_of_year *24. /real(hydrotimestep)
   irec2 = nint(recc+1)
   if (lSaveInitialState) irec2 = irec2+1
   irec1 = max(1,irec2-1)

   if (irec2/=irec_hydro_old) then

      allocate( dum3D(jmax,kmax,iStartD:iEndD) )

      ! time
      if (lSaveInitialState) then
         hydro_t2 = (irec2-1) *hydrotimestep/24.
      else
         hydro_t2 = irec2     *hydrotimestep/24.
      endif
      hydro_t1 = hydro_t2 - hydrotimestep/24.

      if (irec_hydro_old <= 0) then
        call read_hydro_dataset3D( temp_unit, temp_old, irec1, ierr)
        call read_hydro_dataset3D( salt_unit, salt_old, irec1, ierr)
#ifdef makehyd_Input
        call read_hydro_dataset3D(  vdc_unit, vdc_old, irec1, ierr, scaling_factor=secs_per_day)
        call read_hydro_dataset3D(  vol_unit, dum3D, irec1, ierr)
        vol_old = dum3D(:,1,:)
#else
        ! initialize surface layer water level zeta (m) and calculate volumes of surface cells
        !allocate( zeta(jmax, iStartD:iEndD) )
        call read_hydro_dataset2D(   z_unit,  zeta, irec1, ierr)
        do i = max(iStartCalc,iStartD), min(iEndCalc,iEndD)
           do j = max(jStartCalc,1), min(jEndCalc,jmax)
              k0 = k_index(j,i)
              if (k0>0) then
                 if (k0==1) then
                     vol_old(j,i) = (dep_last(j,i)+zeta(j,i)) *area(j,i) ! m
                  else
                     vol_old(j,i) = (dzz(1) + zeta(j,i)) *area(j,i)        ! m
                 endif
!if ( i==53 .and. j==71 ) then
! print*,'hydro#549 ', k_index(j,i),  vol_old(j,i),  dep_last(j,i),  zeta(j,i),  vol_old(j,i)  /area(j,i),  dzz(1),irec1
! print*,'hydro#550 ', k_index(j,i-1),vol_old(j,i-1),dep_last(j,i-1),zeta(j,i-1),vol_old(j,i-1)/area(j,i-1),dzz(1)
! print*,'hydro#551 ', k_index(j,i-2),vol_old(j,i-2),dep_last(j,i-2),zeta(j,i-2),vol_old(j,i-2)/area(j,i-2),dzz(1)
! !stop
! endif
              endif
           enddo
        enddo
        !deallocate( zeta )

        call read_hydro_dataset3D(  av_unit, av_old, irec1, ierr)
        if (lsm_include) call read_hydro_dataset3D(  sm_unit, sm_old, irec1, ierr)
#endif

      else
        temp_old = temp_new
        salt_old = salt_new
        vol_old  = vol_new
#ifdef makehyd_Input
        vdc_old  = vdc_new
#else
        av_old  = av_new
        if (lsm_include) sm_old = sm_new
#endif
      endif
      call read_hydro_dataset3D( temp_unit, temp_new, irec2, ierr)                            ! temperature in degree C
      call read_hydro_dataset3D( salt_unit, salt_new, irec2, ierr)                            ! salinity in psu
#ifdef makehyd_Input
      call read_hydro_dataset3D( vdc_unit, vdc_new, irec2, ierr, scaling_factor=secs_per_day) ! vertical mixing in m2/d
      call read_hydro_dataset3D( vol_unit, dum3D,   irec2, ierr)                              ! grid-cell volumes in m3
      vol_new = dum3D(:,1,:)
      call read_hydro_dataset3D(  tu_unit, tu, irec1, ierr, scaling_factor=secs_per_day)      ! zonal transport in m2/d
      call read_hydro_dataset3D(  tv_unit, tv, irec1, ierr, scaling_factor=secs_per_day)      ! meridional transport in m2/d
      call read_hydro_dataset3D(  tw_unit, dum3D, irec1, ierr, scaling_factor=secs_per_day)   ! vertical transport in m2/d
      !tw(:,0:kmax-1,:) = dum3D(:,1:kmax,:)
      tw(:,1:kmax,:) = dum3D(:,1:kmax,:)
#else
      call read_hydro_dataset3D(  av_unit,  av_new, irec2, ierr)                              ! vertical mixing in m2/d
      if (lsm_include) call read_hydro_dataset3D(  sm_unit,  sm_new, irec2, ierr)             ! Schmidt-Number
#ifdef debug_HAMSOM_forcing
      call read_hydro_dataset3D(  tu_unit, tu_in, irec2, ierr, scaling_factor=1.)             ! u-component in m2/s
      dum3D = tu_in*secs_per_day
      tu_in = tu_in*real(ixistU)
#else
      call read_hydro_dataset3D(  tu_unit, dum3D, irec2, ierr, scaling_factor=secs_per_day)   ! u-component in m2/d
#endif
      tu = dum3D * ddlat(0) * real(ixistU)                                                    ! zonal transport in m3/d
#ifdef debug_HAMSOM_forcing
      call read_hydro_dataset3D(  tv_unit, tv_in, irec2, ierr, scaling_factor=1.)             ! v-component in m2/s
      dum3D = tv_in*secs_per_day
      tv_in = tv_in*real(ixistV)
#else
      call read_hydro_dataset3D(  tv_unit, dum3D, irec2, ierr, scaling_factor=secs_per_day)   ! v-component in m2/d
#endif
      do i = iStartD, iEndD
         tv(:,:,i) = dum3D(:,:,i) * ddlon(i+1) * real(ixistV(:,:,i))                          ! meridional transport in m3/d
      enddo
! reading tw directly is not recommended ! instead recalculate according to lateral transport divergences and the law of mass conservation
!       call read_hydro_dataset3D( tw_unit, dum3D, irec2, ierr, scaling_factor=secs_per_day)    ! w-velocity in m/d
!       do i = iStartD, iEndD
!         where(ixistZ(:,:,i)==1) tw(:,0:kmax-1,i) = -dum3D(:,1:kmax,i)*dxx(i)*dyy              ! vertical transport in m3/d
!       enddo

#ifdef debug_HAMSOM_forcing
      if (lfreshwater) then
         call read_freshwater(ierr)
      endif
#endif

      tw = 0.0
      do i = max(iStartCalc,iStartD+1), min(iEndCalc,iEndD)
         do j = max(jStartCalc,2), min(jEndCalc,jmax)
!      do i = max(2,iStartD+1), iEndD
!         do j = 2, jmax
            k0 = k_index(j,i)
            tw_ = 0.0
#ifdef debug_HAMSOM_forcing
               wIntegral = 0.0
#endif
            do k = k0, 1, -1
               tw_ = tw_ +tu(j-1,k,i) -tu(j,k,i) +tv(j,k,i) -tv(j,k,i-1)              ! vertical transport in m3/d
               tw(j,k-1,i) = real(-tw_)
#ifdef debug_HAMSOM_forcing
          if (i==ii .and. j==jj) then
             if (k==k0) write(debugUnit,'(''----------------'',i5,''----------------'')') irec2
! StandardDegree = 2.0 * pi * EarthRadius / 360.0
! EarthOmega     = 2.0 * pi / EarthPeriod
! deltaX0        = dLam * StandardDegree
! deltaY0        = dPhi * StandardDegree
!deltaX0 = dLam * 2.0 * pi * EarthRadius / 360.0
!deltaY0 = dPhi * 2.0 * pi * EarthRadius / 360.0
! Phi0           = Deg2Rad(Phi0)
! Lam0           = Deg2Rad(Lam0)
! dPhi           = Deg2Rad(dPhi)
! dLam           = Deg2Rad(dLam)
! dl             = deltaY0
! dlr            = 1.0 / dl
!   PhiU       = Phi0 - (i-1) * dPhi
!   PhiVO      = PhiU +  0.5  * dPhi
!   PhiVU      = PhiU -  0.5  * dPhi
!   cosPhiU    = cos(PhiU)
!   cosPhiVO   = cos(PhiVO)
!   cosPhiVU   = cos(PhiVU)
!   dlvo(i)    = cosPhiVO/ cosPhiU
!   dlvu(i)    = cosPhiVU/ cosPhiU
!   dln (i)    = deltaX0 * cosPhiU
!   rdln(i)    = 1.0     / dln(i)
               PhiU = (xlat0 + real(imax-i) * delta_lat) *pi /180.
               dPhi = delta_lat *pi /180.
               dvo =  (cos(PhiU+0.5*dPhi)/ cos(PhiU)) /(delta_lat* earth_umf / .360) ! dvo = dlvo(i) * dlr
               dvu =  (cos(PhiU-0.5*dPhi)/ cos(PhiU)) /(delta_lat* earth_umf / .360) ! dvu = dlvu(i) * dlr
               !rdln = 1.0 / (delta_lon * 2.0 * pi * earth_rad / 360.0 * cos(PhiU))
               rdln = 1.0 / (delta_lon * earth_umf / .360 * cos(PhiU))
               wIntegral =  wIntegral + ixistz_master(j,k,i) *                  &
                           ( rdln * tu_in(j-1,k,i) - rdln * tu_in(j,k,i)        &
                           + dvu  * tv_in(j,k,i)   - dvo  * tv_in(j,k,i-1) )
!          if (i==ii .and. j==jj) then
!             dvo = dlvo(i) * dlr
!             dvu = dlvu(i) * dlr
!             wIntegral(j) = wIntegral(j) + ExistZ(j,k,i) *                     &
!                           ( -(  rdln(i) * real(AvrU(j,k,i  ),RealLength)      &
!                               - rdln(i) * real(AvrU(j-1,k,i),RealLength) )    &
!                             -(  dvo     * real(AvrV(j,k,i-1),RealLength)      &
!                               - dvu     * real(AvrV(j,  k,i),RealLength) ))
!              write(debugUnit,'(4i3,9e20.10)') i,j,k, ExistZ(j,k,i), wIntegral(j),   &
!                                rdln(i), real(AvrU(j,k,i  ),RealLength),      &
!                                rdln(i), real(AvrU(j-1,k,i),RealLength),      &
!                                dvo,     real(AvrV(j,k,i-1),RealLength),      &
!                                dvu,     real(AvrV(j,  k,i),RealLength)
             !write(debugUnit,'(4i3,9e20.10)') i,j,k, ixistz_master(j,k,i), tw_/secs_per_day, &
!              write(debugUnit,'(4i3,9e20.10)') i,j,k, ixistz_master(j,k,i), wIntegral, &
!                                rdln, tu_in(j,k,i  ), rdln,   tu_in(j-1,k,i),   &
!                                dvo,  tv_in(j,k,i-1), dvu,    tv_in(j,  k,i)
             write(debugUnit,'(4i3,9e20.10)') i,j,k, ixistz_master(j,k,i), &
                               wIntegral, wIntegral *area(j,i), freshwater(j,i)/86400.,  &
                               tu_in(j,k,i  ), tu_in(j-1,k,i),   &
                               tv_in(j,k,i-1), tv_in(j,  k,i)
!              if (k==1) write(debugUnit,'(''------------------------------------'')')
          endif
#endif
            enddo
         enddo
      enddo
      
      dAdh(:,:) = -tw(:,0,:)

if (.false.) then

      if (lfreshwater) then
         dVol = 0.0
         call read_hydro_dataset2D(   z_unit,  zeta, irec2, ierr)
         do i = max(iStartCalc,iStartD), min(iEndCalc,iEndD)
            do j = max(jStartCalc,1), min(jEndCalc,jmax)
               k0 = k_index(j,i)
               if (k0>0) then
                  if (k0==1) then
                     !vol_new(j,i) = (dep_last(j,i)+zeta(j,i)) *area(j,i) ! m
                     dVol(j,i) = ((dep_last(j,i)+zeta(j,i)) *area(j,i) - vol_old(j,i) ) &
                                                  *(24./real(hydrotimestep)) ! m
                  else
                     !vol_new(j,i) = (dzz(1) + zeta(j,i)) *area(j,i)      ! m
                     dVol(j,i) = ((dzz(1)+zeta(j,i)) *area(j,i) - vol_old(j,i) )        &
                                                  *(24./real(hydrotimestep)) ! m
                  endif
               endif
            enddo
         enddo
         !dVol = (vol_new - vol_old) *(24./real(hydrotimestep))              ! m3 d-1

         call read_freshwater(ierr)
         i=51;j=73;
!print*,'hydro#630 ',i,j, -tw(j,0,i), dVol(j,i), freshwater(j,i), tw_

        do i = max(iStartCalc,iStartD+1), min(iEndCalc,iEndD)
            do j = max(jStartCalc,2), min(jEndCalc,jmax)
               if ( ixistK(j,i)==0 ) cycle
               tw_ = freshwater(j,i)
               freshwater(j,i) = dVol(j,i) +tw(j,0,i)

!if ( i==51 .and. j==73 ) print*,'hydro#640 ', -tw(j,0,i), dVol(j,i), freshwater(j,i), tw_, (freshwater(j,i)-tw_)/area(j,i)

               tw(j,0,i) = tw(j,0,i) - freshwater(j,i)
            enddo
         enddo
     endif
     dRiv = freshwater

      !where (ixistK == 1) tw(:,0,:) = vol_old - vol_new
      !vol_new(:,:) = vol_old(:,:) + dVol(:,:)
      !vol_new(:,:) = vol_old(:,:) - tw(:,0,:)

else

      if (lfreshwater) then
         call read_freshwater(ierr)
         !write(*,'(''river#146 (i=28,j=23)'',e20.10)') freshwater(23,28)!/secs_per_day!. * dt
         dRiv = freshwater

         do i = max(iStartCalc,iStartD+1), min(iEndCalc,iEndD)
            do j = max(jStartCalc,2), min(jEndCalc,jmax)
               tw(j,0,i) = tw(j,0,i) - dRiv(j,i)              ! m3 d-1
            enddo
         enddo
      endif

endif
    dVol(:,:) = - tw(:,0,:)
    vol_new(:,:) = vol_old(:,:) + dVol(:,:) *(real(hydrotimestep)/24.) ! m3 - hydrotimestep is givin in hours !

!if(iStartD<=51 .and. iEndD>=51 ) print*,'hydro#604 ',vol_new(73,51), vol_old(73,51), tw(73,0,51)
! if(myID==10) print*,'hydro#601 ',iEndCalc,  iEndD,    vol_new(73,50), vol_old(73,50), tw(73,0,50)
! if(myID==11) print*,'hydro#601 ',iStartD, iStartCalc, vol_new(73,50), vol_old(73,50), tw(73,0,50)
! #ifdef MPI
! print*,'hydro#604 call mpi_parallel_exchange_boundary'
!       call mpi_parallel_exchange_boundary('N',tw(:,0:kmax,:), iStartD, iEndD, kMax+1, ierr_mpi)
!       ierr = ierr + ierr_mpi
!      ! check whether data exchange was fine
!      if (ierr/=0) then
!         write(error_message,'("ERROR - hydro#588: exchange boundary data failed")')
!         call stop_ecoham(ierr, error_message);
!      endif
! #endif
! if(myID==10) print*,'hydro#610 ',iEndCalc,  iEndD,    vol_new(73,50), vol_old(73,50), tw(73,0,50)
! if(myID==11) print*,'hydro#610 ',iStartD, iStartCalc, vol_new(73,50), vol_old(73,50), tw(73,0,50)

      !if (irec_hydro_old <= 0) then
      !   vol_old(:,:) = vol(:,:) - 0.5*tw(:,0,:)
      !endif
      !dVol(:,:) = - tw(:,0,:)
      !vol_new(:,:) = vol_old(:,:) + dVol(:,:)
      !vol_new(:,:) = vol_old(:,:) - tw(:,0,:)

!i=51;j=73;
!print*,'hydro#643',idd,vol_old(j,i),vol_new(j,i), vol_new(j,i)-vol_old(j,i), dVol(j,i), &
!                   dAdh(j,i)+dRiv(j,i),dAdh(j,i),dRiv(j,i), &
!                   dVol(j,i)/area(j,i),dAdh(j,i)/area(j,i),dRiv(j,i)/area(j,i)

! if(myID==10) print*,'hydro#620 ',iEndCalc,  iEndD,    vol_new(73,50), vol_old(73,50), tw(73,0,50)
! if(myID==11) print*,'hydro#620 ',iStartD, iStartCalc, vol_new(73,50), vol_old(73,50), tw(73,0,50)
! !if(iStartD<=51 .and. iEndD>=51 ) print*,'hydro#620 ',vol_new(73,51), vol_old(73,51), tw(73,0,51)
! #ifdef MPI
! print*,'hydro#624 call mpi_parallel_exchange_boundary'
!       allocate ( buf(jMax,1,iStartD:iEndD) )
!       buf(:,1,:) = vol_new(:,:)
!       call mpi_parallel_exchange_boundary('N',buf(:,1,:), iStartD, iEndD, 1, ierr_mpi)
!       ierr = ierr + ierr_mpi
!      ! check whether data exchange was fine
!      if (ierr/=0) then
!         write(error_message,'("ERROR - hydro#588: exchange boundary data failed")')
!         call stop_ecoham(ierr, error_message)
!      endif
!      vol_new = buf(:,1,:)
!      deallocate ( buf )
! #endif
! if(myID==10) print*,'hydro#640 ',iEndCalc,  iEndD,    vol_new(73,50), vol_old(73,50), tw(73,0,50)
! if(myID==11) print*,'hydro#640 ',iStartD, iStartCalc, vol_new(73,50), vol_old(73,50), tw(73,0,50)
#endif

      ! horizontal mixing in m2/d
      if (dyn_hor) then
         ! print*,'read ax'
         if (irec_hydro_old <= 0) then
           call read_hydro_dataset3D( ax_unit, ax_old, irec1, ierr, scaling_factor=secs_per_day)
           call read_hydro_dataset3D( ay_unit, ay_old, irec1, ierr, scaling_factor=(secs_per_day/dyy2))
           do i = iStartD, iEndD
              ax_old(:,:,i) = ax_old(:,:,i)/dxx2(i)           ! m2/d
           enddo
         else
           ax_old = ax_new
           ay_old = ay_new
         endif
         call read_hydro_dataset3D( ax_unit, ax_new, irec2, ierr, scaling_factor=secs_per_day)
         do i = iStartD, iEndD
            ax_new(:,:,i) = ax_new(:,:,i)/dxx2(i)           ! m2/d
         enddo
         ! print*,'read ay'
         call read_hydro_dataset3D( ay_unit, ay_new, irec2, ierr, scaling_factor=(secs_per_day/dyy2))
      endif

!   print*,'hydro#669',day_of_year,int((24./real(hydrotimestep))*day_of_year,kind=8),irec_hydro_old,irec1,irec2,dz(jj,kk,ii)

      update_velocities = .true.
      call update_hydro(ierr)
      update_velocities = .false.

      deallocate( dum3D )

      irec_hydro_old = irec2
   endif ! if(irec2/=irec_hydro_old)

   if (ierr/=0) then
      write( error_message, '("[",i2.2,"] ERROR - read_hydro: ",i3)') myID, ierr
      call stop_ecoham(ierr, msg=error_message)
   end if
   return

   end subroutine read_hydro
!
!=======================================================================
#ifndef makehyd_Input
! !INTERFACE:
   subroutine read_freshwater(ierr)
#define SUBROUTINE_NAME 'read_freshwater'
!
! !DESCRIPTION:
!
! !USES:
#ifdef module_rivers
   use mod_rivers, only : num_riv, ipos_riv, jpos_riv, fresh_amount
   use mod_rivers, only : init_rivers
#endif
   implicit none
!
! !OUTPUT PARAMETERS:
   integer, intent(inout)  :: ierr
!
! !LOCAL VARIABLES:
#ifdef module_rivers
   real,    parameter :: eps = 1.e-8
   integer            :: i, j, idd, ir
#endif
!
!--------------------------------------------------------------
#include "call-trace.inc"

   freshwater = zero
   dRiv       = zero

   !if (.true.) then

   ! get freshwater from HAMSOM output

   !else
#ifdef module_rivers
   ! get freshwater from rivers reading freshwater.direct
   if (.not. allocated(fresh_amount) ) then
      call init_rivers(ierr)
   endif

   idd = int(day_of_year+eps)+1
   !call read_fresh_amount(ierr)
   do ir = 1, num_riv
      i = ipos_riv(ir)
      j = jpos_riv(ir)
      if (i >= max(2,iStartD+1) .and. i <= iEndD) then
         dRiv(j,i)       = dRiv(j,i)       + fresh_amount(ir,idd)
         freshwater(j,i) = freshwater(j,i) + fresh_amount(ir,idd)
!if(i==51 .and. j==73) print*,'hydro#797 ',i,j,ir,idd,freshwater(j,i),fresh_amount(ir,idd)
      endif
   enddo
!    freshwater = 0.0
#endif
   !endif

   !i=51;j=73; print*,'hydro#811 ',i,j,ir,idd,freshwater(j,i)

   if (ierr/=0) then
      write( error_message, '("[",i2.2,"] ERROR - read_freshwater: ",i3)') myID, ierr
      call stop_ecoham(ierr, msg=error_message)
   end if
   return

   end subroutine read_freshwater
!
!=======================================================================
!
! !INTERFACE:
   subroutine read_hydro_dataset2D(unitnum, dataset, irec, ierr, scaling_factor, index_char)
#define SUBROUTINE_NAME 'read_hydro_dataset2D'
!
! !DESCRIPTION:
!
! !USES:
      implicit none
!
! !INPUT PARAMETERS:
      integer,          intent(in)           :: unitnum, irec
      real,             intent(in), optional :: scaling_factor
      character(len=1), intent(in), optional :: index_char
!
! !OUTPUT PARAMETERS:
      !real, dimension(:,:), pointer, intent(out)   :: dataset
      real, dimension(jmax,istartD:iendD), intent(out) :: dataset !jp
      integer,              intent(inout) :: ierr
!
! !LOCAL VARIABLES:
      real, dimension(jmax,imax)                   :: full2D
      real(kind=hydro_realkind), dimension(iiwet2) :: input2d
      integer                                      :: ios !,rl
      character*99 :: str
!
!--------------------------------------------------------------
#include "call-trace.inc"

      ios = 0

      if ( present( index_char) .and. index_char == 'v') ios = 0 !to get rid of warning

      dataset = fail

      read( unitnum, rec=irec, iostat=ios ) input2d

! print*,'hydro#592 ',unitnum, irec, ios
! inquire(unitnum,name=str,recl=rl)
! print*,'hydro#594 ',trim(str),rl, size(input2d), hydro_endian, hydro_realkind
! write(*,'("hydro#595 ",a,i4,5e20.10)') trim(str),irec,input2d(1:5)

      if (ios == 5002) then
         write( long_msg, '("[",i2.2,"] ERROR reading HYDRO unit, rec no: ", 2i4,i5)') myID, unitnum, irec
         write( str, '(" -- record ",i4," will be used instead")') irec-1
         long_msg = trim(long_msg)//trim(str)
         call write_log_message(long_msg); long_msg = ''
         read( unitnum, rec=irec-1, iostat=ios ) input2d
     end if

      if (ios /= 0) then
         write( long_msg, '("ERROR reading HYDRO: myID, unit, rec no: ", 2i4,i5)') myID, unitnum, irec
         call write_log_message(long_msg); long_msg = ''
         ierr = 1
      else
         if ( present( scaling_factor) ) then
            !input2d = input2d * scaling_factor
            full2D(:,:) = unpack( real(input2d)*scaling_factor, ixistz_master(:,1,:)==1,fail )
         else
            full2D(:,:) = unpack( real(input2d), ixistz_master(:,1,:)==1,fail )
         end if
         full2D(:,:) = unpack( real(input2d), ixistz_master(:,1,:)==1,fail )
         dataset(:,iStartD:iEndD) = full2D(:,iStartD:iEndD)
      endif

#ifdef debug_HAMSOM_forcing
select case (unitnum)
 case (120); str = 'temp'
 case (121); str = 'salt'
 case (122); str = 'tu'
 case (123); str = 'tv'
 case (126); str = 'zeta'
case default; str = '-'
end select

if (str /='-') then
write(*,'(10x,a4,''(irec) min/max :'',i6,2e20.10)')  trim(str), irec, minval(input2d), maxval(input2d)
endif
#endif

   if (ierr/=0) then
      write( error_message, '("[",i2.2,"] ERROR - read_hydro_dataset2D: ",i3)') myID, ierr
      call stop_ecoham(ierr, msg=error_message)
   end if
   return

   end subroutine read_hydro_dataset2D
#endif
!
!=======================================================================
!
! !INTERFACE:
   subroutine read_hydro_dataset3D(unitnum, dataset, irec, ierr, scaling_factor, index_char)
#define SUBROUTINE_NAME 'read_hydro_dataset3D'
!
! !DESCRIPTION:
!
! !USES:
      implicit none
!
! !INPUT PARAMETERS:
      integer,          intent(in)           :: unitnum, irec
      real,             intent(in), optional :: scaling_factor
      character(len=1), intent(in), optional :: index_char
!
! !OUTPUT PARAMETERS:
!      real, dimension(:,:,:), pointer, intent(out) :: dataset
      real, dimension(jmax,kmax,istartD:iendD), intent(out) :: dataset  !jp
      integer,             intent(inout) :: ierr
!
! !LOCAL VARIABLES:
      real, dimension(jmax,kmax,imax)             :: full3D
      real(kind=hydro_realkind), dimension(iiwet) :: input3d
      integer                                     :: ios !,rl
      character(len=99) :: str
!
!--------------------------------------------------------------
#include "call-trace.inc"

      ios = 0
!      allocate(dataset(jmax,kmax,iStartD:iEndD))
      if ( present( index_char) .and. index_char == 'v') ios = 0 !to get rid of warning
      dataset = fail
      read( unitnum, rec=irec, iostat=ios ) input3d
!print*,'hydro#652 ',unitnum, irec, ios
!inquire(unitnum,name=str,recl=rl)
!print*,'hydro#654 ',trim(str),rl, size(input3d), hydro_endian, hydro_realkind
!write(*,'("hydro#655 ",a,i4,5e20.10)') trim(str),irec,input3d(1:5)

      if (ios == 5002) then
         write( long_msg, '("[",i2.2,"] ERROR reading HYDRO unit, rec no: ", 2i4,i5)') myID, unitnum, irec
         write( str, '(" -- record ",i4," will be used instead")') irec-1
         long_msg = trim(long_msg)//trim(str)
         call write_log_message(long_msg); long_msg = ''
         read( unitnum, rec=irec-1, iostat=ios ) input3d
      end if

      if (ios /= 0) then
         write( long_msg, '("[",i2.2,"] ERROR reading HYDRO unit, rec no: ", 2i4,i5)') myID, unitnum, irec
         call write_log_message(long_msg); long_msg = ''
         ierr = 1
      else
         if ( present( scaling_factor) ) then
            !input3d = input3d * scaling_factor
            full3D(:,:,:) = unpack( real(input3d)*scaling_factor, ixistz_master(:,:,:)==1,fail )
         else
            full3D(:,:,:) = unpack( real(input3d), ixistz_master(:,:,:)==1,fail )
         end if
         dataset(:,:,iStartD:iEndD) = full3D(:,:,iStartD:iEndD)
      endif

#ifdef debug_HAMSOM_forcing
select case (unitnum)
 case (120); str = 'temp'
 case (121); str = 'salt'
 case (122); str = 'tu'
 case (123); str = 'tv'
 case (126); str = 'zeta'
case default; str = '-'
end select

if (str /='-') then
write(*,'(10x,a4,''(irec) min/max :'',i6,2e20.10)')  trim(str), irec, minval(input3d), maxval(input3d)
endif
#endif

   if (ierr/=0) then
      write( error_message, '("[",i2.2,"] ERROR - read_hydro_dataset3D: ",i3)') myID, ierr
      call stop_ecoham(ierr, msg=error_message)
   end if
   return

   end subroutine read_hydro_dataset3D

!=======================================================================
!
! !INTERFACE:
   subroutine update_hydro(ierr)
#define SUBROUTINE_NAME 'update_hydro'
!
! !DESCRIPTION:
!
! !USES:
   use mod_grid,   only : k_index
   use mod_utils , only : unesco
   implicit none
!
! !OUTPUT PARAMETERS:
   integer, intent(inout)  :: ierr
!
! !LOCAL VARIABLES:
   real                    :: td, alpha, beta
   integer                 :: i, j, k, im, ip, jm, jp, k0, kp, km
   real                    :: tu1, tu2, tv1, tv2, tw1, tw2
#ifndef makehyd_Input
   real                    :: av, sm
#endif
!   character(len=100) :: fmtstr
!
!--------------------------------------------------------------
#include "call-trace.inc"

   td = day_of_year

   !interpolation
   alpha = (td-hydro_t1)/(hydro_t2-hydro_t1)
   beta  = 1.-alpha
!jpdeb   print*,td,hydro_t1,hydro_t2,' update_hydro' !jpdeb
!write(*,'(a10,6f12.6,2e20.10)')'hydro#1037',day_of_year,td,hydro_t1,hydro_t2, &
!                              beta,alpha,dz(16,1,31),vol(16,1,31) !jpdeb

   temp =  alpha * temp_new + beta * temp_old   ! deg C
   salt =  alpha * salt_new + beta * salt_old   ! psu
   if (dyn_hor) then
      ax = (alpha * ax_new  + beta * ax_old)    ! m2/d
      ay = (alpha * ay_new  + beta * ay_old)    ! m2/d
   endif
   vol(:,1,:)  =  alpha * vol_new  + beta * vol_old    ! m3
   zeta    = 0.0
   if (isw_dim==3) then
      do i = max(iStartCalc,iStartD), min(iEndCalc,iEndD)
         do j = max(jStartCalc,1), min(jEndCalc,jmax)
            k0 = k_index(j,i)
            if (k0>0) then
               dz(j,1,i) = vol(j,1,i)/area(j,i)
               if (k0==1) then
                  zeta(j,i) = dz(j,1,i)-dep_last(j,i)
               else
                  zeta(j,i) = dz(j,1,i)-dzz(1)
               endif
               if ( dz(j,1,i) < 0.0 ) then
                  ierr = 1
                  write(error_message,'("[",i2.2,"] - cell running dry (i,j,dz): ",2i4,f15.10)') myID,i,j, dz(j,1,i)
                  print*,error_message
                  !call stop_ecoham(ierr, msg=error_message)
               endif
            endif
         enddo
      enddo
   endif

#ifdef makehyd_Input
   vdc  = (alpha * vdc_new  + beta * vdc_old)   ! m2/d
#else

   !if (isw_dim==3) then
   do i = max(iStartCalc,iStartD), min(iEndCalc,iEndD)
      do j = max(jStartCalc,1), min(jEndCalc,jmax)
         k0 = k_index(j,i)
         if (k0>0) then
            do k = 1,k0
               av  = alpha * av_new(j,k,i)  + beta * av_old(j,k,i)    ! m2/d
               if (lsm_include) then
                  sm  = alpha * sm_new(j,k,i)  + beta * sm_old(j,k,i)    !
               else
                  sm = 1.0
               endif
               vdc(j,k,i) = min(vdc_max, av /(1.e-15+sm) )*secs_per_day
            enddo
         endif
      enddo
   enddo
   !endif
#endif

   ! calculation of rho and pres should be done for the entire grid !!!!
   ! (otherwise full array vector operations may fail!)
   do i = iStartD, iEndD
      do j = 1, jmax
         k0 = k_index(j,i)
         do k = 1, k0
            rho (j,k,i) = unesco(temp(j,k,i),salt(j,k,i)) !calculate density (kg dm-3 !!!!)
            pres(j,k,i) = rho(j,k,i)/10.*tiestu(k) ! pres [bar]
            ! NOTE: in ECOHAM "rho" is kg/l = (kg/m3)/1000 !!!
         enddo
      enddo
   enddo

   if (isw_dim==3 .and. update_velocities) then
      do i = iStartCalc, iEndCalc
         do j = jStartCalc, jEndCalc
            im = max(1,i-1)
            jm = max(1,j-1)
            ip = min(imax,i)
            jp = min(jmax,j)
            k = 1
            k0 = k_index(j,i)
            if (k<=k0) then
               kp = k
               km = k-1
               tu1 = tu(jm,k,i) * real(ixistu(jm,k,i))
               tu2 = tu(jp,k,i) * real(ixistu(jp,k,i))
               tv1 = tv(j,k,im) * real(ixistv(j,k,im))
               tv2 = tv(j,k,ip) * real(ixistv(j,k,ip))
               tw1 = tw(j,km,i) * real(ixistw(j,km,i))
               tw2 = tw(j,kp,i) * real(ixistw(j,kp,i))
            endif
#ifdef advection_acceleration
            do k = 1, k0
               kp = k
               km = k-1
               tu1 = tu(jm,k,i) * real(ixistu(jm,k,i))
               tu2 = tu(jp,k,i) * real(ixistu(jp,k,i))
               tv1 = tv(j,k,im) * real(ixistv(j,k,im))
               tv2 = tv(j,k,ip) * real(ixistv(j,k,ip))
               tw1 = tw(j,km,i) * real(ixistw(j,km,i))
               tw2 = tw(j,kp,i) * real(ixistw(j,kp,i))

               if (tu1>0.0) then
                  ustar(-1,j,k,i) = tu1
               else
                  ustar(0,j,k,i) = tu1
               endif
               if (tu2<0.0) then
                  ustar(1,j,k,i) = -tu2
               else
                  ustar(0,j,k,i) = -tu2 + ustar(0,j,k,i)
               endif

               if (tv1>0.0) then
                  vstar(0,j,k,i) = -tv1
               else
                  vstar(-1,j,k,i) = -tv1
               endif
               if (tv2<0.0) then
                  vstar(0,j,k,i) = tv2 + vstar(0,j,k,i)
               else
                  vstar(1,j,k,i) = tv2
               endif

               if (tw1>0.0) then
                  wstar(-1,j,k,i) = tw1
               else
                  wstar(0,j,k,i) = tw1
               endif
               if (tw2>0.0) then
                  wstar(0,j,k,i) = -tw2 + wstar(0,j,k,i)
               else
                  wstar(1,j,k,i) = -tw2
               endif
            enddo
#endif
         enddo  ! do j=jStartCalc,jEndCalc
      enddo  ! do i=iStartCalc,iEndCalc
   endif  ! if(isw_dim==3)

!    write(*,'(a10,6f12.6,2e20.10)')'hydro#1182',day_of_year,td,hydro_t1,hydro_t2, &
!                               beta,alpha,dz(16,1,31),vol(16,1,31) !jpdeb


   if (ierr/=0) then
      write( error_message, '("[",i2.2,"] ERROR - update_hydro: ",i3)') myID, ierr
      call stop_ecoham(ierr, msg=error_message)
   end if
   return

   end subroutine update_hydro

!=======================================================================
!
! !INTERFACE:
   subroutine do_hydro(ierr)
#define SUBROUTINE_NAME 'do_hydro'
!
! !DESCRIPTION:
! apply local changes of statevariables (rates) due to hydrodynamics
!
! !USES:
   use mod_var,  only: sst, n_st1, n_st2
   use mod_flux, only: f_hyd
   implicit none
!
! !IN/OUTPUT PARAMETERS:
   integer, intent(inout)  :: ierr
! !LOCAL VARIABLES:
   integer                 :: i, j, k, n
!
!-----------------------------------------------------------------------
#include "call-trace.inc"

   i=1; j=1; k=1 ! to get rid of "unused warning"

!############################################################
! apply advection and mixing
!############################################################
   do n = n_st1,n_st2
#if defined module_restoring && exclude_restoring_cells_hydro
      do i = iStartCalc, iEndCalc
         do k = 1, kMax
            do j = jstartCalc,jendCalc
               if (k > k_index_exclude_restoring(j,i,n)) cycle
               sst(j,k,i,n) = sst(j,k,i,n) + f_hyd(j,k,i,n)
            enddo
         enddo
      enddo
#else
      where (ixistz == 1) sst(:,:,:,n) = sst(:,:,:,n) + f_hyd(:,:,:,n)
#endif
   enddo
!############################################################

   if (ierr/=0) then
      write( error_message, '("[",i2.2,"] ERROR - do_hydro: ",i3)') myID, ierr
      call stop_ecoham(ierr, msg=error_message)
   end if
   return

   end subroutine do_hydro

!=======================================================================
!
! !INTERFACE:
   subroutine hyd_up(dt_,ierr)
#define SUBROUTINE_NAME 'hyd_up'
!
! !DESCRIPTION:
! algotrithm for advection and diffusion of tracers
! upstream advection & harmonic hor. diff
!
! !USES:
   use mod_var,  only: st, sink, n_st1, n_st2!, st_name
   use mod_flux, only: f_adv, f_adh, f_mxv, f_mxh, f_hyd
   use mod_flux, only: seto_flux, cum_flux_hydro
#ifdef TBNT_output
   use mod_flux, only: d_tu, d_tv, d_tw, d_mu, d_mv, d_mw
#endif
#ifdef module_sections
   use mod_sections,  only: l_sections, secNum3D, secDir, f_sec
#endif
#if defined module_restoring && defined exclude_restoring_cells_hydro
   use mod_restoring, only: k_index_exclude_restoring
#endif
   implicit none
!
! !INPUT PARAMETERS:
   real, intent(in)   :: dt_   ! timestep (days)
   integer, intent(inout) :: ierr
!
! !OUTPUT PARAMETERS:
!
! !LOCAL VARIABLES:
   integer                           :: i, j, k, k0
   integer                           :: ips, jps, kps, kms
   integer                           :: im, jm, km, kmx, n
   real, dimension(kmax)             :: aa=0.0, bb=0.0, cc=0.0, dd=0.0, ee=0.0, ff=0.0, pp=0.0, denom = 0.0 !jp
   real                              :: wd, wd1, wd2, dq, fac1, dqm, fac2
   real                              :: so, su, sc, se, sn, ss, sw, eps
   real                              :: t1, t2
#ifndef advection_acceleration
   real                              :: tin, tout, fu1, fu2, fv1, fv2, fw1, fw2
   real                              :: tu1, tu2, tv1, tv2, tw1, tw2
   integer                           :: ip, jp, kp
#endif
#ifdef TBNT_output
   real                              :: top
#endif
#ifdef module_sections
   integer                           :: snum
#endif
#ifdef MPI
   real, allocatable                 :: buf(:,:,:)
#endif
#ifdef debugMK
   integer                           :: ii=5,jj=59,kk=20
#endif
!
!-----------------------------------------------------------------------
#include "call-trace.inc"

   eps = 1.e-5
   kmx = kmax
   bb  = 1.0

!    call do_advection(day_of_year,ierr)

   wd1=0.; wd2=0. ! to get rid of "uninitialized" warning

   if (.not.advec) then
      tu = zero
      tv = zero
      tw = zero
   endif

   ! vertical sinking positiv downward [m/d]
   ! vertical mixing coefficient vdc [m2/d]
   ! t2=1: implizit vertical mixing
   ! t2=0: explizit vertical mixing
   t2 = one
   t1 = one-t2

   if (isw_dim==3) then
#ifdef MPI
     if (.false.) then
       allocate ( buf(jMax,kMax*(n_st2-n_st1+1),iStartD:iEndD) ); buf = 0.
       k = 1
       do n = n_st1, n_st2
         !buf(:,k:k+kMax-1,:) = st(:,:,:,n)
         buf(:,k:k+kMax-1,iStartD :iStartCalc) = st(:,:,iStartD :iStartCalc,n)
         buf(:,k:k+kMax-1,iEndCalc:iEndD)      = st(:,:,iEndCalc:iEndD,n)
         k = k + kMax
       enddo
       call mpi_parallel_exchange_boundary('1',buf, iStartD, iEndD, kMax*(n_st2-n_st1+1), ierr_mpi)
       ierr = ierr + ierr_mpi
       k = 1
       do n = n_st1, n_st2
         st(:,:,iStartD :iStartCalc,n) = buf(:,k:k+kMax-1,iStartD :iStartCalc)
         st(:,:,iEndCalc:iEndD,n)      = buf(:,k:k+kMax-1,iEndCalc:iEndD)
         k = k + kMax
       enddo
       deallocate ( buf )
     else
       do n = n_st1, n_st2
          call mpi_parallel_exchange_boundary('1',st(:,:,:,n), iStartD, iEndD, kMax, ierr_mpi)
          ierr = ierr + ierr_mpi
       enddo
     endif

     ! check whether data exchange was fine
     if (ierr/=0) then
        write(error_message,'("ERROR - hyd_up: exchange boundary data failed")')
        call stop_ecoham(ierr, error_message);
     endif

#endif

!############################################################
! advection
!############################################################
#ifdef advection_acceleration
      do n = n_st1, n_st2
         do i = iStartCalc, iEndCalc
            do k = 1, kMax
              do j = jStartCalc, jEndCalc
                  k0 = k_index(j,i)
#if defined module_restoring && defined exclude_restoring_cells_hydro
                  k0 = k_index_exclude_restoring(j,i,n)
#endif
                  if (k > k0) cycle
                  im = max(1,i-1)
                  jm = max(1,j-1)
                  ips = min(imax,i+1)
                  jps = min(jmax,j+1)
                  km = k-1
                  kms = max(1,k-1)
                  kps = min(kmax,k+1)

                  sw = st(jm,k,i,n)  * real(ixistz(jm,k,i))
                  se = st(jps,k,i,n) * real(ixistz(jps,k,i))
                  ss = st(j,k,ips,n) * real(ixistz(j,k,ips))
                  sn = st(j,k,im,n)  * real(ixistz(j,k,im))
                  su = st(j,kps,i,n) * real(ixistz(j,kps,i))
                  so = st(j,kms,i,n) * real(ixistz(j,kms,i))
                  sc = st(j,k,i,n)

                  f_adh(j,k,i,n) = (  ustar(-1,j,k,i)*sw + ustar(1,j,k,i)*se  &
                                     +vstar(-1,j,k,i)*sn + vstar(1,j,k,i)*ss  &
                                     +(ustar(0,j,k,i)+vstar(0,j,k,i))*sc ) /area(j,k,i)                         !(mmol m-2 s-1)
                  f_adv(j,k,i,n) = (wstar(-1,j,k,i)*so + wstar(1,j,k,i)*su + wstar(0,j,k,i)*sc) /area(j,k,i)    !(mmol m-2 s-1)
                  f_hyd(j,k,i,n) = f_adh(j,k,i,n)+f_adv(j,k,i,n)                                                !(mmol m-2 s-1)
#ifdef TBNT_output
                  d_tu(j,k,i,n) = d_tu(j,k,i,n) - ustar(1,j,k,i)*se/area(j,i)*dt_  ! transport eastern boundary (W->E positive)    !(mmol m-2 s-1)
                  d_tv(j,k,i,n) = d_tv(j,k,i,n) + vstar(1,j,k,i)*ss/area(j,i)*dt_  ! transport southern boundary (S->N positive)   !(mmol m-2 s-1)
                  d_tw(j,k,i,n) = d_tw(j,k,i,n) - wstar(1,j,k,i)*su/area(j,i)*dt_  ! transport lower boundary (surf->bot positive) !(mmol m-2 s-1)
#endif
!if (i==ii .and. j==jj.and. n==1) print*,'hyd_up#1203 ',f_adh(j,k,i,n),f_adv(j,k,i,n)
               enddo  ! do j=jstartCalc,jEndCalc
            enddo  ! do k=1 kMax
         enddo  ! do i=istartCalc,iEndCalc
      enddo  ! do n=n_st1,n_st2


#else
      do n = n_st1, n_st2
         do i = iStartCalc, iEndCalc
            do k = 1, kMax
              do j = jStartCalc, jEndCalc
                  k0 = k_index(j,i)
#if defined module_restoring && exclude_restoring_cells_hydro
                  k0 = k_index_exclude_restoring(j,i,n)
#endif
                  if (k > k0) cycle
                  im  = max(1,i-1)
                  jm  = max(1,j-1)
                  ip  = min(imax,i)
                  jp  = min(jmax,j)
                  ips = min(imax,i+1)
                  jps = min(jmax,j+1)

                  kp = min(k0,k)
                  km = k-1
                  kms = max(1,k-1)
                  kps = min(kmax,k+1)

                  tu1 = tu(jm,k,i) * real(ixistu(jm,k,i))
                  tu2 = tu(jp,k,i) * real(ixistu(jp,k,i))
                  tv1 = tv(j,k,im) * real(ixistv(j,k,im))
                  tv2 = tv(j,k,ip) * real(ixistv(j,k,ip))  !south
                  tw1 = tw(j,km,i) * real(ixistw(j,km,i))
                  tw2 = tw(j,kp,i) * real(ixistw(j,kp,i))

                  sw = st(jm,k,i,n)  * real(ixistz(jm,k,i))
                  se = st(jps,k,i,n) * real(ixistz(jps,k,i))
                  ss = st(j,k,ips,n) * real(ixistz(j,k,ips))
                  sn = st(j,k,im,n)  * real(ixistz(j,k,im))
                  su = st(j,kps,i,n) * real(ixistz(j,kps,i))
                  so = st(j,kms,i,n) * real(ixistz(j,kms,i))
                  sc = st(j,k,i,n)
! U
                  tin  = max(0., tu1*sw)
                  tout = max(0.,-tu1*sc)
                  fu1  = tin-tout

                  tin  = max(0.,-tu2*se)
                  tout = max(0., tu2*sc)
                  fu2  = tin-tout
! V
                  tin  = max(0., tv2*ss)
                  tout = max(0.,-tv2*sc)
                  fv1  = tin-tout

                  tin  = max(0.,-tv1*sn)
                  tout = max(0., tv1*sc)
                  fv2  = tin-tout
! W
                  tin  = max(0., tw1*so)
                  tout = max(0.,-tw1*sc)
                  fw1  = tin-tout

                  tin  = max(0.,-tw2*su)
                  tout = max(0., tw2*sc)
                  fw2  = tin-tout

                  ! change of mass due to horizontal and vertical advection (transport of water)
                  f_adh(j,k,i,n) = (fu1+fu2+fv1+fv2) /area(j,i)  !(mmol m-2 s-1)
                  f_adv(j,k,i,n) = (fw1+fw2)         /area(j,i)  !(mmol m-2 s-1)
                  f_hyd(j,k,i,n) = f_adh(j,k,i,n)+f_adv(j,k,i,n) !(mmol m-2 s-1)
#ifdef TBNT_output
                  d_tu(j,k,i,n) = d_tu(j,k,i,n) - fu2*dt_/area(j,i)  ! transport eastern boundary (W->E positive)    !(mmol m-2 s-1)
                  d_tv(j,k,i,n) = d_tv(j,k,i,n) + fv1*dt_/area(j,i)  ! transport southern boundary (S->N positive)   !(mmol m-2 s-1)
                  d_tw(j,k,i,n) = d_tw(j,k,i,n) - fw2*dt_/area(j,i)  ! transport lower boundary (surf->bot positive) !(mmol m-2 s-1)
#endif

#ifdef module_sections
                  if (l_sections) then
                     if ( secNum3D(j,k,i) /= 0) then
                        snum = secNum3D(j,k,i)
                        if (abs(secDir(snum)) == 1) then
                           f_sec(k,snum,n) = f_sec(k,snum,n) - sign(dt_,real(secDir(snum))) * fu2
                        else
                           f_sec(k,snum,n) = f_sec(k,snum,n) + sign(dt_,real(secDir(snum))) * fv1
                        endif
                     endif
                  endif
#endif
               enddo  ! do j=jStartCalc,jEndCalc
            enddo  ! do k=1,kMax
         enddo  ! do i=iStartCalc,iEndCalc
      enddo  ! do n=n_st1,n_st2

#endif
!############################################################
! horizontal mixing
!############################################################
      if (dif_hor) then
         do n = n_st1, n_st2
            do i = iStartCalc, iEndCalc
               do k = 1, kMax
                  do j = jStartCalc, jEndCalc
                     k0 = k_index(j,i)
#if defined module_restoring && exclude_restoring_cells_hydro
                     k0 = k_index_exclude_restoring(j,i,n)
#endif
                     if (k > k0) cycle
                     im  = max(1,i-1)
                     jm  = max(1,j-1)
                     ips = min(imax,i+1)
                     jps = min(jmax,j+1)
                     sc = st(j,k,i,n)
                     sn = st(j,k,im,n)
                     ss = st(j,k,ips,n)
                     sw = st(jm,k,i,n)
                     se = st(jps,k,i,n)
                     ! change of mass due to horizontal mixing / diffusion processes (no net transport of water)
                     f_mxh(j,k,i,n) = ax(j,k,i)*(real(ixistz(jm,k,i))*(sw-sc)-real(ixistz(jps,k,i))*(sc-se))   &
                                     +ay(j,k,i)*(real(ixistz(j,k,im))*(sn-sc)-real(ixistz(j,k,ips))*(sc-ss))  !(mmol m-2 s-1)
                     f_mxh(j,k,i,n) = f_mxh(j,k,i,n)*dz(j,k,i)                                                !(mmol m-2 s-1)
                     f_hyd(j,k,i,n) = f_hyd(j,k,i,n) + f_mxh(j,k,i,n)                                         !(mmol m-2 s-1)
#ifdef TBNT_output
                     d_mu(j,k,i,n) = d_mu(j,k,i,n) + ax(j,k,i)*(real(ixistz(jps,k,i))*(sc-se))*dz(j,k,i)*dt_  ! mixing eastern boundary (W->E positive)  !(mmol m-2 s-1)
                     d_mv(j,k,i,n) = d_mv(j,k,i,n) + ay(j,k,i)*(real(ixistz(j,k,ips))*(ss-sc))*dz(j,k,i)*dt_  ! mixing southern boundary (S->N positive) !(mmol m-2 s-1)
#endif
                  enddo  ! do j=jStartCalc,jEndCalc
               enddo  ! do k=1,kMax
            enddo  ! do i=iStartCalc,iEndCalc
         enddo  ! do n=n_st1,n_st2
      endif  !dif_hor

   endif  !(isw_dim.eq.3)

!############################################################
! vertical mixing
!############################################################
! print*,'hydro#635 dif_ver:', dif_ver
   if (dif_ver) then
      do n = n_st1, n_st2
         do i = iStartCalc, iEndCalc
            do j = jStartCalc, jEndCalc
               k0 = k_index(j,i)
#if defined module_restoring && exclude_restoring_cells_hydro
               k0 = k_index_exclude_restoring(j,i,n)
#endif
               if(k0>0)then
                  ! sinking at ground
                  wd1 = sink(n)*dt_/dz(j,k0,i)
                  wd2 = sink(n)*dt_/dz(j,k0,i)

                  if (k0>1) then ! multilayer watercolumn
! upper bc
! mix: no flux
! sink: no influx
                     dq    = dz(j,1,i)*(dz(j,1,i)+dz(j,2,i))/2.
                     fac1  = vdc(j,2,i)*dt_/dq
                     wd    = sink(n)*dt_/dz(j,1,i)
                     bb(1) = -1.-(fac1+wd)*t2
                     dd(1) = -st(j,1,i,n)+t1*(st(j,1,i,n)*(fac1+wd)-st(j,2,i,n)*fac1)
                     cc(1) = fac1*t2
! lower bc
! mix: no flux
! sink: flux out
                     dqm   = dz(j,k0,i)*(dz(j,k0,i)+dz(j,k0-1,i))/2.
                     fac2  = vdc(j,k0,i)*dt_/dqm
                     bb(k0)= -1.-(fac2+wd2)*t2
                     dd(k0)= -st(j,k0,i,n)+t1*(-st(j,k0-1,i,n)*(fac2+wd1)+st(j,k0,i,n)*(fac2+wd2))
                     aa(k0)= (fac2+wd1)*t2

                     do k = 2, k0-1
                        dq   = dz(j,k,i)*(dz(j,k+1,i)+dz(j,k,i))/2.
                        dqm  = dz(j,k,i)*(dz(j,k,i)+dz(j,k-1,i))/2.
                        fac1 = vdc(j,k,i)*dt_/dqm
                        fac2 = vdc(j,k+1,i)*dt_/dq
                        wd   = sink(n)*dt_/dz(j,k,i)
                        aa(k)= (fac1+wd)*t2
                        cc(k)= fac2*t2
                        bb(k)= -1.-(aa(k)+cc(k))
                        dd(k)= -st(j,k,i,n)+t1*(-st(j,k-1,i,n)*(fac1+wd)+st(j,k,i,n)*(fac1+fac2+wd)-st(j,k+1,i,n)*fac2)
                     enddo
                  endif

                  ! solving tridiag
                  do k = 1, k0
                     ee(k) = cc(k)/bb(k)
                     ff(k) = dd(k)/bb(k)
                     denom(k) = 1./bb(k)
                  enddo
                  do k = 2, k0
                     denom(k) = 1.0/(bb(k) - aa(k)*denom(k-1)*cc(k-1))
                  enddo
                  do k = 2, k0
                     ee(k) = cc(k)*denom(k)
                     ff(k) = (dd(k) - aa(k)*ff(k-1))*denom(k)
                     pp(k) = ff(k)
                  enddo

                  ! change of mass due to vertical mixing / diffusion processes (no net transport of water)
                  if (k0>1) then
                     do k = k0-1,1,-1
                        pp(k) = ff(k) - ee(k)*pp(k+1)
                     enddo
                     ! sedimentation
                     seto_flux(j,i,n) = sink(n) * pp(k0)                     !(mmol m-2 s-1)
                     do k = 1, k0
                        f_mxv(j,k,i,n) = (pp(k)-st(j,k,i,n)) /dt_ *dz(j,k,i) !(mmol m-2 s-1)
                        f_hyd(j,k,i,n) = f_hyd(j,k,i,n) + f_mxv(j,k,i,n)     !(mmol m-2 s-1)
                     enddo
                  else !(k0==1) nur das heraussinken hat Einfluss auf das mixing
                     ! sedimentation
                     seto_flux(j,i,n) = sink(n) * st(j,k0,i,n)               !(mmol m-2 s-1)
                     ! flux applied to pelagic state variables
                     f_mxv(j,k0,i,n)  = -seto_flux(j,i,n)                    !(mmol m-2 s-1)
                     f_hyd(j,k0,i,n)  = f_hyd(j,k0,i,n) + f_mxv(j,k0,i,n)    !(mmol m-2 s-1)
                  endif
#ifdef TBNT_output
                  top=0.
                  do k=1,k0
                     !mixing lower boundary (surf->bot positive)
                     d_mw(j,k,i,n) = d_mw(j,k,i,n) + (top - f_mxv(j,k,i,n)) *dt_  !(mmol m-2 s-1)
                     top = top - f_mxv(j,k,i,n)
                  enddo
#endif
               endif ! if(k0>0)then
            enddo  ! do j=jStartCalc, jEndCalc
         enddo  ! do i=iStartCalc, iEndCalc
      enddo  ! do n=n_st1,n_st2
   endif  !(dif_ver)

   if (ierr/=0) then
      write( error_message, '("[",i2.2,"] ERROR - hyd_up: ",i3)') myID, ierr
      call stop_ecoham(ierr, msg=error_message)
   end if
   return

   end subroutine hyd_up

!=======================================================================
!
! !INTERFACE:
   subroutine close_hydro(ierr)
#define SUBROUTINE_NAME 'close_hydro'
!
! !DESCRIPTION:
!
! !USES:
   implicit none
!
! !INPUT PARAMETERS:
   integer, intent(inout) :: ierr
!
! !LOCAL VARIABLES:
!
!-----------------------------------------------------------------------
#include "call-trace.inc"

   close(temp_unit)
   close(salt_unit)
   close(tu_unit)
   close(tv_unit)
#ifdef makehyd_Input
   close(tw_unit)
   close(vdc_unit)
   close(vol_unit)
   deallocate( vdc_old, vdc_new )
#else
   close(z_unit)
   close(av_unit)
   deallocate( zeta )
   deallocate( av_old, av_new )
   if (lsm_include) then
      close(sm_unit)
      deallocate( sm_old, sm_new )
   endif
#endif

   deallocate( vdc, ax, ay, tu, tv, tw ) !,du,dv
   deallocate( rho, temp, salt, pres )
   deallocate( temp_old, temp_new )
   deallocate( salt_old, salt_new )
   deallocate( vol_old, vol_new )
#ifdef advection_acceleration
   deallocate( ustar, vstar, wstar )
#endif

#ifdef debug_HAMSOM_forcing
   close(debugUnit)
#endif

   if (ierr/=0) then
      write( error_message, '("[",i2.2,"] ERROR - close_hydro",i3)') myID, ierr
      call stop_ecoham(ierr, msg=error_message)
   end if
   return

   end subroutine close_hydro

!=======================================================================

   end module mod_hydro

!=======================================================================
