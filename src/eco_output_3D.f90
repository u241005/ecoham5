!--------------------------------
!
! !MODULE: output_3D ---  subroutines for ecoham-3D-output
!
! !INTERFACE:
   MODULE mod_output_3D
!
! !DESCRIPTION:
!
! !USES:
   use mod_common
   use mod_var
   use mod_flux
#ifdef module_chemie
   use mod_chemie
#endif
#ifdef module_sediment
   use mod_sediment
#endif
#ifdef NETCDF
   use mod_ncdfout
#endif
   use mod_output_var
   implicit none
!
!  default: all is public.
   public
!
! !LOCAL VARIABLES:
   integer    :: n_str2D_out, n_str3D_out
   integer    :: n_full2D, n_full3D
   character(len=20), dimension(:), allocatable :: full2D_name, full3D_name
   character(len=99), dimension(:), allocatable :: full2D_long, full3D_long
   character(len=99), dimension(:), allocatable :: full2D_unit, full3D_unit
!
!=======================================================================

   contains

!=======================================================================
!
! !INTERFACE:
   subroutine init_output_3D(ierr)
#define SUBROUTINE_NAME 'init_output_3D'
!
! !DESCRIPTION:
!
! !USES:
   implicit none
!
! !OUTPUT PARAMETERS:
   integer, intent(inout)  :: ierr
!
! !LOCAL VARIABLES:
   character(len=99)  :: title, time_str
   character(len=20)  :: var2D_name, var3D_name
   character(len=3)   :: str
   character(len=3), dimension(:), allocatable :: var_list
   integer            :: n, n_out, n1, n2
!
   namelist /var2D_out_nml/var2D_name
   namelist /var3D_out_nml/var3D_name
!
!-----------------------------------------------------------------------
#include "call-trace.inc"

   if (myID == master) call write_log_message(" INITIALIZING 3D output ")

#ifdef NETCDF
   ! allocate and fill temporary output arrays non-(time)varying grid specifications
   allocate( area_master(jmax,     imax) )
   allocate( dz_master  (jmax,kmax,imax) )
   allocate( vol_master (jmax,kmax,imax) )
   area_master(:,  iStartD:iEndD) = area(:,  iStartD:iEndD)
   dz_master  (:,:,iStartD:iEndD) = dz  (:,:,iStartD:iEndD)
   vol_master (:,:,iStartD:iEndD) = vol (:,:,iStartD:iEndD)
#ifdef MPI
! collect data from all domains
   call mpi_parallel_gather(area_master(:,:),1,ierr_mpi)
   ierr = ierr + ierr_mpi
   call mpi_parallel_gather(dz_master(:,:,:),kmax,ierr_mpi)
   ierr = ierr + ierr_mpi
   call mpi_parallel_gather(vol_master(:,:,:),kmax,ierr_mpi)
   ierr = ierr + ierr_mpi
   ! check whether gather was fine
   if (ierr/=0) then
      write(error_message,'("ERROR - init_output: gathering data from domains failed")')
      call stop_ecoham(ierr, msg=error_message);
   endif
#endif
#endif

   if (myID == master) then

      ! generate default title & filename for netcdf
      title    = trim(runID)//'_3D'
      filename = trim(title)
      ! read title & filename from namelist
      !    ---- !! still ToDo !! ----
      write(time_str,'(i4.4,a1,i2.2,a1,i2.2,x,i2.2,a3)') iy1,'-',im1,'-',id1,ih1,':00'

      ! count 2D output variables
      open(nml_unit,file=trim(settings_file),action='read',status='old')
      n_out = 0
      do while ( n_out < n_str2D_out )
         read(nml_unit,nml=var2D_out_nml,end=90)
         select case (var2D_name)
            case ('statevars')
               st_out2D(:) = .true.
            case ('derivedvars')
               de_out2D(:) = .true.
#ifdef module_chemie
            case ('chemievars')
               ch_out2D(:) = .true.
               !ch_out2D(itdic) = .false.
               !ch_out2D(italk) = .false.
#endif
            case ('from_to')
               flux_out2D(1:max_flux) = .true.
            case ('f_adv','f_adh','f_mxv','f_mxh','f_hyd','f_riv','f_dil','f_pev','f_res')
               where( flux_name(:)(1:3).eq.var2D_name(3:5) ) flux_out2D(:) = .true.
            case ('f_tu','f_tv','f_tw','f_mu','f_mv','f_mw')
#ifdef TBNT_output
               where( flux_name(:)(1:2).eq.var2D_name(3:4) ) flux_out2D(:) = .true.
#else
               ierr = 1
               error_message = 'ERROR: output for TBNT-tracing requires compilation with flag -DTBNT_output'
               call stop_ecoham(ierr, msg=error_message)
#endif
#ifdef module_biogeo
            case ('carboncycle','C-cycle')
               call scan_output_fluxes('c'  ,flux_out2D,ierr)
            case ('nitrogencycle','N-cycle')
               call scan_output_fluxes('n'  ,flux_out2D,ierr)
            case ('phosphoruscycle','P-cycle')
               call scan_output_fluxes('p'  ,flux_out2D,ierr)
            case ('siliconcycle','Si-cycle','S-cycle')
               call scan_output_fluxes('s'  ,flux_out2D,ierr)
            case ('alkalinitycycle')
               call scan_output_fluxes('alk',flux_out2D,ierr)
            case ('oxygencycle')
               call scan_output_fluxes('o2o',flux_out2D,ierr)
            case ('bacteriacycle')
               call scan_output_fluxes('bac',flux_out2D,ierr)
            case ('remineralisation')
               call scan_output_fluxes('d1c',flux_out2D,ierr)
               call scan_output_fluxes('d2c',flux_out2D,ierr)
            case ('NPP-C')
               flux_out2D(i_dic_p1c) = .true.
               flux_out2D(i_dic_p2c) = .true.
               flux_out2D(i_p1c_soc) = .true.
               flux_out2D(i_p2c_soc) = .true.
               !flux_out2D(i_z1c_dic) = .true.
               !flux_out2D(i_z2c_dic) = .true.
               !flux_out2D(i_bac_dic) = .true.
            case ('NEP-C','NCP-C')
               flux_out2D(i_dic_p1c) = .true.
               flux_out2D(i_dic_p2c) = .true.
               flux_out2D(i_z1c_dic) = .true.
               flux_out2D(i_z2c_dic) = .true.
               flux_out2D(i_bac_dic) = .true.
               flux_out2D(i_doc_dic) = .true.
#ifdef module_sediment
               sedi_flux_out2D(i_sed_dic) = .true.
#endif
            case ('NPP-N','N-uptake')
               flux_out2D(i_n3n_p1n) = .true.
               flux_out2D(i_n3n_p2n) = .true.
               flux_out2D(i_n4n_p1n) = .true.
               flux_out2D(i_n4n_p2n) = .true.
            case ('NEP-N','NCP-N')
               flux_out2D(i_n3n_p1n) = .true.
               flux_out2D(i_n3n_p2n) = .true.
               flux_out2D(i_n4n_p1n) = .true.
               flux_out2D(i_n4n_p2n) = .true.
               flux_out2D(i_z1n_n4n) = .true.
               flux_out2D(i_z2n_n4n) = .true.
               flux_out2D(i_ban_n4n) = .true.
               flux_out2D(i_n4n_ban) = .true.
               flux_out2D(i_don_n4n) = .true.
#ifdef module_sediment
               sedi_flux_out2D(i_sed_n4n) = .true.
               sedi_flux_out2D(i_sed_nn2) = .true.
#endif
#endif

#ifdef module_sediment
            case ('sedimentvars')
               sd_out2D(:) = .true.
            case ('sedimentfluxes')
               sedo_flux_out2D(:) = .true.
               sedi_flux_out2D(:) = .true.
#endif
            case ('othervars')
               ot2D_out(:) = .true.

            case default
               ! scan for match in statevars
               where( var2D_name .eq. st_name(:) ) st_out2D(:) = .true.
               ! scan for match in derivedvars
               where( var2D_name .eq. de_name(:) ) de_out2D(:) = .true.
#ifdef module_chemie
               ! scan for match in chemievars
               where( var2D_name .eq. ch_name(:) ) ch_out2D(:) = .true.
#endif
               ! scan for match in pelagic fluxes
               where( var2D_name .eq. 'f_'//flux_name) flux_out2D = .true.
               ! scan for match in sedimentvars
#ifdef module_sediment
               where (var2D_name .eq. sd_name(:)) sd_out2D(:) = .true.
               ! scan for match in fluxes from  pelagic to sediment
               where (var2D_name .eq. 'f_'//sedo_flux_name(:)) sedo_flux_out2D(:) = .true.
               ! scan for match in fluxes from sediment to pelagic
               where (var2D_name .eq. 'f_'//sedi_flux_name(:)) sedi_flux_out2D(:) = .true.
               ! scan for match in othervars
               ! ot-vars may not be treated the same way as above,
               ! because ot2D_name(:) and ot2D_out(:) may have different lenghts
#endif
               do n = 1, n_ot2D_var
                  if (var2D_name .eq. ot2D_name(n)) ot2D_out(n) = .true.
               enddo
            end select
         n_out = n_out + 1
      enddo
      90 continue
      close(nml_unit)

#ifdef exclude_p1x
      ! exclude output for 1st phytoplankton group
      where( index(st_name,'p1') /=0 ) st_out2D = .false.
      where( index(de_name,'p1') /=0 ) de_out2D = .false.
      where( index(flux_name,'p1') /=0 ) flux_out2D = .false.
      do n = 1, n_ot2D_var
         if ( index(ot2D_name(n),'p1') /=0 ) ot2D_out(n) = .false.
      enddo
#ifdef module_sediment
      where ( index(sedo_flux_name,'p1') /=0 ) sedo_flux_out2D = .false.
#endif
#endif
#ifdef exclude_p2x
      ! exclude output for 2nd phytoplankton group
      where( index(st_name,'p2') /=0 ) st_out2D = .false.
      where( index(de_name,'p2') /=0 ) de_out2D = .false.
      where( index(flux_name,'p2') /=0 ) flux_out2D = .false.
      do n = 1, n_ot2D_var
         if ( index(ot2D_name(n),'p2') /=0 ) ot2D_out(n) = .false.
      enddo
#ifdef module_sediment
      where ( index(sedo_flux_name,'p2') /=0 ) sedo_flux_out2D = .false.
#endif
#endif
#ifdef exclude_p3x
      ! exclude output for 3rd phytoplankton group
      where( index(st_name,'p3') /=0 ) st_out2D = .false.
      where( index(de_name,'p3') /=0 ) de_out2D = .false.
      where( index(flux_name,'p3') /=0 ) flux_out2D = .false.
      do n = 1, n_ot2D_var
         if ( index(ot2D_name(n),'p3') /=0 ) ot2D_out(n) = .false.
      enddo
#ifdef module_sediment
      where ( index(sedo_flux_name,'p3') /=0 ) sedo_flux_out2D = .false.
#endif
#endif

      ! count 3D output variables
      open(nml_unit,file=trim(settings_file),action='read',status='old')
      n_out = 0
      do while ( n_out < n_str3D_out )
         read(nml_unit,nml=var3D_out_nml,end=91)
         select case (var3D_name)
            case ('statevars')
               st_out3D(:) = .true.
            case ('derivedvars')
               de_out3D(:) = .true.
#ifdef module_chemie
            case ('chemievars')
               ch_out3D(:) = .true.
               !ch_out3D(itdic) = .false.
               !ch_out3D(italk) = .false.
#endif
            case ('from_to')
               flux_out3D(1:max_flux) = .true.
            case ('f_adv','f_adh','f_mxv','f_mxh','f_hyd','f_riv','f_dil','f_pev','f_res')
               where( flux_name(:)(1:3).eq.var3D_name(3:5) ) flux_out3D(:) = .true.
            case ('f_tu','f_tv','f_tw','f_mu','f_mv','f_mw')
#ifdef TBNT_output
               where( flux_name(:)(1:2).eq.var3D_name(3:4) ) flux_out3D(:) = .true.
#else
               ierr = 1
               error_message = 'ERROR: output for TBNT-tracing requires compilation with flag -DTBNT_output'
               call stop_ecoham(ierr, msg=error_message)
#endif
#ifdef module_biogeo
            case ('carboncycle','C-cycle')
               call scan_output_fluxes('c'  ,flux_out3D,ierr)
            case ('nitrogencycle','N-cycle')
               call scan_output_fluxes('n'  ,flux_out3D,ierr)
            case ('phosphoruscycle','P-cycle')
               call scan_output_fluxes('p'  ,flux_out3D,ierr)
            case ('siliconcycle','Si-cycle','S-cycle')
               call scan_output_fluxes('s'  ,flux_out3D,ierr)
            case ('alkalinitycycle')
               call scan_output_fluxes('alk',flux_out3D,ierr)
            case ('oxygencycle')
               call scan_output_fluxes('o2o',flux_out3D,ierr)
            case ('bacteriacycle')
               call scan_output_fluxes('bac',flux_out3D,ierr)
            case ('remineralisation')
               call scan_output_fluxes('d1c',flux_out3D,ierr)
               call scan_output_fluxes('d2c',flux_out3D,ierr)
            case ('NPP-C')
               flux_out3D(i_dic_p1c) = .true.
               flux_out3D(i_dic_p2c) = .true.
               flux_out3D(i_p1c_soc) = .true.
               flux_out3D(i_p2c_soc) = .true.
               !flux_out3D(i_z1c_dic) = .true.
               !flux_out3D(i_z2c_dic) = .true.
               !flux_out3D(i_bac_dic) = .true.
            case ('NEP-C','NCP-C')
               flux_out3D(i_dic_p1c) = .true.
               flux_out3D(i_dic_p2c) = .true.
               flux_out3D(i_z1c_dic) = .true.
               flux_out3D(i_z2c_dic) = .true.
               flux_out3D(i_bac_dic) = .true.
               flux_out3D(i_doc_dic) = .true.
#ifdef module_sediment
               sedi_flux_out2D(i_sed_dic) = .true. ! NOTE: this a 2D array!!!
#endif
            case ('NPP-N','N-uptake')
               flux_out3D(i_n3n_p1n) = .true.
               flux_out3D(i_n3n_p2n) = .true.
               flux_out3D(i_n4n_p1n) = .true.
               flux_out3D(i_n4n_p2n) = .true.
            case ('NEP-N','NCP-N')
               flux_out3D(i_n3n_p1n) = .true.
               flux_out3D(i_n3n_p2n) = .true.
               flux_out3D(i_n4n_p1n) = .true.
               flux_out3D(i_n4n_p2n) = .true.
               flux_out3D(i_z1n_n4n) = .true.
               flux_out3D(i_z2n_n4n) = .true.
               flux_out3D(i_ban_n4n) = .true.
               flux_out3D(i_n4n_ban) = .true.
               flux_out3D(i_don_n4n) = .true.
#ifdef module_sediment
               sedi_flux_out2D(i_sed_n4n) = .true. ! NOTE: this a 2D array!!!
               sedi_flux_out2D(i_sed_nn2) = .true. ! NOTE: this a 2D array!!!
#endif
#endif
            case ('othervars')
               ot3D_out(:)       = .true.
#ifndef debug_HAMSOM_forcing
               ot3D_out(i_tu)    = .false.
               ot3D_out(i_tv)    = .false.
               ot3D_out(i_tw)    = .false.
#endif
#ifdef module_meteo
               ot3D_out(ipar)    = .false.
#endif
            case ('TBNT-x1x')
#ifdef TBNT_output
               ot3D_out(ivol) = .true.
               str = 'x1x'
               st_out3D(ix1x) = .true.
               where ('f_mu_'//str  .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_mv_'//str  .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_mw_'//str  .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_tu_'//str  .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_tv_'//str  .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_tw_'//str  .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_riv_'//str .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_dil_'//str .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_pev_'//str .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_res_'//str .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
#ifdef module_sediment
              ! NOTE: these are 2D arrays affecting 2D-output list !!!
               where ('f_x1x_sed' .eq. 'f_'//sedo_flux_name(:)) sedo_flux_out2D(:) = .true.
#endif
#else
               ierr = 1
               error_message = 'ERROR: output for TBNT-tracing requires compilation with flag -DTBNT_output'
               call stop_ecoham(ierr, msg=error_message)
#endif
            case ('TBNT-C')
#ifdef TBNT_output
               ot3D_out(ivol) = .true.
#ifdef module_biogeo
               allocate ( var_list(13) )
               var_list = (/'dic','p1c','p2c','p3c','p3k','z1c','z2c','d1c','d2c','d2k', &
                            'doc','soc','bac'/)
               do n = 1, size(var_list)
                  str = var_list(n)
                  where (str           .eq. st_name(:) )        st_out3D(:)   = .true.
                  where ('f_mu_'//str  .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
                  where ('f_mv_'//str  .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
                  where ('f_mw_'//str  .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
                  where ('f_tu_'//str  .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
                  where ('f_tv_'//str  .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
                  where ('f_tw_'//str  .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
                  where ('f_riv_'//str .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
                  where ('f_dil_'//str .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
                  where ('f_pev_'//str .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
                  where ('f_res_'//str .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
#ifdef module_sediment
                  where ('f_'//str//'_sed' .eq. 'f_'//sedo_flux_name(:)) sedo_flux_out2D(:) = .true.
#endif
               enddo
               deallocate (var_list)
               where ('f_dic_p1c' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_p1c_z1c' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_p1c_z2c' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_p1c_d1c' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_p1c_d2c' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_p1c_doc' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_p1c_soc' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_dic_p2c' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_p2c_z1c' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_p2c_z2c' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_p2c_d1c' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_p2c_d2c' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_p2c_doc' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_p2c_soc' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_dic_p3c' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_p3c_z1c' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_p3c_z2c' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_p3c_d1c' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_p3c_d2c' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_p3c_doc' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_p3c_soc' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_bac_z1c' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_d1c_z1c' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_z1c_d1c' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_z1c_d2c' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_z1c_doc' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_z1c_dic' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_bac_z2c' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_d1c_z2c' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_z2c_d1c' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_z2c_d2c' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_z2c_doc' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_z2c_dic' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_doc_bac' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_bac_dic' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_d1c_doc' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_d2c_doc' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_soc_doc' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_dic_psk' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_psk_dic' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_psk_d2k' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_d2k_dic' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_dic_p3k' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_p3k_z1c' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_p3k_z2c' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_p3k_d2k' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_z1c_z2c' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_air_o2c' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
#ifdef module_sediment
              ! NOTE: these are 2D arrays affecting 2D-output list !!!
               where ('sd_poc'    .eq. sd_name(:))              sd_out2D(:)        = .true.
               where ('sd_pok'    .eq. sd_name(:))              sd_out2D(:)        = .true.
               where ('f_sed_dic' .eq. 'f_'//sedi_flux_name(:)) sedi_flux_out2D(:) = .true.
               where ('f_sed_o3c' .eq. 'f_'//sedi_flux_name(:)) sedi_flux_out2D(:) = .true.
#endif
#endif
#else
               ierr = 1
               error_message = 'ERROR: output for TBNT-tracing requires compilation with flag -DTBNT_output'
               call stop_ecoham(ierr, msg=error_message)
#endif
            case ('TBNT-N')
#ifdef TBNT_output
               ot3D_out(ivol) = .true.
#ifdef module_biogeo
               where ('n3n'       .eq. st_name(:))         st_out3D(:)   = .true.
               where ('n4n'       .eq. st_name(:))         st_out3D(:)   = .true.
               where ('p1n'       .eq. st_name(:))         st_out3D(:)   = .true.
               where ('p2n'       .eq. st_name(:))         st_out3D(:)   = .true.
               where ('p3n'       .eq. st_name(:))         st_out3D(:)   = .true.
               where ('d1n'       .eq. st_name(:))         st_out3D(:)   = .true.
               where ('d2n'       .eq. st_name(:))         st_out3D(:)   = .true.
               where ('don'       .eq. st_name(:))         st_out3D(:)   = .true.
               where ('z1n'       .eq. de_name(:))         de_out3D(:)   = .true.
               where ('z2n'       .eq. de_name(:))         de_out3D(:)   = .true.
               where ('ban'       .eq. de_name(:))         de_out3D(:)   = .true.
               where ('f_n3n_p1n' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_n4n_p1n' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_p1n_z1n' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_p1n_z2n' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_p1n_d1n' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_p1n_d2n' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_p1n_don' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_n3n_p2n' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_n4n_p2n' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_p2n_z1n' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_p2n_z2n' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_p2n_d1n' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_p2n_d2n' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_p2n_don' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_n3n_p3n' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_n4n_p3n' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_p3n_z1n' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_p3n_z2n' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_p3n_d1n' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_p3n_d2n' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_p3n_don' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_d1n_z1n' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_z1n_d1n' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_z1n_d2n' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_z1n_don' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_z1n_n4n' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_d1n_z2n' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_z2n_d1n' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_z2n_d2n' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_z2n_don' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_z2n_n4n' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_don_ban' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_n4n_ban' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_ban_n4n' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_d1n_don' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_d2n_don' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_n4n_n3n' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_n3n_nn2' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_ban_z1n' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_ban_z2n' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_z1n_z2n' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_n3n_brm' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_atm_n3n' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_atm_n4n' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               allocate ( var_list(11) )
               var_list = (/'n3n','n4n','p1n','p2n','p3n','d1n','d2n','don','z1c','z2c','bac'/)
               do n = 1, 11
                  str = var_list(n)
                  where ('f_mu_'//str  .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
                  where ('f_mv_'//str  .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
                  where ('f_mw_'//str  .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
                  where ('f_tu_'//str  .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
                  where ('f_tv_'//str  .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
                  where ('f_tw_'//str  .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
                  where ('f_riv_'//str .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
                  where ('f_dil_'//str .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
                  where ('f_pev_'//str .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
                  where ('f_res_'//str .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
#ifdef module_sediment
                  where ('f_'//str//'_sed' .eq. 'f_'//sedo_flux_name(:)) sedo_flux_out2D(:) = .true.
#endif
               enddo
               deallocate (var_list)
#ifdef module_sediment
              ! NOTE: these are 2D arrays affecting 2D-output list !!!
               where ('sd_pon'    .eq. sd_name(:))              sd_out2D(:)        = .true.
               where ('f_sed_n4n' .eq. 'f_'//sedi_flux_name(:)) sedi_flux_out2D(:) = .true.
               where ('f_sed_nn2' .eq. 'f_'//sedi_flux_name(:)) sedi_flux_out2D(:) = .true.
#endif
#endif
#else
               ierr = 1
               error_message = 'ERROR: output for TBNT-tracing requires compilation with flag -DTBNT_output'
               call stop_ecoham(ierr, msg=error_message)
#endif
            case ('TBNT-P') ! for P cycle: n = 12
#ifdef TBNT_output
               ot3D_out(ivol) = .true.
#ifdef module_biogeo
               where ('n1p'       .eq. st_name(:))         st_out3D(:)   = .true.
               where ('p1p'       .eq. st_name(:))         st_out3D(:)   = .true.
               where ('p2p'       .eq. st_name(:))         st_out3D(:)   = .true.
               where ('p3p'       .eq. st_name(:))         st_out3D(:)   = .true.
               where ('d1p'       .eq. st_name(:))         st_out3D(:)   = .true.
               where ('d2p'       .eq. st_name(:))         st_out3D(:)   = .true.
               where ('dop'       .eq. st_name(:))         st_out3D(:)   = .true.
               where ('z1p'       .eq. de_name(:))         de_out3D(:)   = .true.
               where ('z2p'       .eq. de_name(:))         de_out3D(:)   = .true.
               where ('bap'       .eq. de_name(:))         de_out3D(:)   = .true.
               where ('f_n1p_p1p' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_p1p_z1p' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_p1p_z2p' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_p1p_d1p' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_p1p_d2p' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_p1p_dop' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_n1p_p2p' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_p2p_z1p' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_p2p_z2p' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_p2p_d1p' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_p2p_d2p' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_p2p_dop' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_n1p_p3p' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_p3p_z1p' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_p3p_z2p' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_p3p_d1p' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_p3p_d2p' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_p3p_dop' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_d1p_z1p' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_z1p_d1p' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_z1p_n1p' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_z1p_d2p' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_z1p_dop' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_d1p_z2p' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_z2p_d1p' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_z2p_d2p' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_z2p_dop' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_z2p_n1p' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_dop_bap' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_n1p_bap' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_bap_n1p' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_d1p_dop' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_d2p_dop' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_dop_p3p' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_bap_z1p' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_bap_z2p' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_z1p_z2p' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.

               allocate ( var_list(10) )
               var_list = (/'n1p','p1p','p2p','p3p','d1p','d2p','dop','z1c','z2c','bac'/)
               do n = 1, 10
                  str = var_list(n)
                  where ('f_mu_'//str  .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
                  where ('f_mv_'//str  .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
                  where ('f_mw_'//str  .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
                  where ('f_tu_'//str  .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
                  where ('f_tv_'//str  .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
                  where ('f_tw_'//str  .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
                  where ('f_riv_'//str .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
                  where ('f_dil_'//str .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
                  where ('f_pev_'//str .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
                  where ('f_res_'//str .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
#ifdef module_sediment
                  where ('f_'//str//'_sed' .eq. 'f_'//sedo_flux_name(:)) sedo_flux_out2D(:) = .true.
#endif
               enddo
               deallocate (var_list)
#ifdef module_sediment
              ! NOTE: these are 2D arrays affecting 2D-output list !!!
               where ('sd_pop' .eq. sd_name(:)) sd_out2D(:) = .true.
               where ('f_sed_n1p' .eq. 'f_'//sedi_flux_name(:)) sedi_flux_out2D(:) = .true.
#endif
#endif
#else
               ierr = 1
               error_message = 'ERROR: output for TBNT-tracing requires compilation with flag -DTBNT_output'
               call stop_ecoham(ierr, msg=error_message)
#endif
            case ('TBNT-O','TBNT-O2')
#ifdef TBNT_output
               ot3D_out(ivol) = .true.
#ifdef module_biogeo
               where ( 'o2o'       .eq. st_name(:) )        st_out3D(:)   = .true.
               where ( 'f_air_o2o' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ( 'f_o2o_bac' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ( 'f_o2o_brm' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ( 'f_o2o_n4n' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ( 'f_o2o_z1c' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ( 'f_o2o_z2c' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ( 'f_p1c_o2o' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ( 'f_p2c_o2o' .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.

               str = 'o2o'
               where ('f_mu_'//str  .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_mv_'//str  .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_mw_'//str  .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_tu_'//str  .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_tv_'//str  .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_tw_'//str  .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_riv_'//str .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_dil_'//str .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_pev_'//str .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               where ('f_res_'//str .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
#ifdef module_sediment
              ! NOTE: these are 2D arrays affecting 2D-output list !!!
               where ('f_'//str//'_sed' .eq. 'f_'//sedo_flux_name(:)) sedo_flux_out2D(:) = .true.
#endif
#endif
#else
               ierr = 1
               error_message = 'ERROR: output for TBNT-tracing requires compilation with flag -DTBNT_output'
               call stop_ecoham(ierr, msg=error_message)
#endif
            case default
               ! scan for match in statevars
               where( var3D_name .eq. st_name(:) ) st_out3D(:) = .true.
               ! scan for match in derivedvars
               where( var3D_name .eq. de_name(:) ) de_out3D(:) = .true.
#ifdef module_chemie
               ! scan for match in chemievars
               where( var3D_name .eq. ch_name(:) ) ch_out3D(:) = .true.
#endif
               ! scan for match in pelagic fluxes
               where( var3D_name .eq. 'f_'//flux_name(:)) flux_out3D(:) = .true.
               ! scan for match in othervars
               ! ot-vars may not be treated the same way as above,
               ! because ot3D_name(:) and ot3D_out(:) may have different lenghts
               do n = 1, n_ot3D_var
                  if (var3D_name .eq. ot3D_name(n)) ot3D_out(n) = .true.
               enddo
         end select
         n_out = n_out + 1
      enddo
      91 continue
      close(nml_unit)

#ifdef exclude_p1x
      ! exclude output for 1st phytoplankton group
      where( index(st_name,'p1') /=0 ) st_out3D = .false.
      where( index(de_name,'p1') /=0 ) de_out3D = .false.
      where( index(flux_name,'p1') /=0 ) flux_out3D = .false.
      do n = 1, n_ot3D_var
         if ( index(ot3D_name(n),'p1') /=0 ) ot3D_out(n) = .false.
      enddo
#endif
#ifdef exclude_p2x
      ! exclude output for 2nd phytoplankton group
      where( index(st_name,'p2') /=0 ) st_out3D = .false.
      where( index(de_name,'p2') /=0 ) de_out3D = .false.
      where( index(flux_name,'p2') /=0 ) flux_out3D = .false.
      do n = 1, n_ot3D_var
         if ( index(ot3D_name(n),'p2') /=0 ) ot3D_out(n) = .false.
      enddo
#endif
#ifdef exclude_p3x
      ! exclude output for 3rd phytoplankton group
      where( index(st_name,'p3') /=0 ) st_out3D = .false.
      where( index(de_name,'p3') /=0 ) de_out3D = .false.
      where( index(flux_name,'p3') /=0 ) flux_out3D = .false.
      do n = 1, n_ot3D_var
         if ( index(ot3D_name(n),'p3') /=0 ) ot3D_out(n) = .false.
      enddo
#endif

      ! set temperature, salinity and X1X if nothing else choosen !!!
      if (n_str3D_out < 1) then
         st_out3D(ix1x)  = .true.
         ot3D_out(itemp) = .true.
         ot3D_out(isalt) = .true.
      endif

      ! specify array size for 2D-output
      n_full2D = count(st_out2D) +count(de_out2D) +count(flux_out2D)
#ifdef module_chemie
      n_full2D = n_full2D +count(ch_out2D)
#endif
#ifdef module_sediment
      n_full2D = n_full2D +count(sd_out2D) +count(sedo_flux_out2D) +count(sedi_flux_out2D)
#endif
      n_full2D = n_full2D +count(ot2D_out)

      ! allocate and initialize 2D-output metadata
      allocate( full2D_name(n_full2D) ); full2D_name(:) = ''
      allocate( full2D_long(n_full2D) ); full2D_long(:) = ''
      allocate( full2D_unit(n_full2D) ); full2D_unit(:) = ''

      if (n_full2D .gt. 0) then
         n_out = 0
         ! collect metadata from statevars
         do n = 1, n_st_var
            if (st_out2D(n)) then
               n_out = n_out +1
               full2D_name(n_out) = trim('int_'//st_name(n))
               full2D_long(n_out) = trim(st_long(n))
               full2D_unit(n_out) = trim(st_unit(n))
            endif
         enddo
#ifdef module_biogeo
         ! collect metadata from derivedvars
         do n = 1, n_de_var
            if (de_out2D(n)) then
               n_out = n_out +1
               full2D_name(n_out) = trim('int_'//de_name(n))
               full2D_long(n_out) = trim(de_long(n))
               full2D_unit(n_out) = trim(de_unit(n))
            endif
         enddo
#endif
#ifdef module_chemie
      ! collect metadata from chemievars
         do n = 1, n_ch_var
            if (ch_out2D(n)) then
               n_out = n_out +1
               full2D_name(n_out) = trim('int_'//ch_name(n))
               full2D_long(n_out) = trim(ch_long(n))
               full2D_unit(n_out) = trim(ch_unit(n))
            endif
         enddo
#endif
         ! collect metadata for pelagic fluxes
         do n = 1, max_flux_out
            if (flux_out2D(n)) then
               n_out = n_out +1
               full2D_name(n_out) = trim('int_f_'//flux_name(n))
               full2D_long(n_out) = trim(flux_long(n))
               full2D_unit(n_out) = trim(flux_unit(n))
            endif
         enddo
#ifdef module_sediment
         ! collect metadata from sedimentvars
         do n = 1, n_sed_var
            if (sd_out2D(n)) then
               n_out = n_out +1
               full2D_name(n_out) = trim(sd_name(n))
               full2D_long(n_out) = trim(sd_long(n))
               full2D_unit(n_out) = trim(sd_unit(n))
            endif
         enddo
         ! collect metadata for fluxes from  pelagic to sediment
         do n = 1, n_st_var
            if (sedo_flux_out2D(n)) then
               n_out = n_out +1
               full2D_name(n_out) = trim('f_'//sedo_flux_name(n))
               full2D_long(n_out) = trim(sedo_flux_long(n))
               full2D_unit(n_out) = trim(sedo_flux_unit(n))
            endif
         enddo
         ! collect metadata for fluxes from sediment to pelagic
         do n = 1, max_flux_sed
            if (sedi_flux_out2D(n)) then
               n_out = n_out +1
               full2D_name(n_out) = trim('f_'//sedi_flux_name(n))
               full2D_long(n_out) = trim(sedi_flux_long(n))
               full2D_unit(n_out) = trim(sedi_flux_unit(n))
            endif
         enddo
#endif
        ! collect metadata from 2D othervars
         do n = 1, n_ot2D_var
            if (ot2D_out(n)) then
               n_out = n_out +1
               full2D_name(n_out) = trim(ot2D_name(n))
               full2D_long(n_out) = trim(ot2D_long(n))
               full2D_unit(n_out) = trim(ot2D_unit(n))
            endif
         enddo
         do n = 1, n_out
            if (index(full2D_unit(n),'m-3') /= 0) then
               full2D_unit(n)(index(full2D_unit(n),'m-3'):index(full2D_unit(n),'m-3')+2) = 'm-2'
            endif
         enddo

         ! check for consistency of array size
         if (n_out .ne. n_full2D) then
            ierr = 1
            error_message = 'ERROR: couter n_out does not match array size of n_full2D'
            call stop_ecoham(ierr, msg=error_message)
         endif

! do n = 1, n_full2D
!    print*,trim(full2D_name(n))
!    print*,trim(full2D_long(n))
!    print*,trim(full2D_unit(n))
!    print*,line_separator
! enddo
! call stop_ecoham(1, msg='output3D#267')

         call write_log_message('   output variables (2D):')
         do n1 = 1, n_full2D, 5
            n2 = min(n1+4, n_full2D)
            write(long_msg,'(4x,10(x,a))') (full2D_name(n),n=n1,n2)
            call write_log_message(trim(long_msg))
         enddo
         long_msg = ''

      endif ! if (n_full2D .gt. 0)

      ! specify array size for 3D-output
      n_full3D = count(st_out3D) +count(de_out3D) +count(flux_out3D)
#ifdef module_chemie
      n_full3D = n_full3D +count(ch_out3D)
#endif
      n_full3D = n_full3D +count(ot3D_out)

      ! allocate and initialize 3D-output metadata
      allocate( full3D_name(n_full3D) ); full3D_name(:) = ''
      allocate( full3D_long(n_full3D) ); full3D_long(:) = ''
      allocate( full3D_unit(n_full3D) ); full3D_unit(:) = ''
      if (n_full3D .gt. 0) then
         n_out = 0
         ! collect metadata from statevars
         do n = 1, n_st_var
            if (st_out3D(n)) then
               n_out = n_out +1
               full3D_name(n_out) = trim(st_name(n))
               full3D_long(n_out) = trim(st_long(n))
               full3D_unit(n_out) = trim(st_unit(n))
            endif
         enddo
#ifdef module_biogeo
         ! collect metadata from derivedvars
         do n = 1, n_de_var
            if (de_out3D(n)) then
               n_out = n_out +1
               full3D_name(n_out) = trim(de_name(n))
               full3D_long(n_out) = trim(de_long(n))
               full3D_unit(n_out) = trim(de_unit(n))
            endif
         enddo
#endif
#ifdef module_chemie
      ! collect metadata from chemievars
         do n = 1, n_ch_var
            if (ch_out3D(n)) then
               n_out = n_out +1
               full3D_name(n_out) = trim(ch_name(n))
               full3D_long(n_out) = trim(ch_long(n))
               full3D_unit(n_out) = trim(ch_unit(n))
            endif
         enddo
#endif
         ! collect metadata for pelagic fluxes
         do n = 1, max_flux_out
            if (flux_out3D(n)) then
               n_out = n_out +1
               full3D_name(n_out) = trim('f_'//flux_name(n))
               full3D_long(n_out) = trim(flux_long(n))
               full3D_unit(n_out) = trim(flux_unit(n))
               if (index(full3D_unit(n_out),'m-3') /= 0) then ! 3D-flux-output will be interpolated by depth per layer
                  full3D_unit(n_out)(index(full3D_unit(n_out),'m-3'):index(full3D_unit(n_out),'m-3')+2) = 'm-2'
               endif
            endif
         enddo
         ! collect metadata from othervars
         do n = 1, n_ot3D_var
            if (ot3D_out(n)) then
               n_out = n_out +1
               full3D_name(n_out) = trim(ot3D_name(n))
               full3D_long(n_out) = trim(ot3D_long(n))
               full3D_unit(n_out) = trim(ot3D_unit(n))
            endif
         enddo

         ! check for consistency of array size
         if (n_out .ne. n_full3D) then
            ierr = 1
            error_message = 'ERROR: couter n_out does not match array size of n_full3D'
            call stop_ecoham(ierr, error_message)
         endif

! do n = 1, n_full3D
!    print*,trim(full3D_name(n))
!    print*,trim(full3D_long(n))
!    print*,trim(full3D_unit(n))
!    print*,line_separator
! enddo
! call stop_ecoham(1, msg='output3D#312')

         call write_log_message('   output variables (3D):')
         do n1 = 1, n_full3D, 5
            n2 = min(n1+4, n_full3D)
            write(long_msg,'(4x,10(x,a))') (full3D_name(n),n=n1,n2)
            call write_log_message(trim(long_msg))
         enddo
         long_msg = ''

      !write(*,'(a)') line_separator
      endif !if (n_full3D .gt. 0)

#ifdef NETCDF
      filename = trim(filename)//'.nc'
      call write_log_message('   write output to NETCDF-file: '//trim(filename))

      ! allocate and initialize netcdf 2D-output metadata
      nc_out2D = n_full2D
      allocate( nc_name2D(nc_out2D) ); nc_name2D(:) = ''
      allocate( nc_long2D(nc_out2D) ); nc_long2D(:) = ''
      allocate( nc_unit2D(nc_out2D) ); nc_unit2D(:) = ''
      if (nc_out2D .gt. 0) then
         do n = 1, nc_out2D
            nc_name2D(n) = trim(full2D_name(n))
            nc_long2D(n) = trim(full2D_long(n))
            nc_unit2D(n) = trim(full2D_unit(n))
         enddo
      endif

      ! allocate and initialize netcdf 3D-output metadata
      nc_out3D = n_full3D
      allocate( nc_name3D(nc_out3D) ); nc_name3D(:) = ''
      allocate( nc_long3D(nc_out3D) ); nc_long3D(:) = ''
      allocate( nc_unit3D(nc_out3D) ); nc_unit3D(:) = ''
      if (nc_out3D .gt. 0) then
         do n = 1, nc_out3D
            nc_name3D(n) = trim(full3D_name(n))
            nc_long3D(n) = trim(full3D_long(n))
            nc_unit3D(n) = trim(full3D_unit(n))
         enddo
      endif

      ! if (nc_out2D > 0) then
      !    write(*,'(a)') ' output variables (2D): '
      !    write(*,'(3x,5a20)') nc_name2D
      ! endif
      ! if (nc_out3D > 0) then
      !    write(*,'(a)') ' output variables (3D): '
      !    write(*,'(3x,5a20)') nc_name3D
      ! endif
      ! write(*,'(a)') line_separator

      ! initialize netcdf-file
      call init_ncdf(trim(filename),trim(title),trim(time_str),ierr)
      if (ierr/=0) then
         write( error_message, '("[",i2.2,"] ERROR - initializing netcdf output: ",i3)') myID,ierr
         call stop_ecoham(ierr, msg=error_message)
      end if

      ! deallocate arrays no longer needed
      deallocate( nc_name2D, nc_long2D, nc_unit2D )
      deallocate( nc_name3D, nc_long3D, nc_unit3D )
#endif

      ! deallocate arrays no longer needed
      deallocate( full2D_name, full2D_long, full2D_unit )
      deallocate( full3D_name, full3D_long, full3D_unit )
   endif ! master

#ifdef NETCDF
   ! deallocate temporary output arrays
   deallocate( dz_master, area_master, vol_master )
#endif


#ifdef MPI
   ! broadcast dimensions of full2D / full3D arrays
   call MPI_Bcast( n_full2D,        1,            MPI_INT,     master, MPI_COMM_ECOHAM, ierr_mpi ); ierr = ierr + ierr_mpi
   call MPI_Bcast( n_full3D,        1,            MPI_INT,     master, MPI_COMM_ECOHAM, ierr_mpi ); ierr = ierr + ierr_mpi
   call MPI_Bcast( st_out2D,        n_st_var,     MPI_LOGICAL, master, MPI_COMM_ECOHAM, ierr_mpi ); ierr = ierr + ierr_mpi
   call MPI_Bcast( st_out3D,        n_st_var,     MPI_LOGICAL, master, MPI_COMM_ECOHAM, ierr_mpi ); ierr = ierr + ierr_mpi
   call MPI_Bcast( de_out2D,        n_de_var,     MPI_LOGICAL, master, MPI_COMM_ECOHAM, ierr_mpi ); ierr = ierr + ierr_mpi
   call MPI_Bcast( de_out3D,        n_de_var,     MPI_LOGICAL, master, MPI_COMM_ECOHAM, ierr_mpi ); ierr = ierr + ierr_mpi
   call MPI_Bcast( flux_out2D,      max_flux_out, MPI_LOGICAL, master, MPI_COMM_ECOHAM, ierr_mpi ); ierr = ierr + ierr_mpi
   call MPI_Bcast( flux_out3D,      max_flux_out, MPI_LOGICAL, master, MPI_COMM_ECOHAM, ierr_mpi ); ierr = ierr + ierr_mpi
   call MPI_Bcast( ot2D_out,        n_ot2D_var,   MPI_LOGICAL, master, MPI_COMM_ECOHAM, ierr_mpi ); ierr = ierr + ierr_mpi
   call MPI_Bcast( ot3D_out,        n_ot3D_var,   MPI_LOGICAL, master, MPI_COMM_ECOHAM, ierr_mpi ); ierr = ierr + ierr_mpi
#ifdef module_chemie
   call MPI_Bcast( ch_out2D,        n_ch_var,     MPI_LOGICAL, master, MPI_COMM_ECOHAM, ierr_mpi ); ierr = ierr + ierr_mpi
   call MPI_Bcast( ch_out3D,        n_ch_var,     MPI_LOGICAL, master, MPI_COMM_ECOHAM, ierr_mpi ); ierr = ierr + ierr_mpi
#endif
#ifdef module_sediment
   call MPI_Bcast( sd_out2D,        n_sed_var,    MPI_LOGICAL, master, MPI_COMM_ECOHAM, ierr_mpi ); ierr = ierr + ierr_mpi
   call MPI_Bcast( sedo_flux_out2D, n_st_var,     MPI_LOGICAL, master, MPI_COMM_ECOHAM, ierr_mpi ); ierr = ierr + ierr_mpi
   call MPI_Bcast( sedi_flux_out2D, max_flux_sed, MPI_LOGICAL, master, MPI_COMM_ECOHAM, ierr_mpi ); ierr = ierr + ierr_mpi
#endif
#endif

   if (ierr/=0) then
      write( error_message, '("[",i2.2,"] ERROR - init_output_3D: ",i3)') myID,ierr
      call stop_ecoham(ierr, msg=error_message);
   end if
   return

   end subroutine init_output_3D

!=======================================================================
! !INTERFACE:
   subroutine do_output_3D(ierr)
#define SUBROUTINE_NAME 'do_output_3D'
!
! !DESCRIPTION:
!
! !USES:
   use mod_utils,      only: DOYstring
   implicit none
!
! !INPUT PARAMETERS:
   integer, intent(inout) :: ierr
!
! !LOCAL VARIABLES:
!    real, dimension(jmax,iStartD:iEndD)      :: dum2d
!    real, dimension(jmax,kmax,iStartD:iEndD) :: dum3d
! #ifdef MPI
!    real, dimension(:,:,:), allocatable        :: buffer
! #endif
   integer :: n, m, n_out
!
!-----------------------------------------------------------------------
#include "call-trace.inc"

   ! allocate output arrays
   allocate( full2D(jmax,imax,n_full2D) )

   ! collect 2D-output data
   if (n_full2D .gt. 0) then
!----------------------------------------------------------------------
!      !!!!! IMPORTANT !!!!!
! the order of collecting data must match the order collecting metadata
!----------------------------------------------------------------------
      n_out = 0
      ! collect output statevars
      do n = 1, n_st_var
         if (st_out2D(n)) then
            n_out = n_out +1
            full2D(:,iStartD:iEndD,n_out) = sum( st(:,:,:,n) *dz(:,:,:), 2, ixistZ==1)
            where(ixistK(:,iStartD:iEndD)==0) full2D(:,iStartD:iEndD,n_out) = fail
         endif
      enddo
#ifdef module_biogeo
      ! collect output derivedvars
      do n = 1, n_de_var
         if (de_out2D(n)) then
            n_out = n_out +1
            full2D(:,iStartD:iEndD,n_out) = sum( de(:,:,:,n) *dz(:,:,:), 2, ixistZ==1)
            where(ixistK(:,iStartD:iEndD)==0) full2D(:,iStartD:iEndD,n_out) = fail
         endif
      enddo
#endif
#ifdef module_chemie
      ! collect output chemievars
      do n = 1, n_ch_var
         if (ch_out2D(n)) then
            n_out = n_out +1
            full2D(:,iStartD:iEndD,n_out) = sum( ch(:,:,:,n) *dz(:,:,:), 2, ixistZ==1)
            where(ixistK(:,iStartD:iEndD)==0) full2D(:,iStartD:iEndD,n_out) = fail
         endif
      enddo
#endif
      ! collect pelagic output fluxes
      do n = 1, max_flux
         if (flux_out2D(n)) then
            n_out = n_out +1
            full2D(:,iStartD:iEndD,n_out) = sum( d_from_to(:,:,:,n), 2, ixistZ==1)
            where(ixistK(:,iStartD:iEndD)==0) full2D(:,iStartD:iEndD,n_out) = fail
         endif
      enddo
      ! collect hydrodynamic fluxes
      m = max_flux
      do n = 1, n_st_var !adv
         if (flux_out2D(m+n)) then
            n_out = n_out +1
            full2D(:,iStartD:iEndD,n_out) = sum( d_adv(:,:,:,n), 2, ixistZ==1)
            where(ixistK(:,iStartD:iEndD)==0) full2D(:,iStartD:iEndD,n_out) = fail
         endif
      enddo
      m = max_flux + n_st_var
      do n = 1, n_st_var !adh
         if (flux_out2D(m+n)) then
            n_out = n_out +1
            full2D(:,iStartD:iEndD,n_out) = sum( d_adh(:,:,:,n), 2, ixistZ==1)
            where(ixistK(:,iStartD:iEndD)==0) full2D(:,iStartD:iEndD,n_out) = fail
         endif
      enddo
      m = max_flux + n_st_var*2
      do n = 1, n_st_var !mxv
         if (flux_out2D(m+n)) then
            n_out = n_out +1
            full2D(:,iStartD:iEndD,n_out) = sum( d_mxv(:,:,:,n), 2, ixistZ==1)
            where(ixistK(:,iStartD:iEndD)==0) full2D(:,iStartD:iEndD,n_out) = fail
         endif
      enddo
      m = max_flux + n_st_var*3
      do n = 1, n_st_var !mxh
         if (flux_out2D(m+n)) then
            n_out = n_out +1
            full2D(:,iStartD:iEndD,n_out) = sum( d_mxh(:,:,:,n), 2, ixistZ==1)
            where(ixistK(:,iStartD:iEndD)==0) full2D(:,iStartD:iEndD,n_out) = fail
         endif
      enddo
      m = max_flux + n_st_var*4
      do n = 1, n_st_var !hyd
         if (flux_out2D(m+n)) then
            n_out = n_out +1
            full2D(:,iStartD:iEndD,n_out) = sum( d_hyd(:,:,:,n), 2, ixistZ==1)
            where(ixistK(:,iStartD:iEndD)==0) full2D(:,iStartD:iEndD,n_out) = fail
         endif
      enddo
      m = max_flux + n_st_var*5
      do n = 1, n_st_var !riv
         if (flux_out2D(m+n)) then
            n_out = n_out +1
            full2D(:,iStartD:iEndD,n_out) = sum( d_riv(:,:,:,n), 2, ixistZ==1)
            where(ixistK(:,iStartD:iEndD)==0) full2D(:,iStartD:iEndD,n_out) = fail
         endif
      enddo
      m = max_flux + n_st_var*6
      do n = 1, n_st_var !dil
         if (flux_out2D(m+n)) then
            n_out = n_out +1
            full2D(:,iStartD:iEndD,n_out) = sum( d_dil(:,:,:,n), 2, ixistZ==1)
             where(ixistK(:,iStartD:iEndD)==0) full2D(:,iStartD:iEndD,n_out) = fail
         endif
      enddo
      m = max_flux + n_st_var*7
      do n = 1, n_st_var !pev
         if (flux_out2D(m+n)) then
            n_out = n_out +1
            full2D(:,iStartD:iEndD,n_out) = sum( d_pev(:,:,:,n), 2, ixistZ==1)
            where(ixistK(:,iStartD:iEndD)==0) full2D(:,iStartD:iEndD,n_out) = fail
         endif
      enddo
      m = max_flux + n_st_var*8
      do n = 1, n_st_var !res
         if (flux_out2D(m+n)) then
            n_out = n_out +1
            full2D(:,iStartD:iEndD,n_out) = sum( d_res(:,:,:,n), 2, ixistZ==1)
            where(ixistK(:,iStartD:iEndD)==0) full2D(:,iStartD:iEndD,n_out) = fail
         endif
      enddo
#ifdef TBNT_output
      m = max_flux + n_st_var*9
      do n = 1, n_st_var !tu
         if (flux_out2D(m+n)) then
            n_out = n_out +1
            full2D(:,iStartD:iEndD,n_out) = sum(d_tu(:,:,:,n), 2, ixistZ==1)
            where(ixistK(:,iStartD:iEndD)==0) full2D(:,iStartD:iEndD,n_out) = fail
         endif
      enddo
      m = max_flux + n_st_var*10
      do n = 1, n_st_var !tv
         if (flux_out2D(m+n)) then
            n_out = n_out +1
            full2D(:,iStartD:iEndD,n_out) = sum(d_tv(:,:,:,n), 2, ixistZ==1)
            where(ixistK(:,iStartD:iEndD)==0) full2D(:,iStartD:iEndD,n_out) = fail
         endif
      enddo
      m = max_flux + n_st_var*11
      do n = 1, n_st_var !tw
         if (flux_out2D(m+n)) then
            n_out = n_out +1
            full2D(:,iStartD:iEndD,n_out) = sum(d_tw(:,:,:,n), 2, ixistZ==1)
            where(ixistK(:,iStartD:iEndD)==0) full2D(:,iStartD:iEndD,n_out) = fail
         endif
      enddo
      m = max_flux + n_st_var*12
      do n = 1, n_st_var !mu
         if (flux_out2D(m+n)) then
            n_out = n_out +1
            full2D(:,iStartD:iEndD,n_out) = sum(d_mu(:,:,:,n), 2, ixistZ==1)
            where(ixistK(:,iStartD:iEndD)==0) full2D(:,iStartD:iEndD,n_out) = fail
         endif
      enddo
      m = max_flux + n_st_var*13
      do n = 1, n_st_var !mv
         if (flux_out2D(m+n)) then
            n_out = n_out +1
            full2D(:,iStartD:iEndD,n_out) = sum(d_mv(:,:,:,n), 2, ixistZ==1)
            where(ixistK(:,iStartD:iEndD)==0) full2D(:,iStartD:iEndD,n_out) = fail
         endif
      enddo
      m = max_flux + n_st_var*14
      do n = 1, n_st_var !mw
         if (flux_out2D(m+n)) then
            n_out = n_out +1
            full2D(:,iStartD:iEndD,n_out) = sum(d_mw(:,:,:,n), 2, ixistZ==1)
            where(ixistK(:,iStartD:iEndD)==0) full2D(:,iStartD:iEndD,n_out) = fail
         endif
      enddo
#endif
#ifdef module_sediment
      ! collect output sedimentvars
      do n = 1, n_sed_var
         if (sd_out2D(n)) then
            n_out = n_out +1
            full2D(:,iStartD:iEndD,n_out) = sd(:,:,n)
            where(ixistK(:,iStartD:iEndD)==0) full2D(:,iStartD:iEndD,n_out) = fail
         endif
      enddo
      ! collect output fluxes from  pelagic to sediment
      do n = 1, n_st_var
         if (sedo_flux_out2D(n)) then
            n_out = n_out +1
            full2D(:,iStartD:iEndD,n_out) = sedo_flux(:,:,n)
            where(ixistK(:,iStartD:iEndD)==0) full2D(:,iStartD:iEndD,n_out) = fail
         endif
      enddo
      ! collect output fluxes from sediment to pelagic
      do n = 1, max_flux_sed
         if (sedi_flux_out2D(n)) then
            n_out = n_out +1
            full2D(:,iStartD:iEndD,n_out) = sedi_flux(:,:,n)
            where(ixistK(:,iStartD:iEndD)==0) full2D(:,iStartD:iEndD,n_out) = fail
         endif
      enddo
#endif
      ! collect output 2D othervars
      do n = 1, n_ot2D_var
         if (ot2D_out(n)) then
            n_out = n_out +1
            full2D(:,iStartD:iEndD,n_out) = ot2D(:,:,n)
            where(ixistK(:,iStartD:iEndD)==0) full2D(:,iStartD:iEndD,n_out) = fail
         endif
      enddo
      ! check for consistency of array size
      if (n_out .ne. n_full2D) then
         ierr = 1
         write( log_msg(1),'("[",i2.2,"] ERROR: ",a)') myID, &
                'not all indices addressed for writing output data to full2D array'
         write( log_msg(2),'("n_out:",i3,3x,"n_full2D:",i3 )') n_out, n_full2D
         long_msg = trim( log_msg(1)) // new_line('a') // trim( log_msg(2))
         call stop_ecoham(ierr, msg=long_msg); log_msg = ''
      endif
   endif !if (n_full2D .gt. 0)

   ! allocate output arrays
   allocate( full3D(jmax,kmax,imax,n_full3D) )
   ! allocate and collect 3D-output data
   if (n_full3D .gt. 0) then
!----------------------------------------------------------------------
!      !!!!! IMPORTANT !!!!!
! the order of collecting data must match the order collecting metadata
!----------------------------------------------------------------------
      n_out = 0
      ! collect output statevars
      do n = 1, n_st_var
         if (st_out3D(n)) then
            n_out = n_out +1
            where(ixistZ(:,:,iStartD:iEndD)==1)full3D(:,:,iStartD:iEndD,n_out) = st(:,:,:,n)
            where(ixistZ(:,:,iStartD:iEndD)==0) full3D(:,:,iStartD:iEndD,n_out) = fail
         endif
      enddo
      ! collect output derivedvars
#ifdef module_biogeo
      do n = 1, n_de_var
         if (de_out3D(n)) then
            n_out = n_out +1
            where(ixistZ(:,:,iStartD:iEndD)==1)full3D(:,:,iStartD:iEndD,n_out) = de(:,:,:,n)
            where(ixistZ(:,:,iStartD:iEndD)==0) full3D(:,:,iStartD:iEndD,n_out) = fail
         endif
      enddo
#endif
#ifdef module_chemie
      ! collect output chemievars
      do n = 1, n_ch_var
         if (ch_out3D(n)) then
            n_out = n_out +1
            where(ixistZ(:,:,iStartD:iEndD)==1)full3D(:,:,iStartD:iEndD,n_out) = ch(:,:,:,n)
            where(ixistZ(:,:,iStartD:iEndD)==0) full3D(:,:,iStartD:iEndD,n_out) = fail
         endif
      enddo
#endif
      ! collect pelagic output fluxes
      do n = 1, max_flux
         if (flux_out3D(n)) then
            n_out = n_out +1
            where(ixistZ(:,:,iStartD:iEndD)==1)full3D(:,:,iStartD:iEndD,n_out) = d_from_to(:,:,:,n)
            where(ixistZ(:,:,iStartD:iEndD)==0) full3D(:,:,iStartD:iEndD,n_out) = fail
         endif
      enddo
      ! collect hydrodynamic fluxes
      m = max_flux
      do n = 1, n_st_var !adv
         if (flux_out3D(m+n)) then
            n_out = n_out +1
            where(ixistZ(:,:,iStartD:iEndD)==1)full3D(:,:,iStartD:iEndD,n_out) = d_adv(:,:,:,n)
            where(ixistZ(:,:,iStartD:iEndD)==0) full3D(:,:,iStartD:iEndD,n_out) = fail
         endif
      enddo
      m = max_flux + n_st_var
      do n = 1, n_st_var !adh
         if (flux_out3D(m+n)) then
            n_out = n_out +1
            where(ixistZ(:,:,iStartD:iEndD)==1)full3D(:,:,iStartD:iEndD,n_out) = d_adh(:,:,:,n)
            where(ixistZ(:,:,iStartD:iEndD)==0) full3D(:,:,iStartD:iEndD,n_out) = fail
         endif
      enddo
      m = max_flux + n_st_var*2
      do n = 1, n_st_var !mxv
         if (flux_out3D(m+n)) then
            n_out = n_out +1
            where(ixistZ(:,:,iStartD:iEndD)==1)full3D(:,:,iStartD:iEndD,n_out) = d_mxv(:,:,:,n)
            where(ixistZ(:,:,iStartD:iEndD)==0) full3D(:,:,iStartD:iEndD,n_out) = fail
         endif
      enddo
      m = max_flux + n_st_var*3
      do n = 1, n_st_var !mxh
         if (flux_out3D(m+n)) then
            n_out = n_out +1
            where(ixistZ(:,:,iStartD:iEndD)==1)full3D(:,:,iStartD:iEndD,n_out) = d_mxh(:,:,:,n)
            where(ixistZ(:,:,iStartD:iEndD)==0) full3D(:,:,iStartD:iEndD,n_out) = fail
         endif
      enddo
      m = max_flux + n_st_var*4
      do n = 1, n_st_var !hyd
         if (flux_out3D(m+n)) then
            n_out = n_out +1
            where(ixistZ(:,:,iStartD:iEndD)==1)full3D(:,:,iStartD:iEndD,n_out) = d_hyd(:,:,:,n)
            where(ixistZ(:,:,iStartD:iEndD)==0) full3D(:,:,iStartD:iEndD,n_out) = fail
         endif
      enddo
      m = max_flux + n_st_var*5
      do n = 1, n_st_var !riv
         if (flux_out3D(m+n)) then
            n_out = n_out +1
            where(ixistZ(:,:,iStartD:iEndD)==1)full3D(:,:,iStartD:iEndD,n_out) = d_riv(:,:,:,n)
            where(ixistZ(:,:,iStartD:iEndD)==0) full3D(:,:,iStartD:iEndD,n_out) = fail
         endif
      enddo
      m = max_flux + n_st_var*6
      do n = 1, n_st_var !dil
         if (flux_out3D(m+n)) then
            n_out = n_out +1
            where(ixistZ(:,:,iStartD:iEndD)==1)full3D(:,:,iStartD:iEndD,n_out) = d_dil(:,:,:,n) *dz(:,:,:)
            where(ixistZ(:,:,iStartD:iEndD)==0) full3D(:,:,iStartD:iEndD,n_out) = fail
         endif
      enddo
      m = max_flux + n_st_var*7
      do n = 1, n_st_var !pev
         if (flux_out3D(m+n)) then
            n_out = n_out +1
            where(ixistZ(:,:,iStartD:iEndD)==1)full3D(:,:,iStartD:iEndD,n_out) = d_pev(:,:,:,n) *dz(:,:,:)
            where(ixistZ(:,:,iStartD:iEndD)==0) full3D(:,:,iStartD:iEndD,n_out) = fail
         endif
      enddo
      m = max_flux + n_st_var*8
      do n = 1, n_st_var !res
         if (flux_out3D(m+n)) then
            n_out = n_out +1
            where(ixistZ(:,:,iStartD:iEndD)==1)full3D(:,:,iStartD:iEndD,n_out) = d_res(:,:,:,n) *dz(:,:,:)
            where(ixistZ(:,:,iStartD:iEndD)==0) full3D(:,:,iStartD:iEndD,n_out) = fail
         endif
      enddo
#ifdef TBNT_output
      m = max_flux + n_st_var*9
      do n = 1, n_st_var !tu
         if (flux_out3D(m+n)) then
            n_out = n_out +1
            where(ixistZ(:,:,iStartD:iEndD)==1)full3D(:,:,iStartD:iEndD,n_out) = d_tu(:,:,:,n)
            where(ixistZ(:,:,iStartD:iEndD)==0) full3D(:,:,iStartD:iEndD,n_out) = fail
         endif
      enddo
      m = max_flux + n_st_var*10
      do n = 1, n_st_var !tv
         if (flux_out3D(m+n)) then
            n_out = n_out +1
            where(ixistZ(:,:,iStartD:iEndD)==1)full3D(:,:,iStartD:iEndD,n_out) = d_tv(:,:,:,n)
            where(ixistZ(:,:,iStartD:iEndD)==0) full3D(:,:,iStartD:iEndD,n_out) = fail
         endif
      enddo
      m = max_flux + n_st_var*11
      do n = 1, n_st_var !tw
         if (flux_out3D(m+n)) then
            n_out = n_out +1
            where(ixistZ(:,:,iStartD:iEndD)==1)full3D(:,:,iStartD:iEndD,n_out) = d_tw(:,:,:,n)
            where(ixistZ(:,:,iStartD:iEndD)==0) full3D(:,:,iStartD:iEndD,n_out) = fail
         endif
      enddo
      m = max_flux + n_st_var*12
      do n = 1, n_st_var !mu
         if (flux_out3D(m+n)) then
            n_out = n_out +1
            where(ixistZ(:,:,iStartD:iEndD)==1)full3D(:,:,iStartD:iEndD,n_out) = d_mu(:,:,:,n)
            where(ixistZ(:,:,iStartD:iEndD)==0) full3D(:,:,iStartD:iEndD,n_out) = fail
         endif
      enddo
      m = max_flux + n_st_var*13
      do n = 1, n_st_var !mv
         if (flux_out3D(m+n)) then
            n_out = n_out +1
            where(ixistZ(:,:,iStartD:iEndD)==1)full3D(:,:,iStartD:iEndD,n_out) = d_mv(:,:,:,n)
            where(ixistZ(:,:,iStartD:iEndD)==0) full3D(:,:,iStartD:iEndD,n_out) = fail
         endif
      enddo
      m = max_flux + n_st_var*14
      do n = 1, n_st_var !mw
         if (flux_out3D(m+n)) then
            n_out = n_out +1
            where(ixistZ(:,:,iStartD:iEndD)==1)full3D(:,:,iStartD:iEndD,n_out) = d_mw(:,:,:,n)
            where(ixistZ(:,:,iStartD:iEndD)==0) full3D(:,:,iStartD:iEndD,n_out) = fail
         endif
      enddo
#endif
      ! collect output othervars
      do n = 1, n_ot3D_var
         if (ot3D_out(n)) then
            n_out = n_out +1
            where(ixistZ(:,:,iStartD:iEndD)==1)full3D(:,:,iStartD:iEndD,n_out) = ot3D(:,:,:,n)
            where(ixistZ(:,:,iStartD:iEndD)==0) full3D(:,:,iStartD:iEndD,n_out) = fail
         endif
      enddo
      ! check for consistency of array size
      if (n_out .ne. n_full3D) then
         ierr = 1
         write( log_msg(1),'("[",i2.2,"] ERROR: ",a)') myID, &
                'not all indices addressed for writing output data to full3D array'
         write( log_msg(2),'("n_out:",i5,"n_full3D:",i5 )') n_out, n_full3D
         long_msg = trim( log_msg(1)) // new_line('a') // trim( log_msg(2))
         call stop_ecoham(ierr, msg=long_msg); log_msg = ''
      endif
   endif !if (n_full3D .gt. 0)

#ifdef MPI
   ! collect data from all domains
   do n = 1, n_full2D
      call mpi_parallel_gather(full2D(:,:,n),1,ierr_mpi)
      ierr = ierr + ierr_mpi
   enddo
   ! alternative using buffer ...
   ! allocate( buffer(jmax,n_full2D,imax) )
   ! do n = 1, n_full2D
   !    buffer(:,n,:) = full2D(:,:,n)
   ! enddo
   ! call mpi_parallel_gather(buffer(:,:,:),n_full2D,ierr_mpi)
   ! if (myID == master) then
   !    do n = 1, n_full2D
   !       full2D(:,:,n) = buffer(:,n,:)
   !    enddo
   ! endif
   ! deallocate( buffer )

   do n = 1, n_full3D
      call mpi_parallel_gather(full3D(:,:,:,n),kmax,ierr_mpi)
      ierr = ierr + ierr_mpi
   enddo

   ! check whether gather was fine
   if (ierr/=0) then
      write(error_message,'("ERROR - do_output: gathering data from domains failed")')
      call stop_ecoham(ierr, msg=error_message);
   endif
#endif

#ifdef NETCDF
   if (myID == master) then
      ! ---------- ? better / needed ? -------------
      ! allocate( nc_data2D(NLONS, NLATS, n_out2D) )
      ! allocate( nc_data3D(NLONS, NLATS, n_out3D) )
      ! nc_data2D(i,j,  n) <= full2D(j,i,  n)
      ! nc_data3D(i,j,k,n) <= full3D(j,k,i,n)
      ! --------------------------------------------
      ! currently done within mod_ncdfout because
      ! full2D/3D declaration within mod_common
      ! --------------------------------------------
      ! write data to netcdf
      write(long_msg,'(3x,"write data to netcdf for ",a19)') DOYstring(day_of_year)
      call write_log_message(long_msg); long_msg = ''
      call do_ncdf_out(nint(day_of_year*secs_per_day),ierr)
      if (ierr/=0) then
         write( error_message, '("[",i2.2,"] ERROR - writing data to netcdf: ",i3)') myID,ierr
         call stop_ecoham(ierr, msg=error_message)
      end if

      ! deallocate( nc_data2D )
      ! deallocate( nc_data3D )
   endif
#endif

   deallocate( full2D )
   deallocate( full3D )

   if (ierr/=0) then
      write( error_message, '("[",i2.2,"] ERROR - do_output_3D: ",i3)') myID, ierr
      call stop_ecoham(ierr, msg=error_message);
   end if
   return

   end subroutine do_output_3D

!=======================================================================
! !INTERFACE:
   subroutine close_output_3D(ierr)
#define SUBROUTINE_NAME 'close_output_3D'
!
! !DESCRIPTION:
!
! !USES:
   implicit none
!
! !INPUT PARAMETERS:
   integer, intent(inout) :: ierr
!
! !LOCAL VARIABLES:
!
!-----------------------------------------------------------------------
#include "call-trace.inc"

#ifdef NETCDF
   if (myID == master) then
      if (isw_dim==3) call close_ncdf(ierr)
   endif
#endif

   if (ierr/=0) then
      write( error_message, '("[",i2.2,"] ERROR - close_output_3D: ",i3)') myID, ierr
      call stop_ecoham(ierr, msg=error_message);
   end if
   return

   end subroutine close_output_3D

!=======================================================================
!
! !INTERFACE:
   subroutine scan_output_fluxes(str,flux_out,ierr)
#define SUBROUTINE_NAME 'scan_output_fluxes'
!
! !DESCRIPTION:
!
! !USES:
#ifdef module_meteo
   use mod_meteo, only : meteodilution
#endif
   implicit none
!
! !INPUT PARAMETERS:
   character(len=*),      intent(in)    :: str
   logical, dimension(:), intent(inout) :: flux_out
   integer,               intent(inout) :: ierr
!
! !LOCAL VARIABLES:
   character(len=7)   :: name
   integer            :: m, i1, i2
!
!-----------------------------------------------------------------------
#include "call-trace.inc"

   if (len_trim(str) == 1) then ! scan for element cycle
      ! scan for match in source / sink fluxes (excluding physics & restoring, but including river!)
      do m = 1, max_flux_out
      name = trim(flux_name(m))
      i1 = index(name, '_', .true.)-1
      i2 = len_trim(name)
      !if ( (index(name(i1),str)==1 .or.  index(name(i2),str)==1) .and. &
      if ( ( name(i1:i1)==str .or.  name(i2:i2)==str )          .and. &
            index(name, 'adv') ==0 .and. index(name, 'adh') ==0 .and. &
            index(name, 'mxv') ==0 .and. index(name, 'mxh') ==0 .and. &
            index(name, 'tu_') ==0 .and. index(name, 'tv_') ==0 .and. &
            index(name, 'tw_') ==0 .and. index(name, 'mu_') ==0 .and. &
            index(name, 'mv_') ==0 .and. index(name, 'mw_') ==0 .and. &
            index(name, 'hyd') ==0 .and. index(name, 'res') ==0 ) flux_out(m) = .true.
      enddo

      ! scan for match in transport / diffusion fluxes
#ifdef TBNT_output
      where( index(flux_name, str, .true.)  ==6 .and.                          &
            (index(flux_name,'tu_') /=0 .or.  index(flux_name,'tv_') /=0 .or.  &
            index(flux_name,'tw_') /=0 .or.  index(flux_name,'mu_') /=0 .or.  &
            index(flux_name,'mv_') /=0 .or.  index(flux_name,'mw_') /=0)) flux_out = .true.
#else
      where( index(flux_name, str, .true.)  ==7 .and.                          &
            (index(flux_name,'adv') /=0 .or.  index(flux_name,'adh') /=0 .or.  &
            index(flux_name,'mxv') /=0 .or.  index(flux_name,'mxh') /=0)) flux_out = .true.
#endif

   else ! scan for other keywords with len_trim(str)>1 (default)
      ! scan for match in source / sink fluxes (excluding physics & restoring, but including river!)
      where( index(flux_name, str)  /=0 .and. index(flux_name,'hyd') ==0 .and. &
            index(flux_name,'adv') ==0 .and. index(flux_name,'adh') ==0 .and. &
            index(flux_name,'mxv') ==0 .and. index(flux_name,'mxh') ==0 .and. &
            index(flux_name,'tu_') ==0 .and. index(flux_name,'tv_') ==0 .and. &
            index(flux_name,'tw_') ==0 .and. index(flux_name,'mu_') ==0 .and. &
            index(flux_name,'mv_') ==0 .and. index(flux_name,'mw_') ==0 .and. &
            index(flux_name,'res') ==0 ) flux_out = .true.
      ! scan for match in transport / diffusion fluxes
#ifdef TBNT_output
      where( index(flux_name, str)  /=0 .and.                                  &
            (index(flux_name,'tu_') /=0 .or.  index(flux_name,'tv_') /=0 .or.  &
            index(flux_name,'tw_') /=0 .or.  index(flux_name,'mu_') /=0 .or.  &
            index(flux_name,'mv_') /=0 .or.  index(flux_name,'mw_') /=0)) flux_out = .true.
#else
      where( index(flux_name, str)  /=0 .and.                                  &
            (index(flux_name,'adv') /=0 .or.  index(flux_name,'adh') /=0 .or.  &
            index(flux_name,'mxv') /=0 .or.  index(flux_name,'mxh') /=0)) flux_out = .true.
#endif
   endif

   if (meteodilution /= 1) then
      where( index(flux_name, 'pev') /=0 ) flux_out = .false.
   endif

   if (ierr/=0) then
      write( error_message, '("[",i2.2,"] ERROR - scan_output_fluxes: ",i3)') myID,ierr
      call stop_ecoham(ierr, msg=error_message)
   end if

   return

   end subroutine scan_output_fluxes

!=======================================================================

   end module mod_output_3D

!=======================================================================
