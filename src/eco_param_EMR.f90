#if defined module_sediment && defined sediment_EMR
!=======================================================================
   MODULE mod_param_EMR
!=======================================================================
!**** *MODULE mo_param1_bgc* - bgc tracer parameters.
!
!     Patrick Wetzel    *MPI-Met, HH*    01.09.03
!
!     Purpose
!     -------
!     - declaration and memory allocation
!**********************************************************************
! !USES:
     implicit none
!
!  default: all is public.
     public

! tracers in the pelagic
     INTEGER, PARAMETER ::                     isco212  =1
     INTEGER, PARAMETER ::                     ialkali  =2
     INTEGER, PARAMETER ::                     iphosph  =3
     INTEGER, PARAMETER ::                     ioxygen  =4
     INTEGER, PARAMETER ::                     igasnit  =5
     INTEGER, PARAMETER ::                     iano3    =6
     INTEGER, PARAMETER ::                     isilica  =7
     INTEGER, PARAMETER ::                     ianh4    =8
     INTEGER, PARAMETER ::                     i_base_adv=8
#ifdef __c_isotopes
     INTEGER, PARAMETER ::                     isco213  =i_base_adv+1
     INTEGER, PARAMETER ::                     isco214  =i_base_adv+2
     INTEGER, PARAMETER ::                     i_iso_adv=2
#else
     INTEGER, PARAMETER ::                     i_iso_adv=0
     INTEGER, PARAMETER ::                     isco213  =0
     INTEGER, PARAMETER ::                     isco214  =0
#endif
     INTEGER, PARAMETER :: ntraad = i_base_adv + i_iso_adv + 3
     
! solid components in the sediment
     INTEGER, PARAMETER ::                     issso12=1  ! POP (in mmol P m-3)
     INTEGER, PARAMETER ::                     isssc12=2
     INTEGER, PARAMETER ::                     issssil=3
     INTEGER, PARAMETER ::                     issster=4
     INTEGER, PARAMETER ::                     issspon=5
     INTEGER, PARAMETER ::                     issspoc=6
     INTEGER, PARAMETER ::                     nsss_base=6
#ifdef __c_isotopes
     INTEGER, PARAMETER ::                     issso13=nsss_base+1
     INTEGER, PARAMETER ::                     issso14=nsss_base+2
     INTEGER, PARAMETER ::                     isssc13=nsss_base+3
     INTEGER, PARAMETER ::                     isssc14=nsss_base+4
     INTEGER, PARAMETER ::                     nsss_iso = 4
#else
     INTEGER, PARAMETER ::                     issso13=0
     INTEGER, PARAMETER ::                     issso14=0
     INTEGER, PARAMETER ::                     isssc13=0
     INTEGER, PARAMETER ::                     isssc14=0
     INTEGER, PARAMETER ::                     nsss_iso = 0
#endif
      INTEGER, PARAMETER :: nsedtra = nsss_base + nsss_iso

! pore water tracers, index must be the same as for ocetra otherwise problems in dipowa.f90!
     INTEGER, PARAMETER ::                     ipowaic=1
     INTEGER, PARAMETER ::                     ipowaal=2
     INTEGER, PARAMETER ::                     ipowaph=3
     INTEGER, PARAMETER ::                     ipowaox=4
     INTEGER, PARAMETER ::                     ipown2 =5
     INTEGER, PARAMETER ::                     ipowno3=6
     INTEGER, PARAMETER ::                     ipowasi=7
     INTEGER, PARAMETER ::                     ipownh4=8
     INTEGER, PARAMETER ::                     npowa_base = 8
!   sd_n5s            sd_n3n            sd_ph
!     INTEGER, PARAMETER ::                     ipownh4=9
!     INTEGER, PARAMETER ::                     ipownh4=10
!     INTEGER, PARAMETER ::                     ipownh4=11
!     INTEGER, PARAMETER ::                     npowa_base = 11
#ifdef __c_isotopes
     INTEGER, PARAMETER ::                     ipowc13=npowa_base+1
     INTEGER, PARAMETER ::                     ipowc14=npowa_base+2
     INTEGER, PARAMETER ::                     npowa_iso = 2
#else
     INTEGER, PARAMETER ::                     ipowc13=0
     INTEGER, PARAMETER ::                     ipowc14=0
     INTEGER, PARAMETER ::                     npowa_iso = 0
#endif
      INTEGER, PARAMETER :: npowtra = npowa_base + npowa_iso + 3
!      INTEGER, PARAMETER :: npowtra = npowa_base + npowa_iso

!=======================================================================
!      MODULE MO_CHEM_CON
!=======================================================================
!
!**** *CHEMCON* - .
!
!     Ernst Maier-Reimer,    *MPI-Met, HH*    10.04.01
!
!     Modified
!     --------
!     S.Legutke,        *MPI-MaD, HH*    10.04.01
!     - rename: tracer(j,k,i)=ocetra(j,k,i,itracer)
!     - interfacing with ocean model.
!     - use of zonal mean temperature/salinity.
!     akw3(kpje,kpke),akb3(kpje,kpke),ak13(kpje,kpke),ak23(kpje,kpke)
!     aksp(kpje,kpke) again 3d fields! (EMR suggestion)
!
!     Patrick Wetzel,    *MPI-Met, HH*    15.04.02
!     - rename to CHEMCON
!     - chemc(j,i,7) to chemcm(j,i,8,12)
!
!     S.Lorenz/JO.Beismann, OpenMP parallel    *MPI-Met, HH*  24.08.07
!
!     Purpose
!     -------
!     Computes chemical constants in the surface layer (chemcm)
!     and in the water column (ak13, ak23, akb3, aksp)
!
!     Method
!     -------
!**   Interface to ocean model (parameter list):
!     -----------------------------------------
!
!     *INTEGER* *modal*   - .
!     *INTEGER* *jmax*    - 1st dimension of model grid.
!     *INTEGER* *ks*      - 2rd (vertical) dimension of model grid.
!     *INTEGER* *imax*    - 3nd dimension of model grid.
!     *REAL*    *psao*    - salinity [psu.].
!     *REAL*    *ptho*    - potential temperature [deg C].
!     *REAL*    *pddpo*   - size of scalar grid cell (3rd dimension) [m].
!     *REAL*    *pdlxp*   - size of scalar grid cell (1st dimension) [m].
!     *REAL*    *pdlyp*   - size of scalar grid cell (2nd dimension) [m].
!     *REAL*    *ptiestu* - level depths [m].
!     *INTEGER* *kplmonth*  - month in ocean model
!
!     Externals
!     ---------
!     .
!**********************************************************************


! kplmonth is 0 when chemcon is called from INI_BGC
!     -----------------------------------------------------------------
!*         3. SET CONVERSION FACTOR SALINITY -> CHLORINITY
!             ------ ------- -- ---- ------- -- ----------
!             (AFTER WOOSTER ET AL., 1969)
!
      REAL, PARAMETER :: SALCHL = 1./1.80655
!
!     -----------------------------------------------------------------
!*         5. SET MEAN TOTAL [CA++] IN SEAWATER (MOLES/KG)
!             (SEE BROECKER A. PENG, 1982, P. 26)
!             ([CA++](MOLES/KG)=1.026E-2*(S/35.) AFTER
!             CULKIN(1965), CF. BROECKER ET AL. 1982)
!             ------------- --- -------- -- --- -----
!
       REAL, PARAMETER :: calcon = 1.03e4  !wk: CaCO3 [mmol/m3]?
!
!     -----------------------------------------------------------------
!*         6. SET COEFFICIENTS FOR APPARENT SOLUBILITY EQUILIBRIUM
!             OF CALCITE (INGLE, 1800, EQ. 6)
!             -- ------- ------- ----- --- ----------- -----------
!
       REAL, PARAMETER :: akcc1 = -34.452
       REAL, PARAMETER :: akcc2 = -39.866
       REAL, PARAMETER :: akcc3 = 110.21
       REAL, PARAMETER :: akcc4 = -7.5752e-6
!
!     -----------------------------------------------------------------
!*        6A. SET FRACTION OF ARAGONITE IN BIOGENIC CACO3 PARTICLES
!             --- -------- --------- -- -------- ----- ---------
!
       REAL, PARAMETER :: arafra = 0.   ! js: obsolete
!
!     -----------------------------------------------------------------
!*        6B. FRACTION OF CALCITE IN BIOGENIC CACO3 PARTICLES
!             -------- ------- -- -------- ----- ---------
       REAL, PARAMETER :: calfra = 1. - arafra
       REAL, PARAMETER :: SUCALL = ARAFRA + CALFRA
!
!     -----------------------------------------------------------------
!*         7. FACTOR TO GET APPARENT SOLUBILITY PRODUCT FOR
!             ARAGONIT BY MULTIPLICATION WITH APPARENT SOLUBILITY
!             PRODUCT FOR CALCIT (CF. BERNER, 1976,
!             OR BROECKER ET AL., 1982)
!             -- -------- -- ---- ----- -- ------ --- ------- -----
      REAL, PARAMETER :: aracal = arafra * 1.45 + calfra
!
!     -----------------------------------------------------------------
!*         8. SET COEFFICIENTS FOR SEAWATER PRESSURE CORRECTION OF
!             (AFTER CULBERSON AND PYTKOWICZ, 1968, CF. BROECKER
!             ET AL., 1982)
!             ------- -------- --- ---------- ----- --- -------- -
!
      REAL, PARAMETER :: devk1 = 24.2
      REAL, PARAMETER :: devk2 = 16.4
      REAL, PARAMETER :: devkb = 27.5
      REAL, PARAMETER :: devk1t = 0.085
      REAL, PARAMETER :: devk2t = 0.04
      REAL, PARAMETER :: devkbt = 0.095
!
!     -----------------------------------------------------------------
!*         9. SET COEFFICIENTS FOR PRESSURE CORRECTION OF SOLUBILITY
!             PRODUCT OF CACO3 CORRESPONDING TO ARAGONITE/CALCITE
!             RATIO AFTER EDMOND AND GIESKES (1970),
!             P. 1285
!             -- ---- -- --------- ----- ------ --- ------- -------
!
      REAL, PARAMETER :: devkst = 0.23
      REAL, PARAMETER :: devks  = 32.8 * arafra + 35.4 * aracal
!
!     -----------------------------------------------------------------
!*        11. SET UNIVERSAL GAS CONSTANT
!             --- --------- --- --------
!
!      REAL, PARAMETER :: rgas = 83.143
!
!     -----------------------------------------------------------------
!*        12. SET BORON CONCENTRATION IN SEA WATER
!*            IN G/KG PER O/OO CL ACCORDING
!             TO RILEY AND SKIRROW, 1965 (P.250)
!             -- ----- --- -------- ---- ------- -
!
      REAL, PARAMETER :: bor1 = 0.00023
!
!     -----------------------------------------------------------------
!*        13. SET INVERSE OF ATOMIC WEIGHT OF BORON [G**-1]
!             (USED TO CONVERT SPECIFIC TOTAL BORAT INTO CONCENTRATIONS)
!             ----- -- ------- -------- ----- ----- ---- ---------------
!
      REAL, PARAMETER :: bor2 = 1./10.82
!
!     -----------------------------------------------------------------
!*        14. SET INVERS OF NORMAL MOLAL VOLUME OF AN IDEAL GAS
!             [CM**3]
!             ---------- -- ------ ----- ------ -- -- ----- ---
!
      REAL, PARAMETER :: oxyco = 1./22414.4
!
!     -----------------------------------------------------------------
!*        15. SET VOLUMETRIC SOLUBILITY CONSTANTS FOR CO2 IN ML/L
!             WEISS, R. F. (1974)
!             CARBON DIOXIDE IN WATER AND SEAWATER: THE SOLUBILITY OF A
!             NON IDEAL GAS. MARINE CHEMISTRY, VOL. 2, 203-215.
!     -----------------------------------------------------------------

      REAL, PARAMETER :: c00 = -58.0931          ! C null null
      REAL, PARAMETER :: c01 = 90.5069
      REAL, PARAMETER :: c02 = 22.2940
      REAL, PARAMETER :: c03 = 0.027766
      REAL, PARAMETER :: c04 = -0.025888
      REAL, PARAMETER :: c05 = 0.0050578
!
!     -----------------------------------------------------------------
!*        16. SET COEFF. FOR 1. DISSOC. OF CARBONIC ACID
!             (EDMOND AND GIESKES, 1970)
!             ------- --- -------- ------- -------- ----
!
      REAL, PARAMETER :: c10 = 812.27
      REAL, PARAMETER :: c11 = 3.356
      REAL, PARAMETER :: c12 = -0.00171
      REAL, PARAMETER :: c13 =  0.000091

!     -----------------------------------------------------------------
!*        17. SET COEFF. FOR 2. DISSOC. OF CARBONIC ACID
!             (EDMOND AND GIESKES, 1970)
!             ------- --- -------- ------- -------- ----
!
      REAL, PARAMETER :: c20 = 1450.87
      REAL, PARAMETER :: c21 = 4.604
      REAL, PARAMETER :: c22 = -0.00385
      REAL, PARAMETER :: c23 =  0.000182
!
!     -----------------------------------------------------------------
!*        18. SET COEFF. FOR 1. DISSOC. OF BORIC ACID
!             (EDMOND AND GIESKES, 1970)
!             ------- --- -------- ------- ----- ----
!
      REAL, PARAMETER :: cb0 = 2291.90
      REAL, PARAMETER :: cb1 = 0.01756
      REAL, PARAMETER :: cb2 = -3.385
      REAL, PARAMETER :: cb3 = -0.32051
!
!     -----------------------------------------------------------------
!*        19. SET COEFF. FOR DISSOC. OF WATER
!             (DICKSON AND RILEY, 1979, EQ. 7, COEFFICIENT
!             CW2 CORRECTED FROM 0.9415 TO 0.09415 AFTER
!             PERS. COMMUN. TO B. BACASTOW, 1988)
!             ----- ------- -- -- --------- ------ -------
!
      REAL, PARAMETER :: cw0 = 3441.
      REAL, PARAMETER :: cw1 = 2.241
      REAL, PARAMETER :: cw2 = -0.09415
!
!     -----------------------------------------------------------------
!*        20. SET VOLUMETRIC SOLUBILITY CONSTANTS FOR O2 IN ML/L
!             (WEISS, 1970)
!             ------- ------ --------- --------- --- -- -- ----
!
      REAL, PARAMETER :: ox0 = -173.4292
      REAL, PARAMETER :: ox1 = 249.6339
      REAL, PARAMETER :: ox2 = 143.3483
      REAL, PARAMETER :: ox3 = -21.8492
      REAL, PARAMETER :: ox4 = -0.033096
      REAL, PARAMETER :: ox5 = 0.014259
      REAL, PARAMETER :: ox6 = -0.0017

!     -----------------------------------------------------------------
!*            SET VOLUMETRIC SOLUBILITY CONSTANTS FOR N2 IN ML/L
!             WEISS, R. F. (1970) THE SOLUBILITY OF NITROGEN
!             OXYGEN AND ARGON IN WATER AND SEAWATER.
!             DEEP-SEA RESEARCH, VOL. 17, 721-735.
!     -----------------------------------------------------------------

       REAL, PARAMETER :: an0 = -172.4965
       REAL, PARAMETER :: an1 = 248.4262
       REAL, PARAMETER :: an2 = 143.0738
       REAL, PARAMETER :: an3 = -21.7120
       REAL, PARAMETER :: an4 = -0.049781
       REAL, PARAMETER :: an5 = 0.025018
       REAL, PARAMETER :: an6 = -0.0034861

!      Constants for laughing gas solubility
!      (WEISS, 1974, MARINE CHEMISTRY)
!      --------------------------------------
       REAL, PARAMETER :: a1 = -62.7062
       REAL, PARAMETER :: a2 = 97.3066
       REAL, PARAMETER :: a3 = 24.1406
       REAL, PARAMETER :: b1 = -0.058420
       REAL, PARAMETER :: b2 = 0.033193
       REAL, PARAMETER :: b3 = -0.0051313
       REAL, PARAMETER :: atn2o = 3.e-7
       REAL, PARAMETER :: rrrcl = salchl * 1.025 * bor1 * bor2

!      END MODULE MO_CHEM_CON
      
!=======================================================================
!      MODULE mo_BELEGung_BGC
!=======================================================================
!**** *BELEG_BGC* - initialize bgc variables.
!
!     Ernst Maier-Reimer,    *MPI-Met, HH*    10.04.01
!
!     Modified
!     --------
!     S.Legutke,        *MPI-MaD, HH*    10.04.01
!
!     Purpose
!     -------
!     - set start values for  bgc variables.
!
!     Method
!     -------
!     - bgc variables are initialized. They might be overwritten
!       if read from restart by call of AUFR_BGC.
!     - physical constants are defined
!     - fields used for budget calculations (should be in extra SBR!)
!       and as boundary conditions are set to zero.
!
!**   Interface.
!     ----------
!     *CALL*       *BELEG_BGC*
!
!     *PARAMETER*  *PARAM1.h*     - grid size parameters for ocean model.
!     *COMMON*     *PARAM1_BGC.h* - declaration of ocean/sediment tracer.
!     *COMMON*     *COMMO1_BGC.h* - ocean/sediment tracer arrays.
!     *COMMON*     *UNITS_BGC.h*  - std I/O logical units.
!
!**   Interface to ocean model (parameter list):
!     -----------------------------------------
!     *INTEGER* *kpie*    - 1st dimension of model grid.
!     *INTEGER* *kpje*    - 2nd dimension of model grid.
!     *REAL*    *ddpo*   - size of grid cell (3rd dimension) [m].
!
!     Externals
!     ---------
!     none.
!**********************************************************************

      REAL, PARAMETER ::      rcalc  = 35.        ! iris 40 !calcium carbonate to organic phosphorous production ratio
      REAL, PARAMETER ::      ropal  = 25.        ! iris 25 !opal to organic phosphorous production ratio
      REAL, PARAMETER ::      calmax =  0.20      ! max fraction of CaCO3 production (AGG) (max rain ratio)
!ik for interaction with sediment module
      REAL, PARAMETER ::      o2ut   = 172.

! the three values below are from Johnson et al., 1997 Mar. Chemistry 57, 137-161
!     riron   = 5.*rcar*1.e-6       ! 'Q'=5.*122.*1.e-6 = 6.1e-4   (iron to phosphate stoichiometric ratio * umol->mol)
!     riron   = 2.*rcar*1.e-6       ! 14.2.06: 5.->2. (2.5umol P/nmol Fe) emr: 3.*rcar*1.e-6
      REAL, PARAMETER ::     riron   = 3. * 122.*1.e-6
      REAL, PARAMETER ::     fesoly  = 0.6 *1.e-6          ! global mean/max (?) dissolved iron concentration in deep water (for relaxation) [mol/m3]
                                                           ! 'fesoly' stands for 'apparent solubility value' of iron
      REAL, PARAMETER ::     relaxfe = 0.05/365.           ! relaxation time for iron to fesoly corresponds to 20 yrs
                                                           ! relaxfe = 0.005/365.*dtb  ! changed as suggested by emr
                                                           ! (should probably be named scavenge_fe or remove_fe)
                                                           ! (relaxfe is only used to remove iron, corresponding to Johnson's 200yr residence time)
                                                           ! but 200 yrs might be too long, as iron concentrations in the Atlantic are too high.
!      END MODULE mo_BELEGung_BGC

!=======================================================================

   END MODULE mod_param_EMR

!=======================================================================
#endif /*#if defined module_sediment && defined sediment_EMR*/
