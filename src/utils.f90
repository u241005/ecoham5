
! !MODULE: utils --- utilities for ecoham
!
! !INTERFACE:
   MODULE mod_utils
!
! !USES:
   use mod_common
#ifdef MPI
   use mod_mpi_parallel
#endif
   implicit none
!
!  default: all is private.
   private
! !PUBLIC MEMBER FUNCTIONS:
   public integrate_st
#ifdef module_sediment
   public integrate_sd
#endif
   public calcite, press_cor, unesco
   public idays_of_month, next_time_step
   public leerzeile, DOYstring, get_timestamp, dayOfYear2mmdd
!
! !LOCAL VARIABLES:
!
!=======================================================================

   contains

!=======================================================================
!
! !INTERFACE:
   subroutine integrate_st(td_,dt_,level,ierr)
#define SUBROUTINE_NAME 'integrate_st'
!
! !DESCRIPTION:
!
! !USES:
   use mod_var
   use mod_flux
   implicit none
!
! !INPUT PARAMETERS:
   real,    intent(in)      :: td_,dt_
   integer, intent(in)      :: level
!
! !OUTPUT PARAMETERS:
   integer, intent(inout)   :: ierr
!!
! !LOCAL VARIABLES:
   integer  :: n,i,j,k,k0
   integer  :: iS, iE, jS, jE
   real     :: rate, delta_st
   real, parameter :: treshold_o2o = 1.0
   !real     :: dzS, dzE, st_
!
!-----------------------------------------------------------------------
#include "call-trace.inc"

   i = level; rate = td_ ! to get rid of warnings
   i=1; j=1; k=1 ! to get rid of warning: ‘j’ may be used uninitialized ...

   iS = iStartCalc
   iE = iEndCalc
   jS = jStartCalc
   jE = jEndCalc

   ! CHECK PELAGIC STATE VARIABLES
   do n = n_st1, n_st2
      do i = iStartCalc, iEndCalc
         do j = jStartCalc, jEndCalc
            k0 = k_index(j,i)
            do k = 1, k0
               ierr = 0
               log_msg(:) = ''
               rate = 0.0
               delta_st = sst(j,k,i,n)*dt_ /dz(j,k,i) ! (mmol m-3)
               if (n==io2o) then
                  if (abs(st(j,k,i,n))>treshold_o2o) rate = abs(delta_st/st(j,k,i,n))
               else
                  if (abs(st(j,k,i,n))>0.0) rate = abs(delta_st/st(j,k,i,n))
               endif
               if ((n/=io2o .and. st(j,k,i,n)+delta_st < 0.0) .or. (rate>relrate)) then
                  ierr = 1
                  write( log_msg(1), '("STATE VARIABLE negative: ",a6)') st_name(n)
               elseif (rate>relrate) then
                  ierr = 1
                  write( log_msg(1), '("RELRATE violation      : ",a6)') st_name(n)
               endif

               if (ierr > 0) then
                  write( log_msg(2), '("TIME ",f15.10)') td_
                  write( log_msg(3), '("i ",i4)') i
                  write( log_msg(4), '("j ",i4)') j
                  write( log_msg(5), '("k ",i4)') k
                  write( log_msg(6), '("dt level",f15.10,i4)') dt_, level
                  write( log_msg(7), '("value ",e18.10)') st(j,k,i,n)
                  write( log_msg(8), '("flux  ",e18.10)') delta_st
                  ierr = 1
                  exit ! exit all loops, write log message, skip Euler step
                       ! => reduce time step and come back
               endif
            enddo
            if (ierr/=0) exit
         enddo
         if (ierr/=0) exit
      enddo
      if (ierr/=0) exit
   enddo !do n=n_st1,n_st2

   if (ierr/=0) then
      ! set up multi line log message
      long_msg = trim( log_msg(1)) //'   ' // new_line('a') // & ! erforderlich für Vergleich
      !                 andernfalls müssen in der Originalversion drei Leerzeichen entfernt werden
               & trim( log_msg(2)) // new_line('a') // &
               & trim( log_msg(3)) // new_line('a') // &
               & trim( log_msg(4)) // new_line('a') // &
               & trim( log_msg(5)) // new_line('a') // &
               & trim( log_msg(6)) // new_line('a') // &
               & trim( log_msg(7)) // new_line('a') // &
               & trim( log_msg(8)) // new_line('a')
      call write_log_message(long_msg); long_msg = ''
   endif

#if defined MPI && defined SYNCHRONOUS_RECURSION_STEPS
   call MPI_ALLREDUCE(ierr, ierr_all, 1, MPI_INT, MPI_SUM, MPI_COMM_ECOHAM, ierr_mpi)
   ierr = ierr_all + ierr_mpi
#endif

   if (ierr==0) then
      ! euler
      do n = n_st1, n_st2
         do i = iStartCalc, iEndCalc
            do j = jStartCalc, jEndCalc
               k0 = k_index(j,i)
               if (k0 < 1) cycle
               do k = 1, k0
                  st(j,k,i,n) = st(j,k,i,n) + sst(j,k,i,n)/dz(j,k,i) *dt_ ! (mmol m-3)
               enddo
            enddo
         enddo
      enddo
   endif

   end subroutine integrate_st

#ifdef module_sediment
!=======================================================================
!
! !INTERFACE:
   subroutine integrate_sd(td_,dt_,level,ierr)
#define SUBROUTINE_NAME 'integrate_sd'
!
! !DESCRIPTION:
!
! !USES:
   use mod_var
   use mod_flux
   implicit none
!
! !INPUT PARAMETERS:
   real,    intent(in)      :: td_,dt_
   integer, intent(in)      :: level
!
! !OUTPUT PARAMETERS:
   integer, intent(inout)   :: ierr
!!
! !LOCAL VARIABLES:
   integer  :: n, i, j, k0
   integer  :: iS, iE, n1, n2
   real     :: rate
!
!-----------------------------------------------------------------------
#include "call-trace.inc"

   i = level ! to get rid of warnings
   i=1; j=1; ! to get rid of warning: ‘j’ may be used uninitialized ...

   rate = 0.0 ! initialize rate to zero

   iS = iStartCalc
   iE = iEndCalc

   n1 = 1
   n2 = size(sd,3)

   ! CHECK SEDIMENT STATE VARIABLES
   do n = n1, n2
      do i = iStartCalc, iEndCalc
         do j = jStartCalc, jEndCalc
            k0 = k_index(j,i)
            if (k0.ge.1) then
               rate = 0.0
               ! don't limit sinking material from palagial due to sediment inventory !
               if (sd(j,i,n)>0.0 .and. ssd(j,i,n)<0.0 ) rate = abs(ssd(j,i,n)*dt_/sd(j,i,n))
               if ((sd(j,i,n)+ssd(j,i,n)*dt_<0.0) .or. (rate>relrate)) then
                  log_msg(:) = ''
                  write( log_msg(1), '("SEDIMENT VARIABLE negative: ",a6)') sd_name(n)
                  write( log_msg(2), '("TIME ",f15.10)') td_
                  write( log_msg(3), '("i ",i4)') i
                  write( log_msg(4), '("j ",i4)') j
                  write( log_msg(5), '("dt level",f15.10,i4)') dt_, level
                  write( log_msg(6), '("value ",f18.10)') sd(j,i,n)
                  write( log_msg(7), '("flux  ",f18.10)') ssd(j,i,n)*dt_
                  ierr = 1
                  exit ! exit all loops, write log message, skip Euler step
                       ! => reduce time step and come back
               endif
            endif
         enddo
         if (ierr/=0) exit
      enddo
      if (ierr/=0) exit
   enddo !do n=n_sed1,n_sed2

   if (ierr/=0) then
      ! set up multi line log message
      long_msg = trim( log_msg(1)) //'   ' // new_line('a') // & ! erforderlich für Vergleich
      !                 andernfalls müssen in der Originalversion drei Leerzeichen entfernt werden
               & trim( log_msg(2)) // new_line('a') // &
               & trim( log_msg(3)) // new_line('a') // &
               & trim( log_msg(4)) // new_line('a') // &
               & trim( log_msg(5)) // new_line('a') // &
               & trim( log_msg(6)) // new_line('a') // &
               & trim( log_msg(7)) // new_line('a')
      call write_log_message(long_msg); long_msg = ''
   endif

#if defined MPI && defined SYNCHRONOUS_RECURSION_STEPS
   call MPI_ALLREDUCE(ierr, ierr_all, 1, MPI_INT, MPI_SUM, MPI_COMM_ECOHAM, ierr_mpi)
   ierr = ierr_all + ierr_mpi
#endif

   if (ierr==0) then
      ! euler
      do n = n1, n2
         where(ixistK(:,iS:iE)==1) sd(:,iS:iE,n) = sd(:,iS:iE,n) + ssd(:,iS:iE,n)*dt_
      enddo
   endif

   end subroutine integrate_sd
#endif
!=======================================================================
!
! !INTERFACE:
   subroutine calcite(temp_,salt_,rho_,pres_,ca_,aksp_)
!
! !DESCRIPTION:
! This subroutine calculates calcium concentration and solubility
! of calcite as a function of temperature, salinity and pressure
!
! !USES:
   implicit none
!
! !INPUT PARAMETERS:
   real,    intent(in) :: temp_,salt_
   real,    intent(in) :: rho_,pres_
!
! !OUTPUT PARAMETERS:
   real,   intent(out) :: ca_, aksp_
!
! !LOCAL VARIABLES:
   real    :: s2,s12,s32,tk,tz,rt,aksp0
!
!-----------------------------------------------------------------------

   !set coefficients
   s2 = salt_*salt_
   s12= sqrt(salt_)
   s32= salt_*s12
   tk = temp_+temzer
   tz = alog10(tk)
   rt = rgas * tk

   ! calcium concentration
   ca_=10285.*salt_/35.*rho_

   ! calcite solubility
   ! calcite sat: o3c_*ca_/aksp
   ! Mucchi (1983) and Millero (1995) [mumol*mumol/kg/kg soln]
   aksp0=-171.9065-0.077993*tk+2839.319/tk+71.595*alog10(tk)  &
!jp      +(-0.77712+.00028426*tk+178.34/tk)*s12               &                !jp+bm 30.11.2015
         +(-0.77712+.0028426*tk+178.34/tk)*s12               &
         -0.07711*salt_+0.0041249*s32
   aksp0=10.**(12.+aksp0)
   ! pressure correction Millero (1995)
!jp   aksp_=aksp0*press_cor(-48.76,.5304,0.,-11.76,   .3692,   temp_,pres_,rt) !jp+bm 30.11.2015
      aksp_=aksp0*press_cor(-48.76,.5304,0.,-11.76e-3,.3692e-3,temp_,pres_,rt)

   return

   end subroutine calcite

!=======================================================================
! !INTERFACE:
   real function press_cor(a0,a1,a2,b0,b1,tc,p,rt)
!
! !DESCRIPTION:
! pressure correction Millero (1995)
!
! !USES:
   implicit none
!
! !INPUT PARAMETERS:
   real, intent(in)      ::a0,a1,a2,b0,b1,tc,p,rt
!
! !LOCAL VARIABLES:
   real                  :: deltaVK,xKappa,xlnfac

!-----------------------------------------------------------------------

   deltaVK = (tc*a2+a1)*tc+a0
   !xKappa = (tc*b1+b0)/1000. !MK 14-04-2016 see email correspondence with bernhard mayer
   xKappa = tc*b1+b0
   xlnfac = (-deltaVK+.5*xKappa*p)*p/rt
   press_cor = exp(xlnfac)

   return

   end function press_cor

!=======================================================================
!
! !INTERFACE:
   real function unesco(TT,SS) ! unit is kg/l (:= kg/m3/1000) !!
! see gotm eqstate
!
! !DESCRIPTION:
!  Computes the in-situ density according to the UNESCO equation of
!  state for sea water (see FofonoffMillard83).
!  The pressure dependence was neglected.
!
! !USES:
   implicit none
!
! !INPUT PARAMETERS:
   real,    intent(in) :: TT, SS
!
! !LOCAL VARIABLES:
   real                :: T2, T3, T4, T5, S15, S2, S3, x
!
!-----------------------------------------------------------------------

   !    rho_0=1027.0
   T2 = TT*TT
   T3 = TT*T2
   T4 = T2*T2
   T5 = TT*T4

   S15= SS**1.5
   S2 = SS*SS
   S3 = SS*S2

   x=999.842594+6.793952e-02*TT-9.09529e-03*T2+1.001685e-04*T3
   x=x-1.120083e-06*T4+6.536332e-09*T5
   x=x+SS*(0.824493-4.0899e-03*TT+7.6438e-05*T2-8.2467e-07*T3)
   x=x+SS*5.3875e-09*T4
   x=x+sqrt(S3)*(-5.72466e-03+1.0227e-04*TT-1.6546e-06*T2)
   x=x+4.8314e-04*S2

   unesco = x/1000. ! converts kg/m3 to kg/l (and accordingly kg/dm3)

   return

   end function unesco

! !=======================================================================
! !
! ! !INTERFACE:
!    subroutine v2t(u,v,tx,ty)
! !
! ! !DESCRIPTION:
! ! according to NOMADS
! !
! ! !USES:
!    implicit none
! !
! ! !LOCAL VARIABLES:
!    real, parameter   :: RhoAir = 1.25
!    real, parameter   ::     CD = 0.0016
!    real, parameter   :: RhoRef = 1026.
!    real              :: u,v,tx,ty
!    real              :: speedq,wlam,t,speed
! !
! !-----------------------------------------------------------------------
!
!    wlam = 1000.* CD * RhoAir / RhoRef
!    speedq=u*u+v*v
!    speed=sqrt(speedq)
!    t = wlam*speedq
!    tx=u*t/speed
!    ty=v*t/speed
!
!    return
!
!    end subroutine v2t

!=======================================================================
!
! !INTERFACE:
   subroutine leerzeile(iu,le)
!
! !DESCRIPTION:
!
! !USES:
   implicit none
!
! !INPUT PARAMETERS:
   integer, intent(in)   :: iu,le
!
! !LOCAL VARIABLES:
   integer       :: l
   character*1   :: s
!-----------------------------------------------------------------------

   do l=1,le
      read(iu,'(a)')s
   enddo

   return

   end subroutine leerzeile

!=======================================================================
!
! !INTERFACE:
   character(len=19) function DOYstring(DOY)
!
! !DESCRIPTION:
! get DOY as string (yyyy-mm-dd HH:MM:SS)
!
! !USES:
   implicit none
!
! !OUTPUT PARAMETERS:
!   character*19, intent(out) :: str
   real, intent(in) :: DOY
!
! !LOCAL VARIABLES:
   real             :: xx
   integer          :: id, y, m, d, hh, mm, ss
!-----------------------------------------------------------------------

   id = int(DOY)

   xx = (DOY-real(id))*24.         ! decimal hours
   hh = int( xx        )
   xx = (xx-hh)*60.                ! decimal minutes
   mm = int( xx        )
   ss = nint((xx-mm)*60.)          ! decimal seconds

   if (ss.eq.60) then
      mm=mm+1
      ss=0
   endif

   if (mm.eq.60) then
      hh=hh+1
      mm=0
   endif

   if (hh.eq.24) then
      id=id+1
      hh=0
   endif

   y = year
   call dayOfYear2mmdd(id+1,y,m,d)

   !write(*,'(i4.4,2("-",i2.2)," ",2(i2.2,":"),i2.2)') y,m,d,hh,mm,ss
   write(DOYstring,'(i4.4,2("-",i2.2)," ",2(i2.2,":"),i2.2)') y,m,d,hh,mm,ss

   return

   end function DOYstring

!=======================================================================
!
! !INTERFACE:
   subroutine get_timestamp(str)
!
! !DESCRIPTION:
! get current date & time as string (yyyy-mm-dd HH:MM:SS)
!
! !USES:
   implicit none
!
! !OUTPUT PARAMETERS:
   character*(*), intent(out) :: str
!
! !LOCAL VARIABLES:
   character*8   :: datestr
   character*10  :: timestr
!-----------------------------------------------------------------------

   call Date_And_Time(datestr,timestr)
   write(str,'(a4,"-",a2,"-",a2," ",a2,":",a2,":",a2)')   &
             & datestr(1:4),datestr(5:6),datestr(7:8),    &
             & timestr(1:2),timestr(3:4),timestr(5:6)

   return

   end subroutine get_timestamp

!=======================================================================
!
! !INTERFACE:
   subroutine dayOfYear2mmdd( dayOfYear_, iyear, imonth, iday)
!
! !DESCRIPTION:
!
! !USES:
   implicit none
!
! !INPUT PARAMETERS:
   integer, intent(in)    :: dayOfYear_
   integer, intent(inout) :: iyear
!
! !OUTPUT PARAMETERS:
   integer, intent(out)   :: imonth, iday
!
! !LOCAL VARIABLES:
   integer, dimension(12), parameter :: daysPerMonth_no_leapyear = (/31,28,31,30,31,30,31,31,30,31,30,31/)
   integer, dimension(12), parameter :: daysPerMonth_leapyear    = (/31,29,31,30,31,30,31,31,30,31,30,31/)
   integer, dimension(12) :: daysPerMonth
   integer :: m, sum_days, dayOfYear

!-----------------------------------------------------------------------

   dayOfYear = dayOfYear_

   if ( mod(iyear,400)==0 .or. ( mod(iyear,4)==0 .and. .not. mod(iyear,100)==0) ) then
      daysPerMonth = daysPerMonth_leapyear
   else
      daysPerMonth = daysPerMonth_no_leapyear
   endif
   if (dayOfYear .gt. sum(daysPerMonth)) then
      iyear = iyear +1
      dayOfYear = dayOfYear-sum(daysPerMonth)
      if ( mod(iyear,400)==0 .or. ( mod(iyear,4)==0 .and. .not. mod(iyear,100)==0) ) then
         daysPerMonth = daysPerMonth_leapyear
      else
         daysPerMonth = daysPerMonth_no_leapyear
      endif
   endif

   sum_days = 0
   month_loop: do m = 1, 12
      iday = dayOfYear -sum_days
      if ( iday <= daysPerMonth(m) ) then
            imonth = m
            exit month_loop
      endif
      sum_days = sum_days + daysPerMonth(m)
   enddo month_loop
   if ( imonth==12 .and. iday>daysPerMonth(12) ) then
      call write_log_message("date error: dayOfYear too large")
      imonth = 0
      iday = 0
   endif

    end subroutine dayOfYear2mmdd

!=======================================================================
!
! !INTERFACE:
   subroutine idays_of_month(iyr,imon,ida,isw)
!
! !DESCRIPTION:
!
! it does not matter if for example iyr=76 or instead 1976 is used
!            also iyr>2000 is valid
!
! isw=0: Berechnung der Anzahl der Tage des Monats imon im Jahr iyr
!        in : isw,iyr,imon
!        out: ida
!
! isw=1,..31: Berechnung der Anzahl der Tage bis zum isw.imon.iyr
!        in : isw,iyr,imon
!        out: ida
!
! isw<0: Berechnung des Datums isw.imon.iyr des Tags ida
!        in : isw,ida,iyr
!        out: ida,imon,iyr
!
! !USES:
   implicit none
!
! !INPUT PARAMETERS:
   integer, intent(in)     :: isw
!
! !OUTPUT PARAMETERS:
   integer, intent(inout)  :: ida,imon,iyr
!
! !LOCAL VARIABLES:
   integer,parameter   :: id(12)     =(/31,28,31,30,31,30,31,31,30,31,30,31/)
   integer,parameter   :: id_sch(12) =(/31,29,31,30,31,30,31,31,30,31,30,31/)
   integer             :: isum,idm,i

!-----------------------------------------------------------------------

   if(isw==0)then
      if(mod(iyr,4)==0.and.mod(iyr,100)/=0)then
         ida = id_sch(imon)
      else
         ida = id(imon)
      endif
      if(mod(iyr,1000)==0)then
         ida = id_sch(imon)
      endif
   endif
   if(isw>0.and.isw<=31)then
      isum = isw
      do i=1,imon-1
         if(mod(iyr,4)==0.and.mod(iyr,100)/=0)then
            idm = id_sch(i)
         else
            idm = id(i)
         endif
         if(mod(iyr,1000)==0)then
            idm = id_sch(i)
         endif
         isum=isum+idm
      enddo
      ida = isum
   endif
   if(isw<0)then
      imon = 12
      isum = id(1)
      do i=2,12
         if(ida<=isum)then
            imon = i-1
            goto 3
         endif
         if(mod(iyr,4)==0.and.mod(iyr,100)/=0)then
            idm = id_sch(i)
         else
            idm = id(i)
         endif
         if(mod(iyr,1000)==0)then
            idm = id_sch(i)
         endif
         isum=isum+idm
      enddo
   3  continue
      if(mod(iyr,4)==0.and.mod(iyr,100)/=0)then
         idm = id_sch(imon)
      else
         idm = id(imon)
      endif
      if(mod(iyr,1000)==0)then
         idm = id_sch(imon)
      endif
      isum = isum -idm
      ida = ida - isum
   endif

   return

   end subroutine idays_of_month

!=======================================================================
! !INTERFACE:
   subroutine next_time_step(deltat,iyr,imon,iday,ihor)
!
! !DESCRIPTION:
!
! !USES:
   implicit none
!
! !INPUT PARAMETERS:
   real,    intent(in)   :: deltat ! timestep in hours
   integer, intent(inout):: iyr,imon,iday,ihor
!
! !LOCAL VARIABLES:
   integer       :: imo, ida, imin
   real          :: eps
!-----------------------------------------------------------------------

   ! deltat in hours
   eps = 1.e-5

   imin=0
   imin = imin + nint(60.*deltat)
   if(imin>59)then
      ihor = ihor + int(eps+real(imin)/60.)
      imin = mod(imin,60)
      if(ihor>23)then
         call idays_of_month(iyr,imon,ida,iday)
         iday = ida + int(eps+real(ihor)/24.)
         ihor = mod(ihor,24)
         imo = 12 !! MK: idays_of_month(-,X,-,-) is intend(inout) !!
         call idays_of_month(iyr,imo,ida,31)
         if(iday>ida)then
            iday=iday-ida
            iyr=iyr+1
         endif
         call idays_of_month(iyr,imon,iday,-1)
      endif
   endif

   return

   end subroutine next_time_step

! !=======================================================================
! !
! ! !INTERFACE:
!    subroutine read_dataset_r4(unitnum, dataset, irec, ierr, scaling_factor)
! #define SUBROUTINE_NAME 'read_dataset_r4'
! !
! ! !DESCRIPTION:
! !
! ! !USES:
!    use mod_grid, only : ixistz_master
!    implicit none
! !
! ! !INPUT PARAMETERS:
!    integer,          intent(in)           :: unitnum, irec
!    real,             intent(in), optional :: scaling_factor
! !
! ! !OUTPUT PARAMETERS:
!    real, dimension(:,:,:), pointer, intent(out) :: dataset
!    !real, dimension(:,:,:), intent(out) :: dataset
!    integer,              intent(inout) :: ierr
! !
! ! !LOCAL VARIABLES:
!    real        , dimension(jmax,kmax,imax) :: full3D
!    real(kind=4), dimension(iiwet)          :: input3d
!    real        , dimension(iiwet)          :: dum3d
!    integer                                 :: ios
! !
! !--------------------------------------------------------------
! #include "call-trace.inc"
!
!    ios = 0
!    dataset = fail
!
!    if (myID == master) then
!       read( unitnum, rec=irec, iostat=ios ) input3d
!       if (ios /= 0) then
!          write( long_msg, '("ERROR reading UNIT=", i4)') unitnum
!          call write_log_message(long_msg); long_msg = ''
!          ierr = 1
!       else
!          if ( present( scaling_factor) ) then
!             dum3d = real(input3d) * scaling_factor
!          else
!             dum3d = real(input3d)
!          endif
!          full3D(:,:,:) = unpack( dum3d, ixistz_master(:,:,:)==1,fail )
!       endif
!    end if
!
! #ifdef MPI
!    call MPI_Bcast( full3D, ijkMax, MPI_REAL8, master, MPI_COMM_ECOHAM, ierr_mpi )
!    ierr = ierr + ierr_mpi
!    call MPI_ALLREDUCE(ierr, ierr_all, 1, MPI_INT, MPI_SUM, MPI_COMM_ECOHAM, ierr_mpi)
!    ierr = ierr_all + ierr_mpi
! #endif
!
!    dataset(:,:,iStartD:iEndD) = full3D(:,:,iStartD:iEndD)
!
!    return
!
!    end subroutine read_dataset_r4
! !
! !=======================================================================
! !
! ! !INTERFACE:
!    subroutine read_dataset_r8(unitnum, dataset, irec, ierr, scaling_factor)
! #define SUBROUTINE_NAME 'read_dataset_r8'
! !
! ! !DESCRIPTION:
! !
! ! !USES:
!    use mod_grid, only : ixistz_master
!    implicit none
! !
! ! !INPUT PARAMETERS:
!    integer,          intent(in)           :: unitnum, irec
!    real,             intent(in), optional :: scaling_factor
! !
! ! !OUTPUT PARAMETERS:
!    real, dimension(:,:,:), pointer, intent(out) :: dataset
!    !real, dimension(:,:,:), intent(out) :: dataset
!    integer,              intent(inout) :: ierr
! !
! ! !LOCAL VARIABLES:
!    real        , dimension(jmax,kmax,imax) :: full3D
!    real(kind=8), dimension(iiwet)          :: input3d
!    real        , dimension(iiwet)          :: dum3d
!    integer                                 :: ios
! !
! !--------------------------------------------------------------
! #include "call-trace.inc"
!
!    ios = 0
!    dataset = fail
!
!    if (myID == master) then
!       read( unitnum, rec=irec, iostat=ios ) input3d
!       if (ios /= 0) then
!          write( long_msg, '("ERROR reading UNIT=", i4)') unitnum
!          call write_log_message(long_msg); long_msg = ''
!          ierr = 1
!       else
!          if ( present( scaling_factor) ) then
!             dum3d = real(input3d) * scaling_factor
!          else
!             dum3d = real(input3d)
!          endif
!          full3D(:,:,:) = unpack( dum3d, ixistz_master(:,:,:)==1,fail )
!       endif
!    end if
!
! #ifdef MPI
!    call MPI_Bcast( full3D, ijkMax, MPI_REAL8, master, MPI_COMM_ECOHAM, ierr_mpi )
!    ierr = ierr + ierr_mpi
!    call MPI_ALLREDUCE(ierr, ierr_all, 1, MPI_INT, MPI_SUM, MPI_COMM_ECOHAM, ierr_mpi)
!    ierr = ierr_all + ierr_mpi
! #endif
!
!    dataset(:,:,iStartD:iEndD) = full3D(:,:,iStartD:iEndD)
!
!    return
!
!    end subroutine read_dataset_r8
!
!=======================================================================

   end module mod_utils

!=======================================================================
