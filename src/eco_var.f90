!
! !MODULE: eco_var.f90 --- major variables (arrays) for ecoham
!
! !INTERFACE:
   MODULE mod_var
!
! !DESCRIPTION:
!
! !USES:
   use mod_common
!   use mod_mpi_parallel
   implicit none
!
!  default: all is public.
   public
!
! !LOCAL VARIABLES:
#ifdef module_biogeo
   ! parameters for pelagic STATE VARIABLES:
#ifdef TBNT_x1x_test
   integer, parameter :: n_st_var      = 38
#else
   integer, parameter :: n_st_var      = 37
#endif
   integer, parameter :: n_de_var      = 6
   ! INDEX-pointers to pelagic STATE VARIABLES:
   integer, parameter :: ix1x= 1,ialk= 2,idic= 3,in3n= 4,in4n= 5
   integer, parameter :: in1p= 6,in5s= 7,ip1c= 8,ip1n= 9,ip1p=10
   integer, parameter :: ip1s=11,ip2c=12,ip2n=13,ip2p=14,ip3c=15
   integer, parameter :: ip3n=16,ip3p=17,ip3k=18,iz1c=19,iz2c=20
   integer, parameter :: ibac=21,id1c=22,id1n=23,id1p=24,id2c=25
   integer, parameter :: id2n=26,id2p=27,id2s=28,id2k=29,isoc=30
   integer, parameter :: idoc=31,idon=32,idop=33,io2o=34
   integer, parameter :: ip1a=35,ip2a=36,ip3a=37
#ifdef TBNT_x1x_test
   integer, parameter :: ix2x= 38
#endif
   ! INDEX-pointers to DERIVED VARIABLES:
   integer, parameter :: iz1n=1,iz1p=2,iz2n=3,iz2p=4,iban=5,ibap=6
#else
#  ifdef module_chemie
   ! parameters for SIMPLE CO2-MODEL:
   integer, parameter :: ix1x= 1, ialk= 2, idic= 3
   integer, parameter :: io2o= 99 ! dummy assignment required for integrate_st
#ifdef TBNT_x1x_test
   integer, parameter :: n_st_var = 4
   integer, parameter :: ix2x= 4
#else
   integer, parameter :: n_st_var = 3
#endif
   integer, parameter :: n_de_var = 0
#  else
   ! parameters for SINGLE PASSIVE TRACER:
   integer, parameter :: ix1x= 1
#ifdef TBNT_x1x_test
   integer, parameter :: ix2x= 2
   integer, parameter :: n_st_var= 2
#else
   integer, parameter :: n_st_var = 1
#endif
   integer, parameter :: n_de_var = 0
#  endif
#endif
   integer            :: n_st1=1, n_st2=n_st_var
   ! container (array) for pelagic STATE VARIABLES:
   real, dimension(:,:,:,:), allocatable  :: st, sst, de
   ! hydrodynamic properties of pelagic STATE VARIABLES
   real, dimension(n_st_var) :: sink

   ! variables for output control and metadata
   logical,           dimension(n_st_var) :: st_out3D, st_out2D
   character(len=6),  dimension(n_st_var) :: st_name
   character(len=99), dimension(n_st_var) :: st_long, st_unit
   logical,           dimension(n_de_var) :: de_out3D, de_out2D
   character(len=6),  dimension(n_de_var) :: de_name
   character(len=99), dimension(n_de_var) :: de_long, de_unit


   ! container (array) for bentic STATE VARIABLES:
   integer,                                      public :: n_sed_var
   real,          dimension(:,:,:), allocatable, public :: sd, ssd
   logical,           dimension(:), allocatable, public :: sd_out2D
   character(len=6),  dimension(:), allocatable, public :: sd_name
   character(len=99), dimension(:), allocatable, public :: sd_long, sd_unit


!=======================================================================

   contains

!=======================================================================
!
! !INTERFACE:
   subroutine init_vars(ierr)
#define SUBROUTINE_NAME 'init_vars'
!
! !DESCRIPTION:
!
! !USES:
   use mod_grid, only : ixistz
   IMPLICIT NONE
!
! !OUTPUT PARAMETERS:
   integer, intent(inout)  :: ierr
!
! !LOCAL VARIABLES:
!
!-----------------------------------------------------------------------
#include "call-trace.inc"

   ! set varnames
   call set_varnames(ierr)

   ! initialise 3D-output logics
   st_out3D = .false.
   de_out3D = .false.

   ! allocate and initialize arrays
   allocate(  st(jmax,kmax,iStartD:iEndD,n_st_var))
   allocate( sst(jmax,kmax,iStartD:iEndD,n_st_var))
   st = fail

   allocate(  de(jmax,kmax,iStartD:iEndD,n_de_var))
   de = fail

   call empty_sst(ierr)

   !init x1x
   where(ixistz==1) st(:,:,:,ix1x) = 1.0

   sink = ZERO

   if (ierr/=0) then
      write( error_message, '("[",i2.2,"] ERROR - init_vars: ",i3)') myID, ierr
      call stop_ecoham(ierr, msg=error_message)
   end if
   return

   end subroutine init_vars

!=======================================================================
!
! !INTERFACE:
   subroutine set_varnames(ierr)
#define SUBROUTINE_NAME 'set_varnames'
!
! !DESCRIPTION:
!
! !USES:
   IMPLICIT NONE
!
! !OUTPUT PARAMETERS:
   integer, intent(inout)  :: ierr
!
! !LOCAL VARIABLES:
!
!-----------------------------------------------------------------------
#include "call-trace.inc"

   ! clear character arrays
   st_name(:) = ''
   st_long(:) = ''
   st_unit(:) = ''
   de_name(:) = ''
   de_long(:) = ''
   de_unit(:) = ''

   ! prognostic state variables ("st")
   st_name(ix1x)='x1x'
   st_long(ix1x)='PASSIVE TRACER'
   st_unit(ix1x)='-'

#ifdef TBNTx1x_test
   ! prognostic state variables ("st")
   st_name(ix1x)='x2x'
   st_long(ix1x)='PASSIVE DUMMY TRACER'
   st_unit(ix1x)='-'
#endif

#ifdef module_biogeo
   st_name(ialk)='alk'
   st_long(ialk)='TOTAL ALKALINITY'
   st_unit(ialk)='mmol C m-3'

   st_name(idic)='dic'
   st_long(idic)='DISSOLVED INORGANIC CARBON'
   st_unit(idic)='mmol C m-3'

   st_name(in3n)='n3n'
   st_long(in3n)='NITRATE'
   st_unit(in3n)='mmol N m-3'

   st_name(in4n)='n4n'
   st_long(in4n)='AMMONIUM'
   st_unit(in4n)='mmol N m-3'

   st_name(in1p)='n1p'
   st_long(in1p)='PHOSPHATE'
   st_unit(in1p)='mmol P m-3'

   st_name(in5s)='n5s'
   st_long(in5s)='SILICATE'
   st_unit(in5s)='mmol Si m-3'

   st_name(ip1a)='p1a'
   st_long(ip1a)='DIATOMS-Chla'
   st_unit(ip1a)='mg Chl m-3'

   st_name(ip1c)='p1c'
   st_long(ip1c)='DIATOMS-C'
   st_unit(ip1c)='mmol C m-3'

   st_name(ip1n)='p1n'
   st_long(ip1n)='DIATOMS-N'
   st_unit(ip1n)='mmol N m-3'

   st_name(ip1p)='p1p'
   st_long(ip1p)='DIATOMS-P'
   st_unit(ip1p)='mmol P m-3'

   st_name(ip1s)='p1s'
   st_long(ip1s)='DIATOMS-Si'
   st_unit(ip1s)='mmol Si m-3'

   st_name(ip2a)='p2a'
   st_long(ip2a)='FLAGELLATES-Chla'
   st_unit(ip2a)='mg Chl m-3'

   st_name(ip2c)='p2c'
   st_long(ip2c)='FLAGELLATES-C'
   st_unit(ip2c)='mmol C m-3'

   st_name(ip2n)='p2n'
   st_long(ip2n)='FLAGELLATES-N'
   st_unit(ip2n)='mmol N m-3'

   st_name(ip2p)='p2p'
   st_long(ip2p)='FLAGELLATES-P'
   st_unit(ip2p)='mmol P m-3'

   st_name(ip3a)='p3a'
   st_long(ip3a)='COCCOLITHOPHORIDS-Chla'
   st_unit(ip3a)='mg Chl m-3'

   st_name(ip3c)='p3c'
   st_long(ip3c)='COCCOLITHOPHORIDS-C'
   st_unit(ip3c)='mmol C m-3'

   st_name(ip3n)='p3n'
   st_long(ip3n)='COCCOLITHOPHORIDS-N'
   st_unit(ip3n)='mmol N m-3'

   st_name(ip3p)='p3p'
   st_long(ip3p)='COCCOLITHOPHORIDS-P'
   st_unit(ip3p)='mmol P m-3'

   st_name(ip3k)='p3k'
   st_long(ip3k)='COCCOLITHOPHORIDS-CaCO3'
   st_unit(ip3k)='mmol C m-3'

   st_name(iz1c)='z1c'
   st_long(iz1c)='MICRO ZOOPLANKTON-C'
   st_unit(iz1c)='mmol C m-3'

   st_name(iz2c)='z2c'
   st_long(iz2c)='MESO ZOOPLANKTON-C'
   st_unit(iz2c)='mmol C m-3'

   st_name(ibac)='bac'
   st_long(ibac)='BACTERIA-C'
   st_unit(ibac)='mmol C m-3'

   st_name(id1c)='d1c'
   st_long(id1c)='DETRITUS-C (slow sinking)'
   st_unit(id1c)='mmol C m-3'

   st_name(id1n)='d1n'
   st_long(id1n)='DETRITUS-N (slow sinking)'
   st_unit(id1n)='mmol N m-3'

   st_name(id1p)='d1p'
   st_long(id1p)='DETRITUS-P (slow sinking)'
   st_unit(id1p)='mmol P m-3'

   st_name(id2c)='d2c'
   st_long(id2c)='DETRITUS-C (fast sinking)'
   st_unit(id2c)='mmol C m-3'

   st_name(id2n)='d2n'
   st_long(id2n)='DETRITUS-N (fast sinking)'
   st_unit(id2n)='mmol N m-3'

   st_name(id2p)='d2p'
   st_long(id2p)='DETRITUS-P (fast sinking)'
   st_unit(id2p)='mmol P m-3'

   st_name(id2s)='d2s'
   st_long(id2s)='DETRITUS-Si (fast sinking)'
   st_unit(id2s)='mmol Si m-3'

   st_name(id2k)='d2k'
   st_long(id2k)='DETRITUS-CaCO3 (fast sinking)'
   st_unit(id2k)='mmol C m-3'

   st_name(isoc)='soc'
   st_long(isoc)='SEMI-LABILE DOC'
   st_unit(isoc)='mmol C m-3'

   st_name(idoc)='doc'
   st_long(idoc)='LABILE DISSOLVED ORGANIC CARBON'
   st_unit(idoc)='mmol C m-3'

   st_name(idon)='don'
   st_long(idon)='LABILE DISSOLVED ORGANIC NITROGEN'
   st_unit(idon)='mmol N m-3'

   st_name(idop)='dop'
   st_long(idop)='LABILE DISSOLVED ORGANIC PHOSPHORUS'
   st_unit(idop)='mmol P m-3'

   st_name(io2o)='o2o'
   st_long(io2o)='OXYGEN'
   st_unit(io2o)='mmol O2 m-3'

   ! derived state variables ("de")
   de_name(iz1n)='z1n'
   de_long(iz1n)='MICRO ZOOPLANKTON-N'
   de_unit(iz1n)='mmol N m-3'

   de_name(iz1p)='z1p'
   de_long(iz1p)='MICRO ZOOPLANKTON-P'
   de_unit(iz1p)='mmol P m-3'

   de_name(iz2n)='z2n'
   de_long(iz2n)='MESO ZOOPLANKTON-N'
   de_unit(iz2n)='mmol N m-3'

   de_name(iz2p)='z2p'
   de_long(iz2p)='MESO ZOOPLANKTON-P'
   de_unit(iz2p)='mmol P m-3'

   de_name(iban)='ban'
   de_long(iban)='BACTERIA-N'
   de_unit(iban)='mmol N m-3'

   de_name(ibap)='bap'
   de_long(ibap)='BACTERIA-P'
   de_unit(ibap)='mmol P m-3'

#else
#ifdef module_chemie
   st_name(ialk)='alk'
   st_long(ialk)='TOTAL ALKALINITY'
   st_unit(ialk)='mmol C m-3'

   st_name(idic)='dic'
   st_long(idic)='DISSOLVED INORGANIC CARBON'
   st_unit(idic)='mmol C m-3'
#endif
#endif

   if (ierr/=0) then
      write( error_message, '("[",i2.2,"] ERROR - set_varnames: ",i3)') myID, ierr
      call stop_ecoham(ierr, msg=error_message)
   end if
   return

   end subroutine set_varnames

!=======================================================================
!
! !INTERFACE:
   subroutine empty_sst(ierr)
#define SUBROUTINE_NAME 'empty_sst'
!
! !DESCRIPTION:
!
! !USES:
   IMPLICIT NONE
!
! !INPUT PARAMETERS:
   integer, intent(inout) :: ierr
!
! !LOCAL VARIABLES:
!
!-----------------------------------------------------------------------
#include "call-trace.inc"

   sst = zero

   if (ierr/=0) then
      write( error_message, '("[",i2.2,"] ERROR - empty_sst: ",i3)') myID, ierr
      call stop_ecoham(ierr, msg=error_message)
   end if
   return

   end subroutine empty_sst

!=======================================================================
!
! !INTERFACE:
   subroutine empty_ssd(ierr)
#define SUBROUTINE_NAME 'empty_ssd'
!
! !DESCRIPTION:
!
! !USES:
   IMPLICIT NONE
!
! !INPUT PARAMETERS:
   integer, intent(inout) :: ierr
!
! !LOCAL VARIABLES:
!
!-----------------------------------------------------------------------
#include "call-trace.inc"

   ssd = zero

   if (ierr/=0) then
      write( error_message, '("[",i2.2,"] ERROR - empty_ssd: ",i3)') myID, ierr
      call stop_ecoham(ierr, msg=error_message)
   end if
   return

   end subroutine empty_ssd

!=======================================================================

   end module mod_var

!=======================================================================
