!
! !MODULE: eco_boundaries.f90 ---  wrapper for pelagic boundary conditions
!
! !INTERFACE:
   MODULE mod_boundaries
!
! !DESCRIPTION:
! This module is a wrapper for calling subroutines concerning pelagic boundary conditions
!
! !USES:
   use mod_common
   use mod_grid
#ifdef MPI
   use mod_mpi_parallel
#endif
#ifdef module_silt
   use mod_silt
#endif
#ifdef module_meteo
   use mod_meteo
#endif
#ifdef module_rivers
   use mod_rivers
#endif
#ifdef module_restoring
   use mod_restoring
#endif
   implicit none
!
!  default: all is private.
   private
! !PUBLIC MEMBER FUNCTIONS:
   public init_boundaries, get_boundaries, update_boundaries
   public do_boundaries, close_boundaries
!
! !LOCAL VARIABLES:
!   integer     , public :: dt_bounds  !smallest timestep new boundary data are expected
!
!=======================================================================

   contains

!=======================================================================
!
! !INTERFACE:
   subroutine init_boundaries(ierr)
#define SUBROUTINE_NAME 'init_boundaries'
!
! !DESCRIPTION:
!
! !USES:
   implicit none
!
! !OUTPUT PARAMETERS:
   integer, intent(inout)  :: ierr
!
! !LOCAL VARIABLES:
!
!-----------------------------------------------------------------------
#include "call-trace.inc"

   if (myID==master) call write_log_message(' INITIALIZING boundaries ')

#ifdef module_meteo
   call init_meteo(ierr)
#endif

#ifdef module_silt
   call init_silt(ierr)
#endif

#ifdef module_rivers
   call init_rivers(ierr)
#endif

#ifdef module_restoring
   ! prepare restoring data
   call init_restoring(ierr)
#endif

   call get_boundaries(ierr)

   call update_boundaries(ierr)

   if (ierr/=0) then
      write( error_message, '("[",i2.2,"] ERROR - init_boundaries",i3)') myID, ierr
      call stop_ecoham(ierr, msg=error_message)
   end if
   return

   end subroutine init_boundaries

!=======================================================================
! !INTERFACE:
   subroutine get_boundaries(ierr)
#define SUBROUTINE_NAME 'get_boundaries'
!
! !DESCRIPTION:
!
! !USES:
   implicit none
!
! !OUTPUT PARAMETERS:
   integer, intent(inout)  :: ierr
!
! !LOCAL VARIABLES:
!
!-----------------------------------------------------------------------
#include "call-trace.inc"

#ifdef module_silt
   call get_silt(day_of_year,ierr)
#endif

#ifdef module_meteo
   call get_meteo(day_of_year,ierr)
#endif

#ifdef module_rivers
   call get_rivers(day_of_year,ierr)
#endif

#ifdef module_restoring
   call get_restoring(day_of_year,ierr)
#endif

   if (ierr/=0) then
      write( error_message, '("[",i2.2,"] ERROR - get_boundaries",i3)') myID, ierr
      call stop_ecoham(ierr, msg=error_message)
   end if
   return

   end subroutine get_boundaries

!=======================================================================
! !INTERFACE:
   subroutine update_boundaries(ierr)
#define SUBROUTINE_NAME 'update_boundaries'
!
! !DESCRIPTION:
!
! !USES:
   use mod_var
   implicit none
!
! !OUTPUT PARAMETERS:
   integer, intent(inout)  :: ierr
!
! !LOCAL VARIABLES:
!
!-----------------------------------------------------------------------
#include "call-trace.inc"

#ifdef module_meteo
   call update_meteo(day_of_year,ierr)
#endif

#ifdef module_silt
   call update_silt(day_of_year,ierr)
#endif

#ifdef module_meteo
! ====== UNDERWATER LIGHT (PAR) =========
#ifdef two_wave_band    
   call  irradiance_two_wave_band(ierr)
#else
   call  irradiance(ierr)
#endif
#endif

#ifdef module_restoring
   call update_restoring(day_of_year,ierr)
#endif

   if (ierr/=0) then
      write( error_message, '("[",i2.2,"] ERROR - update_boundaries",i3)') myID, ierr
      call stop_ecoham(ierr, msg=error_message)
   end if
   return

   end subroutine update_boundaries

!=======================================================================
! !INTERFACE:
   subroutine do_boundaries(dt_,ierr)
#define SUBROUTINE_NAME 'do_boundaries'
!
! !DESCRIPTION:
!
! !USES:
   use mod_var
   implicit none
!
! !INPUT PARAMETERS:
   real,    intent(in)     :: dt_   ! timestep (days)
   integer, intent(inout)  :: ierr
!
! !OUTPUT PARAMETERS:
!
! !LOCAL VARIABLES:
  real :: dummy
!
!-----------------------------------------------------------------------
#include "call-trace.inc"

   dummy = dt_ ! to get rid of warning: Unused dummy argument

#ifdef module_restoring
   ! set open boundaries (and optionally restore inner domain) (-> st)
   call do_restoring(day_of_year,ierr)
#endif

#ifdef module_meteo
   ! apply air-sea fluxes (-> sst)
   call do_meteo(dt_,ierr)
#endif

#ifdef module_rivers
   ! apply river discharges (-> sst)
   call do_rivers(dt_,ierr)
#endif

   if (ierr/=0) then
      write( error_message, '("[",i2.2,"] ERROR - do_boundaries",i3)') myID, ierr
      call stop_ecoham(ierr, msg=error_message)
   end if
   return

   end subroutine do_boundaries

!=======================================================================
! !INTERFACE:
   subroutine close_boundaries(ierr)
#define SUBROUTINE_NAME 'close_boundaries'
!
! !DESCRIPTION:
!
! !USES:
   implicit none
!
! !INPUT PARAMETERS:
   integer, intent(inout)  :: ierr
!
!-----------------------------------------------------------------------
#include "call-trace.inc"

#ifdef module_silt
   call close_silt(ierr)
#endif

#ifdef module_meteo
   call close_meteo(ierr)
#endif

#ifdef module_rivers
   call close_rivers(ierr)
#endif

#ifdef module_restoring
   call close_restoring(ierr)
#endif

   if (ierr/=0) then
      write( error_message, '("[",i2.2,"] ERROR - close_boundaries",i3)') myID, ierr
      call stop_ecoham(ierr, msg=error_message)
   end if
   return

   end subroutine close_boundaries

!=======================================================================

   end module mod_boundaries

!=======================================================================
