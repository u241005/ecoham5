!
! !MODULE: eco_init.f90  --- initializing ecoham
!
! !INTERFACE:
   MODULE mod_init
!
! !DESCRIPTION:
!
! !USES:
   use mod_common
#ifdef MPI
   use mod_mpi_parallel
#endif
   implicit none
!
!  default: all is private.
   private
! !PUBLIC MEMBER FUNCTIONS:
   public init_setup
!
! !LOCAL VARIABLES:
!
!=======================================================================

   contains

!=======================================================================
!
! !INTERFACE:
   subroutine init_setup(ierr)
#define SUBROUTINE_NAME 'init_setup'
!
! !DESCRIPTION:
!
! !USES:
   use mod_grid,   only: init_grid
   use mod_grid,   only: offset_istart, offset_iend, offset_jstart, offset_jend
   use mod_var,    only: init_vars, n_st_var, n_st1, n_st2
   use mod_flux,   only: init_fluxes
   use mod_hydro,  only: advec, dif_hor, dif_ver, dyn_hor, hev
   use mod_output, only: n_str2D_out, n_str3D_out, n_pos_out
   use mod_utils,  only: idays_of_month, get_timestamp, DOYstring
#ifdef MPI
   use mod_mpi_parallel, only: mpi_parallel_init
#endif

   implicit none
!
! !OUTPUT PARAMETERS:
   integer, intent(inout)  :: ierr
!
! !LOCAL VARIABLES:
   integer       :: icol_output, jcol_output
!  integer :: n,m
!
   namelist /pos_out_nml/icol_output,jcol_output
!
!-----------------------------------------------------------------------
#include "call-trace.inc"

   log_msg(:) = ''

#ifdef MPI
   call mpi_parallel_init(ierr_mpi)
   ierr = ierr + ierr_mpi
   if (ierr/=0) call stop_ecoham(ierr, msg='ERROR - mpi_parallel_init')

   call MPI_File_open( MPI_COMM_ECOHAM, 'eco_logfile.dat', MPI_MODE_create+MPI_MODE_wronly, &
                     & MPI_INFO_NULL, logfile_handle, ierr_mpi)
   ierr = ierr + ierr_mpi
   if (ierr/=0) call stop_ecoham(ierr, msg='ERROR - MPI_File_open for logfile')
#else
   open(log_unit,file='eco_logfile.dat',status='replace')
#endif

   day_of_year = 0.

   call CPU_Time(cpu_t1)
   call get_timestamp(timestamp)
   if (myID==master) then
      call write_log_message(line_separator)
      call write_log_message(" ECOHAM5 started on "//trim(timestamp))
      call write_log_message(line_separator)
   end if

   ! set namelist parameters to defaults
   dt_main = int(secs_per_day) ! set daily hydro coupling/update as default!
   dt_out  = int(secs_per_day) ! set daily output as default!
   n_pos_out  =0
   n_str2D_out=0
   n_str3D_out=0

   ! read namelist settings
   call read_namelists(ierr)

   ! define model grid, variables and fluxes
   call init_grid(ierr)
   call init_vars(ierr)
   call init_fluxes(ierr)

   ! allocate and initialize common variables
   call allocate_commonvars(ierr)

   ! set startday and endday
   call idays_of_month(iy1,im1,iday_start,id1)
   call idays_of_month(iy2,im2,iday_end,  id2)
   rday_start = real(iday_start -1) + max(0,min(24,ih1))/24.
   rday_end   = real(iday_end   -1) + max(0,min(24,ih2))/24.
   year = iy1

   ! set current DOY and scale digits to dt_min
   day_of_year = nint(rday_start/dt_min, kind=8) * dt_min

   ! calculate number of iterations needed
   !iterations to complete start day
   EndOfIteration = ceiling( (iday_start-rday_start) / (dt_main/secs_per_day) )
!print*,iday_start,rday_start,EndOfIteration
   !iterations for full days
   EndOfIteration = EndOfIteration + int( (int(rday_end)-iday_start)/ (dt_main/secs_per_day) )
!print*,iday_end,iday_start, int( (int(rday_end)-iday_start)/ (dt_main/secs_per_day) ),EndOfIteration
   !iterations to complete last day
   EndOfIteration = EndOfIteration + ceiling( (iday_end-rday_end) / (dt_main/secs_per_day) )
!print*,iday_end,rday_end,ceiling( (iday_end-rday_end) / (dt_main/secs_per_day) ),EndOfIteration
!call stop_ecoham(1, msg='init#120')
   ! check flags and performance
   if(isw_dim==1)then
#ifdef module_rivers
      call write_log_message("for 1D-mode (isw_dim==1) please re-compile without flag -Drivers")
      ierr = ierr + 1
#endif
#ifdef module_restoring
      call write_log_message("for 1D-mode (isw_dim==1) please re-compile without flag -Drestoring")
      ierr = ierr + 1
#endif
   end if

   ! adjust indices in case of 1D
   if (isw_dim==1) then
      ! read indices from namelist
      open(nml_unit, file=trim(settings_file), action='read', status='old')
      read(nml_unit, nml=pos_out_nml)
      close(nml_unit)
      iStartCalc = icol_output
      iEndCalc   = icol_output
      jStartCalc = jcol_output
      jEndCalc   = jcol_output
   endif

   ! define format for index string
   if ( max(imax,jmax,kmax) < 10 ) then
      write(fmt_jki,'(a)') '(''(ijk='',i1,'','',i1,'','',i1,'')'')'
   else
      if ( max(imax,jmax,kmax) < 100 ) then
         write(fmt_jki,'(a)') '(''(ijk='',i2,'','',i2,'','',i2,'')'')'
      else
         write(fmt_jki,'(a)') '(''(ijk='',i3,'','',i3,'','',i3,'')'')'
      endif
   endif

   ! define overhead
   n_st1 = 1
   n_st2 = n_st_var
   EndOfMainstep = int(dt_main/real(dt_step))
   dt    = real(dt_step)/secs_per_day  !dt (d)

   if (ierr/=0) then
      write( error_message, '("[",i2.2,"] ERROR - init_setup",i3)') myID, ierr
      call stop_ecoham(ierr, msg=error_message)
   end if

   end subroutine init_setup

!=======================================================================
!
! !INTERFACE:
   subroutine read_namelists(ierr)
#define SUBROUTINE_NAME 'read_namelists'
!
! !DESCRIPTION:
!
! !USES:
   use mod_var,          only: n_st_var
   use mod_output,       only: n_pos_out, n_str2D_out, n_str3D_out
   implicit none
!
! !OUTPUT PARAMETERS:
   integer, intent(inout)  :: ierr
!
! !LOCAL VARIABLES:
!
   namelist /set_nml/runID,iy1,im1,id1,ih1,iy2,im2,id2,ih2,       &
                     dt_step, dt_main, dt_out,                    &
                     n_pos_out, n_str2D_out, n_str3D_out

   namelist /overhead_nml/isw_dim, isw_sed,                       &
                     relrate, talk_treat, CaCO3_diss,             &
                     iwarm, iwarm_out,                            &
                     warmstart_file, warmout_file_ext

!-----------------------------------------------------------------------
#include "call-trace.inc"

! open & read global settings
!--------------------------------------------------------------
   open(nml_unit,file=trim(settings_file),action='read',status='old')
   read(nml_unit,nml=set_nml)
   read(nml_unit,nml=overhead_nml)
   close(nml_unit)

   ! limit output options
   if (dt_out < 60) dt_out = int(secs_per_day)
   if (isw_dim==1) then
      n_pos_out   = 1
      n_str2D_out = 0
      n_str3D_out = 0
   endif

   if (ierr/=0) then
      write( error_message, '("[",i2.2,"] ERROR - read_namelists",i3)') myID, ierr
      call stop_ecoham(ierr, msg=error_message)
   end if

   end subroutine read_namelists

!=======================================================================

   end module mod_init

!=======================================================================
