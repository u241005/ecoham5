#ifdef module_sections
!
! !MODULE: eco_sections.f90 --- sectional fluxes for ecoham
!          unit of these fluxes: unit of parameter per day (e.g. dic: mmol C/d).
!
! !INTERFACE:
   MODULE mod_sections
!
! !DESCRIPTION:
!
! !USES:
   use mod_common
   use mod_var,    only : n_st_var, st_name, st_long
   implicit none
!
   save
!  default: all is public.
   public
!
! !LOCAL VARIABLES:
   logical                       :: l_sections = .true.

   character(len=*),   parameter :: sec_endian   = 'LITTLE_ENDIAN'
   integer,            parameter :: sec_realkind = 4   != ibyte_per_real
   integer,            parameter :: sec_intkind  = 4   != ibyte_per_integer

   character(len=99)             :: sec_ofile
   integer                       :: secMax
   real,             allocatable :: f_sec(:,:,:)
   integer,          allocatable :: iSsec(:), iEsec(:), jSsec(:), jEsec(:)
   integer,          allocatable :: secNum(:), secDir(:)
   integer,          allocatable :: secNum3D(:,:,:)
   character(len=3), allocatable :: secName(:)


!=======================================================================

   contains

!=======================================================================
!
! !INTERFACE:
   subroutine init_sections_output(ierr)
#define SUBROUTINE_NAME 'init_sections_output_sections'
!
! !DESCRIPTION:
!
! !USES:
   IMPLICIT NONE
!
! !OUTPUT PARAMETERS:
   integer, intent(inout)  :: ierr
!
! !LOCAL VARIABLES:
   integer                 :: ios, iS, iE, jS, jE, n
   character(len=99)       :: sec_ifile = 'sections.txt'

   namelist /sections_nml/ l_sections, sec_ifile, sec_ofile
!
!-----------------------------------------------------------------------
#include "call-trace.inc"

   open(nml_unit,file=trim(settings_file),action='read',status='old')
   read(nml_unit,nml=sections_nml)
   close(nml_unit)

   if (.not. l_sections) return

   allocate (secNum3D(jMax,kMax,iStartD:iEndD)); secNum3D = 0
   if (myID==master) call write_log_message(' INITIALIZING section output')
   secMax = 0; ios = 0
   open (asc_unit, file = trim(sec_ifile), form = 'formatted',status = 'old')
   read (asc_unit, *)
   do while (ios == 0)
     read (asc_unit, *, iostat = ios)
     if (ios /= 0) exit
     secMax = secMax + 1
   enddo
   rewind (asc_unit)
   allocate (secName(secMax))
   allocate (iSsec(secMax), iEsec(secMax), jSsec(secMax), jEsec(secMax), secNum(secMax), secDir(secMax))
   read   (asc_unit, *)
   do n = 1, secMax
     read (asc_unit, *) iSsec(n), iEsec(n), jSsec(n), jEsec(n), secNum(n), secDir(n), secName(secNum(n))
   enddo
   close (asc_unit)
   allocate ( f_sec(1:kMax,secMax,n_st_var) ); f_sec = 0.
   if (myID == master) then
     write(log_msg(1),'(3x,a,i0,2a)') 'unit ',sec_unit, ' associated to file ', trim(runID)//trim(sec_ofile)
     write(log_msg(2),'(3x,a,i0,1x,i0)') 'number of partial sections, number of sections: ', secMax, maxval(secNum)
     long_msg = trim(log_msg(1))//new_line('a')//trim(log_msg(2))
     call write_log_message(long_msg); long_msg = ''
     do n = 1, secMax
       write (long_msg,'(3x,6i5,2a)') iSsec(n), iEsec(n), jSsec(n), jEsec(n), secNum(n), secDir(n), '  ', secName(secNum(n))
       call write_log_message(long_msg)
     enddo
   endif

   do n = 1, secMax
     iS = iSsec(n); iE = iEsec(n)
     if (iS <= iEndCalc .and. iE >= iStartCalc) then
       iS = max(iStartCalc,iS); iE = min(iEndCalc,iE); jS = jSsec(n); jE = jEsec(n)
       secNum3D(jS:jE,:,iS:iE) = n
     endif
   enddo

   if (index(sec_ofile,'.bin') /= 0) then
     open (sec_unit, file=trim(runID)//trim(sec_ofile), form='unformatted', convert=trim(sec_endian))
     write (sec_unit) int(kMax, sec_intkind), int(maxval(secNum), sec_intkind), int(n_st_var, sec_intkind)
   else
     open (sec_unit, file=trim(runID)//trim(sec_ofile), form='formatted')
     write (sec_unit,*) kMax, maxval(secNum), n_st_var
   endif

   if (ierr/=0) then
      write( error_message, '("[",i2.2,"] ERROR - init_sections_output: ",i3)') myID,ierr
      call stop_ecoham(ierr, msg=error_message)
   end if
   return

   end subroutine init_sections_output

!=======================================================================
!
! !INTERFACE:
   subroutine do_sections_output(ierr)
#define SUBROUTINE_NAME 'do_sections_output'
!
! !DESCRIPTION:
!
! !USES:
#ifdef MPI
   use mod_mpi_parallel, only: mpi_parallel_add_real
#endif
   IMPLICIT NONE
   integer, intent(inout) :: ierr
!
! !LOCAL VARIABLES:
   integer           :: k, n, s, smax
   real, allocatable :: sec_data(:,:,:)
#ifdef MPI
   real, allocatable :: sec_data_sum(:,:,:)
#endif
   character(len=30) :: sec_fmt
!
!-----------------------------------------------------------------------
#include "call-trace.inc"

   if (day_of_year < 1.e-6) return
   smax = maxval(secNum)
   allocate ( sec_data(kMax,smax,n_st_var) ); sec_data = 0.
   do s = 1, secMax
     sec_data(:,secNum(s),:) = sec_data(:,secNum(s),:) + f_sec(:,s,:)
   enddo

#ifdef MPI
   allocate ( sec_data_sum(kMax,smax,n_st_var) ); sec_data_sum = 0.
   call mpi_parallel_add_real (sec_data, sec_data_sum, kMax*smax*n_st_var, ierr_mpi)
   ierr = ierr + ierr_mpi
   sec_data = sec_data_sum
   deallocate (sec_data_sum)
#endif
   if (myID == master) then
      if (index(sec_ofile,'.bin') /= 0) then
         write (sec_unit) real(day_of_year, sec_realkind), real(sec_data, sec_realkind)
      else
         write (sec_fmt,'(a,i0,a)') '(1x,f10.4,1x,i0,1x,i0,1x,', smax, 'e15.5)'
         do n = 1,n_st_var
            do k = 1,kMax
               write (sec_unit, trim(sec_fmt)) day_of_year, n, k, sec_data(k,1:smax,n)
            enddo
         enddo
      endif
   endif
   f_sec = 0.

   deallocate (sec_data)

   if (ierr/=0) then
      write( error_message, '("[",i2.2,"] ERROR - do_sections_output: ",i3)') myID,ierr
      call stop_ecoham(ierr, msg=error_message)
   else
      if (myID==master) then
         write(long_msg,'(a,i0)') '   data written to section unit ', sec_unit
         call write_log_message(long_msg); long_msg = ''
      endif
   end if

   return

   end subroutine do_sections_output

!=======================================================================

   end module mod_sections

!=======================================================================
#endif
