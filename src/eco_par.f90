!
! !MODULE: eco_par.f90 --- definition of global parameters
!
! !INTERFACE:
   MODULE mod_par
!
! !DESCRIPTION:
!
! !USES:
   implicit none
!
!  default: all is public.
   public

   ! GLOBAL PARAMETERS
   real, parameter    :: dt_min         = 1.e-8    ! dt_min [d]: smallest timestep
   real, parameter    :: zero           = 0.0
   real, parameter    :: one            = 1.0
   real, parameter    :: fail           = -9.999e10
   integer, parameter :: ibyte_per_r8   = 8
   integer, parameter :: ibyte_per_r4   = 4

   ! EARTH PARAMETERS
   real, parameter    :: pi             = 3.141592653589793116
   real, parameter    :: earth_rad      = 6371.040
   real, parameter    :: earth_umf      = 2.*pi*earth_rad
   real, parameter    :: secs_per_day   = 86400.

   ! UNIVERSAL GAS CONSTANT [bar*cm3/mol/K]
   real, parameter    :: rgas=83.1451
   ! ABSOLUTE TEMPERATURE at 0 deg Celsius [K]
   real, parameter    :: temzer=273.16
   ! c0 from six p.36 in mmol C m-3
   real, parameter    :: c0=100.

   ! Wanninkhof, R., 1992 JGR 97:7373-7382 Schmidt Number for CO2
   real, parameter    :: scac=2073.1
   real, parameter    :: scbc=125.62
   real, parameter    :: sccc=3.6276
   real, parameter    :: scdc=0.043219

   ! Wanninkhof, R., 1992 JGR 97:7373-7382 Schmidt Number for oxygen
   real, parameter    :: scao=1953.4
   real, parameter    :: scbo=128.0
   real, parameter    :: scco=3.9918
   real, parameter    :: scdo=0.050091

   !Unesco 1986 Solubility of oxygen [mumol dm-1] (improvement of Weiss 1970)
   real, parameter    :: c1o=-135.90205
   real, parameter    :: c2o= 1.575701e5
   real, parameter    :: c3o=-6.642308e7
   real, parameter    :: c4o= 1.243800e10
   real, parameter    :: c5o=-8.621949e11
   real, parameter    :: d1o= 0.017674
   real, parameter    :: d2o=-10.754
   real, parameter    :: d3o= 2140.7

!=======================================================================

   end module mod_par

!=======================================================================