!
! !MODULE: eco_chemie.f90 --- major subroutines for ecoham carbonate chemistry
!
! !INTERFACE:
   MODULE mod_chemie
#ifdef module_chemie
!
! !DESCRIPTION:
! This module is the container for carbonate chemistry subroutines
!
! !USES:
   use mod_common
   use mod_grid
!   use mod_mpi_parallel
   use mod_hydro, only : temp, salt, rho, pres
   implicit none
!
!  default: all is private.
   private
!
! !PUBLIC MEMBER FUNCTIONS:
   public init_chemie, update_chemie, do_chemie
!
! !LOCAL VARIABLES (public):
   ! parameters for chemical STATE VARIABLES:
   integer, parameter, public :: n_ch_var = 8
   integer           , public :: n_ch1=1, n_ch2=n_ch_var
   ! INDEX-pointers to chemical STATE VARIABLES:
   integer, parameter, public :: itdic=1,italk=2,iprot=3,itbor=4
   integer, parameter, public :: ixbor=5,io2c=6,io3c=7,iohc=8
   ! container (array) for chemical STATE VARIABLES:
   real,   dimension(:,:,:,:), allocatable,  public :: ch
   logical,             dimension(n_ch_var), public :: ch_out2D, ch_out3D
   character(len=6),    dimension(n_ch_var), public :: ch_name
   character(len=99),   dimension(n_ch_var), public :: ch_long, ch_unit
!
! !LOCAL VARIABLES (private):
   integer            :: iup_chem
   integer, parameter :: n_iter_max=15

#ifdef cskc
   character(len=*), parameter, private :: init_endian   = 'LITTLE_ENDIAN'
#else
   character(len=*), parameter, private :: init_endian   = 'BIG_ENDIAN'
#endif
   character(len=*), parameter, private :: init_dir      = 'Input/'
   integer,          parameter, private :: init_realkind = 8   != ibyte_per_real
!
!=======================================================================

   contains

!=======================================================================
!
! !INTERFACE:
   subroutine set_varnames(ierr)
#define SUBROUTINE_NAME 'set_varnames'
!
! !DESCRIPTION:
!
! !USES:
   IMPLICIT NONE
!
! !OUTPUT PARAMETERS:
   integer, intent(inout)  :: ierr
!
! !LOCAL VARIABLES:
!
!-----------------------------------------------------------------------
#include "call-trace.inc"

   ! clear character arrays
   ch_name(:) = ''
   ch_long(:) = ''
   ch_unit(:) = ''

   ! chemical variables ("ch")
   ch_name(itdic)='tdic'
   ch_long(itdic)='total dissolved inorganic carbon'
   ch_unit(itdic)='mumol C kg-1'

   ch_name(italk)='talk'
   ch_long(italk)='total alkalinity'
   ch_unit(italk)='mumol C kg-1'

   ch_name(iprot)='prot'
   ch_long(iprot)='concentration of protons'
   ch_unit(iprot)='mumol kg-1'

   ch_name(itbor)='tbor'
   ch_long(itbor)='boric acid (B(OH)3)'
   ch_unit(itbor)='mumol kg-1'

   ch_name(ixbor)='xbor'
   ch_long(ixbor)='tetrahydroxyborate (B(OH)4-)'
   ch_unit(ixbor)='mumol kg-1'

   ch_name(io2c)='o2c'
   ch_long(io2c)='carbondioxide (CO2)'
   ch_unit(io2c)='mumol kg-1'

   ch_name(io3c)='o3c'
   ch_long(io3c)='carbonate (CO3--)'
   ch_unit(io3c)='mumol kg-1'

   ch_name(iohc)='ohc'
   ch_long(iohc)='bicarbonate (HCO3-)'
   ch_unit(iohc)='mumol kg-1'

   if (ierr/=0) then
      write( error_message, '("[",i2.2,"] ERROR - set_varnames(chemie)",i3)') myID, ierr
      call stop_ecoham(ierr, msg=error_message);
   end if
   return

   end subroutine set_varnames

!=======================================================================
!
! !INTERFACE:
   subroutine init_chemie(ierr)
#define SUBROUTINE_NAME 'init_chemie'
!
! !DESCRIPTION:
!
! !USES:
   use mod_var,   only : n_st_var
   use mod_utils, only : calcite
   implicit none
!
! !INPUT PARAMETERS:
   integer, intent(inout) :: ierr
!
! !LOCAL VARIABLES:
   real                 :: ph0=8.
   integer              :: n
   logical              :: file_exist
   real(kind=init_realkind), allocatable :: input1d(:), input3d(:,:,:)
!
!-----------------------------------------------------------------------
#include "call-trace.inc"

   if (myID==master) call write_log_message(" INITIALIZING chemie ")

! #if defined exclude_restoring_cells_chemie && !defined module_restoring
!    ierr = 1
!    error_message = 'ERROR: You may not specify -Dexclude_restoring_cells_chemie without Flag -Dmodule_restoring!'
!    call stop_ecoham(ierr, msg=error_message)
! #endif

   allocate( ch(jmax,kmax,iStartD:iEndD,n_ch_var))
   ch = fail

   call set_varnames(ierr)

   ! initialise 3D-output logics
   ch_out3D = .false.

   !initialize as coldstart
   if (iwarm /= 1) then

      if (myID==master) call write_log_message("   coldstart initialization")
      ! default initialization, homogenous distribution. NOTE: here, "rho" is kg/l = (kg/m3)/1000
      where(ixistz(:,:,:)==1) ch(:,:,:,itdic)= 2163./rho           ! TDIC conc (mmol kg-1)
      where(ixistz(:,:,:)==1) ch(:,:,:,italk)= 2358./rho           ! TALK conc (mmol kg-1)
      where(ixistz(:,:,:)==1) ch(:,:,:,iprot)= (10.**(6.-ph0))/rho ! proton conc (mmol kg-1)
      where(ixistz(:,:,:)==1) ch(:,:,:,itbor)= 416.*salt/35.       ! boric acid (B(OH)3) (mmol kg-1)
      where(ixistz(:,:,:)==1) ch(:,:,:,ixbor)= 72./rho             ! tetrahydroxyborate (B(OH)4-) (mmol kg-1)
      where(ixistz(:,:,:)==1) ch(:,:,:,io2c )= 15./rho             ! carbondioxide (CO2) (mmol kg-1)
      where(ixistz(:,:,:)==1) ch(:,:,:,io3c )= 137./rho            ! carbonate (CO3--) (mmol kg-1)
      where(ixistz(:,:,:)==1) ch(:,:,:,iohc )= 2010./rho           ! bicarbonate (HCO3-) (mmol kg-1)

      if (iwarm == 2) then
        ! read from initial distribution files, if exist, and overwrite
         allocate (input1d(iiwet),input3d(jMax,kMax,iMax))
         do n = 1, n_ch_var
            filename = trim(init_dir)//'init.'//trim(ch_name(n))//'.direct'
            inquire (file=trim(filename), exist=file_exist)
            if (file_exist) then
               if (myID == master) call write_log_message("   include initialization from "//trim(filename) )
               open (warm_unit, file=trim(filename), status='old', form='unformatted', &
                                access='direct', recl=init_realkind*iiwet, convert=trim(init_endian) )
               read (warm_unit, rec = 1) input1d
               input3d(:,:,:) = unpack(real(input1d),ixistz_master(:,:,:)==1,fail)
               ch(:,:,:,n)    = input3d(:,:,iStartD:iEndD) !./rho !!! hack in case of old restart files !!!
               close (warm_unit)
            else
              if (myID == master) call write_log_message("   init file "//trim(filename)//" does not exist" )
            endif
         enddo
         deallocate (input1d, input3d)
      endif

   endif

   if (ierr/=0) then
      write( error_message, '("[",i2.2,"] ERROR - init_chemie",i3)') myID, ierr
      call stop_ecoham(ierr, msg=error_message);
   end if
   return

   end subroutine init_chemie

!=======================================================================
!
! !INTERFACE:
   subroutine update_chemie(mode,ierr,dic,alk)
#define SUBROUTINE_NAME 'update_chemie'
!
! !DESCRIPTION:
! This subroutine is a wrapper for passing arrays forth and back from
! the chemistry module to others
!
! the unit of all concentrations within the biolog. model are [mmol m-3]
! the units in the chemical submodel are [mol kg-1 soln] or [mumol kg-1 soln]
! NOTE: in ECOHAM "rho" is kg/l = (kg/m3)/1000 !!!
!
! !USES:
   implicit none
!
! !INPUT PARAMETERS:
   character(3), intent(in) :: mode
   real, intent(inout),dimension(jmax,kmax,iStartD:iEndD) :: dic, alk !jp
   integer, intent(inout) :: ierr
!
! !LOCAL VARIABLES:
   integer              :: i,j,k,k0
!
!-----------------------------------------------------------------------
#include "call-trace.inc"

   !if (myID==master) call write_log_message(" UPDATING chemie ")

   select case (isw_dim) !jp
      case (3) !3D-mode
         if (mode=='put')then
            where(ixistz(:,:,:)==1)ch(:,:,:,itdic) = dic(:,:,:)/rho(:,:,:)
            where(ixistz(:,:,:)==1)ch(:,:,:,italk) = alk(:,:,:)/rho(:,:,:)
         elseif(mode=='get')then
            dic(:,:,iStartCalc:iEndCalc) = ch(:,:,iStartCalc:iEndCalc,itdic)*rho(:,:,iStartCalc:iEndCalc)
            alk(:,:,iStartCalc:iEndCalc) = ch(:,:,iStartCalc:iEndCalc,italk)*rho(:,:,iStartCalc:iEndCalc)
        else
            ierr = 1
         endif
      case (1) !1D-mode
         i = iStartCalc
         j = jStartCalc
         k0 = k_index(j,i)
         do k = 1, k0
            if (mode=='put')then
               ch(j,k,i,itdic) = dic(j,k,i) /rho(j,k,i)
               ch(j,k,i,italk) = alk(j,k,i) /rho(j,k,i)
            elseif(mode=='get')then
               dic(j,k,i)  = ch(j,k,i,itdic)*rho(j,k,i)
               alk(j,k,i)  = ch(j,k,i,italk)*rho(j,k,i)
            else
               ierr = 1
            endif
         enddo
   end select
   if (ierr/=0) then
      write( error_message, '("[",i2.2,"] ERROR - update_chemie",i3)') myID, ierr
      call stop_ecoham(ierr, msg=error_message);
   end if
   return

   end subroutine update_chemie

!=======================================================================
!
! !INTERFACE:
   subroutine do_chemie(iadjust,ierr,dic,alk)
#define SUBROUTINE_NAME 'do_chemie'
!
! !DESCRIPTION:
! iadjust = -1 : alk and dic known ph unknown, new alk without bio
! iadjust =  0 : ph, dic known                new alk without bio
! iadjust =  1 : alk, dic known, ph unknown   new alk with bio
! iadjust =  2 : alk, dic known, ph unknown   only CO2 is determined
!
! !USES:
   use mod_utils,     only: press_cor, calcite
#if defined module_restoring && defined exclude_restoring_cells_chemie
   use mod_var,       only: idic
   use mod_restoring, only: k_index_exclude_restoring
#endif
   implicit none
!
! !INPUT PARAMETERS:
   integer, intent(in)  :: iadjust
   real, intent(inout),dimension(jmax,kmax,iStartD:iEndD) :: dic, alk !jp
   integer, intent(inout) :: ierr
!
! !LOCAL VARIABLES:
   integer       :: i,j,k,k0,ic_pro
   real          :: dic_,alk_,o3c_,ohc_,o2c_  ! local conc. in [mumol kg-1 soln]
   real          :: xbor_,tbor_,prot_         ! local conc. in [mumol kg-1 soln]
   real          :: a_mil,b_mil,pk1,pk2,prot0,eps
   real          :: xk10,xk20,xkb,xkw,tg,tz,t1
   real          :: rt,s1,s12,s2,s32,alk_new,alphas
!
!-----------------------------------------------------------------------
#include "call-trace.inc"

   eps=1.e-4
   select case (isw_dim) !jp
      case (3) !3D-mode
         where(ixistz(:,:,:)==1)ch(:,:,:,itdic) = dic(:,:,:)  /rho(:,:,:)
         where(ixistz(:,:,:)==1)ch(:,:,:,italk) = alk(:,:,:)  /rho(:,:,:)
         where(ixistz==1) ch(:,:,:,itbor)= 416.*salt/35. ! estimate boric acid (B(OH)3) from salinity
      case (1) !1D-mode
         i = iStartCalc
         j = jStartCalc
         k0 = k_index(j,i)
         do k = 1, k0
            ch(j,k,i,itdic) = dic(j,k,i)  /rho(j,k,i)
            ch(j,k,i,italk) = alk(j,k,i)  /rho(j,k,i)
            ch(j,k,i,itbor) = 416.*salt(j,k,i)/35. ! estimate boric acid (B(OH)3) from salinity
         enddo
   end select

   ! Millero's talk data by salt for the Atlantic
   a_mil = 520.1
   b_mil = 51.24

!---------------------------------------------------------!
   do i = iStartCalc, iEndCalc
      do k = 1, kMax
         do j = jStartCalc, jEndCalc
            k0 = k_index(j,i)
#if defined module_restoring && defined exclude_restoring_cells_chemie
             k0 = k_index_exclude_restoring(j,i,idic)
#endif
            if (k > k0) cycle

            call calcite(temp(j,k,i),salt(j,k,i),rho(j,k,i),pres(j,k,i),ca(j,k,i),aksp(j,k,i))

            ! the units in the chemical submodel are [mol kg-1 soln] or [mumol kg-1 soln]
            dic_  = ch(j,k,i,itdic)
            alk_  = ch(j,k,i,italk)
            prot_ = ch(j,k,i,iprot)
            tbor_ = ch(j,k,i,itbor)

            !set coefficients
            s12= sqrt(salt(j,k,i))
            s1 = salt(j,k,i)
            s32= salt(j,k,i)*s12
            s2 = salt(j,k,i)*salt(j,k,i)
            t1 = temp(j,k,i)+temzer
            tg = alog(t1)
            tz = alog10(t1)
            rt = rgas * t1

            ! boron acidity constant after Dickson (1990) [mol/kg soln]
            ! pH scale: total
            xkb = exp((-8966.9-2890.53*s12-77.942*s1+1.728*s32-.0996*s2)/t1+148.0248+137.1942*s12+   &
                  1.62142*s1-(24.4344+25.085*s12+.2474*s1)*tg+0.053105*s12*t1)
            ! convert tot->sws
            xkb = tot2sws(xkb,salt(j,k,i),t1)
            ! unit [mumol kg-1]
            xkb = xkb*1.e6
            ! pressure correction
!jp         xkb = xkb*press_cor(-29.48,.1622,2.608e-3,-2.84,   0.,temp(j,k,i),pres(j,k,i),rt)   !jp+bm 30.11.2015
            xkb = xkb*press_cor(-29.48,.1622,2.608e-3,-2.84e-3,0.,temp(j,k,i),pres(j,k,i),rt)

            ! carbonate acidity constants after Goyet and Poisson (1989) and Hannson (1973)
            ! in [mumol/kg soln]
            ! pk1 = (812.27/t1+3.356-0.00171*s1*tg+.000091*s2)
            ! pk2 = (1450.87/t1+4.604-.00385*s1*tg+.000182*s2)
            ! carbonate acidity constants after Goyet and Poisson (1989) org.
            ! in [mol/kg soln] sws
            ! pk1 = (807.18/t1+3.374-0.00175*s1*tg+.000095*s2)
            ! pk2 = (1486.60/t1+4.491-.00412*s1*tg+.000215*s2)

            ! Mehrbach
            pk1 = 3670.7/t1-62.008+9.7944*tg-0.0118*s1+0.000116*s2
            pk2 = 1394.7/t1+4.777-0.0184*s1+0.000118*s2

            ! unit [mumol kg-1]
            xk10 = 10.**(6.-pk1)
            xk20 = 10.**(6.-pk2)
!jp         xk10 = xk10*press_cor(-25.5,.1271,0.,-3.08,   .0877,   temp(j,k,i),pres(j,k,i),rt)    !jp+bm 30.11.2015
            xk10 = xk10*press_cor(-25.5,.1271,0.,-3.08e-3,.0877e-3,temp(j,k,i),pres(j,k,i),rt)
!jp         xk20 = xk20*press_cor(-15.82,-.0219,0.,1.13,   -.1475,   temp(j,k,i),pres(j,k,i),rt)  !jp+bm 30.11.2015
            xk20 = xk20*press_cor(-15.82,-.0219,0.,1.13e-3,-.1475e-3,temp(j,k,i),pres(j,k,i),rt)

            ! ion product of water after Dickson and Goyet (1994) and Millero 1995
            ! in [(mol/kg soln)**2]
            xkw = exp(-13847.26/t1+148.96502-23.6521*tg+(118.67/t1-5.977+1.0495*tg)*s12-0.01615*s1)

            ! convert tot->sws
            xkw = tot2sws(xkw,salt(j,k,i),t1)

            ! unit [mumol kg-1]
            xkw = xkw*1.e12
!jp         xkw = xkw*press_cor(-25.6,.2324,-3.6246e-3,-5.13,   0.0794,   temp(j,k,i),pres(j,k,i),rt) !jp+bm 30.11.2015
            xkw = xkw*press_cor(-25.6,.2324,-3.6246e-3,-5.13e-3,0.0794e-3,temp(j,k,i),pres(j,k,i),rt)

            ! calculate proton concentration
            if (iadjust/=0) then
               prot0 = prot_
               call proton(dic_,alk_,xkb,xk10,xk20,xkw,tbor_,prot_,ierr)
               ic_pro = 1
               do while (ic_pro <= n_iter_max .and. ierr /= 0)
                  ! call proton with a slightly higher starting value
                  prot0 = prot0*1.05
                  prot_ = prot0
! if (i==5.and.j==5.and.k==1)then
! if (i==77.and.j==31.and.k==1)then
!                 write(*,'(a12,8e16.8,i3)') 'chemie#436:',dic_,alk_,xkb,xk10,xk20,xkw,tbor_,prot_,ierr
!                 call proton(dic_,alk_,xkb,xk10,xk20,xkw,tbor_,prot_,ierr)
!                 write(*,'(a12,8e16.8,i3)') 'chemie#438:',dic_,alk_,xkb,xk10,xk20,xkw,tbor_,prot_,ierr
! else
                  call proton(dic_,alk_,xkb,xk10,xk20,xkw,tbor_,prot_,ierr)
! endif
                  ic_pro = ic_pro +1
               enddo
               if (ierr /= 0) exit
            endif
            ! calculate CO2 concentration after Wolf-Gladrow 1996 p.42
            o2c_ = dic_/(1.+xk10/prot_+xk10*xk20/prot_/prot_)
            if (iadjust.ne.2) then

               ! calculate HCO3- concentration after Wolf-Gladrow 1996 p.42
               ohc_ = dic_/(1.+prot_/xk10+xk20/prot_)

               ! calculate CO3-- concentration after Wolf-Gladrow 1996 p.42
               o3c_ = dic_/(1.+prot_/xk20+prot_*prot_/xk10/xk20)

               ! calculate  B(OH)4- concentration
               xbor_ = xkb*tbor_/(prot_+xkb)

               alk_new = 2.*o3c_+ohc_+xbor_-prot_+xkw/prot_
               !if(abs(alk_new-alk_)>1.e-3)then
               if(abs(alk_new-alk_)>1.e-1)then
                  write( long_msg, '("WARNING: alk_ not conservative i,j,k:", &
                     & 2f15.8, f12.8, 3i8)') alk_new, alk_, alk_new-alk_, i, j, k
                  call write_log_message(long_msg); long_msg = ''
                  !if(abs(alk_new-alk_)>1.e-1)then
                  !   call stop_ecoham(1, msg='ERROR in SUBROUTINE chemie: alk_ not conservative')
                  !endif
               endif

               ch(j,k,i,iohc)  = ohc_
               ch(j,k,i,io3c)  = o3c_
               ch(j,k,i,ixbor) = xbor_
            endif

            !ch(j,k,i,itdic) = dic_   !MK+JP: should not be modified within this subroutine
            !ch(j,k,i,italk) = alk_   !MK+JP: should not be modified within this subroutine
            ch(j,k,i,itbor) = tbor_
            ch(j,k,i,iprot) = prot_
            ch(j,k,i,io2c)  = o2c_

            ! calculate pCO2 in water after Peng et al. 1986 p.454
            ! see also Weiss, 1974
            alphas = exp( -60.2409 + 9345.17/t1 + 23.3585*alog(t1/100.)  &
                          +s1*(.023517 -.023656*t1/100. +1.e-8*47.036*t1*t1) )
            pco2w(j,k,i) = o2c_/alphas !pCO2 in muatm
         enddo
         if (ierr /= 0) exit
      enddo
      if (ierr /= 0) exit
   enddo

   if (ierr /= 0) then
      write( long_msg, '("ERROR in subroutine proton: proc,i,j,k,ierr,prot_,dic_,alk_:",5i5,3e15.8)') &
                       &  myID, i, j, k, ierr, prot_, dic_, alk_
      call write_log_message(long_msg); long_msg = ''
      !write(*,'(8e16.8)') dic_,alk_,xkb,xk10,xk20,xkw,tbor_,prot_
   else
      ! the unit of all concentrations within the biolog. model are [mmol m-3]
      ! the units in the chemical submodel are [mol kg-1 soln] or [mumol kg-1 soln]
      ! uni_fac* [mmol m-3] = [mumol kg-1 soln]
      ! NOTE: in ECOHAM "rho" is kg/l = (kg/m3)/1000 !!!
      select case (isw_dim) !jp
         case (3) !3D-mode
         case (1) !1D-mode
            i = iStartCalc
            j = jStartCalc
            k0=k_index(j,i)
            do k=1,k0
            enddo
      end select
   endif

   if (ierr/=0) then
      write( error_message, '("[",i2.2,"] ERROR - do_chemie: ",i3)') myID,ierr
      call stop_ecoham(ierr, msg=error_message);
   end if
   return

   end subroutine do_chemie

!=======================================================================
!
! !INTERFACE:
   subroutine proton(d,a,b,x,y,w,t,p,iret)
#define SUBROUTINE_NAME 'proton'
!
! !DESCRIPTION:
! calculation of proton concentration after Wolf-Gladrow 1996 p.45
! using a NEWTON - iteration
!
! !USES:
   implicit none
!
! !INPUT PARAMETERS:
   real, intent(in)      :: d,a,b,x,y,w,t
   real, intent(inout)   :: p
!
! !OUTPUT PARAMETERS:
   integer, intent(out)  :: iret
!
! !LOCAL VARIABLES:
   integer       :: l, lmax
   real          :: eps, fp0, fps0!, fpsfp
!
!-----------------------------------------------------------------------
!#include "call-trace.inc"

   iret = 0
   l = 0
   lmax = 100
   eps = 1.0e-8
   do while (l<=lmax .and. iret==0)
      l = l+1
      fp0 = fp(d,a,b,x,y,w,t,p)
      fps0 = fps(d,a,b,x,y,w,t,p)
      if (abs(fps0) < eps/1000.) then
         iret = 2
         call write_log_message("DERIVATE = 0")
         return
      endif
      p = p - fp0/fps0
      if (l>lmax) then
         iret = 1
         write(log_msg(1),'("NEWTON ITERATION did not converge")')
         !write(log_msg(2),*) fp0, fps0, d, a, b, x, y, w, t, p ! too long !!
         write(log_msg(2),'(5e24.16)') fp0, fps0, d, a, b
         write(log_msg(3),'(5e24.16)') x, y, w, t, p
         long_msg = trim(log_msg(1))//new_line('a')//trim(log_msg(2))//trim(log_msg(3))
         call write_log_message(long_msg); long_msg = ''
         return
      else
         if (abs(fp0)<eps) then
            if (p.lt.0.0) iret = 3
            return
         endif
      endif
   enddo

   end subroutine proton

!=======================================================================
! !INTERFACE:
   real function fp(d,a,b,x,y,w,t,p)
!#define FUNCTION_NAME 'fp'
!
! !DESCRIPTION:
!
! !USES:
   implicit none
!
! !INPUT PARAMETERS:
   real, intent(in)   :: d,a,b,x,y,w,t,p
!-----------------------------------------------------------------------
!#include "call-trace.inc"
   fp = ((((p+a+b+x)*p                                             &
       -d*x+a*b-b*t-w+a*x+b*x+x*y)*p                               &
       -2.*d*x*y-d*b*x-w*b+a*b*x-b*t*x-w*x+a*x*y+b*x*y)*p          &
       -2.*d*b*x*y-w*b*x+a*b*x*y-b*t*x*y-w*x*y)*p                  &
       -w*b*x*y
   return
   end function fp

!=======================================================================
! !INTERFACE:
   real function fps(d,a,b,x,y,w,t,p)
!#define FUNCTION_NAME 'fps'
!
! !DESCRIPTION:
!
! !USES:
   implicit none
!
! !INPUT PARAMETERS:
   real, intent(in)   :: d,a,b,x,y,w,t,p
!-----------------------------------------------------------------------
!#include "call-trace.inc"
   fps = (((5.+4.*(a+b+x))*p                                       &
         +3.*(-d*x+a*b-b*t-w+a*x+b*x+x*y))*p                       &
         +2.*(-2.*d*x*y-d*b*x-w*b+a*b*x-b*t*x-w*x+a*x*y+b*x*y))*p  &
         -2.*d*b*x*y-w*b*x+a*b*x*y-b*t*x*y-w*x*y
   return
   end function fps

!=======================================================================
!
! !INTERFACE:
   real function tot2sws(x,s,t)
!#define FUNCTION_NAME 'tot2sws'
!
! !DESCRIPTION:
! convert ki from total scale to sws scale
! sea Millero, 1995, p664 and Gladrow 1996
! x: massenwirkungsconst(tot)
! s: salinity
!
! !USES:
   implicit none
!
! !INPUT PARAMETERS:
   real, intent(in)   :: x,s,t
!
! !LOCAL VARIABLES:
   real               :: so4,f,xi,xi02,xi32,xi2,tg,xs,xhso4,st,xf,qh
!
!-----------------------------------------------------------------------
!#include "call-trace.inc"

   so4 = 0.0293*s/35.
   f = 0.00007*s/35.
   xi = 19.924*s/(1000.-1.005*s)
   xi02 = sqrt(xi)
   xi32 = xi*xi02
   xi2 = xi*xi
   tg = alog(t)
   ! xs(=Ks) after D&G 1994 pH free scale
   ! see Gladrow, 1996
   xs = exp(-4276.1/t + 141.328 - 23.093*tg                  &
          +(-13856./t + 324.57 - 47.986*tg)*xi02             &
          +( 35474./t - 771.54 + 114.723*tg)*xi              &
          -2698./t*xi32 + 1776./t*xi2 + alog(1.-.001005*s))
   ! estimate for converting free to tot
   xhso4 = 1.e-8*so4*xs
   st = so4+xhso4

   ! xf(=Kf) after D&G 1994 or D&R 1979 ph tot
   ! see Gladrow, 1996
   xf = exp(1590.2/t - 12.641 + 1.525*xi02+alog(1.-.001005*s)+alog(1.+st/xs))
   qh = (1.+so4/xs+f/xf)/(1.+so4/xs)
   tot2sws = x*qh

   return

   end function tot2sws

!=======================================================================
# endif
   end module mod_chemie

!=======================================================================
