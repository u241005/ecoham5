#ifndef sediment_EMR
! !MODULE: eco_coupler.f90 --- benthic <-> interface for ecoham
!
! !INTERFACE:
   MODULE mod_coupler
#ifdef module_sediment
!
! !DESCRIPTION:
! This module is the container for pelagic <-> benthic biogeochemical flux wrappers
!
! !USES:
   use mod_common
   use mod_grid
#ifdef MPI
   use mod_mpi_parallel
#endif
   use mod_var
   use mod_flux
   implicit none
!
!  default: all is private.
   private
!
! !PUBLIC MEMBER FUNCTIONS:
   public pel2sed, sed2pel
!
! !LOCAL VARIABLES (private):
!
!=======================================================================

   contains

!=======================================================================
#ifdef module_biogeo
!
! !INTERFACE:
   subroutine sed2pel(ierr)
#define SUBROUTINE_NAME 'sed2pel'
!
! !DESCRIPTION:
! routine handles diffusive fluxes (mmol m-3 d-1) from the sediment
! into the pelagic system (incl benthic denitri)
!
! !USES:
   use mod_sediment, only : f_n3n_brm, f_o2o_brm, f_sed_dic
   use mod_sediment, only : f_sed_n4n, f_sed_n1p, f_sed_n5s
   use mod_sediment, only : f_sed_nn2, f_sed_o3c
   implicit none
!
! !IN-/OUTPUT PARAMETERS:
   integer, intent(inout)  :: ierr
!
! !LOCAL VARIABLES:
   integer       :: i, j, k0
!
!-----------------------------------------------------------------------
#include "call-trace.inc"

   !cdir nodep
   do i = iStartCalc, iEndCalc
      do j = jStartCalc, jEndCalc
         k0 = k_index(j,i)
         if (k0<=0) cycle

         ! map sediment fluxes to fluxes defined in the pelagic module (mmol m-2 d-1)
         seti_flux(j,i,i_sed_dic) = f_sed_dic(j,i)
         seti_flux(j,i,i_sed_n4n) = f_sed_n4n(j,i)
         seti_flux(j,i,i_sed_n1p) = f_sed_n1p(j,i)
         seti_flux(j,i,i_sed_n5s) = f_sed_n5s(j,i)
         seti_flux(j,i,i_sed_nn2) = f_sed_nn2(j,i)
         seti_flux(j,i,i_sed_o3c) = f_sed_o3c(j,i)

         f_from_to(j,k0,i,i_o2o_brm) = f_o2o_brm(j,i)
         f_from_to(j,k0,i,i_n3n_brm) = f_n3n_brm(j,i)

         ! source equations (mmol m-2 d-1) for pelagic variables
         sst(j,k0,i,idic) = sst(j,k0,i,idic) +f_sed_dic(j,i)    &
                                             +f_sed_o3c(j,i)
         sst(j,k0,i,in3n) = sst(j,k0,i,in3n) -f_n3n_brm(j,i)
         sst(j,k0,i,in4n) = sst(j,k0,i,in4n) +f_sed_n4n(j,i)
         sst(j,k0,i,in1p) = sst(j,k0,i,in1p) +f_sed_n1p(j,i)
         sst(j,k0,i,in5s) = sst(j,k0,i,in5s) +f_sed_n5s(j,i)
         sst(j,k0,i,io2o) = sst(j,k0,i,io2o) -f_o2o_brm(j,i)
         sst(j,k0,i,ialk) = sst(j,k0,i,ialk) +f_sed_o3c(j,i)*2. &
                                             +f_sed_n4n(j,i)    &
                                             -f_sed_n1p(j,i)
      enddo
   enddo

   if(ierr/=0) then
      write(error_message,'("ERROR - sed2pel ",i3)') ierr
      call stop_ecoham(ierr, msg=error_message)
   endif
   return

   end subroutine sed2pel
#endif

!=======================================================================
#ifdef module_biogeo
!
! !INTERFACE:
   subroutine pel2sed(ierr)
#define SUBROUTINE_NAME 'pel2sed'
!
! !DESCRIPTION:
! sedimentation (sinking) at the interface pelagic-sediment (mmol m-2 d-1)
!
! !USES:
   use mod_sediment, only : f_poc_sed, f_pon_sed, f_pop_sed
   use mod_sediment, only : f_pos_sed, f_pok_sed
   implicit none
!
! !IN-/OUTPUT PARAMETERS:
   integer, intent(inout)  :: ierr
!
! !LOCAL VARIABLES:
   integer :: i, j, k0
!
!-----------------------------------------------------------------------
#include "call-trace.inc"

   do i = iStartCalc, iEndCalc
      do j = jStartCalc, jEndCalc
         k0 = k_index(j,i)
         if (k0<=0) cycle

         f_poc_sed(j,i) =  seto_flux(j,i,ip1c) +seto_flux(j,i,ip2c) &
                          +seto_flux(j,i,ip3c) +seto_flux(j,i,id1c) &
                          +seto_flux(j,i,id2c)
         f_pon_sed(j,i) =  seto_flux(j,i,ip1n) +seto_flux(j,i,ip2n) &
                          +seto_flux(j,i,ip3n) +seto_flux(j,i,id1n) &
                          +seto_flux(j,i,id2n)
         f_pop_sed(j,i) =  seto_flux(j,i,ip1p) +seto_flux(j,i,ip2p) &
                          +seto_flux(j,i,ip3p) +seto_flux(j,i,id1p) &
                          +seto_flux(j,i,id2p)
         f_pos_sed(j,i) =  seto_flux(j,i,ip1s) +seto_flux(j,i,id2s)
         f_pok_sed(j,i) =  seto_flux(j,i,id2k) +seto_flux(j,i,ip3k)

      enddo
   enddo

   if(ierr/=0) then
      write(error_message,'("ERROR - pel2sed ",i3)') ierr
      call stop_ecoham(ierr, msg=error_message)
   endif
   return

   end subroutine pel2sed
# endif
!=======================================================================
#endif /*#ifdef module_sediment*/
   end module mod_coupler

!=======================================================================
#endif /*#ifndef sediment_EMR*/