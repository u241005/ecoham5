#ifdef sediment_EMR
!--------------------------------
!
! !MODULE: output_EMR ---  subroutines for ecoham-1D-output
!
! !INTERFACE:
   MODULE mod_output_EMR
!
! !DESCRIPTION:
!
! !USES:
   use mod_common
   use mod_sediment
   use mod_param_EMR
#ifdef MPI
   use mod_mpi_parallel
#endif
   implicit none
!
!  default: all is private.
   private
!
! !PUBLIC MEMBER FUNCTIONS:
   public init_output_EMR, do_output_EMR, close_output_EMR
!
! !LOCAL VARIABLES:
   integer, parameter :: nn_icol   = 30
   integer, parameter :: ibase_sed_prt = 560-1  !write units ..
   integer, parameter :: ibase_sed_out = 590-1  !write units for sediment info
!
!    real, dimension(:,:), allocatable :: poc_sey, pon_sey, pop_sey
!    real, dimension(:,:), allocatable :: sil_sey, cal_sey, ter_sey
!
   real :: pw_dic(ks,nn_icol),pw_oxy(ks,nn_icol),pw_sil(ks,nn_icol)
   real :: pw_no3(ks,nn_icol),pw_po4(ks,nn_icol),pw_nh4(ks,nn_icol)
   real :: solid_poc(ks,nn_icol),solid_cal(ks,nn_icol),solid_ter(ks,nn_icol)
   real :: solid_sil(ks,nn_icol),solid_pon(ks,nn_icol),solid_pop(ks,nn_icol)
   real :: pw_alk(ks,nn_icol),pw_n2(ks,nn_icol),pw_ph(ks,nn_icol),pw_phneu(ks)

   character(len=6) :: sed_name(nsedtra), pow_name(npowtra)
!
!=======================================================================

   contains

!=======================================================================
!
! !INTERFACE:
   subroutine init_output_EMR(ierr)
#define SUBROUTINE_NAME 'init_output_EMR'
!
! !DESCRIPTION:
!
! !USES:
   use mod_utils, only: DOYstring
   implicit none
!
! !OUTPUT PARAMETERS:
   integer, intent(inout)  :: ierr
!
! !LOCAL VARIABLES:
   character(len=19) :: str
   integer           :: iy, im, id, ih
   integer           :: iu, n, k
#ifdef MPI
   integer           :: ierr_mpi
#endif
!
!-----------------------------------------------------------------------
#include "call-trace.inc"

   if (n_pos_out>=1) then

      if (myID==master) call write_log_message(" INITIALIZING 1D EMR output ")

      write(str,'(a19)') DOYstring(day_of_year)
      read (str,'(i4,x,i2,x,i2,x,i2)') iy, im, id, ih


      ! names for the solid sediment variables
      sed_name(issso12) = 'sd_pop'
      sed_name(isssc12) = 'sd_cal'
      sed_name(issssil) = 'sd_opa'
      sed_name(issster) = 'sd_ter'
      sed_name(issspon) = 'sd_pon'
      sed_name(issspoc) = 'sd_poc'
#ifdef __c_isotopes
      sed_name(issso13) = 'sd_o13'
      sed_name(issso14) = 'sd_o14'
      sed_name(isssc13) = 'sd_c13'
      sed_name(isssc14) = 'sd_c14'
#endif

      ! names for the dissolved sediment variables
      pow_name(ipowaic) = 'sd_dic'
      pow_name(ipowaal) = 'sd_alk'
      pow_name(ipowaph) = 'sd_n1p'
      pow_name(ipowaox) = 'sd_o2o'
      pow_name(ipown2 ) = 'sd_nn2'
      pow_name(ipowno3) = 'sd_n3n'
      pow_name(ipowasi) = 'sd_n5s'
      pow_name(ipownh4) = 'sd_n4n'
#ifdef __c_isotopes
      sed_name(ipowc13) = 'sd_x13'
      sed_name(ipowc14) = 'sd_x14'
#endif

      !multiple station output
      do n = 1, n_pos_out

         ! each process opens the prt files of its computation area
         !if ( icol_out(n) >= latStartIndex .and. icol_out(n) <= latEndIndex ) then
         if ( icol_out(n) >= iStartCalc .and. icol_out(n) <= iEndCalc ) then

            ! Initialise .prt Stations
            iu = ibase_sed_prt+n
            if (max(imax,jmax)>99) then
               write(filename,'(a,"_EMR_ij_",i3.3,"_",i3.3,".prt")') trim(runID),icol_out(n),jcol_out(n)
            else
               write(filename,'(a,"_EMR_ij_",i2.2,"_",i2.2,".prt")') trim(runID),icol_out(n),jcol_out(n)
            endif
            write(long_msg,'(3x,"[",i2.2,"] unit ",i3," associated to file ",a)') myID, iu, filename
            call write_log_message(long_msg); long_msg = ''
            open(iu, file=filename, status='replace')
            write(iu,'(3i4," number of layers, ipos, jpos")') ks, icol_out(n), jcol_out(n)
!mk         write(iu,'(8f10.3)') (dzzs(k),k=1,ks), (porwat(jcol_out(n),k,icol_out(n)),k=1,ks)
            write(iu,'(24f10.3)') (dzzs(k),k=1,ks), (porwat(jcol_out(n),k,icol_out(n)),k=1,ks)


            ! Initialise extra Station output
            iu = ibase_sed_out+0*30+n
            write(filename,'(a,"_EMR_pr_p_",i2.2,"_",i2.2,".dat")') trim(runID),icol_out(n),jcol_out(n)
            open(iu,file=filename, status='replace')           ! pore water concentrations at (jout(),iout())(83)

            iu = ibase_sed_out+1*30+n
            write(filename,'(a,"_EMR_pr_s_",i2.2,"_",i2.2,".dat")') trim(runID),icol_out(n),jcol_out(n)
            open(iu,file=filename, status='replace')           ! solid concentrations(84)

            iu = ibase_sed_out+2*30+n
            write(filename,'(a,"_EMR_sed_flxy_",i2.2,"_",i2.2,".dat")') trim(runID),icol_out(n),jcol_out(n)
            open(iu,file=filename, status='replace')           ! annual effluxes(85)

            iu = ibase_sed_out+3*30+n
            write(filename,'(a,"_EMR_sed_flxd_",i2.2,"_",i2.2,".dat")') trim(runID),icol_out(n),jcol_out(n)
            open(iu,file=filename, status='replace')           ! daily eddluxes(90)

            iu = ibase_sed_out+4*30+n
            write(filename,'(a,"_EMR_in_flxd_",i2.2,"_",i2.2,".dat")') trim(runID),icol_out(n),jcol_out(n)
            open(iu,file=filename, status='replace')           ! daily influxes(86)

            iu = ibase_sed_out+5*30+n
            write(filename,'(a,"_EMR_in_flxy_",i2.2,"_",i2.2,".dat")') trim(runID),icol_out(n),jcol_out(n)
            open(iu,file=filename, status='replace')           ! annual influxes(87)

            iu = ibase_sed_out+6*30+n
            write(filename,'(a,"_EMR_cp_ratio_",i2.2,"_",i2.2,".dat")') trim(runID),icol_out(n),jcol_out(n)
            open(iu,file=filename, status='replace')           ! stoichiometric ratios in surface sediment layer at end of day(88)

            iu = ibase_sed_out+7*30+n
            write(filename,'(a,"_EMR_prp2_",i2.2,"_",i2.2,".dat")') trim(runID),icol_out(n),jcol_out(n)
            open(iu,file=filename, status='replace')           ! pore water concentrations (89)

         endif
      enddo
#ifdef MPI
      ! wait for all procs to finish log_messages (doesn't cost much, because it's only once during init!)
      call MPI_ALLREDUCE(ierr, ierr_all, 1, MPI_INT, MPI_SUM, MPI_COMM_ECOHAM, ierr_mpi)
#endif
   endif

   if (ierr/=0) then
      write( error_message, '("[",i2.2,"] ERROR - init_output_EMR: ",i3)') myID, ierr
      call stop_ecoham(ierr, msg=error_message);
   end if
   return

   end subroutine init_output_EMR

!=======================================================================
!
! !INTERFACE:
#ifdef FALSE
  subroutine update_output_EMR(ierr)
#define SUBROUTINE_NAME 'update_output_EMR'
!
! !DESCRIPTION:
!
! !USES:
   use mod_utils, only: DOYstring
   implicit none
!
! !OUTPUT PARAMETERS:
   integer, intent(inout)  :: ierr
!
! !LOCAL VARIABLES:
   character(len=19) :: str
   integer           :: iy, im, id, ih
   integer           :: n !iu, n, k
#ifdef MPI
   integer           :: ierr_mpi
#endif
!
!-----------------------------------------------------------------------
#include "call-trace.inc"

   if (n_pos_out>=1) then

      if (myID==master) call write_log_message(" UPDATE EMR output variables ")

      write(str,'(a19)') DOYstring(day_of_year)
      read (str,'(i4,x,i2,x,i2,x,i2)') iy, im, id, ih

      !multiple station output
      do n = 1, n_pos_out

         ! each process opens the prt files of its computation area
         !if ( icol_out(n) >= latStartIndex .and. icol_out(n) <= latEndIndex ) then
         if ( icol_out(n) >= iStartCalc .and. icol_out(n) <= iEndCalc ) then


         endif
      enddo
#ifdef MPI
      ! wait for all procs to finish log_messages (doesn't cost much, because it's only once during init!)
      call MPI_ALLREDUCE(ierr, ierr_all, 1, MPI_INT, MPI_SUM, MPI_COMM_ECOHAM, ierr_mpi)
#endif
   endif

   if (ierr/=0) then
      write( error_message, '("[",i2.2,"] ERROR - update_output_EMR: ",i3)') myID, ierr
      call stop_ecoham(ierr, msg=error_message);
   end if
   return

   end subroutine update_output_EMR
#endif

!=======================================================================
!
! !INTERFACE:
#ifdef FALSE
  subroutine cum_fluxes_EMR(dt_,ierr)
#define SUBROUTINE_NAME 'cum_fluxes_EMR'
!
! !DESCRIPTION:
! cumulate EMR sediment fluxes to daily values
!
! !USES:
   implicit none
!
! !INPUT PARAMETERS:
   real, intent(in)        :: dt_
   integer, intent(inout)  :: ierr
!
! !LOCAL VARIABLES:
   real                    :: dummy
   !integer               :: i, j, k, k0
!
!-----------------------------------------------------------------------
#include "call-trace.inc"

   dummy = dt_ ! to get rid of warning: Unused dummy argument

   if (ierr/=0) then
      write( error_message, '("[",i2.2,"] ERROR - cum_fluxes_EMR: ",i3)') myID,ierr
      call stop_ecoham(ierr, msg=error_message)
   end if
   return

   end subroutine cum_fluxes_EMR

!=======================================================================
!
! !INTERFACE:
   subroutine empty_fluxes_EMR(dt_,ierr)
#define SUBROUTINE_NAME 'empty_fluxes_EMR'
!
! !DESCRIPTION:
! reset cumulated EMR sediment fluxes
!
! !USES:
   implicit none
!
! !INPUT PARAMETERS:
   real, intent(in)        :: dt_
   integer, intent(inout)  :: ierr
!
! !LOCAL VARIABLES:
   real                    :: dummy
!   integer               :: i, j, k, k0
!
!-----------------------------------------------------------------------
#include "call-trace.inc"

   dummy = dt_ ! to get rid of warning: Unused dummy argument

   if (ierr/=0) then
      write( error_message, '("[",i2.2,"] ERROR - empty_fluxes_EMR: ",i3)') myID,ierr
      call stop_ecoham(ierr, msg=error_message)
   end if
   return

   end subroutine empty_fluxes_EMR
#endif
!=======================================================================
! !INTERFACE:
   subroutine do_output_EMR(ierr)
#define SUBROUTINE_NAME 'do_output_EMR'
!
! !DESCRIPTION:
!
! !USES:
   use mod_utils, only: DOYstring
   implicit none

   integer, intent(inout) :: ierr
!
! !LOCAL VARIABLES:
   character(len=19) :: str
   integer           :: iu, n, ida, ihour, imo
!
!-----------------------------------------------------------------------
#include "call-trace.inc"

   write(str,'(a19)') DOYstring(day_of_year)
   read (str,'(i4,x,i2,x,i2,x,i2)') iy1, imo, ida, ihour

   do n = 1, n_pos_out
      !if ( icol_out(n) >= latStartIndex .and. icol_out(n) <= latEndIndex ) then
      if ( icol_out(n) >= iStartCalc .and. icol_out(n) <= iEndCalc ) then

         ! Write .prt Stations
         iu = ibase_sed_prt+n
         write(iu,'("time: ",4i6,2x,f15.10)') iy1, imo, ida, ihour, dzzs(1)
         !call write_prt_EMR( iu, jcol_out(n), icol_out(n), ks, ierr )
!print*,'#336  write_sed_prt', iu, jcol_out(n), icol_out(n), ks, ierr
        call write_sed_prt( iu, jcol_out(n), icol_out(n), ks, ierr )
!print*,'#338 write_sed_prt -- done --'

         ! Write extra Station outputs
         iu = ibase_sed_out+4*30+n
         write(iu,991) int(dth), pop_sed(jcol_out(n),icol_out(n)),  &
                                 poc_sed(jcol_out(n),icol_out(n)),  &
                                 pon_sed(jcol_out(n),icol_out(n)),  &
                                 cal_sed(jcol_out(n),icol_out(n)),  &
                                 sil_sed(jcol_out(n),icol_out(n)),  &
                                 ter_sed(jcol_out(n),icol_out(n))
         iu = ibase_sed_out+5*30+n
         write(iu,991) int(dth), pop_sey(jcol_out(n),icol_out(n)),  &
                                 poc_sey(jcol_out(n),icol_out(n)),  &
                                 pon_sey(jcol_out(n),icol_out(n)),  &
                                 cal_sey(jcol_out(n),icol_out(n)),  &
                                 sil_sey(jcol_out(n),icol_out(n)),  &
                                 ter_sey(jcol_out(n),icol_out(n))
         iu = ibase_sed_out+3*30+n
         write(iu,990) int(dth), sedfluxd(jcol_out(n),icol_out(n),ipowaic),  &
                                 sedfluxd(jcol_out(n),icol_out(n),ipowaal),  &
                                 sedfluxd(jcol_out(n),icol_out(n),ipowaph),  &
                                 sedfluxd(jcol_out(n),icol_out(n),ipowaox),  &
                                 sedfluxd(jcol_out(n),icol_out(n),ipowno3),  &
                                 sedfluxd(jcol_out(n),icol_out(n),ipowasi),  &
                                 sedfluxd(jcol_out(n),icol_out(n),ipown2 ),  &
                                 sedfluxd(jcol_out(n),icol_out(n),ipownh4)
         iu = ibase_sed_out+2*30+n
         write(iu,990) int(dth), sedfluxy(jcol_out(n),icol_out(n),ipowaic),  &
                                 sedfluxy(jcol_out(n),icol_out(n),ipowaal),  &
                                 sedfluxy(jcol_out(n),icol_out(n),ipowaph),  &
                                 sedfluxy(jcol_out(n),icol_out(n),ipowaox),  &
                                 sedfluxy(jcol_out(n),icol_out(n),ipowno3),  &
                                 sedfluxy(jcol_out(n),icol_out(n),ipowasi),  &
                                 sedfluxy(jcol_out(n),icol_out(n),ipown2 ),  &
                                 sedfluxy(jcol_out(n),icol_out(n),ipownh4)
         iu = ibase_sed_out+6*30+n
         write(iu,990) int(dth), rcar  (jcol_out(n),1,icol_out(n)),  &
                                 r0    (jcol_out(n),1,icol_out(n)),  &
                                 ro2ut (jcol_out(n),1,icol_out(n)),  &
                                 nitdem(jcol_out(n),1,icol_out(n)),  &
                                 n2prod(jcol_out(n),1,icol_out(n)),  &
                                 rnit  (jcol_out(n),1,icol_out(n))

      endif
   enddo

   if (ierr/=0) then
      write( error_message, '("[",i2.2,"] ERROR - do_output_1D: ",i3)') myID, ierr
      call stop_ecoham(ierr, msg=error_message);
   end if

990 format(i5,8e14.6)
991 format(i5,6e14.6)

   return

   end subroutine do_output_EMR
!
!=======================================================================
! !INTERFACE:
   subroutine write_prt_EMR(FID,jcol,icol,kcol,ierr)
#define SUBROUTINE_NAME 'write_prt_EMR'
!
! !DESCRIPTION:
!
! !USES:
  use mod_var
  implicit none
!
! !INPUT PARAMETERS:
   integer, intent(in)    :: FID, icol, jcol, kcol
   integer, intent(inout) :: ierr
!
! !LOCAL VARIABLES:
   integer             :: k, n, n1, n2
   real, dimension(10) :: dum
   character(len=30)   :: dum_nam(10)
!
!-----------------------------------------------------------------------
#include "call-trace.inc"

   dum = 0 ! to get rid of unused warning

   if (n_sed_var>0) then
      write(FID,'("SEDIMENT STATE VARIABLES")')
      n1 = 1
      n2 = 0
      do while (n2.lt.nsedtra)
         n2 = min(n1+7,nsedtra)
         do n = 1, n2-n1+1; dum_nam(n) = trim(sd_name(n1+n-1));  enddo
         write(FID,'(" k",5x,8(a6,12x))') (dum_nam(n),n=1,(n2-n1+1))
         do k = 1, kcol
            write(FID,'(i3,8(e18.10))') k, (sedlay(jcol,k,icol,n)*porsol(jcol,k,icol),n=n1,n2)
         enddo
         n1 = n2 +1
      enddo

      n1 = 1
      n2 = 0
      do while (n2.lt.npowtra)
         n2 = min(n1+7,npowtra)
         do n = 1, n2-n1+1; dum_nam(n) = trim(sd_name(n1+n-1));  enddo
         write(FID,'(" k",5x,8(a6,12x))') (dum_nam(n),n=1,(n2-n1+1))
         do k = 1, kcol
            write(FID,'(i3,8(e18.10))') k, (powtra(jcol,k,icol,n)*porsol(jcol,k,icol),n=n1,n2)
         enddo
         n1 = n2 +1
      enddo
   endif


!---------- TIME INTEGRATED FLUXES (DAILY) --------------
   write(FID,'("FLUXES FROM PELAGIAL TO SEDIMENT")')
!--------------- FLUXES PEL to SED -------------------
!   write(FID,'("------ PEL to SED ------")')
   n1 = 1
   n2 = n1  ; dum_nam(n1) = 'poc_sed'
   n2 = n2+1; dum_nam(n2) = 'pon_sed'
   n2 = n2+1; dum_nam(n2) = 'pop_sed'
   n2 = n2+1; dum_nam(n2) = 'cal_sed'
   n2 = n2+1; dum_nam(n2) = 'opa_sed'
   n2 = n2+1; dum_nam(n2) = 'ter_sed'
   write(FID,'(" k",5x,8(a7,11x))') (dum_nam(n),n=1,(n2-n1+1))
   do k = 1, kcol
      if (k.gt.1) then
         dum(:) = 0.0
      else
         dum(1) = poc_sed(jcol,icol) / dzzs(1)
         dum(2) = pon_sed(jcol,icol) / dzzs(1)
         dum(3) = pop_sed(jcol,icol) / dzzs(1)
         dum(4) = cal_sed(jcol,icol) / dzzs(1)
         dum(5) = sil_sed(jcol,icol) / dzzs(1)
         dum(6) = ter_sed(jcol,icol) / dzzs(1)
      endif
      write(FID,'(i3,8(e18.10))') k,(dum(n),n=n1,n2)
   enddo

   write(FID,'("FLUXES FROM SEDIMENT TO PELAGIAL")')
!--------------- FLUXES SED to PEL -------------------
!   write(FID,'("------ SED to PEL ------")')
   n1 = 1
   n2 = n1  ; dum_nam(n2) = 'dic_pel'
   n2 = n2+1; dum_nam(n2) = 'n3n_pel'
   n2 = n2+1; dum_nam(n2) = 'n1p_pel'
   n2 = n2+1; dum_nam(n2) = 'n5s_pel'
   n2 = n2+1; dum_nam(n2) = 'alk_pel'
   n2 = n2+1; dum_nam(n2) = 'o2o_pel'
   n2 = n2+1; dum_nam(n2) = 'nn2_pel'
   n2 = n2+1; dum_nam(n2) = 'n4n_pel'
   write(FID,'(" k",5x,8(a7,11x))') (dum_nam(n),n=1,(n2-n1+1))
   do k = 1, kcol
      if (k.gt.1) then
         dum(:) = 0.0
      else
         dum(1) = sedfluxo(jcol,icol,ipowaic) / dzzs(1)                 !mmol C m-2 d-1
         dum(2) = sedfluxo(jcol,icol,ipowno3) / dzzs(1)
         dum(3) = sedfluxo(jcol,icol,ipowaph) / dzzs(1)
         dum(4) = sedfluxo(jcol,icol,ipowasi) / dzzs(1)
         dum(5) = sedfluxo(jcol,icol,ipowaal) / dzzs(1)
         dum(6) = sedfluxo(jcol,icol,ipowaox) / dzzs(1)
         dum(7) = sedfluxo(jcol,icol,ipown2)  / dzzs(1)
         dum(8) = sedfluxo(jcol,icol,ipownh4) / dzzs(1)
      endif
      write(FID,'(i3,8(e18.10))') k,(dum(n),n=n1,n2)
   enddo

   write(FID,'("FLUXES WITHIN SEDIMENT")')
!-------------------- ADV --------------------------------
! write(FID,'("------ ADV ------")')
   n1 = 1
   n2 = n1  ; dum_nam(n2) = 'adv_poc'
   n2 = n2+1; dum_nam(n2) = 'adv_pon'
   n2 = n2+1; dum_nam(n2) = 'adv_pop'
   n2 = n2+1; dum_nam(n2) = 'adv_cal'
   n2 = n2+1; dum_nam(n2) = 'adv_opa'
   n2 = n2+1; dum_nam(n2) = 'adv_ter'
   write(FID,'(" k",5x,8(a7,11x))') (dum_nam(n),n=1,(n2-n1+1))
   do k = 1, kcol
      dum(1) = adv_sol(jcol,k,icol,6)*porsol(jcol,k,icol)   !   POC
      dum(2) = adv_sol(jcol,k,icol,5)*porsol(jcol,k,icol)   !   PON
      dum(3) = adv_sol(jcol,k,icol,1)*porsol(jcol,k,icol)   !   POP
      dum(4) = adv_sol(jcol,k,icol,2)*porsol(jcol,k,icol)   !   CAL
      dum(5) = adv_sol(jcol,k,icol,3)*porsol(jcol,k,icol)   !   OPA
      dum(6) = adv_sol(jcol,k,icol,4)*porsol(jcol,k,icol)   !   TER
      write(FID,'(i3,8(e18.10))') k,(dum(n),n=n1,n2)
   enddo

!-------------------- sol_diss --------------------------------
! write(FID,'("------ sol_diss ------")')
   n1 = 1
   n2 = n1  ; dum_nam(1) = 'poc_dic'
   n2 = n2+1; dum_nam(2) = 'rem_alk'
   n2 = n2+1; dum_nam(3) = 'pop_n1p'
   n2 = n2+1; dum_nam(4) = 'rem_o2o'
   n2 = n2+1; dum_nam(5) = 'pon_nn2'
   n2 = n2+1; dum_nam(6) = 'pon_n3n'
   n2 = n2+1; dum_nam(7) = 'opa_n5s'
   n2 = n2+1; dum_nam(8) = 'pon_n4n'
   write(FID,'(" k",5x,8(a7,11x))') (dum_nam(n),n=1,(n2-n1+1))
   do k = 1, kcol
      dum(1) = sol_diss(jcol,k,icol,1)                      !   POC -> DIC
      dum(2) = sol_diss(jcol,k,icol,2)                      !   ALK
      dum(3) = sol_diss(jcol,k,icol,3)                      !   POP -> N1P
      dum(4) = sol_diss(jcol,k,icol,4)                      !   O2O
      dum(5) = sol_diss(jcol,k,icol,5)                      !   PON -> N2N
      dum(6) = sol_diss(jcol,k,icol,6)                      !   PON -> N3N
      dum(7) = sol_diss(jcol,k,icol,7)                      !   OPA -> N5S
      dum(8) = sol_diss(jcol,k,icol,8)                      !   PON -> N4N
      write(FID,'(i3,8(e18.10))') k,(dum(n),n=n1,n2)
   enddo

   n1 = 1
   n2 = n1  ; dum_nam(1) = 'n3n_nn2'
   n2 = n2+1; dum_nam(2) = 'n4n_n3n'
   n2 = n2+1; dum_nam(3) = 'cal_dic'
   write(FID,'(" k",5x,8(a7,11x))') (dum_nam(n),n=1,(n2-n1+1))
   do k = 1, kcol
      dum(1) = sol_diss(jcol,k,icol, 9)                     !   N3N -> N2N
      dum(2) = sol_diss(jcol,k,icol,10)                     !   N4N -> N3N
      dum(3) = sol_diss(jcol,k,icol,11)                     !   CAL -> DIC
      write(FID,'(i3,8(e18.10))') k,(dum(n),n=n1,n2)
   enddo

!-------------------- MIX --------------------------------
! write(FID,'("------ MIX ------")')
   n1 = 1
   n2 = n1  ; dum_nam(n2) = 'mix_dic'
   n2 = n2+1; dum_nam(n2) = 'mix_n3n'
   n2 = n2+1; dum_nam(n2) = 'mix_n1p'
   n2 = n2+1; dum_nam(n2) = 'mix_n5s'
   n2 = n2+1; dum_nam(n2) = 'mix_alk'
   n2 = n2+1; dum_nam(n2) = 'mix_o2o'
   n2 = n2+1; dum_nam(n2) = 'mix_nn2'
   n2 = n2+1; dum_nam(n2) = 'mix_n4n'
   write(FID,'(" k",5x,8(a7,11x))') (dum_nam(n),n=1,(n2-n1+1))
   do k = 1, kcol
      dum(1) = mix_diss(jcol,k,icol,1)*porwat(jcol,k,icol)  !   DIC
      dum(2) = mix_diss(jcol,k,icol,6)*porwat(jcol,k,icol)  !   N3N
      dum(3) = mix_diss(jcol,k,icol,3)*porwat(jcol,k,icol)  !   N1P
      dum(4) = mix_diss(jcol,k,icol,7)*porwat(jcol,k,icol)  !   N5S
      dum(5) = mix_diss(jcol,k,icol,2)*porwat(jcol,k,icol)  !   ALK
      dum(6) = mix_diss(jcol,k,icol,4)*porwat(jcol,k,icol)  !   O2O
      dum(7) = mix_diss(jcol,k,icol,5)*porwat(jcol,k,icol)  !   N2N
      dum(8) = mix_diss(jcol,k,icol,8)*porwat(jcol,k,icol)  !   N4N
      write(FID,'(i3,8(e18.10))') k,(dum(n),n=n1,n2)
   enddo

   if (ierr/=0) then
      write( error_message, '("[",i2.2,"] ERROR - write_prt_EMR: ",i3)') myID, ierr
      call stop_ecoham(ierr, msg=error_message);
   end if
   return

   end subroutine write_prt_EMR

!=======================================================================
!
! !INTERFACE:
   subroutine write_output_EMR_annual(ierr)
#define SUBROUTINE_NAME 'write_output_EMR_annual'
!
! !DESCRIPTION:
!
! !USES:
    implicit none
!
! !OUTPUT PARAMETERS:
   integer, intent(inout) :: ierr
!
! !LOCAL VARIABLES:
   integer       :: iu, n, k
!
!-----------------------------------------------------------------------
#include "call-trace.inc"

   print*,'write_output_EMR_annual#586 - dth:',dth ! ilast:',dth, ilast

   if(dth.eq.365.) then
      do n = 1, n_pos_out
         do k = 1, ks
            pw_phneu(k) = -log10(sedhpl(jcol_out(k),k,icol_out(k)))
            iu=ibase_sed_out+0*30+k
            write(iu,995) z_sed(k),powtra(jcol_out(k),k,icol_out(k),ipowaic),      &
                                    powtra(jcol_out(k),k,icol_out(k),ipowaox),     &
                                    powtra(jcol_out(k),k,icol_out(k),ipowasi),     &
                                    powtra(jcol_out(k),k,icol_out(k),ipowno3),     &
                                    powtra(jcol_out(k),k,icol_out(k),ipowaph),     &
                                    pw_dic(k,k),pw_oxy(k,k),pw_sil(k,k),           &
                                    pw_no3(k,k),pw_po4(k,k)
            iu=ibase_sed_out+7*30+k
            write(iu,995) z_sed(k),powtra(jcol_out(k),k,icol_out(k),ipowaal),      &
                                   powtra(jcol_out(k),k,icol_out(k),ipown2),       &
                                   pw_alk(k,k),pw_n2(k,k),pw_phneu(k),pw_ph(k,k),  &
                                   powtra(jcol_out(k),icol_out(k),k,ipownh4),pw_nh4(k,k)
            iu=ibase_sed_out+1*30+k
            write(iu,995) z_sed(k),sedlay(jcol_out(k),k,icol_out(k),issspoc), &
                                   sedlay(jcol_out(k),k,icol_out(k),isssc12), &
                                   sedlay(jcol_out(k),k,icol_out(k),issssil), &
                                   sedlay(jcol_out(k),k,icol_out(k),issster), &
                                   solid_poc(k,k),solid_cal(k,k),             &
                                   solid_sil(k,k),solid_ter(k,k)
         enddo
      enddo
   endif

995 format(11e14.6)

   if (ierr/=0) then
      write( error_message, '("[",i2.2,"] ERROR - write_prt_EMR: ",i3)') myID, ierr
      call stop_ecoham(ierr, msg=error_message);
   end if

   return

   end subroutine write_output_EMR_annual

!=======================================================================
! !INTERFACE:
   subroutine close_output_EMR(ierr)
#define SUBROUTINE_NAME 'close_output_EMR'
!
! !DESCRIPTION:
!
! !USES:
   implicit none
!
! !INPUT PARAMETERS:
   integer, intent(inout) :: ierr
!
! !LOCAL VARIABLES:
   integer :: iu, n
   logical :: l_open
!
!-----------------------------------------------------------------------
#include "call-trace.inc"

   call write_output_EMR_annual(ierr)

   do n = 1,n_pos_out
      ! if ( icol_out(n) >= iStartCalc .and. icol_out(n) <= iEndCalc ) then

      inquire (ibase_sed_prt+n, opened = l_open)
      if (l_open) close(ibase_sed_prt+n)

      iu = ibase_sed_out+0*30+n
      inquire (iu, opened = l_open)
      if (l_open) close(iu)

      iu = ibase_sed_out+1*30+n
      inquire (iu, opened = l_open)
      if (l_open) close(iu)

      iu = ibase_sed_out+2*30+n
      inquire (iu, opened = l_open)
      if (l_open) close(iu)

      iu = ibase_sed_out+3*30+n
      inquire (iu, opened = l_open)
      if (l_open) close(iu)

      iu = ibase_sed_out+4*30+n
      inquire (iu, opened = l_open)
      if (l_open) close(iu)

      iu = ibase_sed_out+5*30+n
      inquire (iu, opened = l_open)
      if (l_open) close(iu)

      iu = ibase_sed_out+6*30+n
      inquire (iu, opened = l_open)
      if (l_open) close(iu)

      iu = ibase_sed_out+7*30+n
      inquire (iu, opened = l_open)
      if (l_open) close(iu)

      ! endif
   enddo

   close(3)
   close(4)
   close(9)
   close(11)
   close(200)

   if (ierr/=0) then
      write( error_message, '("[",i2.2,"] ERROR - close_output_EMR: ",i3)') myID, ierr
      call stop_ecoham(ierr, msg=error_message);
   end if
   return

   end subroutine close_output_EMR
!
!=======================================================================
! !INTERFACE:
   subroutine write_sed_prt(FID,jcol,icol,kcol,ierr)
! subroutine write_sed_prt(FID,jcol,icol,dic_pel_,alk_pel_,n3n_pel_, &
!                           n4n_pel_,n1p_pel_,o2o_pel_,n5s_pel_,nn2_pel_)
!
! !USES:
   implicit none
!
! !INPUT PARAMETERS:
   integer, intent(in)    :: FID, icol, jcol, kcol
   integer, intent(inout) :: ierr
!
! !LOCAL VARIABLES:
    real          :: dum(11)
    integer       :: k,n,n1,n2

!    logical        :: ex, op
!    character (99) :: xn
!    character (15) :: di, fo, ac, se
!    integer        :: nu, st, ios

!
!-----------------------------------------------------------------------

! print*,'eco_output_EMR#760 (FID,jcol,icol,kcol)', FID, jcol, icol, kcol
! inquire (unit=FID, name=xn, opened=op, exist = ex, direct = di, sequential = se,  &
!          formatted = fo, access = ac, number = nu, iostat=st)
!
!    if(st == 0) then
!      write (*,*) 'NAME? ', trim(xn)
!      write (*,*) 'OPEN? ', op
!      write (*,*) 'EXIST? ', ex
!      write (*,*) 'DIRECT? ', di
!      write (*,*) 'SEQUENTIAL? ', se
!      write (*,*) 'FORMATTED? ', fo
!      write (*,*) 'ACCESS? ', ac
!      write (*,*) 'NUMBER? ', nu
!    else
!      write (*,*) "inquire-Fehler!"
!    end if

    write(FID,'(''SEDIMENT VARIABLES'')')
    n1 =1
    n2 =nsedtra
    write(FID,'('' k'',5x,6(a6,12x))') (sed_name(n)(1:6),n=n1,n2)
    do k=1,kcol
      write(FID,'(i3,7(e18.10))') k,(sedlay(jcol,k,icol,n),n=n1,n2)!,porsol(jcol,k,icol)
      !write(FID,'(i3,6(e18.10))') k,(sedlay(jcol,k,icol,n)*porsol(jcol,k,icol),n=n1,n2)
    enddo
    n1 =1
    n2 =npowtra-3
    write(FID,'('' k'',5x,8(a6,12x))') (pow_name(n)(1:6),n=n1,n2)
    do k=1,kcol
      write(FID,'(i3,9(e18.10))') k,(powtra(jcol,k,icol,n),n=n1,n2)!,porwat(jcol,k,icol)
      !write(FID,'(i3,8(e18.10))') k,(powtra(jcol,k,icol,n)*porwat(jcol,k,icol),n=n1,n2)
    enddo
    write(FID,'(''FLUXES per day'')')
    write(FID,'('' k       poc_sed       pon_sed       pop_sed'', &
&           ''       cal_sed       opa_sed       ter_sed'')')
    do k=1,kcol
      if(k.eq.1) then
        dum(1)=poc_sed(jcol,icol)!/dzzs(1)
        dum(2)=pon_sed(jcol,icol)!/dzzs(1)
        dum(3)=pop_sed(jcol,icol)!/dzzs(1)
        dum(4)=cal_sed(jcol,icol)!/dzzs(1)
        dum(5)=sil_sed(jcol,icol)!/dzzs(1)
        dum(6)=ter_sed(jcol,icol)!/dzzs(1)
      else
        dum(1)=0.0
        dum(2)=0.0
        dum(3)=0.0
        dum(4)=0.0
        dum(5)=0.0
        dum(6)=0.0
      endif
      write(FID,'(i3,6(e14.6))')k,(dum(n),n=1,6)
    enddo
    write(FID,'('' k      dic_pel       n3n_pel       n1p_pel       n5s_pel'', &
&           ''       alk_pel       o2o_pel       nn2_pel       n4n_pel'')')
    do k=1,kcol
      if(k.eq.1) then
         dum(1) = sedfluxo(jcol,icol,ipowaic) !/ dzzs(1)                 !mmol C m-2 d-1
         dum(2) = sedfluxo(jcol,icol,ipowno3) !/ dzzs(1)
         dum(3) = sedfluxo(jcol,icol,ipowaph) !/ dzzs(1)
         dum(4) = sedfluxo(jcol,icol,ipowasi) !/ dzzs(1)
         dum(5) = sedfluxo(jcol,icol,ipowaal) !/ dzzs(1)
         dum(6) = sedfluxo(jcol,icol,ipowaox) !/ dzzs(1)
         dum(7) = sedfluxo(jcol,icol,ipown2)  !/ dzzs(1)
         dum(8) = sedfluxo(jcol,icol,ipownh4) !/ dzzs(1)
      else
        dum(1)=0.0
        dum(2)=0.0
        dum(3)=0.0
        dum(4)=0.0
        dum(5)=0.0
        dum(6)=0.0
        dum(7)=0.0
        dum(8)=0.0
      endif
      write(FID,'(i3,8(e14.6))')k,(dum(n),n=1,8)
    enddo
    write(FID,'('' k      adv_poc       adv_pon       adv_pop'', &
&           ''       adv_cal       adv_opa       adv_ter'')')
    do k=1,kcol
      dum(1)=adv_sol(jcol,k,icol,6)!*porsol(jcol,k,icol)          !   POC
      dum(2)=adv_sol(jcol,k,icol,5)!*porsol(jcol,k,icol)          !   PON
      dum(3)=adv_sol(jcol,k,icol,1)!*porsol(jcol,k,icol)          !   POP
      dum(4)=adv_sol(jcol,k,icol,2)!*porsol(jcol,k,icol)          !   CAL
      dum(5)=adv_sol(jcol,k,icol,3)!*porsol(jcol,k,icol)          !   OPA
      dum(6)=adv_sol(jcol,k,icol,4)!*porsol(jcol,k,icol)          !   TER
      write(FID,'(i3,6(e14.6))')k,(dum(n),n=1,6)
    enddo
    write(FID,'('' k      poc_dic       pon_n3n       pop_n1p       opa_n5s'', &
&           ''       rem_alk       rem_o2o       n3n_nn2       pon_n4n'', &
&           ''       n4n_n3n     cal_dic      pon_nn2'')')
    do k=1,kcol
      dum(1)=sol_diss(jcol,k,icol,1)          !   POC -> DIC
      dum(2)=sol_diss(jcol,k,icol,6)          !   PON -> N3N
      dum(3)=sol_diss(jcol,k,icol,3)          !   POP -> N1P
      dum(4)=sol_diss(jcol,k,icol,7)          !   OPA -> N5S
      dum(5)=sol_diss(jcol,k,icol,2)          !   ALK
      dum(6)=sol_diss(jcol,k,icol,4)          !   O2O
      dum(7)=sol_diss(jcol,k,icol,9)          !   N3N -> N2N
      dum(8)=sol_diss(jcol,k,icol,8)          !   PON -> N4N
      dum(9)=sol_diss(jcol,k,icol,10)         !   N4N -> N3N
      dum(10)=sol_diss(jcol,k,icol,11)        !   CAL -> DIC
      dum(11)=sol_diss(jcol,k,icol,5)         !   PON -> N2N
      write(FID,'(i3,11(e14.6))')k,(dum(n),n=1,11)
    enddo
    write(FID,'('' k      mix_dic       mix_n3n       mix_n1p       mix_n5s'', &
&                ''       mix_alk       mix_o2o       mix_nn2       mix_n4n'')')
    do k=1,kcol
      dum(1)=mix_diss(jcol,k,icol,1)!*porwat(jcol,k,icol)          !   DIC
      dum(2)=mix_diss(jcol,k,icol,6)!*porwat(jcol,k,icol)          !   N3N
      dum(3)=mix_diss(jcol,k,icol,3)!*porwat(jcol,k,icol)          !   N1P
      dum(4)=mix_diss(jcol,k,icol,7)!*porwat(jcol,k,icol)          !   N5S
      dum(5)=mix_diss(jcol,k,icol,2)!*porwat(jcol,k,icol)          !   ALK
      dum(6)=mix_diss(jcol,k,icol,4)!*porwat(jcol,k,icol)          !   O2O
      dum(7)=mix_diss(jcol,k,icol,5)!*porwat(jcol,k,icol)          !   N2N
      dum(8)=mix_diss(jcol,k,icol,8)!*porwat(jcol,k,icol)          !   N4N
      write(FID,'(i3,8(e14.6))')k,(dum(n),n=1,8)
    enddo
    poc_sed=0.0
    pop_sed=0.0
    pon_sed=0.0
    cal_sed=0.0
    sil_sed=0.0
    ter_sed=0.0

   if (ierr/=0) then
      write( error_message, '("[",i2.2,"] ERROR - write_sed_prt: ",i3)') myID, ierr
      call stop_ecoham(ierr, msg=error_message);
   end if

   return
   end subroutine write_sed_prt
!
!=======================================================================

   end module mod_output_EMR

!=======================================================================
#endif   /* #ifdef sediment_EMR */
