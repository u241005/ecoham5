!
! !MODULE: eco_main.f90 ---  wrapping ecoham into "init", "do" and "end"
!
! !INTERFACE:
   MODULE mod_main
!
! !DESCRIPTION:
! comprehensive subroutine wrapper with variable (recursive) timestep
!
! !USES:
   use mod_common
#ifdef MPI
   use mod_mpi_parallel
#endif
!    use mod_var
   implicit none
!
!  default: all is private.
   private
! !PUBLIC MEMBER FUNCTIONS:
   public init_main, do_main, end_main
!
! !LOCAL VARIABLES:
   integer :: level
   !integer :: ii=3,jj=3,kk=1
   integer :: ii=28, jj=23 ! Rhine @ NS20C
!
!=======================================================================

   contains

!=======================================================================
!
! !INTERFACE:
   subroutine init_main(ierr)
#define SUBROUTINE_NAME 'init_main'
!
! !DESCRIPTION:
!
! !USES:
!   use mod_mpi_parallel
#if defined module_biogeo || defined module_chemie
   use mod_grid,       only: k_index
#endif
!    use mod_var,        only: init_vars
!    use mod_flux,       only: init_fluxes
   use mod_output,     only: warm_in, init_output, do_output
#ifdef module_sediment
   use mod_sediment,   only: init_sediment
#endif
#ifdef module_chemie
   use mod_chemie,     only: init_chemie, update_chemie, do_chemie
   use mod_var,        only: st, ialk, idic
#endif
#ifdef module_biogeo
   use mod_biogeo,     only: init_biogeo, update_biogeo
#endif
#if defined module_biogeo || defined module_chemie
   use mod_utils,      only: calcite
   use mod_hydro,      only: temp, salt, rho, pres
#endif
   implicit none
!
! !INPUT PARAMETERS:
!   real,    intent(in)     :: td_
!
! !OUTPUT PARAMETERS:
   integer, intent(inout)  :: ierr
!
! !LOCAL VARIABLES:
#if defined module_biogeo || defined module_chemie
   integer                 :: i, j, k, k0
#endif
!
!-----------------------------------------------------------------------
#include "call-trace.inc"

   ! initialize modules
#ifdef module_sediment
   call init_sediment(ierr)
#endif

#ifdef module_biogeo
   call init_biogeo(ierr)
#endif

#ifdef module_chemie
   call init_chemie(ierr)
#endif

#if defined module_biogeo || defined module_chemie
   ! initialize calcite
   do i = iStartD, iEndD
      do j = 1, jmax
         k0 = k_index(j,i)
         do k = 1, k0
            ! old ECOHAM
            ! ca concentration [mumol/kg soln]: Riley & Tongudai (1967)
            ! ca(j,k,i)=10285.*salt(j,k,i)/35.

            ! MK new
            call calcite(temp(j,k,i),salt(j,k,i),rho(j,k,i),pres(j,k,i),ca(j,k,i),aksp(j,k,i))
         enddo
      enddo
   enddo
#endif

   ! initialize from warmstart
   if (iwarm == 1) then
      call warm_in(ierr)
   endif

#ifdef module_biogeo
   if (myID==master) call write_log_message(" UPDATING initial biogeo ")
   call update_biogeo(ierr)
#endif

#ifdef module_chemie
   if (myID==master) call write_log_message(" UPDATING initial chemie ")
#ifdef module_biogeo
   ! import DIC and ALK from statevariable to chemistry module
   call update_chemie('put',ierr,st(:,:,:,idic), st(:,:,:,ialk))
#else
   ! transfer DIC and ALK from chemistry module to statevariable
   call update_chemie('get',ierr,st(:,:,:,idic), st(:,:,:,ialk))
#endif
   ! initialize derived constituents
   call do_chemie(1,ierr,st(:,:,:,idic),st(:,:,:,ialk))
#endif

   ! init output
   call init_output(ierr);

   ! output initial values
   if (myID==master) call write_log_message(" WRITING initial state ")
   call do_output(ierr)

   if (ierr/=0) then
      write( error_message, '("[",i2.2,"] ERROR - init_main",i3)') myID, ierr
      call stop_ecoham(ierr, msg=error_message);
   end if
   return

   end subroutine init_main

!=======================================================================
!
! !INTERFACE:
   subroutine do_main(ierr)
#define SUBROUTINE_NAME 'do_main'
!
! !DESCRIPTION:
!
! !USES:
   use mod_var
   use mod_grid,       only: k_index, vol, dVol
   use mod_flux,       only: empty_flux_hydro, empty_fluxd
   use mod_boundaries, only: update_boundaries, do_boundaries
   use mod_hydro,      only: update_hydro, hyd_up, advec, dif_ver
   use mod_output,     only: do_output
   use mod_utils,      only: DOYstring
#ifdef module_sediment
   use mod_coupler,    only: pel2sed
   use mod_sediment,   only: do_sediment
#endif

#ifdef module_biogeo
   use mod_biogeo,     only: update_biogeo, empty_biofunctions
#ifdef module_sediment
   use mod_coupler,    only: sed2pel
#endif
#endif

#ifdef module_chemie
   use mod_chemie,     only: update_chemie, do_chemie
#endif
#ifdef debug_river
   use mod_flux,       only: f_riv, d_riv
#endif
   implicit none
!
! !OUTPUT PARAMETERS:
   integer, intent(inout)  :: ierr
!
! !LOCAL VARIABLES:
   real    :: td, td0, dzFac
   integer :: nextday, is, int_err
   integer :: i, j
!
!-----------------------------------------------------------------------
#include "call-trace.inc"

   int_err = 0
   nextday = int(day_of_year+dt*0.5)+1

   do is = 1, EndOfMainstep

      ! exit main loop reaching new day or rday_end
      if (nextday == int(day_of_year+dt*0.5) .or. day_of_year+dt*0.5 > rday_end) then
         ! scale digits to dt
         td = day_of_year
         day_of_year = int(td) + nint( (td-int(td))/dt )*dt
         return
      endif
      td0 = day_of_year ! keep track of steps starttime

      if (myID == master) then
         write(long_msg,'(3x,"mainstep: ",i8,3x,"time: ",f10.4)') is,day_of_year
         call write_log_message(long_msg); long_msg = ''
      endif

#if defined MPI && defined SYNCHRONOUS_RECURSION_STEPS
      ! call this to keep logfile in (minimum) chronological order
      call MPI_ALLREDUCE(ierr, ierr_all, 1, MPI_INT, MPI_SUM, MPI_COMM_ECOHAM, ierr_mpi)
#endif

#ifdef debug_river
  if (51 >= iStartCalc .and. 51 <= iEndCalc) then
 !print*,'======================================================================'
write(*,'("[",i2.2,"] do_main#219(is,td0,f_riv,d_riv)",i5,2x,f6.3,3e20.10)') &
                      myID,is,td0,f_riv(73,1,51,in3n),f_riv(73,1,51,in3n)/dt_out,d_riv(73,1,51,in3n)
!print*,'======================================================================'
  endif
#endif
#ifdef debugMK
  if (33 >= iStartCalc .and. 33 <= iEndCalc) then
!print*,'======================================================================'
write(*,'("[",i2.2,"] do_main#228(is,td0,p1a(1:9)",i5,2x,f6.3,9e20.10)') &
                      myID,is,td0,st(33,1:9,70,ip1a)
!print*,'======================================================================'
  endif
#endif
!       if ( mod(nint((is-1)*dt*secs_per_day), dt_out) == 0 ) then
!          call empty_fluxd(ierr)
! #ifdef module_biogeo
!          call empty_biofunctions(ierr)
! #endif
!       endif

      ! update  T, S, water level, transport velocities and diffusion coeff.
      call update_hydro(ierr)

      !hydrodynamic fluxes (advection, diffusion & sinking)
      call empty_flux_hydro(ierr)      ! f_adv, f_adh, f_mxv, f_mxh, f_hyd, seto_flux = 0
      if (advec.or.dif_ver) then
         call hyd_up(dt,ierr)     !DISPERSION MODEL
      endif

      call update_boundaries(ierr)

#ifdef module_biogeo
      call update_biogeo(ierr)
#endif

#ifdef module_chemie
      call do_chemie(talk_treat,ierr,st(:,:,:,idic),st(:,:,:,ialk))
#endif

#ifdef module_sediment
#ifdef module_biogeo
      ! update sinking fluxes acting on current mainstep, calculated in hyd_up
      call pel2sed(ierr)
#endif
      ! do mainstep for sediment model
      call do_sediment(td0,dt,ierr)
#endif

      ! do mainstep for pelagic model
      level=1
      call recursive_step(day_of_year,dt,level,ierr)

      ! now update tracer concentrations in surface layer with respect to new cell volumes
      dzFac = 1.
      do i = iStartCalc, iEndCalc
         do j = jStartCalc, jEndCalc
            if (k_index(j,i) < 1) cycle
            dzFac = 1. / (1. + (dvol(j,i)*dt /vol(j,1,i)) )
            st (j,1,i,:) = st(j,1,i,:) *dzFac     ! concentrations at end of timestep
         enddo
      enddo

      ! scale digits to dt
      td = day_of_year
      day_of_year = int(td) + nint( (td-int(td))/dt )*dt

      if ( mod(nint(is*dt*secs_per_day), dt_out) == 0 ) then
         ! updating arrays to output timestamp
         call update_hydro(ierr)
         call update_boundaries(ierr)
#ifdef module_biogeo
         call update_biogeo(ierr)
#endif
#ifdef debug_river
  if (51 >= iStartCalc .and. 51 <= iEndCalc) then
!print*,'======================================================================'
write(*,'("[",i2.2,"] do_main#286(is,td0,f_riv,d_riv)",i5,2x,f6.3,2e20.10)') &
                      myID,is,td0,f_riv(73,1,51,in3n)/dt_out,d_riv(73,1,51,in3n)
!print*,'======================================================================'
  endif
#endif
#ifdef debugMK
  if (33 >= iStartCalc .and. 33 <= iEndCalc) then
!print*,'======================================================================'
write(*,'("[",i2.2,"] do_main#296(is,td0,p1a(1:9)",i5,2x,f6.3,9e20.10)') &
                      myID,is,td0,st(33,1:9,70,ip1a)
!print*,'======================================================================'
  endif
#endif
         call do_output(ierr)

         ! clear arrays for time-integrated parameters
         call empty_fluxd(ierr)
#ifdef module_biogeo
         call empty_biofunctions(ierr)
#endif
      endif
!print*,'main #256  ',DOYstring(day_of_year)

   enddo

   if (ierr/=0) then
      write( error_message, '("[",i2.2,"] ERROR - do_main",i3)') myID, ierr
      call stop_ecoham(ierr, msg=error_message);
   end if
   return

   end subroutine do_main

!=======================================================================
!
! !INTERFACE:
   subroutine main_step(dt_,ierr)
#define SUBROUTINE_NAME 'main_step'
!
! !DESCRIPTION:
!
! !USES:
   use mod_var!,        only: empty_sst
   use mod_flux!,       only: empty_flux, cum_flux
   use mod_boundaries, only: do_boundaries
   use mod_hydro,      only: do_hydro
#ifdef module_biogeo
   use mod_biogeo,     only: do_biogeo
#ifdef module_sediment
   use mod_coupler,    only: sed2pel
#endif
#endif
   implicit none
!
! !OUTPUT PARAMETERS:
   real,    intent(inout)  :: dt_
   integer, intent(inout)  :: ierr
!
! !LOCAL VARIABLES:
!
!-----------------------------------------------------------------------
#include "call-trace.inc"

   !empty instantaneous fluxes
   call empty_sst(ierr)      !SST = 0
   call empty_flux(ierr)     !f_from_to = 0, f_riv, f_dil, f_pev, f_res =0

   !set open boundaries (restoring -> st) and apply local changes due to
   !environmental factors (air-sea fluxes and river discharge -> sst)
   call do_boundaries(dt_,ierr)
#ifdef debug_river
  if (51 >= iStartCalc .and. 51 <= iEndCalc) then
 !print*,'======================================================================'
write(*,'("[",i2.2,"] mainstep#353(f_riv,d_riv)",3e20.10)') myID,f_riv(73,1,51,in3n),d_riv(73,1,51,in3n)
!print*,'======================================================================'
  endif
#endif
#ifdef debugMK
  if (33 >= iStartCalc .and. 33 <= iEndCalc) then
print*,'======================================================================'
write(*,'("[",i2.2,"] time   ",9e20.10)') myID,dt_
write(*,'("[",i2.2,"] mainstep#360(st)   ",9e20.10)') myID,st(70,8:9,33,ip1a)
write(*,'("[",i2.2,"] mainstep#361(f_riv)",9e20.10)') myID,f_riv(70,8:9,33,ip1a)
!print*,'======================================================================'
  endif
#endif
   !apply local changes (rates) due to hydrodynamics
   call do_hydro(ierr)      ! FLUXES WITHIN PELAGIAL (-> sst)
#ifdef debugMK
  if (33 >= iStartCalc .and. 33 <= iEndCalc) then
 !print*,'======================================================================'
write(*,'("[",i2.2,"] mainstep#370(f_hyd)",9e20.10)') myID,f_hyd(70,8:9,33,ip1a)
!print*,'======================================================================'
  endif
#endif

#ifdef module_biogeo
   call do_biogeo(ierr)     ! FLUXES WITHIN PELAGIAL (-> sst)
#ifdef debugMK
  if (33 >= iStartCalc .and. 33 <= iEndCalc) then
 !print*,'======================================================================'
write(*,'("[",i2.2,"] mainstep#380(sst)  ",9e20.10)') myID,sst(70,8:9,33,ip1a)
write(*,'("[",i2.2,"] mainstep#381(fr_to)",9e20.10)') myID,f_from_to(70,8:9,33,i_p1a_p1a)
print*,'======================================================================'
  endif
#endif
#ifdef module_sediment
   call sed2pel(ierr)       ! DIFFUSIVE FLUXES BETWEEN SEDIMENT AND PELAGIAL (-> sst)
#endif
#endif

   if (ierr/=0) then
      write( error_message, '("[",i2.2,"] ERROR - main_step",i3)') myID, ierr
      call stop_ecoham(ierr, msg=error_message);
   end if
   return

   end subroutine main_step

!=======================================================================
!
! !INTERFACE:
   recursive subroutine recursive_step(td_,dt_,level_,ierr)
#define SUBROUTINE_NAME 'recursive_step'
!
! !DESCRIPTION:
!
! !USES:
   use mod_var
   use mod_flux,     only : cum_flux, cum_flux_hydro
   use mod_utils,    only : integrate_st, DOYstring
#ifdef module_biogeo
   use mod_biogeo,   only : cum_biofunctions
#endif
   implicit none
!
! !INPUT PARAMETERS:
   real,    intent(inout) :: td_,dt_
   integer, intent(inout) :: level_
   integer, intent(inout) :: ierr
!
! !LOCAL VARIABLES:
   integer       :: int_err
   real          :: dt0
!
!-----------------------------------------------------------------------
#include "call-trace.inc"

   if (dt_<dt_min) then
      ierr = 1
      call stop_ecoham(ierr, msg='Model stops: minimal dt reached ')
   endif

   if (level_==1) call main_step(dt_,ierr)
   
   int_err = ierr
   call integrate_st(td_,dt_,level_,int_err)

! write( long_msg, '(" myID ",i2.2,"  int_err=",i1,"  level=",i2)') myID,int_err,level_
! call write_log_message(long_msg); long_msg = ''

   if (int_err/=0) then
      dt0=dt_/2.
      level_=level_+1
      call recursive_step(td_,dt0,level_,ierr)
      call recursive_step(td_,dt0,level_,ierr)
      level_=level_-1
   else
      td_=td_+dt_
      call cum_flux(dt_,ierr)          ! INTEGRATE TO DAILY FLUXES
      call cum_flux_hydro(dt_, ierr)
#ifdef module_biogeo
      call cum_biofunctions(dt_,ierr)  ! INTEGRATE FUNCTIONAL PARAMETERS
#endif
      if (level_>1) call main_step(dt_,ierr)
   endif

   if (ierr/=0) then
      write( error_message, '("[",i2.2,"] ERROR - recursive_step, level: ",i3)') myID, level
      call stop_ecoham(ierr, msg=error_message);
   end if
   return

   end subroutine recursive_step

!=======================================================================
!
! !INTERFACE:
   subroutine end_main(ierr)
#define SUBROUTINE_NAME 'end_main'
!
! !DESCRIPTION:
!
! !USES:
   use mod_output,     only: warm_out, close_output
   use mod_utils,      only: get_timestamp
   implicit none
!
! !OUTPUT PARAMETERS:
   integer, intent(inout) :: ierr
!
! !LOCAL VARIABLES:
!
!-----------------------------------------------------------------------
#include "call-trace.inc"

   call warm_out(ierr);

   call close_output(ierr)

   call CPU_Time(cpu_t2)
   call get_timestamp(timestamp)

   if (myID==master) then
      call write_log_message( line_separator)

   if (ierr==0) then
      write(log_msg(1),'(" ECOHAM5 finished on ",a19)') timestamp
      write(log_msg(2),'(" CPU-time (overall)     : ",i8," seconds")') int(cpu_t2-cpu_t1)
      write(log_msg(3),'(" CPU-time (in time loop): ",i8," seconds")') int(cpu_t4-cpu_t3)
      long_msg = trim(log_msg(1))//new_line('a')//trim(log_msg(2))//new_line('a')//trim(log_msg(3))
      call write_log_message(long_msg); long_msg = ''
      !write(long_msg,'(" Sim-time/CPU-time:     ",f8.2)') simtime/(cpu_t2-cpu_t1)
      !call write_log_message(long_msg); long_msg = ''

      open(end_unit,file='Jeb_OK')
      write(end_unit,'("1")')
      close(end_unit)
   else
      write(log_msg(1),'(" ECOHAM5 stopped on ",a19)') timestamp
      write(log_msg(2),'(" CPU-time (overall)     : ",i8," seconds")') int(cpu_t2-cpu_t1)
      write(log_msg(3),'(" CPU-time (in time loop): ",i8," seconds")') int(cpu_t4-cpu_t3)
      long_msg = trim(log_msg(1))//new_line('a')//trim(log_msg(2))//new_line('a')//trim(log_msg(3))
      call write_log_message(long_msg); long_msg = ''
   endif

      call write_log_message( line_separator)
   end if ! master

#ifdef MPI
    call MPI_File_close(logfile_handle, ierr_mpi)
    if (ierr_mpi/=0) call write_log_message('WARNING - MPI_File_close : ierr/=0 !')
    call mpi_parallel_final(ierr_mpi)
    if (ierr_mpi/=0) call write_log_message('WARNING - mpi_parallel_final : ierr/=0 !')
#else
   close(log_unit)
#endif

   return

   end subroutine end_main

!=======================================================================

   end module mod_main

!=======================================================================
