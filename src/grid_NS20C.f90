!
! !MODULE: grid_NS20C.f90 --- parameters grid: North Sea, 20km, Version "C" (eco9)
!
! !INTERFACE:
   MODULE mod_grid_parameters
!
! !DESCRIPTION:
!
! !USES:
   implicit none
!
   ! GRID PARAMETERS
!    real,    parameter :: delta_lat     =  0.2              ! delta lat in degrees
!    real,    parameter :: delta_lon     =  0.3333333333333  ! delta lon in degrees
!    real,    parameter :: xlat0         = 44.4833333333333  ! most southerly zeta point
!    real,    parameter :: xlon0         = -3.4166666666667  ! most westerly zeta point
   real,    parameter :: delta_lat     =  12./60.          ! delta lat in degrees
   real,    parameter :: delta_lon     =  20./60.          ! delta lon in degrees
   real,    parameter :: xlat0         = 44.0 +29./60.     ! most southerly zeta point
   real,    parameter :: xlon0         = -3.0 -25./60.     ! most westerly zeta point
   integer, parameter :: imax          = 33                ! number of latidude indexes
   integer, parameter :: jmax          = 39                ! number of longitude indexes
   integer, parameter :: kmax          = 21                ! number of layer indexes
   integer, parameter :: ijmax         = imax*jmax         ! total number of 2D-surface-cells
   integer, parameter :: ijkmax        = imax*jmax*kmax    ! total number of 3D-cells
   integer, parameter :: iiwet2        = 702               ! number of wet surface cells
   integer, parameter :: iiwet         = 6159              ! number of wet cells
   character*99       :: gridfile      ='indh-NS20C.dat'   ! file defining topography

   ! set external partitioning in latitudinal direction, see eco_set_nml
   logical    :: external_partitioning = .false.           ! default is .false.
!=======================================================================

   end module mod_grid_parameters

!=======================================================================