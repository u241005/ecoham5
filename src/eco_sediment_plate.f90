#ifndef sediment_EMR
! !MODULE: eco_sediment_plate.f90 --- benthic biology for ecoham
!
! !INTERFACE:
   MODULE mod_sediment
#ifdef module_sediment
!
! !DESCRIPTION:
! This module is the container for the benthic biogeochemical "plate" model
!
! !USES:
   use mod_common
   use mod_grid
#ifdef MPI
   use mod_mpi_parallel
#endif
   use mod_var
   use mod_flux
   implicit none
!
!  default: all is private.
   private
!
! !PUBLIC MEMBER FUNCTIONS:
   public init_sediment, do_sediment, sd, n_sed_var
!
! !PUBLIC VARIABLES:
   integer, parameter :: n_sed_var_plate = 5
   integer, parameter :: max_flux_sed_plate = 6
   ! INDEX-pointers to sediment STATE VARIABLES:
   integer, parameter, public :: ised_poc=1,ised_pon=2,ised_pok=3
   integer, parameter, public :: ised_pop=4,ised_pos=5
   !
   real, dimension(:,:), allocatable, public :: f_poc_sed
   real, dimension(:,:), allocatable, public :: f_pon_sed
   real, dimension(:,:), allocatable, public :: f_pop_sed
   real, dimension(:,:), allocatable, public :: f_pos_sed
   real, dimension(:,:), allocatable, public :: f_pok_sed
   real, dimension(:,:), allocatable, public :: f_n3n_brm
   real, dimension(:,:), allocatable, public :: f_o2o_brm
   real, dimension(:,:), allocatable, public :: f_sed_dic      !seti_dic
   real, dimension(:,:), allocatable, public :: f_sed_n4n      !seti_n4n
   real, dimension(:,:), allocatable, public :: f_sed_n1p      !seti_n1p
   real, dimension(:,:), allocatable, public :: f_sed_n5s      !seti_n5s
   real, dimension(:,:), allocatable, public :: f_sed_nn2      !seti_nn2
   real, dimension(:,:), allocatable, public :: f_sed_o3c      !seti_o3c
!
! !LOCAL VARIABLES (private):
! used for namelist:
   real  :: p_seitz,brc,brn,brp,brs
!
#ifdef cskc
   character(len=*), parameter, private :: init_endian   = 'LITTLE_ENDIAN'
#else
   character(len=*), parameter, private :: init_endian   = 'BIG_ENDIAN'
#endif
   character(len=*), parameter, private :: init_dir      = 'Input/'
   integer,          parameter, private :: init_realkind = 8   != ibyte_per_real
!
!=======================================================================

   contains

!=======================================================================
!
! !INTERFACE:
   subroutine init_sediment(ierr)
#define SUBROUTINE_NAME 'init_sediment'
!
! !DESCRIPTION:
!
! !USES:
   implicit none
!
! !INPUT PARAMETERS:
   integer, intent(inout) :: ierr
!
! !LOCAL VARIABLES:
   logical       :: oldDNF
   integer       :: n
   logical              :: file_exist
   real(kind=init_realkind), allocatable :: input1d(:), input2d(:,:)
!
   namelist /sediment_nml/p_seitz,brc,brn,brp,brs
!
!-------------------------------------------------------------------------
#include "call-trace.inc"
#ifdef sediment_EMR
   ierr = 1
   write(log_msg(1),'("##########################################################################")')
   write(log_msg(2),'("# ERROR: you can not run this code with flag -Dsediment_EMR       #")')
   write(log_msg(3),'("#        for using this model please disable and re-compile              #")')
   write(log_msg(4),'("##########################################################################")')
   long_msg = trim( log_msg(1)) // new_line('a') // trim( log_msg(2)) // new_line('a') // &
            & trim( log_msg(3)) // new_line('a') // trim( log_msg(4))
   call write_log_message(long_msg); long_msg = ''
   call stop_ecoham(ierr, msg=long_msg); long_msg = ''
#endif

   if (myID==master) call write_log_message(" INITIALIZING sediment ")

   ! initialise arrays for sediment statevariables
   n_sed_var = n_sed_var_plate
   allocate(  sd(jmax,iStartD:iEndD,n_sed_var))
   allocate( ssd(jmax,iStartD:iEndD,n_sed_var))
   sd = fail
   call empty_ssd(ierr)

   ! initialise arrays for fluxes from sediment to pelagial
   max_flux_sed = max_flux_sed_plate
   allocate( seti_flux(jmax,iStartD:iEndD,max_flux_sed))
   allocate( sedi_flux(jmax,iStartD:iEndD,max_flux_sed)); sedi_flux = 0.  !jp

   ! initialize meta data for output
   allocate( sd_out2D(n_sed_var)); sd_out2D = .false.
   allocate( sd_name (n_sed_var))
   allocate( sd_long (n_sed_var))
   allocate( sd_unit (n_sed_var))
   allocate( sedi_flux_out2D(max_flux_sed)); sedi_flux_out2D = .false.
   allocate( sedi_flux_name (max_flux_sed))
   allocate( sedi_flux_long (max_flux_sed))
   allocate( sedi_flux_unit (max_flux_sed))
   call set_varnames_plate(ierr)

   ! initialize wrapper for fluxes accros pelagic-sediment interface
   allocate( f_poc_sed(jmax,iStartD:iEndD) ); f_poc_sed = 0.
   allocate( f_pon_sed(jmax,iStartD:iEndD) ); f_pon_sed = 0.
   allocate( f_pop_sed(jmax,iStartD:iEndD) ); f_pop_sed = 0.
   allocate( f_pos_sed(jmax,iStartD:iEndD) ); f_pos_sed = 0.
   allocate( f_pok_sed(jmax,iStartD:iEndD) ); f_pok_sed = 0.
   allocate( f_n3n_brm(jmax,iStartD:iEndD) ); f_n3n_brm = 0.
   allocate( f_o2o_brm(jmax,iStartD:iEndD) ); f_o2o_brm = 0.
   allocate( f_sed_dic(jmax,iStartD:iEndD) ); f_sed_dic = 0.
   allocate( f_sed_n4n(jmax,iStartD:iEndD) ); f_sed_n4n = 0.
   allocate( f_sed_n1p(jmax,iStartD:iEndD) ); f_sed_n1p = 0.
   allocate( f_sed_n5s(jmax,iStartD:iEndD) ); f_sed_n5s = 0.
   allocate( f_sed_nn2(jmax,iStartD:iEndD) ); f_sed_nn2 = 0.
   allocate( f_sed_o3c(jmax,iStartD:iEndD) ); f_sed_o3c = 0.

   open(nml_unit,file='eco_bio.nml',action='read',status='old')
   read(nml_unit,nml=sediment_nml)
   close(nml_unit)

   oldDNF  = .false.
   if (oldDNF) p_seitz = zero

#ifndef module_biogeo
   ! set switches for oxic-anoxic condition to default
   ! (only make sense in combination with biogeochemical module)
   oswtch = 1.0
   nswtch = 1.0
#endif

   ! coldstart initialization
   if (iwarm.ne.1) then
      if (myID == master) call write_log_message("   coldstart initialization")
      ! INITIAL VALUES most in [mmol m-3]
      ! default initialization, homogenous distribution
      where(ixistK==1) sd(:,:,ised_poc) = 0.0
      where(ixistK==1) sd(:,:,ised_pon) = 0.0
      where(ixistK==1) sd(:,:,ised_pok) = 0.0
      where(ixistK==1) sd(:,:,ised_pop) = 0.0
      where(ixistK==1) sd(:,:,ised_pos) = 0.0

      if (iwarm == 2) then
        ! read from initial distribution files, if exist, and overwrite
         allocate (input1d(iiwet),input2d(jMax,iMax))
         do n = 1, n_sed_var
            filename = trim(init_dir)//'init.'//trim(sd_name(n))//'.direct'
            inquire (file=trim(filename), exist=file_exist)
            if (file_exist) then
               if (myID == master) call write_log_message("   include initialization from "//trim(filename) )
               open (warm_unit, file=trim(filename), status='old', form='unformatted', &
                                access='direct', recl=init_realkind*iiwet2, convert=trim(init_endian) )
               read (warm_unit, rec = 1) input1d
               input2d(:,:) = unpack(real(input1d),ixistk_master(:,:)==1,fail)
               sd(:,:,n)    = input2d(:,iStartD:iEndD)
               close (warm_unit)
            else
              if (myID == master) call write_log_message("   init file "//trim(filename)//" does not exist" )
            endif
         enddo
         deallocate (input1d, input2d)
      endif
   endif

   if(ierr/=0) then
      write(error_message,'("ERROR - init_sediment ",i3)') ierr
      call stop_ecoham(ierr, msg=error_message)
   endif
   return

   end subroutine init_sediment

!=======================================================================
!
! !INTERFACE:
   subroutine set_varnames_plate(ierr)
#define SUBROUTINE_NAME 'set_varnames_plate'
!
! !DESCRIPTION:
!
! !USES:
   IMPLICIT NONE
!
! !OUTPUT PARAMETERS:
   integer, intent(inout)  :: ierr
!
! !LOCAL VARIABLES:
   integer            :: m
!
!-----------------------------------------------------------------------
#include "call-trace.inc"

   ! clear character arrays
   sd_name(:) = ''
   sd_long(:) = ''
   sd_unit(:) = ''

   ! set varnames for prognostic sediment variables ("sd") - "plate"
   sd_name(ised_poc)='sd_poc'
   sd_long(ised_poc)='SEDIMENT PARTICULATE CARBON'
   sd_unit(ised_poc)='mmolC m-2'

   sd_name(ised_pon)='sd_pon'
   sd_long(ised_pon)='SEDIMENT PARTICULATE NITROGEN'
   sd_unit(ised_pon)='mmolN m-2'

   sd_name(ised_pop)='sd_pop'
   sd_long(ised_pop)='SEDIMENT PARTICULATE PHOSPHORUS'
   sd_unit(ised_pop)='mmolP m-2'

   sd_name(ised_pos)='sd_pos'
   sd_long(ised_pos)='SEDIMENT PARTICULATE SILICATE'
   sd_unit(ised_pos)='mmolSi m-2'

   sd_name(ised_pok)='sd_pok'
   sd_long(ised_pok)='SEDIMENT PARTICULATE CaCO3'
   sd_unit(ised_pok)='mmolC m-2'

   ! fluxes from sediment to pelagial (remineralisation)
   sedi_flux_name(:) = ''
   sedi_flux_long(:) = ''
   sedi_flux_unit(:) = ''

   m=1; i_sed_dic=m; sedi_flux_name(m)='sed_dic'; sedi_flux_long(m)='DIC EXPORT FROM SEDIMENT'; sedi_flux_unit(m)='mmolC m-3 d-1'  ! seti_dic
   m=2; i_sed_n4n=m; sedi_flux_name(m)='sed_n4n'; sedi_flux_long(m)='NH4 EXPORT FROM SEDIMENT'; sedi_flux_unit(m)='mmolN m-3 d-1'  ! seti_n4n
   m=3; i_sed_o3c=m; sedi_flux_name(m)='sed_o3c'; sedi_flux_long(m)='CO3 EXPORT FROM SEDIMENT'; sedi_flux_unit(m)='mmolC m-3 d-1'  ! seti_o3c
   m=4; i_sed_n1p=m; sedi_flux_name(m)='sed_n1p'; sedi_flux_long(m)='PO4 EXPORT FROM SEDIMENT'; sedi_flux_unit(m)='mmolP m-3 d-1'  ! seti_n1p
   m=5; i_sed_n5s=m; sedi_flux_name(m)='sed_n5s'; sedi_flux_long(m)='N5S EXPORT FROM SEDIMENT'; sedi_flux_unit(m)='mmolN m-3 d-1'  ! seti_n5s
   m=6; i_sed_nn2=m; sedi_flux_name(m)='sed_nn2'; sedi_flux_long(m)='N2 EXPORT FROM SEDIMENT' ; sedi_flux_unit(m)='mmolN m-3 d-1'  ! seti_nn2

   if (ierr/=0) then
      write( error_message, '("[",i2.2,"] ERROR - set_varnames: ",i3)') myID, ierr
      call stop_ecoham(ierr, msg=error_message)
   end if
   return

   end subroutine set_varnames_plate

!=======================================================================
!
! !INTERFACE:
subroutine do_sediment(td_,dt_,ierr)
#define SUBROUTINE_NAME 'do_sediment'
!
! !DESCRIPTION:
! routine handles benthic processes
!
! !USES:
   use mod_utils, only : integrate_sd
   implicit none
!
! !OUTPUT PARAMETERS:
   real,    intent(in)    :: td_, dt_
   integer, intent(inout) :: ierr
!
! !LOCAL VARIABLES:
   integer       :: i, j, k0
   integer       :: is, nstep
   real          :: tds, dts
!
!-----------------------------------------------------------------------
#include "call-trace.inc"

   !timestep
   nstep = 1 !dt_plate/dt_   ! number of micro timesteps
   dts   = real(dt_/nstep)   ! timestep (d)
   tds   = td_               ! sediment day_of_year

   ! reset diffusion fluxes from sed to pel
   f_sed_dic = zero
   f_sed_o3c = zero
   f_sed_nn2 = zero
   f_sed_n4n = zero
   f_sed_n1p = zero
   f_sed_n5s = zero
   f_o2o_brm = zero
   f_n3n_brm = zero

   ! time loop
   do is = 1, nstep

      if (myID == master .and. nstep>1) then
         write(long_msg,'(3x,"sediment-step: ",i8," of ",i8,3x,"time: ",f10.4)') is,nstep,tds
         call write_log_message(long_msg); long_msg = ''
      endif

      call empty_ssd(ierr)                  ! SSD = 0

      ! set fluxes accross pelagial interface
      do i = iStartCalc, iEndCalc
         do j = jStartCalc, jEndCalc
            k0 = k_index(j,i)
            if (k0<=0) cycle
            ssd(j,i,ised_poc) = ssd(j,i,ised_poc) + f_poc_sed(j,i)
            ssd(j,i,ised_pon) = ssd(j,i,ised_pon) + f_pon_sed(j,i)
            ssd(j,i,ised_pop) = ssd(j,i,ised_pop) + f_pop_sed(j,i)
            ssd(j,i,ised_pos) = ssd(j,i,ised_pos) + f_pos_sed(j,i)
            ssd(j,i,ised_pok) = ssd(j,i,ised_pok) + f_pok_sed(j,i)
         enddo
      enddo

      call do_sediment_step(dts,ierr)       ! SSD = SSD+f_sed_xxx
!print*,'sediment_plate # 326',ierr
      !print*,'call integrate_sd',td_,dt_
      call integrate_sd(tds,dts,1,ierr)     ! SD = SD + SSD*dts
!print*,'sediment_plate # 329',ierr

      tds = tds + dts

   enddo

   ! convert fluxes to unit (mmol m-2 d-1)
   f_sed_dic = f_sed_dic /dt_
   f_sed_o3c = f_sed_o3c /dt_
   f_sed_nn2 = f_sed_nn2 /dt_
   f_sed_n4n = f_sed_n4n /dt_
   f_sed_n1p = f_sed_n1p /dt_
   f_sed_n5s = f_sed_n5s /dt_
   f_o2o_brm = f_o2o_brm /dt_
   f_n3n_brm = f_n3n_brm /dt_

   if(ierr/=0) then
      write(error_message,'("ERROR - do_sediment ",i3)') ierr
      call stop_ecoham(ierr, msg=error_message)
   endif
   return

   end subroutine do_sediment

!=======================================================================
!
! !INTERFACE:
subroutine do_sediment_step(dt_,ierr)
#define SUBROUTINE_NAME 'do_sediment_step'
!
! !DESCRIPTION:
! routine handles benthic processes
!
! !USES:
#ifdef module_chemie
   use mod_chemie, only : ch, io3c
#endif
   implicit none
!
! !OUTPUT PARAMETERS:
   integer, intent(inout) :: ierr
   real,    intent(inout) :: dt_
!
! !LOCAL VARIABLES:
   integer       :: i, j, k0
   real          :: f_sed_dic_, f_sed_o3c_, f_sed_n1p_, f_sed_nn2_, f_sed_n4n_, f_sed_n5s_
   real          :: f_o2o_brm_, f_n3n_brm_
   real          :: bdnf_basic !,bremi,bdnf
!
!-----------------------------------------------------------------------
#include "call-trace.inc"

   do i = iStartCalc, iEndCalc
      do j = jStartCalc, jEndCalc
         k0 = k_index(j,i)
         if (k0>0) then
            f_sed_dic_ = sd(j,i,ised_poc)*brc                   !vorher seti_flux(ic2,1)
            !f_sed_o3c(j,i)=real(isw_sed)*sd_dsc(j,i)/30.*(1.-delta_o3c(j,k0,i)/(delta_o3c(j,k0,i)+c0))/dz(j,k0,i)  ! DETRITUS-skeleton
            ! found in eco9fs as                                !vorher seti_flux(ic2,3):
#ifdef module_chemie
            if (caco3_diss==1 .or. delta_o3c(j,k0,i)<0.0) then
               delta_o3c(j,k0,i) = max(0.,ch(j,k0,i,io3c)-aksp(j,k0,i)/ca(j,k0,i))
               f_sed_o3c_ = sd(j,i,ised_pok)/30.  &
                             *(1.-delta_o3c(j,k0,i)/(delta_o3c(j,k0,i)+c0))  ! DETRITUS-skeleton
            else
               delta_o3c(j,k0,i)=max(0.,ch(j,k0,i,io3c)-aksp(j,k0,i)/ca(j,k0,i))
               f_sed_o3c_=0.
            endif
#else
            delta_o3c(j,k0,i)=0.
            f_sed_o3c_=0.
#endif
            !seto_flux(ic2,io2o) vorher f_o2o_sed
            ! when st_o2o<0: 1mol H2S=-1mol o2 (not -0.5mol O2)
            !seto_flux(j,i,io2o) = oswtch(j,k0,i)*f_sed_dic(j,i)    &                      !oxic
            !                     +(1.-oswtch(j,k0,i))*(1.-nswtch(j,k0,i))*f_sed_dic(j,i)  !anoxic2
            f_o2o_brm_ = oswtch(j,k0,i)*f_sed_dic_    &                         !oxic
                         +(1.-oswtch(j,k0,i))*(1.-nswtch(j,k0,i))*f_sed_dic_  !anoxic2

            f_sed_n4n_ = sd(j,i,ised_pon)*brn !vorher seti_flux(ic2,2)
            bdnf_basic = p_seitz*f_o2o_brm_
            f_sed_nn2_ = bdnf_basic-max(0.,bdnf_basic-f_sed_n4n_)   ! benthic denitrification !vorher f_sed_nn2
            f_sed_n4n_ = max(0.,f_sed_n4n_-bdnf_basic)              ! benthic N-remi          !vorher f_sed_n4n
            !MK Was verbirgt sich hinter diesem Fluss/Prozess?
            !f_n3n_nn2(ic)=f_n3n_nn2(ic)+0.5*(1.-oswtch(ic))*nswtch(ic)*bremi
            !seto_flux(j,i,in3n) = 0.5*(1.-oswtch(j,k0,i))*nswtch(j,k0,i)*f_sed_n4n(j,i)
            f_n3n_brm_ = 0.5*(1.-oswtch(j,k0,i))*nswtch(j,k0,i)*f_sed_n4n_                 !anoxic1

            f_sed_n1p_ = sd(j,i,ised_pop)*brp !vorher seti_flux(ic2,4)     ! phosphorus

            f_sed_n5s_ = sd(j,i,ised_pos)*brs !vorher seti_flux(ic2,5)

         ! source equations (mmol m-2 d-1) for benthic variables
            ssd(j,i,ised_poc) = ssd(j,i,ised_poc) - f_sed_dic_
            ssd(j,i,ised_pok) = ssd(j,i,ised_pok) - f_sed_o3c_
            ssd(j,i,ised_pon) = ssd(j,i,ised_pon) -(f_sed_nn2_+f_sed_n4n_)
            ssd(j,i,ised_pop) = ssd(j,i,ised_pop) - f_sed_n1p_
            ssd(j,i,ised_pos) = ssd(j,i,ised_pos) - f_sed_n5s_

         ! map sediment fluxes defined in the pelagic module (mmol m-2)
            f_sed_dic(j,i) = f_sed_dic(j,i) + f_sed_dic_ *dt_
            f_sed_o3c(j,i) = f_sed_o3c(j,i) + f_sed_o3c_ *dt_
            f_sed_nn2(j,i) = f_sed_nn2(j,i) + f_sed_nn2_ *dt_
            f_sed_n4n(j,i) = f_sed_n4n(j,i) + f_sed_n4n_ *dt_
            f_sed_n1p(j,i) = f_sed_n1p(j,i) + f_sed_n1p_ *dt_
            f_sed_n5s(j,i) = f_sed_n5s(j,i) + f_sed_n5s_ *dt_
            f_o2o_brm(j,i) = f_o2o_brm(j,i) + f_o2o_brm_ *dt_
            f_n3n_brm(j,i) = f_n3n_brm(j,i) + f_n3n_brm_ *dt_

         endif
      enddo
   enddo

   if(ierr/=0) then
      write(error_message,'("ERROR - do_sediment_step ",i3)') ierr
      call stop_ecoham(ierr, msg=error_message)
   endif
   return

   end subroutine do_sediment_step

!=======================================================================
#endif /*#ifdef module_sediment*/
   end module mod_sediment

!=======================================================================
#endif /*#ifndef sediment_EMR*/
