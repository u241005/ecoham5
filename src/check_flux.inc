! !INTERFACE:
   subroutine check_flux(jcol,icol,ivar,ierr)
#define SUBROUTINE_NAME 'check_flux'
!
! !DESCRIPTION:
!
! !USES:
   use mod_grid,  only: k_index, dz
   use mod_utils, only: DOYstring
   use mod_var
   use mod_flux
   implicit none
!
! !INPUT PARAMETERS:
   integer, intent(in)    :: jcol, icol, ivar
   !real, intent(in)       :: dz_old
   integer, intent(inout) :: ierr
!
! !LOCAL VARIABLES:
   character(len=19)      :: str
   integer                :: iu, n, ida, ihour, imo
   real, dimension(kmax)  :: st_, st_new, st_old, dz_, dz_fac!, dz_old!, add_mass
   real, dimension(kmax)  :: d_adh_, d_adv_, d_mxh_, d_mxv_
   real, dimension(kmax)  :: d_hyd_, d_riv_, d_dil_, d_res_
   real, dimension(kmax)  :: f_adh_, f_adv_, f_mxh_, f_mxv_
   real, dimension(kmax)  :: f_hyd_, f_riv_, f_dil_, f_res_
   integer :: k
!
!-----------------------------------------------------------------------
#include "call-trace.inc"

   write(str,'(a19)') DOYstring(day_of_year)
   read (str,'(i4,x,i2,x,i2,x,i2)') iy1, imo, ida, ihour

   dz_       = dz(jcol,1:kmax,icol)
   !dz_old    = dz_
   !dz_old(1) = dz_(1) - dz_vol(jcol,icol)!/dt_out
   !dz_old(1) = dz_(1) - dVol(jcol,icol)*dt_out/area(jcol,icol)

   dz_fac = 1.0
!   dz_fac(1) = dz_(1) / (dz_(1) - dz_vol(jcol,icol))
   dz_fac(1) = one / (one - dz_vol(jcol,icol)/dz_(1))

   d_hyd_ = d_hyd(jcol,:,icol,ivar)!/dz_old(1:kmax)
   d_adh_ = d_adh(jcol,:,icol,ivar)!/dz_old(1:kmax)
   d_adv_ = d_adv(jcol,:,icol,ivar)!/dz_old(1:kmax)
   d_mxh_ = d_mxh(jcol,:,icol,ivar)!/dz_old(1:kmax)
   d_mxv_ = d_mxv(jcol,:,icol,ivar)!/dz_old(1:kmax)
   d_riv_ = d_riv(jcol,:,icol,ivar)!/dz_old(1:kmax)
   d_dil_ = d_dil(jcol,:,icol,ivar)!/dz_old(1:kmax)

   f_hyd_ = d_hyd(jcol,:,icol,ivar)/dz_old(1:kmax)
   f_adh_ = d_adh(jcol,:,icol,ivar)/dz_old(1:kmax)
   f_adv_ = d_adv(jcol,:,icol,ivar)/dz_old(1:kmax)
   f_mxh_ = d_mxh(jcol,:,icol,ivar)/dz_old(1:kmax)
   f_mxv_ = d_mxv(jcol,:,icol,ivar)/dz_old(1:kmax)
   f_riv_ = d_riv(jcol,:,icol,ivar)/dz_old(1:kmax)
   f_dil_ = d_dil(jcol,:,icol,ivar)/dz_old(1:kmax)

   st_new = st(jcol,1:kmax,icol,ivar)     ! (mmol m-3)
   st_    = st(jcol,1:kmax,icol,ivar)*dz_ ! (mmol m-2)
   do k = 1, k_index(jcol,icol)
      st_old(k) = (st_(k) -d_adh_(k)-d_adv_(k)-d_mxh_(k)-d_mxv_(k)-d_riv_(k)) !/(dz_(1) - dz_vol(jcol,icol))
      !st_old(k) = (st_(k) -d_adh_(k)-d_adv_(k)-d_mxh_(k)-d_mxv_(k)-d_riv_(k)) /(dz_(1) - dz_adh(jcol,icol))
   enddo


! bingo!
   f_hyd_(1) = (d_hyd_(1) - st_new(1)*dz_adh(jcol,icol)) /dz_old(1)
   f_adh_(1) = (d_adh_(1) - st_new(1)*dz_adh(jcol,icol)) /dz_old(1)
   f_riv_(1) = (d_riv_(1) - st_new(1)*dz_riv(jcol,icol)) /dz_old(1)

   !f_hyd_(1) = st_(1)*(d_hyd_(1)/st_(1) - dz_adh(jcol,icol)/dz_(1)) /dz_old(1)
   !f_adh_(1) = st_(1)*(d_adh_(1)/st_(1) - dz_adh(jcol,icol)/dz_(1)) /dz_old(1)
   !f_riv_(1) = st_(1)*(d_riv_(1)/st_(1) - dz_riv(jcol,icol)/dz_(1)) /dz_old(1)

   !f_hyd_(1) = (st_(1)*(1.0 - dz_adh(jcol,icol)/dz_(1)) - (st_(1) - d_hyd_(1)) )  /dz_old(1)
   !f_adh_(1) = (st_(1)*(1.0 - dz_adh(jcol,icol)/dz_(1)) - (st_(1) - d_hyd_(1)) )  /dz_old(1)
   !f_riv_(1) = (st_(1)*(1.0 - dz_riv(jcol,icol)/dz_(1)) - (st_(1) - d_riv_(1)) )  /dz_old(1)

   !f_hyd_(1) = (st_new(1)*(dz_(1) - dz_adh(jcol,icol)) - (st_(1) - d_hyd_(1)) )  /dz_old(1)
   !f_adh_(1) = (st_new(1)*(dz_(1) - dz_adh(jcol,icol)) - (st_(1) - d_hyd_(1)) )  /dz_old(1)
   !f_riv_(1) = (st_new(1)*(dz_(1) - dz_riv(jcol,icol)) - (st_(1) - d_riv_(1)) )  /dz_old(1)

   !f_hyd_(1) = (st_new(1) - (st_(1) - d_hyd_(1)) /(dz_(1) - dz_adh(jcol,icol)) ) *(dz_(1) - dz_adh(jcol,icol)) /dz_old(1)
   !f_adh_(1) = (st_new(1) - (st_(1) - d_hyd_(1)) /(dz_(1) - dz_adh(jcol,icol)) ) *(dz_(1) - dz_adh(jcol,icol)) /dz_old(1)
   !f_riv_(1) = (st_new(1) - (st_(1) - d_riv_(1)) /(dz_(1) - dz_riv(jcol,icol)) ) *(dz_(1) - dz_riv(jcol,icol)) /dz_old(1)


! SCHON NICHT SCHLECHT
!   f_hyd_(1) = (st_new(1) - (st_(1) - d_hyd_(1)) /(dz_(1) - dz_adh(jcol,icol)) )*dz_fac(1)
!   f_adh_(1) = (st_new(1) - (st_(1) - d_hyd_(1)) /(dz_(1) - dz_adh(jcol,icol)) )*dz_fac(1)
!   f_riv_(1) = (st_new(1) - (st_(1) - d_riv_(1)) /(dz_(1) - dz_riv(jcol,icol)) )*dz_fac(1)



   ! loop over all layers
   write(99,'(''-------------------------------------------------------------------------------'')')
   !print*,dz_(1), dz_old(1)
   write(99,'("time: ",4i6,2x,2f15.10,3e20.10)') iy1, imo, ida, ihour, dz_old(1), dz_(1),dz_(1)-dz_old(1) ,& !dz(jcol, 1, icol), &
                                                 dz_vol(jcol,icol), dVol(jcol,icol)/area(jcol,icol)*dt_out/secs_per_day
   !write(99,'(a3,10a20)') 'k','st_old','st_new','diff_st','f_hyd','f_adh','f_adv','f_mxh','f_mxv','f_riv'
   !write(99,'(a3,10a20)') 'k','st_old','st_new','diff_st','f_hyd','f_adh','f_adv','f_riv','f_dil'
   write(99,'(a3,10a20)') 'k','st_old','st_new','diff_st','sum_f','f_adh','f_adv','f_riv','f_dil'
   do k = 1, k_index(jcol,icol)
      !st_old(k) = (st_(k)/dz_(k) -f_adh_(k)-f_adv_(k)-f_mxh_(k)-f_mxv_(k))  /dz_old(k)

      !st_old(k) = (st_(k) -f_adh_(k)-f_adv_(k)-f_mxh_(k)-f_mxv_(k)) /dz_(k) *dz_old(k)
      !st_old(k) = (st_(k) -f_adh_(k)-f_adv_(k)-f_mxh_(k)-f_mxv_(k)) *dz_fac(k)
      !st_old(k) = st_(k)*dz_fac(k) -f_adh_(k)-f_adv_(k)-f_mxh_(k)-f_mxv_(k)-f_riv_(k)
      !st_old(k) = st_(k) -f_adh_(k)-f_adv_(k)-f_mxh_(k)-f_mxv_(k)-f_riv_(k)
      write(99,'(i3,10e20.10)') k, st_old(k), st_(k), st_(k)-st_old(k),  &
!                              d_hyd_(k), d_adh_(k), d_adv_(k), d_mxh_(k), d_mxv_(k), d_riv_(k)
!                              f_hyd_(k), f_adh_(k), f_adv_(k), f_mxh_(k), f_mxv_(k), f_riv_(k)
                               d_hyd_(k)+d_riv_(k)-d_dil_(k), d_adh_(k), d_adv_(k), d_riv_(k), d_dil_(k)
!                              f_hyd_(k), f_adh_(k), f_adv_(k), f_riv_(k), f_dil_(k)
      write(99,'(i3,10e20.10)') k, st_old(k)/dz_old(k), st_(k)/dz_(k), st_(k)/dz_(k)-st_old(k)/dz_old(k),  &
                               f_hyd_(k)+f_riv_(k)-f_dil_(k), f_adh_(k), f_adv_(k), f_riv_(k), f_dil_(k)
   enddo

   return

   end subroutine check_flux
