!
! !MODULE: eco_meteo.f90 ---  meteorological forcing and air-sea-fluxes for ecoham
!
! !INTERFACE:
   MODULE mod_meteo
#ifdef module_meteo
!
! !DESCRIPTION:
! This module is the container for meteorological boundary conditions
! and air-sea-fluxes
!
! IO - UNITS:
!------------------------------
! wins_unit       = 130
! radday_unit     = 131
! rad_unit        = 132
! extw_unit       = 133
! prec_evap_unit  = 134
! N_depos_unit    = 135
!------------------------------
!
! !USES:
   use mod_common
   use mod_grid
#ifdef MPI
   use mod_mpi_parallel
#endif
   implicit none
!
!  default: all is private.
   private
! !PUBLIC MEMBER FUNCTIONS:
   public init_meteo, get_meteo, update_meteo, irradiance, do_meteo, close_meteo
#ifdef two_wave_band
   public irradiance_two_wave_band
#endif
!
! !PUBLIC VARIABLES:
   real,dimension(:,:),   allocatable, public :: u10    !
   real,dimension(:,:),   allocatable, public :: radsol !
   real,dimension(:,:),   allocatable, public :: radsol_mean
   real,dimension(:,:,:), allocatable, public :: par
   real,dimension(:,:,:), allocatable, public :: par_mean
   real,dimension(:,:,:), allocatable, public :: Iopt
   real,dimension(:,:,:), allocatable, public :: extw_field  ! light extinction of pure water
   real,dimension(:,:,:), allocatable, public :: exts_field  ! light extinction due to silt
   real,dimension(:,:,:), allocatable, public :: extp_field  ! light extinction due to biogeo
#ifdef two_wave_band
   real,dimension(:,:,:), allocatable, public :: extw_field_r, extw_field_g  ! light extinction of pure water
   real,dimension(:,:,:), allocatable, public :: exts_field_r, exts_field_g  ! light extinction due to silt
   real,dimension(:,:,:), allocatable, public :: extp_field_r, extp_field_g  ! light extinction due to biogeo
#endif
! !NAMELIST PARAMETERS:
   character(len=99), public :: meteo_dir     ! ='../FORCING_Jeb40/FORC/1997/'
   character(len=99), public :: meteo_suffix  ! ='.direct'
   character(len=99), public :: extw_dat      ! ='extw.dat'
   character(len=99), public :: prec_evap_dat ! ='prec_evap_in.dat'
   integer, public :: iwind          =  0     ! 1 - read wind data from file
!                                             ! 0 - const south-westwind 20 m/s
   integer, public :: windtimestep   =  6     !   - timestep of wind forcing (h)
   integer, public :: isol           =  0     ! 1 - read solar radiation from file
!                                             ! 0 - const. shortwave radiation 100 Wm-2
   integer, public :: radsoltimestep =  2     !   - timestep of solar forcing (h)
   integer, public :: iairsea        =  1     ! 1 - airsea flux of O2,CO2 and N deposition
!                                             ! 0 - no air_sea exchange
   integer, public :: air_sea_mode   =  1     ! parametrisation of gas air_sea transfer
!                                             ! 1 - Wanninkhof 1992
!                                             ! 2 - Wanninkhof & McGillis 1999
!                                             ! 3 - Nightingale et al 2000
   integer, public :: meteodilution  =  0     ! 1 - including dilution from prec-evap bilance
!                                             ! 0 - no dilution
   real,    public :: atm_n          = -1.0   ! surface input of N (mmol N/m2/d)
!                                             ! -1:atm_n.dat will be read as ann. mean loads (mmol N/m2/d) NOX,NRED
!                                             ! -2:atm_n_mon.dat will be read as clim. monthly loads (mmol N/m2/d) NOX,NRED
   real,    public :: extw           = -1.0   ! extinction coefiecient water (1/m) former 0.04
!                                             ! <0  => read extinction coeffiecient from file
   real,    public :: pafr           =  0.43  ! photosynthetic active fraction of solar radiation
   real,    public :: opt_irr        =  0.0   ! optimal light (w m-2) only necessary for P-I after Steele
!                                             ! when opt_irr=0 then light adaptation will be performed
   real,    public :: r_Iopt         =  0.25  ! relaxation (-time) for adapting Iopt to current light climate
   real,    public :: Iopt_min       = 40.0   ! -description missing-
   real,    public :: Iopt_max       = 70.0   ! -description missing-
   real,    public :: adepth_max     =  4.0   ! -description missing-
!
! !LOCAL VARIABLES:
#if defined MeteoBigEndR4
   character(len=*), parameter :: meteo_endian='BIG_ENDIAN'
   integer,          parameter :: meteo_realkind = 4   != ibyte_per_real
#else
   ! standard case used in ECOHAM5
   character(len=*), parameter :: meteo_endian='LITTLE_ENDIAN'
   integer,          parameter :: meteo_realkind = 4   != ibyte_per_real
#endif
   real(kind=meteo_realkind), dimension(iiwet)   :: input3d
   real(kind=meteo_realkind), dimension(iiwet2)  :: input2d
   real,dimension(:,:),   allocatable :: u10_new, u10_old
   real,dimension(:,:),   allocatable :: radsol_new, radsol_old
   !real,dimension(:,:),   allocatable :: pco2a
   real                               :: pco2a
   real,dimension(:,:,:), allocatable :: dIopt_dt !, dIdz_eco  (MK: not needed any moore)
   real,dimension(:,:),   allocatable :: fatm_nred, fatm_nox
   real,dimension(:,:,:), allocatable :: fatm_nred_mon, fatm_nox_mon
   real,dimension(:,:,:), allocatable :: prec_evap
   real      :: u10_t1, u10_t2, radsol_t1, radsol_t2
   integer   :: irec_wind_old, irec_radsol_old, irec_radday_old, irec_atmn_old
   real      :: wind_init   = 20.0   ! const. south-westwind (m/s)
   real      :: radsol_init = 100.0  ! const. shortwave radiation (Wm-2)
!
!=======================================================================

   contains

!=======================================================================
!
! !INTERFACE:
   subroutine init_meteo(ierr)
#define SUBROUTINE_NAME 'init_meteo'
!
! !DESCRIPTION:
!
! !USES:
   use mod_utils, only : idays_of_month
   implicit none
!
! !OUTPUT PARAMETERS:
   integer, intent(inout)  :: ierr
!
! !LOCAL VARIABLES:
   real, dimension(:,:)  , allocatable :: in_array2D
   real, dimension(:,:,:), allocatable :: in_array3D
   integer                 :: im,id,ida
   integer                 :: ios
   !integer :: i,j
!
   namelist /meteo_nml/meteo_dir, meteo_suffix, extw_dat, prec_evap_dat,    &
                      wind_init, windtimestep, radsol_init, radsoltimestep, &
                      iairsea, air_sea_mode, meteodilution, atm_n,          &
                      extw, pafr, opt_irr, r_Iopt, Iopt_min,                &
                      Iopt_max, adepth_max
!
!-----------------------------------------------------------------------
#include "call-trace.inc"

   id = 0  ! to get rid of "unused" warning

   if (myID == master) call write_log_message(" INITIALIZING meteo forcing")

#ifdef two_wave_band
   if (myID == master) then
      write( long_msg, '(3x,a)') '- calc irradiance based on red and green bands'
      call write_log_message(long_msg); long_msg = ''
   endif ! master
#endif

! #if !defined module_restoring && defined exclude_restoring_cells_meteo
!    error_message = 'ERROR: You may not specify -Dexclude_restoring_cells_meteo' &
!                   //' without Flag -Dmodule_restoring!'
!    call stop_ecoham(ierr, msg=error_message)
! #endif

   ! open & read meteo settings
   !--------------------------------------------------------------
   open(nml_unit,file=trim(settings_file),action='read',status='old')
   read(nml_unit,nml=meteo_nml)
   close(nml_unit)
   
   if (wind_init   < 0) iwind = 1
   if (radsol_init < 0) isol  = 1

   ! calculate number of days for current year
   im = 12 !! MK: idays_of_month(-,X,-,-) is intend(inout) !!
   call idays_of_month(year,im,ida,31)

   ! allocate arrays defined on each domain
   allocate(   par_mean( jmax,kmax,iStartD:iEndD) ) ; par_mean = 0.
   allocate(        par( jmax,kmax,iStartD:iEndD) ) ; par = 0.
   allocate(       Iopt( jmax,kmax,iStartD:iEndD) ) ; Iopt = Iopt_min
   allocate(   dIopt_dt( jmax,kmax,iStartD:iEndD) ) ; dIopt_dt = 0.
   !allocate(   dIdz_eco( jmax,kmax,iStartD:iEndD) ) ; dIdz_eco = 0.  (MK: not needed any moore)
   !allocate(     help2D( jmax,     iStartD:iEndD) )
   !allocate(      pco2a( jmax,     iStartD:iEndD) )
   ! allocate arrays defined on full domain
   allocate( in_array2D( jmax,     imax) )
   allocate( in_array3D( jmax,kmax,imax) )

! ============== WIND ==============
   allocate( u10( jmax, iStartD:iEndD) ); u10 = ZERO
   irec_wind_old = 0
   if (iwind == 1) then
      allocate( u10_new( jmax, iStartD:iEndD) ); u10_new = fail
      allocate( u10_old( jmax, iStartD:iEndD) ); u10_old = fail
      filename = trim(meteo_dir)//'wins'//trim(meteo_suffix)
      open(wins_unit, file=trim(filename), status='old', &
      &    form='unformatted', access='direct',          &
      &    recl=iiwet2*meteo_realkind,                   &
      &    convert=trim(meteo_endian), err = 9000)
      if (myID == master) then
         fmtstr = '(3x,"windspeed",11x,"(unit=",i3.0,") from file: ",a)'
         write(long_msg,fmtstr) wins_unit, trim(filename)
         call write_log_message(long_msg); long_msg = ''
      endif
   else
      if (myID == master) then
         write( long_msg, '(3x,a9,11x," set constant: ",a," = ",f4.1,a)') &
         &     'windspeed', 'u10', wind_init,' ms-1'
         call write_log_message(long_msg); long_msg = ''
      endif ! master
      u10 = wind_init
   endif

! ============== RADIATION ==============
   allocate(radsol_mean( jmax, iStartD:iEndD) ); radsol_mean = ZERO
   allocate(     radsol( jmax, iStartD:iEndD) ); radsol      = ZERO
   irec_radday_old = 0
   irec_radsol_old = 0
   if (isol == 1) then
      allocate( radsol_new( jmax, iStartD:iEndD) ); radsol_new = fail
      allocate( radsol_old( jmax, iStartD:iEndD) ); radsol_old = fail
      ! if(abs(opt_irr).lt.1e-4)then
      filename = trim(meteo_dir)//'radday'//trim(meteo_suffix)
      open(radday_unit, file = trim(filename), status='old',   &
      &    form='unformatted', access='direct',                &
      &    recl=iiwet2*meteo_realkind,                         &
      &    convert=trim(meteo_endian), err = 9000)
      if (myID == master) then
         fmtstr = '(3x,"radday",14x,"(unit=",i3.0,") from file: ",a)'
         write(long_msg,fmtstr) radday_unit, trim(filename)
         call write_log_message(long_msg); long_msg = ''
      endif
      ! endif

      filename = trim(meteo_dir)//'rad'//trim(meteo_suffix)
      open(rad_unit, file = trim(filename), status='old',      &
      &    form='unformatted', access='direct',                &
      &    recl=iiwet2*meteo_realkind,                         &
      &    convert=trim(meteo_endian), err = 9000)
      if (myID == master) then
         fmtstr = '(3x,"rad",17x,"(unit=",i3.0,") from file: ",a)'
         write(long_msg,fmtstr) rad_unit, trim(filename)
         call write_log_message(long_msg); long_msg = ''
      endif
   else
      if (myID == master) then
         write( long_msg, '(3x,a19,1x," set constant: ",a," = ",f6.1,a)') &
         &     'shortwave radiation', 'radsol', radsol_init,' Wm-2'
         call write_log_message(long_msg); long_msg = ''
      endif ! master
      radsol_mean = radsol_init
      radsol      = radsol_init
   endif

! ============== LIGHT EXTINCTION ==============
#ifdef two_wave_band
   allocate( extp_field_r( jmax,kmax,iStartD:iEndD) )
   extp_field_r = fail
   where (ixistz==1) extp_field_r = zero

   allocate( exts_field_r( jmax,kmax,iStartD:iEndD) )
   exts_field_r = fail
   where (ixistz==1) exts_field_r = zero

   allocate( extw_field_r( jmax,kmax,iStartD:iEndD) )
   extw_field_r = fail

   allocate( extp_field_g( jmax,kmax,iStartD:iEndD) )
   extp_field_g = fail
   where (ixistz==1) extp_field_g = zero

   allocate( exts_field_g( jmax,kmax,iStartD:iEndD) )
   exts_field_g = fail
   where (ixistz==1) exts_field_g = zero

   allocate( extw_field_g( jmax,kmax,iStartD:iEndD) )
   extw_field_g = fail
#endif

   allocate( extp_field( jmax,kmax,iStartD:iEndD) )
   extp_field = fail
   where (ixistz==1) extp_field = zero

   allocate( exts_field( jmax,kmax,iStartD:iEndD) )
   exts_field = fail
   where (ixistz==1) exts_field = zero

   allocate( extw_field( jmax,kmax,iStartD:iEndD) )
   extw_field = fail


   if (extw < 0.0) then
      filename = trim(extw_dat)
      open(extw_unit,file=trim(filename), status='old',        &
               form = 'unformatted', access = 'direct',        &
               recl = iiwet * meteo_realkind,                  &
               convert = trim(meteo_endian), err = 9000)
      if (myID == master) then
         fmtstr = '(3x,"light extinction",4x,"(unit=",i3.0,") from file: ",a)'
         write(long_msg,fmtstr) extw_unit, trim(filename)
         call write_log_message(long_msg); long_msg = ''
      endif
      read(extw_unit, rec=1, iostat=ios) input3d
      in_array3D(:,:,:)=unpack(real(input3d),ixistz_master(:,:,:)==1,fail)
      if (ios /= 0) then
         ierr = 1
         write( long_msg, '("ERROR reading light extinction iostat=",i4)') ios
         call write_log_message(long_msg); long_msg = ''
      endif
      close(extw_unit)
      extw_field(:,:,iStartD:iEndD) = in_array3D(:,:,iStartD:iEndD)
   else
      if (myID == master) then
         write( long_msg, '(3x,a16,4x," set constant: ",a," = ",f6.1,a)') &
         &     'light extinction', 'extw', extw,' (UNIT?)'
         call write_log_message(long_msg); long_msg = ''
      endif
      where (ixistz==1) extw_field = extw
   endif

!--------------------------------------
! ! output extw.dump for debugging
!--------------------------------------
! if (myID == master) then
!    filename = 'extw_new.dump'
!    open(146,file = trim(filename), form='formatted', status='replace')
!    write(fmtstr,'(''('',i3,''e20.10)'')') jmax
!    do i=1,imax
!       write(146,fmtstr) (extw_field(j,1,i),j=1,jmax)
!    enddo
!    close(146)
!    call stop_ecoham(1, msg='meteo#290')
! end if ! master
!--------------------------------------

! ============== NITROGEN-DEPOSITION ==============
   if (atm_n /= 0.0) then
      allocate(  fatm_nred( jmax,     iStartD:iEndD) ); fatm_nred = fail
      allocate(   fatm_nox( jmax,     iStartD:iEndD) ); fatm_nox  = fail
      irec_atmn_old = 0

      if (atm_n < 0.0) then ! data from file
         if (atm_n == -1.0) then ! annual data
            filename = trim(meteo_dir)//'atm_n.dat'
         elseif (atm_n == -2.0) then ! monthly
            filename = trim(meteo_dir)//'atm_n_mon.dat'
         endif
         open(N_depos_unit,file=trim(filename), status='old', err=9000)
         if (myID == master) then
            fmtstr = '(3x,"atm. N-deposition",3x,"(unit=",i3.0,") from file: ",a)'
            write(long_msg,fmtstr) N_depos_unit, trim(filename)
            call write_log_message(long_msg); long_msg = ''
         endif

      elseif (atm_n > 0.0) then ! set constant value
         if (myID == master) then
            write( long_msg, '(3x,a12,8x," set constant: ",a," = ",f4.1,a)') &
            &     'N-deposition', 'atm_N', atm_n,' mmol m-2'
            call write_log_message(long_msg); long_msg = ''
         endif ! master
         fatm_nox  = atm_n/2.
         fatm_nred = atm_n/2.
      endif
   endif

! ============== PRECIPITATION-EVAPORATION ==============
   if (meteodilution == 1) then
      allocate(  prec_evap( jmax,     iStartD:iEndD,367) ); prec_evap = fail
      filename = trim(prec_evap_dat)
      open(prec_evap_unit,file=filename, status='old', err=9000)
      if (myID == master) then
         fmtstr = '(3x,"meteodilution",7x,"(unit=",i3.0,") from file: ",a)'
         write(long_msg,fmtstr) prec_evap_unit, trim(filename)
         call write_log_message(long_msg); long_msg = ''
      endif
      do id = 1, ida
         read(prec_evap_unit,*) input2d
         in_array2D(:,:) = unpack(real(input2d),ixistz_master(:,1,:)==1,fail)
         prec_evap(:,iStartD:iEndD,id) = in_array2D(:,iStartD:iEndD)
      enddo
      close( prec_evap_unit )
   else
      if (myID == master) then
         write( long_msg, '(3x,"meteodilution",7x," WILL NOT BE APPLIED")')
         call write_log_message(long_msg); long_msg = ''
      endif ! master
   endif

   deallocate( in_array2D)
   deallocate( in_array3D)

   if (ierr/=0) then
      write( error_message, '("[",i2.2,"] ERROR - init_meteo: ",i3)') myID, ierr
      call stop_ecoham(ierr, msg=error_message)
   end if
   return

9000 continue
   ierr = 1
   write( error_message,'("METEO: open error : ",a)') trim(filename)
   call stop_ecoham(ierr, msg=error_message)

   end subroutine init_meteo
!
!=======================================================================
!
! !INTERFACE:
   subroutine get_meteo(td_,ierr)
#define SUBROUTINE_NAME 'get_meteo'
!
! !DESCRIPTION:
! subroutine provides arrays for meteorological forcing
!
! !USES:
   implicit none
!
! !INPUT PARAMETERS:
   real(kind=8), intent(in)     :: td_
! !OUTPUT PARAMETERS:
   integer,      intent(inout)  :: ierr
! !LOCAL VARIABLES:
!
!-----------------------------------------------------------------------
#include "call-trace.inc"

! ============== WIND ==============
   if (iwind==1) then
      call read_wind(td_,ierr)
   endif

! ============== RADIATION ==============
   if (isol==1) then
      call read_radsol(td_,ierr)
      call read_radday(td_,ierr)
   endif

! ======== atmospheric pCO2 ==========
   if (iairsea==1) then
      call airpco2(td_,ierr)
   endif

! ============== N-deposition ==============
   if (atm_n /= 0.0 .and. irec_atmn_old <= 0 ) then
      call read_atm_n(td_,ierr)
   endif

   if (ierr/=0) then
      write( error_message, '("[",i2.2,"] ERROR - get_meteo: ",i3)') myID, ierr
      call stop_ecoham(ierr, msg=error_message)
   end if
   return

   end subroutine get_meteo
!
!=======================================================================
!
! !INTERFACE:
   subroutine update_meteo(td_,ierr)
#define SUBROUTINE_NAME 'update_meteo'
!
! !DESCRIPTION:
! subroutine updates meteorological forcing interpolated to current time
!
! !USES:
   use mod_utils, only : idays_of_month
   implicit none
!
! !INPUT PARAMETERS:
   real,         intent(in)     :: td_
! !OUTPUT PARAMETERS:
   integer,      intent(inout)  :: ierr
! !LOCAL VARIABLES:
   real                    :: alpha, beta
   integer                 :: iyr, imon, ida
!
!-----------------------------------------------------------------------
#include "call-trace.inc"

   call get_meteo(td_,ierr)

! ============== WIND ==============
   if (iwind==1) then
      alpha = (td_-u10_t1)/(u10_t2-u10_t1)
      beta  = 1.-alpha
      u10 = alpha * u10_new + beta * u10_old  !(m/s)
   endif

! ============== RADIATION ==============
   if (isol==1) then
      alpha = (td_-radsol_t1)/(radsol_t2-radsol_t1)
      beta  = 1.-alpha
      radsol = alpha * radsol_new + beta * radsol_old
   endif

! ============== RADDAY ==============
  ! no temporal interpolation

! ====== UNDERWATER LIGHT (PAR) =========
!   call  irradiance(ierr)

! ============== NITROGEN-DEPOSITION ==============
   if (atm_n == -2.0) then ! monthly data
      iyr = year
      ida = int(td_)
      call idays_of_month(iyr,imon,ida,-1)
      if (imon /= irec_atmn_old) then ! monthly data
         fatm_nred = fatm_nred_mon(:,:,imon)
         fatm_nox  = fatm_nox_mon(:,:,imon)
         irec_atmn_old = imon
      endif
   endif

   if (ierr/=0) then
      write( error_message, '("[",i2.2,"] ERROR - update_meteo: ",i3)') myID, ierr
      call stop_ecoham(ierr, msg=error_message)
   end if
   return

   end subroutine update_meteo
!
!=======================================================================
!
! !INTERFACE:
   subroutine read_atm_n(td_,ierr)
#define SUBROUTINE_NAME 'read_atm_n'
!
! !DESCRIPTION:
! subroutine reads new values of windspeed
!
! !USES:
!    use mod_utils, only : idays_of_month
   implicit none
!
! !INPUT PARAMETERS:
   real(kind=8), intent(in)     :: td_
! !OUTPUT PARAMETERS:
   integer,      intent(inout)  :: ierr
! !LOCAL VARIABLES:
   real, dimension(jmax,imax)   :: in_array2D
   integer                      :: irec, ios
   integer                      :: ida!, imon, iyr
   !integer :: i,j
!
!-----------------------------------------------------------------------
#include "call-trace.inc"

   ida = int(td_) ! to get rid of warning 'Unused dummy argument ...'
!    iyr = year
!    call idays_of_month(iyr,imon,ida,-1)

   if (atm_n == -1.0) then ! annual data
!       if (iyr /= irec_atmn_old) then ! read data
      read(N_depos_unit,*,iostat=ios) input2d
      if (ios /= 0) then
         ierr = 1
         write( long_msg, '("ERROR reading atm_nox iostat=",i4)') ios
         call write_log_message(long_msg); long_msg = ''
      endif
      in_array2D(:,:) = unpack(real(input2d),ixistz_master(:,1,:)==1,fail)
      fatm_nox(:,iStartD:iEndD)  = in_array2D(:,iStartD:iEndD)
      read(N_depos_unit,*,iostat=ios) input2d
      if (ios /= 0) then
         ierr = 1
         write( long_msg, '("ERROR reading atm_nred iostat=",i4)') ios
         call write_log_message(long_msg); long_msg = ''
      endif
      in_array2D(:,:) = unpack(real(input2d),ixistz_master(:,1,:)==1,fail)
      fatm_nred(:,iStartD:iEndD) = in_array2D(:,iStartD:iEndD)
      close(N_depos_unit)
      irec_atmn_old = 1
!       endif

!--------------------------------------
! ! output atmN.dump for debugging
!--------------------------------------
! if (myID == master) then
!    filename = 'atmN_new.dump'
!    open(146,file = trim(filename), form='formatted', status='replace')
!    write(fmtstr,'(''('',i3,''e20.10)'')') jmax
!    do i=1,imax
!       write(146,fmtstr) (fatm_nox(j,i),j=1,jmax)
!    enddo
!    close(146)
!    call stop_ecoham(1, msg='meteo#324')
! end if ! master
!--------------------------------------

   elseif (atm_n == -2.0) then ! 2D monthly data set
      allocate( fatm_nred_mon( jmax,  iStartD:iEndD,12) ); fatm_nred_mon = fail
      allocate(  fatm_nox_mon( jmax,  iStartD:iEndD,12) ); fatm_nox_mon  = fail

!       if (imon /= irec_atmn_old) then ! read data
!          if (irec_atmn_old == 0) then
      do irec = 1, 12
         read(N_depos_unit,*,iostat=ios)input2d
         if (ios /= 0) then
            ierr = 1
            write( long_msg, '("ERROR reading atm_nox_mon iostat=",i4)') ios
            call write_log_message(long_msg); long_msg = ''
         endif
         in_array2D(:,:) = unpack(real(input2d),ixistz_master(:,1,:)==1,fail)
         fatm_nox_mon(:,iStartD:iEndD,irec) = in_array2D(:,iStartD:iEndD)
         read(N_depos_unit,*,iostat=ios)input2d
         if (ios /= 0) then
            ierr = 1
            write( long_msg, '("ERROR reading atm_nred_mon iostat=",i4)') ios
            call write_log_message(long_msg); long_msg = ''
         endif
         in_array2D(:,:) = unpack(real(input2d),ixistz_master(:,1,:)==1,fail)
         fatm_nred_mon(:,iStartD:iEndD,irec) = in_array2D(:,iStartD:iEndD)
         irec_atmn_old = irec
      enddo
      close(N_depos_unit)
!          endif
!        endif

!--------------------------------------
! ! output atmN_mon.dump for debugging
!--------------------------------------
! if (myID == master) then
!    filename = 'atmN_mon_new.dump'
!    open(146,file = trim(filename), form='formatted', status='replace')
!    write(fmtstr,'(''('',i3,''e20.10)'')') jmax
!    do i=1,imax
!       write(146,fmtstr) (fatm_nox_mon(j,i,1),j=1,jmax)
!    enddo
!    close(146)
!    call stop_ecoham(1, msg='meteo#371')
! end if ! master
!--------------------------------------

   endif

   if (ierr/=0) then
      write( error_message, '("[",i2.2,"] ERROR - read_atm_n: ",i3)') myID,ierr
      call stop_ecoham(ierr, msg=error_message)
   end if
   return

   end subroutine read_atm_n
!
!=======================================================================
!
! !INTERFACE:
   subroutine read_wind(td_,ierr)
#define SUBROUTINE_NAME 'read_wind'
!
! !DESCRIPTION:
! subroutine reads new values of windspeed
!
! !USES:
   implicit none
!
! !INPUT PARAMETERS:
   real(kind=8), intent(in)     :: td_
! !OUTPUT PARAMETERS:
   integer,      intent(inout)  :: ierr
! !LOCAL VARIABLES:
   real, dimension(jmax,imax)   :: in_array2D
   integer                      :: irec1, irec2, ios
   !integer :: i,j
!
!-----------------------------------------------------------------------
#include "call-trace.inc"

   irec1 = int((24./real(windtimestep))*td_)+1
   !irec1 = min(irec1,1460) ! would be obsolete, if forcing was properly done
   irec2 = min(int(24/windtimestep*365),irec1+1)
   if (irec1/=irec_wind_old) then
      u10_t1 = (irec1-1) *windtimestep/24.
      u10_t2 = u10_t1 + windtimestep/24.
      !if (myID==master) print*,'read wind forcing for day ',td_,irec1,irec2
      read (wins_unit, rec = irec1, iostat = ios) input2d
      in_array2D(:,:) = unpack(real(input2d),ixistz_master(:,1,:)==1,fail)
      u10_old(:,iStartD:iEndD) = in_array2D(:,iStartD:iEndD)
      read (wins_unit, rec = irec2, iostat = ios) input2d
      in_array2D(:,:) = unpack(real(input2d),ixistz_master(:,1,:)==1,fail)
      u10_new(:,iStartD:iEndD) = in_array2D(:,iStartD:iEndD)

!--------------------------------------
! ! output u10.dump for debugging
!--------------------------------------
! if (myID == master) then
!    filename = 'u10_new.dump'
!    open(146,file = trim(filename), form='formatted', status='replace')
!    write(fmtstr,'(''('',i3,''e20.10)'')') jmax
!    do i=1,imax
!       write(146,fmtstr) (u10_new(j,i),j=1,jmax)
!    enddo
!    close(146)
!    call stop_ecoham(1, msg='meteo#561')
! end if ! master
!--------------------------------------

      if (ios /= 0) then
         write(long_msg,'("[",i2.2,"] ERROR reading U10 for day ",f8.3)') myID, td_
         call write_log_message(long_msg); long_msg = ''
         !print*,td_,irec1,irec2,ios
         if (ios == 5002) then ! reached end-of-file
            write(long_msg,'("U10 is set from record entries ",2i6)') irec1,irec2
            call write_log_message(long_msg); long_msg = ''
         endif
      endif

      irec_wind_old = irec1
   endif

   if (ierr/=0) then
      write( error_message, '("[",i2.2,"] ERROR - read_wind: ",i3)') myID,ierr
      call stop_ecoham(ierr, msg=error_message)
   end if
   return

   end subroutine read_wind
!
!=======================================================================
!
! !INTERFACE:
   subroutine read_radsol(td_,ierr)
#define SUBROUTINE_NAME 'read_radsol'
!
! !DESCRIPTION:
! subroutine reads new values of short wave radiation
!
! !USES:
   implicit none
!
! !INPUT PARAMETERS:
   real(kind=8), intent(in)     :: td_
! !OUTPUT PARAMETERS:
   integer,      intent(inout)  :: ierr
! !LOCAL VARIABLES:
   real, dimension(jmax,imax)   :: in_array2D
   integer                :: irec1, irec2, ios
   character(len=99)      :: str
!    integer :: i,j
!
!-----------------------------------------------------------------------
#include "call-trace.inc"

   irec1=int((24./real(radsoltimestep))*td_)+1
   irec2=irec1+1
   if(irec1/=irec_radsol_old)then
      !if (myID==master) print*,'read wind radsol for day ',td_
      radsol_t1 = (irec1-1) *radsoltimestep/24.
      radsol_t2 = radsol_t1 + radsoltimestep/24.
      read (rad_unit, rec = irec1, iostat = ios) input2d
      in_array2D(:,:) = unpack(real(input2d),ixistz_master(:,1,:)==1,fail)
      radsol_old(:,iStartD:iEndD) = in_array2D(:,iStartD:iEndD)
      read (rad_unit, rec = irec2, iostat = ios) input2d
      in_array2D(:,:) = unpack(real(input2d),ixistz_master(:,1,:)==1,fail)
      radsol_new(:,iStartD:iEndD) = in_array2D(:,iStartD:iEndD)

!--------------------------------------
! ! output radsol.dump for debugging
!--------------------------------------
! if (myID == master) then
!    filename = 'radsol_new.dump'
!    open(146,file = trim(filename), form='formatted', status='replace')
!    write(fmtstr,'(''('',i3,''e20.10)'')') jmax
!    do i=1,imax
!       write(146,fmtstr) (radsol_new(j,i),j=1,jmax)
!    enddo
!    close(146)
! !  call stop_ecoham(1, msg='meteo#635')
! end if ! master
!--------------------------------------

      if (ios /= 0) then
         write(long_msg,'("[",i2.2,"] ERROR reading RAD for day ",f8.3)') myID,td_
         !print*,td_,irec1,irec2,ios
         if (ios == 5002) then ! reached end-of-file
            write(str,'(" -- RAD is set from record entries ",2i6)') irec1,irec2
            long_msg = trim(long_msg)//trim(str)
         endif
         call write_log_message(long_msg); long_msg = ''
      endif

      irec_radsol_old = irec1
   endif

   if (ierr/=0) then
      write( error_message, '("[",i2.2,"] ERROR - read_radsol: ",i3)') myID,ierr
      call stop_ecoham(ierr, msg=error_message)
   end if
   return

   end subroutine read_radsol
!
!=======================================================================
!
! !INTERFACE:
   subroutine read_radday(td_,ierr)
#define SUBROUTINE_NAME 'read_radday'
!
! !DESCRIPTION:
! subroutine reads daily integrated short wave radiation
!
! !USES:
   implicit none
!
! !INPUT PARAMETERS:
   real(kind=8), intent(in)     :: td_
! !OUTPUT PARAMETERS:
   integer,      intent(inout)  :: ierr
! !LOCAL VARIABLES:
   real, dimension(jmax,imax)   :: in_array2D
   integer                      :: irec1, ios
   integer                      :: hstep
   character(len=99)      :: str
!    integer :: i,j
!
!-----------------------------------------------------------------------
#include "call-trace.inc"

   hstep = 24
   irec1 = int((24./real(hstep))*td_)+1
   if (irec1/=irec_radday_old) then
      !if (myID==master) print*,'read radday forcing for day ',td_
      read (radday_unit, rec = irec1, iostat = ios) input2d
      in_array2D(:,:) = unpack(real(input2d),ixistz_master(:,1,:)==1,fail)
      radsol_mean(:,iStartD:iEndD) = in_array2D(:,iStartD:iEndD)

!--------------------------------------
! ! output radsol.dump for debugging
!--------------------------------------
! if (myID == master) then
! #ifdef MPI
!    write(filename,'("radday_",i3.3,".dump.",i2.2)') irec1,myID
! #else
!    write(filename,'("radday_",i3.3,".dump")') irec1
! #endif
!    open(146,file = trim(filename), form='formatted', status='replace')
!    write(fmtstr,'(''('',i3,''e20.10)'')') jmax
!    do i=iStartD,iEndD
!       write(146,fmtstr) (radsol_mean(j,i),j=1,jmax)
!    enddo
!    close(146)
!   call stop_ecoham(1, msg='meteo#709')
! end if ! master
!--------------------------------------

      if (ios /= 0) then
         write(long_msg,'("[",i2.2,"] ERROR reading RADDAY for day ",f8.3)') myID, td_
         !print*,td_,irec1,irec2,ios
         if (ios == 5002) then ! reached end-of-file
            write(str,'(" -- RADDAY is set from record entry ",2i6)') irec1
            long_msg = trim(long_msg)//trim(str)
         endif
         call write_log_message(long_msg); long_msg = ''
      endif

      irec_radday_old = irec1
   endif

   if (ierr/=0) then
      write( error_message, '("[",i2.2,"] ERROR - read_radday: ",i3)') myID, ierr
      call stop_ecoham(ierr, msg=error_message)
   end if
   return

   end subroutine read_radday
!
!=======================================================================
!
!INTERFACE:
   subroutine irradiance(ierr)
#define SUBROUTINE_NAME 'irradiance'
!
! !DESCRIPTION:
! compute vertical profile of available PAR
!
! !USES:
   use mod_flux
   use mod_var
   implicit none
!
! !OUTPUT PARAMETERS:
   integer, intent(inout)  :: ierr
!
! !LOCAL VARIABLES:
   integer       :: i,j,k,k0,k1
   real          :: eps, dep, adepth, Iavg_adepth
   !real          :: I_up,I_low  (MK: not needed any more) 
   real          :: xeps, xeps1=0., turb_adepth=0.
   real          :: turb_center, turb_up, turb_low
!
!-----------------------------------------------------------------------
#include "call-trace.inc"

   eps  = 1.0e-6

   ! P-I after Steele at the center of the layer
   do i = iStartCalc, iEndCalc
      do j = jStartCalc, jEndCalc
         k0 = k_index(j,i)
         if (k0>0 .and. dz(j,1,i)<0.0) then
            ierr = 1
            write( error_message, '("SUBROUTINE IRRADIANCE: dz < 0",2i4)') i,j
            call stop_ecoham(ierr, msg=error_message)
         endif
         dep         = zero
         !I_low       = radsol(j,i)
         turb_up     = zero
         turb_center = zero
         turb_low    = zero
         k1 = min(1,k0)
         do k = 1, k0
            dep = dep + 0.5*dz(j,k,i) ! depth at center of current layer
            xeps = extw_field(j,k,i)+exts_field(j,k,i)+extp_field(j,k,i)
            turb_up     = turb_low
            turb_center = turb_up + xeps*dz(j,k,i)*0.5
            turb_low    = turb_up + xeps*dz(j,k,i)
            par(j,k,i)  = pafr*radsol(j,i)*exp(-turb_center)
            !CL ! calculate mean daily PAR using mean surface radiation and current turbidity
            !par_mean(j,k,i) = pafr*radsol_mean(j,i)*exp(-turb_center)
            par_mean(j,k,i) = 0.4*radsol_mean(j,i)*exp(-turb_center) ! *0.4=factor for wm-1 to mol quanta m-2 d-1

            ! prepare light for HAMSOM  (MK: not needed any more)
            !I_up    = I_low
            !I_low   = radsol(j,i)*exp(-turb_low)
            !dIdz_eco(j,k,i) = (I_up - I_low)/dz(j,k,i)

            ! calculate light adaption
            adepth  = min(adepth_max,dep)
            ! ---- MK version ----
            !if (adepth .le. adepth_max)then
            !   turb_adepth = turb_up + xeps*min(dz(j,k,i)*0.5,adepthmax-adepth)
            !endif
            ! ---- jp revision ----
            if (k.eq.1) xeps1 = xeps
            turb_adepth = xeps1*adepth
            ! --------------------
            Iavg_adepth = pafr*radsol_mean(j,i)*exp(-turb_adepth)
            dIopt_dt(j,k,i) = r_Iopt*(min(Iopt_max,max(Iopt_min,Iavg_adepth))-Iopt(j,k,i))
            dep = dep + 0.5*dz(j,k,i)  ! depth at bottom of current layer

! write(*,'(''irradiance #935 (rad, dIopt_dt, Iavg_adepth)'',6e20.10)')  radsol_mean(j,i), &
!              dIopt_dt(j,k,i),Iavg_adepth, Iopt_min, Iopt_max ,Iopt(j,k,i)

!           if(i==14.and.j==32.and.k==1)then
!              write( long_msg, '(6f8.2," jp2")') diopt_dt(j,k,i),r_iopt,iopt_max,iopt_min,iavg_adepth,iopt(j,k,i)
!              call write_log_message(long_msg); long_msg = ''
!           endif

         enddo
      enddo
   enddo

   if (ierr/=0) then
      write( error_message, '("[",i2.2,"] ERROR - irradiance: ",i3)') myID, ierr
      call stop_ecoham(ierr, msg=error_message)
   end if
   return

   end subroutine irradiance
!
!=======================================================================
#ifdef two_wave_band
!
!INTERFACE:
   subroutine irradiance_two_wave_band(ierr)
#define SUBROUTINE_NAME 'irradiance_two_wave_band'
!
! !DESCRIPTION:
! compute vertical profile of available PAR
! two_wave_band light attenuation adapted by lff
!
! !USES:
   use mod_flux
   use mod_var
   implicit none
!
! !OUTPUT PARAMETERS:
   integer, intent(inout)  :: ierr
!
! !LOCAL VARIABLES:
   integer       :: i,j,k,k0,k1
   real          :: eps,dep,adepth,Iavg_adepth
   !real          :: I_up,I_low  (MK: not needed any more) 
   real          :: xeps_g,xeps1_g=0.,turb_adepth_g=0.
   real          :: xeps_r,xeps1_r=0.,turb_adepth_r=0.
   real          :: turb_center_g,turb_up_g,turb_low_g
   real          :: turb_center_r,turb_up_r,turb_low_r
!
!-----------------------------------------------------------------------
#include "call-trace.inc"

   eps  = 1.0e-6

   extw_field_g = 0.058 !extw_field_g(j,k,i)=0.058 Tian 2009
   extw_field_r = 0.8   !extw_field_r(j,k,i)=0.4 Tian 2009

   ! P-I after Steele at the center of the layer
   do i = iStartCalc, iEndCalc
      do j = jStartCalc, jEndCalc
         k0 = k_index(j,i)
         if (k0>0 .and. dz(j,1,i)<0.0) then
            ierr = 1
            write( error_message, '("SUBROUTINE IRRADIANCE_TWO_WAVE_BAND: dz < 0",2i4)') i,j
            call stop_ecoham(ierr, msg=error_message)
         endif
         dep         = zero
         I_low       = radsol(j,i)
         turb_up_g     = zero
         turb_center_g = zero
         turb_low_g    = zero
         turb_up_r     = zero
         turb_center_r = zero
         turb_low_r    = zero
         k1 = min(1,k0)
         do k = 1, k0
            dep = dep + 0.5*dz(j,k,i) ! depth at center of current layer
            xeps_g = extw_field_g(j,k,i)+exts_field(j,k,i)+extp_field(j,k,i)
            xeps_r = extw_field_r(j,k,i)+exts_field(j,k,i)+extp_field(j,k,i)
            turb_up_g     = turb_low_g
            turb_center_g = turb_up_g + xeps_g*dz(j,k,i)*0.5
            turb_low_g    = turb_up_g + xeps_g*dz(j,k,i)
            turb_up_r     = turb_low_r
            turb_center_r = turb_up_r + xeps_r*dz(j,k,i)*0.5
            turb_low_r    = turb_up_r + xeps_r*dz(j,k,i)
            par(j,k,i)  = 0.5* pafr*radsol(j,i)*(exp(-turb_center_g) + exp(-turb_center_r))
            !CL ! calculate mean daily PAR using mean surface radiation and current turbidity
            !par_mean(j,k,i) = 0.5*pafr*radsol_mean(j,i)*(exp(-turb_center_g) + exp(-turb_center_r))
            par_mean(j,k,i) = 0.5*0.4*radsol_mean(j,i)*(exp(-turb_center_g) + exp(-turb_center_r)) ! *0.4=factor for wm-1 to mol quanta m-2 d-1

            ! prepare light for HAMSOM  (MK: not needed any more)
            !I_up    = I_low
            !I_low   = 0.5*radsol(j,i)*(exp(-turb_low_g) + exp(-turb_low_r))
            !dIdz_eco(j,k,i) = (I_up - I_low)/dz(j,k,i)

            ! calculate light adaption
            adepth  = min(adepth_max,dep)
            ! ---- MK version ----
            !if (adepth .le. adepth_max)then
            !   turb_adepth = turb_up + xeps*min(dz(j,k,i)*0.5,adepthmax-adepth)
            !endif
            ! ---- jp revision ----
            if (k.eq.1) then
               xeps1_g = xeps_g
               xeps1_r = xeps_r
            endif
            turb_adepth_g = xeps1_g*adepth
            turb_adepth_r = xeps1_r*adepth
            ! --------------------
            Iavg_adepth = 0.5*pafr*radsol_mean(j,i)*(exp(-turb_adepth_g)+exp(-turb_adepth_r))
            dIopt_dt(j,k,i) = r_Iopt*(min(Iopt_max,max(Iopt_min,Iavg_adepth))-Iopt(j,k,i))
            dep = dep + 0.5*dz(j,k,i)  ! depth at bottom of current layer

!           if(i==14.and.j==32.and.k==1)then
!              write( long_msg, '(6f8.2," jp2")') diopt_dt(j,k,i),r_iopt,iopt_max,iopt_min,iavg_adepth,iopt(j,k,i)
!              call write_log_message(long_msg); long_msg = ''
!           endif

         enddo
      enddo
   enddo

   if (ierr/=0) then
      write( error_message, '("[",i2.2,"] ERROR - irradiance_two_wave_band: ",i3)') myID, ierr
      call stop_ecoham(ierr, msg=error_message)
   end if
   return

   end subroutine irradiance_two_wave_band
#endif
!
!-----------------------------------------------------------------------
!
! !INTERFACE:
   subroutine airpco2(td_,ierr)
#define SUBROUTINE_NAME 'airpco2'
!
! !DESCRIPTION:
! subroutine determines time and lat dependend pco2(air)
! following Drange's PhD
!
! !USES:
   implicit none
!
! !INPUT PARAMETERS:
   real,    intent(in)     :: td_
! !OUTPUT PARAMETERS:
   integer, intent(inout)  :: ierr
! !LOCAL VARIABLES:
   !integer       :: i, j
   integer       :: itd
   real          :: pco2a_ns(12), off
!
!-----------------------------------------------------------------------
#include "call-trace.inc"

#ifdef cskc
   !values for Indo-Pacific low lat (commented is North Sea values)
   pco2a_ns(1)  = 360.4 ! 359.
   pco2a_ns(2)  = 360.6 ! 360.
   pco2a_ns(3)  = 360.8 ! 361.
   pco2a_ns(4)  = 361.0 ! 362.
   pco2a_ns(5)  = 360.6 ! 360.
   pco2a_ns(6)  = 360.0 ! 357.
   pco2a_ns(7)  = 358.8 ! 351.
   pco2a_ns(8)  = 358.0 ! 347.
   pco2a_ns(9)  = 358.0 ! 347.
   pco2a_ns(10) = 359.0 ! 352.
   pco2a_ns(11) = 360.0 ! 357.
   pco2a_ns(12) = 360.6 ! 360.
#else
   !North Sea values
   pco2a_ns(1)  = 359.0
   pco2a_ns(2)  = 360.0
   pco2a_ns(3)  = 361.0
   pco2a_ns(4)  = 362.0
   pco2a_ns(5)  = 360.0
   pco2a_ns(6)  = 357.0
   pco2a_ns(7)  = 351.0
   pco2a_ns(8)  = 347.0
   pco2a_ns(9)  = 347.0
   pco2a_ns(10) = 352.0
   pco2a_ns(11) = 357.0
   pco2a_ns(12) = 360.0
#endif

   ! traditional formula ECOHAM4
   !if (year<=1995) then
   !   off = real(year-1995)*1.32
   !else
   !   off = real(year-1995)*2.00
   !endif

   ! improved formula by Bernhard Mayer
   if (year<=1987) then
      off = real(year-1995)*1.32
   else if (year<=1992) then
      off = real(year-1995)*1.05
   else if (year<=1995) then
      off = real(year-1995)*1.32
   else
      off = real(year-1995)*2.00
   endif

   itd = min(12,nint(td_/30.)+1)
   ! commented out as long as no spatial variation is included
   !do i = iStartCalc, iEndCalc
   !   do j = jStartCalc, jEndCalc
   !      pco2a(j,i) = pco2a_ns(itd)+off  !pCO2 in muatm
   !   enddo
   !enddo
   pco2a = pco2a_ns(itd)+off  !pCO2 in muatm

   if (ierr/=0) then
      write( error_message, '("[",i2.2,"] ERROR - airpco2: ",i3)') myID, ierr
      call stop_ecoham(ierr, msg=error_message)
   end if
   return

   end subroutine airpco2

!=======================================================================
!
! !INTERFACE:
   subroutine do_meteo(dt_,ierr)
#define SUBROUTINE_NAME 'do_meteo'
!
! !DESCRIPTION:
! container for subroutines concerning atmospheric deposition and air-sea fluxes
!
! !USES:
   use mod_var
   implicit none
!
! !INPUT PARAMETERS:
   real,    intent(in)     :: dt_   ! timestep (days)
! !OUTPUT PARAMETERS:
   integer, intent(inout)  :: ierr
! !LOCAL VARIABLES:
   integer                      :: i,j,k,k0
!
!-----------------------------------------------------------------------
#include "call-trace.inc"

   select case (isw_dim) !jp
      case (3) !3D-mode
         if (abs(opt_irr).le.1e-4) then
            Iopt(:,:,:) = Iopt(:,:,:) + dIopt_dt(:,:,:)*dt
         endif
      case (1) !1D-mode
         i = iStartCalc
         j = jStartCalc
         k0 = k_index(j,i)
         do k = 1, k0
            if (abs(opt_irr).le.1e-4) then
               Iopt(j,k,i) = Iopt(j,k,i) + dIopt_dt(j,k,i)*dt
            endif
         enddo
   end select

#ifdef module_chemie
!print*,'meteo#674',st(72,2,50,idic),st(72,2,50,ialk)
   if(iairsea==1)  call air_sea_co2(ierr)   !CO2 EXCHANGE WITH THE ATMOSPHERE
!print*,'meteo#676',st(72,2,50,idic),st(72,2,50,ialk)
#endif

#ifdef module_biogeo
   if(iairsea==1)  call air_sea_o2(ierr)    !OXYGEN EXCHANGE WITH THE ATMOSPHERE

   if (atm_n/=0.0) call n_deposition(ierr)  !ATMOSPHERIC NITROGEN DEPOSITION
#endif

   if (meteodilution.eq.1) then
      call prec_evap_dilution(dt_,ierr)
   endif

   if (ierr/=0) then
      write( error_message, '("[",i2.2,"] ERROR - do_meteo: ",i3)') myID, ierr
      call stop_ecoham(ierr, msg=error_message)
   end if
   return

   end subroutine do_meteo

!=======================================================================
#ifdef module_biogeo
!
! !INTERFACE:
   subroutine air_sea_o2(ierr)
#define SUBROUTINE_NAME 'air_sea_o2'
!
! !DESCRIPTION:
! calculation of air-sea exchange of O2
!
! !USES:
   use mod_hydro, only : temp, salt
   use mod_var
   use mod_flux
#if defined module_restoring && defined exclude_restoring_cells_meteo
   use mod_restoring, only: k_index_exclude_restoring
#endif
   implicit none
!
! !INPUT PARAMETERS:
!
! !OUTPUT PARAMETERS:
   integer,      intent(inout)  :: ierr
!
! !LOCAL VARIABLES:
   real          :: dz_air_sea, transfer_velocity, sc
   real          :: x1, x2, beta_dm, tabs !,fas
   integer       :: i, j, k, k0, kdz
!
!-----------------------------------------------------------------------
#include "call-trace.inc"

   !air_sea_mode=1  !Wanninkhof 92
   !air_sea_mode=2  !Wanninkhof & McGillis 99
   !air_sea_mode=3  !Nightingale et al 2000

   transfer_velocity = 0.0

   do i = iStartCalc, iEndCalc
      do j = jStartCalc, jEndCalc
         k0 = k_index(j,i)
#if defined module_restoring && defined exclude_restoring_cells_meteo
         k0 = k_index_exclude_restoring(j,i,io2o)
#endif
         if (k0>0) then
            kdz = min(k0,1)
            dz_air_sea = 0.
            do k = 1, kdz
               dz_air_sea = dz_air_sea + dz(j,k,i)
            enddo
            ! Schmidt Number
            sc = ( (-scdo*temp(j,1,i) +scco) *temp(j,1,i) -scbo )*temp(j,1,i) +scao
            ! transfer velocity
            if (air_sea_mode==1) then  ! Wanninkhof 92
               transfer_velocity = 0.3*u10(j,i)*u10(j,i)/sqrt(sc/660.)
            endif
            if (air_sea_mode==2) then  ! Wanninkhof & McGillis 99
 ! correction 0.0280 to 0.0283 (7.7.04)
               transfer_velocity = 0.0283*u10(j,i)*u10(j,i)*u10(j,i)/sqrt(sc/660.)
            endif
            if (air_sea_mode==3) then  ! Nightingale et al 2000
               transfer_velocity = (2.5*((4.9946e-4*temp(j,1,i)+1.6256e-2)      &
                                    *temp(j,1,i)+.5246)+0.222*u10(j,i)*u10(j,i) &
                                    +0.333*u10(j,i))/sqrt(sc/660.)
            endif
            transfer_velocity = 0.24*transfer_velocity      ! unit is cm/h must be m/d
            !solubility mumol dm-3 (tabs in kelvin)
            tabs = temp(j,1,i) + temzer
            beta_dm = exp( ((((c5o/tabs+c4o)/tabs+c3o)/tabs+c2o)/tabs+c1o)  &
                          -salt(j,1,i)*((d3o/tabs+d2o)/tabs+d1o) )
            x1 = beta_dm
            x2 = max(0.0,st(j,1,i,io2o))                    ! [mmol m-3]
            f_from_to(j,1,i,i_air_o2o) = transfer_velocity*(x1-x2)         ! [mmol m-2 d-1]
            sst(j,1,i,io2o) = sst(j,1,i,io2o) + f_from_to(j,1,i,i_air_o2o) ! [mmol m-2 d-1]
!             fas = transfer_velocity*(x1-x2)                 ! [mmol m-2 d-1]
!             do k = 1, kdz
!                f_from_to(j,k,i,i_air_o2o) = fas/dz_air_sea  ! [mmol m-3 d-1]
!                sst(j,k,i,io2o) = sst(j,k,i,io2o) + f_from_to(j,k,i,i_air_o2o)
!             enddo
         endif
      enddo
   enddo

   if (ierr/=0) then
      write( error_message, '("[",i2.2,"] ERROR - air_sea_o2: ",i3)') myID, ierr
      call stop_ecoham(ierr, msg=error_message)
   end if
   return

   end subroutine air_sea_o2
#endif
!=======================================================================
#ifdef module_chemie
!
! !INTERFACE:
   subroutine air_sea_co2(ierr)
#define SUBROUTINE_NAME 'air_sea_co2'
!
! !DESCRIPTION:
! calculation of air-sea exchange of CO2
!
! !USES:
   use mod_hydro,  only : temp, salt, rho
   use mod_chemie, only : ch, itdic, io2c
   use mod_var
   use mod_flux
#ifdef module_restoring
#if defined exclude_restoring_cells_meteo || defined exclude_restoring_cells_chemie
   use mod_restoring, only: k_index_exclude_restoring
#endif
#endif
   implicit none
!
! !INPUT PARAMETERS:
!
! !OUTPUT PARAMETERS:
   integer,      intent(inout)  :: ierr
!
! !LOCAL VARIABLES:
   real          :: dz_air_sea, transfer_velocity, sc
   real          :: x1, x2, tabs, alphas !,fas
   integer       :: i, j, k, k0, kdz
!
!-----------------------------------------------------------------------
#include "call-trace.inc"

   !air_sea_mode=1  !Wanninkhof 92
   !air_sea_mode=2  !Wanninkhof & McGillis 99
   !air_sea_mode=3  !Nightingale et al 2000

   transfer_velocity = 0.0

   do i = iStartCalc, iEndCalc
      do j = jStartCalc, jEndCalc
         k0 = k_index(j,i)
#ifdef module_restoring
#if defined exclude_restoring_cells_meteo || defined exclude_restoring_cells_chemie
         k0 = k_index_exclude_restoring(j,i,idic)
#endif
#endif
         if (k0>0) then
            kdz = min(k0,1)
            dz_air_sea = 0.
            do k = 1, kdz
               dz_air_sea = dz_air_sea + dz(j,k,i)
            enddo
            ! Schmidt Number
            sc = ( (-scdo*temp(j,1,i) +scco) *temp(j,1,i) -scbo )*temp(j,1,i) +scao
            ! transfer velocity
            if (air_sea_mode==1) then  ! Wanninkhof 92
               transfer_velocity=(2.5*((4.9946e-4*temp(j,1,i)+1.6256e-2)     &
                                       *temp(j,1,i)+.5246)+0.3*u10(j,i)      &
                                       *u10(j,i))/sqrt(sc/660.)
            endif
            if (air_sea_mode==2) then  ! Wanninkhof & McGillis 99
               transfer_velocity=(2.5*((4.9946e-4*temp(j,1,i)+1.6256e-2)     &
                                       *temp(j,1,i)+.5246)+0.0280*u10(j,i)   &
                                       *u10(j,i)*u10(j,i))/sqrt(sc/660.)
            endif
            if (air_sea_mode==3) then  ! Nightingale et al 2000
               transfer_velocity=(2.5*((4.9946e-4*temp(j,1,i)+1.6256e-2)     &
                                       *temp(j,1,i)+.5246)+0.222*u10(j,i)    &
                                       *u10(j,i)+0.333*u10(j,i))/sqrt(sc/660.)
            endif
            transfer_velocity = 0.24*transfer_velocity      ! unit is cm/h must be m/d
            !solubility (tabs in kelvin)
            tabs = temp(j,1,i) + temzer
            alphas = exp( -60.2409 + 9345.17/tabs + 23.3585*alog(tabs/100.)  &
                           + salt(j,1,i)*(.023517 - .023656*tabs/100.        &
                           + 1.e-8*47.036*tabs*tabs) )      ! alphas: [mumol/kg/muatm]
            !x1 = alphas *pco2a(j,i) *rho(j,1,i) ! NOTE: in ECOHAM "rho" is kg/l = (kg/m3)/1000 !!!
            x1 = alphas *pco2a *rho(j,1,i) ! NOTE: in ECOHAM "rho" is kg/l = (kg/m3)/1000 !!!
            x2 = ch(j,1,i,io2c)            ! [mmol m-3]
            f_from_to(j,1,i,i_air_o2c) = transfer_velocity*(x1-x2)         ! [mmol m-2 d-1]
            sst(j,1,i,idic) = sst(j,1,i,idic) + f_from_to(j,1,i,i_air_o2c) ! [mmol m-2 d-1]
!             fas = transfer_velocity*(x1-x2)                 ! [mmol m-2 d-1]
!             do k = 1, kdz
!                f_from_to(j,k,i,i_air_o2c) = fas/dz_air_sea  ! [mmol m-3 d-1]
!                sst(j,k,i,idic) = sst(j,k,i,idic) + f_from_to(j,k,i,i_air_o2c)
!             enddo
         endif
      enddo
   enddo

   if (ierr/=0) then
      write( error_message, '("[",i2.2,"] ERROR - air_sea_co2: ",i3)') myID, ierr
      call stop_ecoham(ierr, msg=error_message)
   end if
   return

   end subroutine air_sea_co2
#endif
!=======================================================================
#ifdef module_biogeo
!
! !INTERFACE:
   subroutine n_deposition(ierr)
#define SUBROUTINE_NAME 'n_deposition'
!
! !DESCRIPTION:
! define all variables for one day
!
! !USES:
   use mod_var
   use mod_flux
   implicit none
!
! !INPUT PARAMETERS:
!
! !OUTPUT PARAMETERS:
   integer,      intent(inout)  :: ierr
!
! !LOCAL VARIABLES:
   integer :: i,j
!
!-----------------------------------------------------------------------
#include "call-trace.inc"

   ! atmos. N flux: atm_n (mmol N/m2/d)
   select case (isw_dim) !jp
      case (3) !3D-mode
         where(ixistK==1) f_from_to(:,1,:,i_atm_n3n) = fatm_nox (:,:) !/dz(:,1,:) [mmol m-2 d-1]
         where(ixistK==1) f_from_to(:,1,:,i_atm_n4n) = fatm_nred(:,:) !/dz(:,1,:) [mmol m-2 d-1]
         where(ixistK==1) sst(:,1,:,in3n) = sst(:,1,:,in3n) + f_from_to(:,1,:,i_atm_n3n) ![mmol m-2 d-1]
         where(ixistK==1) sst(:,1,:,in4n) = sst(:,1,:,in4n) + f_from_to(:,1,:,i_atm_n4n) ![mmol m-2 d-1]
         if(talk_treat==1)then
            where(ixistK==1) sst(:,1,:,ialk) = sst(:,1,:,ialk) - f_from_to(:,1,:,i_atm_n3n)  & ![mmol m-2 d-1]
                                                               + f_from_to(:,1,:,i_atm_n4n)    ![mmol m-2 d-1]
         endif
      case (1) !1D-mode
         i = iStartCalc
         j = jStartCalc
         f_from_to(j,1,i,i_atm_n3n) = fatm_nox(j,i)  !/dz(j,1,i) [mmol m-2 d-1]
         f_from_to(j,1,i,i_atm_n4n) = fatm_nred(j,i) !/dz(j,1,i) [mmol m-2 d-1]
         sst(j,1,i,in3n) = sst(j,1,i,in3n) + f_from_to(j,1,i,i_atm_n3n) ![mmol m-2 d-1]
         sst(j,1,i,in4n) = sst(j,1,i,in4n) + f_from_to(j,1,i,i_atm_n4n) ![mmol m-2 d-1]
         if(talk_treat==1)then
           sst(j,1,i,ialk) = sst(j,1,i,ialk) - f_from_to(j,1,i,i_atm_n3n) & ![mmol m-2 d-1]
                                             + f_from_to(j,1,i,i_atm_n4n)   ![mmol m-2 d-1]
         endif
   end select

   if (ierr/=0) then
      write( error_message, '("[",i2.2,"] ERROR - n_deposition: ",i3)') myID, ierr
      call stop_ecoham(ierr, msg=error_message)
   end if
   return

   end subroutine n_deposition
#endif
!=======================================================================
!
! !INTERFACE:
   subroutine prec_evap_dilution(dt_,ierr)
#define SUBROUTINE_NAME 'prec_evap_dilution'
!
! !DESCRIPTION:
! freshwater input by precipitation decreases, evaporation increases
! the concentration of matter
!
! !USES:
   use mod_var  ,only : st, n_st_var
   use mod_flux ,only : f_pev    ! prec.-evaporation
   implicit none
!
! !INPUT PARAMETERS:
   real,     intent(in)    :: dt_   ! timestep (days)
!
! !OUTPUT PARAMETERS:
   integer, intent(inout)  :: ierr
!
! !LOCAL VARIABLES:
   integer               :: i, j, k0, n, idd
   real                  :: st_old, st_new
   real                  :: eps = 1.e-8

!
!-----------------------------------------------------------------------
#include "call-trace.inc"

   idd  = int(day_of_year-eps)+1

   do n = 1,n_st_var
      do i = iStartCalc, iEndCalc
         do j = jStartCalc, jEndCalc
            k0 = k_index(j,i)
            if (k0>0) then
               st_old = st(j,1,i,n)                                          ![mmol m-3]
               st_new = st_old/(1.+prec_evap(j,i,max(1,idd))/vol(j,1,i)*dt_) ![mmol m-3]
               f_pev(j,1,i,n) = (st_new-st_old)/dt_                ![mmol m-3 d-1]
               !sst(j,1,i,n) = sst(j,1,i,n) + f_pev(j,1,i,n)       ![mmol m-2 d-1]
            endif
         enddo
      enddo
   enddo

   if (ierr/=0) then
      write( error_message, '("[",i2.2,"] ERROR - prec_evap_dilution: ",i3)') myID, ierr
      call stop_ecoham(ierr, msg=error_message)
   end if
   return

   end subroutine prec_evap_dilution

!=======================================================================
!
! !INTERFACE:
   subroutine close_meteo(ierr)
#define SUBROUTINE_NAME 'close_meteo'
!
! !DESCRIPTION:
!
! !USES:
   implicit none
!
! !INPUT PARAMETERS:
   integer, intent(inout)  :: ierr
!
!-----------------------------------------------------------------------
#include "call-trace.inc"

   close(wins_unit)
   close(radday_unit)
   close(rad_unit)
   close(extw_unit)
   close(N_depos_unit)

   if (ierr/=0) then
      write( error_message, '("[",i2.2,"] ERROR - close_meteo: ",i3)') myID, ierr
      call stop_ecoham(ierr, msg=error_message)
   end if
   return

   end subroutine close_meteo

!=======================================================================
#endif
   end module mod_meteo

!=======================================================================
