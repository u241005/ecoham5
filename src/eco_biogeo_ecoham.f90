#ifdef module_biogeo
!
! !MODULE: eco_biogeo_ecoham.f90 --- pelagic biogeochemistry for ecoham
!
! !INTERFACE:
   MODULE mod_biogeo_ecoham
#ifndef module_biogeo_recom
!
! !DESCRIPTION:
! This module is the container for pelagic ecoham biogeochemistry
!
! !USES:
   use mod_common
!   use mod_mpi_parallel
   use mod_var
   use mod_flux
   implicit none
!
!  default: all is private
   private
!
! !PUBLIC MEMBER FUNCTIONS:
   public init_biogeo, do_biogeo, update_biogeo
   public cum_biofunctions, empty_biofunctions
!
! !LOCAL VARIABLES:
! used for namelist:
   integer :: extp_feedback
   real :: extp, extp1, extp2, extp3, vp1, vp2, vp3, xk1, xk13
   real :: xk21, xk22, xk23, xkp, xkp3, xks, gam1, gam2,  gam3
   real :: xmu11, xmu12, xmu13, xmq11, xmq12, xmq13
   real :: wp1c, wp2c, wp3c, excess, soc_rate
   real, public :: q_c_cal
   real :: c_max, rccalc_min, xkc_ir, xkk_ir, xkk, detach_min
   real :: g1_max, g2_max, g1_exp, g2_exp, xk31, xk32
   real :: p1_p1n, p1_p2n, p1_p3n, p1_d1n, p1_ban
   real :: p2_p1n, p2_p2n, p2_p3n, p2_d1n, p2_ban, p2_z1n
   real :: xmu21, xmu22, xmq21, xmq22, xmu6, xmu6c, beta1, beta2
   real :: xk16, xk26, aeps1, aeps2, delta_don1, delta_don2
   real :: frac_dic, frac_n4n, frac_n1p, frac_det, frac_d2x
   real :: wd1c, wd2c, xmu4n, xmu5n, rxmu4c
   real :: xk4, xkpb, eta, vb, xmu3, xknit, rhoC, rhoN, rhoP
#ifndef adjust_stoichiometry
   real, public :: red, rcn, rcp, rcs, rcn2, rcp2, rcn3, rcp3
   real, public :: rcnb, rcpb, rcnz1, rcpz1, rcnz2, rcpz2
   real, public :: chl_p1c, chl_p2c, chl_p3c
#else
   !real, public :: red, rcn, rcp, rcs, rcn2, rcp2, rcn3, rcp3      !MK defined in eco_common.f90
   real, public :: rcnb, rcpb, rcnz1, rcpz1, rcnz2, rcpz2
   !real, public :: chl_p1c, chl_p2c, chl_p3c                       !MK defined in eco_common.f90
#endif
! public parameters used in other modules:
   real, allocatable, dimension(:,:,:), public :: d_fPAR, d_chl_p1c, d_chl_p2c, d_chl_p3c
   real, allocatable, dimension(:,:,:), public :: d_lim_p1_n1p, d_lim_p1_n3n, d_lim_p1_n4n, d_lim_p1_n5s
   real, allocatable, dimension(:,:,:), public :: d_lim_p2_n1p, d_lim_p2_n3n, d_lim_p2_n4n
   real, allocatable, dimension(:,:,:), public :: d_lim_p3_n1p, d_lim_p3_n3n, d_lim_p3_n4n, d_lim_p3_dop
   integer :: iexcess
! parameters private to module bio:
   real, parameter :: TINY= 1.0e-3
   real, allocatable, dimension(:,:,:) :: lim_p1_n1p, lim_p1_n3n, lim_p1_n4n, lim_p1_n5s
   real, allocatable, dimension(:,:,:) :: lim_p2_n1p, lim_p2_n3n, lim_p2_n4n
   real, allocatable, dimension(:,:,:) :: lim_p3_n1p, lim_p3_n3n, lim_p3_n4n, lim_p3_dop
   real, allocatable, dimension(:,:,:) :: fPAR, q_chl_p1c, q_chl_p2c, q_chl_p3c
   real, allocatable, dimension(:,:,:) :: omega, anitdep
   real, allocatable, dimension(:,:,:) :: fp3, fp3k
   real, allocatable, dimension(:,:,:) :: tfac, tfac0, tfac1, tfac2, tfac3
   real, allocatable, dimension(:,:,:) :: tfac_z1, tfac_z2
!   real, allocatable, dimension(:,:,:) :: q1d, q1f, q1c, q2d, q2f, q2c, q3, q3c, q4, q3cdop
!
#ifdef cskc
   character(len=*), parameter, private :: init_endian   = 'LITTLE_ENDIAN'
#else
   character(len=*), parameter, private :: init_endian   = 'BIG_ENDIAN'
#endif
   character(len=*), parameter, private :: init_dir      = 'Input/'
   integer,          parameter, private :: init_realkind = 8   != ibyte_per_real
!
!=======================================================================

   contains

!=======================================================================
!
! !INTERFACE:
   subroutine init_biogeo(ierr)
#define SUBROUTINE_NAME 'init_biogeo_ecoham'
!
! !DESCRIPTION:
!
! !USES:
   !use mod_hydro,     only : salt, temp, rho, pres
   use mod_utils,     only : calcite
#ifdef module_restoring
   use mod_restoring, only : get_restoring, do_restoring
#endif
   implicit none
!
! !INTPUT PARAMETERS:
   integer, intent(inout) :: ierr
!
! !LOCAL VARIABLES:
   integer              :: k, n, iz(kmax)
   real                 :: wmax
   logical              :: file_exist
   real(kind=init_realkind), allocatable :: input1d(:), input3d(:,:,:)
!
   namelist /bio_nml/  extp_feedback, &
             extp, extp1, extp2, extp3, vp1, vp2, vp3, xk1, xk13,      &
             xk21, xk22, xk23, xkp, xkp3, xks, gam1, gam2, gam3,       &
             xmu11, xmu12, xmu13, xmq11, xmq12, xmq13,                 &
             wp1c, wp2c, wp3c, excess, soc_rate, q_c_cal,              &
             c_max, rccalc_min, xkc_ir, xkk_ir, xkk, detach_min,       &
             g1_max, g2_max, g1_exp, g2_exp, xk31, xk32,               &
             p1_p1n, p1_p2n, p1_p3n, p1_d1n, p1_ban,                   &
             p2_p1n, p2_p2n, p2_p3n, p2_d1n, p2_ban, p2_z1n,           &
             xmu21, xmu22, xmq21, xmq22, xmu6, xmu6c, beta1, beta2,    &
             xk16, xk26, aeps1, aeps2, delta_don1, delta_don2,         &
             frac_dic, frac_n4n, frac_n1p, frac_det, frac_d2x,         &
             wd1c, wd2c, xmu4n, xmu5n, rxmu4c,                         &
             xk4, xkpb, eta, vb, xmu3, xknit, rhoC, rhoN, rhoP,        &
             red, rcn, rcp, rcs, rcn2, rcp2, rcn3, rcp3, rcnb, rcpb,   &
             rcnz1, rcpz1, rcnz2, rcpz2, chl_p1c, chl_p2c, chl_p3c
!
!-------------------------------------------------------------------------
#include "call-trace.inc"

   if (myID == master) call write_log_message(" INITIALIZING biogeo_ecoham")

! #if !defined module_restoring && defined exclude_restoring_cells_biogeo
!    error_message = 'ERROR: You may not specify -Dexclude_restoring_cells_biogeo' &
!                   //' without Flag -Dmodule_restoring!'
!    call stop_ecoham(ierr, msg=error_message)
! #endif

   if (myID == master) then
#ifdef exclude_p1x
      write(long_msg,'(3x,"-exclude first phytoplankton group (e.g. diatoms)")')
      call write_log_message(long_msg); long_msg = ''
#endif
#ifdef exclude_p2x
      write(long_msg,'(3x,"-exclude second phytoplankton group (e.g. flagellates)")')
      call write_log_message(long_msg); long_msg = ''
#endif
#ifdef exclude_p3x
      write(long_msg,'(3x,"-exclude third phytoplankton group (recomended for D093)")')
      call write_log_message(long_msg); long_msg = ''
#endif
#ifdef exclude_z1x
      write(long_msg,'(3x,"-exclude first zooplankton group (e.g. mircozoo)")')
      call write_log_message(long_msg); long_msg = ''
#endif
#ifdef exclude_z2x
      write(long_msg,'(3x,"-exclude second zooplankton group (e.g. mesozoo)")')
      call write_log_message(long_msg); long_msg = ''
#endif
#ifdef exclude_bac
      write(long_msg,'(3x,"-exclude bacteria group (apply direct remineralization DOM->DIM)")')
      call write_log_message(long_msg); long_msg = ''
#endif

#ifdef d093
      write(long_msg,'(3x,"-code modified according to run D093")')
      call write_log_message(long_msg); long_msg = ''
#endif

#if !defined exclude_p1x || !defined exclude_p2x || !defined exclude_p2x
#ifdef extp_d093
      write(long_msg,'(3x,"-phytoplankton light extinction according to fasham (recommended for D093)")')
      call write_log_message(long_msg); long_msg = ''
#endif
#ifdef variable_phytoplankton_stoichiometry
      write(long_msg,'(3x,"-use real C:N:P-ratios from statevariables instead of values from namelist")')
      call write_log_message(long_msg); long_msg = ''
#endif
#endif

#if !defined exclude_z1x || !defined exclude_z2x
#ifdef fasham_grazing
      write(long_msg,'(3x,"-grazing is formulated based on fasham (recomended for D093)")')
      call write_log_message(long_msg); long_msg = ''
#endif
#ifdef fasham_losses
      write(long_msg,'(3x,"-loss-terms are formulated based on Fasham (recomended for D093)")')
      call write_log_message(long_msg); long_msg = ''
#ifdef fasham_losses_revised
      write(long_msg,'(3x,"-apply revision from markus on fasham formulations (will break D093 compatibility)")')
      call write_log_message(long_msg); long_msg = ''
#endif
#ifdef old_feces_stoichiometry
      write(long_msg,'(3x,"-feces have C:N:P from zooplankton (recomended for D093)")')
      call write_log_message(long_msg); long_msg = ''
#else
      write(long_msg,'(3x,"-flag old_feces_stoichiometry disabled (will break D093 compatibility)")')
      call write_log_message(long_msg); long_msg = ''
#endif
#else
      write(long_msg,'(3x,"-loss-terms according to suggestion from book Sterner&Elser(2002)-pp197")')
      call write_log_message(long_msg); long_msg = ''
#endif
#endif

#if !defined exclude_bac
#ifdef old_bac
      write(long_msg,'(3x,"-using old_bac (bacteria according to D093)")')
      call write_log_message(long_msg); long_msg = ''
#else
      write(long_msg,'(3x,"-using new bacteria formulations")')
      call write_log_message(long_msg); long_msg = ''
#endif
#endif
   endif ! master

   open(nml_unit, file=trim(settings_bio), action='read', status='old')
   read(nml_unit, nml=bio_nml)
   close(nml_unit)

   if (abs(excess)<1.e-5) then
      iexcess = 0
   else
      iexcess = 1
   endif

   ! who sinks?
   do n = 1, n_st_var
      sink(n) = 0.0 ! initialize all to zero
   enddo
   sink(ip1c) = wp1c ! sinking of p1c
   sink(ip1n) = wp1c ! sinking of p1n
   sink(ip1p) = wp1c ! sinking of p1p
   sink(ip2c) = wp2c ! sinking of p2c
   sink(ip2n) = wp2c ! sinking of p2n
   sink(ip2p) = wp2c ! sinking of p2p
   sink(ip3c) = wp3c ! sinking of p3c
   sink(ip3n) = wp3c ! sinking of p3n
   sink(ip3p) = wp3c ! sinking of p3p
   sink(id1c) = wd1c ! sinking of d1c
   sink(id1n) = wd1c ! sinking of d1n
   sink(id1p) = wd1c ! sinking of d1p
   sink(id2c) = wd2c ! sinking of d2c
   sink(id2n) = wd2c ! sinking of d2n
   sink(id2p) = wd2c ! sinking of d2p
   sink(id2s) = wd2c ! sinking of d2s
   sink(id2k) = wd2c ! sinking of d2k
#ifdef TBNT_x1x_test
   sink(ix1x) = wd1c ! sinking for "biological" x1x
   sink(ix2x) = wd2c ! sinking for "biological" x2x
#endif

   ! check whether CFL criterion for sinking is violated
   ! for parallel code dz_min ist calculated for each process
   wmax = max(wp1c, wp2c, wp3c, wd1c, wd2c)
   if (dz_min .lt. wmax*dt) then
      ierr = 1
      call stop_ecoham(ierr, msg="ERROR: CFL violated for sinking")
   endif

   ! allocate and initialize arrays
   call allocate_arrays_biogeo(ierr)

   ! initialize chlorophyll-to-C ratio
   where(ixistZ==1) q_chl_p1c(:,:,:) = chl_p1c !0.003*12 ! g Chl (mol C)^(-1)
   where(ixistZ==1) q_chl_p2c(:,:,:) = chl_p2c !0.003*12 ! g Chl (mol C)^(-1)
   where(ixistZ==1) q_chl_p3c(:,:,:) = chl_p3c !0.003*12 ! g Chl (mol C)^(-1)

   !initialize statevariables on wet points
   if (iwarm.ne.1) then
      if (myID == master) call write_log_message("   coldstart initialization")
      ! INITIAL VALUES most in [mmol m-3]
      ! default initialization, homogenous distribution
      where(ixistz==1) st(:,:,:,ix1x) = 1.
      where(ixistz==1) st(:,:,:,ialk) = 2358.
      where(ixistz==1) st(:,:,:,idic) = 2163.    ! DIC
      where(ixistz==1) st(:,:,:,in3n) = 8.       ! nitrate
      where(ixistz==1) st(:,:,:,in4n) = 0.1      ! ammonium
      where(ixistz==1) st(:,:,:,in1p) = 1.       ! phosphate
      where(ixistz==1) st(:,:,:,in5s) = 5.       ! silicate
      where(ixistz==1) st(:,:,:,iz1c) = 0.25*red
      where(ixistz==1) st(:,:,:,iz2c) = 0.25*red
      where(ixistz==1) st(:,:,:,ibac) = 0.25
      where(ixistz==1) st(:,:,:,id1c) = 0.01*red
      where(ixistz==1) st(:,:,:,id1n) = 0.01
      where(ixistz==1) st(:,:,:,id1p) = 0.01/16.
      where(ixistz==1) st(:,:,:,id2c) = 0.0001*red
      where(ixistz==1) st(:,:,:,id2n) = 0.0001
      where(ixistz==1) st(:,:,:,id2p) = 0.0001/16.
      where(ixistz==1) st(:,:,:,id2s) = 0.0001/5.76
      where(ixistz==1) st(:,:,:,id2k) = 0.0001*red/40.
      where(ixistz==1) st(:,:,:,isoc) = 0.1
      where(ixistz==1) st(:,:,:,idon) = 0.01
      where(ixistz==1) st(:,:,:,idoc) = 0.01*red
      where(ixistz==1) st(:,:,:,idop) = 0.01/16.
      where(ixistz==1) st(:,:,:,io2o) = 280.
      ! phytoplankton initialization with homogenous two layer distribution
      iz = 1
      where (tiestl(1:kmax) < 150.) iz = 1
      k = sum(iz)
      where(ixistz(:,  1:k   ,:) ==1) st(:,  1:k   ,:,ip1n) = 0.1
      where(ixistz(:,k+1:kmax,:) ==1) st(:,k+1:kmax,:,ip1n) = 0.0001
      ! derive other phytoplankton variables according to given quotas
      where(ixistz==1) st(:,:,:,ip1c) = st(:,:,:,ip1n)*rcn
      where(ixistz==1) st(:,:,:,ip1p) = st(:,:,:,ip1c)/rcp
      where(ixistz==1) st(:,:,:,ip1s) = st(:,:,:,ip1c)/rcs
      where(ixistz==1) st(:,:,:,ip2c) = st(:,:,:,ip1c)
      where(ixistz==1) st(:,:,:,ip2n) = st(:,:,:,ip1n)
      where(ixistz==1) st(:,:,:,ip2p) = st(:,:,:,ip1p)
      where(ixistz==1) st(:,:,:,ip3c) = st(:,:,:,ip1c)
      where(ixistz==1) st(:,:,:,ip3n) = st(:,:,:,ip1n)
      where(ixistz==1) st(:,:,:,ip3p) = st(:,:,:,ip1p)
      where(ixistz==1) st(:,:,:,ip3k) = st(:,:,:,ip1c)/q_c_cal
      where(ixistz==1) st(:,:,:,ip1a) = st(:,:,:,ip1c)*q_chl_p1c
      where(ixistz==1) st(:,:,:,ip2a) = st(:,:,:,ip2c)*q_chl_p2c
      where(ixistz==1) st(:,:,:,ip3a) = st(:,:,:,ip3c)*q_chl_p3c

      if (iwarm == 2) then
        ! read from initial distribution files, if exist, and overwrite
         allocate (input1d(iiwet),input3d(jMax,kMax,iMax))
         do n = 1, n_st_var
            filename = trim(init_dir)//'init.'//trim(st_name(n))//'.direct'
            inquire (file=trim(filename), exist=file_exist)
            if (file_exist) then
               if (myID == master) call write_log_message("   include initialization from "//trim(filename) )
               open (warm_unit, file=trim(filename), status='old', form='unformatted', &
                                access='direct', recl=init_realkind*iiwet, convert=trim(init_endian) )
               read (warm_unit, rec = 1) input1d
               input3d(:,:,:) = unpack(real(input1d),ixistz_master(:,:,:)==1,fail)
               st(:,:,:,n)    = input3d(:,:,iStartD:iEndD)
               close (warm_unit)
            else
              if (myID == master) call write_log_message("   init file "//trim(filename)//" does not exist" )
            endif
         enddo
         deallocate (input1d, input3d)
      endif
   endif

#ifdef exclude_p1x
   extp1 = 0.0
   chl_p1c = 0.0  ! g Chl (mol C)^(-1)
   where(ixistZ==1) q_chl_p1c(:,:,:) = 0. ! g Chl (mol C)^(-1)
   where(ixistZ==1) st(:,:,:,ip1a)   = 1.
   where(ixistZ==1) st(:,:,:,ip1c)   = 1.
   where(ixistZ==1) st(:,:,:,ip1n)   = 1.
   where(ixistZ==1) st(:,:,:,ip1p)   = 1.
   where(ixistZ==1) st(:,:,:,ip1s)   = 1.
#endif
#ifdef exclude_p2x
   extp2 = 0.0
   chl_p2c = 0.0  ! g Chl (mol C)^(-1)
   where(ixistZ==1) q_chl_p2c(:,:,:) = 0. ! g Chl (mol C)^(-1)
   where(ixistZ==1) st(:,:,:,ip2a)   = 1.
   where(ixistZ==1) st(:,:,:,ip2c)   = 1.
   where(ixistZ==1) st(:,:,:,ip2n)   = 1.
   where(ixistZ==1) st(:,:,:,ip2p)   = 1.
#endif
#ifdef exclude_p3x
   extp3 = 0.0
   chl_p3c = 0.0  ! g Chl (mol C)^(-1)
   where(ixistZ==1) q_chl_p3c(:,:,:) = 0. ! g Chl (mol C)^(-1)
   where(ixistZ==1) st(:,:,:,ip3a)   = 1.
   where(ixistZ==1) st(:,:,:,ip3c)   = 1.
   where(ixistZ==1) st(:,:,:,ip3n)   = 1.
   where(ixistZ==1) st(:,:,:,ip3p)   = 1.
   where(ixistZ==1) st(:,:,:,ip3k)   = 1.
#endif

#ifdef module_restoring
      ! adjust boundaries / restoring-cells
      call get_restoring(day_of_year,ierr)
      call do_restoring(day_of_year,ierr)
#endif

   if (ierr/=0) then
      write( error_message, '("[",i2.2,"] ERROR - init_biogeo_ecoham: ",i3)') myID,ierr
      call stop_ecoham(ierr, msg=error_message)
   end if
   return

   end subroutine init_biogeo

!=======================================================================
!
! !INTERFACE:
   subroutine update_biogeo(ierr)
#define SUBROUTINE_NAME 'update_biogeo_ecoham'
!
! !DESCRIPTION:
!
! !USES:
   use mod_hydro, only : temp
   use mod_meteo, only : extp_field
   implicit none
!
! !INPUT PARAMETERS:
   integer, intent(inout) :: ierr
!
! !LOCAL VARIABLES:
   integer  :: i, j, k, k0
!
!-------------------------------------------------------------------------
#include "call-trace.inc"

   !if (myID==master) call write_log_message(" UPDATING biogeo_ecoham ")

   select case (isw_dim) !jp
      case (3) !3D-mode
         ! update temperature effect factors
         where(ixistz==1) tfac0   = exp( alog(1.5)*((temp-10.)/10.) )   ! for bulk phyto
         where(ixistz==1) tfac1   = exp( alog(1.5)*((temp-10.)/10.) )   ! for diatoms
         where(ixistz==1) tfac2   = exp( alog(1.5)*((temp-10.)/10.) )   ! for flagellates
         where(ixistz==1) tfac3   = exp( alog(1.5)*((temp-10.)/10.) )   ! for coccos
         where(ixistz==1) tfac_z1 = exp( alog(1.0)*((temp-10.)/10.) )   ! for microzoo
         where(ixistz==1) tfac_z2 = exp( alog(1.0)*((temp-10.)/10.) )   ! for mesozoo
         where(ixistz==1) tfac    = one                                 ! for ecosystem
         ! update derived variables ..
         where(ixistz==1) de(:,:,:,iz1n) = st(:,:,:,iz1c)/rcnz1         ! micro-zooplankton (N)
         where(ixistz==1) de(:,:,:,iz1p) = st(:,:,:,iz1c)/rcpz1         ! micro-zooplankton (P)
         where(ixistz==1) de(:,:,:,iz2n) = st(:,:,:,iz2c)/rcnz2         ! meso-zooplankton (N)
         where(ixistz==1) de(:,:,:,iz2p) = st(:,:,:,iz2c)/rcpz2         ! meso-zooplankton (P)
         where(ixistz==1) de(:,:,:,iban) = st(:,:,:,ibac)/rcnb          ! bacteria (N)
         where(ixistz==1) de(:,:,:,ibap) = st(:,:,:,ibac)/rcpb          ! bacteria (P)

         select case(extp_feedback)
            case (1)     ! 1 - light extinction based on dynamic Chla
               !where(ixistz==1) extp_field = extp*(de(:,:,:,ip1a) + de(:,:,:,ip2a) + de(:,:,:,ip3a))
               ! as de(:,:,:,ipXa) no longer exists and to keep extp_field equal to that defined in branch master,
               ! extp_field is not directly related to st(:,:,:,ipXa), which might diverge from "pXc*q_chl_pXc"
               where(ixistz==1) extp_field = extp*(  st(:,:,:,ip1c)*q_chl_p1c   &
                                                   + st(:,:,:,ip2c)*q_chl_p2c   &
                                                   + st(:,:,:,ip3c)*q_chl_p3c)
            case default ! 0 - light extinction based on carbon biomass and constant Chla:C ratios
               where(ixistz==1) extp_field = extp*(  st(:,:,:,ip1c)*chl_p1c   &
                                                   + st(:,:,:,ip2c)*chl_p2c   &
                                                   + st(:,:,:,ip3c)*chl_p3c)
         end select
#ifdef extp_d093
         ! light extinction based on nitrogen biomass and species-specific extinction coefficients (fasham)
         where(ixistz==1) extp_field = extp1*st(:,:,:,ip1n) + extp2*st(:,:,:,ip2n) + extp3*st(:,:,:,ip3n)
#endif

      case (1) !1D-mode
         i = iStartCalc
         j = jStartCalc
         k0 = k_index(j,i)
         do k = 1, k0
            ! update temperature effect factors
            tfac0(j,k,i)   = exp( alog(1.5)*((temp(j,k,i)-10.)/10.) )   ! for bulk phyto
            tfac1(j,k,i)   = exp( alog(1.5)*((temp(j,k,i)-10.)/10.) )   ! for diatoms
            tfac2(j,k,i)   = exp( alog(1.5)*((temp(j,k,i)-10.)/10.) )   ! for flagellates
            tfac3(j,k,i)   = exp( alog(1.5)*((temp(j,k,i)-10.)/10.) )   ! for coccos
            tfac_z1(j,k,i) = exp( alog(1.0)*((temp(j,k,i)-10.)/10.) )   ! for microzoo
            tfac_z2(j,k,i) = exp( alog(1.0)*((temp(j,k,i)-10.)/10.) )   ! for mesozoo
            tfac(j,k,i)    = one                                        ! for ecosystem
            ! update derived variables ..
            de(j,k,i,iz1n) = st(j,k,i,iz1c)/rcnz1                       ! micro-zooplankton (N)
            de(j,k,i,iz1p) = st(j,k,i,iz1c)/rcpz1                       ! micro-zooplankton (P)
            de(j,k,i,iz2n) = st(j,k,i,iz2c)/rcnz2                       ! meso-zooplankton (N)
            de(j,k,i,iz2p) = st(j,k,i,iz2c)/rcpz2                       ! meso-zooplankton (P)
            de(j,k,i,iban) = st(j,k,i,ibac)/rcnb                        ! bacteria (N)
            de(j,k,i,ibap) = st(j,k,i,ibac)/rcpb                        ! bacteria (P)

            select case(extp_feedback)
               case (1)     ! 1 - light extinction based on dynamic Chla
                  !extp_field(j,k,i) = extp*(st(j,k,i,ip1a) + st(j,k,i,ip2a) + st(j,k,i,ip3a))
                  ! as de(:,:,:,ipXa) no longer exists and to keep extp_field equal to that defined in branch master,
                  ! extp_field is not directly related to st(:,:,:,ipXa), which might diverge from "pXc*q_chl_pXc"
                  extp_field(j,k,i) = extp*(  st(j,k,i,ip1c)*q_chl_p1c(j,k,i) &
                                            + st(j,k,i,ip2c)*q_chl_p1c(j,k,i) &
                                            + st(j,k,i,ip3c)*q_chl_p1c(j,k,i))
               case default ! 0 - light extinction based on carbon biomass and constant Chla:C ratios
                  extp_field(j,k,i) = extp*(  st(j,k,i,ip1c)*chl_p1c   &
                                            + st(j,k,i,ip2c)*chl_p2c   &
                                            + st(j,k,i,ip3c)*chl_p3c)
            end select
#ifdef extp_d093
            ! light extinction based on nitrogen biomass and species-specific extinction coefficients (fasham)
            extp_field(j,k,i) = extp1*st(j,k,i,ip1n) + extp2*st(j,k,i,ip2n) + extp3*st(j,k,i,ip3n)
#endif
         enddo
   end select

   if (ierr/=0) then
      write( error_message, '("[",i2.2,"] ERROR - update_biogeo_ecoham: ",i3)') myID,ierr
      call stop_ecoham(ierr, msg=error_message)
   end if
   return

   end subroutine update_biogeo

!=======================================================================
!
! !INTERFACE:
   subroutine do_biogeo(ierr)
#define SUBROUTINE_NAME 'do_biogeo_ecoham'
!
! !DESCRIPTION:
!
! !USES:
   use mod_hydro,  only : temp
   use mod_meteo,  only : par, Iopt, pafr, radsol
#if !defined exclude_p1x || !defined exclude_p2x || !defined exclude_p3x
   use mod_meteo,  only : par_mean
#endif
#ifdef module_chemie
   use mod_chemie
#endif
#if defined module_restoring && defined exclude_restoring_cells_biogeo
   use mod_restoring, only: k_index_exclude_restoring
#endif
   implicit none
!
! !INPUT PARAMETERS:
   integer, intent(inout) :: ierr
!
! !LOCAL VARIABLES:
   real :: f_FromTo(max_flux)  !!! (mol m-3 d-1)!!
! NOTE: Fluxes are first calculated as "change-in-concentration" (e.g. mol m-3 d-1) locally (grid cell)
! and are stored in a local dummy array f_FromTo. Bevore applying to sst, these have to be transfered
! by vertical integration (*dz) to the public array f_from_to, which is defined as (mol m-2).
!
   real, parameter :: tres_n1p= 0.1    ! treshold value for uptake of phosphate by bacteria
   real, parameter :: tres_n4n= 0.1    ! treshold value for uptake of ammonium by bacteria
   real, parameter :: tres_pXn= 0.0    ! treshold value for phytoplankton losses and mortality
   real, parameter :: tres_zXn= 0.0    ! treshold value for zooplankton food concentration ("visibility")
   real, parameter :: eps     = 1.0e-6 ! number smaller than "TINY"
!
   real :: biop1c, biop2c, biop3c, bioz1c, bioz2c, biod1c, biod2c, biodoc, biobac
   real :: biop1n, biop2n, biop3n, bioz1n, bioz2n, biod1n, biod2n, biodon, bioban
   real :: biop1p, biop2p, biop3p, bioz1p, bioz2p, biod1p, biod2p, biodop, biobap
   real :: bion3n, bion4n, bion1p, bion5s, biop1s, biop3k, biod2s, biod2k, bioT
   real :: biop1a, biop2a, biop3a
   real :: rcnd1, rcpd1, rcndo, rcpdo, rccalc
   real :: dia_ups, dia_adds_loss, fy, f_PAR
#if !defined exclude_p1x || !defined exclude_p2x || !defined exclude_p3x
   real :: x1, nut_lim, lim_p
#endif
#ifndef exclude_p1x
   real :: limd_n, limd_np, lim_s, lim_nps
   real :: f_dic_p1c_red, x21, q11, q12, ploss1
#endif
#ifndef exclude_p2x
   real :: limf_n, limf_np
   real :: f_dic_p2c_red, x22, q21, q22, ploss2
#endif
#ifndef exclude_p3x
   real :: lim_calc, limc_n, limc_p, limc_np, limc_npc
   real :: f_dic_p3c_red, q31, q32, x13, x23, ploss3, plossk, rccalc_neu
   logical :: dop_uptake
#endif
#if !defined exclude_z1x || !defined exclude_z2x
   real :: pnenn, grnenn, zloss, mu_max, food
   real :: y1, y2, y3, y4, y5
#endif
#ifndef exclude_z1x
   real :: g1_p1n, g1_p2n, g1_p3n, g1_d1n, g1_ban
   real :: z1c_food, z1n_food, z1p_food
   real :: z1c_faec, z1n_faec, z1p_faec
   real :: z1c_loss, z1n_loss, z1p_loss
   real :: z1c_mort, z1n_mort, z1p_mort
   real :: z1n_pred, fzc1, fzn1, fzp1
#endif
#ifndef exclude_z2x
   real :: g2_p1n, g2_p2n, g2_p3n, g2_d1n, g2_ban, g2_z1n
   real :: z2c_food, z2n_food, z2p_food
   real :: z2c_faec, z2n_faec, z2p_faec
   real :: z2c_loss, z2n_loss, z2p_loss
   real :: z2c_mort, z2n_mort, z2p_mort
   real :: z2n_pred, fzc2, fzn2, fzp2, y6
#endif
#ifndef exclude_bac
   real :: fbc, fbn, fbpdiff, fbpmax, fbpreq, fdc_diff
#endif
   integer :: i, j, k, k0, n

!-------------------------------------------------------------------------
#include "call-trace.inc"

! #ifdef exclude_bac
!    ! hard coded remineralization rates used if bacteria are switched off (-Dexclude_bac)
!    rhoC = 0.372
!    rhoN = 0.240
!    rhoP = 0.240
! #endif

!    pnenn    = zero   ! to get rid of "unused" warning
!   y1=0.0;y2=0.0;y3=0.0;y4=0.0;y5=0.0;y6=0.0 ! to get rid of "unused" warning
   fy = 0.0   ! to get rid of "unused" warning

   ! determination of oxic-anoxic condition
   oswtch = 1
   where (st(:,:,iStartD:iEndD,io2o) <= 0.)  oswtch(:,:,iStartD:iEndD) = 0
   nswtch = 1
   where (st(:,:,iStartD:iEndD,in3n) <= 0.1) nswtch(:,:,iStartD:iEndD) = 0

   do i = iStartCalc, iEndCalc
      do j = jStartCalc, jEndCalc
         k0 = k_index(j,i)
#if defined module_restoring && defined exclude_restoring_cells_biogeo
         ! filter for any statevariable will be x1x mask !!
         k0 = k_index_exclude_restoring(j,i,ix1x)
#else
         k0 = k_index(j,i)
#endif
         do k = 1, k0
            ! write index string (usefull for debugging!)
            write(str_jki, fmt_jki) i, j, k

            ! initialize /reset local f_FromTo
            f_FromTo = zero

! KEEP IN CODE AS LINKER FOR (HISTORIC) FORMULATIONS IN ECOHAM4 !!!!
!             lim_p1_n1p(j,k,i) = q3(j,k,i)
!             lim_p2_n1p(j,k,i) = q3(j,k,i)
!             lim_p1_n3n(j,k,i) = q1d(j,k,i)
!             lim_p2_n3n(j,k,i) = q1f(j,k,i)
!             lim_p1_n4n(j,k,i) = q2d(j,k,i)
!             lim_p2_n4n(j,k,i) = q2f(j,k,i)
!             lim_p1_n5s(j,k,i) = q4(j,k,i)
!             lim_p1_hn(j,k,i) = q1d(j,k,i) + q2d(j,k,i)
!             lim_p2_hn(j,k,i) = q1f(j,k,i) + q2f(j,k,i)
!             lim_p3_n1p(j,k,i) = q3c(j,k,i)
!             lim_p3_n3n(j,k,i) = q1c(j,k,i)
!             lim_p3_n4n(j,k,i) = q2c(j,k,i)
!             lim_p3_hn(j,k,i) = q1c(j,k,i) + q2c(j,k,i)

            ! light limitation factor
            fPAR(j,k,i) = par(j,k,i)/Iopt(j,k,i)*exp(one-par(j,k,i)/Iopt(j,k,i))

           ! extract current gridpoint values
            f_PAR  = fPAR(j,k,i)
            bioT   = temp(j,k,i)
            biop1a = st(j,k,i,ip1a)
            biop1c = st(j,k,i,ip1c)
            biop1n = st(j,k,i,ip1n)
            biop1p = st(j,k,i,ip1p)
            biop1s = st(j,k,i,ip1s)
            biop2a = st(j,k,i,ip2a)
            biop2c = st(j,k,i,ip2c)
            biop2n = st(j,k,i,ip2n)
            biop2p = st(j,k,i,ip2p)
            biop3a = st(j,k,i,ip3a)
            biop3c = st(j,k,i,ip3c)
            biop3n = st(j,k,i,ip3n)
            biop3p = st(j,k,i,ip3p)
            biop3k = st(j,k,i,ip3k)
            bioz1c = st(j,k,i,iz1c)
            bioz1n = st(j,k,i,iz1c)/rcnz1
            bioz1p = st(j,k,i,iz1c)/rcpz1
            bioz2c = st(j,k,i,iz2c)
            bioz2n = st(j,k,i,iz2c)/rcnz2
            bioz2p = st(j,k,i,iz2c)/rcpz2
            biobac = st(j,k,i,ibac)
            bioban = st(j,k,i,ibac)/rcnb
            biobap = st(j,k,i,ibac)/rcpb
            biod1c = st(j,k,i,id1c)
            biod1n = st(j,k,i,id1n)
            biod1p = st(j,k,i,id1p)
            biod2c = st(j,k,i,id2c)
            biod2n = st(j,k,i,id2n)
            biod2p = st(j,k,i,id2p)
            biod2s = st(j,k,i,id2s)
            biodoc = st(j,k,i,idoc)
            biodon = st(j,k,i,idon)
            biodop = st(j,k,i,idop)
            biod2k = st(j,k,i,id2k)
            bion3n = st(j,k,i,in3n)
            bion4n = st(j,k,i,in4n)
            bion1p = st(j,k,i,in1p)
            bion5s = st(j,k,i,in5s)
#ifdef exclude_p1x
            biop1c = 1.0
            biop1n = 1.0
            biop1p = 1.0
            biop1s = 1.0
#endif
#ifdef exclude_p2x
            biop2c = 1.0
            biop2n = 1.0
            biop2p = 1.0
#endif
#ifdef exclude_p3x
            biop3c = 1.0
            biop3n = 1.0
            biop3p = 1.0
            biop3k = biop3c/q_c_cal
#endif

            rcnd1  = biod1c/biod1n    !  C/N Detritus (fast sinking)
            rcpd1  = biod1c/biod1p    !  C/P Detritus (fast sinking)
            rcndo  = biodoc/biodon    !  C/N DOM
            rcpdo  = biodoc/biodop    !  C/P DOM
            rccalc = biop3c/biop3k

#ifdef exclude_p1x
            !================================================================================
            ! PHYTOPLANKTON-p1* (switched-off)
            !================================================================================
            ! set preferences for zooplankton grazing (IMPORTANT, because p1-biomass may be different from zero!)
            p1_p1n = 0.
            p2_p1n = 0.
#else
            !================================================================================
            ! PHYTOPLANKTON
            !================================================================================
            ! DIATOMS (p1*)
            ! nutrient limitation factors
            x1  = bion3n/xk1
            x21 = bion4n/xk21
            lim_p1_n3n(j,k,i) = x1/(one+x1+x21)
            lim_p1_n4n(j,k,i) = x21/(one+x1+x21)
            lim_p1_n1p(j,k,i) = bion1p/(xkp +bion1p)
            lim_p1_n5s(j,k,i) = bion5s/(xks +bion5s)
            ! production
            if (bion3n .lt. TINY) lim_p1_n3n(j,k,i) = zero
            if (bion4n .lt. TINY) lim_p1_n4n(j,k,i) = zero
            limd_n  = lim_p1_n3n(j,k,i) + lim_p1_n4n(j,k,i)
            lim_p   = lim_p1_n1p(j,k,i)
            limd_np = min(limd_n,lim_p)
            lim_s   = lim_p1_n5s(j,k,i)
            lim_nps = min(limd_np,lim_s)
            q11 = lim_p1_n3n(j,k,i)/(limd_n+eps)
            q12 = lim_p1_n4n(j,k,i)/(limd_n+eps)
! print*,'#658',bion3n,bion4n
! print*,'#659',q11,lim_p1_n3n(j,k,i),lim_nps,limd_n
! print*,'#660',q12,lim_p1_n4n(j,k,i),lim_nps,limd_n
            nut_lim = max(lim_nps,real(iexcess))
            f_FromTo(i_dic_p1c) = biop1c*tfac1(j,k,i)*vp1*f_PAR*nut_lim                   ! primary production
            f_dic_p1c_red       = biop1c*tfac1(j,k,i)*vp1*f_PAR*lim_nps
            f_FromTo(i_dic_p1c) = f_dic_p1c_red+excess*(f_FromTo(i_dic_p1c)-f_dic_p1c_red)
! print*,'#663',biop1c/rcn,tfac1(j,k,i),vp1,f_PAR,q11,q12
            f_FromTo(i_n3n_p1n) = f_dic_p1c_red/rcn*q11
            f_FromTo(i_n4n_p1n) = f_dic_p1c_red/rcn*q12
            f_FromTo(i_n1p_p1p) = f_dic_p1c_red/rcp
            f_FromTo(i_n5s_p1s) = f_dic_p1c_red/rcs                                        ! skeleton silikon building
            ! losses
            fy = one
            if (biop1n.lt.tres_pXn) fy = zero ! treshold for mortality/loss niche
            f_FromTo(i_p1c_soc) = f_FromTo(i_dic_p1c)-f_dic_p1c_red
            f_FromTo(i_p1c_doc) = fy* gam1*f_dic_p1c_red
            f_FromTo(i_p1n_don) = fy* f_FromTo(i_p1c_doc)/rcn
            f_FromTo(i_p1p_dop) = fy* f_FromTo(i_p1c_doc)/rcp

            ploss1 = tfac(j,k,i)*xmu11*biop1n + xmq11*biop1n*biop1n
            if (biop1n.lt.eps) ploss1  = zero ! treshold for mortality niche
            f_FromTo(i_p1n_d1n) = fy* (one-frac_d2x)*ploss1
            f_FromTo(i_p1c_d1c) = fy* f_FromTo(i_p1n_d1n)*rcn
            f_FromTo(i_p1p_d1p) = fy* f_FromTo(i_p1c_d1c)/rcp
            f_FromTo(i_p1n_d2n) = fy* frac_d2x*ploss1
            f_FromTo(i_p1c_d2c) = fy* f_FromTo(i_p1n_d2n)*rcn
            f_FromTo(i_p1p_d2p) = fy* f_FromTo(i_p1c_d2c)/rcp
            f_FromTo(i_p1s_d2s) = fy* (f_FromTo(i_p1c_d1c)+f_FromTo(i_p1c_d2c))/rcs
            ! calculate diatom chl:C ratio after Cloern et al. 1995 (cl)
            q_chl_p1c(j,k,i) = (0.003 + max(0.,0.0154*exp(0.05*bioT)   &
                                *exp(-0.059*par_mean(j,k,i))*lim_nps))*12.0 ! g Chl (mol C)^(-1)
#endif  /* #ifdef exclude_p1x */

#ifdef exclude_p2x
            !================================================================================
            ! PHYTOPLANKTON-p2* (switched-off)
            !================================================================================
            ! set preferences for zooplankton grazing (IMPORTANT, because p2-biomass may be different from zero!)
            p1_p2n = 0.
            p2_p2n = 0.
#else
            !================================================================================
            ! PHYTOPLANKTON-p2* (FLAGGELATES)
            !================================================================================
            ! nutrient limitation factors
            x1  = bion3n/xk1
            x22 = bion4n/xk22
            lim_p2_n3n(j,k,i) = x1/(one+x1+x22)
            lim_p2_n4n(j,k,i) = x22/(one+x1+x22)
            lim_p2_n1p(j,k,i) = bion1p/(xkp +bion1p)
            ! production
            if (bion3n .lt. TINY) lim_p2_n3n(j,k,i) = zero
            if (bion4n .lt. TINY) lim_p2_n4n(j,k,i) = zero
            limf_n  = lim_p2_n3n(j,k,i)+lim_p2_n4n(j,k,i)
            lim_p   = lim_p2_n1p(j,k,i)
            limf_np = min(limf_n,lim_p)
            q21 = lim_p2_n3n(j,k,i)/(limf_n+eps)
            q22 = lim_p2_n4n(j,k,i)/(limf_n+eps)
            nut_lim = max(limf_np,real(iexcess))
            f_FromTo(i_dic_p2c) = biop2c*tfac2(j,k,i)*vp2*f_PAR*nut_lim      ! primary production
            f_dic_p2c_red       = biop2c*tfac2(j,k,i)*vp2*f_PAR*limf_np
            f_FromTo(i_dic_p2c) = f_dic_p2c_red+excess*(f_FromTo(i_dic_p2c)-f_dic_p2c_red)
            f_FromTo(i_dic_psk) = f_dic_p2c_red/q_c_cal                         ! skeleton carbonate building
            f_FromTo(i_n3n_p2n) = f_dic_p2c_red/rcn2*q21
            f_FromTo(i_n4n_p2n) = f_dic_p2c_red/rcn2*q22
            f_FromTo(i_n1p_p2p) = f_dic_p2c_red/rcp2
            ! losses
            fy = one
            if (biop2n.lt.tres_pXn) fy = zero ! treshold for mortality/loss niche
            f_FromTo(i_p2c_soc) = f_FromTo(i_dic_p2c)-f_dic_p2c_red
            f_FromTo(i_p2c_doc) = fy* gam2 * f_dic_p2c_red
            f_FromTo(i_p2n_don) = fy* f_FromTo(i_p2c_doc)/rcn2
            f_FromTo(i_p2p_dop) = fy* f_FromTo(i_p2c_doc)/rcp2

            ploss2 = tfac(j,k,i)*xmu12*biop2n + xmq12*biop2n*biop2n
            if (biop2n.lt.eps) ploss2  = zero ! treshold for mortality niche
            f_FromTo(i_p2n_d1n) = fy* (one-frac_d2x)*ploss2
            f_FromTo(i_p2c_d1c) = fy* f_FromTo(i_p2n_d1n)*rcn2
            f_FromTo(i_p2p_d1p) = fy* f_FromTo(i_p2c_d1c)/rcp2
            f_FromTo(i_p2n_d2n) = fy* frac_d2x*ploss2
            f_FromTo(i_p2c_d2c) = fy* f_FromTo(i_p2n_d2n)*rcn2
            f_FromTo(i_p2p_d2p) = fy* f_FromTo(i_p2c_d2c)/rcp2
            ! calculate flagellate chl:C ratio after Cloern et al. 1995 (cl)
            q_chl_p2c(j,k,i) = (0.003 + max(0.,0.0154*exp(0.05*bioT)  &
                                *exp(-0.059*par_mean(j,k,i))*limf_np))*12.0 ! g Chl (mol C)^(-1)
#endif  /* #ifdef exclude_p2x */

#ifdef exclude_p3x
            !================================================================================
            ! PHYTOPLANKTON-p3* (switched-off)
            !================================================================================
            ! set preferences for zooplankton grazing (IMPORTANT, because p3-biomass may be different from zero!)
            p1_p3n = 0.
            p2_p3n = 0.
#else
            !================================================================================
            ! PHYTOPLANKTON-p3* (COCCOLITHOPHORIDES)
            !================================================================================
            x1    = zero   ! to get rid of "unused" warning
            lim_p = zero   ! to get rid of "unused" warning
            ! nutrient limitation factors
            x13 = bion3n/xk13
            x23 = bion4n/xk23
            lim_p3_n3n(j,k,i) = x13/(one+x13+x23)
            lim_p3_n4n(j,k,i) = x23/(one+x13+x23)
            lim_p3_n1p(j,k,i) = bion1p/(xkp3+bion1p)
            !IL-P3:Neu: if n1p is limiting p3c is able to use dop as P source
            lim_p3_dop(j,k,i) = biodop/(xkp3+biodop)
            ! production
#ifdef module_chemie
            !IL-P3:Neu omega
            omega(j,k,i) = ch(j,k,i,io3c)*ca(j,k,i)/aksp(j,k,i)
#else
            omega(j,k,i) = zero
#endif
            if (bion3n .lt. TINY) lim_p3_n3n(j,k,i) = zero
            if (bion4n .lt. TINY) lim_p3_n4n(j,k,i) = zero
            limc_n = lim_p3_n3n(j,k,i) + lim_p3_n4n(j,k,i)
            !if n1p is limiting, p3c is able to use dop
            if((lim_p3_n1p(j,k,i)<=0.7).and.(lim_p3_dop(j,k,i)>=lim_p3_n1p(j,k,i))) dop_uptake=.true.  !0.7 chosen arbitrarily
            if (dop_uptake) then
               limc_p = lim_p3_dop(j,k,i)
            else
               limc_p = lim_p3_n1p(j,k,i)
            endif
            limc_np = min(limc_n,limc_p)
            if (omega(j,k,i)>1.0) then !Die Gefahr bei omega =0.6 durch null zu teilen muss abgefangen werden
               lim_calc = ((omega(j,k,i)-1.0)/((omega(j,k,i)-1.0)+xkk))
            else
               lim_calc = zero
            endif
            limc_npc = min(limc_np,lim_calc)
            q31 = lim_p3_n3n(j,k,i)/(limc_n+eps)
            q32 = lim_p3_n4n(j,k,i)/(limc_n+eps)
            nut_lim  = max(limc_np,real(iexcess))
            f_FromTo(i_dic_p3c) = biop3c     *tfac3(j,k,i)*vp3*fp3(j,k,i)*nut_lim         ! primary production
            f_dic_p3c_red       = biop3c     *tfac3(j,k,i)*vp3*fp3(j,k,i)*limc_npc
            f_FromTo(i_dic_p3c) = f_dic_p3c_red+excess*(f_FromTo(i_dic_p3c)-f_dic_p3c_red)*lim_calc
            f_FromTo(i_dic_p3k) = biop3c*tfac3(j,k,i)*fp3k(j,k,i)*c_max * lim_calc        !calcification
            f_FromTo(i_n3n_p3n) = f_dic_p3c_red/rcn3*q31
            f_FromTo(i_n4n_p3n) = f_dic_p3c_red/rcn3*q32
            if (dop_uptake) then
               f_FromTo(i_dop_p3p) = f_dic_p3c_red/rcp3
               f_FromTo(i_n1p_p3p) = zero
            else
               f_FromTo(i_dop_p3p) = zero
               f_FromTo(i_n1p_p3p) = f_dic_p3c_red/rcp3
            endif
            !losses
            fy = one
            if (biop1n.lt.tres_pXn) fy = zero ! treshold for mortality/loss niche
            f_FromTo(i_p3c_soc) = f_FromTo(i_dic_p3c) - f_dic_p3c_red
            f_FromTo(i_p3c_doc) = fy* gam3*f_FromTo(i_dic_p3c)
            f_FromTo(i_p3n_don) = fy* f_FromTo(i_p3c_doc)/rcn3
            f_FromTo(i_p3p_dop) = fy* f_FromTo(i_p3c_doc)/rcp3

            ploss3 = tfac(j,k,i)*xmu13*biop3n + xmq13*biop3n*biop3n
            if (biop3n.lt.eps) ploss3 = zero ! treshold for mortality niche
            f_FromTo(i_p3n_d1n) = fy* (one-frac_d2x)*ploss3
            f_FromTo(i_p3c_d1c) = fy* f_FromTo(i_p3n_d1n)*rcn3
            f_FromTo(i_p3p_d1p) = fy* f_FromTo(i_p3c_d1c)/rcp3
            f_FromTo(i_p3n_d2n) = fy* frac_d2x*ploss3
            f_FromTo(i_p3c_d2c) = fy* f_FromTo(i_p3n_d2n)*rcn3
            f_FromTo(i_p3p_d2p) = fy* f_FromTo(i_p3c_d2c)/rcp3
            !loss of calcite
            !if a maximum of coccoliths is reached, then all surplus coccospheres are dettached,
            !else, only 10 % are dettached(Tyrell&Taylor)
            rccalc_neu = (biop3c +f_FromTo(i_dic_p3c) -f_FromTo(i_p3c_doc)         &
                                 -f_FromTo(i_p3c_soc) -f_FromTo(i_p3c_d1c)         &
                                 -f_FromTo(i_p3c_d2c))/(biop3k+f_FromTo(i_dic_p3k))

            if (rccalc_neu<rccalc_min) then
               plossk = biop3k +f_FromTo(i_dic_p3k) -((biop3c+f_FromTo(i_dic_p3c)  &
                               -f_FromTo(i_p3c_doc) -f_FromTo(i_p3c_soc)           &
                               -f_FromTo(i_p3c_d1c) -f_FromTo(i_p3c_d2c))/rccalc_min)
               !rccalc = rccalc_min
            else
               !plossk = detach_min*biop3k + (f_FromTo(i_p3c_d1c)+f_FromTo(i_p3c_d2c))/rccalc_neu
               plossk = detach_min*biop3k + (f_FromTo(i_p3c_d1c)+f_FromTo(i_p3c_d2c))/rccalc
               !rccalc = rccalc_neu
            endif
            f_FromTo(i_p3k_d2k) = max(zero,plossk)
            ! calculate cocolithophores chl:C ratio after Cloern et al. 1995 (cl)
            q_chl_p3c(j,k,i) = (0.003 + max(0.,0.0154*exp(0.05*bioT) &
                                       *exp(-0.059*par_mean(j,k,i))*limc_np))*12.0 ! g Chl (mol C)^(-1)
#endif  /* #ifdef exclude_p3x */

#ifndef exclude_z1x
      !================================================================================
      ! MICRO-ZOOPLANKTON (z1*)
      !================================================================================
#ifdef exclude_bac
            p1_ban = 0.
#endif
#ifdef fasham_grazing
      ! grazing formulation originally from Fasham et al. 1990
            pnenn = one/(p1_p1n*biop1n+p1_p2n*biop2n+p1_p3n*biop3n+p1_d1n*biod1n+p1_ban*bioban)
            y1 = p1_p1n*biop1n*pnenn
            y2 = p1_p2n*biop2n*pnenn
            y3 = p1_p3n*biop3n*pnenn
            y4 = p1_d1n*biod1n*pnenn
            y5 = p1_ban*bioban*pnenn
            food = y1*biop1n + y2*biop2n + y3*biop3n + y4*biod1n + y5*bioban
            grnenn = one/(xk31+food)
            g1_p1n = tfac_z1(j,k,i)*g1_max *y1*max(zero,biop1n-tres_zXn)*grnenn *bioz1n       ! microzoo->diat
            g1_p2n = tfac_z1(j,k,i)*g1_max *y2*max(zero,biop2n-tres_zXn)*grnenn *bioz1n       ! microzoo->flag
            g1_p3n = tfac_z1(j,k,i)*g1_max *y3*max(zero,biop3n-tres_zXn)*grnenn *bioz1n       ! microzoo->coco
            g1_d1n = tfac_z1(j,k,i)*g1_max *y4*max(zero,biod1n-tres_zXn)*grnenn *bioz1n       ! microzoo->det1
            g1_ban = tfac_z1(j,k,i)*g1_max *y5*max(zero,bioban-tres_zXn)*grnenn *bioz1n       ! microzoo->bac
#else
      ! grazing according to Gentleman et al. 2003 (Table 2a, 3a)
            g1_exp = 1. !Hollig type II,  "Gentleman"-Class I(b)
            g1_exp = 2. !Hollig type III, "Gentleman"-Class II(c)
            food = p1_p1n*(biop1n**g1_exp) +p1_p2n*(biop2n**g1_exp) +p1_p3n*(biop3n**g1_exp) &
                  +p1_d1n*(biod1n**g1_exp) +p1_ban*(bioban**g1_exp)
            grnenn = (xk31**g1_exp)+food
            g1_p1n = tfac_z1(j,k,i)*g1_max *p1_p1n*(max(zero,biop1n-tres_zXn)**g1_exp)/grnenn *bioz1n    ! microzoo->dia
            g1_p2n = tfac_z1(j,k,i)*g1_max *p1_p2n*(max(zero,biop2n-tres_zXn)**g1_exp)/grnenn *bioz1n    ! microzoo->fla
            g1_p3n = tfac_z1(j,k,i)*g1_max *p1_p3n*(max(zero,biop3n-tres_zXn)**g1_exp)/grnenn *bioz1n    ! microzoo->cocos
            g1_d1n = tfac_z1(j,k,i)*g1_max *p1_d1n*(max(zero,biod1n-tres_zXn)**g1_exp)/grnenn *bioz1n    ! microzoo->det1
            g1_ban = tfac_z1(j,k,i)*g1_max *p1_ban*(max(zero,bioban-tres_zXn)**g1_exp)/grnenn *bioz1n    ! microzoo->bac
#endif
            !fluxes
#ifdef variable_phytoplankton_stoichiometry
            f_FromTo(i_p1c_z1c) = g1_p1n*biop1c/biop1n
            f_FromTo(i_p1n_z1n) = g1_p1n
            f_FromTo(i_p1p_z1p) = g1_p1n*biop1p/biop1n
            f_FromTo(i_p2c_z1c) = g1_p2n*biop2c/biop2n
            f_FromTo(i_p2n_z1n) = g1_p2n
            f_FromTo(i_p2p_z1p) = g1_p2n*biop2p/biop2n
            f_FromTo(i_p3c_z1c) = g1_p3n*biop3c/biop3n
            f_FromTo(i_p3n_z1n) = g1_p3n
            f_FromTo(i_p3p_z1p) = g1_p3n*biop3p/biop3n
#else
      ! for comparison with D093:
            f_FromTo(i_p1c_z1c) = g1_p1n*rcn
            f_FromTo(i_p1n_z1n) = g1_p1n
            f_FromTo(i_p1p_z1p) = g1_p1n*rcn/rcp
            f_FromTo(i_p2c_z1c) = g1_p2n*rcn2
            f_FromTo(i_p2n_z1n) = g1_p2n
            f_FromTo(i_p2p_z1p) = g1_p2n*rcn2/rcp2
            f_FromTo(i_p3c_z1c) = g1_p3n*rcn3
            f_FromTo(i_p3n_z1n) = g1_p3n
            f_FromTo(i_p3p_z1p) = g1_p3n*rcn3/rcp3
#endif
            f_FromTo(i_d1c_z1c) = g1_d1n*biod1c/biod1n
            f_FromTo(i_d1n_z1n) = g1_d1n
            f_FromTo(i_d1p_z1p) = g1_d1n*biod1p/biod1n
            f_FromTo(i_bac_z1c) = g1_ban*biobac/bioban
            f_FromTo(i_ban_z1n) = g1_ban
            f_FromTo(i_bap_z1p) = g1_ban*biobap/bioban

            f_FromTo(i_p3k_z1c) = f_FromTo(i_p3c_z1c)/rccalc  !Amount of attached calcite that is ingested by zooplankton
            !f_FromTo(i_z1c_d2k) = f_FromTo(i_p3k_z1c)        !is leaving the zooplankton gut without changing
      ! total uptake
            z1c_food = f_FromTo(i_p1c_z1c) +f_FromTo(i_p2c_z1c) +f_FromTo(i_p3c_z1c) &
                      +f_FromTo(i_d1c_z1c) +f_FromTo(i_bac_z1c)
            z1n_food = f_FromTo(i_p1n_z1n) +f_FromTo(i_p2n_z1n) +f_FromTo(i_p3n_z1n) &
                      +f_FromTo(i_d1n_z1n) +f_FromTo(i_ban_z1n)
            z1p_food = f_FromTo(i_p1p_z1p) +f_FromTo(i_p2p_z1p) +f_FromTo(i_p3p_z1p) &
                      +f_FromTo(i_d1p_z1p) +f_FromTo(i_bap_z1p)

      ! losses microzooplankton
      !======================================================================================================
      ! fecal pellets
            ! C:N:P of feaces is related to the stoichiometry of food and assimilation efficiencies
            ! (beta). If beta's are equal, faeces have same C:N:P as combined food
            z1c_faec = (one-beta1)*z1c_food
            z1n_faec = (one-beta1)*z1n_food
            z1p_faec = (one-beta1)*z1p_food
       ! predation pressure (NEW)
            z1n_pred = zero
#ifdef explicit_predation
       ! place to implement explicit formulations/sources for predation
#endif
       ! mortality (NOTE: Fasham formulations don't separate metabolic losses from mortality!)
#ifdef fasham_mortality
            !density dependent mortality (according to Fasham et al. 1990)
            z1n_mort = z1n_pred + tfac_z1(j,k,i) *xmu21*bioz1n/(xk16+bioz1n) *bioz1n
#else
            !linear + quadratic mortality closure
            !z1n_mort = z1n_pred + tfac_z1(j,k,i) *(xmu21                     +xmq21*bioz1n) *bioz1n
            ! following formulation does the same job with respect to choice of parameters (e.g. xk16=0),
            ! but additionally allows for keeking "Fasham"-like behavior at low concentrations
            z1n_mort = z1n_pred + tfac_z1(j,k,i) *(xmu21*bioz1n/(xk16+bioz1n)+xmq21*bioz1n) *bioz1n
#endif
            z1c_mort = z1n_mort*rcnz1           ! mortality/predation must have C/N of zooplankton !!
            z1p_mort = z1n_mort*rcnz1/rcpz1     ! mortality/predation must have N/P of zooplankton !!

#ifdef fasham_losses
#ifdef fasham_losses_revised
            !----------------------------------------------------------------------------------
            ! fasham losses revised (markus version 2010)
            !----------------------------------------------------------------------------------
            ! re-calculation of predation-loss (mortality) based on assumptions given by Fasham:
            ! - there is no matter unused by higher trophic levels, thus all mortality
            !   is due to predation (even if dead zooplankton has been eaten) and
            !   everything must be split up to DIM, DOM and detritus
            ! - assuming GGE of 25% and AE of 75%, the infinity series of predators lead
            !   to 33%-egested detritus (:=frac_det) and 66%-excretion/respiration (:=(1-frac_det))
            ! - whithin bulk "fasham-losses" (zloss), predation is the only way to produce detritus
            !   via egestion. Thus, the total fraction of predation on "fasham-losses" is given by
            !   mort = (1-aeps-delta)/frac_det* zloss
            zloss = z1n_mort
            z1n_mort = (one-aeps1-delta_don1)/frac_det*zloss
            z1c_mort = z1n_mort*rcnz2         ! mortality/predation must have C/N = rcnz !!
            z1p_mort = z1n_mort*rcnz2/rcpz2   ! mortality/predation must have N/P = rcpz/rcnz !!
            ! calculate metabolic loss
            z1n_loss = zloss - z1n_mort
            ! calculate growth rate related to organism's N-budget
            ! growth = ingested -egested -metabolism
            mu_max = (z1n_food-z1n_faec-(zloss-z1n_mort))/bioz1n -xmu6
            !metabolic losses = ingested -egested -growth
            z1c_loss = max(zero, z1c_food -z1c_faec -mu_max*bioz1c)
            z1n_loss = max(zero, z1n_food -z1n_faec -mu_max*bioz1n)
            z1p_loss = max(zero, z1p_food -z1p_faec -mu_max*bioz1p)
            !---------------------------------------------------------------------------
            ! carbon-fluxes
            f_FromTo(i_z1c_dic) = (z1c_loss + (one-frac_det)*z1c_mort) *(one-frac_dic)
            f_FromTo(i_z1c_doc) = (z1c_loss + (one-frac_det)*z1c_mort) *     frac_dic
            f_FromTo(i_z1c_d1c) = (z1c_faec +      frac_det *z1c_mort) *(one-frac_d2x)
            f_FromTo(i_z1c_d2c) = (z1c_faec +      frac_det *z1c_mort) *     frac_d2x
            ! nitrogen-fluxes
            f_FromTo(i_z1n_don) = (z1n_loss + (one-frac_det)*z1n_mort) *(one-frac_n4n)
            f_FromTo(i_z1n_n4n) = (z1n_loss + (one-frac_det)*z1n_mort) *     frac_n4n
            f_FromTo(i_z1n_d1n) = (z1n_faec +      frac_det *z1n_mort) *(one-frac_d2x)
            f_FromTo(i_z1n_d2n) = (z1n_faec +      frac_det *z1n_mort) *     frac_d2x
            ! phosphorous-fluxes
            f_FromTo(i_z1p_dop) = (z1p_loss + (one-frac_det)*z1p_mort) *(one-frac_n1p)
            f_FromTo(i_z1p_n1p) = (z1p_loss + (one-frac_det)*z1p_mort) *     frac_n1p
            f_FromTo(i_z1p_d1p) = (z1p_faec +      frac_det *z1p_mort) *(one-frac_d2x)
            f_FromTo(i_z1p_d2p) = (z1p_faec +      frac_det *z1p_mort) *     frac_d2x
#else
            !----------------------------------------------------------------------------------
            ! ECOHAM4-Version (according to D093)
            !----------------------------------------------------------------------------------
            mu_max   = zero   ! to get rid of "unused" warning
            zloss    = zero   ! to get rid of "unused" warning
            z1c_loss = zero
            z1n_loss = zero
            z1p_loss = zero
#ifdef old_feces_stoichiometry
            ! feces have C:N:P from zooplankton (according to run D093)
            z1c_faec = z1n_faec*rcnz1
            z1p_faec = z1n_faec*rcnz1/rcpz1
#endif
      ! parameterization is based on nitrogen
            f_FromTo(i_z1n_d1n) = ((z1n_loss+z1n_mort)*(one-aeps1-delta_don1) + z1n_faec) *(one-frac_d2x)
            f_FromTo(i_z1n_d2n) = ((z1n_loss+z1n_mort)*(one-aeps1-delta_don1) + z1n_faec) *frac_d2x
            f_FromTo(i_z1n_n4n) =  (z1n_loss+z1n_mort)*aeps1
            f_FromTo(i_z1n_don) =  (z1n_loss+z1n_mort)*delta_don1
      ! carbon and phosphorous fluxes
            f_FromTo(i_z1c_d1c) = ((z1c_loss+z1c_mort)*(one-aeps1-delta_don1) + z1c_faec) *(one-frac_d2x)
            f_FromTo(i_z1c_d2c) = ((z1c_loss+z1c_mort)*(one-aeps1-delta_don1) + z1c_faec) *frac_d2x
            f_FromTo(i_z1c_dic) =  (z1c_loss+z1c_mort)*aeps1
            f_FromTo(i_z1c_doc) =  (z1c_loss+z1c_mort)*delta_don1
            f_FromTo(i_z1p_d1p) = ((z1p_loss+z1p_mort)*(one-aeps1-delta_don1) + z1p_faec) *(one-frac_d2x)
            f_FromTo(i_z1p_d2p) = ((z1p_loss+z1p_mort)*(one-aeps1-delta_don1) + z1p_faec) *frac_d2x
            f_FromTo(i_z1p_n1p) =  (z1p_loss+z1p_mort)*aeps1
            f_FromTo(i_z1p_dop) =  (z1p_loss+z1p_mort)*delta_don1
#endif
! #ifdef fasham_losses coded above !
#else
!----------------------------------------------------------------------------------------------
! ATTENTION!!! New formulations strictly separate metabolic losses from mortality / predation !
!----------------------------------------------------------------------------------------------
            zloss    = zero   ! to get rid of "unused" warning
            !----------------------------------------------------------------------------------
            ! following suggestion from book Sterner&Elser(2002)-pp197
            !----------------------------------------------------------------------------------
            ! calculate potential growth rate related to most sparse food-element(-currency)
            ! xmu6c represents energy required for feeding-activities & biosynthesis
            mu_max = min((one-xmu6c)*(z1c_food-z1c_faec)/bioz1c - xmu6, &
                                     (z1n_food-z1n_faec)/bioz1n - xmu6, &
                                     (z1p_food-z1p_faec)/bioz1p - xmu6)
            ! calculate metabolic loss rates, assuming that any "excess" uptake is respired/excreted:
            !metabolic losses = ingested -egested -growth
            z1c_loss = max(zero, z1c_food -z1c_faec -mu_max*bioz1c)
            z1n_loss = max(zero, z1n_food -z1n_faec -mu_max*bioz1n)
            z1p_loss = max(zero, z1p_food -z1p_faec -mu_max*bioz1p)
            !---------------------------------------------------------------------------
            ! NOTE:
            ! further, assimilation efficiencies could be related to match-missmatch of
            ! C:N:P-ratios of prey and zooplankton  -> not present jet !!!
            !---------------------------------------------------------------------------
            ! carbon-fluxes
            f_FromTo(i_z1c_dic) = (z1c_loss + (one-frac_det)*z1c_mort) *(one-frac_dic)
            f_FromTo(i_z1c_doc) = (z1c_loss + (one-frac_det)*z1c_mort) *     frac_dic
            f_FromTo(i_z1c_d1c) = (z1c_faec +      frac_det *z1c_mort) *(one-frac_d2x)
            f_FromTo(i_z1c_d2c) = (z1c_faec +      frac_det *z1c_mort) *     frac_d2x
            ! nitrogen-fluxes
            f_FromTo(i_z1n_don) = (z1n_loss + (one-frac_det)*z1n_mort) *(one-frac_n4n)
            f_FromTo(i_z1n_n4n) = (z1n_loss + (one-frac_det)*z1n_mort) *     frac_n4n
            f_FromTo(i_z1n_d1n) = (z1n_faec +      frac_det *z1n_mort) *(one-frac_d2x)
            f_FromTo(i_z1n_d2n) = (z1n_faec +      frac_det *z1n_mort) *     frac_d2x
            ! phosphorous-fluxes
            f_FromTo(i_z1p_dop) = (z1p_loss + (one-frac_det)*z1p_mort) *(one-frac_n1p)
            f_FromTo(i_z1p_n1p) = (z1p_loss + (one-frac_det)*z1p_mort) *     frac_n1p
            f_FromTo(i_z1p_d1p) = (z1p_faec +      frac_det *z1p_mort) *(one-frac_d2x)
            f_FromTo(i_z1p_d2p) = (z1p_faec +      frac_det *z1p_mort) *     frac_d2x
#endif

      ! balance fluxes for micro-zooplankton
      ! unbalanced net C flux into micro-zooplankton
            fzc1 = z1c_food -f_FromTo(i_z1c_d1c) -f_FromTo(i_z1c_d2c) &
                            -f_FromTo(i_z1c_doc) -f_FromTo(i_z1c_dic)
      ! unbalanced net N flux into micro-zooplankton
            fzn1 = z1n_food -f_FromTo(i_z1n_d1n) -f_FromTo(i_z1n_d2n) &
                            -f_FromTo(i_z1n_don) -f_FromTo(i_z1n_n4n)
      ! unbalanced net P flux into micro-zooplankton
            fzp1 = z1p_food -f_FromTo(i_z1p_d1p) -f_FromTo(i_z1p_d2p) &
                            -f_FromTo(i_z1p_dop) -f_FromTo(i_z1p_n1p)
            if ((abs(fzc1-rcnz1*fzn1)<1.0e-12) .and. (abs(fzc1-rcpz1*fzp1)<1.0e-12)) then
               !print*,'!everything is fine!'
               !print*,(fzc1-rcnz1*fzn1),(fzc1-rcpz1*fzp1),fzc1,rcnz1*fzn1,rcpz1*fzp1
            else
#ifdef debug_zoo
               if ((fzc1 <= rcnz1*fzn1) .and. (fzc1 <= rcpz1*fzp1)) then
                  write(long_msg,'("[",i2.2,"] microzoo",a,": zuviel N und P",3e20.10)') &
                                 myID,trim(str_jki),fzc1,rcnz1*fzn1,rcpz1*fzp1
                  call write_log_message(long_msg); long_msg = ''
               else if ((fzc1 >= rcnz1*fzn1) .and. (fzn1 <= rcpz1/rcnz1*fzp1)) then !Zuviel C und P
                  write(long_msg,'("[",i2.2,"] microzoo",a,": zuviel C und P",3e20.10)') &
                                 myID,trim(str_jki),fzc1,rcnz1*fzn1,rcpz1*fzp1
                  call write_log_message(long_msg); long_msg = ''
               else if ((fzc1 >= rcpz1*fzp1) .and. (fzn1 >= rcpz1/rcnz1*fzp1)) then !Zuviel C und N
                  write(long_msg,'("[",i2.2,"] microzoo",a,": zuviel C und N",3e20.10)') &
                                 myID,trim(str_jki),fzc1,rcnz1*fzn1,rcpz1*fzp1
                  call write_log_message(long_msg); long_msg = ''
               endif
#endif
            ! compensation of unbalanced into meso-zooplankton
               if ((fzc1 <= rcnz1*fzn1) .and. (fzc1 <= rcpz1*fzp1)) then !Zuviel N und P
                  f_FromTo(i_z1n_don) = f_FromTo(i_z1n_don)+(fzn1-fzc1/rcnz1)/2.
                  f_FromTo(i_z1n_n4n) = f_FromTo(i_z1n_n4n)+(fzn1-fzc1/rcnz1)/2.
                  f_FromTo(i_z1p_dop) = f_FromTo(i_z1p_dop)+(fzp1-fzc1/rcpz1)/2.
                  f_FromTo(i_z1p_n1p) = f_FromTo(i_z1p_n1p)+(fzp1-fzc1/rcpz1)/2.
               else if ((fzc1 >= rcnz1*fzn1) .and. (fzn1 <= rcpz1/rcnz1*fzp1)) then !Zuviel C und P
                  f_FromTo(i_z1c_dic) = f_FromTo(i_z1c_dic)+(fzc1-fzn1*rcnz1)
                  f_FromTo(i_z1p_dop) = f_FromTo(i_z1p_dop)+(fzp1-fzn1*rcnz1/rcpz1)/2.
                  f_FromTo(i_z1p_n1p) = f_FromTo(i_z1p_n1p)+(fzp1-fzn1*rcnz1/rcpz1)/2.
               else if ((fzc1 >= rcpz1*fzp1) .and. (fzn1 >= rcpz1/rcnz1*fzp1)) then !Zuviel C und N
                  f_FromTo(i_z1c_dic) = f_FromTo(i_z1c_dic)+(fzc1-fzp1*rcpz1)
                  f_FromTo(i_z1n_don) = f_FromTo(i_z1n_don)+(fzn1-fzp1*rcpz1/rcnz1)/2.
                  f_FromTo(i_z1n_n4n) = f_FromTo(i_z1n_n4n)+(fzn1-fzp1*rcpz1/rcnz1)/2.
               else
                  !print*,myID,'microzoo: ',fzc1,fzn1*rcnz1,fzp1*rcpz1
                  ierr = 1
                  write(fmtstr,'(a)') '("[",i2.2,"] - Fehler bei micro-zooplankton flux",a)'
                  write(error_message,trim(fmtstr)) myID, trim(str_jki)
               endif
            endif
#endif /* #ifdef exclude_z1x */

#ifndef exclude_z2x
      !================================================================================
      ! MESO-ZOOPLANKTON (z2*)
      !================================================================================
#ifdef exclude_bac
            p2_ban = 0.
#endif
#ifdef fasham_grazing
      ! grazing formulation originally from Fasham et al. 1990
            pnenn = one/(p2_p1n*biop1n+p2_p2n*biop2n+p2_p3n*biop3n+p2_d1n*biod1n+p2_ban*bioban+p2_z1n*bioz1n)
            y1 = p2_p1n*biop1n*pnenn
            y2 = p2_p2n*biop2n*pnenn
            y3 = p2_p3n*biop3n*pnenn
            y4 = p2_d1n*biod1n*pnenn
            y5 = p2_ban*bioban*pnenn
            y6 = p2_z1n*bioz1n*pnenn
            food = y1*biop1n + y2*biop2n + y3*biop3n + y4*biod1n + y5*bioban + y6*bioz1n
            grnenn = one/(xk32+food)
            g2_p1n = tfac_z2(j,k,i)*g2_max *y1*max(zero,biop1n-tres_zXn)*grnenn *bioz2n       ! mesozoo->diat
            g2_p2n = tfac_z2(j,k,i)*g2_max *y2*max(zero,biop2n-tres_zXn)*grnenn *bioz2n       ! mesozoo->flag
            g2_p3n = tfac_z2(j,k,i)*g2_max *y3*max(zero,biop3n-tres_zXn)*grnenn *bioz2n       ! mesozoo->coco
            g2_d1n = tfac_z2(j,k,i)*g2_max *y4*max(zero,biod1n-tres_zXn)*grnenn *bioz2n       ! mesozoo->det1
            g2_ban = tfac_z2(j,k,i)*g2_max *y5*max(zero,bioban-tres_zXn)*grnenn *bioz2n       ! mesozoo->bac
            g2_z1n = tfac_z2(j,k,i)*g2_max *y6*max(zero,bioz1n-tres_zXn)*grnenn *bioz2n       ! mesozoo->micro
#else
      ! grazing according to Gentleman et al. 2003 (Table 2a, 3a)
            g2_exp=1. !Hollig type II,  "Gentleman"-Class I(b)
            g2_exp=2. !Hollig type III, "Gentleman"-Class II(c)
            food = p2_p1n*(biop1n**g2_exp) +p2_p2n*(biop2n**g2_exp) +p2_p3n*(biop3n**g2_exp) &
                  +p2_d1n*(biod1n**g2_exp) +p2_ban*(bioban**g2_exp) +p2_z1n*(bioz1n**g2_exp)
            grnenn = (xk32**g2_exp)+food
            g2_p1n = tfac_z2(j,k,i)*g2_max *p2_p1n*(max(zero,biop1n-tres_zXn)**g2_exp)/grnenn *bioz2n    ! mesozoo->dia
            g2_p2n = tfac_z2(j,k,i)*g2_max *p2_p2n*(max(zero,biop2n-tres_zXn)**g2_exp)/grnenn *bioz2n    ! mesozoo->fla
            g2_p3n = tfac_z2(j,k,i)*g2_max *p2_p3n*(max(zero,biop3n-tres_zXn)**g2_exp)/grnenn *bioz2n    ! mesozoo->cocos
            g2_d1n = tfac_z2(j,k,i)*g2_max *p2_d1n*(max(zero,biod1n-tres_zXn)**g2_exp)/grnenn *bioz2n    ! mesozoo->det1
            g2_ban = tfac_z2(j,k,i)*g2_max *p2_ban*(max(zero,bioban-tres_zXn)**g2_exp)/grnenn *bioz2n    ! mesozoo->bac
            g2_z1n = tfac_z2(j,k,i)*g2_max *p2_z1n*(max(zero,bioz1n-tres_zXn)**g2_exp)/grnenn *bioz2n    ! mesozoo->micro
#endif
            !fluxes
#ifdef variable_phytoplankton_stoichiometry
            f_FromTo(i_p1c_z2c) = g2_p1n*biop1c/biop1n
            f_FromTo(i_p1n_z2n) = g2_p1n
            f_FromTo(i_p1p_z2p) = g2_p1n*biop1p/biop1n
            f_FromTo(i_p2c_z2c) = g2_p2n*biop2c/biop2n
            f_FromTo(i_p2n_z2n) = g2_p2n
            f_FromTo(i_p2p_z2p) = g2_p2n*biop2p/biop2n
            f_FromTo(i_p3c_z2c) = g2_p3n*biop3c/biop3n
            f_FromTo(i_p3n_z2n) = g2_p3n
            f_FromTo(i_p3p_z2p) = g2_p3n*biop3p/biop3n
#else
      ! for comparison with D093:
            f_FromTo(i_p1c_z2c) = g2_p1n*rcn
            f_FromTo(i_p1n_z2n) = g2_p1n
            f_FromTo(i_p1p_z2p) = g2_p1n*rcn/rcp
            f_FromTo(i_p2c_z2c) = g2_p2n*rcn2
            f_FromTo(i_p2n_z2n) = g2_p2n
            f_FromTo(i_p2p_z2p) = g2_p2n*rcn2/rcp2
            f_FromTo(i_p3c_z2c) = g2_p3n*rcn3
            f_FromTo(i_p3n_z2n) = g2_p3n
            f_FromTo(i_p3p_z2p) = g2_p3n*rcn3/rcp3
#endif
            f_FromTo(i_d1c_z2c) = g2_d1n*biod1c/biod1n
            f_FromTo(i_d1n_z2n) = g2_d1n
            f_FromTo(i_d1p_z2p) = g2_d1n*biod1p/biod1n
            f_FromTo(i_bac_z2c) = g2_ban*biobac/bioban
            f_FromTo(i_ban_z2n) = g2_ban
            f_FromTo(i_bap_z2p) = g2_ban*biobap/bioban
            f_FromTo(i_z1c_z2c) = g2_z1n*bioz1c/bioz1n
            f_FromTo(i_z1n_z2n) = g2_z1n
            f_FromTo(i_z1p_z2p) = g2_z1n*bioz1p/bioz1n
            f_FromTo(i_p3k_z2c) = f_FromTo(i_p3c_z2c)/rccalc  !Amount of attached calcite that is ingested by zooplankton
            !f_FromTo(i_z2c_d2k)= f_FromTo(i_p3k_z2c)         !is leaving the zooplankton gut without changing
      ! total uptake
            z2c_food = f_FromTo(i_p1c_z2c) +f_FromTo(i_p2c_z2c) +f_FromTo(i_p3c_z2c) &
                      +f_FromTo(i_d1c_z2c) +f_FromTo(i_bac_z2c) +f_FromTo(i_z1c_z2c)
            z2n_food = f_FromTo(i_p1n_z2n) +f_FromTo(i_p2n_z2n) +f_FromTo(i_p3n_z2n) &
                      +f_FromTo(i_d1n_z2n) +f_FromTo(i_ban_z2n) +f_FromTo(i_z1n_z2n)
            z2p_food = f_FromTo(i_p1p_z2p) +f_FromTo(i_p2p_z2p) +f_FromTo(i_p3p_z2p) &
                      +f_FromTo(i_d1p_z2p) +f_FromTo(i_bap_z2p) +f_FromTo(i_z1p_z2p)

      ! losses mesozooplankton
      !======================================================================================================
      ! fecal pellets
            ! C:N:P of feaces is related to the stoichiometry of food and assimilation efficiencies
            ! (beta). If beta's are equal, faeces have same C:N:P as combined food
            z2c_faec = (one-beta2)*z2c_food
            z2n_faec = (one-beta2)*z2n_food
            z2p_faec = (one-beta2)*z2p_food
       ! predation pressure (NEW)
            z2n_pred = zero
#ifdef explicit_predation
       ! place to implement explicit formulations/sources for predation
#endif
       ! mortality (NOTE: Fasham formulations don't separate metabolic losses from mortality!)
#ifdef fasham_mortality
            !density dependent mortality (according to Fasham et al. 1990)
            z2n_mort = z2n_pred + tfac_z2(j,k,i) *xmu22*bioz2n/(xk26+bioz2n) *bioz2n
#else
            !linear + quadratic mortality closure
            !z1n_mort = z2n_pred + tfac_z2(j,k,i) *(xmu22                     +xmq22*bioz2n) *bioz2n
            ! following formulation does the same job with respect to choice of parameters (e.g. xk26=0),
            ! but additionally allows for keeking "Fasham"-like behavior at low concentrations
            z2n_mort = z2n_pred + tfac_z2(j,k,i) *(xmu22*bioz2n/(xk26+bioz2n)+xmq22*bioz2n) *bioz2n
#endif
            z2c_mort = z2n_mort*rcnz2           ! mortality/predation must have C/N of zooplankton !!
            z2p_mort = z2n_mort*rcnz2/rcpz2     ! mortality/predation must have N/P of zooplankton !!

#ifdef fasham_losses
#ifdef fasham_losses_revised
            !----------------------------------------------------------------------------------
            ! fasham losses revised (markus version 2010)
            !----------------------------------------------------------------------------------
            ! re-calculation of predation-loss (mortality) based on assumptions given by Fasham:
            ! - there is no matter unused by higher trophic levels, thus all mortality
            !   is due to predation (even if dead zooplankton has been eaten) and
            !   everything must be split up to DIM, DOM and detritus
            ! - assuming GGE of 25% and AE of 75%, the infinity series of predators lead
            !   to 33%-egested detritus (:=frac_det) and 66%-excretion/respiration (:=(1-frac_det))
            ! - whithin bulk "fasham-losses" (zloss), predation is the only way to produce detritus
            !   via egestion. Thus, the total fraction of predation on "fasham-losses" is given by
            !   mort = (1-aeps-delta)/frac_det* zloss
            zloss = z2n_mort
            z2n_mort = (one-aeps2-delta_don2)/frac_det*zloss
            z2c_mort = z2n_mort*rcnz2         ! mortality/predation must have C/N = rcnz !!
            z2p_mort = z2n_mort*rcnz2/rcpz2   ! mortality/predation must have N/P = rcpz/rcnz !!
            ! calculate metabolic loss
            z2n_loss = zloss - z2n_mort
            ! calculate growth rate related to organism's N-budget
            ! growth = ingested -egested -metabolism -basal metabolism
            mu_max = (z2n_food-z2n_faec-(zloss-z2n_mort))/bioz2n -xmu6
            !metabolic losses = ingested -egested -growth
            z2c_loss = z2c_food -z2c_faec -mu_max*bioz2c
            z2n_loss = z2n_food -z2n_faec -mu_max*bioz2n
            z2p_loss = z2p_food -z2p_faec -mu_max*bioz2p
            !---------------------------------------------------------------------------
            ! carbon-fluxes
            f_FromTo(i_z2c_dic) = (z2c_loss + (one-frac_det)*z2c_mort) *(one-frac_dic)
            f_FromTo(i_z2c_doc) = (z2c_loss + (one-frac_det)*z2c_mort) *     frac_dic
            f_FromTo(i_z2c_d1c) = (z2c_faec +      frac_det *z2c_mort) *(one-frac_d2x)
            f_FromTo(i_z2c_d2c) = (z2c_faec +      frac_det *z2c_mort) *     frac_d2x
            ! nitrogen-fluxes
            f_FromTo(i_z2n_don) = (z2n_loss + (one-frac_det)*z2n_mort) *(one-frac_n4n)
            f_FromTo(i_z2n_n4n) = (z2n_loss + (one-frac_det)*z2n_mort) *     frac_n4n
            f_FromTo(i_z2n_d1n) = (z2n_faec +      frac_det *z2n_mort) *(one-frac_d2x)
            f_FromTo(i_z2n_d2n) = (z2n_faec +      frac_det *z2n_mort) *     frac_d2x
            ! phosphorous-fluxes
            f_FromTo(i_z2p_dop) = (z2p_loss + (one-frac_det)*z2p_mort) *(one-frac_n1p)
            f_FromTo(i_z2p_n1p) = (z2p_loss + (one-frac_det)*z2p_mort) *     frac_n1p
            f_FromTo(i_z2p_d1p) = (z2p_faec +      frac_det *z2p_mort) *(one-frac_d2x)
            f_FromTo(i_z2p_d2p) = (z2p_faec +      frac_det *z2p_mort) *     frac_d2x
#else
            !----------------------------------------------------------------------------------
            ! ECOHAM4-Version (according to D093)
            !----------------------------------------------------------------------------------
            mu_max   = zero   ! to get rid of "unused" warning
            zloss    = zero   ! to get rid of "unused" warning
            z2c_loss = 0.0
            z2n_loss = 0.0
            z2p_loss = 0.0
#ifdef old_feces_stoichiometry
            ! feces have C:N:P from zooplankton (according to run D093)
            z2c_faec = z2n_faec*rcnz2
            z2p_faec = z2n_faec*rcnz2/rcpz2
#endif
      ! parameterization is based on nitrogen
            f_FromTo(i_z2n_d1n) = ((z2n_loss+z2n_mort)*(one-aeps2-delta_don2) + z2n_faec) *(one-frac_d2x)
            f_FromTo(i_z2n_d2n) = ((z2n_loss+z2n_mort)*(one-aeps2-delta_don2) + z2n_faec) *frac_d2x
            f_FromTo(i_z2n_n4n) =  (z2n_loss+z2n_mort)*aeps2
            f_FromTo(i_z2n_don) =  (z2n_loss+z2n_mort)*delta_don2
      ! carbon and phosphorous fluxes
            f_FromTo(i_z2c_d1c) = ((z2c_loss+z2c_mort)*(one-aeps2-delta_don2) + z2c_faec) *(one-frac_d2x)
            f_FromTo(i_z2c_d2c) = ((z2c_loss+z2c_mort)*(one-aeps2-delta_don2) + z2c_faec) *frac_d2x
            f_FromTo(i_z2c_dic) =  (z2c_loss+z2c_mort)*aeps2
            f_FromTo(i_z2c_doc) =  (z2c_loss+z2c_mort)*delta_don2
            f_FromTo(i_z2p_d1p) = ((z2p_loss+z2p_mort)*(one-aeps2-delta_don2) + z2p_faec) *(one-frac_d2x)
            f_FromTo(i_z2p_d2p) = ((z2p_loss+z2p_mort)*(one-aeps2-delta_don2) + z2p_faec) *frac_d2x
            f_FromTo(i_z2p_n1p) =  (z2p_loss+z2p_mort)*aeps2
            f_FromTo(i_z2p_dop) =  (z2p_loss+z2p_mort)*delta_don2
!----------------------------------------------------------------------------------------------------------------

#endif
! #ifdef fasham_losses coded above !
#else
!----------------------------------------------------------------------------------------------
! ATTENTION!!! New formulations strictly separate metabolic losses from mortality / predation !
!----------------------------------------------------------------------------------------------
            !----------------------------------------------------------------------------------
            ! following suggestion from book Sterner&Elser(2002)-pp197
            !----------------------------------------------------------------------------------
            ! calculate potential growth rate related to most sparse food-element(-currency)
            ! xmu6c represents energy required for feeding-activities & biosynthesis
            mu_max = min(((one-xmu6c)*z2c_food-z2c_faec)/bioz2c - xmu6, &
                                     (z2n_food-z2n_faec)/bioz2n - xmu6, &
                                     (z2p_food-z2p_faec)/bioz2p - xmu6)
            ! calculate metabolic loss rates, assuming that any "excess" uptake is respired/excreted:
            !metabolic losses = ingested -egested -growth
            z2c_loss = max(zero, z2c_food -z2c_faec -mu_max*bioz2c)
            z2n_loss = max(zero, z2n_food -z2n_faec -mu_max*bioz2n)
            z2p_loss = max(zero, z2p_food -z2p_faec -mu_max*bioz2p)
            !---------------------------------------------------------------------------
            ! NOTE:
            ! further, assimilation efficiencies could be related to match-missmatch of
            ! C:N:P-ratios of prey and zooplankton  -> not present jet !!!
            !---------------------------------------------------------------------------
            ! carbon-fluxes
            f_FromTo(i_z2c_dic) = (z2c_loss + (one-frac_det)*z2c_mort) *(one-frac_dic)
            f_FromTo(i_z2c_doc) = (z2c_loss + (one-frac_det)*z2c_mort) *     frac_dic
            f_FromTo(i_z2c_d1c) = (z2c_faec +      frac_det *z2c_mort) *(one-frac_d2x)
            f_FromTo(i_z2c_d2c) = (z2c_faec +      frac_det *z2c_mort) *     frac_d2x
            ! nitrogen-fluxes
            f_FromTo(i_z2n_don) = (z2n_loss + (one-frac_det)*z2n_mort) *(one-frac_n4n)
            f_FromTo(i_z2n_n4n) = (z2n_loss + (one-frac_det)*z2n_mort) *     frac_n4n
            f_FromTo(i_z2n_d1n) = (z2n_faec +      frac_det *z2n_mort) *(one-frac_d2x)
            f_FromTo(i_z2n_d2n) = (z2n_faec +      frac_det *z2n_mort) *     frac_d2x
            ! phosphorous-fluxes
            f_FromTo(i_z2p_dop) = (z2p_loss + (one-frac_det)*z2p_mort) *(one-frac_n1p)
            f_FromTo(i_z2p_n1p) = (z2p_loss + (one-frac_det)*z2p_mort) *     frac_n1p
            f_FromTo(i_z2p_d1p) = (z2p_faec +      frac_det *z2p_mort) *(one-frac_d2x)
            f_FromTo(i_z2p_d2p) = (z2p_faec +      frac_det *z2p_mort) *     frac_d2x
#endif

      ! balance fluxes for meso-zooplankton
      ! unbalanced net C flux into meso-zooplankton
            fzc2 = z2c_food -f_FromTo(i_z2c_d1c) -f_FromTo(i_z2c_d2c)   &
                            -f_FromTo(i_z2c_doc) -f_FromTo(i_z2c_dic)
      ! unbalanced net N flux into meso-zooplankton
            fzn2 = z2n_food -f_FromTo(i_z2n_d1n) -f_FromTo(i_z2n_d2n)   &
                            -f_FromTo(i_z2n_don) -f_FromTo(i_z2n_n4n)
      ! unbalanced net P flux into meso-zooplankton
            fzp2 = z2p_food -f_FromTo(i_z2p_d1p) -f_FromTo(i_z2p_d2p)   &
                            -f_FromTo(i_z2p_dop) -f_FromTo(i_z2p_n1p)
            if ((abs(fzc2-rcnz2*fzn2)<1.0e-12).and.(abs(fzc2-rcpz2*fzp2)<1.0e-12)) then
               !print*,'!everything is fine!'
               !print*,(fzc2-rcnz2*fzn2),(fzc2-rcpz2*fzp2),fzc2,rcnz2*fzn2,rcpz2*fzp2
            else
#ifdef debug_zoo
               if ((fzc2 <= rcnz2*fzn2).and.(fzc2 <= rcpz2*fzp2)) then
                  write(long_msg,'("[",i2.2,"] mesozoo",a,": zuviel N und P",3e20.10)') &
                                 myID,trim(str_jki),fzc2,rcnz2*fzn2,rcpz2*fzp2
                  call write_log_message(long_msg); long_msg = ''
               else if ((fzc2 >= rcnz2*fzn2).and.(fzn2 <= rcpz2/rcnz2*fzp2)) then !Zuviel C und P
                  write(long_msg,'("[",i2.2,"] mesozoo",a,": zuviel C und P",3e20.10)') &
                                 myID,trim(str_jki),fzc2,rcnz2*fzn2,rcpz2*fzp2
                  call write_log_message(long_msg); long_msg = ''
               else if ((fzc2 >= rcpz2*fzp2).and.(fzn2 >= rcpz2/rcnz2*fzp2)) then !Zuviel C und N
                  write(long_msg,'("[",i2.2,"] mesozoo",a,": zuviel C und N",3e20.10)') &
                                 myID,trim(str_jki),fzc2,rcnz2*fzn2,rcpz2*fzp2
                  call write_log_message(long_msg); long_msg = ''
               endif
#endif
            ! compensation of unbalanced into meso-zooplankton
               if ((fzc2 <= rcnz2*fzn2).and.(fzc2 <= rcpz2*fzp2)) then !Zuviel N und P
                  f_FromTo(i_z2n_don)=f_FromTo(i_z2n_don)+(fzn2-fzc2/rcnz2)/2.
                  f_FromTo(i_z2n_n4n)=f_FromTo(i_z2n_n4n)+(fzn2-fzc2/rcnz2)/2.
                  f_FromTo(i_z2p_dop)=f_FromTo(i_z2p_dop)+(fzp2-fzc2/rcpz2)/2.
                  f_FromTo(i_z2p_n1p)=f_FromTo(i_z2p_n1p)+(fzp2-fzc2/rcpz2)/2.
               else if ((fzc2 >= rcnz2*fzn2).and.(fzn2 <= rcpz2/rcnz2*fzp2)) then !Zuviel C und P
                  f_FromTo(i_z2c_dic)=f_FromTo(i_z2c_dic)+(fzc2-fzn2*rcnz2)
                  f_FromTo(i_z2p_dop)=f_FromTo(i_z2p_dop)+(fzp2-fzn2*rcnz2/rcpz2)/2.
                  f_FromTo(i_z2p_n1p)=f_FromTo(i_z2p_n1p)+(fzp2-fzn2*rcnz2/rcpz2)/2.
               else if ((fzc2 >= rcpz2*fzp2).and.(fzn2 >= rcpz2/rcnz2*fzp2)) then !Zuviel C und N
                  f_FromTo(i_z2c_dic)=f_FromTo(i_z2c_dic)+(fzc2-fzp2*rcpz2)
                  f_FromTo(i_z2n_don)=f_FromTo(i_z2n_don)+(fzn2-fzp2*rcpz2/rcnz2)/2.
                  f_FromTo(i_z2n_n4n)=f_FromTo(i_z2n_n4n)+(fzn2-fzp2*rcpz2/rcnz2)/2.
               else
                  ierr = 1
                  write(fmtstr,'(a)') '("[",i2.2,"] - Fehler bei meso-zooplankton flux",a)'
                  write(error_message,trim(fmtstr)) myID, trim(str_jki)
                  call stop_ecoham(ierr, msg=error_message)
               endif
            endif
#endif /* #ifdef exclude_z2x */

      !================================================================================
      ! DETRITUS
      !================================================================================
      ! DETRITUS-C
            f_FromTo(i_d1c_doc)=xmu4n*tfac(j,k,i)*biod1c*rxmu4c
            f_FromTo(i_d2c_doc)=xmu5n*tfac(j,k,i)*biod2c*rxmu4c
      ! DETRITUS-N
            f_FromTo(i_d1n_don)=xmu4n*tfac(j,k,i)*biod1n
            f_FromTo(i_d2n_don)=xmu5n*tfac(j,k,i)*biod2n
      ! DETRITUS-P
            f_FromTo(i_d1p_dop)=xmu4n*tfac(j,k,i)*biod1p
            f_FromTo(i_d2p_dop)=xmu5n*tfac(j,k,i)*biod2p

      ! DETRITUS-Silicon
      !  f_FromTo(f_p1s_d2s)=(f_FromTo(i_p1c_d2c)+f_FromTo(i_p1c_d1c) &
      !                       +f_FromTo(i_p1c_doc)+f_FromTo(i_p1c_zec))/rcs !E0xx
      !E065 and E067--------------------------------------------------------
            f_FromTo(i_p1s_d2s)=(f_FromTo(i_p1c_d1c)+f_FromTo(i_p1c_d2c))/rcs
            dia_adds_loss=(f_FromTo(i_p1c_doc)+f_FromTo(i_p1c_z1c)+f_FromTo(i_p1c_z2c))/rcs
            dia_ups=f_FromTo(i_n5s_p1s)
            f_FromTo(i_n5s_p1s)=max(0.0,dia_ups-dia_adds_loss) !MK verstehe ich nicht !JP Vermindere n5s Aufnahme
            f_FromTo(i_p1s_d2s)=f_FromTo(i_p1s_d2s)+max(0.0,dia_adds_loss-dia_ups)    !JP Wenn das nicht reicht, schmeisse den Rest raus
      !E065--------------------------------------------------------
            f_FromTo(i_d2s_n5s)=xmu5n*biod2s*tfac(j,k,i)*rxmu4c/10.         !like C/10

            ! DETRITUS-skeleton
            !Amount of attached calcite that is ingested is leaving the zooplankton gut without changing
            !!f_FromTo(i_psk_d2k) = f_FromTo(i_p3k_d2k)+f_FromTo(i_p3k_z1c)+f_FromTo(i_p3k_z2c)
            !!f_FromTo(i_psk_d2k) =(f_FromTo(i_p2c_d1c)+f_FromTo(i_p2c_d2c)  &
            !!                            +f_FromTo(i_p2c_doc)+f_FromTo(i_p2c_zic)+f_FromTo(i_p2c_zec))/q_c_cal
            !delta_o3c(j,k,i)=ro(j,k,i)*max(0.,o3c(j,k,i)/ro(j,k,i)-aksp(j,k,i)/ca(j,k,i))       !jp ca and aksp per kg
            !f_FromTo(i_d2k_dic)=biod2k/30.*(one-delta_o3c(j,k,i)/(delta_o3c(j,k,i)+c0))   !  Ad-hoc Heinze et al
            !MK found in eco9fs:
            f_FromTo(i_psk_d2k) = (f_FromTo(i_p2c_z1c) +f_FromTo(i_p2c_z2c)  &
                                         +f_FromTo(i_p2c_d1c) +f_FromTo(i_p2c_d2c)  &
                                         +f_FromTo(i_p2c_doc))/q_c_cal
            ! jpnew including overflow
            !f_FromTo(i_psk_d2k) = f_FromTo(i_psk_d2k) +f_FromTo(i_p2c_soc)/q_c_cal

            ! FS: CaCO3-diss in copepod guts
            f_FromTo(i_psk_dic) = 0.*(f_FromTo(i_p2c_z1c)+f_FromTo(i_p2c_z2c))/q_c_cal
            f_FromTo(i_psk_d2k) = f_FromTo(i_psk_d2k) -f_FromTo(i_psk_dic)

#ifdef module_chemie
            delta_o3c(j,k,i) = ch(j,k,i,io3c) - aksp(j,k,i)/ca(j,k,i)
            if (caco3_diss==1.or.delta_o3c(j,k,i)<0.0) then
               delta_o3c(j,k,i) = max(0.,ch(j,k,i,io3c)-aksp(j,k,i)/ca(j,k,i))
               f_FromTo(i_d2k_dic) = biod2k/30.*(one-delta_o3c(j,k,i)/(delta_o3c(j,k,i)+c0))
            else
               delta_o3c(j,k,i) = max(0.,ch(j,k,i,io3c)-aksp(j,k,i)/ca(j,k,i))
               f_FromTo(i_d2k_dic) = 0.0
            endif
#endif

      !================================================================================
      ! BACTERIA (switched-off)
      !================================================================================
            ! dd(doc ,dic ,ci) = rhoC   *Tf  *cc( doc,ci)    !remin. of (labile) dissolved organic carbon
            ! dd(don ,din ,ci) = rhoN   *Tf  *cc( don,ci)    !remin. of (labile) dissolved organic nitrogen
            ! dd(dop ,dip ,ci) = rhoP   *Tf  *cc( dop,ci)    !remin. of (labile) dissolved organic phosphorous
            f_FromTo(i_doc_dic) = rhoC *tfac(j,k,i) *biodoc
            f_FromTo(i_don_n4n) = rhoN *tfac(j,k,i) *biodon
            f_FromTo(i_dop_n1p) = rhoP *tfac(j,k,i) *biodop

#ifndef exclude_bac
      !================================================================================
      ! BACTERIA
      !================================================================================
            fy = biodon/(xk4+biodon)                              ! food dependency
      !  fy = min(biodon/(xk4+biodon),biodop/(xkpb+biodop))        ! food dependency
      ! BACTERIA-N
            f_FromTo(i_don_ban) = vb*tfac(j,k,i)*fy*bioban
            f_FromTo(i_n4n_ban) = 0.0
            f_FromTo(i_ban_n4n) = xmu3*tfac(j,k,i)*bioban
      ! BACTERIA-C
      !      f_FromTo(i_doc_bac) = f_FromTo(i_don_ban)*rcndo !MK macht das Sinn?
            f_FromTo(i_doc_bac) = f_FromTo(i_don_ban)*rcndo
            f_FromTo(i_bac_dic) = xmu3*tfac(j,k,i)*biobac
      ! BACTERIA-P
      !      f_FromTo(i_dop_bap) = f_FromTo(i_don_ban)*rcnb/rcpb
      !changing dop_bap (before: f_dop_bap(j,k,i) = u1/rnpb);now direct calculation; 0.01 estimated because xk4 for N uptake is 0.1
      !FS       if (biodop.gt.1.e-4) then
            if (biodop.gt.1.e-6) then
               f_FromTo(i_dop_bap) = vb*tfac(j,k,i)*biodop/(xkpb+biodop)*biobap
            else
      !        f_FromTo(i_n1p_bap) = u1/rnpb
               f_FromTo(i_dop_bap) = 0.
            endif
      !if (j==14.and.i==32.and.k==1) then  !jpdeb
      !print*, k, td, f_FromTo(i_n4n_ban), rcnb, ' n4n_ban in do bio 3' !jpdeb
      !continue
      !endif

#ifdef old_bac
      ! unbalanced net C flow into bacteria
            fbc = f_FromTo(i_doc_bac) -f_FromTo(i_bac_dic)  &
                 -f_FromTo(i_bac_z1c) -f_FromTo(i_bac_z2c)
      ! unbalanced net N flow into bacteria
            fbn = f_FromTo(i_don_ban) +f_FromTo(i_n4n_ban) -f_FromTo(i_ban_n4n)  &
                 -f_FromTo(i_ban_z1n) -f_FromTo(i_ban_z2n)

            if (fbc.gt.fbn*rcnb) then
               if (bion4n<=tres_n4n) then
                  f_FromTo(i_bac_dic) = f_FromTo(i_bac_dic) +fbc -fbn*rcnb
                  f_FromTo(i_n4n_ban) = 0.
               else
                  f_FromTo(i_n4n_ban) = (rcndo/rcnb - 1.0)*f_FromTo(i_don_ban)
               endif
            else !more N than C
               fdc_diff = f_FromTo(i_bac_dic) +fbc -fbn*rcnb
               f_FromTo(i_bac_dic) = max(0.,fdc_diff)
               f_FromTo(i_ban_n4n) = f_FromTo(i_ban_n4n) -min(0.,fdc_diff/rcnb)
            endif

            fbpmax = (f_FromTo(i_n4n_ban)/bion4n)*bion1p !maximal moeglicher n1p uptake
            fbpreq = (f_FromTo(i_don_ban) + f_FromTo(i_n4n_ban) &
                    - f_FromTo(i_ban_n4n))*rcnb/rcpb - f_FromTo(i_dop_bap) !benoetigtes P
            !fbpreq kann neg. werden.

            f_FromTo(i_n1p_bap) = min(max(0.,fbpreq),fbpmax)
            f_FromTo(i_bap_n1p) = max(0.,(-fbpreq))

            fbpdiff = max(0.,fbpreq - fbpmax)
            f_FromTo(i_ban_n4n) = f_FromTo(i_ban_n4n) + fbpdiff*rcpb/rcnb
            f_FromTo(i_bac_dic) = f_FromTo(i_bac_dic) + fbpdiff*rcpb

#else  /* #ifdef old_bac */
      ! unbalanced net C flow into bacteria
            fbc = f_FromTo(i_doc_bac) -f_FromTo(i_bac_dic)  &
                 -f_FromTo(i_bac_z1c) -f_FromTo(i_bac_z2c)
      ! unbalanced net N flow into bacteria
            fbn = f_FromTo(i_don_ban) +f_FromTo(i_n4n_ban) -f_FromTo(i_ban_n4n)  &
                 -f_FromTo(i_ban_z1n) -f_FromTo(i_ban_z2n)
            fdc_diff = abs(fbc - fbn*rcnb)

            if (fbc.gt.fbn*rcnb) then ! Aufnahem von Ammonium würde Wachstum stimulieren ...
               if (bion4n<=tres_n4n) then ! unterhalb des thresholds findet keine Aufnahme statt,
                  f_FromTo(i_n4n_ban) = 0.
                  ! entsprechend wird respiration erhöht
                  f_FromTo(i_bac_dic)= f_FromTo(i_bac_dic) +fdc_diff
               else ! es wird soviel amonium aufgenommen,wie benötigt wird,
                  f_FromTo(i_n4n_ban)= f_FromTo(i_doc_bac)/rcnb -f_FromTo(i_don_ban)
               endif
            else !more N than C ! -> C-Aufnahme limitiert das Wachstum
               ! zu viel aufgenommenes DON wird als Ammonium ausgeschieden
               f_FromTo(i_ban_n4n) = f_FromTo(i_ban_n4n) +fdc_diff/rcnb
            endif

           ! Nun wird der Bedarf an P berechnet, um der N-Bilanz zu entsprechen.
           ! Für den Fall, dass bereits ausreichend P über DOP aufgenommen wurde, wird dieser Term negativ.
            fbpreq = (f_FromTo(i_don_ban) +f_FromTo(i_n4n_ban) &
                     -f_FromTo(i_ban_n4n))*rcnb/rcpb -f_FromTo(i_dop_bap) !benoetigtes P !fbpreq kann neg. werden.
            if (fbpreq .le. 0) then ! ueberschuessiges P wird als DIP ausgeschieden
               f_FromTo(i_bap_n1p) = abs(fbpreq)
            else   ! fehlendes P wird aus dem DIP-pool aufgenommen
               fbpmax = vb*biobap ! Die P-aufnahme kann nicht schneller als vb erfolgen.
               if (bion1p<=tres_n1p) fbpmax = 0.0 ! unterhalb des thresholds findet keine n1p-Aufnahme statt,
               f_FromTo(i_n1p_bap) = min(fbpreq,fbpmax)
               ! Gegebenenfalls werden die C- und N-Verluste entsprechend angepasst
               if (fbpreq .gt. fbpmax) then
                  fbpdiff = fbpreq - fbpmax
                  f_FromTo(i_ban_n4n) = f_FromTo(i_ban_n4n) + fbpdiff*rcpb/rcnb
                  f_FromTo(i_bac_dic) = f_FromTo(i_bac_dic) + fbpdiff*rcpb
               endif
            endif
#endif /* #ifdef old_bac */
#endif /* #ifndef exclude_bac */

      !================================================================================
      ! SEMI LABILE DOC
      !================================================================================
            f_FromTo(i_soc_doc) = real(iexcess)*st(j,k,i,isoc)*soc_rate

      !================================================================================
      ! NITRIFICATION
      !================================================================================
            if (pafr*radsol(j,i)>eps .and. par(j,k,i)>eps) then
              anitdep(j,k,i) = 0.01 * (pafr*radsol(j,i)/par(j,k,i))
            endif
            if (par(j,k,i)<=eps) then
              anitdep(j,k,i) = 1.
            endif
            anitdep(j,k,i) = min(1.,anitdep(j,k,i))
            f_FromTo(i_n4n_n3n) = oswtch(j,k,i)*xknit*bion4n*anitdep(j,k,i)*tfac(j,k,i)   !additional oxygen limitation
      !if(i==18.and.j==38.and.k==1)then
      !print*,td,f_n4n_n3n(j,k,i),oswtch(j,k,i),xknit,bion4n,anitdep(j,k,i),tfac(j,k,i),' n4n_n3n'  !jpdeb
      !continue
      !endif
      !================================================================================
      ! DENITRIFICATION
      !================================================================================
            f_FromTo(i_n3n_nn2) = 0.5*(one-oswtch(j,k,i))*nswtch(j,k,i)  &
                                            *f_FromTo(i_bac_dic)/rcnb                  !anoxic pel denitri

      !================================================================================
      ! OXYGEN
      !================================================================================
           !f_FromTo(i_p1c_o2o) = f_FromTo(i_n3n_p1n)*(red+2.) +f_FromTo(i_n4n_p1n)*red         !lm
           !f_FromTo(i_p2c_o2o) = f_FromTo(i_n3n_p2n)*(red+2.) +f_FromTo(i_n4n_p2n)*red         !lm
            f_FromTo(i_p1c_o2o) = f_FromTo(i_dic_p1c)
            f_FromTo(i_p2c_o2o) = f_FromTo(i_dic_p2c)
            f_FromTo(i_p3c_o2o) = f_FromTo(i_dic_p3c)
            f_FromTo(i_o2o_z1c) = f_FromTo(i_z1c_dic)
            f_FromTo(i_o2o_z2c) = f_FromTo(i_z2c_dic)
            f_FromTo(i_o2o_n4n) = 2.0*f_FromTo(i_n4n_n3n)                              !oxic    nitrification lm

      !================================================================================
      ! REMINERALISATION
      !================================================================================
      ! pelagic remi
            f_FromTo(i_o2o_bac) = oswtch(j,k,i)*(f_FromTo(i_bac_dic)+f_FromTo(i_doc_dic)) !oxic
      !      when st_o2o<0: 1mol H2S=-1mol o2 (not -0.5mol O2)
            f_FromTo(i_o2o_bac) = f_FromTo(i_o2o_bac)                              &
                                  + (one-oswtch(j,k,i))*(one-nswtch(j,k,i))        &
                                    *( f_FromTo(i_bac_dic)+f_FromTo(i_doc_dic) )       !anoxic2 H2S production

      !================================================================================
      ! CHLOROPHYLL
      !================================================================================
#ifndef exclude_p1x
            f_FromTo(i_p1a_p1a) = ( f_FromTo(i_dic_p1c) -f_FromTo(i_p1c_d1c)   &
                                   -f_FromTo(i_p1c_d2c) -f_FromTo(i_p1c_doc)   &
                                   -f_FromTo(i_p1c_soc) -f_FromTo(i_p1c_z1c)   &
                                   -f_FromTo(i_p1c_z2c) ) * q_chl_p1c(j,k,i)
!apply nudging to carbon biomass
!#ifdef fix_CHL_to_C
            f_FromTo(i_p1a_p1a) = f_FromTo(i_p1a_p1a) + (biop1c*q_chl_p1c(j,k,i) - biop1a)  ! diatoms (Chl)
!#endif
#ifdef debugMK
  if (i==33 .and. j==70) then
if (k==1) print*,'======================================================================'
write(*,'(i2," biogeo#1678(f_p1a) ",9e20.10)') k,st(j,k,i,ip1a),f_FromTo(i_p2a_p2a),         &
                                    f_FromTo(i_dic_p1c), -f_FromTo(i_p1c_d1c), &
                                   -f_FromTo(i_p1c_d2c), -f_FromTo(i_p1c_doc), &
                                   -f_FromTo(i_p1c_soc), -f_FromTo(i_p1c_z1c), &
                                   -f_FromTo(i_p1c_z2c)
if (k==k0) print*,'======================================================================'
  endif
#endif
#endif
#ifndef exclude_p2x
            f_FromTo(i_p2a_p2a) = ( f_FromTo(i_dic_p2c) -f_FromTo(i_p2c_d1c)   &
                                   -f_FromTo(i_p2c_d2c) -f_FromTo(i_p2c_doc)   &
                                   -f_FromTo(i_p2c_soc) -f_FromTo(i_p2c_z1c)   &
                                   -f_FromTo(i_p2c_z2c) ) * q_chl_p2c(j,k,i)
!apply nudging to carbon biomass
!#ifdef fix_CHL_to_C
            f_FromTo(i_p2a_p2a) = f_FromTo(i_p2a_p2a) + (biop2c*q_chl_p2c(j,k,i) - biop2a)  ! diatoms (Chl)
!#endif
#endif
#ifndef exclude_p3x
            f_FromTo(i_p3a_p3a) = ( f_FromTo(i_dic_p3c) -f_FromTo(i_p3c_d1c)   &
                                   -f_FromTo(i_p3c_d2c) -f_FromTo(i_p3c_doc)   &
                                   -f_FromTo(i_p3c_soc) -f_FromTo(i_p3c_z1c)   &
                                   -f_FromTo(i_p3c_z2c) ) * q_chl_p3c(j,k,i)
!apply nudging to carbon biomass
!#ifdef fix_CHL_to_C
            f_FromTo(i_p3a_p3a) = f_FromTo(i_p3a_p3a) + (biop3c*q_chl_p3c(j,k,i) - biop3a)  ! diatoms (Chl)
!#endif
#endif


#ifdef TBNT_x1x_test
      !================================================================================
      ! DUMMY FLUXES FOR X1X
      !================================================================================
      ! constant factor in source should be larger than in sink to avoid negative state variable
            f_FromTo(i_x2x_x1x) = 0.2 *float(k0-k+1)/float(k0)*st(j,k,i,ix2x) ! source
            f_FromTo(i_x1x_x2x) = 0.19*float(k0-k+1)/float(k0)*st(j,k,i,ix1x) ! sink
#endif


      !================================================================================
      ! MAP LOCAL PRIVATE FLUXES f_local (mol m-3) TO PULIC ARRAY f_from_to (mol m-2)
      !================================================================================
            do n = 1, max_flux
               f_from_to(j,k,i,n) = f_from_to(j,k,i,n) + f_FromTo(n) *dz(j,k,i)
            enddo

      !================================================================================
      ! SOURCE EQUATIONS
      !================================================================================
      ! please sort entries in following order ...
      ! ix1x= 1,ialk= 2,idic= 3,in3n= 4,in4n= 5,in1p= 6,in5s= 7,ip1c= 8,ip1n= 9,ip1p=10
      ! ip1s=11,ip2c=12,ip2n=13,ip2p=14,ip3c=15,ip3n=16,ip3p=17,ip3k=18,iz1c=19,iz2c=20
      ! ibac=21,id1c=22,id1n=23,id1p=24,id2c=25,id2n=26,id2p=27,id2s=28,id2k=29,isoc=30
      ! idoc=31,idon=32,idop=33,io2o=34
#ifdef TBNT_x1x_test
            sst(j,k,i,ix1x) = sst(j,k,i,ix1x) +f_from_to(j,k,i,i_x2x_x1x) -f_from_to(j,k,i,i_x1x_x2x)
            sst(j,k,i,ix2x) = sst(j,k,i,ix2x) -f_from_to(j,k,i,i_x2x_x1x) +f_from_to(j,k,i,i_x1x_x2x)
#endif
            if(talk_treat==1)then
! print*,'  ', f_from_to(j,k,i,i_d2k_dic), f_from_to(j,k,i,i_psk_dic), f_from_to(j,k,i,i_dic_psk), f_from_to(j,k,i,i_n4n_n3n)
! print*,'  ', f_from_to(j,k,i,i_n3n_p1n), f_from_to(j,k,i,i_n4n_p1n), f_from_to(j,k,i,i_n3n_p2n), f_from_to(j,k,i,i_n4n_p2n)
! print*,'  ', f_from_to(j,k,i,i_n3n_p3n), f_from_to(j,k,i,i_n4n_p3n), f_from_to(j,k,i,i_ban_n4n), f_from_to(j,k,i,i_n4n_ban)
! print*,'  ', f_from_to(j,k,i,i_z1n_n4n), f_from_to(j,k,i,i_z2n_n4n), f_from_to(j,k,i,i_atm_n4n), f_from_to(j,k,i,i_atm_n3n)
! print*,'  ', f_from_to(j,k,i,i_n1p_p1p), f_from_to(j,k,i,i_n1p_p2p), f_from_to(j,k,i,i_n1p_p3p), f_from_to(j,k,i,i_z1p_n1p)
! print*,'  ', f_from_to(j,k,i,i_z2p_n1p), f_from_to(j,k,i,i_n1p_bap), f_from_to(j,k,i,i_bap_n1p)
               sst(j,k,i,ialk) = sst(j,k,i,ialk) &
                                          +2.*(f_from_to(j,k,i,i_d2k_dic) +f_from_to(j,k,i,i_psk_dic)   &
                                              -f_from_to(j,k,i,i_dic_psk) -f_from_to(j,k,i,i_n4n_n3n))  &
                                              +f_from_to(j,k,i,i_n3n_p1n) -f_from_to(j,k,i,i_n4n_p1n)   &
                                              +f_from_to(j,k,i,i_n3n_p2n) -f_from_to(j,k,i,i_n4n_p2n)   &
                                              +f_from_to(j,k,i,i_n3n_p3n) -f_from_to(j,k,i,i_n4n_p3n)   &
                                              +f_from_to(j,k,i,i_ban_n4n) -f_from_to(j,k,i,i_n4n_ban)   &
                                              +f_from_to(j,k,i,i_z1n_n4n) +f_from_to(j,k,i,i_z2n_n4n)   &
                                              +f_from_to(j,k,i,i_atm_n4n) -f_from_to(j,k,i,i_atm_n3n)   &
                                              +f_from_to(j,k,i,i_n1p_p1p) +f_from_to(j,k,i,i_n1p_p2p)   &
                                              +f_from_to(j,k,i,i_n1p_p3p) -f_from_to(j,k,i,i_z1p_n1p)   &
                                              -f_from_to(j,k,i,i_z2p_n1p) +f_from_to(j,k,i,i_n1p_bap)   &
                                              -f_from_to(j,k,i,i_bap_n1p)
            endif
            sst(j,k,i,idic) = sst(j,k,i,idic) +f_from_to(j,k,i,i_bac_dic) +f_from_to(j,k,i,i_z1c_dic)   &
                                              +f_from_to(j,k,i,i_z2c_dic) +f_from_to(j,k,i,i_d2k_dic)   &
                                              -f_from_to(j,k,i,i_dic_p1c) -f_from_to(j,k,i,i_dic_p2c)   &
                                              -f_from_to(j,k,i,i_dic_p3c) -f_from_to(j,k,i,i_dic_p3k)   &
                                              -f_from_to(j,k,i,i_dic_psk) +f_from_to(j,k,i,i_doc_dic)

!  write(*,'(''#'',9e15.6)')    f_from_to(j,k,i,i_n4n_n3n),-f_from_to(j,k,i,i_n3n_p1n),    &
!                              -f_from_to(j,k,i,i_n3n_p2n),-f_from_to(j,k,i,i_n3n_p3n),    &
!                              -f_from_to(j,k,i,i_n3n_nn2),sst(j,k,i,in3n)

            sst(j,k,i,in3n) = sst(j,k,i,in3n) +f_from_to(j,k,i,i_n4n_n3n) -f_from_to(j,k,i,i_n3n_p1n)   &
                                              -f_from_to(j,k,i,i_n3n_p2n) -f_from_to(j,k,i,i_n3n_p3n)   &
                                              -f_from_to(j,k,i,i_n3n_nn2)
            sst(j,k,i,in4n) = sst(j,k,i,in4n) +f_from_to(j,k,i,i_ban_n4n) +f_from_to(j,k,i,i_z1n_n4n)   &
                                              +f_from_to(j,k,i,i_z2n_n4n) -f_from_to(j,k,i,i_n4n_p1n)   &
                                              -f_from_to(j,k,i,i_n4n_p2n) -f_from_to(j,k,i,i_n4n_p3n)   &
                                              -f_from_to(j,k,i,i_n4n_ban) -f_from_to(j,k,i,i_n4n_n3n)   &
                                              +f_from_to(j,k,i,i_don_n4n)
            sst(j,k,i,in1p) = sst(j,k,i,in1p) +f_from_to(j,k,i,i_bap_n1p) +f_from_to(j,k,i,i_z1p_n1p)   &
                                              +f_from_to(j,k,i,i_z2p_n1p) -f_from_to(j,k,i,i_n1p_p1p)   &
                                              -f_from_to(j,k,i,i_n1p_p2p) -f_from_to(j,k,i,i_n1p_p3p)   &
                                              -f_from_to(j,k,i,i_n1p_bap) +f_from_to(j,k,i,i_dop_n1p)
            sst(j,k,i,in5s) = sst(j,k,i,in5s) +f_from_to(j,k,i,i_d2s_n5s) -f_from_to(j,k,i,i_n5s_p1s)
#ifndef exclude_p1x
            sst(j,k,i,ip1c) = sst(j,k,i,ip1c) +f_from_to(j,k,i,i_dic_p1c) -f_from_to(j,k,i,i_p1c_d1c)   &
                                              -f_from_to(j,k,i,i_p1c_d2c) -f_from_to(j,k,i,i_p1c_doc)   &
                                              -f_from_to(j,k,i,i_p1c_soc) -f_from_to(j,k,i,i_p1c_z1c)   &
                                              -f_from_to(j,k,i,i_p1c_z2c)
            sst(j,k,i,ip1n) = sst(j,k,i,ip1n) +f_from_to(j,k,i,i_n3n_p1n) +f_from_to(j,k,i,i_n4n_p1n)   &
                                              -f_from_to(j,k,i,i_p1n_d1n) -f_from_to(j,k,i,i_p1n_d2n)   &
                                              -f_from_to(j,k,i,i_p1n_don) -f_from_to(j,k,i,i_p1n_z1n)   &
                                              -f_from_to(j,k,i,i_p1n_z2n)
            sst(j,k,i,ip1p) = sst(j,k,i,ip1p) +f_from_to(j,k,i,i_n1p_p1p) -f_from_to(j,k,i,i_p1p_d1p)   &
                                              -f_from_to(j,k,i,i_p1p_d2p) -f_from_to(j,k,i,i_p1p_dop)   &
                                              -f_from_to(j,k,i,i_p1p_z1p) -f_from_to(j,k,i,i_p1p_z2p)
            sst(j,k,i,ip1s) = sst(j,k,i,ip1s) +f_from_to(j,k,i,i_n5s_p1s) -f_from_to(j,k,i,i_p1s_d2s)
            sst(j,k,i,ip1a) = sst(j,k,i,ip1a) +f_from_to(j,k,i,i_p1a_p1a)
#endif
#ifndef exclude_p2x
            sst(j,k,i,ip2c) = sst(j,k,i,ip2c) +f_from_to(j,k,i,i_dic_p2c) -f_from_to(j,k,i,i_p2c_d1c)   &
                                              -f_from_to(j,k,i,i_p2c_d2c) -f_from_to(j,k,i,i_p2c_doc)   &
                                              -f_from_to(j,k,i,i_p2c_soc) -f_from_to(j,k,i,i_p2c_z1c)   &
                                              -f_from_to(j,k,i,i_p2c_z2c)
            sst(j,k,i,ip2n) = sst(j,k,i,ip2n) +f_from_to(j,k,i,i_n3n_p2n) +f_from_to(j,k,i,i_n4n_p2n)   &
                                              -f_from_to(j,k,i,i_p2n_d1n) -f_from_to(j,k,i,i_p2n_d2n)   &
                                              -f_from_to(j,k,i,i_p2n_don) -f_from_to(j,k,i,i_p2n_z1n)   &
                                              -f_from_to(j,k,i,i_p2n_z2n)
            sst(j,k,i,ip2p) = sst(j,k,i,ip2p) +f_from_to(j,k,i,i_n1p_p2p) -f_from_to(j,k,i,i_p2p_d1p)   &
                                              -f_from_to(j,k,i,i_p2p_d2p) -f_from_to(j,k,i,i_p2p_dop)   &
                                              -f_from_to(j,k,i,i_p2p_z1p) -f_from_to(j,k,i,i_p2p_z2p)
            sst(j,k,i,ip2a) = sst(j,k,i,ip2a) +f_from_to(j,k,i,i_p2a_p2a)
#endif
#ifndef exclude_p3x
            sst(j,k,i,ip3c) = sst(j,k,i,ip3c) +f_from_to(j,k,i,i_dic_p3c) -f_from_to(j,k,i,i_p3c_d1c)   &
                                              -f_from_to(j,k,i,i_p3c_d2c) -f_from_to(j,k,i,i_p3c_doc)   &
                                              -f_from_to(j,k,i,i_p3c_soc) -f_from_to(j,k,i,i_p3c_z1c)   &
                                              -f_from_to(j,k,i,i_p3c_z2c)
            sst(j,k,i,ip3n) = sst(j,k,i,ip3n) +f_from_to(j,k,i,i_n3n_p3n) +f_from_to(j,k,i,i_n4n_p3n)   &
                                              -f_from_to(j,k,i,i_p3n_d1n) -f_from_to(j,k,i,i_p3n_d2n)   &
                                              -f_from_to(j,k,i,i_p3n_don) -f_from_to(j,k,i,i_p3n_z1n)   &
                                              -f_from_to(j,k,i,i_p3n_z2n)
            sst(j,k,i,ip3p) = sst(j,k,i,ip3p) +f_from_to(j,k,i,i_n1p_p3p) +f_from_to(j,k,i,i_dop_p3p)   &
                                              -f_from_to(j,k,i,i_p3p_d1p) -f_from_to(j,k,i,i_p3p_d2p)   &
                                              -f_from_to(j,k,i,i_p3p_dop) -f_from_to(j,k,i,i_p3p_z1p)   &
                                              -f_from_to(j,k,i,i_p3p_z2p)
            sst(j,k,i,ip3k) = sst(j,k,i,ip3k) +f_from_to(j,k,i,i_dic_p3k) -f_from_to(j,k,i,i_p3k_z1c)   &
                                              -f_from_to(j,k,i,i_p3k_z2c) -f_from_to(j,k,i,i_p3k_d2k)
            sst(j,k,i,ip3a) = sst(j,k,i,ip3a) +f_from_to(j,k,i,i_p3a_p3a)
#endif
#ifndef exclude_z1x
            sst(j,k,i,iz1c) = sst(j,k,i,iz1c) +f_from_to(j,k,i,i_p1c_z1c) +f_from_to(j,k,i,i_p2c_z1c)   &
                                              +f_from_to(j,k,i,i_p3c_z1c) +f_from_to(j,k,i,i_bac_z1c)   &
                                              +f_from_to(j,k,i,i_d1c_z1c) -f_from_to(j,k,i,i_z1c_dic)   &
                                              -f_from_to(j,k,i,i_z1c_d1c) -f_from_to(j,k,i,i_z1c_d2c)   &
                                              -f_from_to(j,k,i,i_z1c_doc) -f_from_to(j,k,i,i_z1c_z2c)
#endif
#ifndef exclude_z2x
            sst(j,k,i,iz2c) = sst(j,k,i,iz2c) +f_from_to(j,k,i,i_p1c_z2c) +f_from_to(j,k,i,i_p2c_z2c)   &
                                              +f_from_to(j,k,i,i_p3c_z2c) +f_from_to(j,k,i,i_bac_z2c)   &
                                              +f_from_to(j,k,i,i_d1c_z2c) -f_from_to(j,k,i,i_z2c_dic)   &
                                              -f_from_to(j,k,i,i_z2c_d1c) -f_from_to(j,k,i,i_z2c_d2c)   &
                                              -f_from_to(j,k,i,i_z2c_doc) +f_from_to(j,k,i,i_z1c_z2c)
#endif
#ifndef exclude_bac
            sst(j,k,i,ibac) = sst(j,k,i,ibac) +f_from_to(j,k,i,i_doc_bac) -f_from_to(j,k,i,i_bac_dic)   &
                                              -f_from_to(j,k,i,i_bac_z1c) -f_from_to(j,k,i,i_bac_z2c)
#endif
            sst(j,k,i,id1c) = sst(j,k,i,id1c) +f_from_to(j,k,i,i_p1c_d1c) +f_from_to(j,k,i,i_p2c_d1c)   &
                                              +f_from_to(j,k,i,i_p3c_d1c) +f_from_to(j,k,i,i_z1c_d1c)   &
                                              +f_from_to(j,k,i,i_z2c_d1c) -f_from_to(j,k,i,i_d1c_z1c)   &
                                              -f_from_to(j,k,i,i_d1c_z2c) -f_from_to(j,k,i,i_d1c_doc)
            sst(j,k,i,id1n) = sst(j,k,i,id1n) +f_from_to(j,k,i,i_p1n_d1n) +f_from_to(j,k,i,i_p2n_d1n)   &
                                              +f_from_to(j,k,i,i_p3n_d1n) +f_from_to(j,k,i,i_z1n_d1n)   &
                                              +f_from_to(j,k,i,i_z2n_d1n) -f_from_to(j,k,i,i_d1n_z1n)   &
                                              -f_from_to(j,k,i,i_d1n_z2n) -f_from_to(j,k,i,i_d1n_don)
            sst(j,k,i,id1p) = sst(j,k,i,id1p) +f_from_to(j,k,i,i_p1p_d1p) +f_from_to(j,k,i,i_p2p_d1p)   &
                                              +f_from_to(j,k,i,i_p3p_d1p) +f_from_to(j,k,i,i_z1p_d1p)   &
                                              +f_from_to(j,k,i,i_z2p_d1p) -f_from_to(j,k,i,i_d1p_z1p)   &
                                              -f_from_to(j,k,i,i_d1p_z2p) -f_from_to(j,k,i,i_d1p_dop)
            sst(j,k,i,id2c) = sst(j,k,i,id2c) +f_from_to(j,k,i,i_p1c_d2c) +f_from_to(j,k,i,i_p2c_d2c)   &
                                              +f_from_to(j,k,i,i_p3c_d2c) +f_from_to(j,k,i,i_z1c_d2c)   &
                                              +f_from_to(j,k,i,i_z2c_d2c) -f_from_to(j,k,i,i_d2c_doc)
            sst(j,k,i,id2n) = sst(j,k,i,id2n) +f_from_to(j,k,i,i_p1n_d2n) +f_from_to(j,k,i,i_p2n_d2n)   &
                                              +f_from_to(j,k,i,i_p3n_d2n) +f_from_to(j,k,i,i_z1n_d2n)   &
                                              +f_from_to(j,k,i,i_z2n_d2n) -f_from_to(j,k,i,i_d2n_don)
            sst(j,k,i,id2p) = sst(j,k,i,id2p) +f_from_to(j,k,i,i_p1p_d2p) +f_from_to(j,k,i,i_p2p_d2p)   &
                                              +f_from_to(j,k,i,i_p3p_d2p) +f_from_to(j,k,i,i_z1p_d2p)   &
                                              +f_from_to(j,k,i,i_z2p_d2p) -f_from_to(j,k,i,i_d2p_dop)
            sst(j,k,i,id2s) = sst(j,k,i,id2s) +f_from_to(j,k,i,i_p1s_d2s) -f_from_to(j,k,i,i_d2s_n5s)
            sst(j,k,i,id2k) = sst(j,k,i,id2k) +f_from_to(j,k,i,i_psk_d2k) +f_from_to(j,k,i,i_p3k_d2k)   &
                                              -f_from_to(j,k,i,i_d2k_dic)
            sst(j,k,i,isoc) = sst(j,k,i,isoc) +f_from_to(j,k,i,i_p1c_soc) +f_from_to(j,k,i,i_p2c_soc)   &
                                              +f_from_to(j,k,i,i_p3c_soc) -f_from_to(j,k,i,i_soc_doc)
            sst(j,k,i,idoc) = sst(j,k,i,idoc) +f_from_to(j,k,i,i_p1c_doc) +f_from_to(j,k,i,i_p2c_doc)   &
                                              +f_from_to(j,k,i,i_p3c_doc) +f_from_to(j,k,i,i_soc_doc)   &
                                              +f_from_to(j,k,i,i_z1c_doc) +f_from_to(j,k,i,i_z2c_doc)   &
                                              +f_from_to(j,k,i,i_d1c_doc) +f_from_to(j,k,i,i_d2c_doc)   &
                                              -f_from_to(j,k,i,i_doc_bac) -f_from_to(j,k,i,i_doc_dic)
! if(i==79.and.j==11.and.k==9)then
! write(log_unit,'(9e15.6," doc79119")')    f_from_to(j,k,i,i_p1c_doc),f_from_to(j,k,i,i_p2c_doc),    &
!                                                  f_from_to(j,k,i,i_p3c_doc),f_from_to(j,k,i,i_soc_doc),    &
!                                                  f_from_to(j,k,i,i_z1c_doc),f_from_to(j,k,i,i_z2c_doc),    &
!                                                  f_from_to(j,k,i,i_d1c_doc),f_from_to(j,k,i,i_d2c_doc),    &
!                                                  f_from_to(j,k,i,i_doc_bac)
! endif
            sst(j,k,i,idon) = sst(j,k,i,idon) +f_from_to(j,k,i,i_p1n_don) +f_from_to(j,k,i,i_p2n_don)   &
                                              +f_from_to(j,k,i,i_p3n_don) +f_from_to(j,k,i,i_z1n_don)   &
                                              +f_from_to(j,k,i,i_z2n_don) +f_from_to(j,k,i,i_d1n_don)   &
                                              +f_from_to(j,k,i,i_d2n_don) -f_from_to(j,k,i,i_don_ban)   &
                                              -f_from_to(j,k,i,i_don_n4n)
            sst(j,k,i,idop) = sst(j,k,i,idop) +f_from_to(j,k,i,i_p1p_dop) +f_from_to(j,k,i,i_p2p_dop)   &
                                              +f_from_to(j,k,i,i_p3p_dop) +f_from_to(j,k,i,i_z1p_dop)   &
                                              +f_from_to(j,k,i,i_z2p_dop) +f_from_to(j,k,i,i_d1p_dop)   &
                                              +f_from_to(j,k,i,i_d2p_dop) -f_from_to(j,k,i,i_dop_bap)   &
                                              -f_from_to(j,k,i,i_dop_p3p) -f_from_to(j,k,i,i_dop_n1p)
            sst(j,k,i,io2o) = sst(j,k,i,io2o) +f_from_to(j,k,i,i_p1c_o2o) +f_from_to(j,k,i,i_p2c_o2o)   &
                                              +f_from_to(j,k,i,i_p3c_o2o) -f_from_to(j,k,i,i_o2o_n4n)   &
                                              -f_from_to(j,k,i,i_o2o_z1c) -f_from_to(j,k,i,i_o2o_z2c)   &
                                              -f_from_to(j,k,i,i_o2o_bac)
         enddo
      enddo
   enddo

   if (ierr/=0) then
      write( error_message, '("[",i2.2,"] ERROR - do_biogeo_ecoham: ",i3)') myID,ierr
      call stop_ecoham(ierr, msg=error_message)
   end if
   return

   end subroutine do_biogeo

!=======================================================================
!
! !INTERFACE:
   subroutine cum_biofunctions(dt_,ierr)
#define SUBROUTINE_NAME 'cum_biofunctions'
!
! !DESCRIPTION:
! cumulate functional parameters to daily values
!
! !USES:
   implicit none
!
! !INPUT PARAMETERS:
   real, intent(in)        :: dt_
   integer, intent(inout)  :: ierr
!
! !LOCAL VARIABLES:
   integer               :: i, j, k, k0
!
!-----------------------------------------------------------------------
#include "call-trace.inc"

   select case (isw_dim) !jp
      case (3) !3D-mode
         where(ixistz==1) d_fPAR       = d_fPAR       + fPAR*dt_
#ifndef exclude_p1x
         where(ixistz==1) d_lim_p1_n1p = d_lim_p1_n1p + lim_p1_n1p*dt_
         where(ixistz==1) d_lim_p1_n3n = d_lim_p1_n3n + lim_p1_n3n*dt_
         where(ixistz==1) d_lim_p1_n4n = d_lim_p1_n4n + lim_p1_n4n*dt_
         where(ixistz==1) d_lim_p1_n5s = d_lim_p1_n5s + lim_p1_n5s*dt_
         !MK calculate daily mean from Chl:C derived by Cloern
         where(ixistz==1) d_chl_p1c    = d_chl_p1c    + q_chl_p1c*dt_
#endif
#ifndef exclude_p2x
         where(ixistz==1) d_lim_p2_n1p = d_lim_p2_n1p + lim_p2_n1p*dt_
         where(ixistz==1) d_lim_p2_n3n = d_lim_p2_n3n + lim_p2_n3n*dt_
         where(ixistz==1) d_lim_p2_n4n = d_lim_p2_n4n + lim_p2_n4n*dt_
         where(ixistz==1) d_chl_p2c    = d_chl_p2c    + q_chl_p2c*dt_
#endif
#ifndef exclude_p3x
         where(ixistz==1) d_lim_p3_n1p = d_lim_p3_n1p + lim_p3_n1p*dt_
         where(ixistz==1) d_lim_p3_n3n = d_lim_p3_n3n + lim_p3_n3n*dt_
         where(ixistz==1) d_lim_p3_n4n = d_lim_p3_n4n + lim_p3_n4n*dt_
         where(ixistz==1) d_lim_p3_dop = d_lim_p3_dop + lim_p3_dop*dt_
         where(ixistz==1) d_chl_p3c    = d_chl_p3c    + q_chl_p3c*dt_
#endif
      case (1) !1D-mode
         i = iStartCalc
         j = jStartCalc
         k0 = k_index(j,i)
         do k = 1, k0
            d_fPAR(j,k,i)       = d_fPAR(j,k,i)       + fPAR(j,k,i)*dt_
#ifndef exclude_p1x
            d_lim_p1_n1p(j,k,i) = d_lim_p1_n1p(j,k,i) + lim_p1_n1p(j,k,i)*dt_
            d_lim_p1_n3n(j,k,i) = d_lim_p1_n3n(j,k,i) + lim_p1_n3n(j,k,i)*dt_
            d_lim_p1_n4n(j,k,i) = d_lim_p1_n4n(j,k,i) + lim_p1_n4n(j,k,i)*dt_
            d_lim_p1_n5s(j,k,i) = d_lim_p1_n5s(j,k,i) + lim_p1_n5s(j,k,i)*dt_
            !MK calculate daily mean from Chl:C derived by Cloern
            d_chl_p1c(j,k,i)    = d_chl_p1c(j,k,i)    + q_chl_p1c(j,k,i)*dt_
#endif
#ifndef exclude_p2x
            d_lim_p2_n1p(j,k,i) = d_lim_p2_n1p(j,k,i) + lim_p2_n1p(j,k,i)*dt_
            d_lim_p2_n3n(j,k,i) = d_lim_p2_n3n(j,k,i) + lim_p2_n3n(j,k,i)*dt_
            d_lim_p2_n4n(j,k,i) = d_lim_p2_n4n(j,k,i) + lim_p2_n4n(j,k,i)*dt_
            d_chl_p2c(j,k,i)    = d_chl_p2c(j,k,i)    + q_chl_p2c(j,k,i)*dt_
#endif
#ifndef exclude_p3x
            d_lim_p3_n1p(j,k,i) = d_lim_p3_n1p(j,k,i) + lim_p3_n1p(j,k,i)*dt_
            d_lim_p3_n3n(j,k,i) = d_lim_p3_n3n(j,k,i) + lim_p3_n3n(j,k,i)*dt_
            d_lim_p3_n4n(j,k,i) = d_lim_p3_n4n(j,k,i) + lim_p3_n4n(j,k,i)*dt_
            d_lim_p3_dop(j,k,i) = d_lim_p3_dop(j,k,i) + lim_p3_dop(j,k,i)*dt_
            d_chl_p3c(j,k,i)    = d_chl_p3c(j,k,i)    + q_chl_p3c(j,k,i)*dt_
#endif
         enddo
   end select

   if (ierr/=0) then
      write( error_message, '("[",i2.2,"] ERROR - cum_biofunctions: ",i3)') myID,ierr
      call stop_ecoham(ierr, msg=error_message)
   end if
   return

   end subroutine cum_biofunctions

!=======================================================================
!
! !INTERFACE:
   subroutine empty_biofunctions(ierr)
#define SUBROUTINE_NAME 'empty_biofunctions'
!
! !DESCRIPTION:
! cumulate functional parameters to daily values
!
! !USES:
   implicit none
!
! !INPUT PARAMETERS:
   integer, intent(inout)  :: ierr
!
! !LOCAL VARIABLES:
   integer               :: i, j, k, k0
!
!-----------------------------------------------------------------------
#include "call-trace.inc"

   select case (isw_dim) !jp
      case (3) !3D-mode
         where(ixistz==1) d_fPar    = zero
#ifndef exclude_p1x
         where(ixistz==1) d_lim_p1_n1p = zero
         where(ixistz==1) d_lim_p1_n3n = zero
         where(ixistz==1) d_lim_p1_n4n = zero
         where(ixistz==1) d_lim_p1_n5s = zero
#endif
#ifndef exclude_p2x
         where(ixistz==1) d_lim_p2_n1p = zero
         where(ixistz==1) d_lim_p2_n3n = zero
         where(ixistz==1) d_lim_p2_n4n = zero
#endif
#ifndef exclude_p3x
         where(ixistz==1) d_lim_p3_n1p = zero
         where(ixistz==1) d_lim_p3_n3n = zero
         where(ixistz==1) d_lim_p3_n4n = zero
         where(ixistz==1) d_lim_p3_dop = zero
#endif
         where(ixistz==1) d_chl_p1c = zero
         where(ixistz==1) d_chl_p2c = zero
         where(ixistz==1) d_chl_p3c = zero
      case (1) !1D-mode
         i = iStartCalc
         j = jStartCalc
         k0 = k_index(j,i)
         do k = 1, k0
            d_fPAR(j,k,i)    = zero
#ifndef exclude_p1x
            d_lim_p1_n1p(j,k,i) = zero
            d_lim_p1_n3n(j,k,i) = zero
            d_lim_p1_n4n(j,k,i) = zero
            d_lim_p1_n5s(j,k,i) = zero
#endif
#ifndef exclude_p2x
            d_lim_p2_n1p(j,k,i) = zero
            d_lim_p2_n3n(j,k,i) = zero
            d_lim_p2_n4n(j,k,i) = zero
#endif
#ifndef exclude_p3x
            d_lim_p3_n1p(j,k,i) = zero
            d_lim_p3_n3n(j,k,i) = zero
            d_lim_p3_n4n(j,k,i) = zero
            d_lim_p3_dop(j,k,i) = zero
#endif
            d_chl_p1c(j,k,i) = zero
            d_chl_p2c(j,k,i) = zero
            d_chl_p3c(j,k,i) = zero
         enddo
   end select

   if (ierr/=0) then
      write( error_message, '("[",i2.2,"] ERROR - empty_biofunctions: ",i3)') myID,ierr
      call stop_ecoham(ierr, msg=error_message)
   end if
   return

   end subroutine empty_biofunctions

!=======================================================================
!
! !INTERFACE:
   subroutine allocate_arrays_biogeo(ierr)
#define SUBROUTINE_NAME 'allocate_arrays_biogeo_ecoham'
!
! !DESCRIPTION:
!
! !USES:
   implicit none
!
! !OUTPUT PARAMETERS:
   integer, intent(inout) :: ierr
!
! !LOCAL VARIABLES:
!   integer       :: i,j,k,k0,n
!
!-------------------------------------------------------------------------
#include "call-trace.inc"

   !initialize arrays
   allocate(         tfac(jmax,kmax,iStartD:iEndD) );    tfac      = zero
   allocate(        tfac0(jmax,kmax,iStartD:iEndD) );    tfac0     = zero
   allocate(        tfac1(jmax,kmax,iStartD:iEndD) );    tfac1     = zero
   allocate(        tfac2(jmax,kmax,iStartD:iEndD) );    tfac2     = zero
   allocate(        tfac3(jmax,kmax,iStartD:iEndD) );    tfac3     = zero
   allocate(      tfac_z1(jmax,kmax,iStartD:iEndD) );    tfac_z1   = zero
   allocate(      tfac_z2(jmax,kmax,iStartD:iEndD) );    tfac_z2   = zero

   allocate(         fPAR(jmax,kmax,iStartD:iEndD) );    fPAR      = zero
   allocate(       d_fPAR(jmax,kmax,iStartD:iEndD) );    d_fPAR    = zero
   allocate(    q_chl_p1c(jmax,kmax,iStartD:iEndD) );    q_chl_p1c = zero
   allocate(    q_chl_p2c(jmax,kmax,iStartD:iEndD) );    q_chl_p2c = zero
   allocate(    q_chl_p3c(jmax,kmax,iStartD:iEndD) );    q_chl_p3c = zero
   allocate(    d_chl_p1c(jmax,kmax,iStartD:iEndD) );    d_chl_p1c = zero
   allocate(    d_chl_p2c(jmax,kmax,iStartD:iEndD) );    d_chl_p2c = zero
   allocate(    d_chl_p3c(jmax,kmax,iStartD:iEndD) );    d_chl_p3c = zero

#ifndef exclude_p1x
   ! p1x - diatoms
   allocate(   lim_p1_n1p(jmax,kmax,iStartD:iEndD) );    lim_p1_n1p   = zero
   allocate(   lim_p1_n3n(jmax,kmax,iStartD:iEndD) );    lim_p1_n3n   = zero
   allocate(   lim_p1_n4n(jmax,kmax,iStartD:iEndD) );    lim_p1_n4n   = zero
   allocate(   lim_p1_n5s(jmax,kmax,iStartD:iEndD) );    lim_p1_n5s   = zero
   allocate( d_lim_p1_n1p(jmax,kmax,iStartD:iEndD) );    d_lim_p1_n1p = zero
   allocate( d_lim_p1_n3n(jmax,kmax,iStartD:iEndD) );    d_lim_p1_n3n = zero
   allocate( d_lim_p1_n4n(jmax,kmax,iStartD:iEndD) );    d_lim_p1_n4n = zero
   allocate( d_lim_p1_n5s(jmax,kmax,iStartD:iEndD) );    d_lim_p1_n5s = zero
#endif
#ifndef exclude_p2x
   ! p2x - flagellates
   allocate(   lim_p2_n1p(jmax,kmax,iStartD:iEndD) );    lim_p2_n1p   = zero
   allocate(   lim_p2_n3n(jmax,kmax,iStartD:iEndD) );    lim_p2_n3n   = zero
   allocate(   lim_p2_n4n(jmax,kmax,iStartD:iEndD) );    lim_p2_n4n   = zero
   allocate( d_lim_p2_n1p(jmax,kmax,iStartD:iEndD) );    d_lim_p2_n1p = zero
   allocate( d_lim_p2_n3n(jmax,kmax,iStartD:iEndD) );    d_lim_p2_n3n = zero
   allocate( d_lim_p2_n4n(jmax,kmax,iStartD:iEndD) );    d_lim_p2_n4n = zero
#endif
#ifndef exclude_p3x
   ! p3x - 3rd phytoplankton group
   allocate(          fp3(jmax,kmax,iStartD:iEndD) );    fp3          = zero
   allocate(         fp3k(jmax,kmax,iStartD:iEndD) );    fp3k         = zero
   allocate(   lim_p3_n1p(jmax,kmax,iStartD:iEndD) );    lim_p3_n1p   = zero
   allocate(   lim_p3_n3n(jmax,kmax,iStartD:iEndD) );    lim_p3_n3n   = zero
   allocate(   lim_p3_n4n(jmax,kmax,iStartD:iEndD) );    lim_p3_n4n   = zero
   allocate(   lim_p3_dop(jmax,kmax,iStartD:iEndD) );    lim_p3_dop   = zero
   allocate( d_lim_p3_n1p(jmax,kmax,iStartD:iEndD) );    d_lim_p3_n1p = zero
   allocate( d_lim_p3_n3n(jmax,kmax,iStartD:iEndD) );    d_lim_p3_n3n = zero
   allocate( d_lim_p3_n4n(jmax,kmax,iStartD:iEndD) );    d_lim_p3_n4n = zero
   allocate( d_lim_p3_dop(jmax,kmax,iStartD:iEndD) );    d_lim_p3_dop = zero
#endif

   allocate(        omega(jmax,kmax,iStartD:iEndD) );    omega     = zero
   allocate(      anitdep(jmax,kmax,iStartD:iEndD) );    anitdep   = zero

   if (ierr/=0) then
      write( error_message, '("[",i2.2,"] ERROR - allocate_arrays_biogeo_ecoham: ",i3)') myID,ierr
      call stop_ecoham(ierr, msg=error_message)
   end if
   return

   end subroutine allocate_arrays_biogeo

!=======================================================================
# endif
   end module mod_biogeo_ecoham

!=======================================================================
# endif