!
! !MODULE: eco_rivers.f90 ---  river discharge (freshwater & nutrients) for ecoham
!
! !INTERFACE:
   MODULE mod_rivers
!
! !DESCRIPTION:
! This module handles freshwater and nutrient discharge from rivers
!
!------------------------------
!
! !USES:
   use mod_common
   use mod_grid
#ifdef MPI
   use mod_mpi_parallel
#endif
   use mod_var,        only : n_st_var
!#ifdef cskc
!   use mod_river2grid, only : class_riv
!#endif
   implicit none
!
!  default: all is private.
   private
! !PUBLIC MEMBER FUNCTIONS:
   public init_rivers, get_rivers, do_rivers, close_rivers
   public fresh_amount, ipos_riv, jpos_riv
!
! !PUBLIC VARIABLES:
   character(len=99), public :: river_dir, river_dat, freshwater_dat
   integer,   public :: num_riv            =   0    ! 149 old number of rivers loading material   FS:  former 150
!                                          =        !  48 newly implemented by HL
!                                          =        !  23 setup D093 by IL
   integer,   public :: riverdischarge     =   1    ! 1 - including river discharges from file
!                                          =        ! 0 - no river (tracer) input
   integer,   public :: riveraddtracer     =   1    ! 1 - add tracer to compensate dilution effect from pure freshwater ("keep concentration as is")
!                                          =        ! 0 - freshwater input will dilute tracer concentration if no discharge is prescribed
!    integer,   public :: riverdischarge     =   1    ! 1 - including river discharges
! !                                          =        ! 0 - no river input
!    integer,   public :: riverdilution      =   1    ! 1 - including river dilution and freshwater input
! !                                          =        ! 0 - no freshwater input
   integer,   public :: redfield_discharge =   0    ! 1 - derive d1c loads from d1n applying redfield ratio (riv_d1c:=riv_d1n*red)
!                                          =        ! 0 - keep d1c loads as read from file (NOTE: currently a factor *0.1 is applied additionally)
   integer,   public :: kmax_river_input   =   6
!
! !LOCAL VARIABLES:
#if defined RiverBigEndR8
   character(len=*), parameter :: river_endian='BIG_ENDIAN'
   integer,          parameter :: river_realkind = 8   != ibyte_per_real
#else
   ! standard case used in ECOHAM5
   character(len=*), parameter :: river_endian='LITTLE_ENDIAN'
   integer,          parameter :: river_realkind = 4   != ibyte_per_real
#endif
   real,    parameter :: eps = 1.e-8
   integer, parameter :: ipar_riv         = 12
   character(len=3)   :: var_nam_riv(ipar_riv) = '###'
   real               :: time_const       = 30.
   real               :: riv_warm         =  1.
   integer            :: irec_river_old, irec_river_max
   integer            :: n_found, npo_st(n_st_var), npo_ri(ipar_riv)
   integer            :: maxr(n_st_var)
   integer,                    dimension(:),   allocatable :: ipos_riv, jpos_riv
   real,                       dimension(:,:), allocatable :: fresh_amount
   real(kind=river_realkind),  dimension(:,:), allocatable :: riv_load, riv_coef
#ifdef cskc
   integer, parameter :: max_class_riv    =  4
   real(kind=river_realkind),  dimension(:,:), allocatable :: riv_conc_class !riv_conc_class(ipar_riv,0:max_class_riv)
   integer,                    dimension(:),   allocatable :: class_riv
#endif
#ifdef RiverRepairCO2SYS
   real,                       dimension(:),   allocatable :: custom_alk, custom_dic
#endif
   integer,                    dimension(:),   allocatable :: load_check
!
!=======================================================================

   contains

!=======================================================================
!
! !INTERFACE:
   subroutine init_rivers(ierr)
#define SUBROUTINE_NAME 'init_rivers'
!
! !DESCRIPTION:
!
! !USES:
   use mod_var,        only : st_name, n_st1, n_st2
   use mod_flux, only : f_riv  ! river discharge  !jp

   implicit none
!
! !OUTPUT PARAMETERS:
   integer, intent(inout)  :: ierr
!
! !LOCAL VARIABLES:
   integer           :: i, j, n, m
   integer           :: ir, irec, ios
   integer           :: idummy
   character(len=99) :: fmtstr, dummy
!
   namelist /river_nml/river_dir, river_dat, freshwater_dat, num_riv,     &
                       riverdischarge, riveraddtracer, redfield_discharge, &
                       var_nam_riv
!
!-----------------------------------------------------------------------
#include "call-trace.inc"

   ! check whether init_rivers has already been called (e.g. from init_hydro)
   if ( num_riv > 0 .or. allocated(fresh_amount) ) return

   if (myID == master) call write_log_message(" INITIALIZING river data")

! open & read river settings
!--------------------------------------------------------------
   open(nml_unit,file=trim(settings_file),action='read',status='old')
   read(nml_unit,nml=river_nml)
   close(nml_unit)

   ! allocate arrays
   allocate( ipos_riv(num_riv) );           ipos_riv = 0
   allocate( jpos_riv(num_riv) );           jpos_riv = 0

   allocate( riv_coef(ipar_riv, num_riv) ); riv_coef = 1.0
   f_riv=0.0  !jp
! #ifndef RiverReductionScenario
!    do m = 1, ipar_riv
!       if ('d1c'==var_nam_riv(m)) riv_coef(m,:) = real(0.1, river_realkind) ! ATTENTION: Take care of multiplicator (for D093 *0.1) !
!    enddo
! #endif

   if (myID == master) then
      if (num_riv <= 0) then
         call write_log_message("   !! setup contains no rivers !! ")
      else
         write(dummy,'("   -setup contains discharge for ",i3," rivers")') num_riv
         call write_log_message(trim(dummy))
#if defined old_discharge
         call write_log_message("   -old_discharge: discharge conc. not specified in river.bin defined as ZERO")
#else
         call write_log_message("   -new_discharge: discharge conc. not specified in river.bin match tracer conc. in river cells")
#endif
#ifdef RiverRepairCO2SYS
         call write_log_message("   -RiverRepairCO2SYS: replace discharge for alkalinity and DIC" // &
                                " using concentrations given in river_CO2SYS.dat")
#endif
#ifdef RiverRedfieldPOM
         call write_log_message("   -RiverRedfieldPOM: undefined POM discharge is filled assuming Redfield (106:16:1)")
#endif
#ifdef RiverReductionScenario
         call write_log_message("   -ReductionScenario: discharge is reduced according to RiverCoefficients.dat")
! #else
!          fmtstr = '(3x,"-river reduction factor ",f4.2," will be applied to POC loads")'
!          do m = 1, ipar_riv
!             if ('d1c'==var_nam_riv(m)) write(long_msg,fmtstr) riv_coef(m,1)
!          enddo
!          call write_log_message(long_msg); long_msg = ''
#endif
      endif
   endif

   !!!! calculation with freshwater input requires 3D setup!!!!
   if ( lfreshwater .and. isw_dim/=3 ) then
      lfreshwater = .false.
      if (myID == master) then
         write(log_msg(1),'("##########################################################################")')
         write(log_msg(2),'("#  WARNING: You can not run a 1D setup including river freshwater input  #")')
         write(log_msg(3),'("#           Namelist entry is reset to: lfreshwater = .false.            #")')
         write(log_msg(4),'("##########################################################################")')
         long_msg = trim(log_msg(1))//new_line('a')//trim(log_msg(2))//new_line('a')//trim(log_msg(3))// &
                                    & new_line('a')//trim(log_msg(4))
         call write_log_message(long_msg); long_msg = ''
      endif
   endif

   if (num_riv <= 0) return ! skip everything below if setup does not contain any river
   
   !!!! required to deal with HAMSOM simulation including freshwater !!!!
   if (lfreshwater) kmax_river_input = 1

   !!!! calculation of riveraddtracer requires freshwater input!!!!
   if (.not.lfreshwater .and. riveraddtracer==1) then
      !riverdilution = 0
      !if (myID == master) then
      !   write(log_msg(1),'("##########################################################################")')
      !   write(log_msg(2),'("#  WARNING: riverdilution requires freshwater input!                     #")')
      !   write(log_msg(3),'("#           Namelist entry is reset to: riverdilution = 0                #")')
      !   write(log_msg(4),'("##########################################################################")')
      !   long_msg = trim(log_msg(1))//new_line('a')//trim(log_msg(2))//new_line('a')//trim(log_msg(3))// &
      !                              & new_line('a')//trim(log_msg(4))
      !  call write_log_message(long_msg); long_msg = ''
      !endif
      if (myID == master) then
         write(log_msg(1),'("##########################################################################")')
         write(log_msg(2),'("#  WARNING: riveraddtracer whithout freshwater input is quite special!   #")')
         write(log_msg(3),'("#      -------  PLEASE CHECK YOUR CONFIGURATION !!!   -------            #")')
         write(log_msg(4),'("##########################################################################")')
         long_msg = trim(log_msg(1))//new_line('a')//trim(log_msg(2))//new_line('a')//trim(log_msg(3))// &
                                    & new_line('a')//trim(log_msg(4))
         call write_log_message(long_msg); long_msg = ''
      endif
   endif

!    ! accociate river parameters
!    var_nam_riv( 1)='n4n'
!    var_nam_riv( 2)='###' !'n2n'-> n3n
!    var_nam_riv( 3)='n1p'
!    var_nam_riv( 4)='n5s'
!    var_nam_riv( 5)='n3n'
!    var_nam_riv( 6)='d1n' !'pon'
!    var_nam_riv( 7)='d1p' !'pop'
!    var_nam_riv( 8)='###' !'pos'-> d2s ! in D146 not included
!    var_nam_riv( 9)='d1c' !'poc'
!    var_nam_riv(10)='dic'
!    var_nam_riv(11)='###' !'doc' ! in D146 not included
!    var_nam_riv(12)='###' !'alk' ! in D146 not included
! #ifdef cskc
!    var_nam_riv(12)='alk' !'alk'
! #endif
! #ifdef eco9
!    var_nam_riv(12)='alk' !'alk'
! #endif

#ifdef cskc
   ! river conc by class [umol/kg]:
   ! class 0: no class, 1: non-peat in Indonesia, 2: peat in Indonesia, 3+4: like 1+2 outside Indonesia
   allocate(  riv_conc_class(ipar_riv,0:max_class_riv) )
   riv_conc_class       =  0.
   riv_conc_class(10,:) = (/ 0., 620., 290., 620., 290. /)
   riv_conc_class(12,:) = (/ 0., 605., 180., 605., 180. /)
#endif

   ! pelagic STATE VARIABLES:
   ! x1x(1),alk(2),dic(3),n3n(4),n4n(5),n1p(6),n5s(7),p1c(8),p1n(9),p1p(10),
   ! p1s(11),p2c(12),p2n(13),p2p(14),p3c(15),p3n(16),p3p(17),p3k(18),z1c(19),z2c(20),
   ! bac(21),d1c(22),d1n(23),d1p(24),d2c(25),d2n(26),d2p(27),d2s(28),dsc(29),soc(30),
   ! doc(31),don(32),dop(33),o2o(34),

   ! riv_load n4n,n2n,n1p,n5s,n3n,pon,pop,pos,poc,dic,doc,alk in (mmol d-1)
   !           1   2   3   4   5   6   7   8   9  10  11  12

   n_found = 0
   ! loop over parameters included in riv.bin
   do m = 1, ipar_riv
      !check for matching pelagic statevariable
      do n = n_st1, n_st2
         if (st_name(n)==var_nam_riv(m)) then
            n_found = n_found+1
            npo_st(n_found) = n  ! pointer to st_var index
            npo_ri(n_found) = m  ! pointer to river_var index (not all parameters might be used!)
            exit
         endif
      enddo
   enddo

   ! map river mouths to grid cells
#ifdef cskc
   allocate( class_riv(num_riv) )
   class_riv = 0

   filename = trim(river_dir)//'river_indices.dat'
   open(asc_unit, file = trim(filename), status = 'old', form = 'formatted')
   if (myID == master) then
      fmtstr = '(3x,"river indices",7x,"(unit=",i3.0,") from file: ",a)'
      write(long_msg,fmtstr) asc_unit, trim(filename)
      call write_log_message(long_msg); long_msg = ''
   endif
   read(asc_unit, *) ipos_riv
   read(asc_unit, *) jpos_riv
   read(asc_unit, *) class_riv
   close(asc_unit)
#else
   !call riv2pos(num_riv, ierr)
   filename = trim(river_dir)//'RiverIndices.dat'
   open(asc_unit, file = trim(filename), status = 'old', form = 'formatted')
   if (myID == master) then
      fmtstr = '(3x,"river indices",7x,"(unit=",i3.0,") from file: ",a)'
      write(long_msg,fmtstr) asc_unit, trim(filename)
      call write_log_message(long_msg); long_msg = ''
   endif
   read(asc_unit,*) dummy
   do ir = 1, num_riv
      read(asc_unit,*) ipos_riv(ir), jpos_riv(ir), idummy, dummy
   enddo
   close(asc_unit)
#endif
   ! check river positions
   do ir = 1,num_riv
      i = ipos_riv(ir)
      j = jpos_riv(ir)
      if (i >= iStartCalc .and. i <= iEndCalc) then ! each process checks its area
         if (k_index(j,i)<0) then
            ierr = ierr + 1
            write(long_msg, '("[",i2.2,"] riv_pos on land",3i6)') myID, i, j, ir
            call write_log_message(long_msg); long_msg = ''
         endif
      end if
   enddo
   if (ierr/=0) goto 99

#ifdef RiverRepairCO2SYS
   allocate( custom_alk(num_riv) ); custom_alk = 0.0
   allocate( custom_dic(num_riv) ); custom_dic = 0.0

   filename = 'river_RepairCO2SYS.dat'
   open(asc_unit, file = trim(filename), status = 'old', form = 'formatted')
   if (myID == master) then
      fmtstr = '(3x,"river TA + DIC",6x,"(unit=",i3.0,") from file: ",a)'
      write(long_msg,fmtstr) asc_unit, trim(filename)
      call write_log_message(long_msg); long_msg = ''
   endif
   read(asc_unit,*) dummy
   do ir = 1, num_riv
      read(asc_unit,*) custom_alk(ir), custom_dic(ir)
   enddo
   close(asc_unit)
#endif

#ifdef RiverReductionScenario
   filename = 'RiverCoefficients.dat'
   open(asc_unit, file = trim(filename), status = 'old', form = 'formatted')
   if (myID == master) then
      fmtstr = '(3x,"river reduction",5x,"(unit=",i3.0,") from file: ",a)'
      write(long_msg,fmtstr) asc_unit, trim(filename)
      call write_log_message(long_msg); long_msg = ''
   endif
   read(asc_unit,*) dummy
   do ir = 1, num_riv
      read(asc_unit,*) (riv_coef(m, ir), m = 1, ipar_riv)
   enddo
   close(asc_unit)
#endif

   allocate( fresh_amount(num_riv,367) )
   call read_fresh_amount(ierr)
   !fresh_amount = 0.0
   if (ierr/=0) goto 99

   allocate( riv_load(ipar_riv,num_riv) )
   riv_load = 0

#ifdef cskc
! do nothing !
#else
! #ifdef debug
   ! open file with river discharges
   !print*,'open riv.bin'
   filename = trim(river_dir)//trim(river_dat)
   open(river_unit, file = trim(filename), status='old',         &
            form = 'unformatted',access = 'direct',              &
            recl = num_riv * ipar_riv * river_realkind,          &
            convert = trim(river_endian), iostat=ios)
   if (ios/=0) then
      ierr = 1
      write( long_msg, '("[",i2.2,"] ERROR opening ",a)') myID, trim(filename)
      call write_log_message(long_msg); long_msg = ''
      goto 99
   endif
   ! get irec for last dataset entry
   ios  = 0
   irec = 365 ! assumes, that input file keeps daily loads for one year
   do while (ios==0)
      read(river_unit, rec=irec, iostat = ios) riv_load(:,1:num_riv)
      if (ios==0) irec_river_max = irec
      !print*,irec,irec_river_max
      irec = irec + 1
   end do
   irec_river_old = 0

   if (myID == master) then
      fmtstr = '(3x,"river discharges",4x,"(unit=",i3.0,") from file: ",a)'
      write(long_msg,fmtstr) river_unit, trim(filename)
      write(long_msg,'(a," (nrec=",i3.0,")")') trim(long_msg), irec_river_max
      call write_log_message(long_msg); long_msg = ''
   endif

! #endif
#endif

#ifdef debug_riverInput
   if (myID == master) then
      filename = trim(runID)//'_debug_riverInput.dat'
      open(debugRiverUnit,file=trim(filename),form='formatted',status='replace')
      filename = trim(runID)//'_debug_riverFilled.dat'
      open(debugRiverUnit+1,file=trim(filename),form='formatted',status='replace')
   endif
#endif

99 continue
   if (ierr/=0) then
      write( error_message, '("[",i2.2,"] ERROR - init_rivers",i3)') myID, ierr
      call stop_ecoham(ierr, msg=error_message)
   end if
   return

   end subroutine init_rivers

!=======================================================================
!
! !INTERFACE:
   subroutine get_rivers(td,ierr)
#define SUBROUTINE_NAME 'get_rivers'
!
! !DESCRIPTION:
! subroutine reads new values of river loads (mmol d-1)
!
! !USES:
   implicit none
!
! !INPUT PARAMETERS:
   real(kind=8), intent(in)     :: td
!
! !OUTPUT PARAMETERS:
   integer,      intent(inout)  :: ierr
!
! !LOCAL VARIABLES:
   real                         :: zero = 0.0
#ifndef cskc
   integer                      :: irec, ios
   integer                      :: ir, n
#else
   integer                      :: irec, ir
#endif
#ifdef RiverRedfieldPOM
   real                         :: riv_loadX
   integer                      :: m
   character(len=99)            :: fmtstr
#endif
!
!-----------------------------------------------------------------------
#include "call-trace.inc"

   if (num_riv <= 0) return

   irec = int(td+1.e-7)+1
#ifdef cskc
   ! calculation of riv_load from riv_conc_class and freshwater runoff
   ! fresh_amount: [m3 d-1], conc: (umol kg-1], riv_load: [mmol d-1] ==> no factor required
   if (lfreshwater) then
      do ir = 1, num_riv
         riv_load(:,ir) = fresh_amount(ir,irec) *riv_conc_class(:,class_riv(ir))
      enddo
   endif
#else
   ios = 0
   if (irec/=irec_river_old) then
      riv_load = real(fail, river_realkind)
      read(river_unit, rec=min(irec_river_max,irec), iostat = ios) riv_load(:,1:num_riv)
      if (ios /= 0) then
         ierr = 1
         write( error_message, '("[",i2.2,"] ERROR reading riv.bin")') myID
         call stop_ecoham(ierr, msg=error_message)
      endif
      irec_river_old = irec

#ifdef debug_riverInput
      if (myID == master) then
         !ir = 195 ! 51   73    195 - Elbe       D North Sea
         !ir = 195 ! 61   58    199 - Rhine      NL North Sea
         ir = 201 ! 63   56    201 - Scheldt    NL North Sea
         write(fmtstr,'(''(2i5,'',i3,''e20.10)'')') ipar_riv
         write(debugRiverUnit,fmtstr)  ir, irec, riv_load(:,ir)
      endif
#endif

      ! check consistency of river loads [riv_load(1:ipar_riv,1:num_riv)]
      allocate( load_check(n_found) )
      load_check = 1

#ifdef RiverRepairCO2SYS
      do ir = 1, num_riv
         if (fresh_amount(ir,irec)<eps) cycle          ! skip rivers without freshwater discharge
         riv_load(10,ir) = real(custom_dic(ir) * fresh_amount(ir,irec), kind=river_realkind)
         riv_load(12,ir) = real(custom_alk(ir) * fresh_amount(ir,irec), kind=river_realkind)
      enddo
#endif

#ifdef RiverRedfieldPOM
      do ir = 1, num_riv
         if (fresh_amount(ir,irec)<eps) cycle          ! skip rivers without freshwater discharge

         load_check = 1 ! assuming freshwaterloads beeing greater than zero
         where (riv_load(npo_ri(1:n_found),ir)<eps) load_check = 0
         if (sum(load_check).gt.0 .and. sum(load_check).lt.n_found) then
            do m = 1, n_found
               if (load_check(m) > 0) cycle
               log_msg = ''
               if (var_nam_riv(npo_ri(m))=='d1c' .or. var_nam_riv(npo_ri(m))=='d2c') then
                  fmtstr = '("WARNING: ",a3,"-loads for river(",i3,") missing")'
                  write( log_msg(1), trim(fmtstr) ) var_nam_riv(npo_ri(m)), ir
                  riv_loadX = zero
                  do n = 1, n_found ! first look for d1n
                     if     ( (var_nam_riv(npo_ri(n))=='d1n' .or. var_nam_riv(npo_ri(n))=='d2n') .and. load_check(n) > 0) then
                        riv_loadX = riv_load(npo_ri(n),ir) *6.625
                        fmtstr = '(" - substituted with: f_riv_",a3,"*6.625=",2e20.10 )'
                        write( log_msg(2), trim(fmtstr)) var_nam_riv(npo_ri(n)), riv_loadX, riv_load(npo_ri(n),ir)
                     endif
                  enddo
                  if (riv_loadX < eps) then
                     do n = 1, n_found ! then look for d1p
                        if ( (var_nam_riv(npo_ri(n))=='d1p' .or. var_nam_riv(npo_ri(n))=='d2p') .and. load_check(n) > 0) then
                           riv_loadX = riv_load(npo_ri(n),ir) *106.
                           fmtstr = '(" - substituted with: f_riv_",a3,"*106=  ",2e20.10 )'
                           write( log_msg(2), trim(fmtstr)) var_nam_riv(npo_ri(n)), riv_loadX, riv_load(npo_ri(n),ir)
                        endif
                     enddo
                  endif
                  if (riv_loadX > eps) riv_load(npo_ri(m),ir) = real(riv_loadX, river_realkind)
               elseif (var_nam_riv(npo_ri(m))=='d1n' .or. var_nam_riv(npo_ri(m))=='d2n') then
                  fmtstr = '("WARNING: ",a3,"-loads for river(",i3,") missing")'
                  write( log_msg(1), trim(fmtstr) ) var_nam_riv(npo_ri(m)), ir
                  riv_loadX = zero
                  do n = 1, n_found ! first look for d1c
                     if     ( (var_nam_riv(npo_ri(n))=='d1c' .or. var_nam_riv(npo_ri(n))=='d2c') .and. load_check(n) > 0) then
                        riv_loadX = riv_load(npo_ri(n),ir) /6.625
                        fmtstr = '(" - substituted with: f_riv_",a3,"/6.625=",2e20.10 )'
                        write( log_msg(2), trim(fmtstr)) var_nam_riv(npo_ri(n)), riv_loadX, riv_load(npo_ri(n),ir)
                     endif
                  enddo
                  if (riv_loadX < eps) then
                     do n = 1, n_found ! then look for d1p
                        if ( (var_nam_riv(npo_ri(n))=='d1p' .or. var_nam_riv(npo_ri(n))=='d2p') .and. load_check(n) > 0) then
                           riv_loadX = riv_load(npo_ri(n),ir) *16.
                           fmtstr = '(" - substituted with: f_riv_",a3,"*16=   ",2e20.10 )'
                           write( log_msg(2), trim(fmtstr)) var_nam_riv(npo_ri(n)), riv_loadX, riv_load(npo_ri(n),ir)
                        endif
                     enddo
                  endif
                  if (riv_loadX > eps) riv_load(npo_ri(m),ir) = real(riv_loadX, river_realkind)
               elseif (var_nam_riv(npo_ri(m))=='d1p' .or. var_nam_riv(npo_ri(m))=='d2p') then
                  fmtstr = '("WARNING: ",a3,"-loads for river(",i3,") missing")'
                  write( log_msg(1), trim(fmtstr) ) var_nam_riv(npo_ri(m)), ir
                  riv_loadX = zero
                  do n = 1, n_found ! first look for d1n
                     if     ( (var_nam_riv(npo_ri(n))=='d1n' .or. var_nam_riv(npo_ri(n))=='d2n') .and. load_check(n) > 0) then
                        riv_loadX = riv_load(npo_ri(n),ir) /16.
                        fmtstr = '(" - substituted with: f_riv_",a3,"/16=   ",2e20.10 )'
                        write( log_msg(2), trim(fmtstr)) var_nam_riv(npo_ri(n)), riv_loadX, riv_load(npo_ri(n),ir)
                     endif
                  enddo
                  if (riv_loadX < eps) then
                     do n = 1, n_found ! then look for d1c
                        if ( (var_nam_riv(npo_ri(n))=='d1c' .or. var_nam_riv(npo_ri(n))=='d2c') .and. load_check(n) > 0) then
                           riv_loadX = riv_load(npo_ri(n),ir) /106.
                           fmtstr = '(" - substituted with: f_riv_",a3,"/106=  ",2e20.10 )'
                           write( log_msg(2), trim(fmtstr)) var_nam_riv(npo_ri(n)), riv_loadX, riv_load(npo_ri(n),ir)
                        endif
                     enddo
                  endif
                  if (riv_loadX > eps) riv_load(npo_ri(m),ir) = real(riv_loadX, river_realkind)
               endif
#ifndef RiverWarningOff
               long_msg = trim( log_msg(1)) // trim( log_msg(2))
               if (myID == master) call write_log_message(long_msg); long_msg = ''
#endif
            enddo ! do m = 1, n_found
         endif
      enddo !do ir = 1, num_riv
#endif

      if (myID == master) then
         do ir = 1, num_riv
            if (fresh_amount(ir,irec)<eps) cycle          ! skip rivers without freshwater discharge
#ifndef RiverWarningOff
            load_check = 1 ! assuming freshwaterloads beeing greater than zero
            where (riv_load(npo_ri(1:n_found),ir)<eps) load_check = 0
            where (riv_load(npo_ri(1:n_found),ir)==-999.) load_check = 1
            if (sum(load_check).gt.0 .and. sum(load_check).lt.n_found) then
               write( log_msg(1), '("WARNING: loads for river(",i3,") may be inconsistent*")') ir
               write( log_msg(2), '(2x,"code=",12i1)')(load_check(n),n=1,n_found)
               !long_msg = trim( log_msg(1)) // new_line('a') // trim( log_msg(2))
               long_msg = trim( log_msg(1)) // trim( log_msg(2))
               call write_log_message(long_msg); long_msg = ''
            endif
#endif
            load_check = 1 ! assuming freshwaterloads beeing greater than zero
            where (riv_load(npo_ri(1:n_found),ir)<zero) load_check = 0
            where (riv_load(npo_ri(1:n_found),ir)==-999.) load_check = 1
            if (sum(load_check).gt.0 .and. sum(load_check).lt.n_found) then
               !write(*,'(10e20.10)') riv_load(npo_ri(1:n_found),ir)
               write( log_msg(1), '("WARNING: negative loads for river(",i3,")")') ir
               write( log_msg(2), '(2x,"code=",12i1)')(load_check(n),n=1,n_found)
               log_msg(1) = trim( log_msg(1)) // trim( log_msg(2))
               write( log_msg(2), '(" ... discharge concentration will be substituted with concentration from sea !" )')
               long_msg = trim( log_msg(1)) // new_line('a') // trim( log_msg(2))
               call write_log_message(long_msg); long_msg = ''
            endif
         enddo !do ir = 1, num_riv
      endif ! if (myID == master)

      deallocate (load_check)

#ifdef debug_riverInput
      if (myID == master) then
         !ir = 195 ! 51   73    195 - Elbe       D North Sea
         !ir = 195 ! 61   58    199 - Rhine      NL North Sea
         ir = 201 ! 63   56    201 - Scheldt    NL North Sea
         write(fmtstr,'(''(2i5,'',i3,''e20.10)'')') ipar_riv
         write(debugRiverUnit+1,fmtstr)  ir, irec, riv_load(:,ir)
      endif
#endif

   endif !if (irec/=irec_river_old)
#endif /* #ifdef cskc */

   ! apply river reduction coefficients
   riv_load = riv_coef *riv_load

   if (ierr/=0) then
      write( long_msg, '("[",i2.2,"] ERROR - get_rivers",i3)') myID, ierr
      call write_log_message(long_msg); long_msg = ''
   end if
   return

   end subroutine get_rivers
!
!=======================================================================
!
! !INTERFACE:
   subroutine read_fresh_amount(ierr)
#define SUBROUTINE_NAME 'read_fresh_amount'
!
! !DESCRIPTION:
!
! !USES:
   use mod_utils,      only : leerzeile, idays_of_month
   implicit none
!
! !IN/OUTPUT PARAMETERS:
   integer,      intent(inout)  :: ierr
!
! !LOCAL VARIABLES:
   real(kind=4), allocatable :: r4dummy(:)
   character(len=99) :: fmtstr
   logical           :: bin_in = .false.
   integer           :: id, im, ida
   integer           :: ir, ios
!
!-----------------------------------------------------------------------
#include "call-trace.inc"


   ! calculate number of days for current year
   im = 12 !! MK: idays_of_month(-,X,-,-) is intend(inout) !!
   call idays_of_month(year,im,ida,31)

   ! read freshwater input m3/d
   filename = trim(river_dir)//trim(freshwater_dat)
   if (index(filename,'.bin') /= 0 .or. index(filename,'.direct') /= 0) bin_in = .true.
   if (bin_in) then
      open(freshwater_unit, file=trim(filename), access='direct', form='unformatted', &
            recl=num_riv*4, convert = 'LITTLE_ENDIAN', status='old', iostat=ios)
   else
      open(asc_unit, file=trim(filename), status='old', iostat=ios)
   endif
   if (myID == master) then
      fmtstr = '(3x,"river freshwater",4x,"(unit=",i3.0,") from file: ",a)'
      write(long_msg,fmtstr) freshwater_unit, trim(filename)
      call write_log_message(long_msg); long_msg = ''
   endif
   if (ios/=0) then
      ierr = 1
      write( long_msg, '("[",i2.2,"] ERROR opening ",a)') myID, trim(filename)
      call write_log_message(long_msg); long_msg = ''
      goto 99
   endif
   if (bin_in) then         !read binary (m3 s-1) !!!
      allocate( r4dummy(num_riv) )
      r4dummy = 0.0
      do id = 1, ida
         read(freshwater_unit, rec=id, iostat=ios) r4dummy ! (m3 s-1) !!!
         fresh_amount(:,id) = real(r4dummy)*secs_per_day   ! convert (m3 s-1) -> (m3 d-1) !!!
         if (ios/=0) ierr = ierr + 1
      enddo
      close(freshwater_unit)
   else                     !read ascii (m3 d-1) !!!
      do ir = 1, num_riv
         call leerzeile(asc_unit,1)
         do id = 1, ida
            read(asc_unit,*) fresh_amount(ir,id)
         enddo
      enddo
      close(asc_unit)
   endif

99 continue
   if (ierr/=0) then
      write( error_message, '("[",i2.2,"] ERROR - read_fresh_amount: ",i3)') myID, ierr
      call stop_ecoham(ierr, msg=error_message)
   end if
   return

   end subroutine read_fresh_amount
!
!=======================================================================
!
! !INTERFACE:
   subroutine do_rivers(dt_,ierr)
#define SUBROUTINE_NAME 'do_rivers'
!
! !DESCRIPTION:
!
! !USES:
   use mod_var
   use mod_flux, only : f_riv  ! river discharge
   !use mod_flux, only : f_dil  ! river dilution
   implicit none
!
! !INPUT PARAMETERS:
   real,    intent(in)     :: dt_   ! timestep (days)
!
! !OUTPUT PARAMETERS:
   integer, intent(inout)  :: ierr
!
! !LOCAL VARIABLES:
   integer       :: i,j,k,k0,idd,m,n,ir
   real          :: v0, xfac, st_old, st_new
!
!-----------------------------------------------------------------------
#include "call-trace.inc"

   if (num_riv <= 0) return

   i = int(dt_)   ! to get rid of "unused" warning
   st_old = 0     ! to get rid of "unused" warning
   st_new = 0     ! to get rid of "unused" warning

   i=1;j=1;m=1;n=1  ! to get rid of warning:‘i’ may be used uninitialized

   idd = max(1,int(day_of_year-eps)+1)

   time_const = 0. !jp off
   if ( iwarm==0 .and. day_of_year<time_const ) then  !jp
      riv_warm = day_of_year/time_const
   else
      riv_warm = 1.
   endif

   if (riverdischarge==1) then
      do ir = 1, num_riv
         i = ipos_riv(ir)
         j = jpos_riv(ir)
         if (i >= iStartCalc .and. i <= iEndCalc) then ! each process checks its area
! if (ir==195) print*,'rivers#662',ir,i,j,dRiv(j,i)

            if ( lfreshwater .and. dRiv(j,i)<=zero ) cycle ! filter when freshwater<=0 (MK ?)

            k0 = min(kmax_river_input,k_index(j,i))

            v0 = 0.0
            do k = 1, k0
               v0 = v0 + vol(j,k,i)
            enddo

#ifdef old_discharge
            !print*,'old riverdischarge'

!             ! x1x is special ...
!             if ( lfreshwater .and. riveraddtracer==1 ) then
!                !special for x1x as riv_load(x1x) is not specified in riv.bin !!
!                f_riv(j,1,i,ix1x) = f_riv(j,1,i,ix1x) + riv_warm*fresh_amount(ir,idd)/area(j,i)
!             endif

            do k = 1, k0
               xfac = dz(j,k,i)/v0
               ! loop over variables existing in river and pelagic domain
               do n = 1, n_found
                  f_riv(j,k,i,npo_st(n)) = f_riv(j,k,i,npo_st(n))+riv_warm*riv_load(npo_ri(n),ir)*xfac
               enddo
            enddo
#else
            !print*,'new riverdischarge'

            ! loop over variables existing in river and pelagic domain
            do n = 1, n_found
               if (riv_load(npo_ri(n),ir)<=eps) cycle ! filter when load<=0 (MK: supposed beeing temporal hack!)
               do k = 1, k0
                  xfac = dz(j,k,i)/v0
                  select case (npo_ri(n))
                     case (9) !(poc -> d1c)
                           if (redfield_discharge==1) then
                              ! HACK to avoid uncontrolled growth of bactera, nudging d1c loads to d1n*redfield ratio
                              f_riv(j,k,i,npo_st(n)) = f_riv(j,k,i,npo_st(n)) + riv_warm*riv_load(6,ir)*6.625*xfac
                           else
                              ! traditional formulation according to D093 etc.
                              f_riv(j,k,i,npo_st(n)) = f_riv(j,k,i,npo_st(n)) + riv_warm*riv_load(npo_ri(n),ir)*xfac
                           endif
                     case default
                           f_riv(j,k,i,npo_st(n)) = f_riv(j,k,i,npo_st(n)) + riv_warm*riv_load(npo_ri(n),ir)*xfac
                  end select
               enddo !do k = 1, k0
            enddo !do n = 1, n_found
#endif
! if (ir==195) then
!    print*, riv_load(:,ir)
! endif
         end if ! if (i >= iStartCalc .and. i <= iEndCalc)

      enddo ! do ir = 1, num_riv
   endif ! if(riverdischarge==1)

   if (riveraddtracer==1) then
      if ( lfreshwater ) then
#ifdef old_discharge
         ! x1x is special as riv_load(x1x) is not specified in riv.bin !!
         where (dRiv(:,:) > zero .and. f_riv(:,1,:,ix1x) <= zero)
            f_riv(:,1,:,ix1x) = riv_warm*dRiv(:,:)/area(:,:)
         end where
#else
         ! add tracer to keep concentration the same, even it's not included in river.bin
         ! according to HAMSOM, this is restricted to the first layer only !!!
         ! loop over variables not specified in river forcing files
         do n = 1, n_st_var
            where (dRiv(:,:) > zero .and. f_riv(:,1,:,n) <= zero)
               f_riv(:,1,:,n) = riv_warm *st(:,1,:,n) *dRiv(:,:)/area(:,:)
            end where
         enddo
#endif
      else ! lfreshwater = .false.
         ! NOT READY JET!
      endif
   endif ! if (riveraddtracer==1) then

!===================================================================================
!MK: f_dil got redundant in context of rivers as f_riv was defined as "matter"-  MK!
!MK:  flux (mol m-2) and f_dil does not contain any material by definition!      MK!
!===================================================================================
!    if ( lfreshwater .and. riveraddtracer==-1 ) then
!       do i = iStartCalc, iEndCalc
!          do j = jStartCalc, jEndCalc
!             if (freshwater(j,i) > 0.0) then
!
!                k0 = min(kmax_river_input,k_index(j,i))
!                v0 = 0.0
!                do k = 1, k0
!                   v0 = v0 + vol(j,k,i)
!                enddo
!
!                do k = 1, k0
! #ifdef old_discharge
!                   !print*,'old riveraddtracer'
!
!                   ! x1x is special as riv_load(x1x) is not specified in riv.bin !!
!                   st_old = st(j,k,i,ix1x) + f_riv(j,k,i,ix1x)/dz(j,k,i) *dt_
!                   st_new = st_old/(1.+freshwater(j,i)*dt_/v0)
!                   f_dil(j,k,i,ix1x) = f_dil(j,k,i,ix1x) + (st_new-st_old)/dt_
!
!                   ! loop over variables existing in river and pelagic domain
!                   do n = 1, n_found
!                      if (f_riv(j,k,i,npo_st(n))>0.0) then
!                         st_old = st(j,k,i,npo_st(n)) + f_riv(j,k,i,npo_st(n))/dz(j,k,i) *dt_
!                         st_new = st_old/(1.+freshwater(j,i)*dt_/v0)
!                         f_dil(j,k,i,npo_st(n)) = f_dil(j,k,i,npo_st(n)) + (st_new-st_old)/dt_
!                      endif
!                   enddo
! #else
!                   !print*,'new riveraddtracer'
!                   do n = 1, n_st_var ! loop over all statevariables
!                      st_old = st(j,k,i,n) + f_riv(j,k,i,n)/dz(j,k,i) *dt_
!                      st_new = st_old/(1.+freshwater(j,i)*dt_/v0)
!                      f_dil(j,k,i,n) = f_dil(j,k,i,n) + (st_new-st_old)/dt_
!                      !st_new = st(j,k,i,n)
!                      !st_old = (st_new*dz(j,k,i) - f_riv(j,k,i,n)*dt_) / (dz(j,k,i)-freshwater(j,i)/area(j,i)*dt_)
!                      !f_dil(j,k,i,n) = (st_new-st_old)/dt_
!                   enddo
! #endif
!                enddo ! do k = 1, k0
!             endif ! if (freshwater(j,i) > 0.0)
!          enddo ! do j = jStartCalc, jEndCalc
!       enddo ! do i = iStartCalc, iEndCalc
!    endif ! if (riveraddtracer==1) then

   ! sum-up changes to sst
   do n = 1, n_st_var ! loop over all statevariables
      do i = iStartCalc, iEndCalc
         do j = jStartCalc, jEndCalc
            k0 = min(kmax_river_input,k_index(j,i))
            do k = 1, k0
               ! in case of units (mmol m-2) f_dil does no longer contribute to sst !!
               sst(j,k,i,n) = sst(j,k,i,n) + f_riv(j,k,i,n) !+ f_dil(j,k,i,n)
            enddo
         enddo
      enddo
   enddo

   if (ierr/=0) then
      write( long_msg, '("[",i2.2,"] ERROR - do_rivers",i3)') myID, ierr
      call write_log_message(long_msg); long_msg = ''
   end if
   return

   end subroutine do_rivers

!=======================================================================
!
! !INTERFACE:
   subroutine close_rivers(ierr)
#define SUBROUTINE_NAME 'close_rivers'
!
! !DESCRIPTION:
!
! !USES:
   implicit none
!
! !INPUT PARAMETERS:
   integer, intent(inout)  :: ierr
!
!-----------------------------------------------------------------------
#include "call-trace.inc"

   if (num_riv <= 0) return

   close(river_unit)

#ifdef debug_riverInput
   if (myID == master) close(debugRiverUnit)
   if (myID == master) close(debugRiverUnit+1)
#endif

   if (ierr/=0) then
      write( long_msg, '("[",i2.2,"] ERROR - close_rivers",i3)') myID, ierr
      call write_log_message(long_msg); long_msg = ''
   end if
   return

   end subroutine close_rivers

!=======================================================================

   end module mod_rivers

!=======================================================================
