#!/bin/tcsh
#
#---------------------------------------
# serial job
#---------------------------------------
#
set YEAR=$1
#
set Grid=NWCS20
set GridID=NWCS20D
set HydroID=H059
set MeteoID=NCEP
set AirseaID=EMEP-hybridClim
set BoundID=D093-noBelt
set RiverID=CEFAS-249
set SiltID=ERSEM
set ExtwID=ERSEM
#
# basic folders
set TargetDir=./Input
set BaseDir=/home/ifmlinux34a/ifmto/EM

# specific folders
set GridDir=$BaseDir'/grid/'$GridID
set HydroDir=$BaseDir'/forcing/ECOHAM/hydro/'$HydroID'/Avr24/'$YEAR
#set HydroDir=$BaseDir'/forcing/ECOHAM/hydro/'$HydroID'/Job.'$HydroID'.'$YEAR'.00'
set MeteoDir=$BaseDir'/forcing/ECOHAM/meteo/'$MeteoID'/'$Grid'/'$YEAR
#set MeteoDir=$BaseDir'/forcing/ECOHAM/meteo/'$MeteoID'-D093/'$Grid'/'$YEAR
set AirseaDir=$BaseDir'/forcing/ECOHAM/airsea/'$AirseaID'/'$Grid
#set AirseaDir=$BaseDir'/forcing/ECOHAM/airsea/'$AirseaID'-D093/'$Grid
set ExtwDir=$BaseDir'/forcing/ECOHAM/extw'
set SiltDir=$BaseDir'/forcing/ECOHAM/silt'
set RiverDir=$BaseDir'/forcing/ECOHAM/river/'$RiverID'/'$Grid
set RestDir=$BaseDir'/forcing/ECOHAM/restoring/'$GridID'/'$BoundID

if ( -e $TargetDir ) rm -r $TargetDir
mkdir -p  $TargetDir

#---------------------------------------
# set links
#---------------------------------------

# --- grid ---
ln -sf $GridDir'/indh-'$GridID'.dat'                                   $TargetDir/.

# --- HAMSOM hydro ---
set HydroPrefix='NAvr'
ln -sf $HydroDir'/'$HydroPrefix'S.'$HydroID'.'$YEAR'.direct'           $TargetDir'/salt.direct'
ln -sf $HydroDir'/'$HydroPrefix'T.'$HydroID'.'$YEAR'.direct'           $TargetDir'/temp.direct'
#ln -sf $HydroDir'/'$HydroPrefix'Z.'$HydroID'.'$YEAR'.direct'           $TargetDir'/zeta.direct' # use with AvrM2 only
ln -sf $HydroDir'/'$HydroPrefix'CumZ.'$HydroID'.'$YEAR'.direct'        $TargetDir'/zeta.direct'
ln -sf $HydroDir'/'$HydroPrefix'U.'$HydroID'.'$YEAR'.direct'           $TargetDir'/tu.direct'
ln -sf $HydroDir'/'$HydroPrefix'V.'$HydroID'.'$YEAR'.direct'           $TargetDir'/tv.direct'
#ln -sf $HydroDir'/'$HydroPrefix'W.'$HydroID'.'$YEAR'.direct'           $TargetDir'/tw.direct'
ln -sf $HydroDir'/'$HydroPrefix'Av.'$HydroID'.'$YEAR'.direct'          $TargetDir'/av.direct'
ln -sf $HydroDir'/'$HydroPrefix'Sm.'$HydroID'.'$YEAR'.direct'          $TargetDir'/sm.direct'
ln -sf $HydroDir'/'$HydroPrefix'Uv.'$HydroID'.'$YEAR'.direct'          $TargetDir'/ax.direct'
ln -sf $HydroDir'/'$HydroPrefix'Vv.'$HydroID'.'$YEAR'.direct'          $TargetDir'/ay.direct'

# --- old makehyd ---
#ln -sf $HydroDir'/s_'$HydroID'_'$YEAR'.bin'                           $TargetDir'/salt.bin'
#ln -sf $HydroDir'/t_'$HydroID'_'$YEAR'.bin'                           $TargetDir'/temp.bin'
#ln -sf $HydroDir'/tu_'$HydroID'_'$YEAR'.bin'                          $TargetDir'/tu.bin'
#ln -sf $HydroDir'/tv_'$HydroID'_'$YEAR'.bin'                          $TargetDir'/tv.bin'
#ln -sf $HydroDir'/tw_'$HydroID'_'$YEAR'.bin'                          $TargetDir'/tw.bin'
#ln -sf $HydroDir'/vdc_'$HydroID'_'$YEAR'.bin'                         $TargetDir'/vdc.bin'
#ln -sf $HydroDir'/vol_'$HydroID'_'$YEAR'.bin'                         $TargetDir'/vol.bin'

# --- meteo ---
# ln -sf $MeteoDir'/airt_'$MeteoID'_'$Grid'_'$YEAR'.direct'              $TargetDir'/airt.direct'
# ln -sf $MeteoDir'/clou_'$MeteoID'_'$Grid'_'$YEAR'.direct'              $TargetDir'/clou.direct'
# ln -sf $MeteoDir'/prec_'$MeteoID'_'$Grid'_'$YEAR'.direct'              $TargetDir'/prec.direct'
ln -sf $MeteoDir'/rad_'$MeteoID'_'$Grid'_'$YEAR'.direct'               $TargetDir'/rad.direct'
ln -sf $MeteoDir'/radday_'$MeteoID'_'$Grid'_'$YEAR'.direct'            $TargetDir'/radday.direct'
# ln -sf $MeteoDir'/relh_'$MeteoID'_'$Grid'_'$YEAR'.direct'              $TargetDir'/relh.direct'
# ln -sf $MeteoDir'/wind_'$MeteoID'_'$Grid'_'$YEAR'.direct'              $TargetDir'/wind.direct'
ln -sf $MeteoDir'/wins_'$MeteoID'_'$Grid'_'$YEAR'.direct'              $TargetDir'/wins.direct'

# --- airsea ---
ln -sf $AirseaDir'/atm_n_'$AirseaID'_'$Grid'_'$YEAR'.dat'              $TargetDir'/atm_n.dat'
#ln -sf $AirseaDir'/atm_n_'$AirseaID'_annual_'$Grid'_'$YEAR'.dat'       $TargetDir'/atm_n.dat'
#ln -sf $AirseaDir'/atm_n_'$AirseaID'_monthly_'$Grid'_'$YEAR'.dat'      $TargetDir'/atm_n_mon.dat'

# --- extw ---
ln -sf $ExtwDir'/extw_'$ExtwID'_'$GridID'_clim.direct'                 $TargetDir'/extw.direct'
#ln -sf $ExtwDir'/extw_ERSEM_'$GridID'_clim.dat'                        $TargetDir'/extw.dat'

# --- silt ---
ln -sf $SiltDir'/silt_'$SiltID'_'$GridID'_clim.direct'                 $TargetDir'/silt.direct'
#ln -sf $SiltDir'/porosity_'$Grid'.dat'                                 $TargetDir'/porosity.dat'

# --- river ---
ln -sf $RiverDir'/RiverIndices_'$RiverID'_'$Grid'.dat'                 $TargetDir'/RiverIndices.dat'
ln -sf $RiverDir'/freshwater_'$RiverID'_'$Grid'_'$YEAR'.dat'           $TargetDir'/freshwater.dat'
ln -sf $RiverDir'/freshwater_'$RiverID'_'$Grid'_'$YEAR'.direct'        $TargetDir'/freshwater.direct'
ln -sf $RiverDir'/river_'$RiverID'_'$Grid'_'$YEAR'.direct'             $TargetDir'/river.direct'

# --- restoring ---
ln -sf $RestDir'/'$YEAR'/rest_dic_'$GridID'_'$BoundID'_'$YEAR'.direct' $TargetDir'/rest_dic.direct'
ln -sf $RestDir'/'$YEAR'/rest_dic_'$GridID'_'$BoundID'_'$YEAR'.header' $TargetDir'/rest_dic.header'
#ln -sf $RestDir'/1997/rest_dic_'$GridID'_'$BoundID'_1997.direct'       $TargetDir'/rest_dic.direct'
#ln -sf $RestDir'/1997/rest_dic_'$GridID'_'$BoundID'_1997.header'       $TargetDir'/rest_dic.header'
#ln -sf $RestDir'/clim/rest_alk_'$GridID'_'$BoundID'_clim.direct'       $TargetDir'/rest_alk.direct'
ln -sf $RestDir'/clim/rest_alk_'$GridID'_'$BoundID'_clim_repair_jersey.direct'       $TargetDir'/rest_alk.direct'
ln -sf $RestDir'/clim/rest_alk_'$GridID'_'$BoundID'_clim.header'       $TargetDir'/rest_alk.header'
ln -sf $RestDir'/clim/rest_bac_'$GridID'_'$BoundID'_clim.direct'       $TargetDir'/rest_bac.direct'
ln -sf $RestDir'/clim/rest_bac_'$GridID'_'$BoundID'_clim.header'       $TargetDir'/rest_bac.header'
ln -sf $RestDir'/clim/rest_d1c_'$GridID'_'$BoundID'_clim.direct'       $TargetDir'/rest_d1c.direct'
ln -sf $RestDir'/clim/rest_d1c_'$GridID'_'$BoundID'_clim.header'       $TargetDir'/rest_d1c.header'
ln -sf $RestDir'/clim/rest_d1n_'$GridID'_'$BoundID'_clim.direct'       $TargetDir'/rest_d1n.direct'
ln -sf $RestDir'/clim/rest_d1n_'$GridID'_'$BoundID'_clim.header'       $TargetDir'/rest_d1n.header'
ln -sf $RestDir'/clim/rest_d1p_'$GridID'_'$BoundID'_clim.direct'       $TargetDir'/rest_d1p.direct'
ln -sf $RestDir'/clim/rest_d1p_'$GridID'_'$BoundID'_clim.header'       $TargetDir'/rest_d1p.header'
ln -sf $RestDir'/clim/rest_d2c_'$GridID'_'$BoundID'_clim.direct'       $TargetDir'/rest_d2c.direct'
ln -sf $RestDir'/clim/rest_d2c_'$GridID'_'$BoundID'_clim.header'       $TargetDir'/rest_d2c.header'
ln -sf $RestDir'/clim/rest_d2n_'$GridID'_'$BoundID'_clim.direct'       $TargetDir'/rest_d2n.direct'
ln -sf $RestDir'/clim/rest_d2n_'$GridID'_'$BoundID'_clim.header'       $TargetDir'/rest_d2n.header'
ln -sf $RestDir'/clim/rest_d2p_'$GridID'_'$BoundID'_clim.direct'       $TargetDir'/rest_d2p.direct'
ln -sf $RestDir'/clim/rest_d2p_'$GridID'_'$BoundID'_clim.header'       $TargetDir'/rest_d2p.header'
ln -sf $RestDir'/clim/rest_d2k_'$GridID'_'$BoundID'_clim.direct'       $TargetDir'/rest_d2k.direct'
ln -sf $RestDir'/clim/rest_d2k_'$GridID'_'$BoundID'_clim.header'       $TargetDir'/rest_d2k.header'
ln -sf $RestDir'/clim/rest_d2s_'$GridID'_'$BoundID'_clim.direct'       $TargetDir'/rest_d2s.direct'
ln -sf $RestDir'/clim/rest_d2s_'$GridID'_'$BoundID'_clim.header'       $TargetDir'/rest_d2s.header'
ln -sf $RestDir'/clim/rest_soc_'$GridID'_'$BoundID'_clim.direct'       $TargetDir'/rest_soc.direct'
ln -sf $RestDir'/clim/rest_soc_'$GridID'_'$BoundID'_clim.header'       $TargetDir'/rest_soc.header'
ln -sf $RestDir'/clim/rest_doc_'$GridID'_'$BoundID'_clim.direct'       $TargetDir'/rest_doc.direct'
ln -sf $RestDir'/clim/rest_doc_'$GridID'_'$BoundID'_clim.header'       $TargetDir'/rest_doc.header'
ln -sf $RestDir'/clim/rest_don_'$GridID'_'$BoundID'_clim.direct'       $TargetDir'/rest_don.direct'
ln -sf $RestDir'/clim/rest_don_'$GridID'_'$BoundID'_clim.header'       $TargetDir'/rest_don.header'
ln -sf $RestDir'/clim/rest_dop_'$GridID'_'$BoundID'_clim.direct'       $TargetDir'/rest_dop.direct'
ln -sf $RestDir'/clim/rest_dop_'$GridID'_'$BoundID'_clim.header'       $TargetDir'/rest_dop.header'
ln -sf $RestDir'/clim/rest_n1p_'$GridID'_'$BoundID'_clim.direct'       $TargetDir'/rest_n1p.direct'
ln -sf $RestDir'/clim/rest_n1p_'$GridID'_'$BoundID'_clim.header'       $TargetDir'/rest_n1p.header'
ln -sf $RestDir'/clim/rest_n3n_'$GridID'_'$BoundID'_clim.direct'       $TargetDir'/rest_n3n.direct'
ln -sf $RestDir'/clim/rest_n3n_'$GridID'_'$BoundID'_clim.header'       $TargetDir'/rest_n3n.header'
ln -sf $RestDir'/clim/rest_n4n_'$GridID'_'$BoundID'_clim.direct'       $TargetDir'/rest_n4n.direct'
ln -sf $RestDir'/clim/rest_n4n_'$GridID'_'$BoundID'_clim.header'       $TargetDir'/rest_n4n.header'
ln -sf $RestDir'/clim/rest_n5s_'$GridID'_'$BoundID'_clim.direct'       $TargetDir'/rest_n5s.direct'
ln -sf $RestDir'/clim/rest_n5s_'$GridID'_'$BoundID'_clim.header'       $TargetDir'/rest_n5s.header'
ln -sf $RestDir'/clim/rest_o2o_'$GridID'_'$BoundID'_clim.direct'       $TargetDir'/rest_o2o.direct'
ln -sf $RestDir'/clim/rest_o2o_'$GridID'_'$BoundID'_clim.header'       $TargetDir'/rest_o2o.header'
ln -sf $RestDir'/clim/rest_p1c_'$GridID'_'$BoundID'_clim.direct'       $TargetDir'/rest_p1c.direct'
ln -sf $RestDir'/clim/rest_p1c_'$GridID'_'$BoundID'_clim.header'       $TargetDir'/rest_p1c.header'
ln -sf $RestDir'/clim/rest_p1n_'$GridID'_'$BoundID'_clim.direct'       $TargetDir'/rest_p1n.direct'
ln -sf $RestDir'/clim/rest_p1n_'$GridID'_'$BoundID'_clim.header'       $TargetDir'/rest_p1n.header'
ln -sf $RestDir'/clim/rest_p1p_'$GridID'_'$BoundID'_clim.direct'       $TargetDir'/rest_p1p.direct'
ln -sf $RestDir'/clim/rest_p1p_'$GridID'_'$BoundID'_clim.header'       $TargetDir'/rest_p1p.header'
ln -sf $RestDir'/clim/rest_p1s_'$GridID'_'$BoundID'_clim.direct'       $TargetDir'/rest_p1s.direct'
ln -sf $RestDir'/clim/rest_p1s_'$GridID'_'$BoundID'_clim.header'       $TargetDir'/rest_p1s.header'
ln -sf $RestDir'/clim/rest_p2c_'$GridID'_'$BoundID'_clim.direct'       $TargetDir'/rest_p2c.direct'
ln -sf $RestDir'/clim/rest_p2c_'$GridID'_'$BoundID'_clim.header'       $TargetDir'/rest_p2c.header'
ln -sf $RestDir'/clim/rest_p2n_'$GridID'_'$BoundID'_clim.direct'       $TargetDir'/rest_p2n.direct'
ln -sf $RestDir'/clim/rest_p2n_'$GridID'_'$BoundID'_clim.header'       $TargetDir'/rest_p2n.header'
ln -sf $RestDir'/clim/rest_p2p_'$GridID'_'$BoundID'_clim.direct'       $TargetDir'/rest_p2p.direct'
ln -sf $RestDir'/clim/rest_p2p_'$GridID'_'$BoundID'_clim.header'       $TargetDir'/rest_p2p.header'
ln -sf $RestDir'/clim/rest_p3c_'$GridID'_'$BoundID'_clim.direct'       $TargetDir'/rest_p3c.direct'
ln -sf $RestDir'/clim/rest_p3c_'$GridID'_'$BoundID'_clim.header'       $TargetDir'/rest_p3c.header'
ln -sf $RestDir'/clim/rest_p3n_'$GridID'_'$BoundID'_clim.direct'       $TargetDir'/rest_p3n.direct'
ln -sf $RestDir'/clim/rest_p3n_'$GridID'_'$BoundID'_clim.header'       $TargetDir'/rest_p3n.header'
ln -sf $RestDir'/clim/rest_p3p_'$GridID'_'$BoundID'_clim.direct'       $TargetDir'/rest_p3p.direct'
ln -sf $RestDir'/clim/rest_p3p_'$GridID'_'$BoundID'_clim.header'       $TargetDir'/rest_p3p.header'
ln -sf $RestDir'/clim/rest_p3k_'$GridID'_'$BoundID'_clim.direct'       $TargetDir'/rest_p3k.direct'
ln -sf $RestDir'/clim/rest_p3k_'$GridID'_'$BoundID'_clim.header'       $TargetDir'/rest_p3k.header'
ln -sf $RestDir'/clim/rest_x1x_'$GridID'_'$BoundID'_clim.direct'       $TargetDir'/rest_x1x.direct'
ln -sf $RestDir'/clim/rest_x1x_'$GridID'_'$BoundID'_clim.header'       $TargetDir'/rest_x1x.header'
ln -sf $RestDir'/clim/rest_z1c_'$GridID'_'$BoundID'_clim.direct'       $TargetDir'/rest_z1c.direct'
ln -sf $RestDir'/clim/rest_z1c_'$GridID'_'$BoundID'_clim.header'       $TargetDir'/rest_z1c.header'
ln -sf $RestDir'/clim/rest_z2c_'$GridID'_'$BoundID'_clim.direct'       $TargetDir'/rest_z2c.direct'
ln -sf $RestDir'/clim/rest_z2c_'$GridID'_'$BoundID'_clim.header'       $TargetDir'/rest_z2c.header'

exit