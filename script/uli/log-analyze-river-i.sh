#!/bin/bash

# usage: ./log-analyze-river-i.sh
# alternative usage: ./log-analyze-river-i.sh logfile-name   
# default name is eco_logfile.dat

logfile=${1:-eco_logfile.dat}

echo analyzing logfile ${logfile}
echo river information from error STATE VARIABLE negative 

# create unique filename for riverFile
riverFile=riverfile$$.txt

awk -F" " '{ print $1 == "i" ? $2 : " " }' ${logfile} > ${riverFile}
river_i_values=$( cat ${riverFile} | sort -nu)

echo ${river_i_values}

for i in ${river_i_values}
do
   riverCount=$( cat ${riverFile} | grep $i | wc -w )
   echo -e latitude index $i \\t count ${riverCount}
done

sum=$(cat ${riverFile} | grep [0-9] | wc -w)
echo total count:  $sum

rm ${riverFile}
