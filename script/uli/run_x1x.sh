#!/bin/csh

set srcDIR=../src
set runDIR=`pwd`
set TARGET=x1x

echo '======================================================'
echo 'srcDIR:='$srcDIR
echo 'runDIR:='$runDIR
echo '------------------------------------------------------'

cd $runDIR
ln -sf $srcDIR/$TARGET .

# mandatory for running on blizzard !
#------------------------------------------------------------------------
set NAME=`hostname | awk '{print substr($1,1,8)}'`

if ($NAME == "blizzard" || `hostname` == "p249" ) then
   echo ' '
   echo '------------------------------------------------------'
   setenv XLFRTEOPTS "ufmt_littleendian=220,221,222,223,224,225,226"
   echo 'set XLFRTEOPTS = '$XLFRTEOPTS
else
endif
#------------------------------------------------------------------------
echo ' '
echo '======================================================'
echo '  '$TARGET' start running at '`date`
echo '------------------------------------------------------'
if ( `hostname` == "p249" ) then
    #poe ./$TARGET -procs 2 -hostfile $HOME/hostfile-p249
    mpiexec -n 4 ./$TARGET -hostfile $HOME/hostfile-p249
else
    switch ( $1 )
        case  [0-9]:                    # one digit for number of processes
        case  [0-9][0-9]:               # two digits ...
            mpiexec -n $1 ./$TARGET
            breaksw
        default:
            ./$TARGET
#           ./x1x >& run_x1x.log
    endsw
endif
echo '------------------------------------------------------'
echo '  '$TARGET' finished at      '`date`
echo '======================================================'
echo ' '
