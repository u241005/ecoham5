#!/bin/bash
# mode for interactive mpi runs

echo ===========================
echo running in interactive mpi
echo ===========================

NPROCS=4
EXECUTE_CMD="mpiexec -n ${NPROCS}"
BATCH_CMD=""
JOBSCRIPT_NAME=$0

# load run script
. runscript-eco5.sh
