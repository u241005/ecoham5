#!/bin/bash
# prepare tarfile for archive
# remove redundant data files and compress the original output

ECOGRID=NS03A

TIMEFORMAT='%3lR'
# see http://stackoverflow.com/questions/3683434/using-time-command-in-bash-script
#/usr/bin/time -f "elapsed real time %E" gzip --fast phc-2004.nc

if test $# -eq 0
then
   echo year is needed as argument
   echo usage: $0 2002
   exit
elif test $# -eq 1
then
   year=$1
else
   echo wrong number of arguments
   echo usage: $0 2002
   exit
fi

echo remove intermediate datasets phc.nc and subset.nc
rmList=$(ls run-${year}.??.??-??.??/phc.nc run-${year}.??.??-??.??/subset.nc)
if test -f subset-${year}.nc
then
   rm -f ${rmList}
else
   echo file subset-${year}.nc not found
   echo check processing status before compressing files for archive
   echo exit script
   exit
fi

# compress the model output
gzipList=$(ls run-${year}.??.??-??.??/${ECOGRID}_3D.nc phc-${year}.nc subset-${year}.nc)
for filename in ${gzipList}
do
   command="gzip --fast ${filename}"
   echo ${command}; time ${command}
done

