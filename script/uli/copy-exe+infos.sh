#!/bin/bash
# write source code status to file in result directory

# !!! blizzard specific at the moment !!!
# 2 do : add wizard and wr cluster
# on wizard use ". /etc/profile" for module environment

if test $# -eq 0
then
   echo ERROR: missing path to git repository source code directory
   echo usage: $0 path2src
   exit
fi

gitRepSrcDir=$1
cd ..
runDir=$(pwd)
wrkDir=${runDir}/wrk
srcDir=${runDir}/src
resultBaseDir=${runDir}/res

echo working directory is: ${wrkDir}
if ! test -d ${resultBaseDir}
then
   echo creating directory result
   mkdir ${resultBaseDir}
fi

StatusFile=${srcDir}/src-status.txt
rm -f $StatusFile

cd ${gitRepSrcDir}

# collect all neccessary information for reconstruction of src directory from the repository
echo "ECOHAM: source code status for actual run " >  $StatusFile
echo "=========================================" >> $StatusFile
echo -e "\n === last commit ===" >> $StatusFile
git log | head -n 5 >> $StatusFile
echo -e "\n === git status ===" >> $StatusFile
git status >> $StatusFile
echo -e "\n === git diff ===" >> $StatusFile
git diff >> $StatusFile
echo -e "\n === git branch -a ===" >> $StatusFile
git branch -a >> $StatusFile
echo -e "\n === cat .git/config ===" >> $StatusFile
cat ../.git/config >> $StatusFile

# settings for compilation of 3 km grid and mpi on blizzard
export MK_CONFIG_FILE=aix_mpi.mk
export ECOHAM_GRID=NS03A
make -j4

# copy source code directory
cp -r $gitRepSrcDir ${wrkDir}/..

# remove object files in source code copy
cd ${wrkDir}/../src
make clean_objects

cd ${wrkDir}
ln -sf ../src/eco5

