#!/bin/bash
# slurm config, /etc/slurm-llnl/slurm.conf
# time in minutes or hh:mm:ss
#SBATCH --time=08:00:00
#SBATCH --mem=50gb
# if the line below is a comment start the job with the cmd e.g. $sbatch -n 4 run_eco5.csh.slurm 4
#SBATCH -N 1 -n 16
#SBATCH --error=job.err.%j --output=job.out.%j

# srun hostname
NPROCS=16

wrkDIR=$(pwd)
runDIR=${wrkDIR}/..
resultBaseDir=${runDIR}/res
TARGET=eco5
ECOGRID='NS03A'

if [ $(hostname) = "p249" ]; then
   DATECMD=/client/bin/date
else
   DATECMD=$(which date)
fi

#-----------------------
# job specific settings
#-----------------------
# format for DateStrings is yyyy-mm-dd
# default time frame for the job chain is one year
jobChain_StartDateStr=2013-01-01
# uncomment and set jobChain_EndDateStr if less than one year should be processed 
#jobChain_EndDateStr=2003-03-02

# format for TimeInterval is "time unit", e.g. "15 days" or "1 month" (see the unix command date)
#job_TimeInterval="15 days"
job_TimeInterval="1 month"

jobChain_EndDateStrDefault=$(${DATECMD} --date "$jobChain_StartDateStr +1 years -1 days" "+%Y-%m-%d")
jobChain_EndDateStr=${jobChain_EndDateStr:-$jobChain_EndDateStrDefault}
if [[ $jobChain_EndDateStr < $jobChain_StartDateStr ]]; then
   echo -e "JOBCHAIN ERROR: \t begin $jobChain_StartDateStr \t end $jobChain_EndDateStr"
   exit
fi

if [ -f job-datestring.txt ]; then
   job_StartDateStr=$(${DATECMD} --date "$(cat job-datestring.txt) +1 days" "+%Y-%m-%d")
else
   job_StartDateStr=$jobChain_StartDateStr    # first job of chain
   #cp forcing-3km/eco5_warm_3km.in-old-structure NS03A_warmstart.in
   #cp forcing-3km/NS03A_warmstart.in.bkp-new-no-chemistry NS03A_warmstart.in
fi

# minus 1 day because program start is at 00:00:00 and end at 24:00:00
job_EndDateStr=$(${DATECMD} --date "$job_StartDateStr +$job_TimeInterval -1 days" "+%Y-%m-%d")
if [[ $job_EndDateStr > $jobChain_EndDateStr ]]; then
   job_EndDateStr=$jobChain_EndDateStr
fi

# get substrings for directory path
# for runs within a year => yearEndStr is not needed
yearStartStr=$(${DATECMD} --date "$job_StartDateStr" "+%Y")
monStartStr=$(${DATECMD} --date "$job_StartDateStr" "+%m")
dayStartStr=$(${DATECMD} --date "$job_StartDateStr" "+%d")
monEndStr=$(${DATECMD} --date "$job_EndDateStr" "+%m")
dayEndStr=$(${DATECMD} --date "$job_EndDateStr" "+%d")

# get strings without leading zeros
monStart=$(echo $monStartStr | sed 's/0*//')
dayStart=$(echo $dayStartStr | sed 's/0*//')
monEnd=$(echo $monEndStr | sed 's/0*//')
dayEnd=$(echo $dayEndStr | sed 's/0*//')

# create job specific result subdirectory
resultDir=${resultBaseDir}/run-${yearStartStr}.${monStartStr}.${dayStartStr}-${monEndStr}.${dayEndStr}
#echo ${yearStartStr} ${monStartStr} ${dayStartStr} ${monEndStr} ${dayEndStr}

cd $wrkDIR

if ! test -d ${resultBaseDir} ; then
   mkdir ${resultBaseDir}
fi
if ! test -f ${resultBaseDir}/concat-prt.py ; then
   ln -s concat-prt.py     ${resultBaseDir}
   ln -s concat-logfile.sh ${resultBaseDir}
   ln -s concat-subset.sh  ${resultBaseDir}
fi

# create eco_set.nml from template
sed -e "s/YYYY/${yearStartStr}/" \
    -e "s/START_MM/      ${monStart}/" \
    -e "s/START_DD/      ${dayStart}/" \
    -e "s/END_MM/    ${monEnd}/" \
    -e "s/END_DD/    ${dayEnd}/" \
    eco_set.nml.template-${ECOGRID} > eco_set.nml

# run executable
echo '======================================================'
echo 'wrkDIR:='$wrkDIR
echo '------------------------------------------------------'

rm -f eco_logfile.dat

echo ' '
echo '======================================================'
echo '  '$TARGET' start running at '`date`
echo '------------------------------------------------------'

. /etc/profile
module load mpich2/1.3.1-static-gcc46
mpiexec -n ${NPROCS} ./$TARGET

echo '------------------------------------------------------'
echo '  '$TARGET' finished at      '`date`
echo '======================================================'
echo ' '

# job postprocessing
#====================
mkdir ${resultDir}
echo ${resultDir}
# value 1 in Jeb_OK means correct end of program
JobStatus=$(cat Jeb_OK)
echo JobStatus ${JobStatus}
if (( JobStatus==1 ))
then
   mv NS03A_warmstart.out  ${resultDir}
   ln -sf ${resultDir}/NS03A_warmstart.out NS03A_warmstart.in
   cp eco_bio.nml          ${resultDir}
   mv eco_set.nml          ${resultDir}
   mv eco_logfile.dat      ${resultDir}
   cp job-datestring.txt   ${resultDir}
   mv *.prt                ${resultDir}
   mv eco_set.nml          ${resultDir}
   mv NS03A_3D.nc          ${resultDir}
   mv job.???.*            ${resultDir}
else
   echo "error in job -> exit"
   exit
fi

if [[ $job_EndDateStr = $jobChain_EndDateStr ]] ; then
   rm job-datestring.txt
   rm Jeb_OK
   echo last job in chain
   echo -----------------
   exit
else
   echo $job_EndDateStr > job-datestring.txt
   echo submit new job
   echo --------------
   sbatch run_eco5-wiz.slurm-16.sh
fi
