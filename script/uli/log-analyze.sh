#!/bin/bash

# usage: ./log-analyze.sh
# alternative usage: ./log-analyze.sh logfile-name   
# default name is eco_logfile.dat

logfile=${1:-eco_logfile.dat}

./log-analyze-level.sh ${logfile}
./log-analyze-river-i.sh ${logfile}
