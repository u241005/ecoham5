#!/bin/bash
# extract p1c and p2c from netcdf output file
# and concatenate the data from all given directories
# usage: $0 dir_1 dir_2 ... dir_n
declare -a dirList

# nco on wizard
# module load nco/4.0.3-gccsys

# separate variables from input file and concatenate to yearly files
# run in directory result

dirList=( ${@} )
if [[ "${dirList}" == "" ]]
then
   echo ERROR: directory list is empty
   echo usage: $0 dir_1 dir_2 ... dir_n
   exit
fi

for dirname in ${dirList[@]}
do
   if ! test -d $dirname
   then
      echo not a directory: $dirname 
      echo usage: $0 dir_1 dir_2 ... dir_n
      exit
   fi
done

# sort list
dirList=($(echo ${dirList[@]} | awk 'BEGIN{RS=" ";} {print $1}' | sort))
echo directory list: ${dirList[@]}
numDataSets=${#dirList[@]}

echo numDataSets $numDataSets
yearStart=${dirList[0]:4:4}
 monStart=${dirList[0]:9:2}
 dayStart=${dirList[0]:12:2}
yearEnd=${dirList[(($numDataSets-1))]:4:4}
 monEnd=${dirList[(($numDataSets-1))]:15:2}
 dayEnd=${dirList[(($numDataSets-1))]:18:2}
echo $yearStart $monStart $dayStart
echo $yearEnd $monEnd $dayEnd

phc_concat_file=phc_$yearStart.$monStart.$dayStart-$yearEnd.$monEnd.$dayEnd.nc
echo phc_concat_file $phc_concat_file

subsetList=''
phcList=''
forCount=0

for dirname in ${dirList[@]}
do
   echo ----------------------------------------
   echo processing files in ${dirname}
   echo ----------------------------------------

   # get the number of time records in the netcdf file
   # the number at the end of the year may be different from the other jobs count
   daysPerJob=$(ncdump -v time ${dirname}/NS03A_3D.nc | grep -i unlimited | tr -d [:alpha:] | tr -d [:punct:])
   ((daysPerJob=daysPerJob -1))
   echo number of records $daysPerJob
   # skip the first record (counting starts with 0) for all further files
   #if ((forCount==0)); then
   #   ncksRecOption=""
   #else
      ncksRecOption="-d time,1,${daysPerJob}"
   #fi

   echo extracting p1c and p2c
   rm -f ${dirname}/p1c.nc ${dirname}/p2c.nc ${dirname}/phc.nc
   ncks ${ncksRecOption} -v p1c ${dirname}/NS03A_3D.nc ${dirname}/p1c.nc
   ncks ${ncksRecOption} -v p2c ${dirname}/NS03A_3D.nc ${dirname}/p2c.nc

   # renaming p1c and p2c to phc (neccessary for add operation)
   ncrename -v p1c,phc ${dirname}/p1c.nc
   ncatted -a long_name,phc,o,c,"phytoplankton-C"  ${dirname}/p1c.nc
   ncrename -v p2c,phc ${dirname}/p2c.nc
   ncatted -a long_name,phc,o,c,"phytoplankton-C"  ${dirname}/p2c.nc

   echo add p1c and p2c up to phc
   ncbo --op_typ=add ${dirname}/p1c.nc ${dirname}/p2c.nc ${dirname}/phc.nc
   rm -f ${dirname}/p1c.nc ${dirname}/p2c.nc
   phcList="${phcList} ${dirname}/phc.nc"

   ((forCount++))
done

echo concatenate phc files of given directories
rm -f $phc_concat_file
#echo ${phcList}
ncrcat ${phcList} $phc_concat_file
