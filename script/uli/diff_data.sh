#!/bin/bash

# compare with x1x results of the first version

if [ "$(hostname)" = "wrcobios" ]; then
   #COMPARE_PATH=../../ECOHAM5/wrk
   COMPARE_PATH=/home/koerner/ecoham/ECOHAM5-markus/ECOHAM5/wrk
elif [ "$(hostname)" = "cobios2" ]; then
   COMPARE_PATH=../../ECOHAM5/wrk
   COMPARE_PATH=/home/koerner/test/git-test/ECOHAM5_par/wrk
   COMPARE_PATH=/home/koerner/ecoham/markus/ECOHAM5/wrk
elif [ "$(hostname)" = "cluster" ]; then
   COMPARE_PATH=/home/koerner/ecoham/markus/ECOHAM5/wrk
else
   COMPARE_PATH=/home/koerner/ecoham/ECOHAM5-markus/ECOHAM5/wrk
fi

fileList='eco4_3D.nc eco4_ij_??_??.prt eco4_warmstart.out'
#if [[ $1 == "log" ]]
#then
#	fileList="${fileList} eco4_prt.dat"
#fi

for file in ${fileList}
do
    CMD="diff ${file} ${COMPARE_PATH}/${file}"
    echo $CMD; $CMD
done

# logfile diff is optional, because multi process runs have different logfile content
logfile='eco_logfile.dat'
if [[ $1 == "log" ]]
then
	CMD="diff ${logfile} ${COMPARE_PATH}/${logfile}"
	echo $CMD; $CMD
fi
