#!/bin/bash
# concatenate all logfiles of a year

if test $# -eq 0
then
   echo year is needed as argument
   echo usage: $0 2002
   exit
elif test $# -eq 1
then
   year=$1
else
   echo wrong number of arguments
   echo usage: $0 2002
   exit
fi

dirList=$(ls |grep run-${year})
echo directory list: $dirList
if [[ "${dirList}" == "" ]]
then
   echo ERROR: directory list is empty
   echo check directory path and year
   exit
fi

concatLogfile=eco_logfile-${year}.dat

rm -f ${concatLogfile}

for dir in $dirList
do
   cat ${dir}/eco_logfile.dat >> ${concatLogfile}
done
