#!/bin/bash

# nco on wizard
# module load nco/4.0.3-gccsys

# separate variables from input file and concatenate to yearly files
# run in directory result

if test $# -eq 0
then
   echo year is needed as argument
   echo usage: $0 2002
   exit
elif test $# -eq 1
then
   year=$1
else
   echo wrong number of arguments
   echo usage: $0 2002
   exit
fi

# test case lists
# dirList='run-1997.01.01-01.16 run-1997.01.17-02.01'
# dirList='run-1997.12.03-12.18 run-1997.12.19-12.31'
# dirList='run-1997.12.19-12.31'
# dirList='run-1997.01.01-01.16 run-1997.01.17-02.01 run-1997.12.19-12.31'

dirList=$(find ./run-${year}* -name NS03A_3D.nc -exec dirname {} \;)
# build up list with directories that contain *_3D.nc
# the last directory in the list may contain prt files but no netcdf file
# because a broken job chain has an incomplete netcdf file at the end
echo directory list: $dirList
if [[ "${dirList}" == "" ]]
then
   echo ERROR: directory list is empty
   echo check directory path and year
   exit
fi

subsetList=''
phcList=''
forCount=0

for dir in $dirList
do
   echo ----------------------------------------
   echo processing files in ${dir}
   echo ----------------------------------------

   # get the number of time records in the netcdf file
   # the number at the end of the year may be different from the other jobs count
   daysPerJob=$(ncdump -v time ${dir}/NS03A_3D.nc | grep -i unlimited | tr -d [:alpha:] | tr -d [:punct:])
   ((daysPerJob=daysPerJob -1))
   echo number of records $daysPerJob
   # skip the first record (counting starts with 0) for all further files
   if ((forCount==0)); then
      ncksRecOption=""
   else
      ncksRecOption="-d time,1,${daysPerJob}"
   fi

   echo extracting large subset of netcdf file
   rm -f ${dir}/subset.nc
   ncks ${ncksRecOption} -v n1p,n3n,n4n,n5s,p1c,p2c,z1c,z2c,dic,o2o ${dir}/NS03A_3D.nc ${dir}/subset.nc
   subsetList="${subsetList} ${dir}/subset.nc"

   echo extracting p1c and p2c
   rm -f ${dir}/p1c.nc ${dir}/p2c.nc ${dir}/phc.nc
   ncks ${ncksRecOption} -v p1c ${dir}/NS03A_3D.nc ${dir}/p1c.nc
   ncks ${ncksRecOption} -v p2c ${dir}/NS03A_3D.nc ${dir}/p2c.nc

   # renaming p1c and p2c to phc (neccessary for add operation)
   ncrename -v p1c,phc ${dir}/p1c.nc
   ncatted -a long_name,phc,o,c,"phytoplankton-C"  ${dir}/p1c.nc
   ncrename -v p2c,phc ${dir}/p2c.nc
   ncatted -a long_name,phc,o,c,"phytoplankton-C"  ${dir}/p2c.nc

   echo add p1c and p2c up to phc
   ncbo --op_typ=add ${dir}/p1c.nc ${dir}/p2c.nc ${dir}/phc.nc
   rm -f ${dir}/p1c.nc ${dir}/p2c.nc
   phcList="${phcList} ${dir}/phc.nc"

   ((forCount++))
done

echo concatenate subset files to yearly file
rm -f subset-${year}.nc
#echo ${subsetList}
ncrcat ${subsetList} subset-${year}.nc

echo concatenate phc files to yearly file
rm -f phc-${year}.nc
#echo ${phcList}
ncrcat ${phcList} phc-${year}.nc
