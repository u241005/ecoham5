#!/bin/bash
# slurm config, /etc/slurm-llnl/slurm.conf
#SBATCH --time=08:00:00
#SBATCH --mem=50gb
#SBATCH -N 1 -n 16
#SBATCH --error=job.err.%j --output=job.out.%j

NPROCS=16

# load module for mpiexec
. /etc/profile
module load mpich2/1.3.1-static-gcc46

EXECUTE_CMD="mpiexec -n ${NPROCS}"
BATCH_CMD=sbatch
JOBSCRIPT_NAME=$0

# load ecoham run script  !!! check if the value of job_TimeInterval fits into the wall_clock_limit !!!
. runscript-eco5.sh
