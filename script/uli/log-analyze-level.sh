#!/bin/bash

# usage: ./log-analyze-level.sh
# alternative usage: ./log-analyze-level.sh logfile-name   
# default name is eco_logfile.dat

logfile=${1:-eco_logfile.dat}

echo analyzing logfile ${logfile}
echo level information from error STATE VARIABLE negative 

levelList=$(awk -F" " '{ print $2~/level/ ? $4 : " " }' ${logfile} | sort -nu)
echo ${levelList}

# create unique filename for levelFile
levelFile=levelfile$$.txt
awk -F" " '{ print $2~/level/ ? $4 : " " }' ${logfile} > ${levelFile}

for l in ${levelList}
do
   levelCount=$( cat ${levelFile} | grep $l | wc -w )
   echo level $l count ${levelCount}
done

sum=$(cat ${levelFile} | grep [0-9] | wc -w)
echo total count:  $sum

rm ${levelFile}
