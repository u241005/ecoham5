#!/usr/bin/python
import sys
import os
import os.path
import glob
import re
import shutil
'''
list prt-files and concatenate to a one year file
all header sections from the appended files have to be removed
'''

if len(sys.argv) == 2:
   year=sys.argv[1]
   print("processing year "+year)
else:
   print("wrong number of arguments")
   print("usage: "+sys.argv[0]+" 2003")
   sys.exit()

def yearly_filename(filename, year):
   # create name for yearly prt-file, insert year in position prt-file
   (filebase, extension) = os.path.splitext(filename)
   yearlyFile=filebase+'.'+year+extension
   return(yearlyFile)

def append_file(input_file, year_prtfile):
   # read input prt-file and write the new sections to the yearly prt-file
   infile = open(input_file, mode='r')
   text = infile.read()
   infile.close()
   matchList=re.compile('time').finditer(text)
   i=0
   pos={}
   for match in matchList:
      pos[i]=(match.start())
      i=i+1
   outfile = open(year_prtfile, mode='a')
   outfile.write(text[pos[1]:])
   outfile.close()
   
# create prt-file list from the directory at the beginning of the year
globString='run-'+year+'.01.01-*/*.prt'
prt_filelist=sorted(glob.glob(globString))
if len(prt_filelist) == 0:
   print("ERROR: file list is empty")
   print("check directory path and year")
   sys.exit()

for pathname in prt_filelist[0:]:
   filename=os.path.basename(pathname)
   yearfile = yearly_filename(filename, year)
   print("concatenating files into yearly prt-file "+yearfile)

   # remove yearly file if it exists
   try:
      os.remove(yearfile)
   except:
      print('os.remove exception: no old yearly file to remove '+yearfile)

   # copy first file in list (with header information)
   shutil.copyfile(pathname, yearfile)

   # create directory list
   globString="*/"+filename
   file_list=sorted(glob.glob(globString))

   # concatenate to yearly file
   for file_name in file_list[1:]:
      # print('loop over all directories for prt file: '+filename)
      append_file(file_name, yearfile)
