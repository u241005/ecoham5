#!/bin/tcsh
# source /SX/opt/etc/initsx.csh

# module swap IBM/xlf12.1.0.3 IBM/xlf13.1.0.2

set On            = 1
set Off           = 0
#set RunID         = B001
set RunID         = $1
set ExecuteModel  = $Off
set NoOfProcs     = 16
if ( ${NoOfProcs} > 1 ) then
  set FORTRAN_COMPILER = xlf_mpi
else
  set FORTRAN_COMPILER = xlf
endif

set HomeDir  = $HOME
#set HomeDir = /pf/u/${USER}
#set SourceDir     = ${HomeDir}/HAMSOM_3p
set SourceDir     = `pwd`
set Source        = src
set Script        = script
set Project       = ECOHAM
set Grid          = NWCS20
set GridID        = NWCS20D
set Model         = ecoham5


echo '**********************************************************'
echo '***  running compile job for RunID '$RunID
echo '***                          Model '$Model
echo '***                         GridID '$GridID
echo '***               source code from '${SourceDir}/${Source}
echo '***   using compiler configuration '${FORTRAN_COMPILER}'.config'
echo '**********************************************************'

setenv FORTRAN_COMPILER ${FORTRAN_COMPILER}
setenv EXECUTABLE       ${Model}
setenv ECOHAM_GRID      ${GridID}

set BaseDir  = /work/scratch/u/${USER}
set WorkDir  = ${BaseDir}/${Project}.${RunID}

set ListDir  = ${WorkDir}/list.${RunID}
set ObjDir   = ${WorkDir}/objects.${RunID}
set TempDir  = ${WorkDir}/${Source}.${RunID}
set ScriptDir = ${WorkDir}/${Script}.${RunID}

mkdir -p ${WorkDir}
mkdir -p ${TempDir}

cd    ${TempDir}
cp -p ${SourceDir}/${Source}/*.f90  ./
cp -p ${SourceDir}/${Source}/*.inc  ./
if (-e ${ObjDir}) then
  cp -p ${ObjDir}/*.o               ./
  cp -p ${ObjDir}/*.mod             ./
endif
cp -pr ${SourceDir}/${Source}/make* ./

ls -l
echo "===== Run make"

if ( ${NoOfProcs} > 1 ) then
  (make -j ${NoOfProcs} | tee makefile.out > /dev/tty) >& makefile.err
else
  (make --warn-undefined-variables > /dev/tty) >& makefile.err
endif
@ NoError = (-e ${Model})


if (${NoError}) then
  ls -l *${Model}*
  mv ${Model}   ${WorkDir}/${Model}.${RunID}
  echo " "
  echo " ====================================="
  echo "         compiling successful"
  echo " ====================================="
  echo " "
else
  echo "===== Compile messages:"
  cat makefile.err
  cp makefile.out ${WorkDir}/${Model}.${RunID}.make
  cp makefile.err ${WorkDir}/${Model}.${RunID}.err
  echo " "
  echo " #####################################"
  echo "             make error"
  echo " #####################################"
endif

echo "===== Copying listings and object files"
mkdir -p ${ListDir}
mkdir -p ${ObjDir}
cp -p *.lst  ${ListDir}
cp -p *.f90  ${ListDir}
cp -p *.o    ${ObjDir}
cp -p *.mod  ${ObjDir}

cd ${WorkDir}

if ( ! ${NoError} ) exit 10

echo "===== Copying run-scripts and namelist files"
mkdir -p ${ScriptDir}
cp -pr ${SourceDir}/${Script}/* ${ScriptDir}/.
cp -pr ${ScriptDir}/run-ecoham.sh ./run-ecoham.${RunID}.sh
if ( ${NoOfProcs} > 1 ) then
awk '{gsub("RUNID", "'${RunID}'");            \
      print}' ${ScriptDir}/RunJob-blizzard.sh > ./RunJob.${RunID}
else
awk '{gsub("RUNID", "'${RunID}'");            \
      print}' ${ScriptDir}/RunJob-blizzard-serial.sh > ./RunJob.${RunID}
endif

if ( ${ExecuteModel} ) then
  echo " ====================================="
  echo "          submitting run job"
  echo " ====================================="
  llsubmit RunJob.${RunID}
endif

exit 0
