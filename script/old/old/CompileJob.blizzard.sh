#!/bin/tcsh
# source /SX/opt/etc/initsx.csh

source /usr/lpp/ppe.hpct/env_csh

# module swap IBM/xlf12.1.0.3 IBM/xlf13.1.0.2

set NoOfProcs        = 16       # no of processors only for parallel compilation, not for run!
                                # because of all the dependencies, only serial comp is possible
set mpi_job          = 1        # parallel (= 1) or serial (= 0) model run

if ( ${NoOfProcs} >= 1 ) then
  set FORTRAN_COMPILER = xlf_mpi
else
  set FORTRAN_COMPILER = xlf
endif

set HomeDir      = $HOME
set ProjectDir   = `pwd`
set Source       = src
set Input        = input
set Script       = script
set Project      = ECOHAM

set Grid         = NWCS20
set GridID       = NWCS20D
set Model        = ecoham
set ModelRunJob  = RunJob.blizzard.sh

set what         = 0
set which        = 0

if ($1 == '') then
  echo "###############"
  echo "usage: "$0" RunID [what [which]]"
  echo " "
  echo "       RunID: RunID for simulation"
  echo "       what : = 0 for compilation only (default)"
  echo "              = 1 for compilation and preparation of run"
  echo "              = 2 for also submitting model run"
  echo "       which: eco_set_base[.which].nml - could be equal to RunID"
  echo "###############"
  exit 10
else
  set RunID = $1
  if ($2 != '') set what  = $2
  if ($3 != '') set which = $3
endif

set BaseDir  = /work/scratch/u/${USER}
set WorkDir  = ${BaseDir}/${Project}.${RunID}
set RunDir   = ${WorkDir}/wrk.${RunID}
set ListDir  = ${WorkDir}/list.${RunID}
set ObjDir   = ${WorkDir}/objects.${RunID}
set SourceDir = ${WorkDir}/src.${RunID}
set ScriptDir = ${WorkDir}/script.${RunID}
set InputDir = ${WorkDir}/input.${RunID}

echo '**********************************************************'
echo '***  running compile job for RunID '$RunID
echo '***                          Model '$Model
echo '***                         GridID '$GridID
echo '***               source code from '${ProjectDir}/${Source}
echo '***   using compiler configuration '${FORTRAN_COMPILER}'.config'
echo '***                  run directory '${RunDir}
echo '**********************************************************'

setenv FORTRAN_COMPILER ${FORTRAN_COMPILER}
setenv EXECUTABLE       ${Model}
setenv ECOHAM_GRID      ${GridID}

mkdir -p ${WorkDir}
mkdir -p ${SourceDir}
cd    ${SourceDir}
cp -p ${ProjectDir}/${Source}/*.f90  ./
cp -p ${ProjectDir}/${Source}/*.inc  ./
if (-e ${ObjDir}) then
  cp -p ${ObjDir}/*.o               ./
  cp -p ${ObjDir}/*.mod             ./
endif
cp -pr ${ProjectDir}/${Source}/make* ./

ls -l
echo "===== Run make"

if ( ${NoOfProcs} > 1 ) then
  (make -j ${NoOfProcs} | tee makefile.out > /dev/tty) >& makefile.err
else
  (make --warn-undefined-variables > /dev/tty) >& makefile.err
endif
@ NoError = (-e ${Model})

if (${NoError}) then
  ls -l *${Model}*
  mkdir -p ${RunDir}
  mv ${Model} ${RunDir}
  echo " "
  echo " ====================================="
  echo "         compiling successful"
  echo " ====================================="
  echo " "
else
  echo "===== Compile messages:"
  cat makefile.err
  cp makefile.out ${WorkDir}/${Model}.${RunID}.make
  cp makefile.err ${WorkDir}/${Model}.${RunID}.err
  echo " "
  echo " #####################################"
  echo "             make error"
  echo " #####################################"
endif

echo "===== Copying listings and object files"
mkdir -p ${ListDir}
mkdir -p ${ObjDir}
cp -p *.lst  ${ListDir}
cp -p *.f90  ${ListDir}
cp -p *.o    ${ObjDir}
cp -p *.mod  ${ObjDir}

cd ${WorkDir}
#rm -rf ${SourceDir}

if ( ! ${NoError} ) exit 10

if (${what} > 0) then

  echo "===== Copying run-scripts and namelist files"
  mkdir -p ${ScriptDir}
  mkdir -p ${InputDir}
  set RunJob = RunJob.${RunID}
#   if ( (${which} > 0) && -e ${ProjectDir}/${Input}/eco_set_base.${which}.nml ) then
#     cp ${ProjectDir}/${Input}/eco_set_base.${which}.nml ${InputDir}/eco_set_base.nml
#   else
    cp ${ProjectDir}/${Input}/eco_set_base.nml ${InputDir}
#   endif
#   if ( (${which} > 0) && -e ${ProjectDir}/${Input}/eco_bio.${which}.nml ) then
#     cp ${ProjectDir}/${Input}/eco_bio.${which}.nml      ${InputDir}/eco_bio.nml
#   else
    cp ${ProjectDir}/${Input}/eco_bio.nml      ${InputDir}
#   endif
  cp ${ProjectDir}/${Input}/host.list          ${InputDir}
  cp ${ProjectDir}/${Script}/link_input_files.blizzard.sh ${ScriptDir}
  awk '{gsub("RUNID", "'${RunID}'");            \
        print}' ${ProjectDir}/${ModelRunJob} > ${RunJob}

  ls -l

  if ( ${what} == 2 ) then
    echo " ====================================="
    echo "          submitting run job"
    echo " ====================================="
    echo " submitting   "${RunJob}
    echo " in directory "`pwd`
    llsubmit ${RunJob}
  endif

endif

exit 0
