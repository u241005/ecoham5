#!/bin/tcsh
#
#---------------------------------------
# parallel job
#---------------------------------------
#
# @ account_no = uo0121
# @ error    = RunJob.RUNID.$(jobid).out
# @ output   = RunJob.RUNID.$(jobid).out
# @ job_name = Run.RUNID
# @ job_type = serial
# @ environment = COPY_ALL
# @ node = 1
# @ tasks_per_node = 1
# @ node_usage= shared
# @ resources = ConsumableCpus(1) ConsumableMemory(4GB)
# @ wall_clock_limit = 02:00:00
# @ queue

setenv MP_EUILIB us
setenv MP_EUIDEVICE sn_all
setenv MP_SHARED_MEMORY yes
setenv MEMORY_AFFINITY MCM
setenv MP_SINGLE_THREAD yes

echo -n 'start time of this job: '
date
echo -n '               on host: '
hostname

setenv XLFRTEOPTS           "buffering=disable_preconn:ufmt_littleendian=120-129"

set yearDefault = 1997
set RunID       = RUNID
set executable  = ecoham
set this        = $0
set RunDir      = RUNDIR
set InputDir    = INPUTDIR
set ScriptDir   = SCRIPTDIR
set ResultPath  = OUTPUTDIR
set profiling   = 0
set mpitrace    = 0
set NamelistTemplate = NAMELIST_TEMPLATE

cd ${RunDir}

if (`setenv | grep yearStart` == '') then
  set yearS    = ${yearDefault}
else
  set yearS    = ${yearStart}
endif
if (`setenv | grep yearEnd` == '') then
  set yearE    = ${yearS}
else
  set yearE    = ${yearEnd}
endif
if (`setenv | grep yearLast` == '') set yearLast = ${yearE}
if (${yearS} > ${yearLast} || ${yearS} > ${yearE}) then
  echo '### start year after end year. Stopping this run. ###'
  exit 10
endif
if (${yearE} > ${yearLast}) then
  echo '### end year of this run too large, setting it to last year '${yearLast}
  set yearE = ${yearLast}
endif

echo '======================================================'
if (${yearS} != ${yearE}) then
  echo 'running '$executable' for RunID '$RunID' for '${yearS}'-'${yearE}' until '${yearLast}
else
  if (${yearS} != ${yearLast}) then
    echo 'running '$executable' for RunID '$RunID' yearly for '${yearS}' until '${yearLast}
  else
    echo 'running '$executable' for RunID '$RunID' for year '${yearS}
  endif
endif
echo 'in directory '$RunDir

set year = ${yearS}
while (${year} <= ${yearE})

  echo '======================================================'
  echo ' linking input files for year '${year}
  csh ${ScriptDir}/link_input_files.sh ${year}
  set stat = ${status}
  if (${stat} != 0) then
    echo '### link_input_files.sh exited with status '${stat}
    echo '### stopping run with same exit status'
    exit ${stat}
  endif

  #echo '======================================================'
  echo ' preparing eco_set.nml'
  set warm = 2
  set line = `cat ${InputDir}/${NamelistTemplate} | grep warmstart_file`
  set ext  = asc
  if (`echo $line[3] | awk '{print index($line[3],".bin")}'` != 0) set ext = bin
#   set warmstart_link = warmstart.${ext}
#   if (-l ${warmstart_link}) set warm = 1
#   echo ' setting warmstart to '${warm}
  awk '{gsub("RRRR", "'${RunID}'"); \
        gsub("YYYY", "'${year}'");  \
        print}' ${InputDir}/${NamelistTemplate} > eco_set.nml
  cp ${InputDir}/eco_bio.nml .
  #echo '======================================================'

  echo '======================================================'
  set warmstart_link = ${InputDir}/warmstart_NWCS20D.in
  ln -s  ${warmstart_link} warmstart.in
  echo ' linking warmstart.in to '${warmstart_link}
  echo '======================================================'

  if (${profiling}) then
    if (${mpitrace}) then
      setenv TRACE_ALL_EVENTS 0   # i.e. use profiling; for tracing you can limit the trace by MAX_TRACE_EVENTS
      setenv OUTPUT_ALL_RANKS YES # by default only task 0 and the MPI tasks with min, max and median results would write data
      setenv TRACEBACK_LEVEL  1   # or higher depending on MPI Call encapsulation level
      setenv MT_BASIC_TRACE   YES # to see traces of more than 256 MPI tasks, use the envVar TRACE_ALL_TASKS or MAX_TRACE_RANK
      ./${executable}
    else
      trcstop 2>/dev/null       # used to terminate eventually running trace background process
      /client/bin/tprof -usz -p ${executable} -x poe ${RunDir}/${executable}
    endif
  else
    echo ' running '${executable}
    ./${executable}
  endif

  if (-f Jeb_OK) then
    if (`cat Jeb_OK` != '1') then
      echo '###'
      echo '### Jeb_OK != 1, so we stop this run immediately with exit status 10.'
      echo '###'
      echo -n 'end time of this job: '
      date
      exit 10
    endif
  else
      echo '###'
      echo '### Jeb_OK does not exist, so we stop this run immediately with exit status 11.'
      echo '###'
      echo -n 'end time of this job: '
      date
      exit 11
  endif

  set resultDir = ${ResultPath}/${RunID}.${year}
  echo ' moving results to result directory '${resultDir}
  mkdir -p ${resultDir}
  mv ${RunID}* ${resultDir}

  echo ' moving logfile to result directory'
  mv eco_logfile.dat ${resultDir}

  if (-e stop_ecoham) break

  @ year ++

end

if (-e stop_ecoham) then
  echo '*** stop file exists, so we won''t proceed with further years.'
else

  if (${year} <= ${yearLast}) then
    @ yearI = ${yearE} - ${yearS}
    @ yearS = ${yearE} + 1
    @ yearE = ${yearS} + ${yearI}
    if (${yearE} > ${yearLast}) set yearE = ${yearLast}
    echo '*** calling next job for years '${yearS}' to '${yearE}
    echo '***             with last year '${yearLast}
    csh ${this} ${yearS} ${yearE} ${yearLast} >& log.${this}.${yearS}-${yearE}.txt &
  endif

endif

echo -n 'end time of this job: '
date
exit
