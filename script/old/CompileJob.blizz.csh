#!/bin/tcsh
# source /SX/opt/etc/initsx.csh

source /usr/lpp/ppe.hpct/env_csh

# module swap IBM/xlf12.1.0.3 IBM/xlf13.1.0.2

set NoOfProcs        = 1        # no of processors only for parallel compilation, not for run!
                                # because of all the dependencies, only serial comp is possible
set mpi_job          = 1        # parallel (= 1) or serial (= 0) model run
setenv FORTRAN_COMPILER xlf_mpi

set HomeDir      = $HOME
set SourceDir    = src
set SourceDir    = src_opt
set ScriptDir    = script
set Project      = cskc
set Grid         = cskc
set GridID       = cskc
set Model        = ecoham
set ModelRunJob  = run_eco.blizz.csh
set ProjectPath  = ${HomeDir}/ecoham_projects/${Project}
set BaseDir      = ${HomeDir}/run/ecoham_projects
set WorkDir      = ${BaseDir}/${Project}

set what         = 0
set which        = 0

if ($1 == '') then
  echo "###############"
  echo "usage: "$0" RunID [what [which]]"
  echo " "
  echo "       RunID: RunID for simulation"
  echo "       what : = 0 for compilation only (default)"
  echo "              = 1 for compilation and preparation of run"
  echo "              = 2 for also submitting model run"
  echo "       which: eco_set_base[.which].nml - could be equal to RunID"
  echo "###############"
  exit 10
else
  set RunID = $1
  if ($2 != '') set what  = $2
  if ($3 != '') set which = $3
endif

set RunDir   = ${WorkDir}/wrk.${RunID}

echo '**********************************************************'
echo '***  running compile job for RunID '$RunID
echo '***                          Model '$Model
echo '***                         GridID '$GridID
echo '***               source code from '${ProjectPath}/${SourceDir}
echo '***   using compiler configuration '${FORTRAN_COMPILER}'.config'
echo '***                  run directory '${RunDir}
echo '**********************************************************'

setenv ECOHAM_GRID      ${GridID}
setenv EXECUTABLE       ${Model}

set ListDir  = ${BaseDir}/lists/${Project}.${RunID}
set ObjDir   = ${BaseDir}/objects/${Project}.${RunID}
set TempDir  = ${WorkDir}/tmp.${SourceDir}.${RunID}
set ScriptDir = ${ProjectPath}/${ScriptDir}

mkdir -p ${WorkDir}
mkdir -p ${TempDir}

cd    ${TempDir}
cp -p ${ProjectPath}/${SourceDir}/*.f90  ./
cp -p ${ProjectPath}/${SourceDir}/*.inc  ./
if (-e ${ObjDir}) then
  cp -p ${ObjDir}/*.o               ./
  cp -p ${ObjDir}/*.mod             ./
endif
cp -pr ${ProjectPath}/${SourceDir}/make* ./

ls -l
echo "===== Run make"

if ( ${NoOfProcs} > 1 ) then
  (make -j ${NoOfProcs} | tee makefile.out > /dev/tty) >& makefile.err
else
  (make --warn-undefined-variables > /dev/tty) >& makefile.err
endif
@ NoError = (-e ${Model})

if (${NoError}) then
  ls -l *${Model}*
  mkdir -p ${RunDir}
  mv ${Model} ${RunDir}
  echo " "
  echo " ====================================="
  echo "         compiling successful"
  echo " ====================================="
  echo " "
else
  echo "===== Compile messages:"
  cat makefile.err
  cp makefile.out ${WorkDir}/${Model}.${RunID}.make
  cp makefile.err ${WorkDir}/${Model}.${RunID}.err
  echo " "
  echo " #####################################"
  echo "             make error"
  echo " #####################################"
endif

echo "===== Copying listings and object files"
mkdir -p ${ListDir}
mkdir -p ${ObjDir}
cp -p *.lst  ${ListDir}
cp -p *.f90  ${ListDir}
cp -p *.o    ${ObjDir}
cp -p *.mod  ${ObjDir}

cd ${WorkDir}
rm -rf ${TempDir}

if ( ! ${NoError} ) exit 10

if (${what} > 0) then

  echo "===== Copying run-scripts and namelist files"
  set RunJob = run_eco.csh
  if ( ${which} > 0 ) then
    cp ${ProjectPath}/eco_set_base.${which}.nml ${RunDir}/eco_set_base.nml
  else
    cp ${ProjectPath}/eco_set_base.nml ${RunDir}
  endif
  cp ${ScriptDir}/link_input_files.blizz.csh ${RunDir}
  if ( ${mpi_job} == 1 ) then
    awk '{gsub("runid", "'${RunID}'"); print}' ${ScriptDir}/${ModelRunJob} > ${RunDir}/${RunJob}
  else
    awk '{gsub("runid", "'${RunID}'"); print}' ${ScriptDir}/run_eco_serial.csh > ${RunDir}/${RunJob}
  endif
  ls -l ${RunDir}

  if ( ${what} == 2 ) then
    echo " ====================================="
    echo "          submitting run job"
    echo " ====================================="
    echo " submitting   "${RunJob}
    echo " in directory "${RunDir}
    cd ${RunDir}
    llsubmit ${RunJob}
  endif

endif

exit 0
