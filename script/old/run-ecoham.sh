#!/bin/bash
# ecoham job script for dual use
# standalone for serial runs on linux systems
# or included (source cmd) by a test or batch jobscript

EXECUTE_CMD=${EXECUTE_CMD:=""}
BATCH_CMD=${BATCH_CMD:=""}
JOBSCRIPT_NAME=${JOBSCRIPT_NAME:=$0}

wrkDIR=$(pwd)
# create shorter pathnames for result directory and links
cd ..
runDIR=$(pwd)
cd ${wrkDIR}
resultBaseDir=${runDIR}/res
ECOHAM_GRID='NS03A'
ECOHAM_GRID='NWCS20D'
EXECUTABLE=ecoham_${ECOHAM_GRID}

#-----------------------
# job specific settings
#-----------------------
# format for DateStrings is yyyy-mm-dd
# default time frame for the job chain is one year
jobChain_StartDateStr=2013-01-01
jobChain_StartDateStr=1997-01-01
# uncomment and set jobChain_EndDateStr if less than one year should be processed 
jobChain_EndDateStr=2013-01-01
jobChain_EndDateStr=1997-01-01

# format for TimeInterval is "time unit", e.g. "15 days" or "1 month" (see the unix command date)
#job_TimeInterval="15 days"
job_TimeInterval="1 month"
job_TimeInterval="5 days"

jobChain_EndDateStrDefault=$(date --date "$jobChain_StartDateStr +1 years -1 days" "+%Y-%m-%d")
jobChain_EndDateStr=${jobChain_EndDateStr:-$jobChain_EndDateStrDefault}
if [[ $jobChain_EndDateStr < $jobChain_StartDateStr ]]; then
   echo -e "JOBCHAIN ERROR: \t begin $jobChain_StartDateStr \t end $jobChain_EndDateStr"
   exit
fi

if [ -f job-datestring.txt ]; then
   job_StartDateStr=$(date --date "$(cat job-datestring.txt) +1 days" "+%Y-%m-%d")
else
   job_StartDateStr=$jobChain_StartDateStr    # first job of chain
   #cp forcing-3km/eco5_warm_3km.in-old-structure NS03A_warmstart.in
   #cp forcing-3km/NS03A_warmstart.in.bkp-new-no-chemistry NS03A_warmstart.in
fi

# minus 1 day because program start is at 00:00:00 and end at 24:00:00
job_EndDateStr=$(date --date "$job_StartDateStr +$job_TimeInterval -1 days" "+%Y-%m-%d")
if [[ $job_EndDateStr > $jobChain_EndDateStr ]]; then
   job_EndDateStr=$jobChain_EndDateStr
fi

# get substrings for directory path and date
# for runs within a year => end_yyyy is not needed
startdateDirStr=$(date --date "$job_StartDateStr" "+%Y.%m.%d")
start_yyyy=${startdateDirStr:0:4}
start_mm=${startdateDirStr:5:2}
start_dd=${startdateDirStr:8:2}
enddateDirStr=$(date --date "$job_EndDateStr" "+%m.%d")
end_mm=${enddateDirStr:0:2}
end_dd=${enddateDirStr:3:2}

# create job specific result subdirectory
resultDir=${resultBaseDir}/run-${startdateDirStr}-${enddateDirStr}
#echo $resultDir

if ! test -d ${resultBaseDir} ; then
   mkdir ${resultBaseDir}
fi
if ! test -f ${resultBaseDir}/concat-prt.py ; then
   cd ${resultBaseDir}
   ln -sf ${wrkDIR}/concat-prt.py
   ln -sf ${wrkDIR}/concat-logfile.sh
   ln -sf ${wrkDIR}/concat-subset.sh
   ln -sf ${wrkDIR}/prepare-tar.sh
   cd ${wrkDIR}
fi

# create eco_set.nml from template
sed -e "s/YYYY/${start_yyyy}/" \
    -e "s/START_MM/      ${start_mm}/" \
    -e "s/START_DD/      ${start_dd}/" \
    -e "s/END_MM/    ${end_mm}/" \
    -e "s/END_DD/    ${end_dd}/" \
    eco_set.nml.template-${ECOHAM_GRID} > eco_set.nml
echo eco_set.nml has been created from template file eco_set.nml.template-${ECOHAM_GRID}

# run executable
echo '======================================================'
echo 'wrkDIR:='$wrkDIR
echo '------------------------------------------------------'

rm -f eco_logfile.dat

echo ' '
echo '======================================================'
echo '  '${EXECUTABLE}' start running at '`date`
echo '------------------------------------------------------'

${EXECUTE_CMD} ./${EXECUTABLE}

echo '------------------------------------------------------'
echo '  '${EXECUTABLE}' finished at      '`date`
echo '======================================================'
echo ' '

# job postprocessing
#====================
mkdir ${resultDir}
echo ${resultDir}
# value 1 in Jeb_OK means correct end of program
JobStatus=$(cat Jeb_OK)
echo JobStatus ${JobStatus}
if (( JobStatus==1 ))
then
   mv ${ECOHAM_GRID}_warmstart.out  ${resultDir}
   ln -sf ${resultDir}/${ECOHAM_GRID}_warmstart.out ${ECOHAM_GRID}_warmstart.in
   cp eco_bio.nml          ${resultDir}
   mv eco_set.nml          ${resultDir}
   mv eco_logfile.dat      ${resultDir}
   if [ -f job-datestring.txt ]; then
      cp job-datestring.txt   ${resultDir}
   fi
   mv *.prt                ${resultDir}
   mv ${ECOHAM_GRID}_3D.nc ${resultDir}
   if [ -f job.???.* ]; then
      mv job.???.*            ${resultDir}
   fi
else
   echo "error in job -> exit"
   exit
fi

if [[ $job_EndDateStr = $jobChain_EndDateStr ]] ; then
   rm -f job-datestring.txt
   rm Jeb_OK
   echo last job in chain
   echo -----------------
   exit
else
   echo $job_EndDateStr > job-datestring.txt
   echo submit new job
   echo --------------
   ${BATCH_CMD} ${JOBSCRIPT_NAME}
fi
