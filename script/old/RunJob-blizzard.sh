#!/bin/bash
# blizzard specific settings for batch and interactive processing of ecoham runs
# @ account_no = ku0646
# @ error    = job.err.$(jobid)
# @ output   = job.out.$(jobid)
# @ job_name = ecoham
# @ class    = cluster
# @ job_type = parallel
# @ rset     = rset_mcm_affinity
# @ mcm_affinity_options = mcm_accumulate
# @ task_affinity = core(1)
# @ job_type = parallel
# @ environment = COPY_ALL

# @ node_usage = not_shared
# @ network.MPI = sn_all,not_shared,us

# @ node = 1
# @ tasks_per_node = 32
# @ resources = ConsumableMemory(3000mb)
# @ wall_clock_limit = 08:00:00
# @ notification = error
# @ queue

export MEMORY_AFFINITY=MCM
#export MP_PRINTENV=YES
#export MP_LABELIO=YES
#export MP_INFOLEVEL=2
export MP_EAGER_LIMIT=64k
export MP_BUFFER_MEM=64M,256M
export MP_USE_BULK_XFER=NO
export MP_BULK_MIN_MSG_SIZE=128k
export MP_RFIFO_SIZE=4M
export MP_SHM_ATTACH_THRESH=500000
export LAPI_DEBUG_STRIPE_SEND_FLIP=8

EXECUTE_CMD=poe
#EXECUTE_CMD=""
BATCH_CMD=llsubmit
#BATCH_CMD=""
JOBSCRIPT_NAME=$0

# required due to a problem with the default version of date on the interactive blizzard node p249
if [ $(hostname) = "p249" ]; then
   alias date='/client/bin/date'
fi

# load run script  !!!check if the value of job_TimeInterval fits into the wall_clock_limit!!!
. run-ecoham.sh
