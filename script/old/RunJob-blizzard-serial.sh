#!/bin/bash
# blizzard specific settings for serial processing of ecoham runs
#
# @ error    = RunJob.RUNID.$(jobid).out
# @ output   = RunJob.RUNID.$(jobid).out
# @ job_name = Run.RUNID
# @ job_type = serial
# @ environment= COPY_ALL
# @ node_usage= shared
# @ node = 1
# @ tasks_per_node = 1
# @ resources = ConsumableCpus(1) ConsumableMemory(4gb)
# @ wall_clock_limit = 08:00:00
# @ notification = error
# @ queue

#
# node            : enthaelt 16 physikalische CPUs, die je zwei Threads ausfuehren koennen
# task_per_node   : Anzahl der MPI-Prozesse (CPUs) fuer den Job
# ConsumableCpus  : Anzahl der Threads pro task_per_node
# ConsumableMemory: Memory pro Thread
#

echo -n "Job started at: "
date
echo -n "       on host: "
hostname

export MEMORY_AFFINITY=MCM
#export MP_PRINTENV=YES
#export MP_LABELIO=YES
#export MP_INFOLEVEL=2
export MP_EAGER_LIMIT=64k
export MP_BUFFER_MEM=64M,256M
export MP_USE_BULK_XFER=NO
export MP_BULK_MIN_MSG_SIZE=128k
export MP_RFIFO_SIZE=4M
export MP_SHM_ATTACH_THRESH=500000
export LAPI_DEBUG_STRIPE_SEND_FLIP=8

EXECUTE_CMD=poe
#EXECUTE_CMD=""
BATCH_CMD=llsubmit
#BATCH_CMD=""
JOBSCRIPT_NAME=$0

# required due to a problem with the default version of date on the interactive blizzard node p249
if [ $(hostname) = "p249" ]; then
   alias date='/client/bin/date'
fi

# load run script  !!!check if the value of job_TimeInterval fits into the wall_clock_limit!!!
. run-ecoham.sh
