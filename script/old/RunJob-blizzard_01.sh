#!/bin/tcsh
#
#---------------------------------------
# parallel job
#---------------------------------------
#
# @ account_no = uo0121
# @ error    = RunJob.RUNID.$(jobid).out
# @ output   = RunJob.RUNID.$(jobid).out
# @ job_name = Run.RUNID
# @ job_type = parallel
# @ rset     = rset_mcm_affinity
# @ mcm_affinity_options = mcm_accumulate
# @ resources = ConsumableMemory(1500mb)
#   resources = ConsumableMemory(1000mb)
# @ environment = COPY_ALL
# @ task_affinity = core(1)
# @ node = 1
# @ tasks_per_node = 16
#   task_affinity = cpu(1)
#   node = 1
#   tasks_per_node = 64
# @ node_usage = shared
# @ network.MPI = sn_all,shared,us
# @ wall_clock_limit = 02:00:00
#   wall_clock_limit = 00:20:00
#   class            = express
# @ notification = error
# @ queue
#
# node            : enthaelt 16 physikalische CPUs, die je zwei Threads ausfuehren koennen
# task_per_node   : Anzahl der MPI-Prozesse (CPUs) fuer den Job
# ConsumableCpus  : Anzahl der Threads pro task_per_node
# ConsumableMemory: Memory pro Thread
#

echo -n 'start time of this job: '
date
echo -n '               on host: '
hostname

setenv MEMORY_AFFINITY MCM
##########setenv MP_PRINTENV YES
setenv MP_LABELIO YES
##########setenv MP_INFOLEVEL 2
setenv MP_EAGER_LIMIT 64k
setenv MP_BUFFER_MEM "64M,256M"
setenv MP_USE_BULK_XFER NO
setenv MP_BULK_MIN_MSG_SIZE 128k
setenv MP_RFIFO_SIZE 4M
setenv MP_SHM_ATTACH_THRESH 500000
setenv LAPI_DEBUG_STRIPE_SEND_FLIP 8

setenv XLFRTEOPTS           "buffering=disable_preconn:ufmt_littleendian=120-129"
# setenv TARGET_CPU_LIST    -1
# setenv MP_TASK_AFFINITY   core
# setenv MP_SHARED_MEMORY   yes
setenv MEMORY_AFFINITY      MCM@LRU=EARLY
setenv HPM_AGGREGATE        average.so
# setenv HPM_AGGREGATE        loc_merge.so
setenv HPM_UNIQUE_FILE_NAME yes
setenv HPM_PRINT_FORMULA    yes
# setenv HPM_EVENT_SET        47,104,108

set yearDefault = 1997
set RunID       = RUNID
set executable  = ecoham
set this        = $0
set RunDir      = RUNDIR
set InputDir    = INPUTDIR
set ScriptDir   = SCRIPTDIR
set ResultPath  = OUTPUTDIR
set profiling   = 0
set mpitrace    = 0
set NamelistTemplate = NAMELIST_TEMPLATE

cd ${RunDir}

if (`setenv | grep yearStart` == '') then
  set yearS    = ${yearDefault}
else
  set yearS    = ${yearStart}
endif
if (`setenv | grep yearEnd` == '') then
  set yearE    = ${yearS}
else
  set yearE    = ${yearEnd}
endif
if (`setenv | grep yearLast` == '') set yearLast = ${yearE}
if (${yearS} > ${yearLast} || ${yearS} > ${yearE}) then
  echo '### start year after end year. Stopping this run. ###'
  exit 10
endif
if (${yearE} > ${yearLast}) then
  echo '### end year of this run too large, setting it to last year '${yearLast}
  set yearE = ${yearLast}
endif

echo '======================================================'
if (${yearS} != ${yearE}) then
  echo 'running '$executable' for RunID '$RunID' for '${yearS}'-'${yearE}' until '${yearLast}
else
  if (${yearS} != ${yearLast}) then
    echo 'running '$executable' for RunID '$RunID' yearly for '${yearS}' until '${yearLast}
  else
    echo 'running '$executable' for RunID '$RunID' for year '${yearS}
  endif
endif
echo 'in directory '$RunDir

echo '======================================================'
set warmstart_link = ${InputDir}/warmstart_NWCS20D.in
ln -s  ${warmstart_link} warmstart.in
echo ' linking warmstart.in to '${warmstart_link}
echo '======================================================'

set year = ${yearS}
while (${year} <= ${yearE})

  echo '======================================================'
  echo ' linking input files for year '${year}
  csh ${ScriptDir}/link_input_files.sh ${year}
  set stat = ${status}
  if (${stat} != 0) then
    echo '### link_input_files.sh exited with status '${stat}
    echo '### stopping run with same exit status'
    exit ${stat}
  endif

  #echo '======================================================'
  echo ' preparing eco_set.nml'
  awk '{gsub("RRRR", "'${RunID}'"); \
        gsub("YYYY", "'${year}'");  \
        print}' ${InputDir}/${NamelistTemplate} > eco_set.nml
  cp ${InputDir}/eco_bio.nml .
  #echo '======================================================'

  if (${profiling}) then
    if (${mpitrace}) then
      setenv TRACE_ALL_EVENTS 0   # i.e. use profiling; for tracing you can limit the trace by MAX_TRACE_EVENTS
      setenv OUTPUT_ALL_RANKS YES # by default only task 0 and the MPI tasks with min, max and median results would write data
      setenv TRACEBACK_LEVEL  1   # or higher depending on MPI Call encapsulation level
      setenv MT_BASIC_TRACE   YES # to see traces of more than 256 MPI tasks, use the envVar TRACE_ALL_TASKS or MAX_TRACE_RANK
      poe ./${executable} -hfile ${InputDir}/host.list
    else
      trcstop 2>/dev/null       # used to terminate eventually running trace background process
      /client/bin/tprof -usz -p ${executable} -x poe ${RunDir}/${executable}
    endif
  else
    echo ' running '${executable}
    poe ./${executable} -hfile ${InputDir}/host.list
  endif

  if (-f Jeb_OK) then
    if (`cat Jeb_OK` != '1') then
      echo '###'
      echo '### Jeb_OK != 1, so we stop this run immediately with exit status 10.'
      echo '###'
      echo -n 'end time of this job: '
      date
      exit 10
    endif
  else
      echo '###'
      echo '### Jeb_OK does not exist, so we stop this run immediately with exit status 11.'
      echo '###'
      echo -n 'end time of this job: '
      date
      exit 11
  endif

  set resultDir = ${ResultPath}/${RunID}.${year}
  echo ' moving results to result directory '${resultDir}
  mkdir -p ${resultDir}
  mv ${RunID}* ${resultDir}

  echo ' moving logfile to result directory'
  mv eco_logfile.dat ${resultDir}

  if (-e stop_ecoham) break

  @ year ++

end

if (-e stop_ecoham) then
  echo '*** stop file exists, so we won''t proceed with further years.'
else

  if (${year} <= ${yearLast}) then
    @ yearI = ${yearE} - ${yearS}
    @ yearS = ${yearE} + 1
    @ yearE = ${yearS} + ${yearI}
    if (${yearE} > ${yearLast}) set yearE = ${yearLast}
    echo '*** calling next job for years '${yearS}' to '${yearE}
    echo '***             with last year '${yearLast}
    csh ${this} ${yearS} ${yearE} ${yearLast} >& log.${this}.${yearS}-${yearE}.txt &
  endif

endif

echo -n 'end time of this job: '
date
exit
