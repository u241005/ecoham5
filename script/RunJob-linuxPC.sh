#!/bin/tcsh
#
#---------------------------------------
# single serial job on linux hosts
#---------------------------------------
#

set submit_cmd   = sbatch

echo -n 'start time of this job: '
date
echo -n '               on host: '
hostname

# ECOHAM input-units (forcing):
# hydro : 120-129
# meteo : 130-139 (wins_unit=130, radday_unit=131, rad_unit=132, extw_unit=133, prec_evap_unit=134, N_depos_unit=135)
# river : 140-149 (river_unit=140, freshwater_unit=141, silt_unit=145)
# restoring  : 150-200

set RunID       = RUNID
set executable  = EXECUTABLE
set this        = $0
set ProjectDir  = PROJECTDIR
set SourceDir   = SOURCEDIR
set RunDir      = RUNDIR
set InputDir    = INPUTDIR
set ScriptDir   = SCRIPTDIR
set ResultPath  = OUTPUTDIR
set profiling   = 0
set mpitrace    = 0
set NamelistTemplate = NAMELIST_TEMPLATE
set RiverCoefficients = RIVER_COEFFICIENTS

set yearStart   = 2000
set yearEnd     = 2000

set nSpinup     = 0

#---------------------------------
set On  = 1
set Off = 0
set StartAgain  = $Off
set ReadYear    = $On
set Warmstart   = $On
#---------------------------------

if (${nSpinup} <= 0) then
   set Spinup = $Off
else
   set Spinup = $On
endif

# - set up run directory
mkdir -p ${RunDir}
cd ${RunDir}

# - get executable
cp ${SourceDir}/${executable} ${RunDir}/.

# - set run year
if ( -e ${ProjectDir}/Year.${RunID} && ${ReadYear} ) then
  set year = `cat ${ProjectDir}/Year.${RunID}`
else
  set year = ${yearStart}
endif

set yearS    = ${yearStart}
set yearE    = ${yearEnd}

if (`setenv | grep yearLast` == '') set yearLast = ${yearE}
if (${yearS} > ${yearLast} || ${yearS} > ${yearE}) then
  echo '### start year after end year. Stopping this run. ###'
  exit 10
endif
if (${yearE} > ${yearLast}) then
  echo '### end year of this run too large, setting it to last year '${yearLast}
  set yearE = ${yearLast}
endif

echo '======================================================'
echo 'running '$executable' for RunID '$RunID' for year '${year}
echo 'in directory '$RunDir
echo '======================================================'

if (-e ${ResultPath}/warmstart.in) then
   set Warmstart = $On
   set warmstart_link = ${ResultPath}/warmstart.in
   echo '-get warmstart for '${year}' from ...'
   echo '   '${warmstart_link}
   cp  ${warmstart_link} warmstart.in
   echo '   md5sum: ' `md5sum warmstart.in`
else
   if (${Warmstart}) then
      set warmstart_link = ${InputDir}/warmstart_NWCS20D.in
      #set warmstart_link = ${InputDir}/B016_warmstart_1995-01-01.out
      #set warmstart_link = ${InputDir}/B023_warmstart_2000-01-01.out
      echo '-get initial warmstart for '${year}' from ...'
      echo '   '${warmstart_link}
      cp  ${warmstart_link} warmstart.in
      echo '   md5sum: ' `md5sum warmstart.in`
   endif
endif
if (! -e warmstart.in) set Warmstart = $Off

echo '-linking input files for year '${year}
csh ${ScriptDir}/link_input_files.sh ${year}
set stat = ${status}
if (${stat} != 0) then
   echo '### link_input_files.sh exited with status '${stat}
   echo '### stopping run with same exit status'
   exit ${stat}
endif

echo '-preparing eco_set.nml'
awk '{gsub("RRRR", "'${RunID}'"); \
      gsub("YYYY", "'${year}'");  \
      gsub("IWARM", "  '${Warmstart}' ");  \
      print}' ${InputDir}/${NamelistTemplate} > eco_set.nml
cp ${InputDir}/eco_bio.nml .
cp ${InputDir}/eco_recomCNP.nml .

cp ${InputDir}/${RiverCoefficients} RiverCoefficients.dat

echo '======================================================'
echo ' running '${executable}
echo '======================================================'
echo ''
if (${profiling}) then
   if (${mpitrace}) then
      setenv TRACE_ALL_EVENTS 0   # i.e. use profiling; for tracing you can limit the trace by MAX_TRACE_EVENTS
      setenv OUTPUT_ALL_RANKS YES # by default only task 0 and the MPI tasks with min, max and median results would write data
      setenv TRACEBACK_LEVEL  1   # or higher depending on MPI Call encapsulation level
      setenv MT_BASIC_TRACE   YES # to see traces of more than 256 MPI tasks, use the envVar TRACE_ALL_TASKS or MAX_TRACE_RANK
      poe ./${executable} -hfile ${InputDir}/host.list
   else
      trcstop 2>/dev/null       # used to terminate eventually running trace background process
      /client/bin/tprof -usz -p ${executable} -x poe ${RunDir}/${executable}
   endif
else
   #srun -l --cpu_bind=verbose,cores --distribution=block:cyclic ./${executable} -hfile ${InputDir}/host.list
   #srun -l --cpu_bind=verbose,cores --distribution=block:cyclic ./${executable}
   ./${executable}
endif

if (-f Jeb_OK) then
   if (`cat Jeb_OK` != '1') then
   echo '###'
   echo '### Jeb_OK != 1, so we stop this run immediately with exit status 10.'
   echo '###'
   echo -n 'end time of this job: '
   date
   exit 10
   endif
else
   echo '###'
   echo '### Jeb_OK does not exist, so we stop this run immediately with exit status 11.'
   echo '###'
   echo -n 'end time of this job: '
   date
   exit 11
endif

echo ''
echo '======================================================'
echo ' running '${executable}' successful - wrapping up for '${RunID}' ...'
echo '======================================================'
#+----------------------------------------------------+
#|                 find free directory                |
#+----------------------------------------------------+
set Counter =  0
set JID     = 00
while ( -d ${ResultPath}/${RunID}.${year}.${JID} )
  @ Counter++
  set JID = ${Counter}
  if ( $Counter < 10 ) set JID = 0${JID}
end
set resultDir = ${ResultPath}/${RunID}.${year}.${JID}
mkdir -p ${resultDir}

#+----------------------------------------------------+
#|                evaluate spinup mode                |
#+----------------------------------------------------+
if (${Spinup} && ${year} == ${yearS}) then
   if ( $Counter > $nSpinup ) then
      set Spinup = $Off
   else
      echo ''
      echo '++++++++++++++++++++++++++++++++++++++++'
      echo '+++   this was spinup run no. '$Counter
      echo '++++++++++++++++++++++++++++++++++++++++'
   endif
else
   set Spinup = $Off
endif

#+----------------------------------------------------+
#|             save restartfile                       |
#+----------------------------------------------------+
echo '-copy warmstart.out to warmstart.in ...'
cp   ${RunID}_warmstart_*.out ${ResultPath}/warmstart.in
echo '   md5sum: ' `md5sum ${RunID}_warmstart_*.out`

#+----------------------------------------------------+
#|             save files                             |
#+----------------------------------------------------+
echo '-store results in directory ...'
echo '   '${resultDir}
mv ${RunID}*  ${resultDir}/.

#+----------------------------------------------------+
#|             save setup                             |
#+----------------------------------------------------+
echo '-fingerprint Input directory ...'
md5sum Input/* >  ${resultDir}/Input_${RunID}.md5sum
ls -l  Input/* >> ${resultDir}/Input_${RunID}.md5sum

echo '-moving logfile to result directory'
mv *logfile*       ${resultDir}/.
mv *.nml           ${resultDir}/.

#echo '-compressing nc-files'
#cd ${resultDir}
#gzip *.nc &
#cd ${RunDir}

#+----------------------------------------------------+
#|             save profiling                         |
#+----------------------------------------------------+
if (${profiling}) then
   if (${mpitrace}) then
      set profileDir = ${ProjectDir}/mpitrace.${RunID}/${RunID}.${year}.${JID}
      mkdir -p ${profileDir}
      echo '-store mpitrace profiling in ...'
      echo '   '${profileDir}
      mv *profile*  ${profileDir}/.
      mv *trace*    ${profileDir}/.
   endif
endif

#+----------------------------------------------------+
#|                 start new job                      |
#+----------------------------------------------------+
echo '======================================================'
if (-e stop_ecoham) then
  echo '*** stop file exists, so we won''t proceed with further years.'
else

  cd ${ProjectDir}
  if  ( ${StartAgain} ) then
    if (${year} < ${yearLast}) then
      if (${Spinup} == $Off) @ year = ${year} + 1
      echo '*** calling next job for year '${year}
      echo ${year} >! ${ProjectDir}/Year.${RunID}
      #${submit_cmd} ${ProjectDir}/RunJob.${RunID} # if slurm is not working
      ./RunJob.${RunID}
      rm -r ${RunDir}
    endif
  endif

endif

echo -n 'end time of this job: '
date
exit
