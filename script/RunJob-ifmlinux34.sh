#!/bin/tcsh
#
#---------------------------------------
# single serial job on linux hosts
#---------------------------------------
#


echo -n 'start time of this job: '
date
echo -n '               on host: '
hostname

set RunID       = $1
set year        = $2

set executable  = ecoham5
set this        = $0
set RunDir      = `pwd`
set SourceDir   = '../src'
set InputDir    = '../input'
set NamelistTemplate = 'eco_set.template.nml'
set ScriptDir   = '../script'
set LinkerScript = 'link_input_files.ifmlinux34.sh'


echo '======================================================'
echo 'running '$executable' with RunID '$RunID' for year '${year}
echo 'in directory '$RunDir
echo '======================================================'

# - get executable
#if ( ! -e ${RunDir}/${executable} ) then
if ( -e ${RunDir}/${executable} ) then
   rm ${RunDir}/${executable}
endif
#echo '-get executable from '${SourceDir}/${executable}' ...'
echo '-link executable from '${SourceDir}/${executable}' ...'
ln -s ${SourceDir}/${executable} ${RunDir}/.
echo '   md5sum: ' `md5sum ${executable}`


set warmstart_link = ${InputDir}/warmstart_NWCS20D.in
echo '-get initial warmstart for '${year}' from ...'
echo '   '${warmstart_link}
cp  ${warmstart_link} warmstart.in
echo '   md5sum: ' `md5sum warmstart.in`

echo '-linking input files for year '${year}
csh ${ScriptDir}/${LinkerScript} ${year}
set stat = ${status}
if (${stat} != 0) then
   echo '### '${LinkerScript}' exited with status '${stat}
   echo '### stopping run with same exit status'
   exit ${stat}
endif

echo '-preparing eco_set.nml'
awk '{gsub("RRRR", "'${RunID}'"); \
      gsub("YYYY", "'${year}'");  \
      print}' ${InputDir}/${NamelistTemplate} > eco_set.nml
cp ${InputDir}/eco_bio.nml .

echo '======================================================'
echo ' running '${executable}
echo '======================================================'
echo ''
./${executable}

if (-f Jeb_OK) then
   if (`cat Jeb_OK` != '1') then
   echo '###'
   echo '### Jeb_OK != 1, so we stop this run immediately with exit status 10.'
   echo '###'
   echo -n 'end time of this job: '
   date
   exit 10
   endif
else
   echo '###'
   echo '### Jeb_OK does not exist, so we stop this run immediately with exit status 11.'
   echo '###'
   echo -n 'end time of this job: '
   date
   exit 11
endif

echo ''
echo '======================================================'
echo ' running '${executable}' successful - wrapping up for '${RunID}' ...'
echo '======================================================'


echo -n 'end time of this job: '
date
exit
